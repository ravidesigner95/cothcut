import axios from 'axios';

class LineLoadingService {
    constructor() { }
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    orderUrl = process.env.REACT_APP_BASEURL_DASHBOARD;
    plantListUrl = this.baseUrl + `/master/plant`;
    unitListUrl = this.plantListUrl;
    unitLinesUrl = this.baseUrl + `/master`;
    apmUrl = this.baseUrl + `/unit/apm`;
    selectedBuyerPoLineItemsUrlOrder = this.orderUrl + `/orderservice/v1/order/buyer_pos`;
    supervisorUrl = this.baseUrl + `/user/SUPERVISOR`;
    buyerPoUrlLine = this.baseUrl + `/load/buyerPo`;
    purchaseOrderListUrl = this.baseUrl + `/load/prodorder`;
    loadUrl = this.baseUrl + `/load/getLoads`;
    loadLineURL = this.baseUrl + `/load/line`;
    scheduledLinesUrl = this.baseUrl + `/load/scheduled`;
    addLineLoadURL = this.baseUrl + `/load/doLoad`;
    deleteLineUrl = this.baseUrl + `/load/deleteLoad`;
    editLineLoadURL = this.baseUrl + `/load/editLoad`;
    lineDetailURL = this.baseUrl + `/load/line/detail`;
    confgkeyUrl = this.baseUrl + `/appConfg/appCnfgkey`;
    updateStoreUrl = this.baseUrl + `/load/refresh`;
    maxLoadUrl = this.baseUrl + `/load/maxLoad`;
    prodConfUrl = this.baseUrl + `/prod_confirm/failed`;
    editProdConfUrl = this.baseUrl + `/prod_confirm/edit`;
    transferLoadURL = this.baseUrl + `/load/transferLoad`;
    generateReportUrl = this.baseUrl + `/hr/employee/saveReportOfLine`
    downloadReportUrl = this.baseUrl + `/hr/employee/downloadReportOfLine`
    updateSapUrl = this.baseUrl + `/load/pushInSap`;
    revertConfUrl = this.baseUrl + `/prod_confirm/revertConfirmation`
    generateValidationReportUrl = this.baseUrl + `/load/generateDistributedReport`


    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    //---------------------------------------API-CALLS-----------------------------------------//

    async getConfgKey() {
        try {
            const response = await axios.get(this.confgkeyUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getProdConf(LoadedLineId) {
        try {
            const response = await axios.get(this.prodConfUrl + `/${LoadedLineId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async editProdConf(editedProdConfObj) {
        try {
            const response = await axios.put(this.editProdConfUrl, editedProdConfObj, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }


    async getMaxLoad(saidId, lineItemNo, buyerPoNo, prodOrderId) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    said_id: saidId,
                    line_item: lineItemNo,
                    buyer_po: buyerPoNo,
                    prod_order_id: prodOrderId,
                }
            };
            return await axios.get(this.maxLoadUrl, config);
        }
        catch (error) {
            return error.response;

        }
    }

    async getSupervisorList() {
        try {
            const response = await axios.get(this.supervisorUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getBuyerPoList(plantIds, unitName) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plantIds: plantIds + '',
                    unit: unitName
                }
            };
            return await axios.get(this.buyerPoUrlLine, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async updateStore(saidId, LineItemNo, BuperPoNo) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    said_id: saidId,
                    line_item_no: LineItemNo,
                    buyer_po_no: BuperPoNo
                }
            };
            return await axios.get(this.updateStoreUrl, config);
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async updateSap(loadId,modifiedOn) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    load_id: loadId,
                    modifiedOn: modifiedOn
                
                }
            };
            return await axios.get(this.updateSapUrl, config);
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async getSelectedBuyerPoLineItems(saidId, buyerPoNo, plantId) {
        try {
            const response = await axios.get(this.selectedBuyerPoLineItemsUrlOrder + `/${saidId}/${buyerPoNo}/${plantId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getScheduledLines(plantId, unitId) {

        try {
            const response = await axios.get(this.scheduledLinesUrl + `/${plantId}/${unitId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getProductionOrderList(saidId, lineItem, buyerPO) {

        try {
            const config = {
                headers: this.options.headers,
                params: {
                    said_id: saidId,
                    line_item: lineItem,
                    buyer_po: buyerPO
                }
            };
            const response = await axios.get(this.purchaseOrderListUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getLoadedLines(plantId, unitId) {

        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plantId: plantId,
                    unitId: unitId,
                }
            };
            const response = await axios.get(this.loadUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getLineDetails(prodLine, prodDate, saidId, lineItem, buyerPO) {

        try {
            const config = {
                headers: this.options.headers,
                params: {
                    prod_line: prodLine,
                    prod_date: prodDate,
                    said_id: saidId,
                    line_item: lineItem,
                    buyer_po: buyerPO
                }
            };
            const response = await axios.get(this.lineDetailURL, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getPlantList() {
        try {
            const response = await axios.get(this.plantListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getLoad(prodIds, prodDate) {
        let response = [];
        let allApiCalls = [];
        prodIds.map(item => {
            const config = {
                headers: this.options.headers,
                params: {
                    prod_line: item,
                    prod_date: prodDate,
                }
            };
            allApiCalls.push(axios.get(this.loadLineURL, config))
        })

        try {
            await axios.all(allApiCalls).then(axios.spread((...responses) => {
                response = responses;
            }))
        } catch (errors) {
            console.log(errors);
        }
        return response;
    }


    async getPlantList() {
        try {
            const response = await axios.get(this.plantListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnitList(plantId) {
        try {
            const response = await axios.get(this.unitListUrl + `/${plantId}/units/`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getUnitProductionLines(unitId) {
        try {
            const response = await axios.get(this.unitLinesUrl + `/${unitId}/lines/`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getAPM(plantId) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plant_id: plantId
                }
            };
            const response = await axios.get(this.apmUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async addLineLoad(newlineLoad) {
        try {
            const response = await axios.post(this.addLineLoadURL, newlineLoad, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }


    async editLineLoad(editlineLoad) {
        try {
            const response = await axios.post(this.editLineLoadURL, editlineLoad, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async transferLoad(model, transferLoadQty, transferToLine) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    prodLine: transferToLine,
                    qty: transferLoadQty
                }
            };
            const response = await axios.post(this.transferLoadURL, model, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async deleteLoadedLine(deletedLineId,modifiedOn) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    load_id: deletedLineId,
                    modified_on: modifiedOn
                }
            };
            const response = await axios.delete(this.deleteLineUrl, config)
            return response;
        }
        catch (error) {
            return error.response;
        }
    }

    async generateReport(plantId, unitId) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plantId: plantId,
                    unitId: unitId
                }
            };
            const response = await axios.post(this.generateReportUrl, null, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async downloadReport(reportId) {
        try {
            const config = {
                responseType: 'blob',
                headers: {
                    'Content-Type': 'application/octet-stream',
                    'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
                },
                headers: this.options.headers,
                params: {
                    reportId: reportId
                }
            };
            const response = await axios.get(this.downloadReportUrl, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async revertConfirmation(prodConfirmationId) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    prodConfirmationId: prodConfirmationId
                }
            };
            const response = await axios.post(this.revertConfUrl, null, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async generateValidationReport(plantId, unitId, startDate, endDate) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plant_id: plantId,
                    unit_id: unitId,
                    start_date: startDate,
                    end_date: endDate
                }
            };
            const response = await axios.get(this.generateValidationReportUrl, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }
}




export default LineLoadingService;