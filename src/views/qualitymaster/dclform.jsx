import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { GridComponent, ColumnsDirective, ColumnDirective, Filter, Inject, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, Reorder } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { sampleData } from '../../assets/data/inspection'
// import { sampleBase64pdf } from "../../../sampleBase64pdf";
class DclformComponent extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    this.countries = sampleData;
    this.state = {
      filterBox: true,
      divHidden: null,
      listfield: { dataSource: this.countries, id: 'id', text: 'name', parentID: 'pid', hasChildren: 'hasChild' },

    }
  }
  componentDidMount() {
  }
  filterBoxOpen = () => {
    this.setState({
      filterBox: !this.state.filterBox
    });

  }
  content1 = () => {
    return (<div>
      <Row>
        <Col>
          <span className='margin-top16'>
            <Button color="info" size="sm" onClick="">Yes / No / NA</Button>&nbsp;
               <Button className="brdblue" size="sm" onClick="">Pass / Fail</Button>
            <Button className="brdblue" size="sm" onClick="">Free Text</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }
  content2 = () => {
    return (<div>
        <Row>
            <Col sm='2'>    <DropDownListComponent id="Category" filterBarPlaceholder='Defect Category' placeholder="Select a category" popupHeight="220px" /></Col>
        <Col sm='3'>
        <RadioButtonComponent checked={true} label='100% Checking' name='' value=""></RadioButtonComponent>
        </Col>
        <Col sm='2'>
        <RadioButtonComponent checked={false} label='AQL' name='' value=""></RadioButtonComponent>
        </Col>
        <Col sm='2'>
        <RadioButtonComponent checked={false} label='4 Point' name='' value=""></RadioButtonComponent>
        </Col>
        <Col sm='3'>
        <DropDownListComponent id="Category" filterBarPlaceholder='Select Material Type' placeholder="Select Material Type" popupHeight="220px" />
        </Col>
       
      </Row>
     
      <Row>
        <Col>
          <span className='float-right margin-top16'>
            <Button color="primary" size="sm" onClick="">Add Questions</Button>&nbsp;
               <Button className="btncancel" size="sm" onClick="">Cancel</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }
  content3 = () => {
    return (<div>
      <Row>
        <Col>
          <span className='margin-top16'>
            <Button color="info" size="sm" onClick="">Yes / No / NA</Button>&nbsp;
               <Button className="brdblue" size="sm" onClick="">Pass / Fail</Button>
            <Button className="brdblue" size="sm" onClick="">Free Text</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }

  render() {
    return (
      <div className="container-fluid-inner">
           <div className='margin-top-16'>
          <Row>
            <Col sm='3'>
            
            </Col>
            <Col sm='9'>
              <AccordionComponent>
                <AccordionItemsDirective>
                  <AccordionItemDirective header='General Information' expanded={false} content={this.content1} />
                  <AccordionItemDirective header='Technical Verification : DCL Forms' content={this.content2} />
                  <AccordionItemDirective header='Does the product size meet as per approved sample/technical specification.' content={this.content3} />
                </AccordionItemsDirective>
              </AccordionComponent>
            </Col>
          </Row>          
        </div>
      </div>

    );
  }
}

export default DclformComponent;
