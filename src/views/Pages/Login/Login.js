import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation, Trans } from "react-i18next";
import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logo from '../../../assets/img/brand/logo.png';
import engicon from '../../../assets/img/eng-icon.png';
import hindiicon from '../../../assets/img/hindi-icon.png';
import LoginModel from './LoginModel';
import LoginService from './LoginService';
import axios from 'axios';
import errorMessages from '../../errorHandler/errorMessages';

//import ResetPassword from '../ResetPassword/ResetPassword'
class Login extends Component {

  version = "/api/v1"
  baseUrl = process.env.REACT_APP_BASEURL + this.version;
  leftMenuUrl = this.baseUrl + `/auth/left_menu`;


  constructor(props) {
    super(props);
    this.loginService = new LoginService();
    this.state = {
      opened: false,
      model: new LoginModel('', '')
    }
  }


  async getLoginToken(data) {
    const responseData = (await this.loginService.login(data))
    return responseData;
  }
  async sendmailForReset(data) {
    const responseData = (await this.loginService.sendingmail(data))
    return responseData;
  }

  async sendmail(e) {
    let that=this;
    const { t, i18n } = that.props;
    this.inputData = {
      "usernameOrEmail": this.state.model.userName.trim(),
    }
    if ((this.inputData.usernameOrEmail != undefined) && (this.inputData.usernameOrEmail != '')) {
      const response = await this.sendmailForReset(this.inputData)
      if ((response != undefined) && (response.status == "success")) {
        alert(response.message);
        this.setState({
          model: new LoginModel('', '')
        })
      } else {
        alert(t(errorMessages[response.data.errorCode]));
      }
    }
  }

  async logIn(e) {

    this.inputData = {
      "usernameOrEmail": this.state.model.userName.trim(),
      "password": this.state.model.password.trim()
    }
    if (((this.inputData.usernameOrEmail != undefined) && ((this.inputData.usernameOrEmail != ''))) && ((this.inputData.password != undefined) && ((this.inputData.password != '')))) {
      const response = await this.getLoginToken(this.inputData)

      if ((response != undefined) && (response.status == "success")) {
        localStorage.setItem("ACCESS_TOKEN", response.accessToken);
        localStorage.setItem("USER_NAME", this.inputData.usernameOrEmail);
        sessionStorage.setItem("headerToken", "Bearer " + response.accessToken);
        this.data = await this.getLeftMenu();
        localStorage.setItem("NAV", JSON.stringify(this.data));
        //   e.preventDefault()
        // this.props.history.nav=await this.getLeftMenu();
        if (this.data != undefined) {
          this.props.history.push('/home')

        }
        else {
          this.props.history.push('/Noaccess')
        }
      }
      else if ((response != undefined) && (response.status == "fail")) {
        alert(response.message)
      }
      else {
        alert("Please check your username and password")
        // code to handle error
      }

    }

  }

  async getLeftMenu() {
    this.options = {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
      }
    };
    try {
      const response = await axios.get(this.leftMenuUrl, this.options);
      console.log(response.data.data[0])
      return response.data.data[0];
      //  console.log(this.values[0]);
    }
    catch (error) {
      console.log(error);
      // alert("error")
    }
  }

  handleChange = (event) => {
    let that = this;
    that.setState({ model: { ...that.state.model, [event.target.id]: event.target.value } })
  }

  handleKeyPress = (event) => {
    if ((event.key == 'Enter') && ((this.state.model.userName.trim() != undefined) && (this.state.model.userName != '')) && ((this.state.model.password.trim() != undefined) && (this.state.model.password != ''))) {
      this.logIn();
    }
  }
  getComponent = (event) => {
    this.setState(prevState => ({
      opened: !prevState.opened
    }));
    console.log(this.state.opened);
  }

  render() {
    const { t, i18n } = this.props;

    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    return (
      <div className="app flex-row align-items-center loginbg">
        <Container hidden={this.state.opened}>
          <Row className="justify-content-center">
            <Col md="12" lg="8">
              <div className="mb-1">
                <a className="btn-icon" onClick={() => changeLanguage("de")}><img src={engicon} /></a>
                <a className="btn-icon" onClick={() => changeLanguage("en")} ><img src={hindiicon} /></a>
              </div>
              <CardGroup>
                <Card className="text-white bg-primary py-5" style={{ width: '44%' }}>

                  <CardBody className="text-center">
                    <div>
                      <img src={logo} />
                      <p className="text-muted">
                      </p>
                      <h2>MOEP</h2>
                      <h2>Line Management System</h2>
                    </div>
                  </CardBody>
                </Card>
                <Card className="p-4 login-mobile">
                  <CardBody>
                    <Form>  <img src={logo} className="d-lg-none mb-3" />
                      {/* <button onClick={() => changeLanguage("de")}>English</button>
                    <button onClick={() => changeLanguage("en")}>Hindi</button> */}
                      <h2>{t("cpm.login")}</h2>
                      {/* <h1>Login</h1> */}
                      {/* <p className="text-muted">Sign In to your account</p> */}<p> </p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder={t("cpm.login.username")} onKeyPress={this.handleKeyPress} autoComplete="off" id="userName" value={this.state.model.userName} onChange={this.handleChange} />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" onKeyPress={this.handleKeyPress} placeholder={t("cpm.login.password")} id="password" autoComplete="off" value={this.state.model.password} onChange={this.handleChange} />
                      </InputGroup>
                      {/* <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <input type="text" name="title" value={this.state.title} 
    onChange={this.handleChange.bind(this)}/>
                      </InputGroup> */}
                      <Row>
                        <Col xs="6">
                          <Button color="primary" id="login" onClick={e => this.logIn(e)}>{t("cpm.login")}</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          {/* <Button color="link" className="px-0">Forgot password?</Button>   */}
                          <Button className="link-btn" color="link" onClick={this.getComponent} label="Action">Forgot password?</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>

              </CardGroup>
            </Col>
          </Row>
        </Container>
        <Container hidden={!this.state.opened}>
          <Row className="justify-content-center">
            <Col md="8">
              <div className="mb-1">
                <a className="btn-icon" onClick={() => changeLanguage("de")}><img src={engicon} /></a>
                <a className="btn-icon" onClick={() => changeLanguage("en")} ><img src={hindiicon} /></a>
              </div>
              <CardGroup>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>

                  <CardBody className="text-center">
                    <div>
                      <img src={logo} />
                      <p className="text-muted">
                      </p>
                      <h2>MOEP</h2>
                      <h2>Line Management System</h2>
                    </div>
                  </CardBody>
                </Card>
                <Card className="p-4 login-mobile">
                  <CardBody>
                    <Form>
                      <img src={logo} className="d-lg-none mb-3" />
                      <h2>{t("cpm.forgotheading")}</h2>
                      <InputGroup className="mt-3 mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" placeholder={t("cpm.login.username")} onKeyPress={this.handleKeyPress} autoComplete="off" id="userName" value={this.state.model.userName} onChange={this.handleChange} />
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" id="login" onClick={e => this.sendmail(e)}>{t("cpm.submit")}</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          <Button className="link-btn" color="link" onClick={this.getComponent} label="Action">Go Back to Login</Button>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>

              </CardGroup>
            </Col>
          </Row>
          {/* <ResetPassword></ResetPassword> */}
        </Container>
      </div>

    );
  }
}
export default withTranslation("translations")(Login);
// export default Login;
