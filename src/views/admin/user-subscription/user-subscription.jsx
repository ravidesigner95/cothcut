import React from 'react';
import { ColumnDirective, ColumnsDirective, CommandColumn, GridComponent, Group, Inject, Page, Sort, Resize, Selection } from '@syncfusion/ej2-react-grids';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { PermissionModel } from './SubscriptionModel';
import { PermissionServices } from './SubscriptionServices';
import { SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';


class UserSubscription extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.permissionService = new PermissionServices();
        this.state = {
            userSubscriptionList: [],
            toasterPosition: { X: 'Right', Y: 'Bottom' },
            toasterMessage: [
                { title: 'Success!', content: 'Updated successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'Something went wrong !!', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
            ],
        };
    }

    componentDidMount() {
        this.getUserSubscription();
    }

    async getUserSubscription() {
        global.showLoader();
        let response = (await this.permissionService.getUserSubscription())
        if ((response != undefined) && (response.data.status == "success")) {
            let userSubscriptionList = [];
            response.data.data.forEach(element => {
                userSubscriptionList.push(new PermissionModel(element.id, element.userId, element.alertKey, element.userRole, element.status, element.createdOn, element.modifiedOn));
            });
            this.setState({ userSubscriptionList }, () => {
                global.hideLoader();
            })
        } else {
            this.toastObj.show(this.state.toasterMessage[1]);
            global.hideLoader();
        }
    }

    async onSave(that) {
        global.showLoader();
        let response = (await this.permissionService.saveUserSubscription(that.state.userSubscriptionList))
        if ((response != undefined) && (response.status == "success")) {
            this.toastObj.show(this.state.toasterMessage[0]);
            this.getUserSubscription();
        } else {
            this.toastObj.show(this.state.toasterMessage[1]);
            global.hideLoader();
        }
    }

    rowSelected = (args) => {
        let that = this;
        if (((args.target.className == "e-switch-handle") || (args.target.className == "e-switch-off") || (args.target.className == "e-switch-handle e-switch-active")) && (args.data != undefined)) {
            that.state.userSubscriptionList.map(element => {
                if (element.id == args.data.id)
                    element.status = !element.status;
            })
        }
    }

    statusTemplate(args) {
        return (<SwitchComponent id="toggle" className="switch" checked={args.status} ></SwitchComponent>);
    }

    render() {
        return (
            <div>
                <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
                <div className="container-fluid-inner">
                    <h1 className='pagesheading'>User Subscription</h1>
                    <div className='clearfix'></div>
                    <GridComponent allowGrouping={true} rowSelected={this.rowSelected}
                        allowTextWrap={true} gridLines='Both' selectionSettings={{ enableToggle: false }}
                        allowReordering={true} allowPaging={true} pageSettings={{ pageCount: 5, pageSizes: true }} allowSelection={true}
                        allowResizing={true} dataSource={this.state.userSubscriptionList} >
                        <ColumnsDirective>
                            <ColumnDirective field='userId' headerText='User Id' width='120'></ColumnDirective>
                            <ColumnDirective field='alertKey' headerText='Alert Key' width='150'></ColumnDirective>
                            <ColumnDirective field='userRole' headerText='User Role' width='130' />
                            <ColumnDirective field='status' headerText='Status' width='100' template={this.statusTemplate} />
                        </ColumnsDirective>
                        <Inject services={[Page, Group, Sort, Selection]} />
                    </GridComponent>
                    <div>
                        <Button color="primary" size="sm" id="save" className="float-left mr-3" onClick={()=>{this.onSave(this)}}>{"Save"}</Button>
                    </div>
                </div>
            </div>
        );
    }
}
export default UserSubscription;
