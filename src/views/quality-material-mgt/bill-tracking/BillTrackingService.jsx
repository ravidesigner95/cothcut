import axios from 'axios';
class BillTrackingService {
    constructor() { }
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    qualityServiceUrl = process.env.REACT_APP_BASEURL_DASHBOARD+'/qualityservice'+this.version;
    // qualityServiceUrl = 'http://localhost:8082'+this.version;
    btListUrl = this.qualityServiceUrl + `/bt/getBillTrackingInfo`;
    grnInvoiceUrl = this.qualityServiceUrl + `/bt/getInvoiceAndGrnDetail`;
    poInfoUrl = this.qualityServiceUrl + `/bt/getBtPoDetail`;
    poBtHistoryUrl = this.qualityServiceUrl + `/bt/getBtHistory`;
    documentsUrl=this.qualityServiceUrl + `/bt/getBtAllDocumnets`;
    btProcessHistoryUrl=this.qualityServiceUrl + `/bt/getBtProcessHistory`;
    btProcessStatusUrl=this.qualityServiceUrl + `/bt/getBtProcessLatestStatus`;
    resetActionUrl=this.qualityServiceUrl + `/bt/resetAction`;
    btReviewProcessUrl=this.qualityServiceUrl + `/bt/getBtReviewProcess`;
    saveBtHistoryUrl=this.qualityServiceUrl + `/bt/saveBTHistory`;
    uploadFileUrl =this.qualityServiceUrl +  "/bt/upload"
    downloadReportUrl = this.qualityServiceUrl + "/form/downloadFile";
    downloadfileUrl = this.qualityServiceUrl + "/bt/downloadPicture";
    btMiroUrl = this.qualityServiceUrl + "/bt/getMiroDetail";
    previousDebitNoteUrl=this.qualityServiceUrl + `/bt/getPreviouslyDebitNote`;
    createDebitNoteUrl=this.qualityServiceUrl + `/bt/createOrUpdateDebitNote`;
    debitNoteInfoUrl=this.qualityServiceUrl + `/bt/getDebitNoteInfo`;
    debitNoteDeleteUrl=this.qualityServiceUrl + `/bt/deleteDebitNoteByDbtNo`;
    specificDebitNoteDeleteUrl=this.qualityServiceUrl + `/bt/deleteDebitNote`;

    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    //---------------------------------------API-CALLS-----------------------------------------//

    async getBtList() {
        try {
            const response = await axios.get(this.btListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getGrnInvoiceList(btNumber) {
        try {
        const config = {
                headers: this.options.headers,
                params: {
                    btNumber: btNumber,
        
                }
            };
            return await axios.get(this.grnInvoiceUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getPoInfoList(btNumber) {
        try {
        const config = {
                headers: this.options.headers,
                params: {
                    btNumber: btNumber,
        
                }
            };
            return await axios.get(this.poInfoUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getBtHistory(btNumber) {
        try {
        const config = {
                headers: this.options.headers,
                params: {
                    btNumber: btNumber,
        
                }
            };
            return await axios.get(this.poBtHistoryUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getAllDocuments(btNumber) {
        try {
        const config = {
                headers: this.options.headers,
                params: {
                    btNumber: btNumber,
        
                }
            };
            return await axios.get(this.documentsUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getBtProcessHistory(btNumber,processId) {
        try {
        const config = {
                headers: this.options.headers,
                params: {
                    btNumber: btNumber,
                    processId: processId,
                }
            };
            return await axios.get(this.btProcessHistoryUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getBtReviewProcess(btNumber,processId) {
        try {
        const config = {
                headers: this.options.headers,
                params: {
                    btNumber: btNumber,
                    currentProcessId: processId,
                }
            };
            return await axios.get(this.btReviewProcessUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getBtProcessStatus(btNumber) {
        try {
        const config = {
                headers: this.options.headers,
                params: {
                    btNumber: btNumber,
                   
                }
            };
            return await axios.get(this.btProcessStatusUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async downloadReport(docId) {
        try {
            const response = await axios.get(this.downloadReportUrl + `/${docId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async downloadFile(docId) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    docId: docId,
                   
                }
            };
            return await axios.get(this.downloadfileUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async saveCommentsAndReview(processDto) {
        try {
            const response = await axios.post(this.resetActionUrl, processDto, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async saveBtHistory(btHistoryDto) {
        try {
            const response = await axios.post(this.saveBtHistoryUrl, btHistoryDto, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async uploadFile(files) {
        try {
            // const config = {
            //         headers: this.options.headers,
            //         params: {
            //             file: files,
                    
            //         }
            //     };
                return await axios.post(this.uploadFileUrl,files, this.options);
            }
            catch (error) {
                console.log(error);
                // alert("error")
            }
        }
        async getMiroDetail(btNumber) {
            try {
            const config = {
                    headers: this.options.headers,
                    params: {
                        btNumber: btNumber,
            
                    }
                };
                return await axios.get(this.btMiroUrl, config);
            }
            catch (error) {
                console.log(error);
                // alert("error")
            }
        }


        async getPreviousDebitNote(btNumber) {
            try {
            const config = {
                    headers: this.options.headers,
                    params: {
                        btNumber: btNumber,
                       
                    }
                };
                return await axios.get(this.previousDebitNoteUrl, config);
            }
            catch (error) {
                console.log(error);
                // alert("error")
            }
        }

        async getBtDebitNoteInfo(btNumber) {
            try {
            const config = {
                    headers: this.options.headers,
                    params: {
                        btNumber: btNumber,
                       
                    }
                };
                return await axios.get(this.debitNoteInfoUrl, config);
            }
            catch (error) {
                console.log(error);
                // alert("error")
            }
        }

        async createDebitNote(debitNoteList) {
            try {
                const response = await axios.post(this.createDebitNoteUrl, debitNoteList, this.options)
                return response;
            }
            catch (error) {
                console.log(error);
                return error.response;
                // alert("error")
            }
        }

        async deleteDebitNote(debitNoteNumber) {
            try {
            const config = {
                    headers: this.options.headers,
                    params: {
                        debitNoteNumber: debitNoteNumber,
                       
                    }
                };
                return await axios.delete(this.debitNoteDeleteUrl, config);
            }
            catch (error) {
                console.log(error);
                // alert("error")
            }
        }

        async deleteDebitNoteById(debitNoteId) {
            try {
            const config = {
                    headers: this.options.headers,
                    params: {
                        debitNoteId: debitNoteId,
                       
                    }
                };
                return await axios.delete(this.specificDebitNoteDeleteUrl, config);
            }
            catch (error) {
                console.log(error);
                // alert("error")
            }
        }
    
}
export default BillTrackingService;