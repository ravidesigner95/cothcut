class UserProductionModel
{
    constructor(id,name,isChecked)
    {
        this.id=id;
        this.name=name;
        this.isChecked=isChecked;
    }
    id=null;
    name=""; 
    isChecked=false;
}
export default UserProductionModel;