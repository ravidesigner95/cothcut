import axios from 'axios';
class UserProfileService {
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    rolesUrl = this.baseUrl + `/roles`;
    groupUrl = this.baseUrl + `/group`;
    saveGroupUrl = this.baseUrl + `/group`;
    deleteGroupUrl = this.baseUrl + `/group`;
    permissionURL = this.baseUrl + `/role/permission`;

    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }

    };

    async deleteGroup(groupId) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    id: groupId
                }
            };
            const response = await axios.delete(this.deleteGroupUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async saveGroup(data) {
        try {
            const response = await axios.post(this.saveGroupUrl, data, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }


    async getRolesList() {
        try {
            const response = await axios.get(this.rolesUrl, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }

    async getGroupList() {
        try {
            const response = await axios.get(this.groupUrl, this.options)
            return response;
        }
        catch (error) {
            alert("error")
        }
    }

}
export default UserProfileService;