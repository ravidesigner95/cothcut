import axios from 'axios';

class LinePlanningService {
    constructor() {
    }
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;

    plantListUrl = this.baseUrl + `/master/plant`;
    unitListUrl = this.plantListUrl;
    unitLinesUrl = this.baseUrl + `/master`;
    planningDetails_Get_Post_Url = this.baseUrl + `/unit/line/plan`;
    categoryUrl = this.baseUrl + `/master/category`;
    selectedPlanDataUrl = this.baseUrl + `/unit/line/doplan`;
    saidNoUrl = this.baseUrl + `/unit/plantId/saids`;
    saidDetailsUrl = this.baseUrl + `/said/lineitems`;
    eventPlannedDataUrl = this.baseUrl + `/unit/line/period`;
    apmUrl = this.baseUrl + `/unit/apm`;
    plannedQty = this.baseUrl + `/planned/qty`;
    summaryDataPlantsUrl = this.baseUrl + `/unit/line/plan/summary_plant`;
    summaryDataCategoryUrl = this.baseUrl + `/unit/line/plan/summary_category`;
    holidayListUrl = this.baseUrl + `/unit/holidays`;
    availableMachinesUrl = this.baseUrl + `/unit/availableMachines`;
    confgkeyUrl = this.baseUrl + `/appConfg/appCnfgkey`;
    estimatedWorkingDaysUrl =this.baseUrl + `/unit/getEstimatedWorkDays`;
    orderListUrl=this.baseUrl+`/line/schedule/planning_status`;
    planningStatusUrl=this.baseUrl+`/line/schedule/generatePlanningStatusReport`;
    downloadReportUrl = this.baseUrl + `/hr/employee/downloadReportOfLine`;

    //---------------------------------------------------------------API CALLS---------------------------------------------------------//
    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    async getConfgKey() {
        try {
            const response = await axios.get(this.confgkeyUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getEstimatedWorkingDays(buyerPo,lineItem,noOfMachines,prodLineId,qty,saidNo,sam,startDate){
        let obj={
            buyerPo: buyerPo,
            lineItem: lineItem,
            noOfMachines: noOfMachines,
            prodLineId: prodLineId,
            qty:qty,
            saidNo: saidNo,
            sam: sam,
            startDate: startDate     
        }
        try {
            const response = await axios.post(this.estimatedWorkingDaysUrl,obj, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getDaywiseNoOfAllocatedMachines(prodLineId) {
        try {
            const response = await axios.get(this.availableMachinesUrl + `/${prodLineId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getHolidaysList() {
        const config = {
            headers: this.options.headers,
            params: {
                year: new Date().getFullYear()
            }
        };
        try {
            const response = await axios.get(this.holidayListUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getSummeryViewData(plantList, isCategoryURL, viewType, targetYear) {
        let url = isCategoryURL ? this.summaryDataCategoryUrl : this.summaryDataPlantsUrl;
        const config = {
            headers: this.options.headers,
            params: {
                type: viewType,
                year: targetYear
            }
        };
        try {
            const response = await axios.post(url, plantList, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getPlantList() {
        try {
            const response = await axios.get(this.plantListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }  


    async getAllOrders() {
        try {
            const response = await axios.get(this.orderListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnitList(plantId) {
        try {
            const response = await axios.get(this.unitListUrl + `/${plantId}/units/`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnitProductionLines(unitId) {
        try {
            const response = await axios.get(this.unitLinesUrl + `/${unitId}/lines/`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getCategory() {
        try {
            const response = await axios.get(this.categoryUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getSaidNo(plantId,unitName) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plantId: plantId,
                    unit:unitName
                }
            };
            const response = await axios.get(this.saidNoUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getPlannedQty(saidNo, lineItem) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    said_no: saidNo,
                    line_item: lineItem
                }
            };
            const response = await axios.get(this.plannedQty, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getAPM() {
        try {
            const response = await axios.get(this.apmUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getSaidDetails(saidId, plantId,unitName) {
        try {
            const response = await axios.get(this.saidDetailsUrl + `/${saidId}/${plantId}/${unitName}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getLinePlanningDetails(lineId, unitId, targetYear) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    unit_id: unitId,
                    prod_line_id: lineId,
                    target_year: targetYear
                }
            };
            return await axios.get(this.planningDetails_Get_Post_Url, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async savePlannedData(plannedDataModel) {      // **TO BE FIXED
        try {
            const response = await axios.post(this.selectedPlanDataUrl, plannedDataModel, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async getSelectedMonthPlannedEvents(lineId, month, year) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    prod_line_id: lineId,
                    target_month: "MARCH",// From march - march (1 year)
                    target_year: year//2019
                }
            };
            const response = await axios.get(this.selectedPlanDataUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getClickedEventPlannedData(eventModel, lineId) {
        try {
            let data = JSON.parse(JSON.stringify(eventModel));
            const config = {
                headers: this.options.headers,
                params: {
                    said_id: data.oldSaidId,
                    line_item: data.oldLineItem,
                    prod_line_id: lineId,
                    start_date_time: data.oldStartDateTime,
                    end_date_time: data.oldEndDateTime,
                    category_color: data.categoryColor,
                }
            };
            const response = await axios.get(this.eventPlannedDataUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async generatePlanningStatusReport(plantId, unitId, startDate, endDate) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plant_id: plantId,
                    unit_id: unitId,
                    start_date: startDate,
                    end_date: endDate
                }
            };
            const response = await axios.get(this.planningStatusUrl, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }
    
    // async downloadReport(reportId) {
    //     try {
    //         const config = {
    //             responseType: 'blob',
    //             headers: {
    //                 'Content-Type': 'application/octet-stream',
    //                 'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
    //             },
    //             headers: this.options.headers,
    //             params: {
    //                 reportId: reportId
    //             }
    //         };
    //         const response = await axios.get(this.downloadReportUrl, config)
    //         return response;
    //     }
    //     catch (error) {
    //         console.log(error);
    //         return error.response;
    //         // alert("error")
    //     }
    // }
    async downloadReport(reportId) {
        try {
            const config = {
                responseType: 'blob',
                headers: {
                    'Content-Type': 'application/octet-stream',
                    'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
                },
                headers: this.options.headers,
                params: {
                    reportId: reportId
                }
            };
            const response = await axios.get(this.downloadReportUrl, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }
}
export default LinePlanningService;
