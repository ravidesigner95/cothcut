import PlanningDetailsModel from './PlanningDetailsModel';
export class SchedulerDetailsModel {
    constructor(id, monthId, categoryColor, description, startDateTime, endDateTime,oldStartDateTime,oldEndDateTime, saidId, oldSaidId, lineItem,oldLineItem,prodLineId,plannedInfoId,plannedData,isNewEvent) {
        this.id = id;
        this.monthId = monthId;
        this.categoryColor = categoryColor;
        this.description = description;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.oldStartDateTime=oldStartDateTime;
        this.oldEndDateTime=oldEndDateTime;
        this.saidId = saidId;
        this.lineItem = lineItem;
        this.plannedData = plannedData;
        this.oldSaidId = oldSaidId;
        this.oldLineItem=oldLineItem;
        this.plannedInfoId=plannedInfoId;
        this.prodLineId=prodLineId; 
        this.isNewEvent=isNewEvent;
    }
    id = null;
    monthId = null;
    categoryColor = "";
    description = "";
    startDateTime = null;
    endDateTime = null;
    oldStartDateTime=null;
    oldEndDateTime=null;
    saidId = null;
    oldSaidId = null;
    lineItem = null;
    oldLineItem=null;
    prodLineId=null;
    plannedInfoId=null;
    plannedData = {}
    isNewEvent=false;
}

export default SchedulerDetailsModel;