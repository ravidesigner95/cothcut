export class AqlModel {
    constructor(id, name,isActive,isTab, qualityMasterAqlData) {
        this.id = id;
        this.name = name;
        this.isActive=isActive;
        this.isTab = isTab
        this.qualityMasterAqlData = qualityMasterAqlData;
        
    }
    id = null;
    name = null;
    isActive=null;
    isTab=null;
    qualityMasterAqlData= [];

}
export default AqlModel; 

export class QualityMasterAqlDataModel {
    constructor(id,deid,orderQuantityFrom, orderQuantityTo, sampleSize, criticalAccept, criticalReject, majorAccept, majorReject, minorAccept, minorReject) {
        this.id=id;
        this.deid=deid;
        this.orderQuantityFrom = orderQuantityFrom;
        this.orderQuantityTo = orderQuantityTo;
        this.sampleSize = sampleSize;
        this.criticalAccept = criticalAccept;
        this.criticalReject = criticalReject;
        this.majorAccept = majorAccept;
        this.majorReject = majorReject;
        this.minorAccept = minorAccept;
        this.minorReject = minorReject;
    }
    id=null;
    deid=null;
    orderQuantityFrom = null;
    orderQuantityTo = null;
    sampleSize = null;
    criticalAccept = null;
    criticalReject = null;
    majorAccept = null;
    majorReject = null;
    minorAccept = null;
    minorReject = null;

}
