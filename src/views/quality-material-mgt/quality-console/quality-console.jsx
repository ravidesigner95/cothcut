import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { GridComponent, ColumnChooser, ColumnsDirective, ColumnDirective, Filter, Inject, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, Reorder } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent, DropDownList, MultiSelect } from '@syncfusion/ej2-react-dropdowns';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TabComponent, TabItemDirective, MenuComponent, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import QualityConsoleService from './QualityConsoleService';
import { InspectionAssigneeModel, InspectionModel, InspectionCommonsModel, UsersModel, QualityInspType } from './qualityConsoleModels';
import { QualityTableAssigneeModel } from './QualityTableAssigneeModel';
import { assigneeBoxGridData, assignTableGridData } from '../../../assets/data/purchaseOrder';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import SinglePagePDFViewer from "../../../components/pdf/single-page";
import QualityTableOrMachineModel from './QualityTableOrMachineModel';
import { DropDownButtonComponent } from '@syncfusion/ej2-react-splitbuttons';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import LineLoadingModel from '../../line/line-loading/LineLoadingModel';
import CountDownClockComponent from '../../line/line-loading/CountDownClockComponent';
import InspectionService from '../../inspection/InspectionService';
import { AppConfig } from '../../../appConfig/appConfig';
import { elementType } from 'prop-types';

// import { sampleBase64pdf } from "../../../sampleBase64pdf";
class QualityConsole extends React.PureComponent {
  constructor(props) {
    super()
    this.toolbarOptions = ['Add', 'Edit', 'Update', 'Delete', 'Cancel'];
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Normal' };
    this.qualityControlService = new QualityConsoleService();
    this.inspectionService = new InspectionService();
    /*     this.viewIcon = [
      {
        type: 'View', buttonOption: {
          iconCss: 'e-icons',
          // content: 'Update Store',
          cssClass: 'e-flat e-btn pdf-view'
        }
      },
      {
        type: 'Email', buttonOption: {
          iconCss: 'e-icons',
          // content: 'Update Store',
          cssClass: 'e-flat e-btn email-view'
        }
      },
      {
        type: 'Copy', buttonOption: {
          iconCss: 'e-icons',
          // content: 'Update Store',
          cssClass: 'e-flat e-btn copy-view'
        }
      }
    ]; */

    this.state = {
      FilterSettingsModel: { type: 'Menu' },
      IFilter: { type: 'CheckBox' },
      usersModel: new UsersModel(null, null, null, null, null, null),
      assigneeModel: new InspectionAssigneeModel(null, null, null, null, null, null, null, null, null, null, null, null),
      inspectionModel: new InspectionModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, {}),
      commonsModel: new InspectionCommonsModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
        null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null),
      lineLoadingModel: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0),

      qtyrules: { required: true, number: true },
      validationrules: { required: true },

      select: {
        persistSelection: false,
        type: "Single",
        checkboxOnly: true
      },
      format: { type: 'dateTime', format: 'dd/MM/yyyy HH:mm:ss' },
      inspectionDetails: [],
      rolesList: [],
      usersList: [],
      plants: [],
      units: [],
      selectedUserId: null,
      selectedRole: null,
      filterBox: true,
      showDialogBox: false,
      manualInspectionDialog: false,
      showDialogBoxHeading: null,
      divHidden: null,
      assigneeBoxGrid: assigneeBoxGridData,
      assignTableGrid: assignTableGridData,
      selectedTickets: [],
      selectedInspections: [],
      selectedInsType: null,
      filteredPlants: [],
      filteredUnits: [],
      filteredTablesOrLines: [],
      filteredInspNums: [],
      filteredInspStatuses: [],
      filteredOrders: [],
      filteredDates: [],
      filteredInspResults: [],
      filteredBuyerPos: [],
      filteredSaids: [],
      filteredLineItems: [],
      filteredVendors: [],
      filteredInspectionLotNums: [],
      filteredDescriptions: [],
      filteredBuyers: [],
      selectedPlants: [],
      selectedUnits: [],
      selectedTablesOrLines: [],
      selectedInspNums: [],
      selectedInspStatuses: [],
      selectedOrders: [],
      selectedDates: [],
      selectedInspResults: [],
      selectedSaids: [],
      selectedLineItems: [],
      selectedVendors: [],
      selectedInspectionLotNums: [],
      selectedDescriptions: [],
      selectedBuyers: [],
      tableAssignData: [],
      filteredInspectionForAssignTable: [],
      thanList:[],
      pdfInfo: null,
      isCheckBoxSelected: false,
      pdf: false,
      selectedRowQaGrid: [],
      pdfViewDialog: false,
      roleRadio: false,
      forcedRadio: false,
      notStartedRadio: false,
      windowsHeight: window.innerHeight - 300 + 'px',
      selectedPlantName: null,
      selectedUnitName: null,
      inspEndDate: new Date(),
      inspStartDate: new Date(new Date().setMonth(new Date().getMonth() - 2)),
      selectedStatus: ['New', 'Pending'],
      manualPlant: null,
      manualUnit: null,
      manualSaids: [],
      manualLineItems: [],
      selectedManualSaid: null,
      selectedManualLineItem: null,
      loadList: [],
      manualInspList: [],
      selectedManualInsp: null,
      clockCount: null,
      isClockCountRunning: false,
      selectedLoad: null,
      productionProcessList: []
    }

    this.columnChooser = ['Search','ColumnChooser'];

    this.items = [
      {
        iconCss: 'e-sb-icons',
        text: 'View'
      },
      {
        iconCss: 'e-icons',
        text: 'Email'
      },
      {
        iconCss: 'e-icons',
        text: 'Copy'
      },
      {
        iconCss: 'e-icons',
        text: 'Sap Conf'
      },
    ];

    this.statusData = ['New', 'Pending', 'Completed'];

  }

  actionTemplate = (args) => {
    return (<div><DropDownButtonComponent items={this.items} select={(e) => { this.onCommandClick(args, e) }} change cssClass='e-caret-hide'><i class="fa fa-bars"></i></DropDownButtonComponent></div>);
  }

  componentDidMount() {
    // this.getInspectionList()
    this.getRoles()
    this.getPlantList()
    // this.getManualInspList()
    this.handleCountdown()
    this.getProductionProcesses()
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.isClockCountRunning !== prevState.isClockCountRunning) {
      switch (this.state.isClockCountRunning) {
        case true:
          this.handleStartClockCountDown();
      }
    }
  }


  async getPlantList() {

    let response = (await this.qualityControlService.getPlantList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ plants: response.data.data }, () => {
      })
  }

  async getUnitList(plantId) {
    let response = (await this.qualityControlService.getUnitList(plantId))
    if ((response != undefined) && (response.data.status == "success")) {
      this.setState({ units: response.data.data, selectedUnitName: null })
    }
  }

  async getUnitListForManual(plantId) {
    let response = (await this.qualityControlService.getUnitList(plantId))
    if ((response != undefined) && (response.data.status == "success")) {
      this.setState({ units: response.data.data, manualUnit: null })
    }
  }

  async getManualInspList(process) {
    let inspList = [];
    let response = (await this.qualityControlService.getManualInspList())
    if ((response != undefined) && (response.status == 200))
      response.data.forEach(element => {
        if (element.inspTypeInfo.length > 0 && element.inspTypeInfo[0].value === process) {
          inspList.push(new QualityInspType(element.id, element.name))
        }
      });
    this.setState({ manualInspList: inspList }, () => {
    })
  }

  async getProductionProcesses() {
    let response = (await this.inspectionService.getProductionProcesses())
    if ((response != undefined) && (response.status == 200))
      this.setState({ productionProcessList: response.data }, () => {
      })
  }



  async getInspectionList(plantId, unitId, startDate, endDate, status) {
    global.showLoader();
    let inspectionsList = [];
    let response = (await this.qualityControlService.getInspectionList(plantId, unitId, startDate, endDate, status))
    global.hideLoader();
    if ((response != undefined) && (response.data.status == "success")) {
      response.data.data.map(inspObj => {
        let assigneeList = [];
        let commonsData = new InspectionCommonsModel(inspObj.inspectionCommonsDTO.id, inspObj.inspectionCommonsDTO.said_id,
          inspObj.inspectionCommonsDTO.line_item, inspObj.inspectionCommonsDTO.qa_name, inspObj.inspectionCommonsDTO.qa_id, inspObj.inspectionCommonsDTO.mr_name, inspObj.inspectionCommonsDTO.mr_id,
          inspObj.inspectionCommonsDTO.apm_name, inspObj.inspectionCommonsDTO.apm_id, inspObj.inspectionCommonsDTO.buyer_name, inspObj.inspectionCommonsDTO.buyer_id, inspObj.inspectionCommonsDTO.vendor_name,
          inspObj.inspectionCommonsDTO.vendor_id, inspObj.inspectionCommonsDTO.plant_name, inspObj.inspectionCommonsDTO.plant_id, inspObj.inspectionCommonsDTO.unit_name,
          inspObj.inspectionCommonsDTO.unit_id, inspObj.inspectionCommonsDTO.prod_ord_nr, inspObj.inspectionCommonsDTO.purch_ord_nr, inspObj.inspectionCommonsDTO.material_kd,
          inspObj.inspectionCommonsDTO.material_desc, inspObj.inspectionCommonsDTO.process_name, inspObj.inspectionCommonsDTO.product_type,
          inspObj.inspectionCommonsDTO.request_handler_name, inspObj.inspectionCommonsDTO.request_handler_id, inspObj.inspectionCommonsDTO.date, inspObj.inspectionCommonsDTO.uom, inspObj.inspectionCommonsDTO.fm_id,
          inspObj.inspectionCommonsDTO.fm_name, inspObj.inspectionCommonsDTO.grn_no, inspObj.inspectionCommonsDTO.grn_date, inspObj.inspectionCommonsDTO.said_soid_qty, inspObj.inspectionCommonsDTO.grn_qty,
          inspObj.inspectionCommonsDTO.bill_no, inspObj.inspectionCommonsDTO.bill_date, inspObj.inspectionCommonsDTO.report_no, inspObj.inspectionCommonsDTO.pe_name, inspObj.inspectionCommonsDTO.lot_no,
          inspObj.inspectionCommonsDTO.product_type_group);

        inspObj.inspectionsAssigneeDTOS.forEach(element => {
          assigneeList.push(new InspectionAssigneeModel(element.id, element.inspectionNo, element.role, element.userId, element.userName, element.sequence, element.createdOn,
            element.startTime, element.endTime, element.status, element.result, element.isNecessary))
        });
        let tableNames = inspObj.inspectionTableAssigneeDtos.map(item => item.tableOrMachineDesc).join(', ');
        inspectionsList.push(new InspectionModel(inspObj.id, inspObj.ticketNo, inspObj.date, inspObj.stringDate, inspObj.name, inspObj.keyName, inspObj.reminderRule,
          inspObj.expirationRule, inspObj.createdOn, inspObj.modifiedOn, inspObj.requestHandlerId, inspObj.requestHandlerName, inspObj.manuallyGenerated, inspObj.formId,
          inspObj.versionNo, inspObj.isActive, inspObj.status, inspObj.docId, tableNames,
          commonsData.plantName, commonsData.unitName, commonsData.prodOrder != "" ? commonsData.prodOrder : commonsData.purchaseOrder, commonsData.vendorName, inspObj.result, commonsData.saidId,
          commonsData.lineItem, commonsData.lot_no, this.dateConverter(inspObj.startTime), this.dateConverter(inspObj.endTime),
          commonsData.grn_no, inspObj.type, commonsData.materialDesc, commonsData.mrName, commonsData.buyerName, commonsData.grn_date, commonsData.grn_qty, inspObj.passQty, inspObj.failQty,
          assigneeList, commonsData, inspObj.inspectionTableAssigneeDtos))
      })

      let uniquePlants = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.plantName === value.plantName) && (value.plantName != null) && (value.plantName != "")) === index);
      let uniqueUnits = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.unitName === value.unitName) && (value.unitName != null) && (value.unitName != "")) === index);
      let uniqueTablesOrLines = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.tableOrLineName === value.tableOrLineName) && (value.tableOrLineName != null) && (value.tableOrLineName != "")) === index);
      let uniqueNumbers = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.ticketNo === value.ticketNo) && (value.ticketNo != null) && (value.ticketNo != "")) === index);
      let uniqueStatuses = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.inspStatus === value.inspStatus) && (value.inspStatus != null) && (value.inspStatus != "")) === index);
      let uniqueDates = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.date === value.date) && (value.date != null) && (value.date != "")) === index);
      let uniqueInspResults = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.inspResult === value.inspResult) && (value.inspResult != null) && (value.inspResult != "")) === index);
      let uniqueOrders = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.order === value.order) && (value.order != null) && (value.order != "")) === index);
      let uniqueSaids = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.said === value.said) && (value.said != null) && (value.said != "")) === index);
      let uniqueLineItems = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.lineItem === value.lineItem) && (value.lineItem != null) && (value.lineItem != "")) === index);
      let uniqueVendors = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.vendorName === value.vendorName) && (value.vendorName != null) && (value.vendorName != "")) === index);
      let uniqueInspectionLotNums = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.lotNum === value.lotNum) && (value.lotNum != null) && (value.lotNum != "")) === index);
      let uniqueDescriptions = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.name === value.name) && (value.name != null) && (value.name != "")) === index);
      let uniqueBuyers = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.buyerName === value.buyerName) && (value.buyerName != null) && (value.buyerName != "")) === index);
      // let uniqueQAs = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.alertCode === value.alertCode)) === index);
      // let uniqueQCs = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.alertCode === value.alertCode)) === index);
      // let uniqueLabAssistants = inspectionsList.filter((value, index, array) => array.findIndex(t => (t.alertCode === value.alertCode)) === index);
      this.setState({
        inspectionDetails: inspectionsList, filteredPlants: uniquePlants, filteredUnits: uniqueUnits, filteredInspNums: uniqueNumbers, filteredInspStatuses: uniqueStatuses,
        filteredOrders: uniqueOrders, filteredDates: uniqueDates, filteredInspResults: uniqueInspResults, filteredSaids: uniqueSaids,
        filteredLineItems: uniqueLineItems, filteredVendors: uniqueVendors, filteredTablesOrLines: uniqueTablesOrLines, filteredInspectionLotNums: uniqueInspectionLotNums,
        filteredDescriptions: uniqueDescriptions, filteredBuyers: uniqueBuyers, selectedTickets: [], selectedInspections: [], isCheckBoxSelected: false
      })
    }
  }

  async getRoles() {
    let roles = [];
    let response = (await this.qualityControlService.getRoles())
    if ((response != undefined) && (response.data.status == "success")) {
      response.data.data.map(roleObj => {
        roles.push(roleObj.name);
      })
    }
    this.setState({ rolesList: roles })
  }

  async getUsers(roleName) {
    global.showLoader();
    let response = (await this.qualityControlService.getUsers(roleName))
    if ((response != undefined)) {
      let usersList = [];
      response.data.forEach(element => {
        usersList.push({ fullName: element.firstName + " " + element.lastName, userId: element.id })
      })
      this.setState({ usersList }, () => {
        global.hideLoader();
      })
    }
  }

  assignInspections = async () => {
    global.showLoader();
    let that = this;
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = that.state;
    if (that.state.selectedRole != null && that.state.selectedUserId != null) {
      let response = (await that.qualityControlService.addAssignee(that.state.selectedTickets, that.state.selectedRole, that.state.selectedUserId));
      if ((response != undefined) && (response.data.status == "success")) {
        that.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-info toast-icons' });
        global.hideLoader()
      }
      else if ((response != undefined) && (response.data.status == "unsuccess")) {
        // this.setState({ isDisabledSaveButton: false })
        that.toastObj.show({ title: 'Information!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else if ((response != undefined) && (response.data.status == "fail")) {
        // this.setState({ isDisabledSaveButton: false })
        that.toastObj.show({ title: 'Warning!', content: response.data.message, cssClass: 'e-toast-warning', icon: 'e-info toast-icons' });
        global.hideLoader()
      }
      that.closeDialog()
      that.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
      global.hideLoader()
    }
    else {
      that.toastObj.show({ title: 'Information!', content: "Please Select Inputs!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      global.hideLoader()
    }
  }

  reAssignInspections = async () => {
    let that = this;
    global.showLoader();
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = that.state;
    if (that.state.selectedRole != null && that.state.selectedUserId != null) {
      let response = (await this.qualityControlService.reAssign(that.state.selectedTickets, that.state.selectedRole, that.state.selectedUserId));
      if ((response != undefined) && (response.data.status == "success")) {
        that.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-info toast-icons' });
        global.hideLoader()
      }
      else if ((response != undefined) && (response.data.status == "unsuccess")) {
        // this.setState({ isDisabledSaveButton: false })
        that.toastObj.show({ title: 'Information!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else if ((response != undefined) && (response.data.status == "fail")) {
        // this.setState({ isDisabledSaveButton: false })
        that.toastObj.show({ title: 'Warning!', content: response.data.message, cssClass: 'e-toast-warning', icon: 'e-info toast-icons' });
        global.hideLoader()
      }
      that.closeDialog()
      that.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
      global.hideLoader()
    }
    else {
      that.toastObj.show({ title: 'Information!', content: "Please Select Inputs!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      global.hideLoader()
    }
  }

  waiveOffInspectionByRole = async () => {
    global.showLoader();
    let that = this;
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = that.state;
    if (that.state.selectedRole != null) {
      let response = (await that.qualityControlService.waiveOffByRole(that.state.selectedTickets, that.state.selectedRole));
      if ((response != undefined) && (response.data.status == "success")) {
        that.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-info toast-icons' });
        global.hideLoader()
      }
      else if ((response != undefined) && (response.data.status == "unsuccess")) {
        // this.setState({ isDisabledSaveButton: false })
        that.toastObj.show({ title: 'Information!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else if ((response != undefined) && (response.data.status == "fail")) {
        // this.setState({ isDisabledSaveButton: false })
        that.toastObj.show({ title: 'Warning!', content: response.data.message, cssClass: 'e-toast-warning', icon: 'e-info toast-icons' });
        global.hideLoader()
      }
      that.closeDialog()
      that.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
      global.hideLoader()
    }
    else {
      that.toastObj.show({ title: 'Information!', content: "Please Select Inputs!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      global.hideLoader()
    }
  }

  waiveOffByType = async (type) => {
    global.showLoader();
    let that = this;
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = that.state;
    let response = (await that.qualityControlService.waiveOffByType(that.state.selectedTickets, type));
    if ((response != undefined) && (response.data.status == "success")) {
      that.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-info toast-icons' });
      global.hideLoader()
    }
    else if ((response != undefined) && (response.data.status == "unsuccess")) {
      // this.setState({ isDisabledSaveButton: false })
      that.toastObj.show({ title: 'Information!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    }
    else if ((response != undefined) && (response.data.status == "fail")) {
      // this.setState({ isDisabledSaveButton: false })
      that.toastObj.show({ title: 'Warning!', content: response.data.message, cssClass: 'e-toast-warning', icon: 'e-info toast-icons' });
      global.hideLoader()
    }
    that.closeDialog()
    that.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
    global.hideLoader()
  }

  async copyInspection(ticketNo) {
    global.showLoader();
    let that = this;
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = that.state;

    let response = (await this.qualityControlService.copyInspection(ticketNo))
    that.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
    if ((response != undefined) && (response.data.status == "success")) {
      that.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
      global.hideLoader();
    }
    else if ((response != undefined) && (response.data.status == "fail")) {
      that.toastObj.show({ title: 'Error!', content: response.data.message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      global.hideLoader();
    }
    else {
      that.toastObj.show({ title: 'Error!', content: 'Something Went Wrong!', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      global.hideLoader();
    }
  }

  onChangeRole = (event) => {
    let that = this;
    if (event.e != undefined) {
      that.setState({ selectedUserId: null, selectedRole: event.value })
      that.getUsers(event.value)
    }
  }

  onChangePlant = (event) => {
    let that = this;
    if (event.e != undefined) {
      that.setState({ selectedTable: null, selectedPlant: event.value })
      that.getTableOrMachine(event.value)
    }
  }

  onChangePlantDropDown = (event) => {
    this.setState({
      selectedPlantName: event.value,
    }, () => {
      if (event.e != undefined) {
        this.setState({ units: [] }, () => {
          this.getUnitList(event.value)
        })
      }
    })
  }

  onChangeUnits = (event) => {
    if (event.e != undefined) {
      this.setState({ selectedUnitName: event.value })
    }
  }

  onChangeManualPlant = (event) => {
    this.setState({
      manualPlant: event.value,
    }, () => {
      if (event.e != undefined) {
        this.setState({ units: [], loadList: [], manualSaids: [], manualLineItems: [], selectedManualSaid: null, selectedManualLineItem: null }, () => {
          this.getUnitListForManual(event.value)
        })
      }
    })
  }

  onChangeManualUnit = (event) => {
    const { manualPlant } = this.state;
    this.setState({
      manualUnit: event.value,
    }, () => {
      if (event.e != undefined) {
        this.getLoads(manualPlant, event.value)
      }
    })
  }

  onChangeInsId = (event) => {
    let that = this;
    // if (event.e != undefined) {
    event.inspectionType = "VSINS";
    let data = that.assignTableGridObj;
    console.log(data)
    // }
  }

  async getLoads(plantId, unitId) {
    let that = this;
    let response = (await that.qualityControlService.getLoadedLines(plantId, unitId))

    if ((response != undefined) && (response.data.status == "success")) {
      let lines = [];
      response.data.data.map(item => {
        let convertedStartDate = new Date(Date.UTC(item.startdate.year, item.startdate.month - 1, item.startdate.day))
        let convertedEndDate = new Date(Date.UTC(item.endDate.year, item.endDate.month - 1, item.endDate.day))
        lines.push(new LineLoadingModel(item.id, item.supervisor, item.fmName, item.qaName, item.apmId, item.unitId, item.lineName, item.unitName, item.scheduleId, item.apm, item.fmId, item.lineId, item.processId, item.prodOrderId, item.qaId, item.bulkUpload, item.buyerPoNr, item.confQty, item.dataPoint, item.defectQty, convertedStartDate, convertedEndDate, item.labour, item.leftQty, item.lineItemNr, item.materialKd,
          item.noOfmachines, item.processName, item.saidNr, item.sam, item.sapConfQty, item.skilledWorker, item.status, item.totalQtyLoaded, item.unskilledWorker, item.workingHrs,
          item.yellowCeiling, item.reworkQty, null, null, null, item.wipTargetTime, null, item.materialDesc, item.prodOrderRate, item.altBom, item.isTeco, item.sapRejectQty))
      })
      let uniqueSaids = lines.filter((value, index, array) => array.findIndex(t => (t.saidNr === value.saidNr) && (value.saidNr != null) && (value.saidNr != "")) === index);
      let uniqueLineItems = lines.filter((value, index, array) => array.findIndex(t => (t.lineItemNr === value.lineItemNr) && (value.lineItemNr != null) && (value.lineItemNr != "")) === index);
      this.setState({ loadList: lines, manualSaids: uniqueSaids, manualLineItems: uniqueLineItems })

    } else {

    }
    global.hideLoader();
  }

  onChangeManualSaid = (event) => {
    this.setState({
      selectedManualSaid: event.value,
    })
    let that = this;
    const { selectedManualSaid, selectedManualLineItem } = that.state;
    that.loadGrid.clearFiltering();
    if (selectedManualSaid != null)
      that.loadGrid.filterByColumn("saidNr", "equal", selectedManualSaid);
    if (selectedManualLineItem != null)
      that.loadGrid.filterByColumn("lineItemNr", "equal", selectedManualLineItem);
  }

  onChangeManualLineItem = (event) => {
    this.setState({
      selectedManualLineItem: event.value,
    })
    let that = this;
    const { selectedManualSaid, selectedManualLineItem } = that.state;
    that.loadGrid.clearFiltering();
    if (selectedManualSaid != null)
      that.loadGrid.filterByColumn("saidNr", "equal", selectedManualSaid);
    if (selectedManualLineItem != null)
      that.loadGrid.filterByColumn("lineItemNr", "equal", selectedManualLineItem);
  }

  onChangeProductionProcess = (event) => {
    this.setState({
      selectedProdProcess: event.value,
    }, () => {
      if (event.e != undefined) {
        this.setState({ selectedManualInsp: null }, () => {
          this.getManualInspList(event.value)
        })
      }
    })
    let that = this;
    const { selectedManualSaid, selectedManualLineItem, selectedProdProcess } = that.state;
    that.loadGrid.clearFiltering();
    if (selectedManualSaid != null)
      that.loadGrid.filterByColumn("saidNr", "equal", selectedManualSaid);
    if (selectedManualLineItem != null)
      that.loadGrid.filterByColumn("lineItemNr", "equal", selectedManualLineItem);
    if (selectedProdProcess != null)
      that.loadGrid.filterByColumn("processName", "equal", selectedProdProcess);
  }

  onChangeManualInsp = (event) => {
    if (event.e != undefined) {
      this.setState({ selectedManualInsp: event.value })
    }
  }

  async getTableOrMachine(plantId) {
    global.showLoader();
    let tableInfo = [];
    let response = (await this.qualityControlService.getTableOrMachine(plantId))
    if ((response != undefined) && (response.data.status == "success")) {
      response.data.data.forEach(item => {
        tableInfo.push(new QualityTableOrMachineModel(item.id, item.ticketNo, item.tableOrMachineCode + ":" + item.tableOrMachineDesc, item.tableOrMachineCode))
      })
      this.setState({ tableList: tableInfo })
      global.hideLoader();
    }
    else {
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      global.hideLoader()
    }
  }

  async saveAsiggnTableInfo(newAssignee) {
    let response = (await this.qualityControlService.saveAssignTable(newAssignee))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster(response.data.message, "Success"));
      this.getAssigneeTableData();
    }
    else if ((response != undefined) && (response.data.status == "unsuccess")) {
      this.toastObj.show(this.createToaster(response.data.message, "Error"));
      this.getAssigneeTableData();
    }
    else {
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      global.hideLoader()
    }
  }

  async deleteAssignTable(id) {
    global.showLoader();
    let response = (await this.qualityControlService.deleteAssignTable(id))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show({ title: 'Success!', content: "Assign Table/Machine deleted Successfully!!", cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
      this.getAssigneeTableData();
      global.hideLoader();
    }
    else {
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      global.hideLoader()
    }
  }

  async updateAssignTable(updatedInfo) {
    global.showLoader();
    let response = (await this.qualityControlService.updateAssignTable(updatedInfo))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster(response.data.message, "Success"));
      this.getAssigneeTableData();
      global.hideLoader()
    }
    else if ((response != undefined) && (response.data.status == "unsuccess")) {
      this.toastObj.show(this.createToaster(response.data.message, "Error"));
      this.getAssigneeTableData();
      global.hideLoader()
    }
    else {
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      this.getAssigneeTableData();
      global.hideLoader()
    }
  }


  generateInspectionReport = (event) => {

    let that = this;
    const { selectedTickets } = this.state;
    if (selectedTickets.length > 0) {
      selectedTickets.forEach(item => {
        this.generateReport(item);
      })

      this.setState({ pdfInfo: [], pdf: false, selectedTickets: [], selectedInspections: [] })
    }
    else {
      this.toastObj.show({ title: 'Information!', content: 'No Inspection Selected to generate report!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    }

  }

  downloadInspectionReport = (event) => {

    let that = this;
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus, selectedTickets, selectedInspections } = that.state;

    if (selectedInspections.length > 0) {
      selectedInspections.forEach(item => {
        if (item.docId != null) {
          this.downloadReport(item.docId, item.ticketNo, false);
        }
        else {
          this.toastObj.show({ title: 'Information!', content: 'Report File Not Found For' + " " + item.ticketNo, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });

        }
      })

      // this.setState({ pdfInfo: [], pdf: false, selectedTickets: [], selectedInspections: [] })
      that.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
    }
    else {
      this.toastObj.show({ title: 'Information!', content: 'No Inspection Selected to download report!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    }

  }

  createManualInspection = (event) => {

    let that = this;
    const { selectedManualInsp, selectedLoad, selectedType } = that.state;

    if (selectedManualInsp != null && selectedLoad != null) {
      that.createInspection(selectedLoad, 'Production', selectedManualInsp);
    }
    else {
      that.toastObj.show({ title: 'Information!', content: 'Please select Inspection Type and Load!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    }

  }

  selectLoad = (args) => {
    let that = this;
    that.setState({ selectedLoad: args.data.id });
  }

  onCommandClick = (args, event) => {
    if (args != null && event != null) {
      if (event.item.text === "View") {
        this.viewReport(args.docId, args.ticketNo);
      }
      else if (event.item.text === "Email") {
        this.sendMail(args.ticketNo);
      }
      else if (event.item.text === "Copy") {
        this.copyInspection(args.ticketNo);
      }
    }
  }

  viewReport(docId, ticketNo) {

    this.setState({ pdfInfo: [], pdf: false })
    // this.downloadReport(args.rowData.docId)
    if (docId != null) {
      this.downloadReport(docId, ticketNo, true)
    }
    else {
      this.toastObj.show({ title: 'Information!', content: 'Report File Not Found', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });

    }


  }

  async createInspection(entityId, type, inspMasterId) {
    let that = this;
    global.showLoader();
    let response = (await this.qualityControlService.createManualInspection(entityId, type, inspMasterId))
    if ((response != undefined) && (response.data.status == "success")) {
      that.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
      global.hideLoader();
      that.closeManualDialog();
    }
    else if ((response != undefined) && (response.data.status == "unsuccess")) {
      that.toastObj.show({ title: 'Error!', content: response.data.message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      global.hideLoader();
    }
    else {
      that.toastObj.show({ title: 'Error!', content: 'Something Went Wrong!', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      global.hideLoader();
    }
  }

  async sendMail(ticketNo) {
    global.showLoader();
    let response = (await this.qualityControlService.sendMail(ticketNo))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show({ title: 'Success!', content: "Mail Sent Successfully!!", cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
      global.hideLoader();
    }
    else {
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
      global.hideLoader();
    }
  }

  async generateReport(ticketNo) {
    global.showLoader();
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = this.state;
    let response = (await this.qualityControlService.generateReport(ticketNo))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show({ title: 'Success!', content: "Report Generated Successfully for " + response.data.inspection, cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
      //update the inspection
    }
    else if ((response != undefined) && (response.data.status == "fail")) {
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong for " + response.data.inspection, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }
    else {
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }
    global.hideLoader();
    this.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
  }

  async downloadReport(docId, ticketNo, isView) {
    global.showLoader();
    let response = (await this.qualityControlService.downloadReport(docId))
    if ((response != undefined) && (response.status == 200)) {
      if (isView) {
        let pdfEncoded = "data:application/pdf;base64," + response.data.data.imageEncoded;
        this.setState({ pdfInfo: pdfEncoded, pdf: true })
        global.hideLoader();
        this.showPDFDialog();
      }
      else {
        var arrBuffer = this.base64ToArrayBuffer(response.data.data.imageEncoded);
        var blob = new Blob([arrBuffer], { type: 'application/pdf' });
        var url = URL.createObjectURL(blob);
        //  window.open(blobURL);              // to open in new window 
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', ticketNo + '.pdf'); //or any other extension
        document.body.appendChild(link);
        link.click();
        global.hideLoader();
      }
      // const url = window.URL.createObjectURL(new Blob([new Uint8Array(new ArrayBuffer(response.data.data.imageEncoded[0]))]));
      // const link = document.createElement('a');
      // link.href = url;
      // link.setAttribute('download', 'report.xlsx'); //or any other extension
      // document.body.appendChild(link);
      // link.click();
      // global.hideLoader();
      // // this.toastObj.show(this.state.toasterMessage[13]);
    }
    else {
      global.hideLoader();
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }

  }

  base64ToArrayBuffer(data) {
    var binaryString = window.atob(data);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  };

  searchInspections = () => {
    let that = this;
    const { selectedPlants, selectedUnits, selectedInspNums, selectedInspStatuses, selectedDates, selectedInspResults, selectedOrders,
      selectedSaids, selectedLineItems, selectedVendors, selectedTablesOrLines, selectedInspectionLotNums, selectedDescriptions, selectedBuyers } = that.state;
    if (selectedPlants.length == 0 && selectedUnits.length == 0 && selectedInspNums.length == 0 && selectedInspStatuses.length == 0 && selectedOrders.length == 0
      && selectedDates.length == 0 && selectedInspResults.length == 0 && selectedSaids.length == 0 && selectedLineItems.length == 0 && selectedVendors.length == 0
      && selectedTablesOrLines.length == 0 && selectedInspectionLotNums.length == 0 && selectedDescriptions.length == 0 && selectedBuyers.length == 0) {
      this.toastObj.show({ title: 'Information!', content: "Please select at least one filter", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    } else {
      that.inspectionGrid.clearFiltering();
      if (selectedPlants.length > 0)
        that.inspectionGrid.filterByColumn("plantName", "equal", selectedPlants);
      if (selectedUnits.length > 0)
        that.inspectionGrid.filterByColumn("unitName", "equal", selectedUnits);
      if (selectedInspNums.length > 0)
        that.inspectionGrid.filterByColumn("ticketNo", "equal", selectedInspNums);
      if (selectedInspStatuses.length > 0)
        that.inspectionGrid.filterByColumn("inspStatus", "equal", selectedInspStatuses);
      if (selectedOrders.length > 0)
        that.inspectionGrid.filterByColumn("order", "equal", selectedOrders);
      if (selectedDates.length > 0)
        that.inspectionGrid.filterByColumn("date", "equal", selectedDates);
      if (selectedInspResults.length > 0)
        that.inspectionGrid.filterByColumn("inspResult", "equal", selectedInspResults);
      if (selectedSaids.length > 0)
        that.inspectionGrid.filterByColumn("said", "equal", selectedSaids);
      if (selectedLineItems.length > 0)
        that.inspectionGrid.filterByColumn("lineItem", "equal", selectedLineItems);
      if (selectedVendors.length > 0)
        that.inspectionGrid.filterByColumn("vendorName", "equal", selectedVendors);
      if (selectedTablesOrLines.length > 0)
        that.inspectionGrid.filterByColumn("tableOrLineName", "equal", selectedTablesOrLines);
      if (selectedInspectionLotNums.length > 0)
        that.inspectionGrid.filterByColumn("lotNum", "equal", selectedInspectionLotNums);
      if (selectedDescriptions.length > 0)
        that.inspectionGrid.filterByColumn("name", "equal", selectedDescriptions);
      if (selectedBuyers.length > 0)
        that.inspectionGrid.filterByColumn("buyerName", "equal", selectedBuyers);
      that.toastObj.show({ title: 'Success!', content: "Filter Applied Successfully!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    }
  }


  resetOrders = (isInvokedFromOnClick) => {
    let that = this;
    that.setState({
      selectedPlants: [], selectedUnits: [], selectedInspNums: [], selectedInspStatuses: [], selectedBuyersOrVendors: [],
      selectedOrders: [], selectedDates: [], selectedInspResults: [], selectedSaids: [], selectedLineItems: [], selectedTablesOrLines: [],
      selectedInspectionLotNums: [], selectedDescriptions: [], selectedBuyers: [], selectedVendors: []
    });
    that.inspectionGrid.clearFiltering();
    // if (isInvokedFromOnClick)
    this.toastObj.show({ title: 'Success!', content: "Filter Reset Successfully!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
  }

  fillTicketsList = (args) => {
    let that = this;
    let tickets = [];
    let selectedInspections = [];
    args.selectedRowIndexes.forEach(element => {
      tickets.push(that.inspectionGrid.currentViewData[element].ticketNo);
      selectedInspections.push(that.inspectionGrid.currentViewData[element])
    })
    that.setState({ selectedTickets: tickets, selectedInspections: selectedInspections })
    if (that.state.selectedTickets.length == 0) {
      that.setState({ isCheckBoxSelected: false })
    } else {
      that.setState({ isCheckBoxSelected: true })
    }
  }

  filterBoxOpen = () => {
    this.setState({
      filterBox: !this.state.filterBox
    });
  }

  showdialog = (event, selectedDialog) => {
    const { isCheckBoxSelected } = this.state
    if (selectedDialog == 'assignee') {
      if (!isCheckBoxSelected) {
        this.toastObj.show({ title: 'Information!', content: 'No Inspection Selected to Assign', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else {
        this.setState({ showDialogBox: !this.state.showDialogBox });
        this.setState({ divHidden: 'assigneeBox', showDialogBoxHeading: 'Add Assignee' });
      }
    }
    else if (selectedDialog == 'reAssign') {
      if (!isCheckBoxSelected) {
        this.toastObj.show({ title: 'Information!', content: 'No Inspection Selected to Re-Assign', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else {
        this.setState({ showDialogBox: !this.state.showDialogBox });
        this.setState({ divHidden: 'reAssigneeBox', showDialogBoxHeading: 'Change Assignee' });
      }
    }
    else if (selectedDialog == 'assignTable') {
      this.setState({ divHidden: 'assignTableBox', showDialogBoxHeading: 'Add Assignee' });
    }
    else if (selectedDialog == 'manualInspection') {
      this.setState({ manualInspectionDialog: true });
    }
    else if (selectedDialog == 'waiveOff') {
      if (!isCheckBoxSelected) {
        this.toastObj.show({ title: 'Information!', content: 'No Inspection Selected to Waive-Off', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else {
        this.setState({ showDialogBox: !this.state.showDialogBox });
        this.setState({ divHidden: 'waiveOffBox', showDialogBoxHeading: 'Waive Off' });
      }
    }
    else if (selectedDialog == 'assignTableOrMachine') {
      if (!isCheckBoxSelected) {
        this.toastObj.show({ title: 'Information!', content: 'No Inspection Selected to assign Table/Machine', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else if(this.state.selectedTickets.length>1){
        this.toastObj.show({ title: 'Information!', content: 'Please select only 1 inspection', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else {
        this.getAssigneeTableData();

      }
    }
  }
  showPDFDialog = () => {
    this.setState({ pdfViewDialog: !this.state.pdfViewDialog });
  }
  closeDialog = () => {
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = this.state;
    this.setState({ showDialogBox: false, manualInspectionDialog: false, selectedUserId: null, selectedRole: null, pdfInfo: null, pdf: false, pdfViewDialog: false });
    this.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
  }

  closeManualDialog = () => {
    this.setState({ manualPlant: null, manualUnit: null, manualInspectionDialog: false, selectedManualInsp: null, selectedManualLineItem: null, selectedManualSaid: null, selectedLoad: null, selectedProdProcess: null });
  }

  clearDropDowns = () => {
    this.setState({ selectedUserId: null, selectedRole: null });
  }

  onChangeInspectionMultiSelect = (event) => {
    // if (event.e != undefined && event.e != null)
    this.setState({ [event.element.id]: event.value })
  }


  refreshGrid = () => {
    global.showLoader();
    let that = this;
    that.getInspectionList();
    // global.hideLoader();
  }

  onChangeStartDate = (event) => {
    // if (event.e != undefined && event.e != null)
    let that = this;
    let date = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 9)

    that.setState({ inspStartDate: date });
  }

  onChangeEndDate = (event) => {
    // if (event.e != undefined && event.e != null)
    let that = this;
    let date = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 9)

    that.setState({ inspEndDate: date });
  }

  assigneetemplate(props) {
    let actual = [];
    let test = [];
    props.inspectionAssigneeDTOS.forEach((element) => {
      test.push(

        <li className={element.status}>
          <b>{element.role}</b>
          <span>{element.userName}</span>
        </li>

      )
    });



    actual.push(<div className='assignee'>
      <ul>
        {test}
      </ul>
    </div>);

    return actual;
  }

  typetemplate(props) {
    return (
      <div className="description-content">
        {props.keyName}
        <div className="overlay-description"> {props.name}</div>
      </div>
    )
  }

  async getAssigneeTableData() {
    let that = this;
    this.state.tableAssignData = [];
    this.state.filteredInspectionForAssignTable = [];
    this.state.thanList=[];
    global.showLoader();
    let updatedData = []; let updatedInsList = [];
    let response = (await this.qualityControlService.getInspectionsByTicketNo(that.state.selectedTickets))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      updatedInsList = response.data.data;
      updatedInsList.forEach(elem => {
        elem.inspectionTableAssigneeDtos.forEach(item => {
          updatedData.push(new QualityTableAssigneeModel(item.id, elem.ticketNo, elem.date, item.qty, item.tableOrMachineDesc, elem.keyName, item.tableOrMachineCode,item.thanOrRollId))
        })
        this.setState({ showDialogBox: true, tableAssignData: updatedData, filteredInspectionForAssignTable: updatedInsList ,thanList : elem.inspectionDCLDtos });
        this.setState({ divHidden: 'assignTableBox', showDialogBoxHeading: 'Add Table/Machine' });
      })
    }
    else if ((response != undefined) && (response.data.status == "unsuccess")) {
      global.hideLoader();
      this.toastObj.show(this.createToaster(response.data.message, "Information"));
    }

  }

  async getUpdatedInspection(elem) {
    let updatedData = [];
    let response = (await this.qualityControlService.getInspectionsByTicketNo(elem.ticketNo))
    if ((response != undefined) && (response.data.status == "success")) {
      response.data.data.inspectionTableAssigneeDtos.forEach(item => {
        updatedData.push(new QualityTableAssigneeModel(item.id, elem.ticketNo, elem.date, item.qty, item.tableOrMachineDesc, elem.keyName, item.tableOrMachineCode))
      })
    }
    this.setState({ tableAssignData: updatedData })
  }

  dateConverter = function (data) {
    if (data != undefined && data != null && data.date !== null && data.date !== undefined) {
      let date = data.date;
      let time = data.time;
      //let startDate = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 23, 59)
      return new Date(date.year, date.month - 1, date.day, time.hour, time.minute, time.second)
    }
  }

  rowSelectedQaGrid = (args) => {
    let that = this;
    that.setState({ selectedRowQaGrid: args.data });
    // console.log(that.state.selectedRowBtGrid)
  }
  // pdfViewOpen = () => {
  //   let that =this;
  //   this.viewReport(this.state.selectedRowQaGrid.docId)

  // }
  // buttonTemplate = (args) => {
  //   return (<div><a onClick={this.pdfViewOpen}> View </a></div>);
  // }

  onChangeRadioButton = (event) => {
    const { roleRadio, forcedRadio, notStartedRadio } = this.state;

    if ('roleRadio' == event.value) {
      this.setState({ roleRadio: true, forcedRadio: false, notStartedRadio: false })
    }
    else if ('forcedRadio' == event.value) {
      this.setState({ roleRadio: false, forcedRadio: true, notStartedRadio: false })
    }
    else if ('notStartedRadio' == event.value) {
      this.setState({ roleRadio: false, forcedRadio: false, notStartedRadio: true })
    }
  }

  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }

  waiveOffInspection = () => {
    const { roleRadio, forcedRadio, notStartedRadio } = this.state;

    if (roleRadio) {
      this.waiveOffInspectionByRole();
    }
    else if (forcedRadio) {
      this.waiveOffByType('Forced')
    }
    else if (notStartedRadio) {
      this.waiveOffByType('Not Started')
    }
  }

  createInsdrop(args) {
    this.elem = document.createElement('input');
    return this.elem;
  }
  readInsdrop(args) {

    return this.dropInstance.value;
  }
  destroyInsdrop(args) {
    this.dropInstance.destroy();
  }
  writeInsdrop(args) {
    debugger;
    this.dropInstance = new DropDownList({
      dataSource: this.state.filteredInspectionForAssignTable,
      fields: { text: 'ticketNo', value: 'ticketNo' },
      value: (args.rowData).ticketNo,
      change: this.change.bind(this)
    });
    this.dropInstance.appendTo(this.elem);
  }

  change(args) {
    let that = this;
    let insObj = that.state.filteredInspectionForAssignTable.filter(function (item) {
      return item.ticketNo == args.value;
    })
    that.setState({thanList:insObj[0].inspectionDCLDtos})
    args.element.form[5].value = insObj[0].keyName;
    args.element.form[6].value = insObj[0].date;
    // that.writeThandrop(args);

  }

  inspectionParams = {
    create: this.createInsdrop.bind(this),
    read: this.readInsdrop.bind(this),
    destroy: this.destroyInsdrop.bind(this),
    write: this.writeInsdrop.bind(this)
  };

  createThandrop(args) {
    this.elem2 = document.createElement('input');
    return this.elem2;
  }
  readThandrop(args) {

    return this.dropInstance2.value;
  }
  destroyThandrop(args) {
    this.dropInstance2.destroy();
  }
  writeThandrop(args) {
    this.dropInstance2 = new MultiSelect({
      dataSource: this.state.thanList, 
      fields: { text: 'thanOrRollId', value: 'thanOrRollId' },
      value: args.rowData!=undefined?(args.rowData).thanOrRollId:"",
      change: this.thanChange.bind(this)
    });
    this.dropInstance2.appendTo(this.elem2);
  }

  thanChange(args) {
    let that = this;
    let qty=0;
    for(let j=0;j<args.value.length;j++){
    { for(let i=0;i<this.state.thanList.length;i++){
      if(args.value[j]==this.state.thanList[i].thanOrRollId){
        qty=qty+this.state.thanList[i].thanDCLDataDTO.thanMeters;
      }
    }

    }
    // let thanObj = that.state.thanList.filter(function (item) {
    //   return item.thanOrRollId == args.value;
    // })
    args.element.form[7].value = qty;
  }
}

  thanParams = {
    create: this.createThandrop.bind(this),
    read: this.readThandrop.bind(this),
    destroy: this.destroyThandrop.bind(this),
    write: this.writeThandrop.bind(this)
  };

  


  createTabledrop(args) {
    this.elem1 = document.createElement('input');
    return this.elem1;
  }
  readTabledrop(args) {

    return this.dropInstance1.value;
  }
  destroyTabledrop(args) {
    this.dropInstance1.destroy();
  }
  writeTabledrop(args) {
    debugger;
    this.dropInstance1 = new DropDownList({
      dataSource: this.state.tableList,
      fields: { text: 'tableOrMachineDesc', value: 'tableOrMachineDesc' },
      value: (args.rowData).tableOrMachineDesc,
      // change: this.change.bind(this)
    });
    this.dropInstance1.appendTo(this.elem1);
  }

  tableParams = {
    create: this.createTabledrop.bind(this),
    read: this.readTabledrop.bind(this),
    destroy: this.destroyTabledrop.bind(this),
    write: this.writeTabledrop.bind(this)
  };



  // editInspectionIdTemplate = (args) => {
  //   let that = this;
  //   return (
  //     <DropDownListComponent id="ticketNo" allowFiltering={true} dataSource={that.state.selectedInspections} fields={{ text: 'ticketNo', value: 'ticketNo' }} change={()=>{this.onChangeInsId(args)}} placeholder="Select Inspection Id">
  //     </DropDownListComponent >
  //   )
  // }

  // editMachineTableTemplate = () => {
  //   let that = this;
  //   return (
  //     <DropDownListComponent id="tableOrMachineDesc" allowFiltering={true}  dataSource={that.state.tableList}  fields={{ text: 'tableOrMachineDesc', value: 'tableOrMachineCode' }} placeholder="Select Table/Machine">
  //     </DropDownListComponent >
  //   )
  // }

  assignTable = (args) => {
    let that = this;
    // let thans =[];
    if (args.requestType === "save") {
      if (args.data.id == undefined) {
        let assignData = [];
        assignData.push(new QualityTableAssigneeModel(null, args.data.ticketNo, null, args.data.qty, args.data.tableOrMachineDesc, null, args.data.tableOrMachineDesc.split(":")[0],args.data.thanOrRollId))
        that.saveAsiggnTableInfo(assignData);
      }
      else {
        let assignData = new QualityTableAssigneeModel(args.data.id, args.data.ticketNo, null, args.data.qty, args.data.tableOrMachineDesc, null, args.data.tableOrMachineDesc.split(":")[0],args.data.thanOrRollId);
        that.updateAssignTable(assignData);
      }
    }
    else if (args.requestType === "delete") {
      that.deleteAssignTable(args.data[0].id);
    }
  }

  search = () => {
    let that = this;
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = that.state;
    that.inspectionGrid.clearFiltering();

    if (selectedPlantName == null || selectedUnitName == null || inspStartDate == null || inspEndDate == null || selectedStatus.length == 0) {
      that.toastObj.show(this.createToaster("Please select all mandatory fields!", "Information"));
    } else {
      that.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
    }
  }

  //############## COUNT-DOWN-TIMER ###############//

  handleCountdown() {
    this.setState({
      clockCount: 300,
      isClockCountRunning: true
    })
  }

  handleStartClockCountDown = () => {
    this.timer = setInterval(() => {
      const newCount = this.state.clockCount - 1;
      if (newCount == -1) {
        this.handleStop()
      } else {
        this.setState(
          { clockCount: newCount >= 0 ? newCount : 0 }
        );
      }
    }, 1000);
  }

  handleStop = () => {
    let that = this;
    const { selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus } = that.state;
    if (that.timer) {
      clearInterval(that.timer);
      that.setState(
        { isClockCountRunning: false }, () => {
          if (selectedPlantName == null || selectedUnitName == null || inspStartDate == null || inspEndDate == null || selectedStatus.length == 0) {
            global.showLoader()
            that.getInspectionList(selectedPlantName, selectedUnitName, inspStartDate, inspEndDate, selectedStatus);
          } else {
            that.setState({ clockCount: 300, isClockCountRunning: true })
          }
        }
      )
    }
  }


  render() {
    return (
      <div className="container-fluid-inner">
        <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={{ X: 'Right', Y: 'Bottom' }}   ></ToastComponent>
        <h1 className='pagesheading'>Quality Console</h1>
        <div className='clearfix'></div>
        <span className='float-right'>
          <Row>
            <Col xs='6' md='6' className='text-right'>
              <p id="demo"></p>
            </Col>
            <Col xs='6' md='6'>
              <CountDownClockComponent time={this.state.clockCount} />

            </Col>
          </Row>
        </span>
        <Row>
          <Col sm='3'>
            <div className="required-field">
              <DropDownListComponent color="primary" id="plantId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedPlantName}
                change={this.onChangePlantDropDown} dataSource={this.state.plants} placeholder={"Select Plant"} floatLabelType="Auto" />
            </div>

          </Col>
          <Col sm='2'>
            <div className="required-field">
              <DropDownListComponent color="primary" id="unitId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedUnitName}
                change={this.onChangeUnits} dataSource={this.state.units} placeholder={"Select Unit"} floatLabelType="Auto" />

            </div>

          </Col>
          <Col sm='2'>
            <div className="required-field">
              <DatePickerComponent placeholder="Inspection Start Date" format='dd/MM/yy' floatLabelType="Auto" value={this.state.inspStartDate} change={this.onChangeStartDate}></DatePickerComponent>
            </div>
          </Col>
          <Col sm='2'>
            <div className="required-field">
              <DatePickerComponent placeholder="Inspection End Date" format='dd/MM/yy' floatLabelType="Auto" value={this.state.inspEndDate} change={this.onChangeEndDate}></DatePickerComponent>
            </div>
          </Col>
          <Col sm='2'>
            {/* <DropDownListComponent id="inspectionStatus" placeholder="Inspection Status" dataSource={this.statusData} value={this.state.selectedStatus} change={this.onChangeStatus} floatLabelType="Auto" popupHeight="300px" /> */}
            <MultiSelectComponent id="selectedStatus" dataSource={this.statusData} value={this.state.selectedStatus} change={this.onChangeInspectionMultiSelect}
              placeholder="Inspection Status" mode="CheckBox" showSelectAll={true} showDropDownIcon={true} floatLabelType="Auto" fields={{ text: 'status', value: 'status' }} >
              <Inject services={[CheckBoxSelection]} />
            </MultiSelectComponent>
          </Col>
        </Row>
        <div><Button color="primary" size="sm" className="mr-2" onClick={this.search} id="search"> Search</Button></div>

        <div className="mt-2"></div>
        <a className={this.state.filterBox ? 'filter-icon-open' : 'filter-icon-close'} onClick={this.filterBoxOpen}>Filter <i className='fa  fa-filter'></i></a>
        <div className='filter-dropdown' hidden={this.state.filterBox}>
          <div className='filter-Selection'>
            <h3>Location</h3>
            <Row>
              <Col sm='3'>
                <MultiSelectComponent id="selectedTablesOrLines" ref={(scope) => { this.selectedTablesOrLinesObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredTablesOrLines}
                  value={this.state.selectedTablesOrLines} change={this.onChangeInspectionMultiSelect} fields={{ text: 'tableOrLineName', value: 'tableOrLineName' }}
                  placeholder={"Select Table/Machine"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                  showDropDownIcon={true} filterBarPlaceholder="Search Table/Machine" popupHeight="300px">
                  <Inject services={[CheckBoxSelection]} />
                </MultiSelectComponent>
              </Col>
            </Row>
          </div>
          <div className='filter-Selection'>
            <h3>Inspection</h3>
            <Row>
              <Col sm='3'>  <MultiSelectComponent id="selectedDescriptions" ref={(scope) => { this.selectedDescObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredDescriptions}
                value={this.state.selectedDescriptions} change={this.onChangeInspectionMultiSelect} fields={{ text: 'name', value: 'name' }}
                placeholder={"Select Inspection Types"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Inspection Type" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              <Col sm='2'>  <MultiSelectComponent id="selectedInspNums" ref={(scope) => { this.selectedInspTicketObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredInspNums}
                value={this.state.selectedInspNums} change={this.onChangeInspectionMultiSelect} fields={{ text: 'ticketNo', value: 'ticketNo' }}
                placeholder={"Select Inspection Numbers"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Inspection Numbers" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              <Col sm='2'>  <MultiSelectComponent id="selectedInspectionLotNums" ref={(scope) => { this.selectedInspectionLotNums = scope; }} floatLabelType="Auto" dataSource={this.state.filteredInspectionLotNums}
                value={this.state.selectedInspectionLotNums} change={this.onChangeInspectionMultiSelect} fields={{ text: 'lotNum', value: 'lotNum' }}
                placeholder={"Select Lot Number"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Lot Number" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              <Col sm='2'>  <MultiSelectComponent id="selectedInspStatuses" ref={(scope) => { this.selectedInspStatusObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredInspStatuses}
                value={this.state.selectedInspStatuses} change={this.onChangeInspectionMultiSelect} fields={{ text: 'inspStatus', value: 'inspStatus' }}
                placeholder={"Select Status"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Status" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
            </Row>
            <div className='mt-3'></div>
            <Row>
              <Col sm='2'>  <MultiSelectComponent id="selectedDates" ref={(scope) => { this.selectedDateObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredDates}
                value={this.state.selectedDates} change={this.onChangeInspectionMultiSelect} fields={{ text: 'date', value: 'date' }}
                placeholder={"Select Date"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Date" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              <Col sm='2'>  <MultiSelectComponent id="selectedInspResults" ref={(scope) => { this.selectedInspResultObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredInspResults}
                value={this.state.selectedInspResults} change={this.onChangeInspectionMultiSelect} fields={{ text: 'inspResult', value: 'inspResult' }}
                placeholder={"Select Result"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Result" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>

            </Row>
          </div>
          <div className='filter-Selection'>
            <h3>Order</h3>
            <Row>
              <Col sm='2'>  <MultiSelectComponent id="selectedOrders" ref={(scope) => { this.selectedOrderObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredOrders}
                value={this.state.selectedOrders} change={this.onChangeInspectionMultiSelect} fields={{ text: 'order', value: 'order' }}
                placeholder={"Select Production/Purchase Order"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterType="Contains" filterBarPlaceholder="Search Production/Purchase Order" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              <Col sm='2'>  <MultiSelectComponent id="selectedSaids" ref={(scope) => { this.selectedSaidObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredSaids}
                value={this.state.selectedSaids} change={this.onChangeInspectionMultiSelect} fields={{ text: 'said', value: 'said' }}
                placeholder={"Select Said"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Said" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              <Col sm='2'>  <MultiSelectComponent id="selectedLineItems" ref={(scope) => { this.selectedLineItemObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredLineItems}
                value={this.state.selectedLineItems} change={this.onChangeInspectionMultiSelect} fields={{ text: 'lineItem', value: 'lineItem' }}
                placeholder={"Select Line Item"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Line Item" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              <Col sm='3'>  <MultiSelectComponent id="selectedVendors" ref={(scope) => { this.selectedVendorObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredVendors}
                value={this.state.selectedVendors} change={this.onChangeInspectionMultiSelect} fields={{ text: 'vendorName', value: 'vendorName' }}
                placeholder={"Select Vendor"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Vendor" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              <Col sm='3'>  <MultiSelectComponent id="selectedBuyers" ref={(scope) => { this.selectedBuyerObj = scope; }} floatLabelType="Auto" dataSource={this.state.filteredBuyers}
                value={this.state.selectedBuyers} change={this.onChangeInspectionMultiSelect} fields={{ text: 'buyerName', value: 'buyerName' }}
                placeholder={"Select Buyer"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Buyer" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
            </Row>
          </div>
          <div className='filter-Selection'>
            <h3>Assignee</h3>
            <Row>
              <Col sm='3'>
                <DropDownListComponent id="selectPlant" placeholder="Select QA" floatLabelType="Auto" popupHeight="300px" /></Col>
              <Col sm='3'>
                <DropDownListComponent id="selectPlant" placeholder="Select QC" floatLabelType="Auto" popupHeight="300px" /></Col>
              <Col sm='3'>
                <DropDownListComponent id="selectPlant" placeholder="Select Lab Assistant" floatLabelType="Auto" popupHeight="300px" /></Col>
            </Row>
          </div>

          <Button color="primary" size="sm" className='mt-2 mb-2' onClick={this.searchInspections}>{"Apply filter"}</Button>
          <Button color="primary" size="sm" id="resetOrders" onClick={() => { this.resetOrders(true) }} className="ml-2">{"Reset Filter"}</Button>

        </div>
        <div className="mt-2"></div>
        {/* <div className='work-stausBox'>
          <ul className='work-staus'>
            <li>Done <b>{this.state.uniqueInspResults.length}</b></li>
            <li>  Pending  <b>13</b> </li>
            <li>  New  <b>100</b> </li>
            <li> PO  <b>2</b></li>
            <li>Prod Ord  <b>43</b></li>
            <li>  Job Work  <b>12</b></li>
            <li>Line  <b>40</b></li>
            <li>Table <b>7</b></li>
            <li>Pass  <b>24</b></li>
            <li>Fail  <b>12</b></li>
            <li>Time Dur(Hr)<b>100</b></li>
          </ul>
          <div className='clearfix'></div>
        </div> */}

        <div className='clearfix'></div>
        <div>
          <Button color="primary" size="sm" className='mr-2' hidden={true}>Waive</Button>
          <Button color="primary" size="sm" onClick={(e) => { this.showdialog(e, "assignee") }}>Add Assignee</Button>
          <Button color="primary" size="sm" className='ml-2' onClick={(e) => { this.showdialog(e, "reAssign") }}>Re-Assign</Button>
          <Button color="primary" size="sm" className='ml-2' onClick={(e) => { this.showdialog(e, "manualInspection") }}>Manual Inspection</Button>
          <Button color="primary" size="sm" className='ml-2' hidden={true}>Copy Inspection</Button>
          <Button color="primary" size="sm" className='ml-2' hidden={true} className='ml-2 mr-2' onClick={(e) => { this.showdialog(e, "assignTable") }}>Assign Table/Line</Button>
          <Button color="primary" size="sm" className='ml-2' onClick={this.downloadInspectionReport}>Print/Download</Button>
          <Button color="primary" size="sm" className='ml-2' onClick={(e) => { this.showdialog(e, "waiveOff") }}>Waive-Off</Button>
          <Button color="primary" size="sm" className='ml-2' onClick={(e) => { this.showdialog(e, "assignTableOrMachine") }}>Assign Table/Machine</Button>
          <Button color="primary" size="sm" className='ml-2' onClick={this.generateInspectionReport}>Generate Report</Button>
          {/* <Button color="primary" size="sm" className='ml-2 float-right' onClick={this.refreshGrid}>Refresh</Button> */}
          <ul className="legend-group">
            <li><span className="assigned"></span>Assigned </li>
            <li><span className="progress"></span>In progress </li>
            <li><span className="completed"></span> Completed </li>
            <li><span className="not"></span> Not Assigned </li>
          </ul>
        </div>

        <div className='clearfix'></div>
        <GridComponent gridLines='Both' toolbar={this.columnChooser} showColumnChooser={true} allowResizing={true} allowTextWrap={false} allowSelection={true} height={this.state.windowsHeight} rowHeight={30} ref={grid => this.inspectionGrid = grid}
          dataSource={this.state.inspectionDetails} enableHover={true} checkBoxChange={this.fillTicketsList} selectionSettings={this.state.select} allowReordering={true}
          allowSorting={true} allowPaging={true} filterSettings={this.state.FilterSettingsModel} pageSettings={AppConfig.pageSettings} allowFiltering={true} rowSelected={this.rowSelectedQaGrid}>
          <ColumnsDirective>
            <ColumnDirective field='text' headerText='Action' allowFiltering={false} width='50' template={this.actionTemplate.bind(this)} textAlign='Center' />
            <ColumnDirective type='checkbox' allowSorting={false} allowFiltering={true} width='35' defaultValue={false} ></ColumnDirective>
            <ColumnDirective field='ticketNo' filter={this.state.IFilter} headerText='Inspection Id' width='130' />
            <ColumnDirective field='name' filter={this.state.IFilter} headerText='Description' width='150' visible={false} />
            <ColumnDirective field='keyName' filter={this.state.IFilter} template={this.typetemplate} headerText='Type' width='100' />
            <ColumnDirective field='date' filter={this.state.IFilter} headerText='Date' width='100' />
            <ColumnDirective field='assignee' template={this.assigneetemplate} headerText='Assignee' width='400' />
            <ColumnDirective field='order' filter={this.state.IFilter} headerText='PR/PO' width='100' />
            <ColumnDirective field='vendorName' filter={this.state.IFilter} headerText='Vendor' width='250' />
            <ColumnDirective field='buyerName' filter={this.state.IFilter} headerText='Buyer' width='250' />
            <ColumnDirective field='mrName' filter={this.state.IFilter} headerText='MR' width='130' />
            <ColumnDirective field='plantName' filter={this.state.IFilter} headerText='Plant' width='130' />
            <ColumnDirective field='unitName' filter={this.state.IFilter} headerText='Unit' width='100' />
            <ColumnDirective field='inspResult' filter={this.state.IFilter} headerText='Result' width='110' />
            <ColumnDirective field='inspStatus' filter={this.state.IFilter} headerText='Status' width='120' />
            <ColumnDirective field='said' filter={this.state.IFilter} headerText='Said' width='110' />
            <ColumnDirective field='lineItem' filter={this.state.IFilter} headerText='LI' width='80' />
            <ColumnDirective field='lotNum' filter={this.state.IFilter} headerText='Lot Num' width='110' />
            <ColumnDirective field='startTime' filter={this.state.IFilter} format={this.state.format} headerText='Start Time' width='140' />
            <ColumnDirective field='endTime' filter={this.state.IFilter} format={this.state.format} headerText='End Time' width='140' />
            <ColumnDirective field='tableOrLineName' filter={this.state.IFilter} headerText='Table/Line' width='130' />
            <ColumnDirective field='grnNo' filter={this.state.IFilter} headerText='GRN' width='100' />
            <ColumnDirective field='grnDate' filter={this.state.IFilter} headerText='GRN Date' width='100' />
            <ColumnDirective field='grnQty' filter={this.state.IFilter} headerText='GRN Qty' width='100' />
            <ColumnDirective field='materialType' filter={this.state.IFilter} headerText='Dept' width='100' />
            <ColumnDirective field='materialDesc' filter={this.state.IFilter} headerText='Material Desc' width='250' />
            <ColumnDirective field='passQty' filter={this.state.IFilter} headerText='Pass Qty' width='100' />
            <ColumnDirective field='failQty' filter={this.state.IFilter} headerText='Fail Qty' width='100' />
            {/* <ColumnDirective headerText='Action' textAlign='Left' commands={this.viewIcon} width='85' /> */}


          </ColumnsDirective>
          <Inject services={[Filter, Sort, Selection, Resize, Page, Group, CommandColumn, Reorder, Toolbar, ColumnChooser]} />
        </GridComponent>

        {/* ----------------------------------------- Manual Inspection Begins -------------------------------------- */}
        <DialogComponent
          visible={this.state.manualInspectionDialog}
          showCloseIcon={true}
          animationSettings={this.animationSettings}
          isModal={true} allowDragging={true}
          width={'70%'}
          ref={dialog => this.dialogInstance = dialog}
          header='Manual Inspection'
          close={this.closeManualDialog}>

          <Col sm='8'>
            <Row className='mt-3'>
              <Col>
                <RadioButtonComponent label='Store' value='Store' name='1'></RadioButtonComponent>
              </Col>
              <Col>
                <RadioButtonComponent label='Production' checked='true' value='Production' name='1'></RadioButtonComponent>
              </Col>
              <Col>
                <RadioButtonComponent label='Pre-Production' value='Pre-Production' name='1'></RadioButtonComponent>
              </Col>
            </Row>
          </Col>
          <Row className='mt-1'>
            <Col>
              <DropDownListComponent color="primary" id="plantId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.manualPlant}
                change={this.onChangeManualPlant} dataSource={this.state.plants} placeholder={"Select Plant"} floatLabelType="Auto" popupHeight="220px" />
            </Col>
            <Col>
              <DropDownListComponent color="primary" id="unitId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.manualUnit}
                change={this.onChangeManualUnit} dataSource={this.state.units} placeholder={"Select Unit"} floatLabelType="Auto" />
            </Col>
            <Col>
              <DropDownListComponent placeholder="Select Said" popupHeight="220px" allowFiltering={true} value={this.state.selectedManualSaid} fields={{ text: 'saidNr', value: 'saidNr' }}
                change={this.onChangeManualSaid} dataSource={this.state.manualSaids} placeholder={"Select Said"} floatLabelType="Auto" />
            </Col>
            <Col>
              <DropDownListComponent placeholder="Select Line Items" floatLabelType='Auto' popupHeight="220px" allowFiltering={true} fields={{ text: 'lineItemNr', value: 'lineItemNr' }} value={this.state.selectedManualLineItem}
                change={this.onChangeManualLineItem} dataSource={this.state.manualLineItems} placeholder={"Select Line Item"} />
            </Col>
          </Row>
          <Row className='mt-1'>
            <Col sm='3'>
              <DropDownListComponent placeholder="Select Production process" popupHeight="220px" floatLabelType='Auto' allowFiltering={true} value={this.state.selectedProdProcess} placeholder="Select a Production Process"
                dataSource={this.state.productionProcessList} change={this.onChangeProductionProcess} fields={{ text: 'processName', value: 'processName' }} popupHeight="220px" />
            </Col>
            <Col sm='3'>
              <DropDownListComponent placeholder="Select Inspection Type" floatLabelType='Auto' popupHeight="220px" allowFiltering={true}
                fields={{ text: 'name', value: 'id' }} value={this.state.selectedManualInsp} change={this.onChangeManualInsp}
                dataSource={this.state.manualInspList} placeholder={"Select Manual Inspection"} />
            </Col>
          </Row>

          <div className='mt-2'></div>
          <GridComponent
            visible={true}
            enableHover={false}
            allowFiltering={true}
            filterSettings={this.state.FilterSettingsModel}
            allowSorting={true}
            allowSelection={true}
            allowResizing={true}
            rowHeight={25}
            dataSource={this.state.loadList}
            ref={grid => this.loadGrid = grid}
            rowSelected={this.selectLoad}>
            <ColumnsDirective>
              <ColumnDirective field='id' headerText='Load ID' width='120' />
              <ColumnDirective field='buyerPoNr' headerText='Buyer po' width='120' />
              <ColumnDirective field='prodOrderId' headerText='Production Order ID' width='120' />
              <ColumnDirective field='processName' headerText='Process Name' width='120' />
              <ColumnDirective field='totalQtyLoaded' headerText='Total loaded Qty' width='120' />
              <ColumnDirective field='confQty' headerText='Confirmed Qty' width='120' />
              <ColumnDirective field='defectQty' headerText='Defect QTY' width='120' />
              <ColumnDirective field='reworkQty' headerText='Rework QTY' width='120' />
              <ColumnDirective field='saidNr' filter={this.state.IFilter} headerText='Defect QTY' width='120' />
              <ColumnDirective field='lineItemNr' filter={this.state.IFilter} headerText='Rework QTY' width='120' />
            </ColumnsDirective>
            <Inject services={[Filter, Sort, Selection, Resize, Page, Group, CommandColumn, Reorder, Toolbar, ColumnChooser]} />
          </GridComponent>

          <Row className='float-right mt-2'>
            <Button color="primary" className="mr-2" size="sm" onClick={this.createManualInspection} >Make Manual Inspection</Button>
            <Button size="sm" onClick={this.closeDialog} >Cancel</Button>
          </Row>

        </DialogComponent>
        {/* ----------------------------------------- Manual Inspection Ends -------------------------------------- */}

        {/* ------------- ---------------------- View all Documents ------------- ---------------------- */}
        <DialogComponent allowDragging={true} id="modalDialog" isModal={true} width='50%' visible={this.state.showDialogBox} showCloseIcon={true} header={this.state.showDialogBoxHeading} close={this.closeDialog}>
          {/* ----------------------------- UD Form  -----------------------------*/}
          {this.state.divHidden == 'assigneeBox' ?
            <div className='pl-3 mt-n2 pb-3 pr-3'>
              <Row>
                <Col sm='4'>
                  <DropDownListComponent id="selectedRole" placeholder="Select Role" floatLabelType="Auto" floatLabelType="Auto" filterType="Contains"
                    value={this.state.selectedRole} dataSource={this.state.rolesList} popupHeight="300px" allowFiltering={true} change={this.onChangeRole} /></Col>
                <Col sm='4'>
                  <DropDownListComponent id="selectedUserId" placeholder="Select User" floatLabelType="Auto" popupHeight="300px" fields={{ text: 'fullName', value: 'userId' }}
                    value={this.state.selectedUserId} dataSource={this.state.usersList} filterType="Contains" popupHeight="300px" allowFiltering={true} change={this.onChangeInspectionMultiSelect} /></Col>
              </Row>
              <div className='mt-3'></div>
              <Button color="primary" size="sm" className='mr-2' onClick={this.assignInspections} >Save</Button>
              <Button color="primary" size="sm" onClick={this.clearDropDowns} >Cancel</Button>

              {/* <GridComponent gridLines='Both' allowResizing={true} allowTextWrap={true} rowHeight={30} dataSource={this.state.assigneeBoxGrid} enableHover={true} allowSorting={true} toolbar={this.toolbarOptions} allowPaging={true} editSettings={this.editSettings} >
                <ColumnsDirective>
                  <ColumnDirective field='id' headerText='Id' width='50' isPrimaryKey={true} />
                  <ColumnDirective field='description' headerText='Description' width='200' isPrimaryKey={true} />
                  <ColumnDirective field='roles' headerText='Roles' width='80' isPrimaryKey={true} />
                  <ColumnDirective field='userid' headerText='User ids' editType='dropdownedit' width='100' />
                </ColumnsDirective>
                <Inject services={[Filter, Toolbar, Sort, Resize, Edit]} />
              </GridComponent> */}
            </div>
            : null}
          {this.state.divHidden == 'reAssigneeBox' ?
            <div className='pl-3 mt-n2 pb-3 pr-3'>
              <Row>
                <Col sm='4'>
                  <DropDownListComponent id="selectedRole" placeholder="Select Role" floatLabelType="Auto" floatLabelType="Auto" filterType="Contains"
                    value={this.state.selectedRole} dataSource={this.state.rolesList} popupHeight="300px" allowFiltering={true} change={this.onChangeRole} /></Col>
                <Col sm='4'>
                  <DropDownListComponent id="selectedUserId" placeholder="Select User" floatLabelType="Auto" popupHeight="300px" fields={{ text: 'fullName', value: 'userId' }}
                    value={this.state.selectedUserId} dataSource={this.state.usersList} filterType="Contains" popupHeight="300px" allowFiltering={true} change={this.onChangeInspectionMultiSelect} /></Col>
              </Row>
              <div className='mt-3'></div>
              <Button color="primary" size="sm" className='mr-2' onClick={this.reAssignInspections} >Save</Button>
              <Button color="primary" size="sm" onClick={this.clearDropDowns} >Cancel</Button>

              {/* <GridComponent gridLines='Both' allowResizing={true} allowTextWrap={true} rowHeight={30} dataSource={this.state.assigneeBoxGrid} enableHover={true} allowSorting={true} toolbar={this.toolbarOptions} allowPaging={true} editSettings={this.editSettings} >
                <ColumnsDirective>
                  <ColumnDirective field='id' headerText='Id' width='50' isPrimaryKey={true} />
                  <ColumnDirective field='description' headerText='Description' width='200' isPrimaryKey={true} />
                  <ColumnDirective field='roles' headerText='Roles' width='80' isPrimaryKey={true} />
                  <ColumnDirective field='userid' headerText='User ids' editType='dropdownedit' width='100' />
                </ColumnsDirective>
                <Inject services={[Filter, Toolbar, Sort, Resize, Edit]} />
              </GridComponent> */}
            </div>
            : null}
          {this.state.divHidden == 'assignTableBox' ?
            <div className='pl-3  mt-n2 pb-3 pr-3'>
              <Row>
                <Col sm='4'>
                  <DropDownListComponent id="selectedPlant" placeholder="Select Plant" floatLabelType="Auto" floatLabelType="Auto" filterType="Contains" fields={{ text: 'name', value: 'id' }}
                    value={this.state.selectedPlant} dataSource={this.state.plants} popupHeight="300px" allowFiltering={true} change={this.onChangePlant} /></Col>
                {/* <Col sm='4'> */}
                {/* <DropDownListComponent id="selectedTable" placeholder="Select Table/Machine" floatLabelType="Auto" popupHeight="300px" fields={{ text: 'tableOrMachineDesc', value: 'tableOrMachineCode' }}
                    value={this.state.selectedTable} dataSource={this.state.tableList} filterType="Contains" popupHeight="300px" allowFiltering={true}  /></Col> */}
              </Row>
              <GridComponent gridLines='Both' allowResizing={true} allowTextWrap={true} rowHeight={30} dataSource={this.state.tableAssignData} ref={grid => this.assignTableGridObj = grid} enableHover={true} allowSorting={true} toolbar={this.toolbarOptions} allowPaging={true} actionBegin={this.assignTable} editSettings={this.editSettings} >
                <ColumnsDirective>
                  {/* <ColumnDirective field='ticketNo' headerText='Inspection Id' width='120' editType='dropdownedit' editTemplate={this.editInspectionIdTemplate}   disabled allowEditing={false}/> */}
                  <ColumnDirective field='ticketNo' headerText='Inspection Id' width='150' edit={this.inspectionParams} validationRules={this.state.validationrules} />
                  <ColumnDirective field='thanOrRollId' headerText='Than' width='170' edit={this.thanParams} validationRules={this.state.validationrules} />

                  <ColumnDirective field='inspectionType' headerText='Inspection Type' width='120' value={this.state.selectedInsType} disabled allowEditing={false} />
                  <ColumnDirective field='inspectionDate' headerText='Date' width='120' disabled allowEditing={false} />
                  {/* <ColumnDirective field='qty' headerText='Qty' width='70' allowEditing={true} editType='numericedit' edit={{ params: { decimals: 2, min: 1 } }} validationRules={{ required: true }} /> */}
                  <ColumnDirective field='qty' headerText='Qty' width='70' disabled allowEditing={false} />
                  <ColumnDirective field='tableOrMachineDesc' headerText='Table/Machine Name' width='180' edit={this.tableParams} validationRules={this.state.validationrules} />
                  {/* <ColumnDirective field='table' headerText='Table' editType='dropdownedit' width='100' /> */}
                </ColumnsDirective>
                <Inject services={[Filter, Toolbar, Sort, Resize, Edit]} />
              </GridComponent>
            </div>
            : null}

          {this.state.divHidden == 'waiveOffBox' ?
            <div className='pl-3  mt-n2 pb-3 pr-3'>
              <RadioButtonComponent label="Forced Waive Off " name="default" checked={this.state.forcedRadio} onChange={this.onChangeRadioButton} value={'forcedRadio'} />
              <span className='ml-3 mr-3'><RadioButtonComponent label="Not Started Inspection Waive off" name="default" checked={this.state.notStartedRadio} onChange={this.onChangeRadioButton} value={'notStartedRadio'} /></span>
              <RadioButtonComponent label="Select Role" name="default" checked={this.state.roleRadio} onChange={this.onChangeRadioButton} value={'roleRadio'} />
              <div style={{ display: (this.state.roleRadio ? 'block' : 'none') }}>
                <Row>
                  <Col sm="4">
                    <DropDownListComponent id="selectedRole" placeholder="Select Role" floatLabelType="Auto" floatLabelType="Auto" filterType="Contains"
                      value={this.state.selectedRole} dataSource={this.state.rolesList} popupHeight="300px" allowFiltering={true} change={this.onChangeRole} />
                  </Col>
                </Row>
              </div>
              <div>

              </div>
              <div className='mt-3'></div>
              <Button color="primary" size="sm" className='mr-2' onClick={this.waiveOffInspection} >Save</Button>
              <Button color="primary" size="sm" onClick={this.clearDropDowns} >Cancel</Button>
            </div>
            : null}
        </DialogComponent>
        {/* ------------- ---------------------- View all Documents ------------- ---------------------- */}

        {/* ------------- ---------------------- PDF Documents ------------- ---------------------- */}
        <DialogComponent allowDragging={true} id="modalDialogPDF" isModal={true} width='60%' visible={this.state.pdfViewDialog} showCloseIcon={true} header='PDF View' close={this.closeDialog}>
          <br />
          <br />
          <div className='pl-3 mt-n2 pb-3 pr-3'>
            {this.state.pdf == true &&
              <center> <div >
                <SinglePagePDFViewer pdf={this.state.pdfInfo} />
                <hr />
              </div>
              </center>
            }
          </div>
        </DialogComponent>
        {/* ------------- ---------------------- PDF Documents ------------- ---------------------- */}



      </div>

    );
  }
}

export default QualityConsole;
