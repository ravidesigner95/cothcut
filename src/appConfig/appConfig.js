
export class AppConfig {
    /* --------------grid var -------------- */
    static gridHeight = window.innerHeight - 280 + 'px';
    static editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, showDeleteConfirmDialog: true, newRowPosition: 'Top' }
    static toolbarOptionsButton = ['Add', 'Edit', 'Delete', 'Update', 'Cancel']
    static pageSettings = { pageCount: 4, currentPage: '1', pageSize: 20, pageSizes: [10, 20, 50, 100, 200, 500] }
    static FilterSettingsModel = { type: 'Menu' }
    // height={AppConfig.gridHeight}
    // editSettings={AppConfig.editSettings} 
    // toolbar={AppConfig.toolbarOptionsButton}            
    // pageSettings={AppConfig.pageSettings} 
    /* --------------grid var -------------- */
}

