import { rightToLeft } from '@syncfusion/ej2-base';
import { rippleMouseHandler } from '@syncfusion/ej2-buttons';
import { SwitchComponent } from '@syncfusion/ej2-react-buttons';
// import { RadioButtonComponent } from '../../assets/data/data';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { ColumnDirective, ColumnsDirective, CommandColumn, Edit, Filter, Grid, GridComponent, Group, Inject, Page, Reorder, Resize, Selection, SelectionType, Sort, Toolbar, VirtualScroll } from '@syncfusion/ej2-react-grids';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { Toast, ToastComponent, ToastModel } from '@syncfusion/ej2-react-notifications';
import { AnimationSettingsModel, ButtonPropsModel, DialogComponent } from '@syncfusion/ej2-react-popups';
import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import AqlModel, { QualityMasterAqlDataModel } from './AqlModel';
import QualityMasterService from './QualityMasterService';
import { AppConfig } from '../../appConfig/appConfig';
import '../../App.scss';

class AQLQualityMasterComponent extends React.PureComponent {

  constructor() {
    super(...arguments);
    this.QualityMasterService = new QualityMasterService();
    this.state = {
      liClicked: false,
      highlighted: false,
      aqlList: [],
      aqlNames: [],
      selectedAqlData: [],
      selectedAqlName: null,
      selectedAqlId: null,
      aqlStatus: false,
      aqlfield:false,
      showDialog: false,
      toasterPosition: { X: 'Right', Y: 'Bottom' },
      /* active: '', */
    };

    // this.setState({
    //   orderDataSource: this.orderDataSource 
    // });
    this.buttonRef = element => {
      this.buttonEle = element;
    };
    this.buttons = [{
      click: () => {
        this.onSaveClick();
        // this.getAqlList();
        // this.dialogInstance.hide();
      },
      buttonModel: {
        content: 'Save',
        isPrimary: true,
      }
    },
    {
      click: () => {
        this.getAqlList();
        this.setState({aqlfield:false})
        this.dialogInstance.hide();
      },
      buttonModel: {
        content: 'Cancel',
        cssClass: 'e-secondary'
      },
      click: () => {
        // this.getAqlList();
        this.dialogInstance.hide();
        // console.log(this.dialogInstance);
        // this.setState({showDialog:false})
      }
      
    }];
    this.animationSettings = { effect: 'None' };

    this.toolbarOptions = ['Add', 'Edit', 'Delete', 'Update', 'Cancel'];
    // this.toolbarOptionsedit = ['Add', 'Delete', 'Cancel'];
    // this.editSettingsForAQLLeftPanel = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Bottom' };
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top', mode: 'Normal' };
    this.editparams = { params: { popupHeight: '500px' } };
    this.validationRule = { required: true };
    this.orderidRules = { required: true, number: true };
/*     this.pageSettings = { pageCount: 5 }; */
    this.format = { type: 'dateTime', format: 'M/d/y hh:mm a' };

  }

/*   addActiveClass(e){
    const clicked = e.target.id
    if(this.state.active === clicked) { 
        this.setState({active: ''});
    } else {
        this.setState({active: clicked})
   }
} */

  componentDidMount() {
    this.getAqlList();
  }

  //////////////////////////////////////////////////////////////////////api calling////////////////////////////////////////////////////////////////////////

  async getAqlList() {
    let response = (await this.QualityMasterService.getAqlList())
    global.showLoader();
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let aqlInfoList = [];
      let idSelectedData;
      let idSelectedAQLName;
      response.data.data.map(aqlInfo => {
        let aqlData = [];
        if(aqlInfo.id == this.state.selectedAqlId){
          idSelectedData = aqlInfo.qualityMasterAqlData
          idSelectedAQLName = aqlInfo.name
        }
        aqlInfo.qualityMasterAqlData.map(item => {
          aqlData.push(new QualityMasterAqlDataModel(item.id,null, item.orderQuantityFrom, item.orderQuantityTo, item.sampleSize, item.criticalAccept, item.criticalReject, item.majorAccept, item.majorReject,
            item.minorAccept, item.minorReject));
        })
        aqlInfoList.push(new AqlModel(aqlInfo.id, aqlInfo.name, aqlInfo.isActive, false, aqlData));
      })
      if(this.state.selectedAqlId){
        this.setState({ aqlList: aqlInfoList, selectedAqlData: idSelectedData, selectedAqlName: idSelectedAQLName , selectedAqlId:this.state.selectedAqlId, });
      }else{
        this.setState({ aqlList: aqlInfoList, selectedAqlData: aqlInfoList[0].qualityMasterAqlData, selectedAqlName: aqlInfoList[0].name, selectedAqlId: aqlInfoList[0].id });
      }
      const newobj = [...this.state.aqlList];
      if(this.state.selectedAqlId){
        newobj.filter((item,findex) => item.id == this.state.selectedAqlId ? item.isTab = true : item.isTab = false)
      }else{
        newobj.filter((item,findex) => findex == 0 ? item.isTab = true : item.isTab = false)
      }
      this.setState({aqlList:newobj})
      
      this.aqlNametemplate();
    }
    else {
      global.hideLoader();
      // this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }


  async saveAqlDetail(newAql) {
    let that= this;
    let response = (await this.QualityMasterService.saveAql(newAql))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster(response.data.message, "Success"));
       this.getAqlList();
       that.dialogInstance.hide();
      // that.setState({showDialog:false})
    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }

  async deleteAql(id) {
    let response = (await this.QualityMasterService.deleteAql(id))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster("Aql  deleted successfully!!", "Success"));
      this.setState({selectedAqlId:null})
      this.getAqlList();
    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }

  async deleteAqlData(id) {
    let that=this;
    let response = (await this.QualityMasterService.deleteAqlData(id))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster("Aql  deleted successfully!!", "Success"));
      this.getAqlList();
      // that.dialogInstance.hide();
      this.setState({showDialog:false})
    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }
  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }

  onSaveClick() {
    let that =this;
    if(this.state.selectedAqlName=="" || this.state.selectedAqlName==null ){
      that.toastObj.show(that.createToaster("Please enter Aql name!","Information" ));
    }else if(this.state.aqlfield){
      that.toastObj.show(that.createToaster("Please fill all fields and update!","Information" ));
    }
    else{
    let aqlUpdated = new AqlModel(this.state.selectedAqlId, this.state.selectedAqlName, this.state.aqlStatus, false, this.state.selectedAqlData)
    this.saveAqlDetail(aqlUpdated)
    }
  }

  actionBegin(args) {
    let that = this
    if (args.requestType === 'add') {
      that.setState({aqlfield:true})
    }
    if (args.requestType === 'cancel') {
      that.setState({aqlfield:false})
    }
    if (args.requestType === 'save') {
      args.cancel = true;
      that.setState({aqlfield:false})
      if(args.data.id == undefined && args.data.deid == undefined){
        that.setState({selectedAqlData:[...that.state.selectedAqlData,new QualityMasterAqlDataModel(null, new Date().valueOf(), args.data.orderQuantityFrom, args.data.orderQuantityTo, args.data.sampleSize, args.data.criticalAccept, args.data.criticalReject, args.data.majorAccept, args.data.majorReject,
            args.data.minorAccept, args.data.minorReject)]})
      }else{
        let selectAqlCop = [...that.state.selectedAqlData]
        if(args.data.id){
          this.state.selectedAqlData.forEach((el,i)=>{
            if(el.id == args.data.id) {
              selectAqlCop[i].criticalAccept = args.data.criticalAccept
              selectAqlCop[i].criticalReject = args.data.criticalReject
              selectAqlCop[i].majorAccept = args.data.majorAccept
              selectAqlCop[i].majorReject = args.data.majorReject
              selectAqlCop[i].minorAccept = args.data.minorAccept
              selectAqlCop[i].minorReject = args.data.minorReject
              selectAqlCop[i].orderQuantityFrom = args.data.orderQuantityFrom
              selectAqlCop[i].orderQuantityTo = args.data.orderQuantityTo
              selectAqlCop[i].sampleSize = args.data.sampleSize
            }
          })
        }else{
          this.state.selectedAqlData.forEach((el,i)=>{
            if(el.deid == args.data.deid) {
              selectAqlCop[i].criticalAccept = args.data.criticalAccept
              selectAqlCop[i].criticalReject = args.data.criticalReject
              selectAqlCop[i].majorAccept = args.data.majorAccept
              selectAqlCop[i].majorReject = args.data.majorReject
              selectAqlCop[i].minorAccept = args.data.minorAccept
              selectAqlCop[i].minorReject = args.data.minorReject
              selectAqlCop[i].orderQuantityFrom = args.data.orderQuantityFrom
              selectAqlCop[i].orderQuantityTo = args.data.orderQuantityTo
              selectAqlCop[i].sampleSize = args.data.sampleSize
            }
          })
        }

        that.setState({selectedAqlData:selectAqlCop})
      }
      // if(this.state.selectedAqlName=="" || this.state.selectedAqlName==null ){
      //   this.toastObj.show(this.createToaster("Please enter Aql name!","Information" ));
      // }
      // else{
      // let aqlNewRow = [];
      // let aqlData = [];
      // aqlData.push(new QualityMasterAqlDataModel(args.data.id, args.data.orderQuantityFrom, args.data.orderQuantityTo, args.data.sampleSize, args.data.criticalAccept, args.data.criticalReject, args.data.majorAccept, args.data.majorReject,
      //   args.data.minorAccept, args.data.minorReject));
      // aqlNewRow = (new AqlModel(this.state.selectedAqlId, this.state.selectedAqlName, this.state.aqlStatus, aqlData));
      // this.saveAqlDetail(aqlNewRow);
      // }

      // if (this.gridInstance.pageSettings.currentPage !== 1 && this.gridInstance.editSettings.newRowPosition === 'Top') {
      //   args.index = (this.gridInstance.pageSettings.currentPage * this.gridInstance.pageSettings.pageSize) - this.gridInstance.pageSettings.pageSize;
      // }
      // else if (this.gridInstance.editSettings.newRowPosition === 'Bottom') {
      //   args.index = (this.gridInstance.pageSettings.currentPage * this.gridInstance.pageSettings.pageSize) - 1;
      // }
    }
  
    if (args.requestType === 'delete') {
      args.cancel = true;
      let data = that.state.selectedAqlData.filter((el,i)=> {
        if(args.data[0].id){
          return  el.id !== args.data[0].id
        }else{
          return  el.deid !== args.data[0].deid
        }
      })
      
      that.setState({selectedAqlData:data})
    }
  }
  ddChange() {
    this.gridInstance.editSettings.newRowPosition = this.dropDownInstance.value;
  }

  aqlClicked = (item_id,i) => {
    let selectedObj = this.state.aqlList.filter(function (item) {
      return item.id == item_id;
    });
    this.setState({ selectedAqlData: selectedObj[0].qualityMasterAqlData, selectedAqlName: selectedObj[0].name, selectedAqlId: selectedObj[0].id })
    this.setState({liClicked: true});
    
    const newobj = [...this.state.aqlList];
    newobj.filter((item,findex) => findex == i ? item.isTab = true : item.isTab = false)
    this.setState({aqlList:newobj})

    

    this.aqlNametemplate()
    
  }

  linkClicked = (args) => {
    let that = this
    if (args == 'new') {
      that.setState({ selectedAqlData: [], selectedAqlName: null, selectedAqlId: null, aqlStatus: false });
    }
    else {
      let selectedObj = that.state.aqlList.filter(function (item) {
        return item.id == args;
      })
      that.setState({ selectedAqlData: selectedObj[0].qualityMasterAqlData, selectedAqlName: selectedObj[0].name, selectedAqlId: selectedObj[0].id, aqlStatus: selectedObj[0].isActive })
    }
    // that.setState({
    //   showDialog: true
    // });
    // console.log(that.gridInstance)
    that.dialogInstance.show();
  }

  deleteAqlById = (args) => {
   let that = this;
   that.deleteAql(args);
  }

  dialogClose() {
    this.setState({
      showDialog: false,
      aqlfield:false
      
      //  orderDataSource: this.orderDataSource
    });
    // this.buttonEle.style.display = "block";
  }
  dialogOpen() {
    // this.buttonEle.style.display = "none";
    // this.dialogInstance.show()
  }
  onChangeAqlName = (event) => {
    this.setState({ selectedAqlName: event.value.trim() })
  }

  onChangeStatus = (event) => {
    this.setState({ aqlStatus: event.checked })
  }
  aqlNametemplate = (args) => {
    const aqlNames = []
    for (const [index, value] of this.state.aqlList.entries()) {
      aqlNames.push(<li key={index} className={`selected-vertical-tabs ${value.isTab ? 'active':''}`} onClick={this.aqlClicked.bind(this, value.id,index)}><a>{value.name}</a> <span className='actions'><i className="fa fa-edit"   onClick={this.linkClicked.bind(this, value.id)}></i> <i className="fa fa-trash" onClick={this.deleteAqlById.bind(this, value.id)}></i></span></li>)
    }
    this.setState({ aqlNames: aqlNames })
  }

  numericParams = {
    params: {
        decimals: 0,
        format: "N",
        validateDecimalOnType: true
    }
  };

  render() {
    return (
      <div>
        <Button color="primary" className='addbtn' size="sm" onClick={this.linkClicked.bind(this, 'new')} >Add</Button>
        <Row>
          <Col sm='2' xs='12' className='vertical-tabs-scroller'>
            <ul className='vertical-tabs'>
              {this.state.aqlNames}
            </ul>
          </Col>
          <Col sm='10' xs='12'>
            <GridComponent
              enableHover={false}
              allowFiltering={false}
              allowSorting={true}
              allowSelection={true}
              allowResizing={true}
              rowHeight={25}
              gridLines='Both'
              // toolbar={this.toolbarOptions}
              allowPaging={true}
              allowTextWrap={true}
              allowRowDragAndDrop={false}
              height={AppConfig.gridHeight}
              filterSettings={AppConfig.FilterSettingsModel}
              // editSettings={AppConfig.editSettings}
              pageSettings={AppConfig.pageSettings}
              dataSource={this.state.selectedAqlData}
              ref={grid => this.gridInstance = grid}
              // actionBegin={this.actionBegin.bind(this)}
              >
              <ColumnsDirective>
                <ColumnDirective
                  columns={[
                    { field: 'orderQuantityFrom', headerText: 'Min', textAlign: 'Center', width: 70, validationRules: { required: true }, editType:'numericedit' },
                    { field: 'orderQuantityTo', headerText: 'Max', width: 70, textAlign: 'Center', validationRules: { required: true }, editType:'numericedit' }]} headerText='Available Quality' textAlign="Center" />
                <ColumnDirective field='sampleSize' headerText='Sample Size' width='90' textAlign='center' editType='numericedit' validationRules= {{ required: true }}></ColumnDirective>
                <ColumnDirective columns={[
                  { field: 'criticalAccept', headerText: 'Accept', textAlign: 'Center', width: 70, validationRules: { required: true }, editType:'numericedit' }, { field: 'criticalReject', headerText: 'Reject', textAlign: 'Center', width: 70, }
                ]} headerText='Critical (AQL=2.5)' textAlign="Center" />
                <ColumnDirective columns={[
                  { field: 'majorAccept', headerText: 'Accept', textAlign: 'Center', width: 70, validationRules: { required: true }, editType:'numericedit' },
                  { field: 'majorReject', headerText: 'Reject', textAlign: 'Center', width: 70, validationRules: { required: true }, editType:'numericedit' }
                ]} headerText='Major (AQL=2.5)' textAlign="Center" />
                <ColumnDirective columns={[
                  { field: 'minorAccept', headerText: 'Accept', textAlign: 'Center', width: 70, validationRules: { required: true }, editType:'numericedit' },
                  { field: 'minorReject', headerText: 'Reject', className: 'text-center', textAlign: 'Center', width: 70, validationRules: { required: true }, editType:'numericedit' }]}
                  headerText='Minor (AQL=2.5)' textAlign="Center" />
              </ColumnsDirective>
              <Inject services={[Page, Toolbar, Edit]} />
            </GridComponent>
          </Col>
        </Row>
        <DialogComponent
          id="defaultDialog"
          showCloseIcon={true}
          allowDragging={true}
          animationSettings={this.animationSettings}
          visible={this.state.showDialog} width={'96%'}
          ref={dialog => this.dialogInstance = dialog}
          //target={'#AQLDialogBox'}
          header='AQL Details'
          isModal={true}
          buttons={this.buttons}
          // open={this.dialogOpen.bind(this)}
          // close={this.dialogClose.bind(this)}
          >
          <Row>
            <Col sm='3'>
              <TextBoxComponent placeholder="AQL Name" value={this.state.selectedAqlName} onChange={this.onChangeAqlName} validationRules={{ required: true }} floatLabelType="Auto" />
            </Col>
            <Col sm='9' className='status-switch'>
              Status<label htmlFor="unchecked" className='mr-3' ></label>
              <SwitchComponent id="unchecked" checked={this.state.aqlStatus} change={this.onChangeStatus} ></SwitchComponent>
            </Col>
          </Row>
          <div className='mt-2'></div>
          <GridComponent
            enableHover={false}
            allowFiltering={false}
            allowSorting={true}
            allowSelection={true}
            allowResizing={true}
            rowHeight={25}
            height='70'
            gridLines='Both'
            allowPaging={true}
            allowTextWrap={true}
            allowRowDragAndDrop={false} 
            toolbar={AppConfig.toolbarOptionsButton}
            filterSettings={AppConfig.FilterSettingsModel}
            editSettings={AppConfig.editSettings}
            pageSettings={AppConfig.pageSettings}
            dataSource={this.state.selectedAqlData}
            ref={grid => this.gridInstance = grid}
            actionBegin={this.actionBegin.bind(this)}>
            <ColumnsDirective>
              <ColumnDirective columns={[{ field: 'orderQuantityFrom', headerText: 'Min', textAlign: 'Center', width: 150, edit:this.numericParams, editType:'numericedit', validationRules:{ required: true } }, { field: 'orderQuantityTo', headerText: 'Max', width: 90, textAlign: 'Center', width: 150, edit:this.numericParams, editType:'numericedit', validationRules:{ required: true } }]} headerText='Available Quality' textAlign="Center" validationRules={{ required: true , number:true }}/>
              <ColumnDirective field='sampleSize' headerText='Sample Size' width='120' textAlign='center' width='150' editType='numericedit' edit={this.numericParams} validationRules={this.SampleSizeRule} validationRules={{ required: true }}></ColumnDirective>
              <ColumnDirective columns={[{ field: 'criticalAccept', headerText: 'Accept', textAlign: 'Center', width: 150, edit:this.numericParams, editType:'numericedit', validationRules:{ required: true }}, { field: 'criticalReject', textAlign: 'Center', headerText: 'Reject', width: 150, edit:this.numericParams, editType:'numericedit', validationRules:{ required: true }}]} headerText='Critical (AQL=2.5)' textAlign="Center" />
              <ColumnDirective columns={[{ field: 'majorAccept', headerText: 'Accept', textAlign: 'Center', width: 150, edit:this.numericParams, editType:'numericedit', validationRules:{ required: true }}, { field: 'majorReject', textAlign: 'Center', headerText: 'Reject', width: 150, edit:this.numericParams, editType:'numericedit', validationRules:{ required: true }}]} headerText='Major (AQL=2.5)' textAlign="Center" />
              <ColumnDirective columns={[{ field: 'minorAccept', headerText: 'Accept', textAlign: 'Center', width: 150, edit:this.numericParams, editType:'numericedit', validationRules:{ required: true } }, { field: 'minorReject', textAlign: 'Center', headerText: 'Reject', width: 150, edit:this.numericParams, editType:'numericedit', validationRules:{ required: true } }]} headerText='Minor (AQL=2.5)' textAlign="Center" />
            </ColumnsDirective>
            <Inject services={[Page, Toolbar, Edit]} />
          </GridComponent>
        </DialogComponent>
        <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
      </div>);
  }
}

export default AQLQualityMasterComponent;
