import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { GridComponent, ColumnsDirective, ColumnDirective, Filter, Inject, RowDD, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, Reorder, Search } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { sampleData } from '../../assets/data/inspection'
import { data } from '../../assets/data/data';
import { Query } from '@syncfusion/ej2-data';
// import { sampleBase64pdf } from "../../../sampleBase64pdf";
class QuestionformComponent extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    this.countries = sampleData;
   
    this.state = {
      filterBox: true,
      divHidden: null,
      listfield: { dataSource: this.countries, id: 'id', text: 'name', parentID: 'pid', hasChildren: 'hasChild' },
      dataGrid:[
        {EmployeeID:12,}
      ]
    }
    this.toolbarOptions = ['Add', 'Edit', 'Delete', 'Update', 'Cancel', 'Search'];
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top' }
    
    this.formData = [
      {
      
        Questions: 'Does the product size meet as per approved sample/technical specification.',
       
      },
      {

        Questions: 'Does the stitched product design, Color & construction matches as per buyers Approved Sample or Yellow seal sample and technical specification.',
      
      }, 

    ];
    //define the filtering data
    this.temp = 'categories'; 
    this.newsearchData = data['Questionform'];
    // maps the appropriate column to fields property
    this.fields = { text: 'Name', value: 'Code' };
    // filtering event handler to filter a Country
    this.onFiltering = (e) => {
      let query = new Query();
      //frame the query based on search string with filter type.
      query = (e.text !== '') ? query.where('Name', 'startswith', e.text, true) : query;
      //pass the filter data source, filter query to updateData method.
      e.updateData(this.searchData, query);
    };
  }
  componentDidMount() {
  }
  filterBoxOpen = () => {
    this.setState({
      filterBox: !this.state.filterBox
    });

  }
 
categorytemplate(){ 
  return (<div>  <DropDownListComponent placeholder="Select a Category" popupHeight="220px" /></div> );
} 
questointemplate(){ 
  return (<div>  <DropDownListComponent placeholder="Select a Questions" popupHeight="220px" /></div> );
} 
answertypetemplate(){ 
  return (<div>  Yes</div> );
}  
  content2 = () => {
    return (<div>
      <Row>
        <Col sm='8'>
        <div id='filtering'>
            <DropDownListComponent id="category" ref={(dropdownlist) => { this.listObj = dropdownlist; }} dataSource={this.newsearchData} filtering={this.onFiltering.bind(this)} filterBarPlaceholder='Questions' allowFiltering={true} fields={this.fields} placeholder="Select a Field" popupHeight="220px" />
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          <span className='float-right margin-top16'>
            <Button color="primary" size="sm" onClick="">Add Questions</Button>&nbsp;
               <Button className="btncancel" size="sm" onClick="">Cancel</Button>
          </span>
        </Col>
      </Row>
      {/* <Row>
        <Col>
          {this.content3}
        </Col>
      </Row> */}
    </div>)
  }
  content4 = () => {
    return (<div>
      <Row>
        <Col sm='3'>
          <TextBoxComponent placeholder="Technical Verification" floatLabelType="Auto" />
        </Col>
        <Col sm='3'>
          <TextBoxComponent placeholder="Field Name" floatLabelType="Auto" />
        </Col>

      </Row>
      <Row className='sectiontype'>
        <Col sm='3'>
          <TextBoxComponent placeholder="Technical Verification" floatLabelType="Auto" />
        </Col>
        <Col sm='2' className='margin-top16'>
          <RadioButtonComponent checked={true} label='Prefilled' name='payment' value=""></RadioButtonComponent>
        </Col>
        <Col sm='2' className='margin-top16'>
          <RadioButtonComponent checked={false} label='Manual' name='payment' value=""></RadioButtonComponent>
        </Col>
        <Col sm='2' >
          <DropDownListComponent id="Category" filterBarPlaceholder='Search a category' placeholder="Select a category" popupHeight="220px" />
        </Col>
        <Col sm='2'>
          <DropDownListComponent id="Category" filterBarPlaceholder='Search a category' placeholder="Select Field" popupHeight="220px" />
        </Col>
      </Row>
      <Row>
        <Col>
          <span className='float-right margin-top16'>
            <Button color="primary" size="sm" onClick="">Add</Button>&nbsp;
               <Button className="btncancel" size="sm" onClick="">Cancel</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }

  render() {
    return (
      <Row>
      <Col sm='12 mt-4'>
        <GridComponent  toolbar={this.toolbarOptions} allowPaging={true} editSettings={this.editSettings} pageSettings={this.pageSettings} allowRowDragAndDrop={true} width='auto' dataSource={this.formData}>
                  <ColumnsDirective>                 
                   <ColumnDirective headerText='Category' template={this.categorytemplate} textAlign='Center' width='200'/> 
                  <ColumnDirective headerText='Questions' template={this.questointemplate} textAlign='Center'/>  
                  <ColumnDirective field='Answertype' headerText='Answertype' width='125' template={this.answertypetemplate}  textAlign='Right'/>                    
                  </ColumnsDirective>
                  <Inject services={[Page, Toolbar, Edit, RowDD, Selection, Search]} />
                </GridComponent> 
                </Col>
                </Row>
//       <AccordionComponent>
//         <AccordionItemsDirective>
//           <AccordionItemDirective className='panel-heading' expanded='true' header='Technical Verification : Question Form' content={this.content2} />
//           <AccordionItemDirective  header="Does the stitched product design, Color & construction matches as per buyer's Approved Sample or Yellow seal sample and

// technical specification" content={this.content3} />
//           <AccordionItemDirective header='Does the product size meet as per approved sample/technical specification.' content={this.content3} />
//           <AccordionItemDirective header='Are the approved Fabric, Accessories, trims used.' content={this.content3} />

//         </AccordionItemsDirective>
//       </AccordionComponent>


    );
  }
}

export default QuestionformComponent;
