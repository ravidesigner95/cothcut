export class UserModel {
    constructor(role, radiantUserId, userName) {
        this.role = role;
        this.radiantUserId = radiantUserId;
        this.userName = userName;
        
    }
    role = null;
    radiantUserId = null;
    userName = null;

}

export default UserModel; 