export class InspectionModel {
    constructor(id, ticketNo, date, stringDate, name, keyName, reminderRule, expirationRule, createdOn, modifiedOn, requestHandlerId, requestHandlerName, manuallyGenerated, formId, versionNo,
        isActive, inspStatus, docId, tableOrLineName, plantName, unitName, order, vendorName, inspResult, said, lineItem, lotNum, startTime, endTime, grnNo, materialType,
        materialDesc, mrName, buyerName, grnDate, grnQty, passQty, failQty,
        inspectionAssigneeDTOS, inspectionCommonsDTO, inspectionTableAssigneeDtos) {
        this.id = id;
        this.ticketNo = ticketNo;
        this.date = date;
        this.stringDate = stringDate;
        this.name = name;
        this.keyName = keyName;
        this.reminderRule = reminderRule;
        this.expirationRule = expirationRule;
        this.createdOn = createdOn;
        this.modifiedOn = modifiedOn;
        this.requestHandlerId = requestHandlerId;
        this.requestHandlerName = requestHandlerName;
        this.manuallyGenerated = manuallyGenerated;
        this.formId = formId;
        this.versionNo = versionNo;
        this.isActive = isActive;
        this.inspStatus = inspStatus;
        this.docId = docId;
        this.tableOrLineName = tableOrLineName;
        this.plantName = plantName;
        this.unitName = unitName;
        this.order = order;
        this.vendorName = vendorName;
        this.inspResult = inspResult
        this.said = said;
        this.lineItem = lineItem;
        this.lotNum = lotNum;
        this.startTime = startTime;
        this.endTime = endTime;
        this.grnNo = grnNo;
        this.grnDate = grnDate;
        this.grnQty = grnQty;
        this.materialType = materialType;
        this.materialDesc = materialDesc;
        this.mrName = mrName;
        this.buyerName = buyerName;
        this.passQty = passQty;
        this.failQty = failQty;
        this.inspectionAssigneeDTOS = inspectionAssigneeDTOS;
        this.inspectionCommonsDTO = inspectionCommonsDTO;
        this.inspectionTableAssigneeDtos = inspectionTableAssigneeDtos;
    }

    id = null;
    ticketNo = null;
    date = null;
    stringDate = null;
    name = null;
    keyName = null;
    reminderRule = null;
    expirationRule = null;
    createdOn = null;
    modifiedOn = null;
    requestHandlerId = null;
    requestHandlerName = null;
    manuallyGenerated = null;
    formId = null;
    versionNo = null;
    isActive = null;
    inspStatus = null;
    docId = null;
    tableOrLineName = null;
    plantName = null;
    unitName = null;
    order = null;
    buyerOrVendorName = null;
    inspResult = null;
    said = null;
    lineItem = null;
    lotNum = null;
    startTime = null;
    endTime = null;
    grnNo = null;
    materialType = null;
    materialDesc = null;
    mrName = null;
    inspectionAssigneeDTOS = [];
    inspectionCommonsDTO = null;
    inspectionTableAssigneeDtos = [];
}
export default InspectionModel;

export class InspectionAssigneeModel {
    constructor(id, inspectionNo, role, userId, userName, sequence, createdOn, startTime, endTime, status, result, isNecessary) {
        this.id = id;
        this.inspectionNo = inspectionNo;
        this.role = role;
        this.userId = userId;
        this.userName = userName;
        this.sequence = sequence;
        this.createdOn = createdOn;
        this.startTime = startTime;
        this.endTime = endTime;
        this.status = status;
        this.result = result;
        this.isNecessary = isNecessary;
    }
    id = null;
    inspectionNo = null;
    role = null;
    userId = null;
    userName = null;
    sequence = null;
    createdOn = new Date();
    startTime = new Date();
    endTime = new Date();
    status = null;
    result = null;
    isNecessary = false;
}

export class InspectionCommonsModel {
    constructor(id, saidId, lineItem, qaName, qaId, mrName, mrId, apmName, apmId, buyerName, buyerId, vendorName, vendorId, plantName, plantId, unitName, unitId, prodOrder, purchaseOrder, materialKd, materialDesc,
        processName, productType, requestHandlerName, requestHandlerId, date, uom, fm_id, fm_name, grn_no, grn_date, said_soid_qty, grn_qty, bill_no, bill_date, report_no, pe_name,
        lot_no, product_type_group) {
        this.id = id;
        this.saidId = saidId;
        this.lineItem = lineItem;
        this.qaName = qaName;
        this.qaId = qaId;
        this.mrName = mrName;
        this.mrId = mrId;
        this.apmName = apmName;
        this.apmId = apmId;
        this.buyerName = buyerName;
        this.buyerId = buyerId;
        this.vendorName = vendorName;
        this.vendorId = vendorId;
        this.plantName = plantName;
        this.plantId = plantId;
        this.unitName = unitName;
        this.unitId = unitId;
        this.prodOrder = prodOrder;
        this.purchaseOrder = purchaseOrder;
        this.materialKd = materialKd;
        this.materialDesc = materialDesc;
        this.processName = processName;
        this.productType = productType;
        this.requestHandlerName = requestHandlerName;
        this.requestHandlerId = requestHandlerId;
        this.date = date;
        this.uom = uom;
        this.fm_id = fm_id;
        this.fm_name = fm_name;
        this.grn_no = grn_no;
        this.grn_date = grn_date;
        this.said_soid_qty = said_soid_qty;
        this.grn_qty = grn_qty;
        this.bill_no = bill_no;
        this.bill_date = bill_date;
        this.report_no = report_no;
        this.pe_name = pe_name;
        this.lot_no = lot_no;
        this.product_type_group = product_type_group;

    }
    id = null;
    saidId = null;
    lineItem = null;
    qaName = null;
    qaId = null;
    mrName = null;
    mrId = null;
    apmName = null;
    apmId = null;
    vendorName = null;
    vendorId = null;
    buyerName = null;
    buyerId = null;
    plantName = null;
    plantId = null;
    unitName = null;
    unitId = null;
    prodOrder = null;
    purchaseOrder = null;
    materialKd = null;
    materialDesc = null;
    processName = null;
    productType = null;
    requestHandlerName = null;
    requestHandlerId = null;
    date = null;
    uom = null;
    fm_id = null;
    fm_name = null;
    grn_no = null;
    grn_date = null;
    said_soid_qty = null;
    grn_qty = null;
    bill_no = null;
    bill_date = null;
    report_no = null;
    pe_name = null;
    lot_no = null;
    product_type_group = null;

}
export class UsersModel {
    constructor(id, firstName, lastName, userName, userId, roles) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userId = userId;
        this.userName = userName;
        this.roles = roles;
    }
    id = null;
    firstName = null;
    lastName = null;
    userId = null;
    userName = null;
    roles = null;
}

export class QualityInspType {
    constructor(id, name) {
        this.id = id;
        this.name = name;
    }
    id = null;
    name = null;
}