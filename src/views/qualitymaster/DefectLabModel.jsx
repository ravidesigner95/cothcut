export class BuyerTestPlanMappingModel {
    constructor(id, buyerName, buyerId, productType, productTypeGroup, testPlanGroupDTO, testPlanGroup) {
        this.id = id;
        this.buyerName = buyerName;
        this.buyerId = buyerId;
        this.productType = productType;
        this.productTypeGroup = productTypeGroup;
        this.testPlanGroupDTO = testPlanGroupDTO;
        this.testPlanGroup = testPlanGroup;

    }
    id = null;
    buyerName = null;
    buyerId = null;
    productType = null;
    productTypeGroup = null;
    testPlanGroupDTO = null;
    testPlanGroup = null;

}
export default BuyerTestPlanMappingModel;

export class TestPlanGroupModel {
    constructor(id, testPlanGroupCode, testPlanGroupName, labTestDTOs) {
        this.id = id;
        this.testPlanGroupCode = testPlanGroupCode;
        this.testPlanGroupName = testPlanGroupName;
        this.labTestDTOs = labTestDTOs;

    }
    id = null;
    testPlanGroupCode = null;
    testPlanGroupName = null;
    labTestDTOs = [];


}

export class LabTestMasterModel {
    constructor(id, name, description, code, testMethod, micDTOs, pid) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.code = code;
        this.testMethod = testMethod;
        this.micDTOs = micDTOs;
        this.pid = pid;
    }
    id = null;
    name = null;
    description = null;
    code = null;
    testMethod = null;
    micDTOs = [];
    pid = null;
}

export class MICMasterModel {
    constructor(id, micCode, description, inputMinRange, inputMaxRange, passMinRange, passMaxRange, isManual, unit, pid, uiGridId) {
        this.id = id;
        this.micCode = micCode;
        this.description = description;
        this.inputMinRange = inputMinRange;
        this.inputMaxRange = inputMaxRange;
        this.passMinRange = passMinRange;
        this.passMaxRange = passMaxRange;
        this.isManual = isManual;
        this.unit = unit;
        this.pid = pid;
        this.uiGridId = uiGridId;
    }
    id = null;
    micCode = null;
    description = null;
    inputMinRange = null;
    inputMaxRange = null;
    passMinRange = null;
    passMaxRange = null;
    isManual = null;
    unit = null;
    pid = null;
    uiGridId = null;
}