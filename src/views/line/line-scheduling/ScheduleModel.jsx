export class ScheduleModel {
    constructor(id, date, prodLineId, dayWiseWorkLoadSchedule) {
        this.id = id;
        this.date = date;
        this.prodLine.id = prodLineId;
        this.dayWiseWorkLoadSchedule = dayWiseWorkLoadSchedule;
    }
    id = null;
    date = {};
    prodLine = {
        id: null
    };
    dayWiseWorkLoadSchedule = [];
}

export default ScheduleModel; 