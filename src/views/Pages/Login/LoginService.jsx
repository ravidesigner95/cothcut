import axios from 'axios';
class LoginService {
    constructor() { }
    
    version = "/api/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    loginUrl = this.baseUrl + `/auth/signin`;
    sendinMailgUrl = this.baseUrl + `/auth/sendingMail`;

    async login(data) {
        try {
            const options = {
                headers: { 'Content-Type': 'application/json' }
            };
            const response = await axios.post(this.loginUrl, data, options)
            return response.data;
        }
        catch (error) {
          //  alert("error")
        }
    }


    async sendingmail(data) {
        try {
            const options = {
                headers: { 'Content-Type': 'application/json' }
            };
            const response = await axios.post(this.sendinMailgUrl, data, options)
            return response.data;
        }
        catch (error) {
         //   alert("error")
        }
    }

}
export default LoginService;