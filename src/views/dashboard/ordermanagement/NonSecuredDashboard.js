import React from 'react';

class OrderManagementDashboard extends React.PureComponent { 
  baseUrl = process.env.REACT_APP_MOEP_DASHBOARD;
  moepUrl = process.env.REACT_APP_BASEURL_DASHBOARD
  dashboard = this.moepUrl + "/dashboard/index.html";
  prodURLTabOrderDashboard= process.env.REACT_APP_MOEP_PRODUCTION_DASHBOARD_OrderMgmt; 
  render() {  
    return (
      <div className="container-fluid-inner">
        <h1 className='pagesheading'>Order Dashboard</h1>
        <div>
      
        <iframe src={this.prodURLTabOrderDashboard} height="765" width="100%" allowFullScreen={true} frameBorder="0"></iframe>
      </div>
         </div>
    );
  }
}

export default OrderManagementDashboard;
