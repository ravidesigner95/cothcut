let de = {
  translations: {
    "Welcome to Radiant App": "Welcome to Radiant App",

    //############### Login #################//

    "cpm.login": "Login",
    "cpm.login.username": "Username",
    "cpm.login.password": "Password",
    "cpm.forgotheading": "Forgot Password",
    "cpm.email": "Email",
    "cpm.submit": "Submit",
    "cpm.resetpassword": "Reset Password",
    "cpm.password": "Password",
    "cpm.confirmpassword": "Confirm Password",
    "cpm.savepassword": "Save",
    //############## Line Process ##########//

    "cpm.lm.grid.key.name": "Key Name",
    "cpm.lm.grid.key.value": " Value",
    "cpm.lm.field.key.name": "Key Name",
    "cpm.lm.field.line.description": "Line Description",
    "cpm.lm.field.key.description": " key Description",
    "cpm.lm.field.key.value": "Value",
    "cpm.lm.field.line.process": "Line Process",
    "cpm.lm.addkey": "Add Key",
    "cpm.lm.addnewLine": "Add New Line Process ",
    "cpm.lm.grid.line.description": " Description",
    "cpm.lm.line.changeCategory": "Change Category",


    //############## Line Management ########//

    "cpm.lm.addline": "Add Line",
    "cpm.lm.editline": "Edit Line",
    "cpm.lm.grid.plant.id": "Plant Id",
    "cpm.lm.grid.plant.name": "Plant Name",
    "cpm.lm.grid.line.process": "Line Process",
    "cpm.lm.field.line.name": "Line Name",
    "cpm.lm.field.line.noOfMachine": "No of Machines",
    "cpm.lm.field.line.workingHour": "Working Hour",
    "cpm.lm.field.line.inActive": "Inactive",
    "cpm.lm.field.line.active": "Active",
    "cpm.lm.field.line.supervisor": "Select Supervisor",
    "cpm.lm.grid.category": "Category",
    "cpm.lm.grid.unit.name": "Unit Name",
    "cpm.lm.field.unit.name": "Unit Name",
    "cpm.lm.grid.line.name": "Line Name",
    "cpm.lm.grid.line.type": "Line Type",
    "cpm.lm.grid.status": "Status",
    "cpm.lm.grid.line.processes": "Line Processes",
    "cpm.lm.grid.action": "Action",
    "cpm.lm.line.plant.name": "Select Plant Name",
    "cpm.lm.line.shift": "Working Shift",
    "cpm.lm.line.showshiftdetails": "Show Shift details",
    "cpm.lm.line.category": "Select Category",
    "cpm.lm.line.unit.name": "Select Unit Name",
    "cpm.lm.line.line.type.int": "Internal",
    "cpm.lm.line.line.type.ext": "External",
    "cpm.lm.line.line.process": "Line Process",
    "cpm.lm.line.save": "Save",
    "cpm.lm.line.add": "Add",
    "cpm.lm.line.cancel": "Cancel",
    "cpm.lm.line.close": "Close",
    "cpm.lm.grid.capacity": "Capacity",
    "cpm.lm.grid.act.capacity": "Actual Capacity",
    "cpm.lm.line.prod.line": "Production Line",
    "cpm.lm.line.said.no": "Said No",
    "cpm.lm.line.assign.to": "APM",
    "cpm.lm.line.merchant.name": "Merchant Name",
    "cpm.lm.line.buyer.name": "Buyer Name",
    "cpm.lm.line.prod.category": "Line Category",
    "cpm.lm.line.line.itemno": "Line Item Number",
    "cpm.lm.line.total.qty": "Total Qty",
    "cpm.lm.line.line.qty": "Line Qty",
    "cpm.lm.line.current.date": "Current Date",
    "cpm.lm.line.add.planning": "Add Planning",
    "cpm.lm.line.edit.planning": "Edit Planning",
    // "cpm.lm.line.lineName":"Select Line",

    //----------Validation Message-----------//
    "cpm.lm.line.validation.plantName": " Please select Plant Name !!",
    "cpm.lm.line.validation.unitName": "Please select Unit Name!!",
    "cpm.lm.line.validation.lineName": "Please enter Line Name !!",
    "cpm.lm.line.validation.noOfMachines": "Please enter No of Machines !!",
    "cpm.lm.line.validation.workingHours": "Please enter working hours !!",
    "cpm.lm.line.validation.lineNameLength": "Length of line name shouldn't be greater than 25 !!",
    "cpm.lm.line.validation.supervisor": "Please select supervisorv !!",
    "cpm.lm.line.validation.shift": "Please select working shift !!",

    //----------Validation Message for add user-----------//
    "cpm.lm.line.validation.firstName": "Please enter Firstname",
    "cpm.lm.line.validation.lastName": " Please enter LastName",
    "cpm.lm.line.validation.email": " Please enter valid email",
    "cpm.lm.line.validation.phone": " Please check Phone number",
    "cpm.lm.line.validation.userId": " Please enter userId",
    "cpm.lm.line.validation.managerId": " Please enter managerId",
    "cpm.lm.line.validation.username": "Please enter username",
    "cpm.lm.line.validation.password": "Please enter password (7-14)",
    "cpm.lm.line.validation.confPassword": "Please check confirmPasssword",
    "cpm.lm.line.validation.plants": "Please select Plant",
    "cpm.lm.line.validation.units": " Please select unit",
    "cpm.lm.line.validation.Roles": "Please select role Or Group",
    "cpm.lm.line.adduser": "Add User",
    "cpm.lm.line.edituser": "Edit User",


    //################## Line Planning ###################//

    //---------------- Pivot Grid -----------------------//
    "cpm.lm.line.plan.tt.mach.avail": "Tt. Line Capacity (M/C)",
    "cpm.lm.line.plan.line.capacity.md": "Tt. Line Capacity (M/D)",
    "cpm.lm.line.plan.line.capacity.hrs": "Tt. Line Capacity (HR'S)",
    "cpm.lm.line.plan.line.booked.md": "Tt. Line Capacity Booked (M/D)",
    "cpm.lm.line.plan.line.booked.hrs": "Tt. Line Capacity Booked (HR'S)",
    "cpm.lm.line.plan.line.open.md": "Tt. Line Capacity Open (M/D)",
    "cpm.lm.line.plan.line.open.hrs": "Tt. Line Capacity Open (HR'S)",
    // "cpm.lm.line.plan.open.cap.tt.md": "Opened Capacity Tailor(M/D)",

    //----------------Map-legend------------------------//
    "cpm.lm.line.plan.map.legend.Editable": "Avl. Capacity",
    "cpm.lm.line.plan.map.legend.Overloaded": "Cap. Overloaded",
    "cpm.lm.line.plan.map.legend.Freeze": "Freeze",

    //----------------Fields----------------------------//
    "cpm.lm.line.plan.header.lineName": "Line Name",
    "cpm.lm.line.plan.schedulerHeader.eventName": "Selected Event",
    "cpm.lm.line.plan.field.plannedQty": "Remaining Qty. to Plan",
    "cpm.lm.line.plan.field.machineAllocated": "No. of Machine Allocated",
    "cpm.lm.line.plan.field.sam": "SAM (In minutes)",
    "cpm.lm.line.plan.field.actualSam": "Actual SAM (In minutes)",
    "cpm.lm.line.plan.field.startDate": "Start Date",
    "cpm.lm.line.plan.field.endDate": "End Date",
    "cpm.lm.line.plan.field.lineDesc": "Line Description",
    "cpm.lm.line.plan.schedulerHeader.selected.eventName": "No event Selected !!",
    "cpm.lm.line.plan.field.onApply": "Apply",


    //##################Line-Scheduling##################//

    "cpm.lm.line.schedule.header": "Scheduled Line",
    "cpm.lm.line.schedule.header.main.lineItem": "Selected Line Item to schedule",
    "cpm.lm.line.schedule.header.main.scheduled": "Selected scheduled Line Item",

    //-------------------Filter----------------------//
    "cpm.lm.line.schedule.filter.prodLine": "Select Production Line",
    "cpm.lm.line.schedule.filter.plant": "Select Plant Name",
    "cpm.lm.line.schedule.filter.unit": "Select Unit Name",



    //----------------Fields-----------------------// 
    "cpm.lm.line.schedule.sidebar.field.transferLine": "Transfer Line",
    "cpm.lm.line.schedule.sidebar.field.transferTo": "Transfer LineItem",
    "cpm.lm.line.schedule.field.noOfDays": "No Of Days",
    "cpm.lm.line.schedule.field.noOfMachineToUse": "No of pl Mac",
    "cpm.lm.line.schedule.field.totalNoOfMachineToUse": "Total No of Mac",
    "cpm.lm.line.schedule.field.scheduleStartDateTime": "Scheduled Start Date",
    "cpm.lm.line.schedule.field.scheduleEndDateTime": "Scheduled End Date",
    "cpm.lm.line.schedule.field.sam": "SAM",
    "cpm.lm.line.schedule.sidebar.field.saidNo": "Said No",
    "cpm.lm.line.schedule.sidebar.field.lineItemNo": "Line Item No",
    "cpm.lm.line.schedule.sidebar.field.buyerName": "Buyer Name",
    "cpm.lm.line.schedule.sidebar.field.merchantName": "Merchant Name",
    "cpm.lm.line.schedule.sidebar.field.totalQty": "Total Qty",
    "cpm.lm.line.schedule.sidebar.field.buyerQty": "Total Buyer Po Qty",
    "cpm.lm.line.schedule.sidebar.field.buyerQtyRemaining":"Left po Qty",
    "cpm.lm.line.schedule.sidebar.field.allocatedQty.": "Planned qty.",
    "cpm.lm.line.schedule.sidebar.field.plannedQtyRemaining":"Left Planned Qty",

    "cpm.lm.line.schedule.sidebar.field.category": "Line Category",
    "cpm.lm.line.schedule.sidebar.field.apm": "Apm",
    "cpm.lm.line.schedule.sidebar.field.scheduleQty": "Schedule Qty",
    "cpm.lm.line.schedule.sidebar.field.buyerPo": "Buyer Po",
    "cpm.lm.line.schedule.sidebar.field.shipToDate": "Ship To Date",
    "cpm.lm.line.schedule.sidebar.field.transferLineItem": "Transfer LineItem",
    "cpm.lm.line.schedule.sidebar.field.transferToUnit": "Transfer To Unit",
    "cpm.lm.line.schedule.sidebar.field.schedule": "Schedule",
    "cpm.lm.line.schedule.sidebar.field.unSchedule": "Delete Schedule",
    "cpm.lm.line.schedule.sidebar.field.edit": "Save",

    // ---------------Error-Messages--------------- //
    "error.code.400": "Something went wrong",
    "error.code.420": "Could not create a new line, because <b>same line name already</b> exist !!",
    "error.code.421": "Line is already planned !! You can't delete this line.",
    "error.code.401": "Access Denied !!",
    "error.code.422": "You cannot Delete this planning ! Line Item is already Scheduled !!",
    "error.code.423": "You cannot Unschedule it ! Line Item is already Loaded !!",
    "error.code.424": "Buyer po is not scheduled !!",
    "error.code.425": "Line failed to transfer !!",
    "error.code.426": "Material Quantity not available !!",
    "error.code.427": "Dependent Material need to be free !!",
    "error.code.428": "Material not found in Virtual Store !!",
    "error.code.429": "Failed to Edit line load !!",
    "error.code.430": "Failed to Delete line load !!",
    "error.code.431": "Hour added more than the actual Line Hours !!",
    "error.code.432": "Total hours could not be adjusted in a day !!",
    "error.code.433": "Production Order total loaded qty should not be more than total qty. !!",
    "error.code.434": "Total Day Qty could not be adjusted in Hours !!",
    "error.code.435": "Already Load is available on given dates !!",
    "error.code.436": "Quantity is neither loaded nor available in SAP Store !!",
    "error.code.437": "Quantity is neither loaded nor available in Virtual Store !!",
    "error.code.438": "Authetication is failed On Server Side !!",
    "error.code.439": "Encoding is not Supported !!",
    "error.code.440": "User not found for the given username or email !!",


    //#################### Line-Loading ####################//
    "cpm.lm.line.load.filter.plant": "Select Plant Name",
    "cpm.lm.line.load.filter.unit": "Select Unit Name",
    "cpm.lm.line.load.filter.prodline": "Select Production Line",
    "cpm.lm.line.load.filter.date": "Select Date",
    "cpm.lm.line.load.filter.buyerpo": "Select BuyerPo",
    "cpm.lm.line.load.filter.lineitem": "Select Line Item",
    "cpm.lm.line.load.filter.said": "Select Said No",
    "cpm.lm.line.load.filter.po": "PO",
    "cpm.lm.line.load.filter.poType": "PO Type",
    "cpm.lm.line.load.filter.grn": "GRN",
    "cpm.lm.line.load.filter.bt": "BT",
    "cpm.lm.line.load.filter.department": "Department",
    "cpm.lm.line.load.filter.grnDateRang": "GRN Date Range", 
    "cpm.lm.line.load.filter.decisionWise": "Decision Wise",
    
 


    "cpm.lm.line.load.popup.addlineload": "Add Line Load",
    "cpm.lm.line.load.popup.editlineload": "Edit Line Load",
    "cpm.lm.line.load.popup.prodorder": "Production Order",
    "cpm.lm.line.load.popup.wip": "WIP",
    "cpm.lm.line.load.popup.wipTarget": "WIP Target (In minutes)",
    "cpm.lm.line.load.popup.prodconf": "Prod Conf",
    "cpm.lm.line.load.popup.process": "Process Name",
    "cpm.lm.line.load.popup.startdate": "Start Date",
    "cpm.lm.line.load.popup.enddate": "End Date",
    "cpm.lm.line.load.popup.operators": "Operators",
    "cpm.lm.line.load.popup.datapoint": "DataPoint",
    "cpm.lm.line.load.popup.status": "Status",
    "cpm.lm.line.load.popup.transferLoadQty": "Transfer Load Qty",
    "cpm.lm.line.load.popup.transferLoad": "Transfer Load",
    "cpm.lm.line.load.popup.transferLine": "Transfer Line",
    "cpm.lm.line.load.popup.bomSeq": "Bom Seq",

    "cpm.lm.line.load.popup.MaxLoadQTY": "Max Load QTY",

    "cpm.lm.line.load.popup.loadqty": "Load Qty",
    "cpm.lm.line.load.popup.totalQty": "Total Qty",
    "cpm.lm.line.load.popup.workinghrs": "Working Hrs",
    "cpm.lm.line.load.popup.skilledworker": "Skilled Worker",
    "cpm.lm.line.load.popup.unskilledworker": "Unskilled Worker",
    "cpm.lm.line.load.popup.labour": "Labour",
    "cpm.lm.line.load.popup.bulkuploadqty": "Bulk upload(Qty)",
    "cpm.lm.line.load.popup.yellowceiling": "Yellow Ceiling",
    "cpm.lm.line.load.popup.fm": "FM",
    "cpm.lm.line.load.popup.apm": "APM",
    "cpm.lm.line.load.popup.qa": "qa",
    "cpm.lm.line.load.popup.supervisor": "Supervisor",
    "cpm.lm.line.load.popup.sam": "SAM",
    "cpm.lm.line.load.popup.line": "Line",
    "cpm.lm.line.load.popup.materialId": "Material Id",
    "cpm.lm.line.load.popup.materialDesc": "Material Description",

    //--------------Validation Message-----------------//
    "cpm.lm.line.validation.lineprocessName": "Please enter Line Process!!",
    "cpm.lm.line.validation.lineprocessDis": "Please enter Line Process Description!!",

    "cpm.lm.line.validation.keyName": "Please enter Key Name!!",
    "cpm.lm.line.validation.keyvalue": "Please enter Key Value!!"
  }
};

export default de;
