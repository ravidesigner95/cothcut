
export class UserProfileModel {
    constructor(id, firstName, lastName, email, phone, userId, managerId, password, confirmPassword, plantName, unitIds, plantId, orderFilters, groups, roles, description, username, active, mr, apm, pe, qa) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.radiantUserId = userId;
        this.managerId = managerId;
        this.password = password;
        this.confirmPassword = confirmPassword;
        this.plantName = plantName;
        this.units = unitIds; 
        this.plants = plantId;
        this.orderFilters = orderFilters
        this.groups = groups;
        this.roles = roles;
        this.description = description;
        this.username = username;
        this.active = active;
        this.mr = mr;
        this.apm = apm;
        this.pe = pe;
        this.qa = qa;
    }

    id = null;
    firstName = null;
    lastName = null;
    email = null;
    phone = null;
    radiantUserId = null;
    managerId = null;
    password = null;
    confirmPassword = null;
    plantName = null;
    units = [];
    plants = [];
    orderFilters = [];
    groups = [];
    roles = [];
    description = null;
    username = null;
    active = null;
    mr = [];
    apm = [];
    pe = [];
    qa = [];

}

export default UserProfileModel;