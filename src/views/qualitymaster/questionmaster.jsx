import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { GridComponent, ColumnsDirective, ColumnDirective, Filter, Inject, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, ExcelExport, ForeignKey, Reorder } from '@syncfusion/ej2-react-grids';
// import {
// //   GridComponent, ColumnsDirective, ColumnDirective, Resize, Inject, Sort, VirtualScroll,
// //   Filter, Toolbar, Page, ExcelExport, CommandColumn, ForeignKey, Edit
// // } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent, MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { PropertyPane } from '../../assets/data/purchaseOrder';
import { SwitchComponent, CheckBoxComponent } from '@syncfusion/ej2-react-buttons';
import { rippleMouseHandler, ChangeEventArgs } from '@syncfusion/ej2-buttons';
import { data } from '../../assets/data/data';
import { RadioButtonComponent, ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { sampleData } from '../../assets/data/inspection';
import { SampleBase } from '../../assets/data/sample-base';
import { rightToLeft, rippleEffect } from '@syncfusion/ej2-base';
import QualityMasterService from './QualityMasterService';
import { QuestionModel } from './QuestionModel';
import { AppConfig } from '../../appConfig/appConfig';
// import { orderDetails } from '../../assets/data/data';
// import { sampleBase64pdf } from "../../../sampleBase64pdf";

class questionmasterComponent extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    this.QualityMasterService = new QualityMasterService();
    this.commands = [
      // { type: 'Edit Template', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
      { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
      // { type: 'Save', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
      // { type: 'Print', buttonOption: { iconCss: 'e-icons e-print', cssClass: 'e-flat' } },
      // { type: 'Download', buttonOption: { iconCss: 'e-icons e-download', cssClass: 'e-flat' } },
      { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },
      // { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } }
    ];

    this.orderDataSource = [
      {
        Questions: 'Does the product size meet as per approved sample/technical specification.',
        Category: 'Stiching',
        AnswerType: 'Yes',
        ViewAction: '2',
        Status: 'Enable',
        Action: '',
      },
      {

        Questions: 'Does the product size meet as per approved sample/technical specification.',
        Category: 'Stiching',
        AnswerType: 'Yes',
        ViewAction: '1',
        Status: 'Enable',
        Action: '',
      },
      {

        Questions: 'Does the product size meet as per approved sample/technical specification.',
        Category: 'Stiching',
        AnswerType: 'Yes',
        Action: '3',
        Status: 'Enable',
        Action: '5',
      },
    ];

    this.state = {
      quesCategory: [],
      selectedQuesCategory: [],
      getAllQuTitle:['Yes/No/NA','Pass/Fail','Free Text'],
      selectedTabIndex:0,
      quesAns: [],
      quesStatus: 'Disable',
      quesName: "",
      selectedQuesId: null,
      showStatus: false,
      showDialog: false,
      optionIds: [],
      actionIds: [],
      quesIds: null,
      categoryIds: [],
      toasterPosition: { X: 'Right', Y: 'Bottom' },

      orderDataSource: [
        {
          Questions: 'Does the product size meet as per approved sample/technical specification.',
          Category: 'Stiching',
          AnswerType: 'Yes',
          ViewAction: '2',
          Status: 'Enable',
          Action: '',
        },
        {

          Questions: 'Does the product size meet as per approved sample/technical specification.',
          Category: 'Stiching',
          AnswerType: 'Yes',
          ViewAction: '1',
          Status: 'Enable',
          Action: '',
        },
        {

          Questions: 'Does the product size meet as per approved sample/technical specification.',
          Category: 'Stiching',
          AnswerType: 'Yes',
          ViewAction: '3',
          Status: 'Enable',
          Action: '',
        },
      ]
    };
    this.buttonRef = element => {
      this.buttonEle = element;
    };
    this.buttons = [{
      click: () => {
        this.onSaveClick();
        
      },
      buttonModel: {
        content: 'Save',
        isPrimary: true
      }
    },
    {
      click: () => {
        this.onClickCancel();

      },
      buttonModel: {
        content: 'Cancel',
        cssClass: 'e-secondary'
      }
    }
    ];
    this.animationSettings = { effect: 'None' };
    this.temp = 'categories';
    this.catgrysearchData = data[this.temp];
    // this.newsearchData = data['QuestionCategory'];
    this.toolbarOptions = ['Add', 'Edit', 'Delete', 'Update', 'Cancel'];
    // this.toolbarOptionsedit = ['Add', 'Delete', 'Cancel'];
    // this.editSettingsForAQLLeftPanel = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Bottom' };
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top' };
    this.editparams = { params: { popupHeight: '500px' } };
    this.validationRule = { required: true };
    this.fields = { text: 'Name', value: 'Code' };
    this.orderidRules = { required: true, number: true };
    this.pageSettings = { pageCount: 5 };
    this.format = { type: 'dateTime', format: 'M/d/y hh:mm a' };
    this.droplist = [
      { text: 'Top', value: 'Top' },
      { text: 'Bottom', value: 'Bottom' }
    ];
  
  }

  componentDidMount() {
    this.setState({
      orderDataSource: this.orderDataSource
    });
    this.getQuesCategoryList();
    this.getQuesAnsList();
    this.getQuesList();
  }

  ////////////////////////////////////////////////////////////////////////////API CALLS///////////////////////////////////////////////////////////////////////////////
  async getQuesCategoryList() {
    let response = (await this.QualityMasterService.getQuesCategoryList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ quesCategory: response.data.data }, () => {
      })
  }

  async getQuesAnsList() {
    let response = (await this.QualityMasterService.getQuesAnsList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ quesAns: response.data.data }, () => {
      })

    this.anstemplate(0);
  }

  async getQuesList() {
    global.showLoader();
    let response = (await this.QualityMasterService.getQuesList())
    global.hideLoader();
    if ((response != undefined) && (response.data.status == "success")) {
      let quesList = [];
      response.data.data.map(quesInfo => {
        let count = 0;
        let categoryNames = quesInfo.questionCategoryDTOS.map(item => item.quesCategory).join(', ');
        let ansType = quesInfo.qualityQuesAnsGroupDTO.filter(function (item) {
          return item.id == quesInfo.quesGroupId;
        })
        if (ansType.length > 0) {
          for (let i = 0; i<ansType[0].quesAnsGrpOption.length; i++) {
            for (let j = 0;j<ansType[0].quesAnsGrpOption[i].quesAnsGrpOptionAction.length; j++) {
              if (ansType[0].quesAnsGrpOption[i].quesAnsGrpOptionAction[j] !=undefined && ansType[0].quesAnsGrpOption[i].quesAnsGrpOptionAction[j].isChecked) {
                count = count + 1;
              }
            }
          }
        }

        quesList.push(new QuestionModel(quesInfo.id, quesInfo.quesType, quesInfo.quesText, quesInfo.prefilledIndicator, quesInfo.mappingField, quesInfo.status, quesInfo.isBold,
          quesInfo.categoryId, quesInfo.quesGroupId, quesInfo.quesGroupOption, quesInfo.quesGroupOptionAction,
          quesInfo.qualityQuesAnsGroupDTO, quesInfo.questionCategoryDTOS, categoryNames, ansType.length != 0 ? ansType[0].ansGrpName : "", count));
      })
      this.setState({ quesList, selectedQuesCategory: [] })
    }else{
      global.hideLoader();
    }


  }

  async saveQues(newQues) {
    let response = (await this.QualityMasterService.saveQues(newQues))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster("Question  saved successfully!!", "Success"));
      this.getQuesAnsList();
      this.getQuesList();
      this.setState({ansListOption1:[], ansListOption2:[],selectedTabIndex:0,quesIds:null})
      this.dialogInstance.hide();


    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }

  async deleteQues(quesId) {
    let response = (await this.QualityMasterService.deleteQues(quesId))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster("Question deleted successfully!!", "Success"));
      this.getQuesList();
    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }

  actionBegin(args) {
    if (args.requestType === 'save') {
      if (this.gridInstance.pageSettings.currentPage !== 1 && this.gridInstance.editSettings.newRowPosition === 'Top') {
        args.index = (this.gridInstance.pageSettings.currentPage * this.gridInstance.pageSettings.pageSize) - this.gridInstance.pageSettings.pageSize;
      }
      else if (this.gridInstance.editSettings.newRowPosition === 'Bottom') {
        args.index = (this.gridInstance.pageSettings.currentPage * this.gridInstance.pageSettings.pageSize) - 1;
      }
    }
  }

  anstemplate = (args) => {
    const ansListOption1 = []
    const ansListOption2 = []
    // const selectansid = this.state.quesAns[0].id

    const optionDetails = [];
    for (const [index, ques] of this.state.quesAns.entries()) {
      for (const [index, value] of ques.quesAnsGrpOption.entries()) {
        if (ques.ansGrpName.includes("yes")) {
          ansListOption1.push(
            <Col key={index} className='ml2'>
              <h6>{value.optionName}</h6>
              {value.quesAnsGrpOptionAction.map((item,index) => <div key={index} className="col-md-4">
                <CheckBoxComponent checked={item.isChecked} label={item.actionName} ref={(scope) => { this.checkboxObj1 = scope; }} change={this.onChange.bind(this, item.actionName, item.id, ques.id, value.id, value, ques)} ></CheckBoxComponent>
              </div>)}
            </Col>)
        }
        else {
          ansListOption2.push(
            <Col key={index} className='ml2'>
              <h6>{value.optionName}</h6>
              {value.quesAnsGrpOptionAction.map((item,index) => <div key={index} className="col-md-4">
                <CheckBoxComponent label={item.actionName} checked={item.isChecked} ref={(scope) => { this.checkboxObj1 = scope; }} change={this.onChange.bind(this, item.actionName, item.id, ques.id, value.id, value, ques)} ></CheckBoxComponent>
              </div>)}
            </Col>)
        }
      }
    }

    this.setState({ ansListOption1, ansListOption2 })
  }

  onChangeQuesName = (event) => {
    this.setState({ quesName: event.value.trim() })
  }

  onChangeStatus = (event) => {
    if (event.checked) {
      this.setState({ quesStatus: 'Enable' })
    }
    else {
      this.setState({ quesStatus: 'Disable' })

    }
  }
  onChangeFilterMultiSelect = (event) => {
    this.setState({ categoryIds: event.value })
  }



  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }
  tabsView(selectedAns) {
    if (selectedAns != '') {
      for (const [index, value] of this.state.quesAns.entries()) {
        if (value.ansGrpName == selectedAns) {
          this.anstemplate(index);
        }
      }

    }
    else {
      this.setState({ tabContent: null })
    }
  }

  onSaveClick() {
    let that = this;
    const { selectedQuesId, quesName, quesStatus, quesIds, optionIds, actionIds, categoryIds } = that.state;
    if (quesName != "" && categoryIds.length > 0) {
      let quesModel = new QuestionModel(selectedQuesId, 'question', quesName, "", "", quesStatus, true, categoryIds, quesIds, optionIds, actionIds, null, null)
      this.saveQues(quesModel);
      
    }
    else {
      this.toastObj.show(this.createToaster(quesName == "" ? "Please enter question" : categoryIds.length == 0 ? "Please select category" : "", "Information"));

    }
  }

  onClickCancel() {
    this.setState({ selectedQuesId: null, quesName: "", quesStatus: "Disable", ansListOption1:[], ansListOption2:[], quesIds: null, optionIds: [], actionIds: [], categoryIds: [], selectedQuesCategory: [],selectedTabIndex:0 },()=> this.dialogInstance.hide());
  }
  ddChange() {
    this.gridInstance.editSettings.newRowPosition = this.dropDownInstance.value;
  }

  linkClicked = (args)  => {
    if (args == 'new') {
      this.getQuesAnsList();
      this.setState({
        selectedQuesCategory: [], selectedQuesId: null, quesName: "", quesStatus: "Enable", quesIds:this.state.quesAns[0].id,
        optionIds: [], actionIds: [], categoryIds: [], showStatus: false
      });

      this.dialogInstance.show();
    }
    this.dialogInstance.show();

  }


  dialogClose() {
    // this.setState({
    //   showDialog: false,
      //  orderDataSource: this.orderDataSource
    //   ansListOption1:[],
    //   ansListOption2:[],
    //   quesIds:null,
    //   selectedTabIndex:0
    // });
    this.setState({ selectedQuesId: null, quesName: "", quesStatus: "Disable", ansListOption1:[], ansListOption2:[], quesIds: null, optionIds: [], actionIds: [], categoryIds: [], selectedQuesCategory: [],quesIds:null,selectedTabIndex:0 });
    // this.buttonEle.style.display = "block";
  }
  dialogOpen() {
    // this.buttonEle.style.display = "none";
  }

  // function to handle the CheckBox change event
  onChange(actionName, actionId, quesId, optionId, value, quetion, args) {

    const quetioncopy = [...this.state.quesAns];
    const selectquestion = quetioncopy.filter(item=> item.id == quesId ? item : null)
    const selectquesOptionn = selectquestion[0].quesAnsGrpOption.filter(item => item.id == optionId ? item : null )
    selectquesOptionn[0].quesAnsGrpOptionAction.filter(item => item.id == actionId ? item.isChecked == false ? item.isChecked = true : item.isChecked = false : null )
    this.setState({quesAns:quetioncopy})

    if (args.checked) {
      this.setState({actionIds:[...this.state.actionIds,actionId]});
      if(!this.state.optionIds.includes(optionId)){
          this.setState({optionIds:[...this.state.optionIds,optionId]});
        }
      // this.state.actionIds.push(actionId);
      // this.state.optionIds.push(optionId);
    }
    else {
      const afterremoveaction = [...this.state.actionIds].filter(item =>(
        item != actionId
      ))
      this.setState({actionIds:afterremoveaction})
      if(!this.ckeckactionoption(optionId,quesId)){
        const afterremoveoption = [...this.state.optionIds].filter(item =>(
          item != optionId
        ))

        this.setState({optionIds:afterremoveoption})
      }
      
    }
    // this.setState({quesIds:quesId})
    // console.log(args)
    // this.checkboxObj1.label = 'CheckBox: ' + args.checked;
  }

  ckeckactionoption = (getoptionId,getquesId)=> {
    const selectquestion = this.state.quesAns.filter(item=> item.id == getquesId ? item : null)
    const getquop = selectquestion[0].quesAnsGrpOption.filter(item=> item.id == getoptionId ? item : null)
    if(getquop[0].quesAnsGrpOptionAction[0].isChecked != true && getquop[0].quesAnsGrpOptionAction[1].isChecked != true){
      return false
    }else{
      return true
    }
  }
  rendereComplete() {
    // To enable ripple in checkbox/radio type ButtonGroup.
    let buttons = document.querySelectorAll('label.e-btn');
    let button;
    for (let i = 0; i < buttons.length; i++) {
      button = buttons.item(i);
      rippleEffect(button, { selector: '.e-btn' });
    }

  }

  commandsClicked = (e) => {
    let that = this
    let type = e.commandColumn.type;

    if (type == "Edit") {
      this.setState({
        selectedQuesId: e.rowData.id, quesName: e.rowData.quesText, quesStatus: e.rowData.status, quesAns: e.rowData.qualityQuesAnsGroupDTO,
        selectedQuesCategory: e.rowData.categoryId, categoryIds: e.rowData.categoryId, quesIds: e.rowData.quesGroupId, optionIds: e.rowData.quesGroupOption, actionIds: e.rowData.quesGroupOptionAction, showStatus: true
      })
      that.state.quesAns.forEach((el,i)=> el.id == e.rowData.quesGroupId && that.setState({selectedTabIndex:i}))
      this.anstemplate();
      // if(args =='new')
      // {
      this.setState({
        orderDataSource: [],
      });
      // }
      // this.setState({

      // });
      // console.log("e", e);
      this.dialogInstance.show()
    }
    else if (type == "Save") {

    }
    else if (type == "Delete") {
        this.deleteQues(e.rowData.id)
    }
    // console.log("this.gridInstance", this.gridInstance);

  }

  selecteTabFun = args=>{
    this.state.quesAns.filter((item,i) => i == args.selectedIndex && this.setState({quesIds:item.id}))
    this.setState({selectedTabIndex:args.selectedIndex})

  }
 

  render() {

    return (<div className='control-pane' id='targetElementQuest'>
      <div className='control-section col-lg-12 defaultDialogComponent dialog-target'>
        {/* <span><i class="fa fa-pencil pt-1" onClick={ this.linkClicked.bind(this,'edit')}></i></span> */}
        <Button color="primary" className='addbtn' size="sm" onClick={this.linkClicked.bind(this, 'new')} >Add</Button>
        <Row >
          <Col sm='12'>
            <GridComponent
              id="overviewgrid"
              enableHover={false}
              allowFiltering={false}
              allowSorting={true}
              allowSelection={true}
              allowResizing={true}
              rowHeight={25}
              gridLines='Both'
              // toolbar={this.toolbarOptions}
              allowPaging={true}
              allowTextWrap={true}
              allowRowDragAndDrop={false}
              height={AppConfig.gridHeight}
              filterSettings={AppConfig.FilterSettingsModel}
              // editSettings={AppConfig.editSettings}
              pageSettings={AppConfig.pageSettings}
              dataSource={this.state.quesList}
              ref={(scope) => this.gridInstance = scope}
              commandClick={this.commandsClicked}>
              <ColumnsDirective>
                <ColumnDirective field='quesText' headerText='Questions' textAlign='left' width='380' validationRules={this.Questions}></ColumnDirective>
                <ColumnDirective field='categoryName' headerText='Category' textAlign='center' width='100' validationRules={this.Category}></ColumnDirective>
                <ColumnDirective field='ansType' headerText='Answer Type' textAlign='center' width='100' validationRules={this.AnswerType}></ColumnDirective>
                <ColumnDirective field='actionCount' headerText='View Action' textAlign='center' width='100' validationRules={this.Action}></ColumnDirective>
                <ColumnDirective field='status' headerText='Status' textAlign='center' width='100' validationRules={this.Status}></ColumnDirective>
                <ColumnDirective field='Action' headerText='Action' commands={this.commands} width='100' />
              </ColumnsDirective>
              <Inject services={[Filter, Edit, CommandColumn, ForeignKey, Toolbar, ExcelExport, VirtualScroll, Sort, Resize, Toolbar, Page]} />
            </GridComponent>

          </Col>
        </Row>
        <DialogComponent
          id="defaultDialog"
          showCloseIcon={true}
          animationSettings={this.animationSettings}
          visible={this.state.showDialog} width={'96%'}
          ref={dialog => this.dialogInstance = dialog}
          //target={'#targetElementQuest'} 
          isModal={true}
          header='Question Details'
          buttons={this.buttons}
          // open={this.dialogOpen.bind(this)}
          close={this.dialogClose.bind(this)}
          >
          <Row>
            <Col sm='3'>
              <TextBoxComponent placeholder="Enter Question" value={this.state.quesName} change={this.onChangeQuesName} floatLabelType="Auto" />
            </Col>
            <Col sm='3'>
              <TextBoxComponent placeholder="Type" value="question" disabled floatLabelType="Auto" />
            </Col>
            <Col sm='3'>
              <MultiSelectComponent id="checkbox" ref={(scope) => { this.mulObj = scope; }} value={this.state.selectedQuesCategory} dataSource={this.state.quesCategory} fields={{ text: 'quesCategory', value: 'id' }} placeholder="Select Question Category" floatLabelType="Auto" mode="CheckBox"
                change={this.onChangeFilterMultiSelect} showSelectAll={true} showDropDownIcon={true} filterBarPlaceholder="Search countries" popupHeight="350px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent>
            </Col>
            <Col sm='3' className='status-switch'>
              <div hidden={!this.state.showStatus}>
                Status <label htmlFor="unchecked"></label> <SwitchComponent id="unchecked" checked={this.state.quesStatus == "Enable" ? true : false} change={this.onChangeStatus}></SwitchComponent>
              </div>
            </Col>
          </Row>

          <TabComponent id='defaultTab' selectedItem={this.state.selectedTabIndex} selected={args => this.selecteTabFun(args)}>
            <div className="e-tab-header" >
              <div>Yes/No/NA</div>
              <div>Pass/Fail</div>
              <div>Free Text</div>
            </div>
            <div className="e-content mt-2">
              <div>
                <div className='bggrey'>
                  <Row>
                    {this.state.ansListOption1}
                  </Row>
                </div>
              </div>
              <div>
                <div className='bggrey'>
                  <Row>
                    {this.state.ansListOption2}
                  </Row>
                </div>
              </div>
              <div>
                <div className='bggrey'>
                 
                </div>
              </div>
            </div>
          </TabComponent>

        </DialogComponent>
      </div>
      <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
    </div>);

  }
}

export default questionmasterComponent;
