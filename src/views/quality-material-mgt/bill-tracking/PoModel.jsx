export class PoModel {
    constructor(poDeliverDate,vendorName,totalPoQty,status,btPODetailDTOS,score) {
        this.poDeliverDate = poDeliverDate;
        this.vendorName = vendorName;
        this.totalPoQty = totalPoQty;
        this.status = status;
        this.btPODetailDTOS=btPODetailDTOS;
        this.score=score;
        
    }
    poDeliverDate = null;
    vendorName = null;
    totalPoQty = null;
    status = null;
    btPODetailDTOS=null;
    score=null;
}

export default PoModel; 