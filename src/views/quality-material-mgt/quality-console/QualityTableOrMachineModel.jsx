export class QualityTableOrMachineModel {
    constructor(id, ticketNo,tableOrMachineDesc,tableOrMachineCode) {
        this.id = id;
        this.ticketNo = ticketNo;
        this.tableOrMachineDesc = tableOrMachineDesc;
        this.tableOrMachineCode = tableOrMachineCode;

    }
    id = null;
    ticketNo = null;
    tableOrMachineDesc = null;
    tableOrMachineCode = null;

}

export default QualityTableOrMachineModel; 