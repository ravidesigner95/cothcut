export class UserProfileFiltersModel{
    constructor(id,assignedUserIds,assignedUserName,roleName){
     this.id=id;
      this.assignedUserIds = assignedUserIds
      this.assignedUserName = assignedUserName
      this.roleName = roleName

    }
    id=null;
    assignedUserIds = [];
    assignedUserName = [];
    roleName = '';
}

export default UserProfileFiltersModel; 