export class BtHistoryModel {
    constructor(date, comments, btHistoryDocDTOList, btProcessName,userId,userName,btProcessId,status,btNumber) {
        this.date = date;
        this.comments = comments;
        this.btHistoryDocDTOList = btHistoryDocDTOList;
        this.btProcessName = btProcessName;
        this.userId = userId;
        this.userName = userName;
        this.btProcessId = btProcessId;
        this.status = status;
        this.btNumber =btNumber;
    }
    date = null;
    comments = null;
    btHistoryDocDTOList = [];
    btProcessName = null;
    userId=null;
    userName=null;
    btProcessId = null;
    status=null;
    btNumber= null;
}

export default BtHistoryModel; 
