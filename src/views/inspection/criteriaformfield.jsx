import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups'; 
import { GridComponent, ColumnsDirective, ColumnDirective, Filter, Inject, RowDD, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, Reorder, Search } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { sampleData } from '../../assets/data/inspection'
import { data } from '../../assets/data/data';
import { Query } from '@syncfusion/ej2-data';
// import { sampleBase64pdf } from "../../../sampleBase64pdf";
class CriteriaformFieldComponent extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    this.countries = sampleData;
    this.state = {
      filterBox: true,
      divHidden: null,
      ddCountryHidden:false,
      ddFieldHidden:false,
      listfield: { dataSource: this.countries, id: 'id', text: 'name', parentID: 'pid', hasChildren: 'hasChild' },

    }
    this.toolbarOptions = ['Add', 'Edit', 'Delete', 'Update', 'Cancel', 'Search'];
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top' }
    this.formData = [
      {
        Code: 'STC020',
        Description: 'Bunched treads, slightly affecting appreance',
        Level: 'Minor',
      },
      {

        Code: 'STC021',
        Description: 'Stitching not under the marks/notches',
        Level: 'Major',
      }, 
    ];
    //define the filtering data
    this.temp = 'categories'; 
    this.searchData = data[this.temp];
    this.newsearchData = data['Fielddrpdown'];
    // maps the appropriate column to fields property
    this.fields = { text: 'Name', value: 'Code', };
    // filtering event handler to filter a Country
    this.onFiltering = (e) => {
      let query = new Query();
      //frame the query based on search string with filter type.
      query = (e.text !== '') ? query.where('Name', 'startswith', e.text, true) : query;
      //pass the filter data source, filter query to updateData method.
      e.updateData(this.searchData, query);
    };
  }
  
  filterBoxOpen = () => {
    this.setState({
      filterBox: !this.state.filterBox
    });

  }
  prefilledtemplate(){ 
    return (<div> <RadioButtonComponent label=' ' name='payment' value="netbanking"></RadioButtonComponent></div> );
}
manualtemplate(){ 
  return (<div> <RadioButtonComponent label=' ' name='payment' value="netbanking"></RadioButtonComponent></div> );
}  
categorytemplate(){ 
  return (<div>  <DropDownListComponent placeholder="Select a Field" popupHeight="220px" /></div> );
} 
inputtemplate(){ 
  return (<div>  <TextBoxComponent placeholder="" floatLabelType="Auto" /></div> );
}  
  onPaymentSelectionChange = (args) =>{
  
    let hideDropdowns = false;
    if(args.value == 'Manual')
    {
      hideDropdowns = true;
    }

    this.setState({
      ddcategoryHidden: hideDropdowns,
      ddFieldHidden:hideDropdowns
    });
  }

  content1 = () => {
    return (<div>
      <Row>
        <Col>
          <span className='margin-top16'>
            <Button color="info" size="sm" onClick="">Yes / No / NA</Button>&nbsp;
               <Button className="brdblue" size="sm" onClick="">Pass / Fail</Button>
            <Button className="brdblue" size="sm" onClick="">Free Text</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }
  content2 = () => {
    return (<div>
      <Row>
     
        <Col sm='3'>
          <TextBoxComponent placeholder="Field Name" floatLabelType="Auto" />
        </Col>
        <Col sm='2'>
          <RadioButtonComponent checked={true} label='Prefilled' name='payment' value="Prefilled" change={this.onPaymentSelectionChange}></RadioButtonComponent>
        </Col>
        <Col sm='2'>
          <RadioButtonComponent checked={false} label='Manual' name='payment' value="Manual" change={this.onPaymentSelectionChange}></RadioButtonComponent>
        </Col>
        <Col sm='2'>
          <div id='filtering' hidden={this.state.ddcategoryHidden}>
            <DropDownListComponent id="category" ref={(dropdownlist) => { this.listObj = dropdownlist; }} dataSource={this.searchData} filtering={this.onFiltering.bind(this)} filterBarPlaceholder='Search a category' allowFiltering={true} fields={this.fields} placeholder="Select a Category" popupHeight="220px" />
          </div>
        </Col>
        <Col sm='2'>
        <div id='filtering'>
            <DropDownListComponent id="category" ref={(dropdownlist) => { this.listObj = dropdownlist; }} dataSource={this.newsearchData} filtering={this.onFiltering.bind(this)} filterBarPlaceholder='Search a Field' allowFiltering={true} fields={this.fields} placeholder="Select a Field" popupHeight="220px" />
          </div>
        </Col>
      </Row>
    <Row>
        <Col>
          <span className='float-right margin-top16'>
            <Button color="primary" size="sm" onClick="">Add</Button>&nbsp;
               <Button className="btncancel" size="sm" onClick="">Cancel</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }
  content3 = () => {
    return (<div>
      <Row>
        <Col>
          <span className='margin-top16'>
            <Button color="info" size="sm" onClick="">Yes / No / NA</Button>&nbsp;
               <Button className="brdblue" size="sm" onClick="">Pass / Fail</Button>
            <Button className="brdblue" size="sm" onClick="">Free Text</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }
  content4 = () => {
    return (
      <div className="container-fluid-inner">
        <div className='margin-top-16'>
       
              <div>
              
                  <div sm='3'>
                    <TextBoxComponent placeholder=" " floatLabelType="Auto" />
                  </div>
                  <div sm='3'>
                    <TextBoxComponent placeholder="Field Name" floatLabelType="Auto" />
                  </div>
              <Row className='sectiontype'>
                  <div sm='3'>
                    <TextBoxComponent placeholder="Technical Verification" floatLabelType="Auto" />
                  </div>
                  <div sm='2' className='margin-top16'>
                    <RadioButtonComponent checked={true} label='Prefilled' name='payment' value=""></RadioButtonComponent>
                  </div>
                  <div sm='2' className='margin-top16'>
                    <RadioButtonComponent checked={false} label='Manual' name='payment' value=""></RadioButtonComponent>
                  </div>
                  <div sm='2' >
                    <DropDownListComponent id="category" filterBarPlaceholder='Search a category' placeholder="Select a category" popupHeight="220px" />
                  </div>
                  <div sm='2'>
                    <DropDownListComponent id="Fielddrpdown" filterBarPlaceholder='Search a Field' placeholder="Select Field" popupHeight="220px" />
                  </div>
                </Row>
                <Row>
                  <Col>
                    <span className='float-right margin-top16'>
                      <Button color="primary" size="sm" onClick="">Add</Button>&nbsp;
             <Button className="btncancel" size="sm" onClick="">Cancel</Button>
                    </span>
                  </Col>
                </Row>
              </div>
         
        </div>
      </div>

    )
  }
  render() {
    return (

      <Row>
      <Col sm='12 mt-4'>
        <GridComponent toolbar={this.toolbarOptions} allowPaging={true} editSettings={this.editSettings} pageSettings={this.pageSettings} allowRowDragAndDrop={true} width='auto' dataSource={this.formData}>
                  <ColumnsDirective>
                  <ColumnDirective field='FieldName' headerText='Field Name' width='180' template={this.inputtemplate}  textAlign='Right'/>
                    <ColumnDirective headerText='Prefilled' template={this.prefilledtemplate} textAlign='Center'/>
                    <ColumnDirective headerText='Manual' template={this.manualtemplate} textAlign='Center'/> 
                    <ColumnDirective headerText='Category' template={this.categorytemplate} textAlign='Center'/> 
                    <ColumnDirective headerText='Field' template={this.categorytemplate} textAlign='Center'/>  
                                       
                  </ColumnsDirective>
                  <Inject services={[Page, Toolbar, Edit, RowDD, Selection, Search]} />
                </GridComponent> 
                </Col>
                </Row>
//       <AccordionComponent>
//         <AccordionItemsDirective>
//           <AccordionItemDirective expanded='true' header='Field Form' content={this.content2} />
//           {/* <AccordionItemDirective expanded='true' header="Does the stitched product design, Color & construction matches as per buyer's Approved Sample or Yellow seal sample and

// technical specification" content={this.content3} />
//           <AccordionItemDirective expanded='true' header='Does the product size meet as per approved sample/technical specification.' content={this.content3} />
//           <AccordionItemDirective expanded='true' header='Are the approved Fabric, Accessories, trims used.' content={this.content3} /> */}

//         </AccordionItemsDirective>
//       </AccordionComponent>


    );
  }
}

export default CriteriaformFieldComponent;
