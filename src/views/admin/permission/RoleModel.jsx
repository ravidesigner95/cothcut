export class RoleModel {
    constructor(id, name) {
        this.id = id;
        this.name = name;    
    }
    id = null;
    name = null;

}

export default RoleModel;