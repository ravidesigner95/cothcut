export class BuyerPoModel {
    constructor(buyerPoDate, buyerPoDelDate, buyerPoDelQty, buyerPoDpciPCDDate, buyerPoNr, buyerPoOrangeDate1, buyerPoOrangeDate2, buyerPoOrderedQty,
        buyerPoPCDDate, buyerPoPCDPercentage, exFactoryDate, lineItemNr, saidNr, windowDate1, windowDate2) {
        this.buyerPoDate = buyerPoDate;
        this.buyerPoDelDate = buyerPoDelDate;
        this.buyerPoDelQty = buyerPoDelQty;
        this.buyerPoDpciPCDDate = buyerPoDpciPCDDate;
        this.buyerPoNr = buyerPoNr;
        this.buyerPoOrangeDate1 = buyerPoOrangeDate1;
        this.buyerPoOrangeDate2 = buyerPoOrangeDate2;
        this.buyerPoOrderedQty = buyerPoOrderedQty;
        this.buyerPoPCDDate = buyerPoPCDDate;
        this.buyerPoPCDPercentage = buyerPoPCDPercentage;
        this.exFactoryDate = exFactoryDate;
        this.lineItemNr = lineItemNr;
        this.saidNr = saidNr;
        this.windowDate1 = windowDate1;
        this.windowDate2 = windowDate2;
    }
    buyerPoDate = null;
    buyerPoDelDate = null;
    buyerPoDelQty = null;
    buyerPoDpciPCDDate = null;
    buyerPoNr = null;
    buyerPoOrangeDate1 = null;
    buyerPoOrangeDate2 = null;
    buyerPoOrderedQty = null;
    buyerPoPCDDate = null;
    buyerPoPCDPercentage = null;
    exFactoryDate = null;
    lineItemNr = null;
    saidNr = null;
    windowDate1 = null;
    windowDate2 = null;
}
export default BuyerPoModel;