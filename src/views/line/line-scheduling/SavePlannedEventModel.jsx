export class SavePlannedEventModel {
    constructor(id, monthId, categoryColor, description, startDateTime, endDateTime, oldStartDateTime, oldEndDateTime, saidId, lineItem,prodLineId,plannedInfoId, scheduledData,IsReadonly) {
        this.id = id;
        this.monthId = monthId;
        this.categoryColor = categoryColor;
        this.description = description;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.oldStartDateTime = oldStartDateTime;
        this.oldEndDateTime = oldEndDateTime;
        this.saidId = saidId;
        this.lineItem = lineItem;
        this.IsReadonly=IsReadonly;
        this.prodLineId=prodLineId;
        this.plannedInfoId=plannedInfoId;
        this.scheduledData = scheduledData;
    } 
    id = null;
    monthId = null;
    categoryColor = "";
    description = "";
    startDateTime = null;
    endDateTime = null;
    oldStartDateTime = null;
    oldEndDateTime = null;
    saidId = null;
    lineItem = null;
    IsReadonly=false;
    prodLineId=null;
    plannedInfoId=null;
    scheduledData = {}
}

export default SavePlannedEventModel;