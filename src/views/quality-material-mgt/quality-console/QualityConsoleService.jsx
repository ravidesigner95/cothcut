import axios from 'axios';

class QualityConsoleService {
    constructor() { }
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL_QUALITY;
    moepUrl = process.env.REACT_APP_BASEURL + this.version;
    inspectionUrl = this.baseUrl + "/inspection/getAllInspectionsForConsole";
    rolesUrl = this.baseUrl + "/v1/roles";
    usersUrl = this.baseUrl + "/v1/user";
    addAssigneeUrl = this.baseUrl + "/inspection/addAssignee";
    reAssignUrl = this.baseUrl + "/inspection/reAssign"
    generateReportUrl = this.baseUrl + "/inspection/getReportByInspectionNum";
    downloadReportUrl = this.baseUrl + "/v1/form/downloadFile";
    waiveOffByTypeUrl = this.baseUrl + "/inspection/waiveOff";
    waiveOffByRoleUrl = this.baseUrl + "/inspection/waiveOffByRole";
    sendMailUrl = this.baseUrl + "/inspection/SendEmailForQualityInspection";
    copyInspectionUrl = this.baseUrl + "/inspection/copyInspection";
    tableOrMachineUrl = this.baseUrl + "/inspection/getTableAndMachine";
    assignTableOrMAchineUrl = this.baseUrl + "/inspection/assignTable";
    deleteAssignTableOrMAchineUrl = this.baseUrl + "/inspection/assignTable";
    updateAssignTableOrMAchineUrl = this.baseUrl + "/inspection/updateAssignTable";
    getInspectionsByTicketNoUrl = this.baseUrl + "/inspection/getInspectionByNum";
    plantListUrl = this.moepUrl + `/master/plant`;
    loadUrl = this.moepUrl + `/load/getLoads`;
    unitListUrl = this.plantListUrl;
    manualInspMasterUrl = this.baseUrl + "/quality_insp_master/getManualInspections";
    createManualInspUrl = this.baseUrl + "/inspection/generateInspection";

    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }

    };

    async getPlantList() {
        try {
            const response = await axios.get(this.plantListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnitList(plantId) {
        try {
            const response = await axios.get(this.unitListUrl + `/${plantId}/units/`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getInspectionList(plantId, unitId, startDate, endDate, statuses) {
        const config = {
            headers: this.options.headers,
            params: {
                plantId: plantId,
                unitId: unitId,
                inspectionStartDate: startDate,
                inspectionEndDate: endDate
            }
        };
        try {
            const response = await axios.post(this.inspectionUrl, statuses, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getRoles() {
        try {
            const response = await axios.get(this.rolesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getUsers(roleName) {
        try {
            const response = await axios.get(this.usersUrl + `/${roleName}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async addAssignee(inspectionList, role, userId) {
        const config = {
            headers: this.options.headers,
            params: {
                role: role,
                userId: userId
            }
        };
        try {
            const response = await axios.post(this.addAssigneeUrl, inspectionList, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async reAssign(inspectionList, role, userId) {
        const config = {
            headers: this.options.headers,
            params: {
                role: role,
                userId: userId
            }
        };
        try {
            const response = await axios.post(this.reAssignUrl, inspectionList, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async generateReport(inspectionNo) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    inspectionNum: inspectionNo,

                }
            };
            return await axios.post(this.generateReportUrl,null, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async sendMail(inspectionNo) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    inspectionId: inspectionNo,

                }
            };
            return await axios.post(this.sendMailUrl, null, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async downloadReport(docId) {
        try {
            const response = await axios.get(this.downloadReportUrl + `/${docId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getInspectionsByTicketNo(inspNum) {
        const config = {
            headers: this.options.headers,
        };
        try {
            const response = await axios.post(this.getInspectionsByTicketNoUrl, inspNum, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async waiveOffByType(inspectionList, waiveOffType) {
        const config = {
            headers: this.options.headers,
            params: {
                waiveOffType: waiveOffType
            }
        };
        try {
            const response = await axios.post(this.waiveOffByTypeUrl, inspectionList, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async waiveOffByRole(inspectionList, role) {
        const config = {
            headers: this.options.headers,
            params: {
                role: role
            }
        };
        try {
            const response = await axios.post(this.waiveOffByRoleUrl, inspectionList, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getTableOrMachine(plantId) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plantId: plantId,

                }
            };
            return await axios.get(this.tableOrMachineUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async saveAssignTable(newAssignee) {
        try {
            const response = await axios.post(this.assignTableOrMAchineUrl, newAssignee, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async updateAssignTable(updateAssignee) {
        try {
            const response = await axios.put(this.updateAssignTableOrMAchineUrl, updateAssignee, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async copyInspection(inspectionNum) {
        const config = {
            headers: this.options.headers,
            params: {
                inspectionNum: inspectionNum
            }
        };
        try {
            const response = await axios.post(this.copyInspectionUrl, null, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteAssignTable(id) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    assign_id: id,

                }
            };
            return await axios.delete(this.deleteAssignTableOrMAchineUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getLoadedLines(plantId, unitId) {

        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plantId: plantId,
                    unitId: unitId,
                }
            };
            const response = await axios.get(this.loadUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getManualInspList() {
        try {
            const response = await axios.get(this.manualInspMasterUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async createManualInspection(entityId, type, inspMasterId) {
        const config = {
            headers: this.options.headers,
            params: {
                entityId: entityId,
                type: type,
                inspMasterId: inspMasterId
            }
        };
        try {
            const response = await axios.post(this.createManualInspUrl, null, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }



}
export default QualityConsoleService;