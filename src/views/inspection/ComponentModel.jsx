export class ComponentModel {
    constructor(id, qualityCategoryMasterDTOList,componentDesc, componentName,isEditable,screen,sequence,qualityHeaderTypeMasterDTO,
        qualityInspSaveDTO,qualitySectionMasterDTOList,qualityInspAqlFpointDclDTO) {
        this.id = id;
        this.qualityCategoryMasterDTOList = qualityCategoryMasterDTOList;
        this.componentDesc=componentDesc;
        this.componentName=componentName;
        this.isEditable=isEditable;
        this.screen=screen;
        this.sequence=sequence;
        this.qualityInspSaveDTO=qualityInspSaveDTO;
        this.qualityHeaderTypeMasterDTO=qualityHeaderTypeMasterDTO;
        this.qualitySectionMasterDTOList = qualitySectionMasterDTOList;
        this.qualityInspAqlFpointDclDTO=qualityInspAqlFpointDclDTO;
        

    }
    id = null;
    qualityCategoryMasterDTOList = null;
    componentDesc=null;
    componentName=null;
    isEditable=null;
    screen=null;
    sequence=null;
    qualityHeaderTypeMasterDTO=null;
    qualityInspSaveDTO=null;
    qualitySectionMasterDTOList=null;
    qualityInspAqlFpointDclDTO=null;
}

export default ComponentModel; 
export class InpectionTypeModel {
    constructor(id, code,date, description,isActive,name,revision,rule,componentSequenceDTOS,inspectionCategoryMasterDTO,isAutomatic,inspTypeInfo) {
        this.id = id;
        this.code = code;
        this.date=date;
        this.description= description;
        this.isActive=isActive;
        this.name=name;
        this.revision=revision;
        this.rule=rule;
        this.componentSequenceDTOS=componentSequenceDTOS;
        this.inspectionCategoryMasterDTO = inspectionCategoryMasterDTO;
        this.isAutomatic=isAutomatic;
        this.inspTypeInfo=inspTypeInfo;
        

    }
    id=null;
    code = null;
    date = null;
    description=null;
    isActive =null;
    name=null;
    revision=null;
    rule=null;
    componentSequenceDTOS=[];
    inspectionCategoryMasterDTO=null;
    isAutomatic=null;
    inspTypeInfo=[];

}

export class CategoryModel{
    constructor(id,categoryName){
        this.id=id;
        this.categoryName = categoryName;
    }
    id=null;
    categoryName=null;
}

export class HeadTypeModel{
    constructor(id,headerTypeName){
        this.id=id;
        this.headerTypeName = headerTypeName;
    }
    id=null;
    headerTypeName=null;
}
export class ComponentSequenceModel{
    constructor(componentId,sequence){
        this.componentId=componentId;
        this.sequence = sequence;
    }
    componentId=null;
    sequence=null;
}
export class QualityInspAqlFpointDclModel{
    constructor(id,defaultIndicator,sampleSize,standardName,dclAqlId){
        this.id=id;
        this.defaultIndicator = defaultIndicator;
        this.sampleSize=sampleSize;
        this.standardName=standardName;
        this.dclAqlId=dclAqlId;
    }
    id=null;
    defaultIndicator=null;
    sampleSize = null;
    standardName=null;
    dclAqlId=null;
}