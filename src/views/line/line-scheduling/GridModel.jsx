class GridModel {
    constructor(prodLineId,saidId,plannedLineItems) { 
        this.prodLineId=prodLineId;
        this.saidId=saidId;
        this.plannedLineItems=plannedLineItems;
    }
    prodLineId = null;
    saidId=null;
    plannedLineItems = [];
} 
export default GridModel; 