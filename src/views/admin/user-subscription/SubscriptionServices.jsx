import axios from 'axios';

export class PermissionServices {

    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    saveUserSubscriptionURL = process.env.REACT_APP_BASEURL_DASHBOARD + `/qualityservice/alertSubscription/updateSubscription`;
    getUserSubscriptionURL = process.env.REACT_APP_BASEURL_DASHBOARD + `/qualityservice/alertSubscription/getAllSubscription`;
    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    async saveUserSubscription(data) {
        console.log(this.saveUrl)
        try {
            const response = await axios.post(this.saveUserSubscriptionURL, data, this.options)
            return response.data;
        }
        catch (error) {
            // alert("error")
        }
    }

    async getUserSubscription() {
        try {
            const response = await axios.get(this.getUserSubscriptionURL, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
}
export default PermissionServices;
