export class PoInfoModel {
    constructor(id,poQty,pOLineDescription,poNumber,pcdDate, balanceQty, grnTotQty, pcdQty,returnQty,poLineItem,Uom) {
        this.id = id;
        this.poQty = poQty;
        this.pOLineDescription = pOLineDescription;
        this.poNumber = poNumber;
        this.pcdDate = pcdDate;
        this.balanceQty = balanceQty;
        this.grnTotQty = grnTotQty;
        this.pcdQty = pcdQty;
        this.returnQty = returnQty;
        this.poLineItem = poLineItem;
        this.Uom = Uom;
    }
    id = null;
    poQty = null;
    pOLineDescription = null;
    poNumber = null;
    pcdDate = null;
    balanceQty = null;
    grnTotQty = null;
    pcdQty = null;
    returnQty=null;
    poLineItem = null;
    Uom=null;
}

export default PoInfoModel; 

    