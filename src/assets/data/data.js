export const data =
{
    "category": [
        { "categoryName": "Stiching", "categoryId": "2" },
        { "categoryName": "Finishing", "categoryId": "1" }
    ],

    "categories": [
        { "Name": "Finishing", "Code": "Fin" },
        { "Name": "Stiching", "Code": "Sti" },
        { "Name": "Re Cutting", "Code": "Cut" },
        { "Name": "Packaging", "Code": "Pack" },
        { "Name": "Fabric", "Code": "Fab" },
        { "Name": "Accessories", "Code": "Acc" },

    ],

    "Fielddrpdown": [
        { "Name": "Field 1", "Code": "Fin" },
        { "Name": "Field 2", "Code": "Sti" },
        { "Name": "Field 3", "Code": "Cut" },
        { "Name": "Field 4", "Code": "Pack" },
        { "Name": "Field 5", "Code": "Fab" },
        { "Name": "Field 6", "Code": "Acc" },

    ],
    "Questionform": [
        { "Name": "Does the product size meet as per approved sample/technical specification.", "Code": "Fin" },
        { "Name": "Does the stitched product design, Color & construction matches as per buyer's Approved Sample or Yellow seal sample and technical specification.", "Code": "Sti" },
        { "Name": "Are the approved Fabric, Accessories, trims used.", "Code": "Cut" },

    ],
    "QuestionCategory": [
        { "Name": "Finishing", "Code": "Fin" },
        { "Name": "Stiching", "Code": "Sti" },
        { "Name": "Re Cutting", "Code": "Cut" },
        { "Name": "Packaging", "Code": "Pack" },
        { "Name": "Fabric", "Code": "Fab" },
        { "Name": "Accessories", "Code": "Acc" },

    ],
    "AnswerType": [
        { "Name": "Does the product size meet as per approved sample/technical specification.", "Code": "Fin" },
        { "Name": "Does the stitched product design, Color & construction matches as per buyer's Approved Sample or Yellow seal sample and technical specification.", "Code": "Sti" },
        { "Name": "Are the approved Fabric, Accessories, trims used.", "Code": "Cut" },

    ],
    "HeaderType": [
        { "Name": "General", "Code": "Gen" },
        { "Name": "Defects", "Code": "Def" },
        { "Name": "AQL", "Code": "AQL" },
        { "Name": "4 Point", "Code": "4Point" },
        { "Name": "100%  Summer", "Code": "Sum" },
        { "Name": "Summary", "Code": "Summary" },

    ],
    "HeaderInfo": [
        { "Name": "General Info", "Code": "GI" },
        { "Name": "Color Defects", "Code": "CD" },
        { "Name": "Than Pieces", "Code": "TC" },
    ],
    "InspecitonRule": [
        { "Name": "Store", "Code": "Rule" },
        { "Name": "Production", "Code": "Rule" },
        { "Name": "Pre-Production", "Code": "Rule" },
    ],
    "Storecategsearch": [
        { "Name": "Visula Inspection", "Code": "VI" },
        { "Name": "AQL", "Code": "aql" },
        { "Name": "100% Checking", "Code": "checking" },
        { "Name": "4 Point", "Code": "4Point" },
        { "Name": "LOb", "Code": "4Point" },
        { "Name": "Others", "Code": "Others" },
    ],
    "processtypesearchData": [
        { "Name": "Re-Cutting", "Code": "Rc" },
        { "Name": "Stiching", "Code": "Stch" },
        { "Name": "Finishing", "Code": "Finsh" },
        { "Name": "Washing", "Code": "4Point" },
        { "Name": "Packing", "Code": "4Point" },
    ],
    "insrulesearchData": [
        { "Name": "Store", "Code": "store" },
        { "Name": "Production", "Code": "prod" },
        { "Name": "Pre-Production", "Code": "PreProd" },
    ],
}

export const inspectionDropdownListData = [
    { id: 1, Name: 'Target GS', Designation: "Target GS - Packing Softhome Workflow", type: 'gsp', totalinspection: 15 }
]

export const treedataSource = [
    { "id": 1, "name": "General Information", "hasChild": true,  "count": "2"},
    { "id": 2,"pid": 1, "name": "General Information1", "count": "4" },
    { "id": 3,"pid": 1, "name": "General Information2"},
    { "id": 4,"pid": 1, "name": "General Information3 ", "count": "6"},
    { "id": 5, "name": "Criteria List", "hasChild": true, "expanded": false,  "count": "1" },
    { "id": 6,"pid": 5, "name": "Criteria List 1",  "selected": true , "count": "20"},
    { "id": 7,"pid": 5,  "name": "Criteria List 2", "count": "5"},
    { "id": 8,"pid": 5,  "name": "Criteria List 3"}, 
    { "id": 9, "name": "Criteria List 1", "hasChild": true, "expanded": false,  "count": "0" },
    { "id": 10, "name": "Criteria List2", "hasChild": true, "expanded": false,  "count": "0" },
]
// var coll = document.getElementsByClassName("collapsible");
// var i;

// for (i = 0; i < coll.length; i++) {
//   coll[i].addEventListener("click", function() {
//     this.classList.toggle("active");
//     var content = this.nextElementSibling;
//     if (content.style.maxHeight){
//       content.style.maxHeight = null;
//     } else {
//       content.style.maxHeight = content.scrollHeight + "px";
//     } 
//   });
// }
