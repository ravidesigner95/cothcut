export class SchedulerModel {
    constructor(id, date, prodLineId, dayWiseWorkLoad) {
        this.id = id;
        this.date = date;
        this.prodLine.id = prodLineId;
        this.dayWiseWorkLoad = dayWiseWorkLoad;
    }
    id = null;
    date = {};
    prodLine = {
        id: null
    }; 
    dayWiseWorkLoad = [];
}

export default SchedulerModel;