import axios from 'axios'; 
global.baseUrl = "/"; // For local 

global.showLoader = function () {
  document.getElementById("pageloader").style.display = "block";
}

global.hideLoader = function () {
  document.getElementById("pageloader").style.display = "none";
}
 