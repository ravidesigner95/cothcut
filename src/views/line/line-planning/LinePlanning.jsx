import React from 'react';
import { DropDownListComponent, revertHighlightSearch } from '@syncfusion/ej2-react-dropdowns';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { DateRangePickerComponent } from '@syncfusion/ej2-react-calendars';
import { ScheduleComponent, ViewsDirective, ViewDirective, Day, Week, WorkWeek, Month, Agenda, Inject, Resize, DragAndDrop, lastDateOfMonth } from '@syncfusion/ej2-react-schedule';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { Button, Card, CardHeader, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { GridComponent, ColumnsDirective, ColumnDirective,  ExcelExport, Toolbar,Edit, QueryCellInfoEventArgs, RowDataBoundEventArgs, Selection, Filter, Sort, Page, Group, ColumnMenu } from '@syncfusion/ej2-react-grids';
import PlanningModel from './PlanningModel';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
// import './custom.css';
import { withTranslation } from "react-i18next";
import { PivotViewComponent, ConditionalFormatting } from "@syncfusion/ej2-react-pivotview";
import LinePlanningService from './LinePlanningService';
import SchedulerModel from './SchedulerModel';
import SchedulerDetailsModel from './SchedulerDetailsModel';
import PlanningDetailsModel from './PlanningDetailsModel';
import SplitterLayout from 'react-splitter-layout';
import { MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import 'react-splitter-layout/lib/index.css';
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import { L10n, setCulture, setCurrencyCode } from '@syncfusion/ej2-base';
import { number } from 'prop-types';
import errorMessages from '../../errorHandler/errorMessages';
import { PlanningHistoryModel } from './PlanningHistoryModel';
import '../line-scheduling/cell.css';
import { CalendarComponent } from '@syncfusion/ej2-react-calendars';


L10n.load({
    'de': {
        'pivotview': {
            'collapse': 'englishCollapse',
            'expand': 'englishExpand'
        },
    },
    'en': {
        'pivotview': {
            'collapse': 'hindiCollapse',
            'expand': 'hindiExpand'
        }
    }
});

class LinePlanning extends React.PureComponent {

    constructor(props) {
        super()
        const { t, i18n } = props;
        this.linePlanningService = new LinePlanningService();
        this.mounted = false;
        this.gridSettings = {
            allowReordering: true,
            width: '100% ',
            height: '600px'
        }
        this.state = {
            planningHistoryPlants: [],
            showDialog: false,
            planningHistoryUnits: [],
            selectedPlanningHistoryAlert: [],
            planningHistoryAlert: [],
            planningHistoryBuyerPo: [],
            planningHistorySaid: [],
            planningHistoryLineItem: [],
            planningHistoryStatus: [],
            selectedReportPlantId: null,
            selectedReportUnitId: null,
            selectedReportStartDate: null,
            selectedReportEndDate: null,
            selectedPlanningHistoryAlert: [],
            selectedPlanningHistoryPlants: [],
            selectedPlanningHistoryUnits: [],
            selectedPlanningHistoryBuyerPo: [],
            selectedPlanningHistorySaid: [],
            selectedPlanningHistoryLineItem: [],
            selectedPlanningHistoryStatus: [],
            selectedPlanningHistoryEndDate: null,
            selectedPlanningHistoryStartDate: null,
            isShowFilters: false,
            isShowPlanningHistoryScreen: false,
            isShowLineIDSelectionDialog: false,
            selectedPlanningHistoryProdLine: null,
            planningHistoryProdLines: [],
            orders: [],
            orderDetails: {
                isInvokedFromPlanningHistory: false,
                plantId: null,
                unitId: null,
                prodLineId: null,
                lineName: null,
                unitName: null
            },
            FilterSettingsModel: { type: 'Menu' },
            IFilter: { type: 'CheckBox' },
            //------------------------------//
            isRefreshAllLines: false,
            isSummaryView: false,
            isCategoryView: false,
            isDirtyOnEdit: false,
            isSidebar: false,
            model: new PlanningDetailsModel(null, null, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", null, null, null, 0, null, null, null, null, null, null, null),
            plannedDataModel: new PlanningModel(),
            linePlanningDetails: [],
            deletedEventsId: [],
            holidaysList: [],
            configKeys: {},
            daywiseAvailableMachines: new Map(),
            selectedPlantName: null,
            selectedUnitName: null,
            selectedYear: null,
            summarySelectedYear: null,
            selectedPlantList: [],
            selectedNameOfUnitForApi: null,
            selectedLineName: null,
            currrentDate: new Date(),
            selectedProdLineId: null,
            selectedProductCategoryId: null,
            selectedProdLineMonth: null,
            selectedProdLineYear: null,
            selectedEventIdForEdit: null,
            argsData: null,
            summaryDataPlants: [],
            summaryDataCategory: [],
            dynamicSummaryViewPlants: [],
            dynamicSummaryViewCategory: [],
            multiSelectedPlants: [],
            plants: [],
            units: [],
            years: [],
            unitProductionLines: [],
            apm: [],
            productionCategory: [],
            lineItemNo: [],
            saidNoDropdownList: [],
            selectionSettings: { mode: 'Cell' },
            currentView: "Month",
            schedulerDataOnLoad: [],
            dynamicPivotGrids: [],
            planningStatusExportProperties:{fileName:'Planning_Status.xlsx'},

            toasterMessage: [
                { title: 'Information!', content: 'Navigation to freezed months are no  t allowed !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Warning!', content: 'Selected cell is not editable !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
                { title: 'Success!', content: 'Saved successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Information!', content: 'Fill all mandatory fields to create new event !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Warning!', content: 'No Change Found !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Information!', content: 'Estimated Successfully !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Error!', content: 'Data is not available for the given said Id !!', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
                { title: 'Warning!', content: "Production lines doesn't exist  !!", cssClass: 'e-toast-warning', icon: 'e-info toast-icons' },
                { title: 'Information!', content: "Start Date can't be greater that End Date & End Date can't be less than Start Date !!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Information!', content: "SAM can't be zero for planning !!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Information!', content: "Quantity can't be zero !!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Warning!', content: 'Start Date or End Date should be greater than current date !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Information!', content: "Please select any Filter !!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Information!', content: 'Search completed !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Information!', content: 'Filter Reset Successfully !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Success!', content: 'Report Downloaded successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' }

            ],
            index: null,
            isEdit: false,
            refreshDate: false,
            maxAllowedMachines: null,
            maxAllowedWorkingHours: null,
            minNavigationDate: new Date(),
            maxNavigationDate: new Date(),
            minStartDate: null,
            maxLineQtyAllocated: null,
            selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"),
            animationSettings: { effect: 'Fade' },
            isShowConfirmationDialog: false,
            isDisabledSaveButton: false,
            selectedEventColor: null,
            isCreateNewEvent: false,
            workingDays: [1, 2, 3, 4, 5, 6],
            lineNameToRefresh: null,
            isValidateMachines: false,
            extraMachines: null,
            confirmButton: [{
                click: () => {
                    if (this.state.isValidateMachines)
                        this.onApply()
                    else
                        this.getPlannedEventData();
                },
                buttonModel: { content: 'Yes', isPrimary: true }
            },
            {
                click: () => {
                    this.cancelConfirmDialog();
                },
                buttonModel: { content: 'No' }
            }],
            isMonthlyView: "month",
        }

        this.onClickPlanning = this.onClickPlanning.bind(this);

    }

    componentDidMount() {
        this.mounted = true;
        this.getPlantList()
        this.getAPM();
        this.getCategory()
        this.getConfgKey()
        this.getAllOrders()
        // this.getHolidaysList()   IN-CASE OF HLOIDAYS
        document.addEventListener("keydown", this.escFunction, false);
    }

    componentWillUnmount() {
        this.mounted = false;
        document.removeEventListener("keydown", this.escFunction, false);
    }

    componentWillReceiveProps(props) {
        // let that = this;
        // that["A80-Unit-1-PL-1"].properties.locale = props.i18n.language
        // that["A80-Unit-1-PL-1"].pivotRefresh()
        // console.log(props);
    }

    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.sideBar();
            document.body.classList.remove('overflow-hidden-body');
        }

    }

    //---------------------------------------------------------------API----------------------------------------------------------------//

    async getEstimatedWorkingDays(buyerPo, lineItem, noOfMachines, prodLineId, qty, saidNo, sam, startDate) {
        global.showLoader();
        let response = (await this.linePlanningService.getEstimatedWorkingDays(buyerPo, lineItem, noOfMachines, prodLineId, qty, saidNo, sam, startDate))
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            global.hideLoader();
            return response.data.data;
        }
        global.hideLoader();
    }


    async getPlantList() {
        let response = (await this.linePlanningService.getPlantList())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted))
            this.setState({ plants: response.data.data })
    }


    async getUnitList(plantId) {
        let response = (await this.linePlanningService.getUnitList(plantId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ units: response.data.data, selectedUnitName: null, selectedNameOfUnitForApi: null, unitProductionLines: [], dynamicPivotGrids: [] }, () => {
                global.hideLoader();
            })
        }
    }

    async getAllOrders() {
        global.showLoader();
        let response = (await this.linePlanningService.getAllOrders())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            let orders = response.data.data;
            let ordersList = [];
            orders.forEach(element => {
                ordersList.push(new PlanningHistoryModel(element.apmName, element.fmName, element.plantName, element.buyerName, element.qaName, element.unitName,
                    element.merchandiserName, element.lineItemDesc, element.lineItemNo, element.leftPlannedQty, element.leftPoQtyTobePlanned, element.orderedQty,
                    element.plannedQty, element.poQty, Number(element.plantId), element.saidId, element.pcdDate == null ? null : new Date(element.pcdDate), element.status, Number(element.unitId),
                    element.alertCode, element.pcdDate == null ? null : new Date(element.pcdDate)));
            })
            let uniquePlants = ordersList.filter((value, index, array) => array.findIndex(t => (t.plantName === value.plantName)) === index);
            let uniqueUnits = ordersList.filter((value, index, array) => array.findIndex(t => (t.unitName === value.unitName) && (value.unitName != null) && (value.unitName != "")) === index);
            // let uniqueLines = ordersList.filter((value, index, array) => array.findIndex(t => (t.plantName === value.plantName)) === index); //lineid not available now
            let uniqueStatus = ordersList.filter((value, index, array) => array.findIndex(t => (t.status === value.status)) === index);
            let uniqueBuyerPos = ordersList.filter((value, index, array) => array.findIndex(t => (t.buyerName === value.buyerName)) === index);
            let uniqueSaids = ordersList.filter((value, index, array) => array.findIndex(t => (t.saidId === value.saidId)) === index);
            let uniqueLineItems = ordersList.filter((value, index, array) => array.findIndex(t => (t.lineItemNo === value.lineItemNo)) === index);
            let uniqueAlertCode = ordersList.filter((value, index, array) => array.findIndex(t => (t.alertCode === value.alertCode)) === index);
            this.setState({
                orders: ordersList, planningHistoryAlert: uniqueAlertCode, planningHistoryStatus: uniqueStatus, planningHistoryLineItem: uniqueLineItems, planningHistoryPlants: uniquePlants,
                planningHistoryUnits: uniqueUnits, planningHistoryBuyerPo: uniqueBuyerPos, planningHistorySaid: uniqueSaids, isShowPlanningHistoryScreen: true,
                orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: true }
            })
        }
        global.hideLoader();
    }

    async getDaywiseNoOfAllocatedMachines(prodLineId) {
        let response = (await this.linePlanningService.getDaywiseNoOfAllocatedMachines(prodLineId))
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            response.data.data.forEach(element => {
                this.state.daywiseAvailableMachines.set((new Date(element.date.year, element.date.month - 1, element.date.day)).toDateString(), element.availableMachines);
            });
        }
    }

    async getConfgKey() {
        let response = (await this.linePlanningService.getConfgKey())
        if ((response != undefined) && (response.data.data.length > 0) && (response.data.status == "success") && (this.mounted)) {
            let configObj = {};
            response.data.data.forEach(element => {
                configObj[element.keyName] = /^\d+$/.test(element.keyValue) == true ? Number(element.keyValue) : element.keyValue;
            })
            this.setState({ configKeys: configObj })
        }
    }


    async getUnitProductionLines(unitId) {
        let response = (await this.linePlanningService.getUnitProductionLines(unitId))
        if ((response != undefined) && (response.data.status == "success")) {
            let defaultSelectionModifiedArray = response.data.data;
            defaultSelectionModifiedArray.push({ lineName: "All", id: 0 });
            this.setState({ unitProductionLines: defaultSelectionModifiedArray, selectedLineName: 0 })
            console.log('unitProductionLines', this.state.unitProductionLines)
        }
    }


    async getPlannedQty(saidNo, lineItem) {
        let response = (await this.linePlanningService.getPlannedQty(saidNo, lineItem))
        if ((response != undefined) && (response.data.status == "success") && (this.mounted))
            return response.data.data;
    }


    async getCategory() {
        let response = (await this.linePlanningService.getCategory())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            this.setState({ productionCategory: response.data.data })
        }
    }


    async getSaidNo(plantId, unitName) {
        let response = (await this.linePlanningService.getSaidNo(plantId, unitName))
        if ((response != undefined) && (response.data.status == "success") && (this.mounted))
            this.setState({ saidNoDropdownList: response.data.data })
        else
            this.setState({ saidNoDropdownList: [] })

        global.hideLoader();
    }


    async getAPM() {
        let response = (await this.linePlanningService.getAPM())
        if ((response != undefined) && (response.data.status == "success"))
            this.setState({ apm: response.data.data })
    }


    async getHolidaysList() {
        let response = (await this.linePlanningService.getHolidaysList())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted))
            this.setState({ holidaysList: response.data.data })
    }


    async getLinePlanningDetails(prodLineId, unitId, type, lineNameToRefresh, targetYear) {
        let selectedUnitId = unitId == null ? this.state.selectedUnitName : unitId;
        let response = (await this.linePlanningService.getLinePlanningDetails(prodLineId, selectedUnitId, targetYear))
        if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
            this.state.linePlanningDetails = response.data.data;
            if (type == null) // On change unit dropdown
            {
                this.createPivotGrid();
            }
            else {
                this[lineNameToRefresh].dataSourceSettings.dataSource = this.convertPivotData(this.returnChangedLineFromPivot(lineNameToRefresh, response.data.data).linePlanningForMonthDtos);
                this[lineNameToRefresh].pivotRefresh();
                this.state.isRefreshAllLines = false;
                global.hideLoader();
            }
        } else {
            if ((response != undefined) && (response.data != undefined) && (response.data.data != undefined) && (response.data.data.length == 0)) {
                this.toastObj.show(this.state.toasterMessage[8]);
                this.setState({ dynamicPivotGrids: [], isRefreshAllLines: false }, () => {
                    global.hideLoader();
                })
            } else {
                this.toastObj.show(this.state.toasterMessage[2]);
                this.setState({ dynamicPivotGrids: [], isRefreshAllLines: false }, () => {
                    global.hideLoader();
                })
            }

        }
    }


    async getSelectedMonthPlannedEvents(prodLineId, month, year, selectedMonthView, lineObj) {
        let that = this;
        const { model, linePlanningDetails, planningHistoryProdLines, orderDetails } = that.state;
        await that.getDaywiseNoOfAllocatedMachines(prodLineId);
        let response = (await that.linePlanningService.getSelectedMonthPlannedEvents(prodLineId, month, year))
        if ((response != undefined) && (response.data.status == "success")) {
            let tempMinNavigationDate;
            let tempMaxNavigationDate;
            let tempSchedulerDataOnLoad = [];
            let noOfMachineInLine;
            let workingHoursForLine;
            response.data.data.map(item => {
                item.dayWiseWorkLoad.map(dayWiseWorkLoadObj => {
                    let convertedStartDate = new Date(Date.UTC(dayWiseWorkLoadObj.startDateTime.date.year, dayWiseWorkLoadObj.startDateTime.date.month - 1, dayWiseWorkLoadObj.startDateTime.date.day, dayWiseWorkLoadObj.startDateTime.time.hour, dayWiseWorkLoadObj.startDateTime.time.minute))
                    let convertedEndDate = new Date(Date.UTC(dayWiseWorkLoadObj.endDateTime.date.year, dayWiseWorkLoadObj.endDateTime.date.month - 1, dayWiseWorkLoadObj.endDateTime.date.day, dayWiseWorkLoadObj.endDateTime.time.hour, dayWiseWorkLoadObj.endDateTime.time.minute))
                    let schedulerDateObj = new SchedulerDetailsModel(dayWiseWorkLoadObj.id, item.id, dayWiseWorkLoadObj.categoryColor, dayWiseWorkLoadObj.description, convertedStartDate, convertedEndDate, convertedStartDate, convertedEndDate, dayWiseWorkLoadObj.saidId, dayWiseWorkLoadObj.saidId, dayWiseWorkLoadObj.lineItem, dayWiseWorkLoadObj.lineItem, dayWiseWorkLoadObj.prodLineId, dayWiseWorkLoadObj.plannedInfoId,
                        new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, null, model.plantId, model.unitId, model.prodLineId, null, null, null, model.plantName, model.unitName, model.lineName, "", "", null, null, dayWiseWorkLoadObj.lineItem, 0, null, null, null, null, model.noOfMachineToUse, null, null), false)
                    tempSchedulerDataOnLoad.push(schedulerDateObj);
                })
            })
            // Fetching dates to be freeze.
            if (orderDetails.isInvokedFromPlanningHistory) {
                noOfMachineInLine = lineObj.numberOfMachines;
                workingHoursForLine = lineObj.workingHour;
                tempMinNavigationDate = new Date();
                that.resetOrders(false);
            } else {
                for (let i = 0; i < linePlanningDetails.length; i++) {
                    if (prodLineId == linePlanningDetails[i].prodLineId) {
                        noOfMachineInLine = linePlanningDetails[i].maxNoOfMachAllowedInLine;
                        workingHoursForLine = linePlanningDetails[i].numberOfLineWorkingHours;
                        for (let j = 0; j < linePlanningDetails[i].linePlanningForMonthDtos.length; j++) {
                            if (linePlanningDetails[i].linePlanningForMonthDtos[j].isFreeze == false) {
                                tempMinNavigationDate = new Date(linePlanningDetails[i].linePlanningForMonthDtos[j].year, linePlanningDetails[i].linePlanningForMonthDtos[j].monthNumber - 1)
                                break;
                            }
                        }
                        let lastMonthObject = linePlanningDetails[i].linePlanningForMonthDtos[linePlanningDetails[i].linePlanningForMonthDtos.length - 1];
                        let lastDateOfMonth = new Date(lastMonthObject.year, lastMonthObject.monthNumber, 0).getDate()
                        tempMaxNavigationDate = new Date(lastMonthObject.year, lastMonthObject.monthNumber - 1, lastDateOfMonth)   // find last date of month
                    }
                }
            }
            that.setState({
                isShowLineIDSelectionDialog: false, selectedPlanningHistoryProdLine: null, planningHistoryProdLines: [], isShowPlanningHistoryScreen: false
            }, () => {
                that.setState({
                    schedulerDataOnLoad: tempSchedulerDataOnLoad, isSidebar: true, currrentDate: selectedMonthView, maxAllowedWorkingHours: workingHoursForLine, maxAllowedMachines: noOfMachineInLine,
                    minNavigationDate: tempMinNavigationDate, model: { ...model, noOfMachineToUse: noOfMachineInLine, startDateTime: null, endDateTime: null }
                })
                document.body.classList.add('overflow-hidden-body');
                global.hideLoader();
            })
        } else {
            that.toastObj.show(that.state.toasterMessage[2]);
            document.body.classList.remove('overflow-hidden-body');
            global.hideLoader();
        }
    }


    async getNewPlannedEvents(prodLineId, month, year) {
        let that = this;
        const { model, linePlanningDetails } = that.state;
        await that.getDaywiseNoOfAllocatedMachines(prodLineId);
        let response = (await that.linePlanningService.getSelectedMonthPlannedEvents(prodLineId, month, year))
        if ((response != undefined) && (response.data.status == "success")) {
            let tempSchedulerDataOnLoad = [];
            response.data.data.map(item => {
                item.dayWiseWorkLoad.map(dayWiseWorkLoadObj => {
                    let convertedStartDate = new Date(Date.UTC(dayWiseWorkLoadObj.startDateTime.date.year, dayWiseWorkLoadObj.startDateTime.date.month - 1, dayWiseWorkLoadObj.startDateTime.date.day, dayWiseWorkLoadObj.startDateTime.time.hour, dayWiseWorkLoadObj.startDateTime.time.minute))
                    let convertedEndDate = new Date(Date.UTC(dayWiseWorkLoadObj.endDateTime.date.year, dayWiseWorkLoadObj.endDateTime.date.month - 1, dayWiseWorkLoadObj.endDateTime.date.day, dayWiseWorkLoadObj.endDateTime.time.hour, dayWiseWorkLoadObj.endDateTime.time.minute))
                    let schedulerDateObj = new SchedulerDetailsModel(dayWiseWorkLoadObj.id, item.id, dayWiseWorkLoadObj.categoryColor, dayWiseWorkLoadObj.description, convertedStartDate, convertedEndDate, convertedStartDate, convertedEndDate, dayWiseWorkLoadObj.saidId, dayWiseWorkLoadObj.saidId, dayWiseWorkLoadObj.lineItem, dayWiseWorkLoadObj.lineItem, dayWiseWorkLoadObj.prodLineId, dayWiseWorkLoadObj.plannedInfoId,
                        new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, null, model.plantId, model.unitId, model.prodLineId, null, null, null, model.plantName, model.unitName, model.lineName, "", "", null, null, dayWiseWorkLoadObj.lineItem, 0, null, null, null, null, model.noOfMachineToUse, null, null), false)
                    tempSchedulerDataOnLoad.push(schedulerDateObj);
                })
            })
            that.setState({ schedulerDataOnLoad: [] }, () => {
                that.setState({ schedulerDataOnLoad: tempSchedulerDataOnLoad })
                that.scheduleObj.refresh();
                global.hideLoader();
            })
        }
        else {
            that.toastObj.show(that.state.toasterMessage[2]);
            global.hideLoader();
        }
    }


    async getSaidDetailsOnEventClick(saidId, data, eventDetailFromDb) {
        let that = this;
        const { t, i18n } = that.props;
        const { model, isEdit, maxAllowedMachines, schedulerDataOnLoad, currrentDate, selectedPlantName, selectedUnitName, selectedNameOfUnitForApi, selectedLineName, orderDetails } = that.state;
        let response;
        if (orderDetails.isInvokedFromPlanningHistory) {
            response = (await this.linePlanningService.getSaidDetails(saidId, orderDetails.plantId, orderDetails.unitName))
        } else {
            response = (await this.linePlanningService.getSaidDetails(saidId, selectedPlantName, selectedNameOfUnitForApi))
        }
        if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
            let index = that.getSelectedEventIndex(data);
            let clickedEvent = Object.assign({}, schedulerDataOnLoad[index])
            let plannedQty = await this.getPlannedQty(eventDetailFromDb.saidId, eventDetailFromDb.lineItemNo);
            let remainingQtyToPlan = plannedQty == undefined ? eventDetailFromDb.totalQtyToProduced : (eventDetailFromDb.totalQtyToProduced) - (plannedQty);
            clickedEvent.plannedData = new PlanningDetailsModel(eventDetailFromDb.id, eventDetailFromDb.fmId, eventDetailFromDb.fmName, eventDetailFromDb.qaId, eventDetailFromDb.qaName, eventDetailFromDb.apmId, eventDetailFromDb.saidId, clickedEvent.plannedData.plantId, clickedEvent.plannedData.unitId, clickedEvent.plannedData.prodLineId, eventDetailFromDb.mechandiserId, eventDetailFromDb.productCategoryId, eventDetailFromDb.buyerId, clickedEvent.plannedData.plantName, clickedEvent.plannedData.unitName,
                clickedEvent.plannedData.lineName, eventDetailFromDb.merchandiserName, eventDetailFromDb.buyerName, eventDetailFromDb.apm, eventDetailFromDb.sam, eventDetailFromDb.lineItemNo, eventDetailFromDb.totalQtyAllocatedInLine, eventDetailFromDb.totalQtyToProduced, remainingQtyToPlan, clickedEvent.startDateTime, clickedEvent.endDateTime, eventDetailFromDb.noOfMachineToUse, eventDetailFromDb.estimatedWorkingDays, eventDetailFromDb.lineItemDesc);
            let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEvent })
            that.setState({
                argsData: null, isDirtyOnEdit: false, maxLineQtyAllocated: remainingQtyToPlan + eventDetailFromDb.totalQtyAllocatedInLine, minStartDate: data.data.startDateTime, selectedEventIdForEdit: clickedEvent.id, selectedEventColor: clickedEvent.categoryColor, schedulerDataOnLoad: modifiedArray, isEdit: true, selectedEventName: clickedEvent.description, index,
                model: new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, null, null, null, model.prodLineId, null, null, null, "", "", model.lineName, "", "", null, null, null, 0, null, null, null, null, maxAllowedMachines, null, null), lineItemNo: response.data.data
            }, () => {
                global.hideLoader();
            })
        }
        else {
            that.setState({
                selectedEventColor: null, argsData: null, isEdit: false, selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"),
                model: new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, null, null, null, model.prodLineId, null, null, null, "", "", model.lineName, "", "", null, null, null, 0, null, null, null, null, maxAllowedMachines, null, null), lineItemNo: []
            }, () => {
                global.hideLoader();
            })
            that.toastObj.show(that.state.toasterMessage[2]);
        }
    }


    async getLineItemDetails(event) {
        let that = this;
        const { t, i18n } = that.props;
        const { model, isEdit, index, currrentDate, productionCategory, lineItemNo, maxAllowedMachines, selectedPlantName, selectedUnitName, schedulerDataOnLoad } = that.state;
        if (event.e != undefined) {
            let selectedLineItemData = lineItemNo.find(function (item) {
                return item.lineItemNo == event.value;
            })
            // let prodCategoryIdObject = productionCategory.find(function (item) {
            //     return item.name == selectedLineItemData.lineItemCategory;
            // })
            if (event.e != undefined) {
                let plannedQty = await this.getPlannedQty(model.saidId, selectedLineItemData.lineItemNo);
                let remainingQtyToPlan = plannedQty == undefined ? selectedLineItemData.lineItemOrderedQty : (selectedLineItemData.lineItemOrderedQty) - plannedQty;
                that.setState({
                    model: {
                        ...model,fmId:event.itemData.fMUserId,fmName:event.itemData.fmName,qaId: selectedLineItemData.qaUserId, qaName: selectedLineItemData.qaName, apmId: selectedLineItemData.apmUserId, remainingQtyToPlan, mechandiserId: selectedLineItemData.mechandiserId, merchandiserName: selectedLineItemData.merchandiserName,
                        buyerName: selectedLineItemData.buyerName, buyerId: selectedLineItemData.buyerId, totalQtyAllocatedInLine: 0, estimatedWorkingDays: null, totalQtyToProduced: selectedLineItemData.lineItemOrderedQty,
                        lineItemNo: selectedLineItemData.lineItemNo, apm: selectedLineItemData.prodManagerName, sam: selectedLineItemData.lineItemSAM, noOfMachineToUse: maxAllowedMachines, lineItemDesc: selectedLineItemData.lineItemDesc, startDateTime: null, endDateTime: null
                    }, isEdit: false, isDisabledSaveButton: selectedLineItemData.lineItemSAM == 0 ? true : false, maxLineQtyAllocated: remainingQtyToPlan, selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName")
                })
            }
        }
    }


    async getSaidDetailsOnChange(saidId, data) {
        let that = this;
        const { t, i18n } = that.props;
        const { model, isEdit, maxAllowedMachines, schedulerDataOnLoad, currrentDate, selectedPlantName, lineItemNo, index, selectedUnitName, selectedNameOfUnitForApi, selectedLineName, orderDetails } = that.state;
        let response;
        if (orderDetails.isInvokedFromPlanningHistory)
            response = (await this.linePlanningService.getSaidDetails(saidId, orderDetails.plantId, orderDetails.unitName));
        else
            response = (await this.linePlanningService.getSaidDetails(saidId, selectedPlantName, selectedNameOfUnitForApi));

        if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
            that.setState({
                selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"),
                model: new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, saidId, null, null, model.prodLineId, null, model.productCategoryId, null, "", "", model.lineName, "", "", null, null, null, 0, null, null, null, null, maxAllowedMachines, null, null), lineItemNo: response.data.data, isEdit: false
            })
            global.hideLoader();
        }
        else {  // SOMETHING WENT WRONG
            that.setState({
                selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"),
                model: new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, saidId, null, null, model.prodLineId, null, model.productCategoryId, null, "", "", model.lineName, "", "", null, null, null, 0, null, null, null, null, maxAllowedMachines, null, null), lineItemNo: [], isEdit: false
            })
            global.hideLoader();
            that.toastObj.show(that.state.toasterMessage[7]);
        }
    }


    async getClickedEventPlannedData(eventModel, args) {
        let that = this;
        const { t, i18n } = that.props;
        const { schedulerDataOnLoad, maxAllowedMachines, model, selectedPlantName, currrentDate, selectedUnitName } = that.state;
        let response = (await this.linePlanningService.getClickedEventPlannedData(eventModel, eventModel.prodLineId))
        if ((response != undefined) && (response.data.status == "success")) {
            let eventDetailFromDb = response.data.data;
            that.getSaidDetailsOnEventClick(eventDetailFromDb.saidId, args, eventDetailFromDb)
        } else {
            that.toastObj.show(that.state.toasterMessage[2]);
            that.scheduleObj.closeEditor();
            that.setState({
                selectedEventColor: null, isEdit: false, argsData: null, selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"),
                model: new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, null, null, null, model.prodLineId, null, null, null, "", "", model.lineName, "", "", null, null, null, 0, null, null, null, null, maxAllowedMachines, null, null)
            }, () => {
                global.hideLoader();
            })
        }
    }


    async savePlannedData() {
        let that = this;
        const { t, i18n } = that.props;
        const { selectedProdLineId, selectedProdLineMonth, isDirtyOnEdit, selectedProdLineYear, selectedProductCategoryId, model } = that.state;
        let response = (await that.linePlanningService.savePlannedData(that.state.plannedDataModel))
        if ((response != undefined) && (response.data.status == "success")) {
            that.toastObj.show(that.state.toasterMessage[3]);;
            that.getNewPlannedEvents(selectedProdLineId, selectedProdLineMonth, selectedProdLineYear)
            that.setState({
                isValidateMachines: false, extraMachines: null, isRefreshAllLines: true, selectedEventColor: null, deletedEventsId: [], isDirtyOnEdit: false, selectedEventIdForEdit: null, isDisabledSaveButton: false, isEdit: false, selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"),
                model: new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, null, null, null, selectedProdLineId, null, selectedProductCategoryId, null, "", "", "", "", "", null, null, null, 0, null, null, null, null, null, null, null)
            })
            // global.hideLoader();
        } else if ((response != undefined) && (response.data.status == "fail")) {
            this.setState({ isDisabledSaveButton: false })
            global.hideLoader()
            that.toastObj.show(that.createToaster(t(errorMessages[response.data.data.errorCode]), "Error"));
        }
        else {
            // CODE TO REVERT CHANGES IN CASE OF SWR
            that.toastObj.show(that.state.toasterMessage[2]);
            that.getNewPlannedEvents(selectedProdLineId, selectedProdLineMonth, selectedProdLineYear)
            that.setState({
                isValidateMachines: false, extraMachines: null, isDisabledSaveButton: false, isDirtyOnEdit: false, deletedEventsId: [], selectedEventIdForEdit: null, isEdit: false, selectedEventColor: null, selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"),
                model: new PlanningDetailsModel(null, model.fmId, model.fmName, null, null, null, null, null, null, selectedProdLineId, null, selectedProductCategoryId, null, "", "", "", "", "", null, null, null, 0, null, null, null, null, null, null, null)
            })
            global.hideLoader();
        }
    }


    async getSummeryViewData(plantList, viewType, targetYear) {
        let response = (await this.linePlanningService.getSummeryViewData(plantList, this.state.isCategoryView, viewType, targetYear))
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            if (this.state.isCategoryView) {
                this.setState({ summaryDataCategory: response.data.data }, () => {
                    this.createSummaryViewCategory()
                })
            } else {
                this.setState({ summaryDataPlants: response.data.data }, () => {
                    this.createSummaryViewPlants()
                })
            }
        }
        else {
            this.toastObj.show(this.state.toasterMessage[2]);
            global.hideLoader()
        }
    }


    //--------------------------------------------------------ON-CHANGE-HANDLERS------------------------------------------------------//

    onChangePlant = (event) => {
        this.setState({
            selectedPlantName: event.value,
            model: {
                ...this.state.model, [event.element.id]: event.itemData.id, plantName: event.itemData.name
            }
        }, () => {
            if (event.e != undefined) {
                this.setState({ units: [], selectedUnitName: null, selectedYear: null, selectedNameOfUnitForApi: null, unitProductionLines: [], dynamicPivotGrids: [] }, () => {
                    global.showLoader();
                    this.getUnitList(event.value);
                    // this.getSaidNo(event.value);
                })
            }
        })
    }

    onChangeUnits = (event) => {
        const { selectedPlantName } = this.state;
        if (event.e != undefined) {
            global.showLoader();
            this.setState({ unitProductionLines: [], targetYear: null, selectedYear: null })
            this.getUnitProductionLines(event.value);
            // this.getLinePlanningDetails(0, event.value, null, null);
            this.getSaidNo(selectedPlantName, event.itemData.name);
            this.setState({ selectedUnitName: event.value, selectedNameOfUnitForApi: event.itemData.name, model: { ...this.state.model, [event.element.id]: event.itemData.id, unitName: event.itemData.name } })

        }
    }

    onChangeYear = (event) => {
        const { selectedUnitName, selectedLineName } = this.state;
        if (event.event != undefined) {
            global.showLoader();
            if (selectedUnitName != null && event.value != null) {
                if (selectedLineName != null)
                    this.getLinePlanningDetails(selectedLineName, selectedUnitName, null, null, event.value.getFullYear());
                else {
                    this.setState({ selectedLineName: 0 });
                    this.getLinePlanningDetails(0, selectedUnitName, null, null, event.value.getFullYear());
                }
            } else {
                global.hideLoader();
            }
            this.setState({ selectedYear: event.value, targetYear: event.value != null ? event.value.getFullYear() : null })
        }
    }

    onChangeSummaryYear = (event) => {
        let that = this;
        const { multiSelectedPlants, isMonthlyView, selectedLineName } = this.state;
        if (event.event != undefined) {
            global.showLoader();
            if (multiSelectedPlants != null && event.value != null) {
                that.getSummeryViewData(multiSelectedPlants, isMonthlyView, event.value.getFullYear());
            }
            else {
                global.hideLoader();
            }
            this.setState({ summarySelectedYear: event.value })
        }
    }

    onChangeProductionLine = (event) => {
        const { selectedYear } = this.state;
        if (event.e != undefined) {
            if (selectedYear == null) {
                this.toastObj.show({ title: 'Info!', content: "Please select Year!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
                this.setState({ selectedLineName: event.value })
                return;
            }
            else {
                global.showLoader();
                this.state.dynamicPivotGrids = []
                this.getLinePlanningDetails(event.value, null, null, null, selectedYear.getFullYear());
            }
        }
        this.setState({ selectedLineName: event.value })
    }


    //----------------------------------------------------SIDE-BAR-EVENTS-----------------------------------------------------//

    onChangeSidebarDropdown = (event) => {
        let that = this;
        const { index, isEdit, schedulerDataOnLoad, model } = that.state;
        if (event.e != null) {
            if (isEdit) {
                let clickedEventObj = Object.assign({}, schedulerDataOnLoad[index])
                clickedEventObj.plannedData[event.element.id] = event.value;
                let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEventObj })
                that.setState({ schedulerDataOnLoad: modifiedArray, isDirtyOnEdit: true })
            } else {
                that.setState({ model: { ...model, [event.element.id]: event.value } })
            }
        }
    }

    onChangeSidebarSaidDropdown = (event) => {
        let that = this;
        if (event.e != undefined) {
            global.showLoader();
            that.getSaidDetailsOnChange(event.value)
        }
    }


    onChangeLineItemNoSidebarDropdown = (event) => {
        this.getLineItemDetails(event);
    }


    onChangeNoOfMachines = (event) => {
        let that = this;
        const { schedulerDataOnLoad, index, isEdit, model, isDirtyOnEdit } = that.state;
        if ((event.syntheticEvent != null) && (event.syntheticEvent != undefined)) {
            if (isEdit) {
                let clickedEventObj = Object.assign({}, schedulerDataOnLoad[index])
                clickedEventObj.plannedData[event.target.id] = event.value;
                let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEventObj })
                that.setState({ schedulerDataOnLoad: modifiedArray, isDirtyOnEdit: true }, () => {
                    that.estimateReqWorkingDays('machin');
                })
            }
            else {
                that.setState({ model: { ...model, [event.target.id]: event.value } }, () => {
                    that.estimateReqWorkingDays('machin');
                })
            }
        }
    }


    onChangeSideBarNumericText = (event) => {
        let that = this;
        const { schedulerDataOnLoad, index, isEdit, model, isDirtyOnEdit } = that.state;
        if ((event.syntheticEvent != null) && (event.syntheticEvent != undefined)) {
            if (isEdit) {
                let clickedEventObj = Object.assign({}, schedulerDataOnLoad[index])
                let remainingQty = event.value;
                if (event.syntheticEvent.type == "change") {
                    let oldQty = clickedEventObj.plannedData.totalQtyAllocatedInLine;
                    let balancedQty = oldQty - event.value;
                    remainingQty = clickedEventObj.plannedData.remainingQtyToPlan + balancedQty;
                } else {
                    if (event.syntheticEvent.toElement.title == "Increment value") {
                        remainingQty = clickedEventObj.plannedData.remainingQtyToPlan - 1;
                    }
                    if (event.syntheticEvent.toElement.title == "Decrement value") {
                        remainingQty = clickedEventObj.plannedData.remainingQtyToPlan + 1;
                    }
                }
                clickedEventObj.plannedData[event.target.id] = event.value;
                clickedEventObj.plannedData.remainingQtyToPlan = remainingQty;
                let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEventObj })
                that.setState({ schedulerDataOnLoad: modifiedArray, isDirtyOnEdit: true }, () => {
                    that.estimateReqWorkingDays();
                })
            }
            else {
                let remainingQty = event.value;
                if (event.syntheticEvent.type == "change") {
                    let oldQty = model.totalQtyAllocatedInLine;
                    let balancedQty = oldQty - event.value;
                    remainingQty = model.remainingQtyToPlan + balancedQty;
                } else {
                    if (event.syntheticEvent.toElement.title == "Increment value") {
                        remainingQty = model.remainingQtyToPlan - 1;
                    }
                    if (event.syntheticEvent.toElement.title == "Decrement value") {
                        remainingQty = model.remainingQtyToPlan + 1;
                    }
                }
                that.setState({ model: { ...model, [event.target.id]: event.value, remainingQtyToPlan: remainingQty } }, () => {
                    that.estimateReqWorkingDays('lineQTY');
                })
            }
        }
    }


    async estimateReqWorkingDays(getopt) {
        const { model, schedulerDataOnLoad, maxAllowedWorkingHours, index, currrentDate } = this.state;
        try {
            if ((this.state.isEdit) && ((schedulerDataOnLoad[index].plannedData.sam != null) && (schedulerDataOnLoad[index].plannedData.sam != 0)) && ((schedulerDataOnLoad[index].plannedData.totalQtyAllocatedInLine != null) && (schedulerDataOnLoad[index].plannedData.totalQtyAllocatedInLine != 0)) && ((schedulerDataOnLoad[index].plannedData.noOfMachineToUse != null) && (schedulerDataOnLoad[index].plannedData.noOfMachineToUse != 0))) {   // Edit mode
                let clickedEventObj = Object.assign({}, schedulerDataOnLoad[index])
                let estimatedDays = await this.getEstimatedWorkingDays(null, clickedEventObj.plannedData.lineItemNo, clickedEventObj.plannedData.noOfMachineToUse, clickedEventObj.plannedData.prodLineId, clickedEventObj.plannedData.totalQtyAllocatedInLine, clickedEventObj.plannedData.saidId, clickedEventObj.plannedData.sam, clickedEventObj.plannedData.startDateTime);
                let ceilDay = Math.ceil(estimatedDays.workingDays + estimatedDays.holidays);
                ceilDay = ceilDay > 0 ? ceilDay - 1 : 0
                let estimatedEndDate = this.getEndDateExcludingWeekend(new Date((clickedEventObj.plannedData.startDateTime).getFullYear(), (clickedEventObj.plannedData.startDateTime).getMonth(), (clickedEventObj.plannedData.startDateTime).getDate()), ceilDay);
                clickedEventObj.plannedData.endDateTime = estimatedEndDate;
                clickedEventObj.plannedData.estimatedWorkingDays = estimatedDays.workingDays;
                let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEventObj })
                this.setState({ schedulerDataOnLoad: modifiedArray, minStartDate: clickedEventObj.plannedData.startDateTime })
            }
            if ((!this.state.isEdit) && ((model.sam != null) && (model.sam != 0)) && ((model.totalQtyAllocatedInLine != null) && (model.totalQtyAllocatedInLine != 0)) && ((model.noOfMachineToUse != null) && (model.noOfMachineToUse != 0))) {  // normal mode
                let startDateTime = this.getStartDateExcludingWeekend(currrentDate);
                let estimatedDays = await this.getEstimatedWorkingDays(null, model.lineItemNo, model.noOfMachineToUse, model.prodLineId, model.totalQtyAllocatedInLine, model.saidId, model.sam, startDateTime);
                let ceilDay = Math.ceil(estimatedDays.workingDays + estimatedDays.holidays);
                ceilDay = ceilDay > 0 ? ceilDay - 1 : 0
                let endDateTime = this.getEndDateExcludingWeekend(new Date(currrentDate.getFullYear(), currrentDate.getMonth(), currrentDate.getDate()), ceilDay);
                if(getopt == 'lineQTY'){
                    this.setState({ model: { ...model, estimatedWorkingDays: estimatedDays.workingDays, startDateTime, endDateTime }, minStartDate: startDateTime })
                }else{
                    this.setState({ model: { ...model, estimatedWorkingDays: estimatedDays.workingDays }, minStartDate: startDateTime })
                }
            }
        } catch (error) {
            this.toastObj.show(this.state.toasterMessage[2]);
        }

    }


    async onChangeSideBarDatePicker(args, that) {
        //  let that=this;
        const { isEdit, model, index, schedulerDataOnLoad, minStartDate, isDirtyOnEdit } = that.state;
        try {
            if (((args.event != null) && (args.value != null)) || (('values' in args) && (args.value != null))) {  // Change + Today
                if (isEdit) {
                    let clickedEventObj = Object.assign({}, schedulerDataOnLoad[index])
                    let isDirty = ((args.event != null) && ('values' in args)) ? true : false;
                    if ((clickedEventObj.plannedData.estimatedWorkingDays != null) && (args.element.id == "startDateTime")) {
                        let estimatedDays = await that.getEstimatedWorkingDays(null, clickedEventObj.plannedData.lineItemNo, clickedEventObj.plannedData.noOfMachineToUse, clickedEventObj.plannedData.prodLineId,
                            clickedEventObj.plannedData.totalQtyAllocatedInLine, clickedEventObj.plannedData.saidId, clickedEventObj.plannedData.sam, that.getStartDateExcludingWeekend(args.value));
                        let ceilDay = Math.ceil(estimatedDays.workingDays + estimatedDays.holidays);
                        ceilDay = ceilDay > 0 ? ceilDay - 1 : 0;
                        let estimatedEndDate = that.getEndDateExcludingWeekend(new Date((args.value).getFullYear(), (args.value).getMonth(), (args.value).getDate()), ceilDay);
                        clickedEventObj.plannedData.startDateTime = that.getStartDateExcludingWeekend(args.value);
                        clickedEventObj.plannedData.endDateTime = estimatedEndDate;
                        let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEventObj })
                        that.setState({ schedulerDataOnLoad: modifiedArray, minStartDate: clickedEventObj.plannedData.startDateTime, isDirtyOnEdit: isDirty })
                    } else if (args.element.id == "startDateTime") {  // For validation
                        clickedEventObj.plannedData[args.element.id] = that.getStartDateExcludingWeekend(args.value);
                        let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEventObj })
                        that.setState({ schedulerDataOnLoad: modifiedArray, minStartDate: clickedEventObj.plannedData.startDateTime, isDirtyOnEdit: isDirty })
                    }
                    else {
                        let endDate = new Date((args.value).getFullYear(), (args.value).getMonth(), (args.value).getDate(), 23, 59)
                        clickedEventObj.plannedData[args.element.id] = endDate;
                        let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEventObj })
                        that.setState({ schedulerDataOnLoad: modifiedArray, isDirtyOnEdit: isDirty })
                    }
                } else {
                    if ((model.estimatedWorkingDays != null) && (args.element.id == "startDateTime")) {
                        let estimatedDays = await that.getEstimatedWorkingDays(null, model.lineItemNo, model.noOfMachineToUse, model.prodLineId, model.totalQtyAllocatedInLine, model.saidId,
                            model.sam, that.getStartDateExcludingWeekend(args.value));
                        let ceilDay = Math.ceil(estimatedDays.workingDays + estimatedDays.holidays);
                        console.log(estimatedDays.workingDays);
                        console.log(estimatedDays.holidays);
                        console.log(ceilDay);
                        ceilDay = ceilDay > 0 ? ceilDay - 1 : 0
                        let estimatedEndDate = that.getEndDateExcludingWeekend(new Date((args.value).getFullYear(), (args.value).getMonth(), (args.value).getDate()), ceilDay);
                        that.setState({ model: { ...model, startDateTime: that.getStartDateExcludingWeekend(args.value), endDateTime: estimatedEndDate }, minStartDate: args.value })
                    }
                    else if (args.element.id == "startDateTime") {   // For validation
                        that.setState({ model: { ...model, [args.element.id]: that.getStartDateExcludingWeekend(args.value) }, minStartDate: args.value })
                    }
                    else {
                        let endDate = new Date((args.value).getFullYear(), (args.value).getMonth(), (args.value).getDate(), 23, 59)
                        that.setState({ model: { ...model, [args.element.id]: endDate } })
                    }
                }
            } else if ((args.event != null) && (args.value == null)) {
                if (isEdit) {
                    let clickedEventObj = Object.assign({}, schedulerDataOnLoad[index])
                    clickedEventObj.plannedData.startDateTime = null;
                    clickedEventObj.plannedData.endDateTime = null;
                    let modifiedArray = Object.assign([], schedulerDataOnLoad, { [index]: clickedEventObj })
                    that.setState({ schedulerDataOnLoad: modifiedArray })
                } else {
                    that.setState({ model: { ...model, startDateTime: null, endDateTime: null } })
                }

            }
        } catch (error) {
            that.toastObj.show(that.state.toasterMessage[2]);
        }

    }

    //-----------------------------------------------------GRID-EVENTS---------------------------------------------------------//

    // onResetPlanningData = (event) => {
    //     global.showLoader();
    //     this.getLinePlanningDetails(this.state.selectedLineName, null, "refresh", event.target.name);
    // }


    onCellClick = (event, selectedProdLineId, selectedLineName) => {
        let that = this;
        const { t, i18n } = that.props;
        const { planningHistoryProdLines, productionCategory, orderDetails, selectedYear } = that.state;

        let year = selectedYear;
        if (year == null) {
            year = new Date().getFullYear();
        }
        let selectedMonth;
        let prodCategoryIdObject;
        if (orderDetails.isInvokedFromPlanningHistory) {
            let lineObj = planningHistoryProdLines.find(function (element) {
                return element.id == selectedProdLineId;
            });
            prodCategoryIdObject = productionCategory.find(function (item) {
                return item.name == lineObj.categories[0].name;
            });
            that.setState({
                model: { ...that.state.model, lineName: selectedLineName, prodLineId: selectedProdLineId, productCategoryId: prodCategoryIdObject.id },
                lineNameToRefresh: null, deletedEventsId: [], selectedProductCategoryId: prodCategoryIdObject.id, selectedProdLineId: selectedProdLineId,
                selectedProdLineMonth: null, selectedProdLineYear: year
            }, () => {
                that.getSelectedMonthPlannedEvents(selectedProdLineId, null, year, new Date(), lineObj);
            })
        }
        else {
            if ((event.data != undefined) && (event.data.columnHeaders != undefined)) {
                selectedMonth = (event.data.columnHeaders.indexOf('.') !== -1) ? event.data.columnHeaders.substr(0, event.data.columnHeaders.indexOf('.')) : event.data.columnHeaders;
            }
            if ((event.data != undefined) && (event.data.rowHeaders != undefined)) {
                let selectedYear;
                let selectedMonthNo;
                that.state.linePlanningDetails.map(linePlanningDetail => {
                    if (linePlanningDetail.prodLineId == selectedProdLineId) {
                        prodCategoryIdObject = that.state.productionCategory.find(function (item) {
                            return item.name == linePlanningDetail.lineCategory;
                        })
                        linePlanningDetail.linePlanningForMonthDtos.map(linePlanningForMonthDto => {
                            if (linePlanningForMonthDto.monthName == selectedMonth) {
                                selectedYear = linePlanningForMonthDto.year;
                                selectedMonthNo = linePlanningForMonthDto.monthNumber;
                            }
                        })
                    }
                })
                that.setState({ model: { ...that.state.model, lineName: selectedLineName, prodLineId: selectedProdLineId, productCategoryId: prodCategoryIdObject.id }, selectedProductCategoryId: prodCategoryIdObject.id, lineNameToRefresh: selectedLineName, deletedEventsId: [], selectedProdLineId: selectedProdLineId, selectedProdLineMonth: selectedMonth, selectedProdLineYear: selectedYear }, () => {
                    that.getSelectedMonthPlannedEvents(selectedProdLineId, selectedMonth, selectedYear, new Date(selectedYear, selectedMonthNo - 1), false);
                })
            }
        }

    }


    convertPivotData(linePlanningMonthsData) {
        let modifiedLinePlanningDataForGrid = [];
        for (let i = 0; i < linePlanningMonthsData.length; i++) {
            for (let j = 0; j < linePlanningMonthsData[i].linePlanningForWeekDtos.length; j++) {
                modifiedLinePlanningDataForGrid = modifiedLinePlanningDataForGrid.concat(linePlanningMonthsData[i].linePlanningForWeekDtos[j].linePlanningWeekInfoDtos)
            }
        }
        return modifiedLinePlanningDataForGrid;
    }


    createPivotGrid() {
        const { t, i18n } = this.props;
        const { linePlanningDetails } = this.state;
        let dynamicPivotGridLinesDetails = [];
        for (let i = 0; i < linePlanningDetails.length; i++) {
            dynamicPivotGridLinesDetails.push(
                <div key={i.toString()}>
                    <div className="mt-2"></div>
                    <Row>
                        <Col lg='12'>
                            <Card>
                                <CardHeader>
                                    <Row>
                                        <Col md="3" sm="4" xs="4" className="pt-1">Line Name : <b>{linePlanningDetails[i].lineName}</b> </Col>
                                        <Col md="2" sm="4" xs="4" className="pt-1">Catg : <b>{linePlanningDetails[i].lineCategory}</b> </Col>
                                        <Col md="2" sm="4" xs="4" className="pt-1">Machines : <b>{linePlanningDetails[i].maxNoOfMachAllowedInLine}</b> </Col>
                                        <Col md="2" sm="4" xs="4" className="pt-1"> Work.Hrs : <b>{linePlanningDetails[i].numberOfLineWorkingHours}</b> </Col>

                                        {/* <Col md="3" sm="3" xs="4" className="text-right">
                                            <a className="icon-button reset-planning-data ml-1 mr-1" id={linePlanningDetails[i].prodLineId} name={linePlanningDetails[i].lineName} onClick={this.onResetPlanningData} title="Reset Planning Data"></a>
                                        </Col> */}
                                    </Row>
                                </CardHeader>
                                <CardBody>
                                    <div className="clearfix"></div>
                                    <PivotViewComponent className="line-planning-cal"
                                        locale={i18n.language}
                                        ref={(pivotview) => { this[linePlanningDetails[i].lineName] = pivotview; }}
                                        id={"lineNameEnds" + linePlanningDetails[i].prodLineId + "lineIdEnds" + i}   // FORMAT : lineNameabc-789_1
                                        dataSourceSettings={{
                                            enableSorting: false,
                                            columns: [{ name: "month", caption: "Month" }, { name: "week", caption: "Week" }],
                                            values: [{ name: "cellValue", caption: "Data" }],
                                            rows: [{ name: "rowLabel", caption: "Parameter" }],
                                            expandAll: false,
                                            showGrandTotals: false,
                                            filters: [],
                                            formatSettings: [{ name: 'cellValue', useGrouping: true }],
                                            // emptyCellsTextContent: '0',
                                            dataSource: this.convertPivotData(linePlanningDetails[i].linePlanningForMonthDtos),
                                            conditionalFormatSettings: this.conditionalFormattingPlanningView(linePlanningDetails[i])
                                        }}
                                        allowExcelExport={true}
                                        // editSettings={{ allowAdding: false, allowDeleting: false, allowEditing: true, mode: 'Normal' }}
                                        cellClick={(e) => { this.onCellClick(e, linePlanningDetails[i].prodLineId, linePlanningDetails[i].lineName, false) }}
                                        gridSettings={{
                                            allowSelection: true,
                                            selectionSettings: { mode: 'Cell', type: 'Single' }
                                        }}
                                        allowConditionalFormatting={true}
                                        showTooltip={false}>
                                        <Inject services={[ConditionalFormatting]} />
                                    </PivotViewComponent>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            )
        }
        this.setState({ dynamicPivotGrids: dynamicPivotGridLinesDetails })
        global.hideLoader();
    }


    conditionalFormattingPlanningView(linePlanningDetails) {
        const { t, i18n } = this.props;
        let conditionalFormatting = [];
        let namesOfFreezedColumnHeader = [];
        linePlanningDetails.linePlanningForMonthDtos.map(item => {
            if (item.isFreeze == true) {
                namesOfFreezedColumnHeader.push(item.monthName)
            }
        })
        linePlanningDetails.linePlanningForMonthDtos.map(item => {
            if (item.isFreeze == true) {
                conditionalFormatting.push(
                    {
                        label: item.monthName,
                        conditions: 'GreaterThan',
                        value1: -100000000000000,
                        //value2: 50000,
                        style: {
                            backgroundColor: '#bbb9b7',
                            text: 'freeze',
                            text2: namesOfFreezedColumnHeader
                        },
                    },
                )
            }
        })
        conditionalFormatting.push(
            {
                label: t("cpm.lm.line.plan.line.open.md") || t("cpm.lm.line.plan.line.open.hrs"),
                conditions: 'LessThan',
                value1: -1,
                // value2: 50000,
                style: {
                    backgroundColor: '#ff656d',
                    text: 'overloaded',
                    text2: namesOfFreezedColumnHeader
                }
            },
            {
                label: t("cpm.lm.line.plan.line.open.md") || t("cpm.lm.line.plan.line.open.hrs"),
                conditions: 'GreaterThan',
                value1: -1,
                //value2: -50000,
                style: {
                    backgroundColor: '#6dc76e',
                    text: 'editable',
                    text2: namesOfFreezedColumnHeader
                }
            })
        return conditionalFormatting;
    }

    //-----------------------------------------------------------SCHEDULER-EVENTS-----------------------------------------------------//


    onRenderCell = (args) => {
        let that = this;
        if ((args.elementType === 'monthCells') && (that.scheduleObj.currentView === 'Month') && (that.state.daywiseAvailableMachines.size > 0)) {
            let ele = document.createElement('div');
            ele.innerHTML = this.getAllocatedMachines(args.date);
            (args.element).appendChild(ele.firstChild);
        }
    }

    getAllocatedMachines = (value) => {
        const { daywiseAvailableMachines } = this.state;
        let machines = daywiseAvailableMachines.get(value.toDateString());
        if (machines != undefined)
            return '<div class="machine-cap">' + machines + '</div>';
        else
            return '<div></div>';
    }


    onNavigateSchedulerDate = (args) => {
        let that = this;
        if ((Number(new Date(that.state.minNavigationDate.getFullYear(), that.state.minNavigationDate.getMonth(), 0)) > Number(new Date(args.currentDate.getFullYear(), args.currentDate.getMonth(), 0)))) {
            args.cancel = true;
            that.toastObj.show(this.state.toasterMessage[0]);
        }
    }


    getSelectedEventIndex(args) {
        let that = this;
        let index;
        for (let i = 0; i < that.state.schedulerDataOnLoad.length; i++) {
            if ((that.state.schedulerDataOnLoad[i].saidId == args.data.saidId) && (that.state.schedulerDataOnLoad[i].id == args.data.id)) {
                index = i;
                break;
            }
        }
        return index;
    }


    onClickEventPopup = (args) => {
        let that = this;
        const { t, i18n } = that.props;
        const { model, maxAllowedMachines, currrentDate, isEdit, isDirtyOnEdit } = that.state;
        if (args.type == "QuickInfo") {
            args.cancel = true;
            if ((args.data.id != undefined) && (args.data.monthId != undefined) && (!args.data.isNewEvent) && (!isEdit)) {    // (API-CALL : when switch from Add to Edit). 
                that.state.isEdit = true;
                global.showLoader();
                that.getClickedEventPlannedData(args.data, args); // API-CALL  // *note : data will always refresh,every time user clicked on event. 
            }
            if ((args.data.id != undefined) && (args.data.monthId != undefined) && (!args.data.isNewEvent) && (isEdit) && (!isDirtyOnEdit)) {  // (API-CALL : when switch from Edit to Edit). 
                global.showLoader();
                that.getClickedEventPlannedData(args.data, args); // API-CALL  // *note : data will always refresh,every time user clicked on event. 
            }
            if ((args.data.id != undefined) && (args.data.monthId != undefined) && (!args.data.isNewEvent) && (isEdit) && (isDirtyOnEdit)) {  // (API-CALL : when switch from Edit to Edit & Dirty). 
                this.setState({ isShowConfirmationDialog: true, argsData: args, minStartDate: args.data.startDateTime })
            }
        }
    }


    setColorToEventOnUI = (args) => {
        let categoryColor = args.data.categoryColor;
        if (!args.element || !categoryColor) {
            return;
        }
        args.element.firstChild.style.borderLeftColor = categoryColor;
        args.element.style.backgroundColor = categoryColor;
    }


    addUniqueColorToEventObj = (args) => {
        if ((args.requestType == "eventCreated") && (args.addedRecords.length > 0)) {
            let uniqueColor = this.randomColorGenerator(args)
            args.addedRecords[0].categoryColor = uniqueColor;
        }
        if (args.requestType == "eventChanged") {
            this.setState({ schedulerDataOnLoad: this.state.schedulerDataOnLoad, refreshDate: false })
        }
    }


    randomColorGenerator(startDateTime) {
        var letters = '0123456789ABCDEF';
        var categoryColor = '#';
        for (var i = 0; i < 6; i++) {
            categoryColor += letters[Math.floor(Math.random() * 16)];
        }
        this.state.schedulerDataOnLoad.map(obj => {
            if ((obj.startDateTime.getMonth() == startDateTime.getMonth()) && (obj.categoryColor == categoryColor)) {
                this.randomColorGenerator(startDateTime);
            }
        })
        return categoryColor;
    }


    getPlannedEventData = () => {
        let that = this;
        that.setState({ isShowConfirmationDialog: false }, () => {
            global.showLoader();
            that.state.isEdit = true;
            that.getClickedEventPlannedData(that.state.argsData.data, that.state.argsData); // API-CALL  // *note : data will always refresh,every time user clicked on event. 
        })
    }


    addSchedulerDataToModel() {
        let copiedSchedulerDataOnLoad = this.state.schedulerDataOnLoad;
        let modifiedSchedulerDataToSetInModel = [];

        //Converting To Desired Date Format
        for (let i = 0; i < copiedSchedulerDataOnLoad.length; i++) {
            let newEventObject = new SchedulerDetailsModel(null, null, copiedSchedulerDataOnLoad[i].categoryColor, copiedSchedulerDataOnLoad[i].description, copiedSchedulerDataOnLoad[i].startDateTime, copiedSchedulerDataOnLoad[i].endDateTime, null, null, copiedSchedulerDataOnLoad[i].saidId, null, copiedSchedulerDataOnLoad[i].lineItem, null, copiedSchedulerDataOnLoad[i].prodLineId, copiedSchedulerDataOnLoad[i].plannedInfoId, copiedSchedulerDataOnLoad[i].plannedData, false)

            // DIFFERENCIATE BW NEW & OLD EVENT
            let monthId = null;
            if (copiedSchedulerDataOnLoad[i].isNewEvent == false) {
                newEventObject.id = copiedSchedulerDataOnLoad[i].id;
                monthId = copiedSchedulerDataOnLoad[i].monthId;
            }

            if ((copiedSchedulerDataOnLoad[i].isNewEvent == false) && (copiedSchedulerDataOnLoad[i].plannedData.id != null)) {
                newEventObject.startDateTime = newEventObject.plannedData.startDateTime;
                newEventObject.endDateTime = newEventObject.plannedData.endDateTime;
            }

            delete newEventObject.isNewEvent;
            delete newEventObject.monthId;
            delete newEventObject.oldSaidId;
            delete newEventObject.oldLineItem;
            delete newEventObject.oldStartDateTime;
            delete newEventObject.oldEndDateTime;
            delete newEventObject.plannedData.remainingQtyToPlan;

            // IF EVENT OBJECT EXIST MODIFY THAT MODEL
            let isAddedToExistedObj = false;
            for (let j = 0; j < modifiedSchedulerDataToSetInModel.length; j++) {
                if (((modifiedSchedulerDataToSetInModel[j].date).getFullYear() == (newEventObject.startDateTime).getFullYear()) && ((modifiedSchedulerDataToSetInModel[j].date).getMonth() == (newEventObject.startDateTime).getMonth())) {
                    if (((newEventObject.id == null) || (newEventObject.id == this.state.selectedEventIdForEdit)) && (this.state.deletedEventsId.length == 0)) {
                        modifiedSchedulerDataToSetInModel[j].dayWiseWorkLoad.push(newEventObject);
                        isAddedToExistedObj = true;
                    }
                    break;
                }
            }
            // IF EVENT OBJECT DOESN'T EXIST CREATE NEW MODEL
            if (modifiedSchedulerDataToSetInModel.length == 0) {
                isAddedToExistedObj = true;
                let dayWiseWorkLoadData = [];
                if (((newEventObject.id == null) || (newEventObject.id == this.state.selectedEventIdForEdit)) && (this.state.deletedEventsId.length == 0)) {
                    dayWiseWorkLoadData.push(newEventObject)
                    let newSchedulerModel = new SchedulerModel(monthId, copiedSchedulerDataOnLoad[i].startDateTime, copiedSchedulerDataOnLoad[i].prodLineId, dayWiseWorkLoadData)
                    modifiedSchedulerDataToSetInModel.push(newSchedulerModel);
                }
            }
            if (!isAddedToExistedObj) {
                let dayWiseWorkLoadData = [];
                if (((newEventObject.id == null) || (newEventObject.id == this.state.selectedEventIdForEdit)) && (this.state.deletedEventsId.length == 0)) {
                    dayWiseWorkLoadData.push(newEventObject)
                    let newSchedulerModel = new SchedulerModel(monthId, copiedSchedulerDataOnLoad[i].startDateTime, copiedSchedulerDataOnLoad[i].prodLineId, dayWiseWorkLoadData)
                    modifiedSchedulerDataToSetInModel.push(newSchedulerModel);
                }
            }
        }
        this.setState({ isDisabledSaveButton: true, isShowConfirmationDialog: false, plannedDataModel: { ...this.state.plannedDataModel, schedulerData: modifiedSchedulerDataToSetInModel, deletedEventsId: this.state.deletedEventsId } }, () => {
            global.showLoader()
            this.savePlannedData();
        })
        console.log(modifiedSchedulerDataToSetInModel);
    }

    //-----------------------------------------------------------------CRUD-EVENTS--------------------------------------------------------------//


    onApply = () => {
        let that = this;
        const { startDateTime, endDateTime, lineItemNo, lineItemDesc, totalQtyAllocatedInLine, index, maxAllowedMachines, prodLineId, saidId } = that.state.model;
        if (that.state.isEdit) {
            that.addSchedulerDataToModel()
            that.scheduleObj.refresh()
        } else {
            let eventName = lineItemDesc + "- Qty " + totalQtyAllocatedInLine;
            let uniqueColor = this.randomColorGenerator(startDateTime)
            let newEvent = new SchedulerDetailsModel(null, null, uniqueColor, eventName, startDateTime, endDateTime, startDateTime, endDateTime, saidId, saidId, lineItemNo, lineItemNo, prodLineId, null, that.state.model, true)
            that.state.schedulerDataOnLoad.push(newEvent);
            that.addSchedulerDataToModel();
        }
    }


    onCreateNewEvent = () => {
        let that = this;
        const { t, i18n } = that.props;
        that.setState({
            selectedEventColor: null, isDirtyOnEdit: false, argsData: null, selectedEventIdForEdit: null, isDisabledSaveButton: false, isEdit: false, selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"),
            model: new PlanningDetailsModel(null, that.state.model.fmId, that.state.model.fmName, null, null, null, null, null, null, that.state.selectedProdLineId, null, that.state.selectedProductCategoryId, null, "", "", "", "", "", null, null, null, 0, null, null, null, null, null, null, null)
        })
    }


    onDelete = () => {
        let that = this;
        that.state.deletedEventsId = [];
        that.state.deletedEventsId.push(that.state.selectedEventIdForEdit);
        that.addSchedulerDataToModel()
    }


    sideBar = (args) => {
        let that = this;
        const { t, i18n, selectedYear } = that.props;
        if (this.state.isRefreshAllLines && this.state.lineNameToRefresh != null && this.state.orderDetails.isInvokedFromPlanningHistory == false) {
            global.showLoader()
            this.getLinePlanningDetails(this.state.selectedLineName, null, "refresh", this.state.lineNameToRefresh, new Date().getFullYear())
        }
        this.setState({
            orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: false, plantId: null, unitId: null, lineName: null, prodLineId: null },
            deletedEventsId: [], argsData: null, selectedEventColor: null, isDirtyOnEdit: false, selectedEventName: t("cpm.lm.line.plan.schedulerHeader.selected.eventName"), selectedProductCategoryId: null, isSidebar: false, isEdit: false, index: null,
            model: new PlanningDetailsModel(null, that.state.model.fmId, that.state.model.fmName, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", null, null, null, 0, null, null, new Date(), new Date(), null, null, null), maxAllowedMachines: null, maxAllowedWorkingHours: null, lineItemNo: [], isEdit: false
        })
        document.body.classList.toggle('overflow-hidden-body');
    }
    //-----------------------------------------------------------------OTHER-EVENTS------------------------------------------------------------//

    createToaster(message, type) {
        switch (type) {
            case "Success":
                return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
            case "Error":
                return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
            case "Warning":
                return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
            case "Information":
                return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
    }

    cancelConfirmDialog = () => {
        this.setState({ isShowConfirmationDialog: false, argsData: null })
    }


    disableDatePickerDate = (args) => {
        if (args.date.getDay() === 0) {
            args.isDisabled = true;
        }
    }

    getEndDateExcludingWeekend(startDate, noOfDaysToAdd) {
        let endDateExcludingWeekend = null;
        let count = 0;
        while (count < noOfDaysToAdd) {
            endDateExcludingWeekend = new Date(startDate.setDate(startDate.getDate() + 1));
            endDateExcludingWeekend.setHours(23)
            endDateExcludingWeekend.setMinutes(59)
            // if (endDateExcludingWeekend.getDay() != 0) {
            count++;
            // }
        }
        if (noOfDaysToAdd == 0) {
            endDateExcludingWeekend = startDate;
            endDateExcludingWeekend.setHours(23)
            endDateExcludingWeekend.setMinutes(59)
        }
        return endDateExcludingWeekend;
    }

    getStartDateExcludingWeekend(currentDateFromPivotCell) {
        let startDate = currentDateFromPivotCell;
        // if (startDate.getDay() == 0) {
        //     startDate = new Date(startDate.setDate(startDate.getDate() + 1));
        // }
        startDate.setHours(9);
        return startDate;
    }

    validateMachinesBeforeApply = () => {
        if (this.isValidateMandatoryFields()) {
            let machines = this.validateMachines();
            if (machines > 0)
                this.setState({ isShowConfirmationDialog: true, isValidateMachines: true, extraMachines: machines })
            else
                this.onApply()
        }
    }

    validateMachines = () => {
        const { model, isEdit, index, schedulerDataOnLoad } = this.state;
        if (isEdit) {
            let editedEvent = Object.assign({}, schedulerDataOnLoad[index])
            let startDate = Date.parse(editedEvent.plannedData.startDateTime.toDateString());
            let endDate = Date.parse(editedEvent.plannedData.endDateTime.toDateString());
            return this.getExtraMachines(startDate, endDate, editedEvent.plannedData.noOfMachineToUse);
        } else {
            let startDate = Date.parse(model.startDateTime.toDateString());
            let endDate = Date.parse(model.endDateTime.toDateString());
            return this.getExtraMachines(startDate, endDate, model.noOfMachineToUse);
        }

    }

    getExtraMachines = (startDate, endDate, noOfMachineToUse) => {
        const { daywiseAvailableMachines } = this.state;
        let extraMachines = 0;
        for (const entry of daywiseAvailableMachines.entries()) {
            let schedulerDate = Date.parse(entry[0]);
            if ((startDate <= schedulerDate) && (endDate >= schedulerDate)) {
                if (noOfMachineToUse > entry[1]) {
                    extraMachines = noOfMachineToUse - entry[1];
                    break;
                }
            }
        }
        return extraMachines;
    }


    isValidateMandatoryFields() {
        let isValidate = true;
        let date = new Date();
        const { model, isEdit, index, schedulerDataOnLoad } = this.state;
        if (isEdit) {
            let editedEvent = Object.assign({}, schedulerDataOnLoad[index])
            if (editedEvent.plannedData.totalQtyAllocatedInLine == 0) {
                this.toastObj.show(this.state.toasterMessage[11]);
                isValidate = false;
                return isValidate;
            }
            else if ((editedEvent.plannedData.saidId == null) || (editedEvent.plannedData.lineItemNo == null) || (editedEvent.plannedData.totalQtyAllocatedInLine == null) || (editedEvent.plannedData.noOfMachineToUse == null) || (editedEvent.plannedData.productCategoryId == null) || (editedEvent.plannedData.sam == null) || (editedEvent.plannedData.apm == null) || (editedEvent.plannedData.startDateTime == null) || (editedEvent.plannedData.endDateTime == null)) {
                this.toastObj.show(this.state.toasterMessage[4]);
                isValidate = false;
                return isValidate;
            }
            else if (new Date((editedEvent.plannedData.startDateTime).toDateString()) < new Date(date.getFullYear(), date.getMonth(), date.getDate())) {
                this.toastObj.show(this.state.toasterMessage[12]);
                isValidate = false;
                return isValidate;
            } else {
                return isValidate;
            }
        } else {
            if (model.sam == 0) {
                this.toastObj.show(this.state.toasterMessage[10]);
                isValidate = false;
                return isValidate;
            } else if (model.totalQtyAllocatedInLine == 0) {
                this.toastObj.show(this.state.toasterMessage[11]);
                isValidate = false;
                return isValidate;
            }
            else if ((model.saidId == null) || (model.lineItemNo == null) || (model.totalQtyAllocatedInLine == null) || (model.noOfMachineToUse == null) || (model.productCategoryId == null) || (model.sam == null) || (model.apm == null) || (model.startDateTime == null) || (model.endDateTime == null)) {
                this.toastObj.show(this.state.toasterMessage[4]);
                isValidate = false;
                return isValidate;
            }
            else if (new Date((model.startDateTime).toDateString()) < new Date(date.getFullYear(), date.getMonth(), date.getDate())) {
                this.toastObj.show(this.state.toasterMessage[12]);
                isValidate = false;
                return isValidate;
            }
            else {
                return isValidate;
            }
        }
    }

    onClickPlanning(){
        this.setState({
            showDialog: true
        });
    }
    dialogClose() {
        this.setState({
            showDialog: false,
        });
    }

    returnChangedLineFromPivot(lineNameToRefresh, data) {
        let lineItemDataToRefresh = data.filter(function (item) {
            return item.lineName == lineNameToRefresh;
        })
        return lineItemDataToRefresh[0];
    }

    //----------------------------------------------------------SUMMARY-VIEW-----------------------------------------------------------//


    switchSummaryView = () => {
        let that = this;
        that.setState({ isSummaryView: !that.state.isSummaryView, dynamicSummaryViewPlants: [], dynamicSummaryViewCategory: [], isMonthlyView: "month" }, () => {
            if ((that.state.isSummaryView) && (that.state.multiSelectedPlants.length > 0) && (that.state.summarySelectedYear)) {
                global.showLoader()
                that.getSummeryViewData(that.state.multiSelectedPlants, "month", that.state.summarySelectedYear.getFullYear())
            }
        })
    }


    switchView = (isCategory) => {
        let that = this;
        that.setState({ isCategoryView: isCategory, dynamicSummaryViewPlants: [], dynamicSummaryViewCategory: [], isMonthlyView: "month" }, () => {
            if ((that.state.isSummaryView) && (that.state.multiSelectedPlants.length > 0) && (that.state.summarySelectedYear)) {
                global.showLoader()
                that.getSummeryViewData(that.state.multiSelectedPlants, "month", that.state.summarySelectedYear.getFullYear())
            }
        })
    }


    onChangeMultiSelectPlant = (event) => {
        let that = this;
        that.setState({ selectedPlantList: event.value, dynamicSummaryViewPlants: [], dynamicSummaryViewCategory: [], multiSelectedPlants: event.value, summarySelectedYear: null })
    }


    createSummaryViewPlants() {
        const { summaryDataPlants, isMonthlyView } = this.state;
        let dynamicSummaryViewPlants = [];
        let gridColumns = isMonthlyView == "month" ? [{ name: 'month' }] : [{ name: 'month' }, { name: 'week' }];
        let modifiedSummaryData = [];
        summaryDataPlants.map(item => {
            modifiedSummaryData = modifiedSummaryData.concat(item);
        })
        dynamicSummaryViewPlants.push(
            <div key={Math.random()}>
                <PivotViewComponent
                    gridSettings={this.gridSettings}
                    ref={d => this.summeryPlantGridObj = d}
                    id={"summeryViewPlants"}
                    height={700}
                    allowConditionalFormatting={true}
                    showTooltip={false}
                    allowExcelExport={true}
                    dataSourceSettings={{
                        enableSorting: false,
                        columns: gridColumns,
                        values: [
                            { name: 'capAMC', caption: 'Cap. Avl. M/C' },
                            { name: 'capAVLMD', caption: 'Cap. Avl. M/D' },
                            { name: 'bookQty', caption: 'Booked Qty' },
                            { name: 'bookCAPMD', caption: 'Booked Cap. M/D' },
                            { name: 'bookAVLMD', caption: 'Avl. M/Days' }
                        ],
                        formatSettings: [{ name: 'capAMC', useGrouping: true }, { name: 'capAVLMD', useGrouping: true }, { name: 'bookQty', useGrouping: true },
                        { name: 'bookCAPMD', useGrouping: true }, { name: 'bookAVLMD', useGrouping: true }],
                        dataSource: modifiedSummaryData,
                        rows: [{ name: 'plant' }, { name: 'units' }, { name: 'line' }],
                        expandAll: isMonthlyView == "month" ? false : true,
                        showGrandTotals: false,
                        filters: [],
                        conditionalFormatSettings: this.conditionalFormattingSummaryView()
                    }}>
                    <Inject services={[ConditionalFormatting]} />
                </PivotViewComponent>
            </div >
        )
        this.setState({ dynamicSummaryViewPlants }, () => {
            global.hideLoader();
        })
    }


    conditionalFormattingSummaryView() {
        let ConditionalFormattingData = [];
        ConditionalFormattingData.push(
            {
                measure: 'bookAVLMD',
                conditions: 'LessThan',
                value1: -1,
                style: {
                    backgroundColor: '#ff656d',
                    text: 'overloaded',
                }
            },
            {
                measure: 'bookAVLMD',
                conditions: 'GreaterThan',
                value1: -1,
                style: {
                    backgroundColor: '#6dc76e',
                    text: 'editable',
                }
            }
        )
        return ConditionalFormattingData;
    }


    createSummaryViewCategory() {
        const { summaryDataCategory, isMonthlyView } = this.state;
        let dynamicSummaryViewCategory = [];
        let modifiedSummaryData = [];
        let gridColumns = isMonthlyView == "month" ? [{ name: 'month' }] : [{ name: 'month' }, { name: 'week' }];
        summaryDataCategory.map(item => {
            modifiedSummaryData = modifiedSummaryData.concat(item);
        })
        dynamicSummaryViewCategory.push(<div key={Math.random()}>
            <PivotViewComponent
                gridSettings={this.gridSettings}
                ref={d => this.summeryCategoryGridObj = d}
                id={"summeryViewCategory"}
                height={700}
                allowConditionalFormatting={true}
                showTooltip={false}
                allowExcelExport={true}
                dataSourceSettings={{
                    enableSorting: false,
                    columns: gridColumns,
                    values: [
                        { name: 'capAMC', caption: 'Cap. Avl. M/C' },
                        { name: 'capAVLMD', caption: 'Cap. Avl. M/D' },
                        { name: 'bookQty', caption: 'Booked Qty' },
                        { name: 'bookCAPMD', caption: 'Booked Cap. M/D' },
                        { name: 'bookAVLMD', caption: 'Avl. M/Days' }
                    ],
                    formatSettings: [{ name: 'capAMC', useGrouping: true }, { name: 'capAVLMD', useGrouping: true }, { name: 'bookQty', useGrouping: true },
                    { name: 'bookCAPMD', useGrouping: true }, { name: 'bookAVLMD', useGrouping: true }],
                    dataSource: modifiedSummaryData,
                    rows: [{ name: 'line' }, { name: 'plant' }, { name: 'units' }],
                    expandAll: isMonthlyView == "month" ? false : true,
                    showGrandTotals: false,
                    filters: [],
                    conditionalFormatSettings: this.conditionalFormattingSummaryView()
                }}>
                <Inject services={[ConditionalFormatting]} />
            </PivotViewComponent>
        </div >
        )
        this.setState({ dynamicSummaryViewCategory }, () => {
            global.hideLoader();
        })
    }

    switchMonthView = (monthType) => {
        let that = this;
        this.setState({ isMonthlyView: monthType }, () => {
            if ((that.state.isSummaryView) && (that.state.multiSelectedPlants.length > 0) && (that.state.summarySelectedYear != null)) {
                global.showLoader();
                this.getSummeryViewData(this.state.multiSelectedPlants, monthType, that.state.summarySelectedYear.getFullYear())
              }
        })
    }

    //----------------------------------------------EXPORT-EXCEL----------------------------------------------------//

    exportLines = () => {
        let that = this;
        const { linePlanningDetails } = that.state;

        let excelExportProperties = {
            multipleExport: { type: 'AppendToSheet', blankRows: 3 },
            fileName: 'lines.xlsx',
            header: {
                headerRows: 2,
                rows: [
                    { cells: [{ colSpan: 5, value: "LINES", style: { fontColor: '#C67878', fontSize: 20, hAlign: 'Center', bold: true, underline: true } }] }
                ]
            },
        };
        let firstGridExport = that[linePlanningDetails[0].lineName].grid.excelExport(excelExportProperties, true);
        firstGridExport.then((data) => {
            if (linePlanningDetails.length > 1)
                that.mergeGrid(linePlanningDetails[1].lineName, data, 1, true, excelExportProperties)
            else
                that[linePlanningDetails[0].lineName].excelExport();  // If only 1 grid exist.
        })
    }


    mergeGrid = (name, data, index, isMultiple, excelExportProperties) => {
        let that = this;
        const { linePlanningDetails } = that.state;
        that[name].grid.excelExport(excelExportProperties, isMultiple, data).then((data2) => {
            index++;
            if (linePlanningDetails.length > index)
                this.mergeGrid(linePlanningDetails[index].lineName, data2, index, linePlanningDetails.length - 1 == index ? false : true, excelExportProperties);
        })
    }


    exportSummeryGrid = () => {
        let that = this;
        if (that.state.isCategoryView && that.state.dynamicSummaryViewCategory.length > 0)
            that.summeryCategoryGridObj.excelExport();
        if (that.state.isSummaryView && that.state.dynamicSummaryViewPlants.length > 0)
            that.summeryPlantGridObj.excelExport();
    }

    exportPlanningStatusGrid = (args) => {
        let that = this;
        // exportProperties={this.state.excelExportProperties}
        if (that.orderGrid && args.item.id === 'orderStatus_excelexport') {
            that.orderGrid.excelExport(that.state.planningStatusExportProperties);
        }
      }
    //-------------------------------------------------------PLANNING-HISTORY-SCREEN------------------------------------------------------//

    async closePlanningHistoryScreen(args, isShowPlanningHistoryScreen) {
        let that = this;
        const { orderDetails } = that.state;
        if ((args.event != null && args.event != undefined) || (args.type != undefined && args.type != null)) {
            if (args.type != undefined && args.type != null) // for status button
                await that.getAllOrders();
            that.setState({
                isShowPlanningHistoryScreen, orderDetails: { ...orderDetails, isInvokedFromPlanningHistory: isShowPlanningHistoryScreen, prodLineId: null, unitId: null, plantId: null, lineName: null, unitName: null }
            }, () => {
                that.resetOrders();
            });
        }
    }

    expandCollapseFilterOption = () => {
        this.setState({
            isShowFilters: !this.state.isShowFilters
        });
    }

    searchOrders = () => {
        let that = this;
        const { selectedPlanningHistoryAlert, selectedPlanningHistoryStartDate, selectedPlanningHistoryEndDate, selectedPlanningHistoryPlants, selectedPlanningHistoryUnits, selectedPlanningHistoryBuyerPo,
            selectedPlanningHistorySaid, selectedPlanningHistoryLineItem, selectedPlanningHistoryStatus } = that.state;
        if (selectedPlanningHistoryAlert.length == 0 && selectedPlanningHistoryPlants.length == 0 && selectedPlanningHistoryUnits.length == 0 && selectedPlanningHistoryBuyerPo.length == 0 && selectedPlanningHistorySaid.length == 0
            && selectedPlanningHistoryLineItem.length == 0 && selectedPlanningHistoryLineItem.length == 0 && selectedPlanningHistoryStatus.length == 0 && selectedPlanningHistoryStartDate == null && selectedPlanningHistoryEndDate == null) {
            that.toastObj.show(this.state.toasterMessage[13]);
        } else {
            that.orderGrid.clearFiltering();
            if (selectedPlanningHistoryPlants.length > 0)
                that.orderGrid.filterByColumn("plantName", "equal", selectedPlanningHistoryPlants);
            if (selectedPlanningHistoryUnits.length > 0)
                that.orderGrid.filterByColumn("unitName", "equal", selectedPlanningHistoryUnits);
            if (selectedPlanningHistoryBuyerPo.length > 0)
                that.orderGrid.filterByColumn("buyerName", "equal", selectedPlanningHistoryBuyerPo);
            if (selectedPlanningHistorySaid.length > 0)
                that.orderGrid.filterByColumn("saidId", "equal", selectedPlanningHistorySaid);
            if (selectedPlanningHistoryLineItem.length > 0)
                that.orderGrid.filterByColumn("lineItemNo", "equal", selectedPlanningHistoryLineItem);
            if (selectedPlanningHistoryStatus.length > 0)
                that.orderGrid.filterByColumn("status", "equal", selectedPlanningHistoryStatus);
            if (selectedPlanningHistoryAlert.length > 0)
                that.orderGrid.filterByColumn("alertCode", "equal", selectedPlanningHistoryAlert);
            if (selectedPlanningHistoryStartDate != null)
                that.orderGrid.filterByColumn("pcdDate", "greaterthanorequal", selectedPlanningHistoryStartDate);
            if (selectedPlanningHistoryEndDate != null)
                that.orderGrid.filterByColumn("pcdDateForFilter", "lessthanorequal", selectedPlanningHistoryEndDate);
            that.toastObj.show(this.state.toasterMessage[14]);
        }
    }


    resetOrders = (isInvokedFromOnClick) => {
        let that = this;
        that.setState({
            selectedPlanningHistoryPlants: [], selectedPlanningHistoryUnits: [], selectedPlanningHistoryBuyerPo: [], selectedPlanningHistoryLineItem: [],
            selectedPlanningHistorySaid: [], selectedPlanningHistoryAlert: [], selectedPlanningHistoryStatus: [], selectedPlanningHistoryStartDate: null,
            selectedPlanningHistoryEndDate: null
        });
        that.orderGrid.clearFiltering();
        if (isInvokedFromOnClick)
            that.toastObj.show(this.state.toasterMessage[15]);
    }

    async getUnitProductionLinesForPlanningHistory(unitId) {
        let response = (await this.linePlanningService.getUnitProductionLines(unitId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ planningHistoryProdLines: response.data.data, isShowLineIDSelectionDialog: true });
        } else {
            this.setState({ planningHistoryProdLines: [] });
        }
    }

    getSelectedRowPlannings = (args) => {
        let that = this;
        that.state.orderDetails.plantId = args.data.plantId;
        that.state.orderDetails.unitId = args.data.unitId;
        that.state.orderDetails.unitName = args.data.unitName;
        that.getUnitProductionLinesForPlanningHistory(args.data.unitId);
        that.getSaidNo(that.state.orderDetails.plantId, that.state.orderDetails.unitName);
    }

    cancelLineSelectionDialog = () => {
        let that = this;
        that.setState({ isShowLineIDSelectionDialog: false, selectedPlanningHistoryProdLine: null, planningHistoryProdLines: [] })
    }

    onChangePlanningHistoryProdLineDropdown = (event) => {
        let that = this;
        if (event.e != null && event.e != undefined) {
            that.state.orderDetails.prodLineId = Number(event.itemData.id);
            that.state.orderDetails.lineName = event.itemData.lineName;
            that.state.selectedPlanningHistoryProdLine = event.value;
            global.showLoader();
            that.onCellClick(null, Number(event.itemData.id), event.itemData.lineName, true);
        }
    }

    onChangeFilterDatePicker = (args) => {
        if (args.event != undefined && args.event != null) {
            this.setState({ [args.element.id]: args.value })
        }
    }

    onChangePlanningHistoryMultiSelect = (event) => {
        // if (event.e != undefined && event.e != null)
        this.setState({ [event.element.id]: event.value })
    }

    customizeCell = (args) => {
        if (args.column.field === "alertCode" && args.data && args.cell) {
            if (args.data.alertCode == "lt 7 days") {
                args.cell.classList.add('below-80');
            } else if (args.data.alertCode == "lt 14 days") {
                args.cell.classList.add('below-30');
            }
        }
    }
    onChangeReportDialoguePlant = (event) => {
        this.setState({
            selectedReportPlantId: event.value,
        }, () => {
            if (event.e != undefined) {
                this.multiSelectObj.selectAll(false);
                this.setState({ units: [] }, () => {
                    this.getUnitList(event.value)
                })
            }
        })
    }
    onChangeReportDialogueUnit = (event) => {
      let that = this;
      that.setState({ selectedReportUnitId: event.value });
    }
    onChangeReportDialogueStartDate = (event) => {
      let that = this;
      that.setState({ selectedReportStartDate: event.value });
    }
    onChangeReportDialogueEndDate = (event) => {
      let that = this;
      that.setState({ selectedReportEndDate: event.value });
    }

    refreshPlanningReportGrid(){
        this.setState({showDialog:false,selectedReportPlantId:null,selectedReportUnitId:null,selectedReportStartDate:null,selectedReportEndDate:null}) 
     }


     generatePlanningStatusReport = async (args) => {
        const {selectedReportPlantId, selectedReportUnitId, selectedReportStartDate, selectedReportEndDate} = this.state;
 
        if(selectedReportPlantId != null && selectedReportUnitId != null && selectedReportStartDate !=null && selectedReportEndDate !=null){
             this.genPlanningReport(selectedReportPlantId, selectedReportUnitId, selectedReportStartDate, selectedReportEndDate)
             this.refreshPlanningReportGrid()   
         }
     }

     async genPlanningReport(plantId, unitId, startDate, endDate) {
        global.showLoader();
        let startMonth = startDate.getMonth()+1;
        let startDay = startDate.getDate();
        if(startMonth < 10){
            startMonth = '0' + startMonth;
        }
        if(startDay < 10){
            startDay = '0' + startDay;
        }
        let selectStartDate = startDay+ '-' + startMonth + '-' + startDate.getFullYear() ;

        let endMonth = endDate.getMonth()+1;
        let endDay = endDate.getDate();
        if(endMonth < 10){
            endMonth = '0' + endMonth;
        }
        if(endDay < 10){
            endDay = '0' + endDay;
        }
        let selectEndDate = endDay+ '-' + endMonth + '-' +  endDate.getFullYear() ;
      
        let response1 = (await this.linePlanningService.generatePlanningStatusReport(plantId, unitId, selectStartDate, selectEndDate))
        if ((response1 != undefined) && (response1.data.status == "success")) {
            this.downloadPlanningStatusReport(response1.data.message)
        //     let response = (await this.lineLoadingService.downloadReport(response1.data.value))
        //     if ((response != undefined) && (response.data.status == "success")) {
        //         this.toastObj.show({ title: 'Success!', content: response1.data.message, cssClass: 'e-toast-success', icon: 'e-error toast-icons' });
        //         this.setState({selectedReportStartDate: null, selectedReportPlantId: null, selectedReportUnitId: null, selectedReportEndDate: null})
        //     }
        //   global.hideLoader();
        }
        else {
          this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
          global.hideLoader();
        }
      }

      async downloadPlanningStatusReport(reportId) {
        global.showLoader();
        let response = (await this.linePlanningService.downloadReport(reportId))
        if ((response != undefined) && (response.status == 200)) {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'Planning Status.xlsx'); //or any other extension
            document.body.appendChild(link);
            link.click();
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[16]);

        }
        else {
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[2]);
        }
    }

    //-----------------------------------------------------------RENDER-----------------------------------------------------------------//

    render() {
        const { t, i18n } = this.props;
        return (
            <div className="container-fluid-inner">
                <div hidden={this.state.isSummaryView}>
                    <h1 className='pagesheading'>Planning</h1>
                    <div className='clearfix'></div>
                    <Row>
                    <Col xs='6' md='6'>
                        <div>
                            <Button color="primary" size="sm" className="mb-2" onClick={this.switchSummaryView}>Switch Summary</Button>
                            <Button color="primary" size="sm" className="mb-2 ml-2" onClick={(e) => { this.closePlanningHistoryScreen(e, true) }}>Order Status</Button>
                        </div>
                    </Col>
                    <Col xs='6' md='6'>
                        <div>
                            <a color="primary" className="ml-2 float-right primaryText" onClick={this.onClickPlanning} id="Report"><i className='fa fa-download'></i> Planning Status</a>
                        </div>
                    </Col>
                    </Row>

                    <Row>
                        <Col xs='12' md='4' lg='3'>
                            <div className="required-field">
                                <DropDownListComponent color="primary" id="plantId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedPlantName} change={this.onChangePlant} dataSource={this.state.plants} placeholder={t("cpm.lm.line.plant.name")} floatLabelType="Auto" />
                            </div>
                        </Col>
                        <Col xs='12' md='4' lg='2'>
                            <div className="required-field">
                                <DropDownListComponent color="primary" id="unitId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedUnitName} change={this.onChangeUnits} dataSource={this.state.units} placeholder={t("cpm.lm.line.unit.name")} floatLabelType="Auto" />
                            </div>
                        </Col>
                        
                        <Col xs='12' md='4' lg='2'>
                            <DropDownListComponent color="primary" id="lineName" allowFiltering={true} fields={{ text: 'lineName', value: 'id' }} value={this.state.selectedLineName} change={this.onChangeProductionLine} dataSource={this.state.unitProductionLines} placeholder={t("cpm.lm.line.prod.line")} floatLabelType="Auto" />
                        </Col>
                        <Col xs='12' md='4' lg='1'>
                            <div className="required-field">
                                {/* <DropDownListComponent color="primary" id="targetYear" allowFiltering={true} fields={{ text: 'year', value: 'year' }} value={this.state.selectedYear} change={this.onChangeYear} dataSource={this.state.years} placeholder={"Select Year"} floatLabelType="Auto" /> */}
                                <DatePickerComponent size='sm' min={this.minDate} max={this.maxDate} value={this.state.selectedYear} start="Decade" depth="Decade" format="yyyy" floatLabelType="Auto" placeholder="Year" change={this.onChangeYear} />
                            </div>
                        </Col>
                        <Col xs='12' md='12' lg='4'>
                            <ul className="legend-group mt-3">
                                <li><span className="editable"></span>{t("cpm.lm.line.plan.map.legend.Editable")}</li>
                                <li><span className="overloaded"></span>{t("cpm.lm.line.plan.map.legend.Overloaded")}</li>
                                <li><span className="freeze"></span>{t("cpm.lm.line.plan.map.legend.Freeze")}</li>
                            </ul>
                        </Col>
                        {/* <div>
                            {this.state.dynamicPivotGrids.length ? <div><Button color="primary" size="sm" className="mb-2" onClick={this.exportLines}>Excel Export</Button></div> : null}
                        </div> */}
                    </Row>


                    {/* //---------------------------------------------------------------------------------Planning Report Dialog Start---------------------------------------------------------------// */}
                    <DialogComponent visible={this.state.showDialog} id="defaultDialog" showCloseIcon={true} animationSettings={this.animationSettings} isModal={true} allowDragging={true} width={'70%'} ref={dialog => this.dialogInstance = dialog}
                    header= 'Planning Status' close={this.dialogClose.bind(this)}>
                        
                        <Row>
                            <Col><DropDownListComponent className='datePicker' id='selectPlantName' placeholder="Select Plant Name" value={this.state.selectedReportPlantId} floatLabelType="Auto" fields={{ text: 'name', value: 'id' }} change={this.onChangeReportDialoguePlant} dataSource={this.state.plants} popupHeight="220px"></DropDownListComponent></Col>
                            <Col><DropDownListComponent className='datePicker' id='selectUnitName' placeholder="Select Unit Name" value={this.state.selectedReportUnitId} floatLabelType="Auto" dataSource={this.state.units} fields={{ text: 'name', value: 'id' }}  change={this.onChangeReportDialogueUnit}  popupHeight="220px"></DropDownListComponent></Col>
                            <Col><DatePickerComponent className='datePicker' value={this.state.selectedReportStartDate}  placeholder=" Start Date" floatLabelType="Auto"  change={this.onChangeReportDialogueStartDate} ></DatePickerComponent></Col>
                            <Col><DatePickerComponent className='datePicker' value={this.state.selectedReportEndDate}  placeholder=" End Date" floatLabelType="Auto"  change={this.onChangeReportDialogueEndDate} ></DatePickerComponent></Col>
                        </Row>
                        <div className='mt-2'></div>
                        <Row className='float-right'><Button color="primary" size="sm" id="generatePlanningStatusReport" onClick={this.generatePlanningStatusReport}><i className='fa fa-download'></i> Generate Report</Button></Row>

                    </DialogComponent>
                    {/* //---------------------------------------------------------------------------------Planning Report Dialog End---------------------------------------------------------------// */}


                    <div className='control-section'>
                        {this.state.dynamicPivotGrids}
                    </div>
                </div>

                <div hidden={!this.state.isSummaryView}>
                    <h1 className='pagesheading'>Planning Summary</h1>
                    <div className='clearfix'></div>
                    <Button color="primary" size="sm" onClick={this.switchSummaryView}>Switch Planning</Button>
                    <Button color="primary" size="sm" className="ml-2 mr-2" id="plantView" color={this.state.isCategoryView == false ? "primary" : "secondary"} onClick={() => { this.switchView(false) }}>Plant View</Button>
                    <Button color="primary" size="sm" id="categoryView" color={this.state.isCategoryView == true ? "primary" : "secondary"} onClick={() => { this.switchView(true) }}>Category View</Button>
                    <div className="mt-3"></div>
                    <Row>
                        <Col xs='12' md='3'>
                            <div className="required-field">
                                <MultiSelectComponent id="plantIdCheckbox" ref={(scope) => { this.multiSelectObj = scope; }} dataSource={this.state.plants} value={this.state.selectedPlantList}
                                    change={this.onChangeMultiSelectPlant} fields={{ text: 'name', value: 'id' }} placeholder={t("cpm.lm.line.plant.name")} mode="CheckBox" showSelectAll={true}
                                    showDropDownIcon={true} filterBarPlaceholder="Search plants" popupHeight="350px">
                                    <Inject services={[CheckBoxSelection]} />
                                </MultiSelectComponent>
                            </div>
                        </Col>
                        <Col xs='12' md='4' lg="1">
                            <div className="required-field">
                                <DatePickerComponent size='sm' min={this.minDate} max={this.maxDate} value={this.state.summarySelectedYear} start="Decade" depth="Decade" format="yyyy" placeholder="Year" change={this.onChangeSummaryYear} />                            </div>
                        </Col>
                        <Col xs='12' md='3'>
                            <Button color="primary" size="sm" className="ml-2" color={this.state.isMonthlyView == "month" ? "primary" : "secondary"} onClick={(e) => this.switchMonthView("month")}>Monthly View</Button>
                            <Button color="primary" size="sm" className="ml-2" color={this.state.isMonthlyView == "week" ? "primary" : "secondary"} onClick={(e) => this.switchMonthView("week")}>Weekly View</Button>
                        </Col>

                        <Col xs='12' lg='5' md='12'>
                            <ul className="legend-group">
                                <li><span className="editable"></span>{t("cpm.lm.line.plan.map.legend.Editable")}</li>
                                <li><span className="overloaded"></span>{t("cpm.lm.line.plan.map.legend.Overloaded")}</li>
                                <li><span className="freeze"></span>{t("cpm.lm.line.plan.map.legend.Freeze")}</li>
                            </ul>
                        </Col>
                    </Row>

                    <div>
                        {/* <Button color="primary" size="sm" className="mb-2" onClick={this.exportSummeryGrid}>Export Excel</Button> */}
                    </div>
                    {(this.state.isSummaryView && this.state.dynamicSummaryViewPlants.length > 0) ? this.state.dynamicSummaryViewPlants : null}
                    {(this.state.isCategoryView && this.state.dynamicSummaryViewCategory.length > 0) ? this.state.dynamicSummaryViewCategory : null}
                </div>
                <div id="container"></div>

                {/* ----------------------- Add line Section  -----------------------  */}
                <div>
                    <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={{ X: 'Left', Y: 'Bottom' }}   ></ToastComponent>
                    <div className="add-line-section" hidden={!this.state.isSidebar}>
                        <SplitterLayout secondaryInitialSize={750} secondaryMinSize={250}>
                            <div className="blank-space" onClick={this.sideBar}></div>
                            <div className="overflow-hidden add-line-section-form">
                                <div className="heading mb-2">{this.state.isEdit ? t("cpm.lm.line.edit.planning") : t("cpm.lm.line.add.planning")}<a className="close" onClick={this.sideBar}>+</a></div>
                                <Col xs='12'>
                                    <Row>
                                        <Col md="12">
                                            <div className="line-name-message">{t("cpm.lm.line.plan.header.lineName")} : <b>{this.state.model.lineName}</b></div>
                                        </Col>
                                    </Row>
                                    <div className="mt-1"></div>
                                    <Row>
                                        <Col xs='12' md='4'>
                                            <div className="required-field">
                                                <DropDownListComponent color="primary" enabled={this.state.isEdit ? false : true} id="saidId" value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.saidId : this.state.model.saidId} allowFiltering={true} dataSource={this.state.saidNoDropdownList} change={this.onChangeSidebarSaidDropdown} placeholder={t("cpm.lm.line.said.no")} floatLabelType="Auto" />
                                            </div>
                                        </Col>
                                        <Col xs='12' md='4'>
                                            <div className="required-field">
                                                <DropDownListComponent color="primary" enabled={this.state.isEdit ? false : true} id="lineItemNo" allowFiltering={true} fields={{ text: 'lineItemNo', value: 'lineItemNo' }} value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.lineItemNo : this.state.model.lineItemNo} dataSource={this.state.lineItemNo} change={this.onChangeLineItemNoSidebarDropdown} placeholder={t("cpm.lm.line.line.itemno")} floatLabelType="Auto" />
                                            </div>
                                        </Col>

                                        <Col xs='12' md='4'>
                                            <div className="required-field">
                                                <DropDownListComponent color="primary" id="apm" allowFiltering={true} fields={{ text: 'apmName' }} value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.apm : this.state.model.apm} enabled={false} dataSource={this.state.apm} placeholder={t("cpm.lm.line.assign.to")} floatLabelType="Auto" />
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs='9' md='3'>
                                            <div className="required-field">
                                                <NumericTextBoxComponent id="noOfMachineToUse" decimals={0} placeholder={t("cpm.lm.line.plan.field.machineAllocated")} onChange={this.onChangeNoOfMachines} min={0} max={this.state.maxAllowedMachines} format='###.###' value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.noOfMachineToUse : this.state.model.noOfMachineToUse} floatLabelType="Auto" />
                                            </div>
                                        </Col>
                                        <Col xs='3' md='1'>
                                            <div className="sew-machine mt-3 mr-1"></div>
                                        </Col>

                                        <Col xs='12' md='4'>
                                            <div className="required-field">
                                                <DropDownListComponent color="primary" enabled={false} id="productCategoryId" allowFiltering={true} value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.productCategoryId : this.state.model.productCategoryId} fields={{ text: 'name', value: 'id' }} dataSource={this.state.productionCategory} change={this.onChangeSidebarDropdown} placeholder={t("cpm.lm.line.prod.category")} floatLabelType="Auto" />
                                            </div>
                                        </Col>
                                        <Col xs='12' md='2'>
                                            <TextBoxComponent placeholder={t("cpm.lm.line.total.qty")} id="totalQtyToProduced" value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.totalQtyToProduced : this.state.model.totalQtyToProduced} disabled={true} floatLabelType="Auto" />
                                        </Col>
                                        <Col xs='12' md='2'>
                                            <div className="required-field">
                                                <NumericTextBoxComponent id="totalQtyAllocatedInLine" decimals={0} max={this.state.maxLineQtyAllocated} onChange={this.onChangeSideBarNumericText} placeholder={t("cpm.lm.line.line.qty")} min={0} format='###.###' value={this.state.isEdit ? (this.state.schedulerDataOnLoad[this.state.index].plannedData.totalQtyAllocatedInLine) : this.state.model.totalQtyAllocatedInLine} floatLabelType="Auto" />
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row >
                                        <Col xs='12' md='4'>
                                            <TextBoxComponent disabled={true} value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.merchandiserName : this.state.model.merchandiserName} id="merchandiserName" placeholder={t("cpm.lm.line.merchant.name")} floatLabelType="Auto" />
                                        </Col>


                                        <Col xs='12' md='4'>
                                            <TextBoxComponent placeholder={t("cpm.lm.line.buyer.name")} value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.buyerName : this.state.model.buyerName} disabled={true} id="buyerName" floatLabelType="Auto" />
                                        </Col>

                                        <Col xs='12' md='4'>
                                            <NumericTextBoxComponent id="remainingQtyToPlan" disabled={true} placeholder={t("cpm.lm.line.plan.field.plannedQty")} min={0} format='###.###' value={this.state.isEdit ? (this.state.schedulerDataOnLoad[this.state.index].plannedData.remainingQtyToPlan) : this.state.model.remainingQtyToPlan} floatLabelType="Auto" />
                                        </Col>
                                    </Row>

                                    <Row>
                                        <Col xs='12' md='4'>
                                            <div className="required-field">
                                                <NumericTextBoxComponent id="sam" placeholder={t("cpm.lm.line.plan.field.sam")} disabled={true} min={0} value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.sam : this.state.model.sam} floatLabelType="Auto" />
                                            </div>
                                        </Col>
                                        <Col xs='12' md='8'>
                                            <TextBoxComponent placeholder={t("cpm.lm.line.plan.field.lineDesc")} id="lineDesc" value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.lineItemDesc : this.state.model.lineItemDesc} disabled={true} floatLabelType="Auto" />
                                        </Col>
                                        <Col xs='12' md='4'>
                                            <span>Estimated working days with </span><b>{this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.noOfMachineToUse : this.state.model.noOfMachineToUse}</b><span> Machines is : </span><b>{this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.estimatedWorkingDays : this.state.model.estimatedWorkingDays}</b><span> Days.</span>
                                        </Col>
                                        <Col xs='12' md='4'>
                                            <div className="required-field">
                                                <DatePickerComponent weekNumber={true} floatLabelType="Auto" renderDayCell={this.disableDatePickerDate} allowEdit={false} min={this.state.minNavigationDate} change={(e) => { this.onChangeSideBarDatePicker(e, this) }} value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.startDateTime : this.state.model.startDateTime} format='dd/MM/yy hh:mm a' id="startDateTime" placeholder={t("cpm.lm.line.plan.field.startDate")}></DatePickerComponent >
                                            </div>
                                        </Col>
                                        <Col xs='12' md='4'>
                                            <div className="required-field">
                                                <DatePickerComponent weekNumber={true} renderDayCell={this.disableDatePickerDate} floatLabelType="Auto" allowEdit={false} min={this.state.minStartDate} change={(e) => { this.onChangeSideBarDatePicker(e, this) }} value={this.state.isEdit ? this.state.schedulerDataOnLoad[this.state.index].plannedData.endDateTime : this.state.model.endDateTime} format='dd/MM/yy hh:mm a' id="endDateTime" placeholder={t("cpm.lm.line.plan.field.endDate")}></DatePickerComponent >
                                            </div>
                                        </Col>
                                    </Row>
                                    <div className="mt-3"></div>
                                    <Row>
                                        <Col xs='12' md='12'>
                                            <Button color="primary" size="sm" id="apply" className="float-left mr-3" disabled={this.state.isDisabledSaveButton} onClick={this.validateMachinesBeforeApply}>{t("cpm.lm.line.plan.field.onApply")}</Button>
                                            <Button color="primary" size="sm" id="apply" className="float-left mr-3" hidden={!this.state.isEdit} onClick={this.onDelete}>{"Delete"}</Button>
                                            <Button color="primary" size="sm" id="apply" className="float-left mr-3" hidden={!this.state.isEdit} onClick={this.onCreateNewEvent}>{"Create New Event"}</Button>
                                        </Col>
                                    </Row>

                                    <div>
                                        <div className="mt-2"></div>
                                        <div className="line-name-message"><span className="selectedEvent-mark" style={{ backgroundColor: this.state.selectedEventColor }}></span>  {t("cpm.lm.line.plan.schedulerHeader.eventName")} : <b>{this.state.selectedEventName}</b></div>
                                        <Row>
                                            <Col xs="12">
                                                <div className='control-wrapper'>
                                                    <ScheduleComponent
                                                        id={1}
                                                        ref={t => this.scheduleObj = t}
                                                        workDays={this.state.workingDays}
                                                        showWeekend={false}
                                                        allowDragAndDrop={true}
                                                        allowResizing={true}
                                                        showTimeIndicator={true}
                                                        readonly={true}
                                                        showWeekNumber={true}
                                                        // timezone='UTC' 
                                                        selectedDate={this.state.currrentDate}
                                                        currentView={this.state.currentView}
                                                        popupOpen={this.onClickEventPopup}
                                                        // cellDoubleClick={this.onCellDoubleClick}
                                                        eventRendered={this.setColorToEventOnUI}
                                                        navigating={this.onNavigateSchedulerDate}
                                                        renderCell={this.onRenderCell}
                                                        eventSettings={{
                                                            dataSource: this.state.schedulerDataOnLoad,
                                                            fields: {
                                                                id: 'id',
                                                                subject: { name: 'description' },
                                                                location: { name: 'lineItem' },
                                                                description: { name: 'description', title: 'Description', validation: { required: true } },
                                                                startTime: { name: 'startDateTime', title: 'Start Duration', validation: { required: true } },
                                                                endTime: { name: 'endDateTime', title: 'End Duration', validation: { required: true } },
                                                            }
                                                        }}
                                                    >
                                                        <ViewsDirective>
                                                            <ViewDirective option='Month' />
                                                        </ViewsDirective>
                                                        <Inject services={[Month]} />
                                                    </ScheduleComponent>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>

                                    {/*----------------------------- End Schudule -----------------------------*/}
                                    <hr />
                                    <Row className="mt-3">
                                        <Col>
                                            <Button color="primary" size="sm" id="cancel" className="float-left" disabled={this.state.isDisabledSaveButton} onClick={this.sideBar}>{t("cpm.lm.line.close")}</Button>
                                        </Col>
                                    </Row>
                                </Col>

                            </div>
                        </SplitterLayout>
                    </div>
                </div>
                <div className="add-line-section-overlay" hidden={!this.state.isSidebar}></div>
                {/* ----------------------- Add line Section end -----------------------  */}
                <div className="mt-3"></div>
                <DialogComponent id="confirmDialog" animationSettings={this.state.animationSettings} showCloseIcon={true} close={this.cancelConfirmDialog} buttons={this.state.confirmButton} header='Confirmation Alert !!' visible={this.state.isShowConfirmationDialog} width='400px' content={this.state.isValidateMachines ? 'You have allocated ' + this.state.extraMachines + ' extra machines !! Are you sure ? You want to continue !!' : 'Are you sure you ? All your unsaved data will be lost !!'}></DialogComponent>
                <div className="overlayer-popup" hidden={!this.state.isShowConfirmationDialog} ></div>
                {/*---------------- Filter Dialog ---------------- */}
                <DialogComponent id="planningHistoryScreen" isModal={true} animationSettings={this.state.animationSettings} showCloseIcon={true} beforeClose={(e) => { this.closePlanningHistoryScreen(e, false) }} header='Order Status' visible={this.state.isShowPlanningHistoryScreen} width='95%' >
                    <a className={this.state.isShowFilters ? 'filter-icon-open' : 'filter-icon-close'} onClick={this.expandCollapseFilterOption}>Filter <i className='fa  fa-filter'></i></a>
                    <div className='filter-dropdown' hidden={!this.state.isShowFilters}>
                        <div className='filter-Selection'>
                            <h3>Location</h3>
                            <Row>
                                <Col sm='3'>
                                    <MultiSelectComponent id="selectedPlanningHistoryPlants" ref={(scope) => { this.planningHistoryPlantObj = scope; }} dataSource={this.state.planningHistoryPlants}
                                        value={this.state.selectedPlanningHistoryPlants} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'plantName', value: 'plantName' }}
                                        placeholder={"Select Plants"} mode="CheckBox" showSelectAll={true}
                                        showDropDownIcon={true} filterBarPlaceholder="Search plants" popupHeight="300px">
                                        <Inject services={[CheckBoxSelection]} />
                                    </MultiSelectComponent></Col>
                                <Col sm='3'>   <MultiSelectComponent id="selectedPlanningHistoryUnits" ref={(scope) => { this.planningHistoryUnitObj = scope; }} dataSource={this.state.planningHistoryUnits}
                                    value={this.state.selectedPlanningHistoryUnits} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'unitName', value: 'unitName' }} placeholder={"Select Units"}
                                    mode="CheckBox" showSelectAll={true}
                                    showDropDownIcon={true} filterBarPlaceholder="Search Units" popupHeight="300px">
                                    <Inject services={[CheckBoxSelection]} />
                                </MultiSelectComponent></Col>
                            </Row>
                        </div>

                        <div className='filter-Selection'>
                            <h3>Order</h3>
                            <Row>
                                <Col sm='3'>
                                    <MultiSelectComponent id="selectedPlanningHistoryBuyerPo" ref={(scope) => { this.planningHistoryBuyerPoObj = scope; }} dataSource={this.state.planningHistoryBuyerPo}
                                        value={this.state.selectedPlanningHistoryBuyerPo} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'buyerName', value: 'buyerName' }}
                                        placeholder={"Select Buyer Po"} mode="CheckBox" showSelectAll={true} showDropDownIcon={true} filterBarPlaceholder="Search Buyer Po" popupHeight="300px">
                                        <Inject services={[CheckBoxSelection]} />
                                    </MultiSelectComponent></Col>
                                <Col sm='3'>   <MultiSelectComponent id="selectedPlanningHistorySaid" ref={(scope) => { this.planningHistorySaidObj = scope; }} dataSource={this.state.planningHistorySaid}
                                    value={this.state.selectedPlanningHistorySaid} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'saidId', value: 'saidId' }} placeholder={"Select Said"} mode="CheckBox" showSelectAll={true}
                                    showDropDownIcon={true} filterBarPlaceholder="Search Said" popupHeight="300px">
                                    <Inject services={[CheckBoxSelection]} />
                                </MultiSelectComponent></Col>
                                <Col sm='3'>   <MultiSelectComponent id="selectedPlanningHistoryLineItem" ref={(scope) => { this.planningHistoryLineItemObj = scope; }} dataSource={this.state.planningHistoryLineItem}
                                    value={this.state.selectedPlanningHistoryLineItem} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'lineItemNo', value: 'lineItemNo' }} placeholder={"Select LineItems"} mode="CheckBox" showSelectAll={true}
                                    showDropDownIcon={true} filterBarPlaceholder="Search LineItems" popupHeight="300px">
                                    <Inject services={[CheckBoxSelection]} />
                                </MultiSelectComponent></Col>
                            </Row>
                        </div>
                        <div className='filter-Selection'>
                            <h3>Status</h3>
                            <Row>
                                <Col sm='3'>
                                    <DatePickerComponent id="selectedPlanningHistoryStartDate" popupHeight="300px" placeholder='Start Date' format='dd/MM/yy hh:mm a' allowEdit={false} change={this.onChangeFilterDatePicker} value={this.state.selectedPlanningHistoryStartDate} /></Col>
                                <Col sm='3'>
                                    <DatePickerComponent id="selectedPlanningHistoryEndDate" popupHeight="300px" placeholder='End Date' format='dd/MM/yy hh:mm a' allowEdit={false} change={this.onChangeFilterDatePicker} value={this.state.selectedPlanningHistoryEndDate} /></Col>
                                <Col sm='3'>   <MultiSelectComponent id="selectedPlanningHistoryStatus" ref={(scope) => { this.planningHistoryStatus = scope; }} dataSource={this.state.planningHistoryStatus}
                                    value={this.state.selectedPlanningHistoryStatus} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'status', value: 'status' }} placeholder={"Status"} mode="CheckBox" showSelectAll={true}
                                    showDropDownIcon={true} filterBarPlaceholder="Search Pending" popupHeight="300px">
                                    <Inject services={[CheckBoxSelection]} />
                                </MultiSelectComponent></Col>
                                <Col sm='3'>
                                    <MultiSelectComponent id="selectedPlanningHistoryAlert" ref={(scope) => { this.planningHistoryAlert = scope; }} dataSource={this.state.planningHistoryAlert}
                                        value={this.state.selectedPlanningHistoryAlert} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'alertCode', value: 'alertCode' }} placeholder={"Alert"} mode="CheckBox" showSelectAll={true}
                                        showDropDownIcon={true} filterBarPlaceholder="Search Pending" popupHeight="300px">
                                        <Inject services={[CheckBoxSelection]} />
                                    </MultiSelectComponent></Col>
                            </Row>
                            <div className='mt-2'></div>
                            <Row>
                                <Col sm='6'>
                                    <Button color="primary" size="sm" id="searchOrders" onClick={this.searchOrders} >{"Search"}</Button>

                                    <Button color="primary" size="sm" id="resetOrders" onClick={() => { this.resetOrders(true) }} className="ml-2">{"Reset Filter"}</Button>
                                </Col>
                            </Row>
                        </div>
                    </div>
                    <div className='mt-1'>
                        <GridComponent gridLines='Both' queryCellInfo={this.customizeCell} rowSelected={this.getSelectedRowPlannings} ref={grid => this.orderGrid = grid} allowGrouping={true}
                            allowResizing={true} allowTextWrap={true} rowHeight={30} filterSettings={this.state.FilterSettingsModel} enableHover={true} dataSource={this.state.orders} allowFiltering={true}
                            enableHover={true} allowSorting={true} allowPaging={true} allowExcelExport={true} id="orderStatus"  toolbarClick={this.exportPlanningStatusGrid} toolbar={['ExcelExport', 'Search']}>
                            <ColumnsDirective>
                                <ColumnDirective field='saidId' filter={this.state.IFilter} headerText='Said No' width='85' />
                                <ColumnDirective field='plantName' filter={this.state.IFilter} headerText='Plant Name' width='100' />
                                <ColumnDirective field='unitName' filter={this.state.IFilter} headerText='Unit Name' width='80' />
                                <ColumnDirective field='lineItemNo' filter={this.state.IFilter} headerText='line Item No' width='90' />
                                <ColumnDirective field='pcdDate' filter={this.state.IFilter} format={{ type: 'date', format: "dd/MM/yy" }} headerText='PCD Date' width='70' />
                                <ColumnDirective field='orderedQty' filter={this.state.IFilter} headerText='Total Qty' width='90' />
                                <ColumnDirective field='plannedQty' filter={this.state.IFilter} headerText='Planned Qty' width='90' />
                                <ColumnDirective field='leftPlannedQty' filter={this.state.IFilter} headerText='Balanced Planned Qty' width='120' />
                                <ColumnDirective field='poQty' filter={this.state.IFilter} headerText='Total Buyer PO Qty' width='100' />
                                <ColumnDirective field='leftPoQtyTobePlanned' filter={this.state.IFilter} headerText='Balanced PO Qty' width='90' />
                                <ColumnDirective field='buyerName' filter={this.state.IFilter} headerText='Buyer Name' width='80' />
                                <ColumnDirective field='status' filter={this.state.IFilter} headerText='Status' width='80' />
                                <ColumnDirective field='alertCode' filter={this.state.IFilter} headerText='7 or 14 days left' width='100' />
                                <ColumnDirective field='pcdDateForFilter' visible={false} format={{ type: 'date', format: "dd/MM/yy" }} headerText='Filter Date' width='70' />
                            </ColumnsDirective>
                            <Inject services={[Filter, Sort, Resize, Page, Group, Toolbar, ExcelExport]} />
                        </GridComponent>
                    </div>

                </DialogComponent>
                {/*---------------- Filter Dialog ---------------- */}

                {/*---------------- Line Selction Dialog ---------------- */}
                <DialogComponent id="lineSelectionDialog" isModal={true} animationSettings={this.state.animationSettings} showCloseIcon={true} beforeClose={this.cancelLineSelectionDialog} header='Order Status' visible={this.state.isShowLineIDSelectionDialog} width='30%' >
                    <div >
                        <DropDownListComponent color="primary" id="lineSelectionDropdown" fields={{ text: 'lineName', value: 'id' }} ref={(scope) => { this.lineSelectionDropdownObj = scope; }} value={this.state.selectedPlanningHistoryProdLine} allowFiltering={true}
                            dataSource={this.state.planningHistoryProdLines} change={this.onChangePlanningHistoryProdLineDropdown} placeholder={"Production Line"} floatLabelType="Auto" />
                    </div>
                </DialogComponent>
                {/*---------------- Line Selction Dialog ---------------- */}
            </div>
        );
    }
}
export default withTranslation("translations")(LinePlanning);

