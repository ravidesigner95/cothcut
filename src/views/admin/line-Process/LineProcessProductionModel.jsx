class LineProductionProcessModel 
{
    constructor(id,name,description,isChecked)
    {
        this.id=id;
        this.name=name;
        this.description =description
        this.isChecked=isChecked;
    }
    id=null;
    name="";
    description =""
    isChecked=false;
}
export default LineProductionProcessModel;