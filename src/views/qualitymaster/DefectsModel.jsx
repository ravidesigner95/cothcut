export class DefectsModel {
    constructor(id, name,isActive,isTab, qualityMasterDefectsData) {
        this.id = id;
        this.name = name;
        this.isActive=isActive;
        this.isTab=isTab
        this.qualityMasterDefectsData= qualityMasterDefectsData;
        
    }
    id = null;
    name = null;
    isActive=null;
    isTab=null;
    qualityMasterDefectsData =[];

}
export default DefectsModel; 

export class QualityMasterDefectsDataModel {
    constructor(id,deid,code, level, description) {
        this.id=id;
        this.deid=deid;
        this.code = code;
        this.level = level;
        this.description = description;
    
    }
    id=null;
    deid=null;
    code = null;
    level = null;
    description = null;


}
