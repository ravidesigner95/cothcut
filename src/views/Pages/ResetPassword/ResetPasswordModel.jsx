export class ResetPasswordModel{
  constructor(emailOrUsernameOrDate, updatedPassword,confirmPassword) {
      this.updatedPassword=updatedPassword;
      this.emailOrUsernameOrDate = emailOrUsernameOrDate
      this.confirmPassword=confirmPassword;
      
   }
   updatedPassword = '';
   emailOrUsernameOrDate =''
   confirmPassword = '';
 // email='';
}
export default ResetPasswordModel;
