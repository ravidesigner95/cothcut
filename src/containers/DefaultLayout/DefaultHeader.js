import React, { Component } from 'react';
import {  UncontrolledDropdown, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from 'reactstrap';
import PropTypes from 'prop-types';
import { withTranslation , Trans } from "react-i18next";
import { Link, NavLink } from 'react-router-dom';  
import engicon from '../../assets/img/eng-icon.png';
import hindiicon from '../../assets/img/hindi-icon.png';

import {AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.png';
import sygnet from '../../assets/img/brand/sygnet.png'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props)

    this.state = {
      name: localStorage.getItem("USER_NAME")
    }
  }


  componentDidMount() {
    
    // let acessTokken = localStorage.getItem('ACCESS_TOKEN');
    // if(acessTokken){
    //   this.inactivityTime()
    // }else{
    //   this.props.propsData.history.push('/login')
    // }
    this.expiryCode()
  }

  expiryCode() {
    let acessTokken = localStorage.getItem('ACCESS_TOKEN')
    if(acessTokken == null){
      this.props.onLogout('fne',this.clrinterval);
    }else{
      this.inactivityTime()
    }
  }

  inactivityTime() {
    this.time = 0;
    this.currentprop = this.props
    this.newfun = this.resetTimer;
    this.clrinterval = setInterval(() => this.newfun(this.currentprop), 60000);
    document.onmousemove = () => this.time = 0;
    document.onkeydown = () => this.time = 0;

  };

  resetTimer(getProps) {
    this.time = this.time + 1;
    console.log(this.time)
    if (this.time > 119) {
      getProps.onLogout('timefun',this.clrinterval)
    }

  }

  render() {
    const { t, i18n, userName } = this.props;

    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 150, height: 42, alt: 'Radiant Exports CPM' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'Radiant Exports CPM' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <div className="mt-1 float-left">Radiant 4.0</div>
            <a className="btn-icon white-bg" onClick={() => changeLanguage("de")}><img src={engicon} /></a>
            <a className="btn-icon white-bg" onClick={() => changeLanguage("en")} ><img src={hindiicon} /></a>

          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>

          <span className="d-none d-sm-block"> <b className="pr-2">{this.state.name}</b>{new Date().toDateString()}</span>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <i className="fa fa-user fa-lg"></i>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center user-info-sm">
                <b>{this.state.name}</b><br /> {new Date().toDateString()}</DropdownItem>
              <DropdownItem onClick={()=>this.props.onLogout('menual',this.clrinterval)}><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default withTranslation("translations")(DefaultHeader);
