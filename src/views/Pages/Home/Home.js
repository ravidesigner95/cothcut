import React, { Component } from 'react';
import { Button, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import lineloading from '../../../assets/img/lineloading.svg';
import productionDashboard from '../../../assets/img/productionDashboard.svg';
import hourlyDashboard from '../../../assets/img/hourlydashboard.svg';
import adminicon from '../../../assets/img/admin.svg';
import cpmimg from '../../../assets/img/cpm.svg'; 
import homeLogo from '../../../assets/img/logocpm.png';
class HomePage extends Component {
  render() {
    const heading = { 
      zIndex: '1'
    };
    return (
      <div className="container-fluid-inner homebg" >       
        <h1 className='pagesheading' style={heading}>Home</h1>
        <div className='clearfix'></div> 
        <Row>
          <Col sm='12'>
          <div  className='home-Logo'><img  src={homeLogo}  alt="Cpm" /></div>
          </Col>
        </Row>
        {/* <div className='home'>
          <div className="item_Box">
            <img  src={lineloading}  alt="Cpm" />
            <h4>Line Loading</h4>
             <p>Line capicicty and curreent status</p>  
            {/* <a  class="learn_btn">Go to CPM <i class="arrow_right"></i></a>  

          </div>
          <div className="item_Box ml-3">
            <img  src={productionDashboard}  alt="adminicon" />
            <h4>Production Dashboard</h4>
            <p>Plant Production status</p>
            {/* <a  class="learn_btn">Go to CPM <i class="arrow_right"></i></a>  
          </div>
          <div className="item_Box">
            <img  src={hourlyDashboard}  alt="Cpm" />
            <h4>Hourly Dashboard</h4>
            <p>Plant Hourly Production View</p>
            {/* <a  class="learn_btn">Go to CPM <i class="arrow_right"></i></a>  

          </div>
              <div className="item_Box">
            <img  src={cpmimg}  alt="Cpm" />
            <h4>CPM</h4>
            <p>Capacity planning management</p>
            {/* <a  class="learn_btn">Go to CPM <i class="arrow_right"></i></a>  

          </div>
          <div className="item_Box ml-3">
            <img  src={adminicon}  alt="adminicon" />
            <h4>Admin</h4>
            <p>Assign to role and team building</p>
            {/* <a  class="learn_btn">Go to CPM <i class="arrow_right"></i></a>  
          </div>
        </div> */}

      </div>
    );
  }
}

export default HomePage;
