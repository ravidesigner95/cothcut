class WorkingShiftModel {
    constructor(id, hour, shiftCategory, workCategory, startTime, endTime,shiftMasterId) {
        this.id = id;
        this.hour = hour;
        this.shiftCategory = shiftCategory;
        this.workCategory = workCategory;
        this.startTime = startTime;
        this.endTime = endTime;
        this.shiftMasterId=shiftMasterId;
    }
    id = null;
    hour = null;
    shiftCategory = false;
    workCategory = null;
    startTime = null;
    endTime = null;
    shiftMasterId=null;
}
export default WorkingShiftModel;