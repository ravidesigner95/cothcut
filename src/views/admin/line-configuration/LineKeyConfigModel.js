import ProductionProcess from '../../line/line-configuration/ProductionProcessModel';
export class LineKeyConfigurationModel {
    constructor( id,keyName,keyDescription,keyValue){
        this.keyName= keyName;
        this.keyDescription = keyDescription;
        this.keyValue  = keyValue;
        this.id = id;
    }   
}

export default LineKeyConfigurationModel;


