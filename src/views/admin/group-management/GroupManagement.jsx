import React from 'react';
import ReactDOM from 'react-dom';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { CheckBoxComponent, RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { ListBoxComponent } from '@syncfusion/ej2-react-dropdowns';
import { ListViewComponent } from '@syncfusion/ej2-react-lists';
import { nestedListData } from './listData';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import Groupmanagementservices from './groupmanagementservices';
import GroupManagementModel from './GroupMangementModel'
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { element } from 'prop-types';
import errorMessages from '../../errorHandler/errorMessages';
import { withTranslation } from "react-i18next";

class GroupManagement extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.groupmanagementservices = new Groupmanagementservices();
        this.state = {
            model: new GroupManagementModel(null, null, []),
            selectedGroupName: [],
            groups: [],
            filteredRoles: [],
            rolesFromModel: [],
            allRoles: [],
            isEdit: false,
            commands: [
                { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
                { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },],

            toasterPosition: { X: 'Right', Y: 'Bottom' },
            toasterMessage: [
                { title: 'Success!', content: 'Saved successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Success!', content: 'Deleted successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Information!', content: 'Please fill Group Name !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Information!', content: 'Please select Roles !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
            ],
            toolbarSettings: {
                items: [
                    "moveUp",
                    "moveDown",
                    "moveTo",
                    "moveFrom",
                    "moveAllTo",
                    "moveAllFrom"
                ]
            }
        }
    }

    componentDidMount() {
        this.getUserRoles();
        this.getGroups(null);
    }

    async getGroups(fetchRoles) {
        const response = await this.groupmanagementservices.getGroupList()
        if ((response != undefined) && (response.data.length > 0)) {
            if (fetchRoles != null) {
                this.listObj2.dataSource = [];
                this.listObj1.dataSource = [];
            }
            this.setState({ groups: response.data, model: new GroupManagementModel(null, null, []), isEdit: false, rolesFromModel: [], filteredRoles: [] })
            global.hideLoader();
        } else {
            global.hideLoader();
        }
    }

    async getUserRoles() {
        const response = await this.groupmanagementservices.getRolesList();
        if ((response != undefined) && (response.data.length > 0))
            this.setState({ allRoles: response.data })
    }

    async saveGroup() {
        const { t, i18n } = this.props;
        const response = (await this.groupmanagementservices.saveGroup(this.state.model))
        if (response.status != undefined && response.status == 'success') {
            this.toastObj.show(this.createToaster(response.message,'Success'));
            this.getGroups();
        }else if(response.status != undefined && response.status == "fail"){
            if(response.message != undefined){
                this.toastObj.show(this.createToaster(response.message,'Error'));
            }else{
                this.toastObj.show(this.createToaster(t(errorMessages[response.data.errorCode]),'Error')); 
            } 
            global.hideLoader();
        }else {
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[4]);
        }
    }

    async deleteGroup() {
        const { t, i18n } = this.props;
        const response = await this.groupmanagementservices.deleteGroup(this.state.model.id);
        if (response != undefined && response.data.status == "success") {
            this.toastObj.show(this.state.toasterMessage[1]);
            this.getGroups();
        }else if (response != undefined && response.data.status == "fail"){
            if(response.message != undefined){
                this.toastObj.show(this.createToaster(response.message,'Error'));
            }else{
                this.toastObj.show(this.createToaster(t(errorMessages[response.data.data.errorCode]),'Error')); 
            } 
            global.hideLoader();
        }
         else {
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[4]);
        }
    }


    createToaster(message, type) {
        switch (type) {
          case "Success":
            return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
          case "Error":
            return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
          case "Warning":
            return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
          case "Information":
            return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
      }




    onDragSourceRole = (event) => {
        let that = this;
        let rolesIds = [];
        event.destination.currentData.forEach(element => {
            rolesIds.push(element.id);
        });
        this.setState({ model: { ...that.state.model, roleName: rolesIds } })
    }

    onDragDestinationRole = (event) => {
        let that = this;
        let rolesIds = [];
        event.source.currentData.forEach(element => {
            rolesIds.push(element.id);
        });
        this.setState({ model: { ...that.state.model, roleName: rolesIds } })
    }


    onChangeText = (event) => {
        let that = this;
        that.setState({ model: { ...that.state.model, [event.target.id]: event.value.trim() } })
    }

    onSelectGroup = (event) => {
        let that = this;
        let filteredSourceRoles = [];
        let filteredDestinationRoles = [];
        let selectedModel = {};
        const { groups, allRoles } = that.state
        groups.forEach(element => {
            if (event.value[0] == element.id) {
                allRoles.forEach(item => {
                    if (element.roleName.includes(item.id))
                        filteredDestinationRoles.push(item);
                    else
                        filteredSourceRoles.push(item);
                })
                selectedModel = element;
            }
        })
        that.setState({ isEdit: true, rolesFromModel: filteredDestinationRoles, filteredRoles: filteredSourceRoles, model: selectedModel })
    }

    save = () => {
        let that = this;
        if (that.validateMandatoryFields()) {
            global.showLoader();
            that.saveGroup();
        }
    }

    cancel = () => {
        let that = this;
        that.listObj2.dataSource = [];
        that.listObj1.dataSource = [];
        that.setState({ model: new GroupManagementModel(null, null, []), isEdit: false, rolesFromModel: [], filteredRoles: [] })
    }

    delete = () => {
        global.showLoader();
        this.deleteGroup();
    }


    validateMandatoryFields = () => {
        const { model } = this.state;
        let isValidate = true;
        if ((model.groupName == null) || (model.groupName == "")) {
            this.toastObj.show(this.state.toasterMessage[2]);
            isValidate = false;
        }
        else if (model.roleName.length == 0) {
            this.toastObj.show(this.state.toasterMessage[3]);
            isValidate = false;
        }
        return isValidate;
    }

    render() {
        return (
            <div className="container-fluid-inner">
                <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>

                <h1 className='pagesheading'>Group Management</h1>
                <div className='clearfix'></div>
                <Row>
                    <Col xs='12' md="6">
                        <h4>Group List</h4><hr />
                        <div className="group-list">
                            <ListBoxComponent dataSource={this.state.groups} fields={{ text: 'groupName', value: 'id' }} height="450px" scope="#combined-listbox" floatLabelType="Auto" onChange={this.onSelectGroup} />

                        </div>
                    </Col>
                    <Col xs='12' md="6">
                        <div className="mt-3  d-block  d-sm-none"></div>
                        <h4>{this.state.isEdit ? "Edit Group" : "Create New Group"}</h4><hr />
                        <Row>
                            <Col xs='12' md='6'>
                                <div className="dual-list-groupa">
                                    <div className="mt-3"></div>
                                    <h6 className="text-heading"><span>Roles List</span></h6>
                                    {/* <ListBoxComponent id="role"  dataSource={this.GROUPA} fields={{ text: 'name', value: 'id' }} floatLabelType="Auto" onChange={this.handleRole} height="330px" scope="#combined-listbox" toolbarSettings={this.state.toolbarSettings} /> */}
                                    <ListBoxComponent ref={(scope) => { this.listObj1 = scope; }} id="grpA" dataSource={this.state.isEdit ? this.state.filteredRoles : this.state.allRoles}
                                        scope="combined-list" height="330px" allowDragAndDrop={true} fields={{ text: 'name', value: 'id' }} drop={this.onDragSourceRole} />

                                </div>
                            </Col>
                            <Col xs='12' md='6'>
                                <div className="dual-list-groupb">
                                    <TextBoxComponent placeholder=' Group Name' id="groupName" value={this.state.model.groupName} onChange={this.onChangeText} floatLabelType="Auto" />
                                    <div className="mt-2"></div>
                                    <ListBoxComponent ref={(scope) => { this.listObj2 = scope; }} id="grpB" dataSource={this.state.rolesFromModel} scope="combined-list" height="330px"
                                        allowDragAndDrop={true} fields={{ text: 'name', value: 'id' }} drop={this.onDragDestinationRole} />
                                    {/* <ListBoxComponent id="combined-listbox" height="330px"  fields={{ text: 'name', value: 'id' }} dataSource={this.GROUPB} onChange={this.handleRole} floatLabelType="Auto" height="330px" /> */}
                                </div>
                            </Col>
                        </Row>
                        <hr />
                        <Row className="mt-3">
                            <Col>
                                <Button color="primary" size="sm" id="save" className="float-left mr-3" onClick={this.save}>Save</Button>
                                {this.state.isEdit ?
                                    <Button color="primary" size="sm" id="cancel" className="float-left mr-3" onClick={this.delete}>Delete</Button>
                                    : null}
                                <Button color="primary" size="sm" id="cancel" className="float-left" onClick={this.cancel}>Cancel</Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default withTranslation("translations")(GroupManagement);
