import React from 'react';

import UserSubscription from './views/admin/user-subscription/user-subscription';
import HourlyProduction from './views/dashboard/hourly-prodcution/Dashboard';
import OrderManagement from './views/dashboard/ordermanagement/Dashboard';
import OrderManagementDashboard from './views/dashboard/ordermanagement/NonSecuredDashboard';
import ProdDashboard from './views/dashboard/Production/Dashboard';
import VcmDashboard from './views/dashboard/vcm/Dashboard';
import dashboard from './views/quality-material-mgt/dashboard/dashboard';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const LineConfiguration = React.lazy(() => import('./views/line/line-configuration/LineConfiguration'));
const LinePlanning = React.lazy(() => import('./views/line/line-planning/LinePlanning'));
const LineScheduling = React.lazy(() => import('./views/line/line-scheduling/LineScheduling'));
const Lineloading = React.lazy(() => import('./views/line/line-loading'));
const UserProfiling = React.lazy(() => import('./views/admin/user-management/UserProfiling'));
const UserPermission = React.lazy(() => import('./views/admin/permission/user-permission'));
const GroupManagement = React.lazy(() => import('./views/admin/group-management/GroupManagement'));
const LineProcess = React.lazy(() => import('./views/admin/line-Process/LineProcess'));
const LinConfiguration = React.lazy(() => import('./views/admin/line-configuration/LineConfiguration')); 
const InspectionComponent = React.lazy(() => import('./views/inspection/inspection'));
const InspectionListComponent = React.lazy(() => import('./views/inspection/inspectionList'));
const QualitymasterComponent = React.lazy(() => import('./views/qualitymaster/quality-master'));
const QualityDashboard = React.lazy(() => import('./views/quality-material-mgt/dashboard/dashboard'));
const ProdQualityDashboard = React.lazy(() => import('./views/quality-material-mgt/dashboard/ProdDashboard'));

const MappingMetrics = React.lazy(() => import('./views/quality-material-mgt/mapping-metrics/mapping-metrics'));
const QualityConsole = React.lazy(() => import('./views/quality-material-mgt/quality-console/quality-console'));
const BillTracking = React.lazy(() => import('./views/quality-material-mgt/bill-tracking/bill-tracking'));
 
const login = React.lazy(() => import('./views/Pages/Login'));
const HomePage = React.lazy(() => import('./views/Pages/Home'));


const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/line-loading', name: 'line Loading', component: Lineloading },
  { path: '/cpm/line-configuration', name: 'CPM / Line Configuration', component: LineConfiguration },
  { path: '/cpm/planning', name: 'CPM / Planning', component: LinePlanning },
  { path: '/cpm/scheduling', name: 'CPM / Scheduling', component: LineScheduling },
  { path: '/admin/user-subscription', name: 'admin /User Subscription', component: UserSubscription },
  { path: '/admin/user-management', name: 'admin / User Management', component: UserProfiling },
  { path: '/admin/user-permission', name: 'admin / User Permission', component: UserPermission },
  { path: '/admin/team-management', name: 'admin / Team Management', component: GroupManagement },
  { path: '/admin/line-Process', name: 'admin / Line Process', component: LineProcess },
  { path: '/admin/app-configuration', name: 'admin / App Configuration', component: LinConfiguration },
  { path: '/hourly-dashboard', name: 'Line Dashboard', component: HourlyProduction },
  { path: '/prod-Dashboard', name: 'Production Dashboard', component: ProdDashboard },
  { path: '/dashboad/vcm', name: 'Production Dashboard', component: VcmDashboard },
  { path: '/order-management', name: 'Production Dashboard', component: OrderManagement },
  { path: '/order-dashboard', name: 'Production Dashboard', component: OrderManagementDashboard },
  { path: '/quality-material-mgt/quality-store-dashboard', name: 'Quality Store Dashboard', component: QualityDashboard },
  { path: '/quality-material-mgt/quality-prod-dashboard', name: 'Quality Prod Dashboard', component: ProdQualityDashboard },

  { path: '/quality-material-mgt/mapping-metrics', name: 'Mapping Metrics', component: MappingMetrics },
  { path: '/quality-material-mgt/quality-console', name: 'Quality Console', component: QualityConsole },
  { path: '/quality-material-mgt/bill-tracking', name: 'Bill Tracking', component: BillTracking },
  { path: '/inspection', name: 'Inspection', component: InspectionComponent },
  { path: '/inspectionList', name: 'Inspection List', component:InspectionListComponent},
  { path: '/quality-master', name: 'Quality Master', component: QualitymasterComponent },
  { path: '/dashboad/prod-Dashboard', name: 'Production Dashboard', component: ProdDashboard },

  { path: '/login', name: 'Scheduling', component: login },
  { path: '/home', name: '', component: HomePage },   
];

export default routes;