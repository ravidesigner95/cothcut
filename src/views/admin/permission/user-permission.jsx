import { CheckBoxComponent } from '@syncfusion/ej2-react-buttons';
import { DropDownListComponent, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import { Toolbar, Search, ColumnDirective, ColumnsDirective, CommandColumn, GridComponent, Group, Inject, Page, Freeze, Sort, Resize, ColumnChooser } from '@syncfusion/ej2-react-grids';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import React from 'react';
import SplitterLayout from 'react-splitter-layout';
import 'react-splitter-layout/lib/index.css';
import { Button, Col, Row } from 'reactstrap';
import UserpermissionService from './userpermissionService';
import { ToastComponent } from '@syncfusion/ej2-react-notifications';
import { any } from 'prop-types';
import RoleModel from './RoleModel';
import errorMessages from '../../errorHandler/errorMessages';
import { withTranslation } from "react-i18next";

let refresh;
class UserPermission extends React.PureComponent {
    constructor() {
        super(...arguments);
        this.userpermissionService = new UserpermissionService();
        this.groupOptions = { showGroupedColumn: false, };
        this.moduleAcessList = [
            { "acl": 1, "role": "Read Only" },
            { "acl": 2, "role": "Read/Write Only" },
            { "acl": 4, "role": " Full Control" },
        ];
        this.fields = { text: "role", value: "acl" };
        this.value = 'role';
        this.groupList = [
            { "Id": "Group1", "group": "Group1" },
            { "Id": "Group1", "group": "Group1" },
        ];
        this.uploadWrapper = document.getElementsByClassName('e-upload')[0];
        this.dropContainerEle = null;
        this.dropContainerRef = element => {
            this.dropContainerEle = element;
        };
        this.asyncSettings = {
            saveUrl: 'https://aspnetmvc.syncfusion.com/services/api/uploadbox/Save',
            removeUrl: 'https://aspnetmvc.syncfusion.com/services/api/uploadbox/Remove'
        };
        this.state = {
            filterSettings: { type: 'CheckBox' },
            toolbarOptions: ['Search'],
            isSidebar: true,
            isEdit: false,
            isShowConfirmationDialog: false,
            toasterMessage: [
                { title: 'Warning!', content: 'Changes Discarded !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Success!', content: 'User added successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
                { title: 'Information!', content: 'Please read the comments carefully.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Success!', content: 'Role saved successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Success!', content: 'Role and Permissions saved successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Success!', content: 'Role deleted successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Warning!', content: 'Please add Role name', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Warning!', content: 'Please select valid permissions', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Warning!', content: 'Special Character Not Allowed In role Name', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
            ],
            animationSettings: { effect: 'FlipYLeft' },

            roleName: [],
            roleId: null,
            tableData: [],
            rolesList: [],
            moduleList: [],
            moduleSaveList: [],
            permissionList: [],
            confirmButton: [{
                click: (args) => {
                    this.delete(args);
                    // this.setState({ isShowConfirmationDialog: false });
                },
                buttonModel: { content: 'Yes', isPrimary: true }
            },
            {
                click: (args) => {
                    this.cancelConformDialog(args);
                    // this.setState({ isShowConfirmationDialog: false });
                },
                buttonModel: { content: 'No' }
            }],
            // permissionsToBeAdded: RolePermissionModel,
            model: {
                permissionDtos: [],
                roleName: undefined,

            },
            commands: [
                { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
                { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },],
            confirmButton: [{
                click: (args) => {
                    this.delete(args);
                },
                buttonModel: { content: 'Yes', isPrimary: true }
            },
            {
                click: (args) => {
                    this.cancelConformDialog(args);
                },
                buttonModel: { content: 'No' }
            }],
        }
    }
    componentDidMount() {
        global.showLoader();
        this.UserModuleData()
        this.createModel()
        document.addEventListener("keydown", this.escFunction, false);
    }
    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
    }
    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.setState({ isSidebar: true, isEdit: false })
        }
    }
    rendereComplete() {
        this.dropElement = this.dropContainerEle;
        this.uploadObj.dropArea = this.dropElement;
        this.uploadObj.dataBind();
        this.uploadObj.element.setAttribute('name', 'UploadFiles');
    }
    onRemoveFile(args) {
        args.postRawFile = false;
    }
    clearButtonClick() {
        this.uploadObj.clearAll();
    }
    load() {
        refresh = this.refreshing;
    }
    //------------------------------------API---------------------------------//
    async onSaveRole() {
        const { t, i18n } = this.props;
        let errorModulesList = new Array();
        if (this.state.model.roleName == undefined || this.state.model.roleName == null || this.state.model.roleName.trim() == "") {
            this.setState({ toasterMessage: this.state.toasterMessage });
            this.toastObj.show(this.state.toasterMessage[7]);
            return false;
        }
        this.state.model.permissionDtos.forEach(moduleDetails => {
            if (moduleDetails.permissionName == undefined || moduleDetails.acl == undefined) {
                errorModulesList.push(moduleDetails)
            }
        })
        if (errorModulesList.length) {
            this.toastObj.show(this.state.toasterMessage[8]);
        }
        else {
            global.showLoader();
            this.setState({ isSidebar: !this.state.isSidebar, })
            let response =any;
            if(this.state.isEdit){
                let roleDto=new RoleModel(this.state.roleId,this.state.model.roleName);
                response = await this.userpermissionService.updateRole(roleDto)
            }else{
                response = await this.userpermissionService.saveRole(this.state.model.roleName)
            } 
            
            if ( response.status != undefined && response.status == "success") {
                let permissionResponse = await this.userpermissionService.savePermission(this.state.model) 
                this.toastObj.show((permissionResponse != undefined && permissionResponse.status == "success") ? this.state.toasterMessage[5] : this.createToaster("Role Saved But Permission Not Saved",'Information')); 
            }else if(response.status != undefined && response.status == "fail"){
                if(response.message != undefined){
                    this.toastObj.show(this.createToaster(response.message,'Error'));
                }else{
                    this.toastObj.show(this.createToaster(t(errorMessages[response.data.errorCode]),'Error')); 
                } 
            }else {
                this.toastObj.show(this.state.toasterMessage[4]);
            }
            this.createModel()
        }
    }

    
    createToaster(message, type) {
        switch (type) {
          case "Success":
            return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
          case "Error":
            return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
          case "Warning":
            return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
          case "Information":
            return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
      }

    sideBar = (event, isEdit) => {
        let that = this;
        this.setState({ isSidebar: !this.state.isSidebar, isEdit: isEdit })

        if (!isEdit) {
            that.state.model.roleName = null
            that.state.model.permissionDtos = []
            that.state.moduleSaveList = []
            this.state.moduleList.forEach(moduleDetails => {
                let x = new ModuleAccessModel(moduleDetails.id, moduleDetails.moduleName, moduleDetails.isActive, moduleDetails.parentId, moduleDetails.modulUrl, moduleDetails.modulIcon, false, null)
                this.state.moduleSaveList.push(x)
            })
        }
        else if (isEdit) {
            that.state.model.roleName = event.value
        }
    }
    handleChange = (event) => {
        let that = this;
        that.state.model.roleName = event.value
    }
    async UserRoleData() {
        const responseData = (await this.userpermissionService.getroleList())
        this.setState({ rolesList: responseData.data })
    }
    async UserModuleData() {
        const { moduleList } = this.state;
        const responseData = (await this.userpermissionService.getmoduleList())
        responseData.data.forEach(moduleDetails => {
            if (moduleDetails.parentId == 0) {
                let modules = new ModuleAccessModel(moduleDetails.id, moduleDetails.moduleName, moduleDetails.isActive, moduleDetails.parentId, moduleDetails.modulUrl, moduleDetails.modulIcon, true, moduleDetails.acl)
                moduleList.push(modules);
            }
        })
        // this.setState({ moduleList: responseData.data })
    }
    async permissionData() {
        const responseData = (await this.userpermissionService.getPermissions(undefined))
        this.setState({ permissionList: responseData.message })
    }
    async createModel() {
        this.state.moduleSaveList = []
        this.state.model.roleName = null
        this.state.model.permissionDtos = []
        await this.UserRoleData()
        await this.permissionData()
        global.hideLoader();
        // this.state.moduleList.forEach(moduleDetails => {
        //     let x = new ModuleAccessModel(moduleDetails.id, moduleDetails.moduleName, moduleDetails.isActive, moduleDetails.parentId, moduleDetails.modulUrl, moduleDetails.modulIcon, false, null)
        //     this.state.moduleSaveList.push(x)
        // })

        // Object.keys(this.state.permissionList).map(key => {
        //     // <MyComponent key={ Math.random() } stateKey={ key } stateValue={ state[key] }/>
        //     let xx = this.state.permissionList[key]
        //     console.log(xx)
        // });
        // this.setState({ moduleAcessList: this.state.moduleSaveList })
    }

    selectModule = (event, moduleToSave) => {
        let that = this;
        let permissionsToBeAdded = new RolePermissionModel(moduleToSave.moduleName, moduleToSave.modulUrl, undefined)
        let modulesToBeDeleted = new Array()
        let addNewModule = that.state.model.permissionDtos.length ? false : true
        that.state.model.permissionDtos.forEach(modulesAdded => {
            if (modulesAdded.permissionName == permissionsToBeAdded.permissionName) {
                if (event.target.checked) {
                    modulesAdded = Object.assign({}, permissionsToBeAdded)
                }
                else {
                    modulesToBeDeleted.push(modulesAdded)
                }
            } else if (event.target.checked) {
                addNewModule = true;
            }
        })
        if (addNewModule)
            that.state.model.permissionDtos.push(permissionsToBeAdded)
        modulesToBeDeleted.forEach(modules => {
            this.state.model.permissionDtos.splice(this.state.model.permissionDtos.indexOf(modules), 1)
        })
        const changedObject = Object.assign({}, that.state.moduleSaveList[event.target.props.id], { checked: event.target.checked });
        const moduleSaveList = Object.assign([], that.state.moduleSaveList, { [event.target.props.id]: changedObject });
        that.setState({ moduleSaveList: moduleSaveList })
    }
    selectPermission(selectedModule, event) {
        this.state.model.permissionDtos.forEach(moduleDetails => {
            if (moduleDetails.permissionName == selectedModule)
                moduleDetails.acl = event.itemData.acl
        })
    }
    dynamicColumnsWIP = () => {
        let temp = [];
        temp.push(<ColumnDirective field='name' headerText='Roles' width='150' key={Math.random()} ></ColumnDirective>)
        if (this.state.permissionList) {
            Object.keys(this.state.permissionList).map(key => {
                temp.push(<ColumnDirective key={Math.random()} field='permissionName' dataSource={this.state.permissionList[key]} headerText={key} width='100' />)
            });
        }
        temp.push(<ColumnDirective field='action' key={Math.random()} allowSorting={false} allowFiltering={false} headerText={'Action'} width='60' commands={this.state.commands} ></ColumnDirective>)
        return temp;
    }
    customizeCell(args) {
        args.cell.classList.add('blankCell');
        if ((args.column).field == "permissionName"
            && args.data && args.cell && args.column.dataSource && args.column.dataSource.length) {
            args.column.dataSource.forEach(moduleDetails => {
                if (args.data.name == moduleDetails.roleName && moduleDetails.acl == 1) {
                    args.cell.classList.add('readOnly');
                } else if (args.data.name == moduleDetails.roleName && moduleDetails.acl == 2) {
                    args.cell.classList.add('readWriteOnly');
                } else if (args.data.name == moduleDetails.roleName && moduleDetails.acl == 4) {
                    args.cell.classList.add('readWriteDeleteOnly');
                }
            })
        }
    }

    // enablePermissionSelection(selectedModule) {
    //     let doEnable = false
    //     for (let modelIndex = 0; modelIndex < this.state.model.length; modelIndex++) {
    //         if (this.state.model[modelIndex].permissionName == selectedModule) {
    //             doEnable = true
    //         }
    //     }
    //     return doEnable
    // }
    rowselected = (args) => {                     // TO BE IMPROVED !!
        let that = this;
        if ((args.target.classList.length > 2) && (that.state.isSidebar == true)) {
            if ((args.target.classList.contains('e-edit')) && (args.data != undefined)) {
                that.onEditClick(args.data)
            }
            if ((args.target.classList.contains('e-delete')) && (args.data != undefined)) {
                // delete
                args.cancel = true;
                const name = args.data.name;
                this.setState({ isShowConfirmationDialog: true, lineIdToDelete: name })
            }
        }
    }
    async deleteUserPermission(roleName) {
        const { t, i18n } = this.props;
        global.showLoader();
        let response = (await this.userpermissionService.deletePermission(roleName))
        if ((response != undefined) && (response.data.status == "success")) {
            this.createModel()
            this.toastObj.show(this.state.toasterMessage[6]);
        }else if(response.data.status != undefined && response.data.status == "fail"){
            this.toastObj.show(this.createToaster(t(errorMessages[response.data.data.errorCode]),'Error')); 
        }else {
            this.toastObj.show(this.state.toasterMessage[2]);
        }
        global.hideLoader();
        this.setState({ lineIdToDelete: null });
    }

    delete = (args) => {
        const { lineIdToDelete } = this.state;
        this.deleteUserPermission(lineIdToDelete);
        this.setState({ isShowConfirmationDialog: false })

        //  this.UserModuleData()
        // this.permissionData()
        // setTimeout(() => {
        //    this.UserRoleData()
        //    this.setState({ isSidebar: true, isEdit: false, lineProcesses: this.state.resetLineProcesses, model: new PermissionModel(null, null, null, null, null, null, null, null, null, null, [], [], [], [], [], [], [], null, null) })
        // }, 2);
    }
    cancelConformDialog = (args) => {
        this.setState({ isShowConfirmationDialog: false, lineIdToDelete: null })
    }
    async onEditClick(data) {
        if (data != undefined) {
            this.state.roleId=data.id;
            this.state.moduleSaveList = []
            let response = (await this.userpermissionService.getPermissions(data.name))
            this.state.model.roleName = data.name
            this.state.model.permissionDtos = response.data
            let moduleSaveList = []
            this.state.moduleList.forEach(moduleDetails => {
                let checkIfChanged = false;
                this.state.model.permissionDtos.forEach(modulesAdded => {
                    if (moduleDetails.moduleName == modulesAdded.permissionName) {
                        let x = new ModuleAccessModel(moduleDetails.id, moduleDetails.moduleName, moduleDetails.isActive, moduleDetails.parentId, moduleDetails.modulUrl, moduleDetails.modulIcon, true, modulesAdded.acl)
                        moduleSaveList.push(x);
                        checkIfChanged = true
                    }
                })
                if (!checkIfChanged) {
                    let x = new ModuleAccessModel(moduleDetails.id, moduleDetails.moduleName, moduleDetails.isActive, moduleDetails.parentId, moduleDetails.modulUrl, moduleDetails.modulIcon, false, null)
                    moduleSaveList.push(x)
                }
            })
            this.setState({ isSidebar: false, isEdit: true, moduleSaveList: moduleSaveList })
        }
    }
    render() {
        const { t, i18n } = this.props;
        const { moduleList } = this.state;
        const { moduleSaveList } = this.state;
        const dynamicLineProcessesArray = [];
        for (let i = 0; i < moduleSaveList.length; i++) {
            dynamicLineProcessesArray.push(<div key={i}>
                <Row className='mt-2'>
                    <Col xs='12' md='12'>
                        <div className="mt-1"></div>
                        <CheckBoxComponent id={i} label={moduleSaveList[i].moduleName} onChange={(e) => this.selectModule(e, moduleSaveList[i])} checked={moduleSaveList[i].checked} />
                    </Col>
                    {moduleSaveList[i].checked ?
                        <Col xs='6' md='6'>
                            <DropDownListComponent dataSource={this.moduleAcessList} value={moduleSaveList[i].acl} fields={this.fields} select={(e) => this.selectPermission(moduleList[i].moduleName, e)} placeholder="Select Permission" />
                        </Col>
                        : null}
                </Row>
            </div>)
        }
        return (
            <div className="container-fluid-inner">
                <h1 className='pagesheading'>User Roles Permission</h1>
                <div className='clearfix'></div>
                <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>

                <Row>
                    <Col>
                        <div className='d-flex mb-2'>
                            <div className='mr-1'>  <Button color="primary" size="sm" onClick={e => this.sideBar(e, false)}>Assign Role Access</Button></div>
                            <div className='col-md-6'> <MultiSelectComponent id="selectApplication" dataSource={['All','VCM','Cash Mgt']} mode="Default" fields={this.fields} placeholder="Select Application" /> </div>
                            <div>  
                            <div className="top-legend float-right text-right">
                                    <div className='userNoright'>
                                        <span className="readOnly"></span>
                                    </div>
                                    <label>Read Only</label>
                                    <div className='userNoright'>
                                        <span className="readWriteOnly"></span>
                                    </div>
                                    <label>Read/Write Only</label>
                                    <div className='userNoright'>
                                        <span className="readWriteDeleteOnly"></span>
                                    </div>
                                    <label> Full Control</label>
                                </div>
                            </div>
                        </div>

                     
                        <GridComponent frozenColumns={1} toolbar={this.state.toolbarOptions} allowPaging={true} pageSettings={{ pageCount: 5, pageSizes: true }} className='userPermission' allowResizing={true} allowTextWrap={true} gridLines='Both' dataSource={this.state.rolesList} queryCellInfo={this.customizeCell} rowSelected={this.rowselected} >
                            <ColumnsDirective >
                                {this.dynamicColumnsWIP()}
                            </ColumnsDirective>
                            <Inject services={[CommandColumn, Freeze, Page, Resize, Group, Sort, Toolbar]} />
                        </GridComponent>
                        {/* ----------------------- Add User Section  -----------------------  */}
                        <div className="add-line-section" hidden={this.state.isSidebar}>
                            <SplitterLayout secondaryInitialSize={450} secondaryMinSize={450}>
                                <div> </div>
                                <div className="overflow-hidden add-line-section-form">
                                    <div className="heading mb-2">{this.state.isEdit ? "Edit Role" : "Add Role"} <a className="close" onClick={e => this.sideBar(e, false)}>+</a></div>
                                    <Col xs='12'>
                                        <Row>
                                            <Col xs='12' md='12'>
                                                <TextBoxComponent placeholder='Add New Role' id="roleName" value={this.state.model.roleName} onChange={this.handleChange} floatLabelType="Auto" />
                                            </Col>
                                        </Row>
                                        {dynamicLineProcessesArray}
                                        <hr />
                                        <Row className="mt-3">
                                            <Col>
                                                <Button color="primary" size="sm" id="save" className="float-left mr-3" onClick={e => this.onSaveRole(e)}>Save</Button>
                                                <Button color="primary" size="sm" id="cancel" className="float-left" onClick={e => this.sideBar(e, false)}>Cancel</Button>
                                            </Col>
                                        </Row>
                                    </Col>
                                </div>
                            </SplitterLayout>
                        </div>
                        <div className="add-line-section-overlay" hidden={this.state.isSidebar}></div>
                        {/* ----------------------- Add User end          -----------------------  */}
                        <DialogComponent id="confirmDialog" animationSettings={this.state.animationSettings} showCloseIcon={true} close={this.cancelConformDialog} buttons={this.state.confirmButton} header='Delete Role Permission !!' visible={this.state.isShowConfirmationDialog} width='400px' content='Are you sure you want to permanently delete Role ?'></DialogComponent>
                        <div className="overlayer-popup" hidden={true} ></div>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default withTranslation("translations")(UserPermission);
export class RolePermissionModel {
    constructor(permissionName, permissionUrl, acl, roleName) {
        this.permissionName = permissionName;
        this.permissionUrl = permissionUrl;
        this.acl = acl;
        this.roleName = roleName
    }
    roleName;
    permissionName;
    permissionUrl;
    acl;
}
export class ModuleAccessModel {
    id;
    moduleName;
    isActive;
    parentId;
    modulUrl;
    modulIcon;
    checked;
    acl;
    constructor(id, moduleName, isActive, parentId, modulUrl, modulIcon, checked, acl) {
        this.id = id;
        this.moduleName = moduleName;
        this.isActive = isActive;
        this.parentId = parentId;
        this.modulUrl = modulUrl;
        this.modulIcon = modulIcon
        this.checked = checked;
        this.acl = acl;

    }
}

