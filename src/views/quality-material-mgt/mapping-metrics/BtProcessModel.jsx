export class BtProcessModel {
    constructor(id,poTypeId, poTypeName, poGroupName, poGroupId,plantName,plantId,processFlowId, processFlowName, assigneeId, assigneeName,sequence,POBTProcessFlowDTOS) {
        this.id=id;
        this.poTypeId = poTypeId;
        this.poTypeName = poTypeName;
        this.poGroupName = poGroupName;
        this.poGroupId = poGroupId;
        this.plantName=plantName;
        this.plantId=plantId;
        this.processFlowId = processFlowId;
        this.processFlowName = processFlowName;
        this.assigneeId = assigneeId;
        this.assigneeName = assigneeName;
        this.sequence = sequence;
        this.POBTProcessFlowDTOS = POBTProcessFlowDTOS;
    }
    id=null;
    poTypeId = null;
    poTypeName = null;
    poGroupName = null;
    poGroupId = null;
    plantName=null;
    plantId=null;
    processFlowId = null;
    processFlowName = null;
    assigneeId = null;
    assigneeName = null;
    sequence=null;
    POBTProcessFlowDTOS = [];
}

export default BtProcessModel; 