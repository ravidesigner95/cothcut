import axios from 'axios';
class UserProfileService {

    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    roleUrl = this.baseUrl + `/roles`;
    groupUrl = this.baseUrl + `/team`;
    saveUrl = this.baseUrl + `/user`;
    getUserUrl = this.baseUrl + `/user`;
    plantListUrl = this.baseUrl + `/master/admin/plants`;
    unitListUrl = this.plantListUrl;
    MechandiserunitListUrl = this.baseUrl + `/user/MR`;
    QaListUrl = this.baseUrl + `/user/QAMGR`;
    apmListUrl = this.baseUrl + `/user/APM`;
    peListUrl = this.baseUrl + `/user/PE`;
    ProductionMgrListUrl = this.baseUrl + `/user/BH`;
    lineCrudUrl = this.baseUrl + `/unit/line`;
    allLinesUrl = this.baseUrl + `/master/lines`;
    groupUrl = this.baseUrl + `/group`;
    unitsUri=this.baseUrl+`/master/plant/units/`;

    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }

    };

   
    async getGridData() {
        try {
            const response = await axios.get(this.allLinesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getRoles() {
        console.log(this.roleUrl)
        try {

            const response = await axios.get(this.roleUrl, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }

    async getPlantList() { 
        try {
            const response = await axios.get(this.plantListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    // async getUnitList(plantId) {
    //     try {
    //         const response = await axios.get(this.unitListUrl + `/${plantId}/units/`, this.options);
    //         return response;
    //     }
    //     catch (error) {
    //         console.log(error);
    //         // alert("error")
    //     }
    // }

    async getgroupList() {
        try {
            const response = await axios.get(this.groupUrl, this.options)
            console.log("groupList",response)
            return response;
        }
        catch (error) {
            alert("error")
        }
    }
    async getMechandiserList() {
        try {
            const response = await axios.get(this.MechandiserunitListUrl, this.options);
            return response;
        }
        catch (error) {
            // return error;
            console.log("--------------");
            console.log(error);
            // alert("error")
        }
    }
    async getQaList() {
        try {
            const response = await axios.get(this.QaListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getAPMList() {
        try {
            const response = await axios.get(this.apmListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getPEList() {
        try {
            const response = await axios.get(this.peListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getProductionMgrList() {
        try {
            const response = await axios.get(this.ProductionMgrListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getGroups() {
        console.log(this.groupUrl)
        try {

            const response = await axios.get(this.groupUrl, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }
    async saveUser(data) {
        console.log(this.saveUrl)
        try {

            const response = await axios.post(this.saveUrl, data, this.options)
            return response.data;
        }
        catch (error) {
            alert(error)
        }
    }


    async getUsers() {
        console.log(this.getUserUrl)
        try {


            const response = await axios.get(this.getUserUrl, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }

    async editLine(editedLine) {
        try {
            const response = await axios.post(this.saveUrl, editedLine, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async deleteUser(deletedUserId) {
        try {
            const response = await axios.delete(this.getUserUrl + `?id=`+`${deletedUserId}`, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnits(plantList) {
        try {
            const response = await axios.post(this.unitsUri, plantList, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
}
export default UserProfileService;