import { RadioButtonComponent, SwitchComponent, CheckBoxComponent, ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { ComboBoxComponent, DropDownListComponent, CheckBoxSelection, MultiSelectComponent, DropDownList } from '@syncfusion/ej2-react-dropdowns';
import { ColumnDirective, ColumnsDirective, CommandColumn, DetailRow, Edit, Filter, Grid, GridComponent, Group, Inject, Page, Reorder, Resize, RowDD, Selection, SelectionType, Sort, Toolbar, VirtualScroll, Search } from '@syncfusion/ej2-react-grids';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { Toast, ToastComponent, ToastModel } from '@syncfusion/ej2-react-notifications';
import { AnimationSettingsModel, ButtonPropsModel, DialogComponent } from '@syncfusion/ej2-react-popups';
import React from 'react';
import '../../App.scss';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import DropdownItem from 'reactstrap/lib/DropdownItem';
import { data, inspectionDropdownListData, treedataSource } from '../../assets/data/data';
import { sampleData, sectionGridData, orderDataSourceData } from '../../assets/data/inspection'
import CriteriaformFieldComponent from './criteriaformfield';
import DclformComponent from './dclform.jsx';
import InspectionListComponent from './inspectionList';
import { names } from './nameSharing'
import QuestionformComponent from './questionform.jsx';
import ResultformComponent from './resultform.jsx';
import InspectionService from './InspectionService';
import ComponentModel, { InpectionTypeModel, CategoryModel, HeadTypeModel, ComponentSequenceModel, QualityInspAqlFpointDclModel } from './ComponentModel';
import SectionModel, { SubSectionModel } from './SectionModel';
import TreeViewModel, { childViewModel, ChildViewModel } from './TreeViewModel';
import SplitterLayout from 'react-splitter-layout';
import 'react-splitter-layout/lib/index.css';
import SubSectionAnsGrpModel from '../qualitymaster/SubSectionAnsGrpModel';
import { InspectionTypeInfoDTO } from './InspectionModel';
import { AppConfig } from '../../appConfig/appConfig';

class InspectionComponent extends React.PureComponent {
    constructor(props) {
        // console.log('data', props.location.data)
        super(...arguments);
        this.inspectionService = new InspectionService();

        this.commands = [

            { type: 'Edit Template', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
            { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },
            { type: 'Update', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
            // { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } }
        ];

        this.temp = 'categories';
        this.dateValue = new Date();
        this.inspectionId = props.location.data;
        this.searchData = data[this.temp];
        this.fields = { text: 'Name', value: 'Code' };
        // this.value = 'Game3';    

        this.Storecategsearch = data['Storecategsearch'];
        this.headtypesearchData = data['HeaderType'];
        this.insrulesearchData = data['insrulesearchData'];
        this.processtypesearchData = data['HeaderType'];
        this.countries = sampleData;
        this.state = {
            sectionSequenceNo: null,
            subSection: [],
            isSidebar: false,
            editHeaderDialog: false,
            editSetionDialog: false,
            headTypes: [],
            headerCategoryIds: [],
            categories: [],
            categoryIds: [],
            formCategories: ['Store', 'Production', 'Pre-production'],
            rules: [],
            inspections: [],
            selectedInspectionInfo: null,
            insId: null,
            insName: null,
            insCode: null,
            insDate: null,
            minDate: new Date(),
            insRevision: null,
            isAutomatic: false,
            insDesc: null,
            insRuleId: null,
            compId: null,
            compName: null,
            compDesc: null,
            compHeadTypeId: null,
            compHeadTypeName: null,
            sectionData: [],
            sectionId: null,
            sectionName: null,
            sectionDesc: null,
            sectionCategoryIds: [],
            selectedCompId: null,
            isPrefilled: false,
            subSectionModel: [],
            subSectionForSave: [],
            materialValue: null,
            uomValue: null,
            fpointValue: null,
            checkingValue: null,

            materialValueId: null,
            uomValueId: null,
            fpointValueId: null,
            checkingValueId: null,
            processValueId: null,

            sectionView: true,
            filterBox: true,
            filterBox2: true,
            filterBox3: true,
            filterBox4: true,
            tabContentSection: '',
            lineLoadView: false,
            sectionDialogBox: false,
            showMore: false,
            inspTypeInfo: [],
            productionProcessList: [],
            selectedProdProcess: null,
            //isHeaderInfoEdit: false,
            //headerInfovisibleCheckCss:'headerInfoHiddenCheckCss',
            toasterPosition: { X: 'Right', Y: 'Bottom' },

            // lineLoadView2: true,
            divHidden: null,
            showDialog: false,
            listfield: { dataSource: this.countries, id: 'id', text: 'name', parentID: 'pid', hasChildren: 'hasChild' },

            //Animate Store rules
            forStore: true,
            forProduction: true,
            forHeaderInfo: true,
            addNewInspectionSection: true,
            isEditHeader: false,
            isEditSection: false,

            inspectionDropdownList: [
                { id: 1, Name: 'Target GS', Designation: "Target GS - Packing Softhome Workflow", Type: 'gsp', totalinspection: 15 },
                { id: 2, Name: 'Target PS', Designation: "Target GS - Packing Softhome Workflow", Type: 'gsp', totalinspection: 15 },
            ],
            // maps the appropriate column to fields property
            inspectionDropdownListfields: { text: 'Name', value: 'Eimg' },

            treeData: [],

            sectionGrid: sectionGridData,
            gridHeight: window.innerHeight - 545 + 'px',

        }

        this.toolbarOptions = ['Add', 'Edit', 'Delete', 'Update', 'Cancel'];
        this.sectionCommands = [{ type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
        { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },
        { type: 'Save', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
        { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } }];
        this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Bottom' }
        this.childEditSettings = { allowEditing: false, allowAdding: true, allowDeleting: true, showDeleteConfirmDialog: true, newRowPosition: 'Bottom' }
        this.headerEditSettings = { allowEditing: false, allowAdding: true, allowDeleting: true, newRowPosition: 'Bottom' }
        // this.setState({
        //     orderDataSource: this.orderDataSource
        // });
        this.buttonRef = element => {
            this.buttonEle = element;
        };
        this.treefields = {
            dataSource: this.state.treeData, id: 'id', parentID: 'pid', text: 'name',
            hasChildren: 'hasChild',selected: 'isSelected'
        };
        this.childGrid = {
            dataSource: this.state.subSectionData,
            queryString: 'pid',
            allowPaging: true,
            editSettings: this.childEditSettings,
            pageSettings: { pageSize: 6, pageCount: 5 },
            // ref:this.sectionChildGrid,
            // ref:{grid ,...this.sectionChildGrid = grid},
            columns: [
                { field: 'type', headerText: 'Section Type', width: 120 },
                { field: 'text', headerText: 'Name', width: 120 },
                { field: 'mappingFieldName', headerText: 'Mapping Field', width: 120 },
                { field: 'subSectionQuesAnsGroupDTO.answerGroupName', headerText: 'Answer Type', width: 120 },
                // { field: 'description', headerText: 'Description', width: 120 },
                // { field: 'action', headerText: 'Actions', width: 120, template: this.viewActions },
            ],
        };
        // this.minDate= Date =new Date(new Date().getFullYear(), new Date().getMonth(), 7, 0, 0, 0);

        
        this.animationSettings = { effect: 'None' };
        this.allowDragAndDrops = true;

        this.sideBarPanel = this.sideBarPanel.bind(this);
        this.closeSidePanel = this.closeSidePanel.bind(this);
        this.showMoreToggle = this.showMoreToggle.bind(this);
    }

    buttons = [{
        click: () => {
            this.onSaveHeader();
        },
        buttonModel: {
            content: 'Save',
            isPrimary: true
        }
    },
    {
        click: () => {
            this.cleanHeaderGrid();
            this.headerdialogInstance.hide();
            this.setState({ showDialog: false });
        },
        buttonModel: {
            content: 'Cancel',
            cssClass: 'e-secondary'
        }
    }];
    sectionButtons = [{
        click: () => {
            this.onSaveSection();
        },
        buttonModel: {
            content: 'Save',
            isPrimary: true
        }
    },
    {
        click: () => {
            this.cleanSectionGrid();
        },
        buttonModel: {
            content: 'Cancel',
            cssClass: 'e-secondary'
        }
    }];

    componentDidMount() {
        this.getRulesList();
        this.getCategories();
        this.getInspCategories();
        this.getHeadTypes();
        this.getRolesList();
        this.getQuesCategoryList();
        this.getAllQuestions();
        this.getAqlList();
        this.getDefectList();
        this.getProductionProcesses();

    }

    componentWillReceiveProps(data) {
        if (this.props.history.location.data !== undefined && this.props.history.location.data !== null) {
            let id = this.props.history.location.data
            this.getSpecificInsp(id);
        }
    }

    ///////////////////////////////////////////////////////////////////////API CALLS///////////////////////////////////////////////////////////////////////////////////
    async getAqlList() {
        let response = (await this.inspectionService.getAqlList())
        global.showLoader();
        if ((response != undefined) && (response.data.status == "success")) {
            global.hideLoader();
            this.setState({ aqlList: response.data.data });
        }
        else {
            global.hideLoader();
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
        }
    }

    async getDefectList() {
        let response = (await this.inspectionService.getDefectsList())
        global.showLoader();
        if ((response != undefined) && (response.data.status == "success")) {
            global.hideLoader();
            this.setState({ defectList: response.data.data });
        }
        else {
            global.hideLoader();
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
        }
    }

    async getRolesList() {
        let response = (await this.inspectionService.getRolesList())
        if ((response != undefined) && (response.data.status == "success"))
            this.setState({ roles: response.data.data }, () => {
            })
    }

    async getRulesList() {
        let response = (await this.inspectionService.getRulesList())
        if ((response != undefined) && (response.status == 200)) {
            this.setState({ rules: response.data }, () => {
            })
            if (this.inspectionId != undefined) {
                this.setState({ isNewInsp: false });
                this.getSpecificInsp(this.inspectionId);
            }
            else {
                this.setState({ isNewInsp: true });
            }
        }


    }
    async getCategories() {
        let response = (await this.inspectionService.getCategoriesList())
        if ((response != undefined) && (response.status == 200))
            this.setState({ categories: response.data }, () => {
            })

    }

    async getProductionProcesses() {
        let response = (await this.inspectionService.getProductionProcesses())
        if ((response != undefined) && (response.status == 200))
            this.setState({ productionProcessList: response.data }, () => {
            })

    }

    async getInspCategories() {
        let response = (await this.inspectionService.getInspCategoriesList())
        if ((response != undefined) && (response.status == 200))
            this.setState({ inspCategories: response.data }, () => {
            })

    }

    async getQuesCategoryList() {
        let response = (await this.inspectionService.getQuesCategoryList())
        if ((response != undefined) && (response.data.status == "success"))
            this.setState({ quesCategory: response.data.data }, () => {
            })
    }

    async getHeadTypes() {
        let response = (await this.inspectionService.getHeadTypes())
        if ((response != undefined) && (response.status == 200))
            this.setState({ headTypes: response.data }, () => {
            })
    }
    async getComponents(ids) {
        let response = (await this.inspectionService.getComponents(ids))
        if ((response != undefined) && (response.status == 200))
            this.setState({ components: response.data }, () => {
            })
    }

    async getSections(ids) {
        let response = (await this.inspectionService.getSections(ids))
        if ((response != undefined) && (response.status == 200))
            this.setState({ sections: response.data }, () => {
            })
    }

    async getSpecificInsp(id) {
        global.showLoader();
        let response = (await this.inspectionService.getSpecificInspDetails(id))
        global.hideLoader();
        if ((response != undefined) && (response.status == 200)) {

            this.setState({ selectedInspectionInfo: response.data }, () => {
            })
            let categoriesIdsList = response.data.inspectionCategoryMasterDTO != null ? response.data.inspectionCategoryMasterDTO.id : "";
            //create tree view of components
            let treeData = [];
            response.data.qualityComponentMasterDTOS.map((compItem,i) => {
                if(this.state.selectedCompId){
                    if(compItem.id == this.state.selectedCompId){
                        treeData.push(new TreeViewModel(compItem.id, compItem.componentName, compItem.qualitySectionMasterDTOList.length > 0 ? true : false, compItem.qualitySectionMasterDTOList.length,true));
                        if (compItem.qualitySectionMasterDTOList.length > 0) {
                            compItem.qualitySectionMasterDTOList.map(sectionItem => {
                                treeData.push(new ChildViewModel(sectionItem.id, compItem.id, sectionItem.sectionName, sectionItem.qualitySubSectionDTOs.length,true));
                            })
                        }
                    }else{
                        treeData.push(new TreeViewModel(compItem.id, compItem.componentName, compItem.qualitySectionMasterDTOList.length > 0 ? true : false, compItem.qualitySectionMasterDTOList.length,false));
                        if (compItem.qualitySectionMasterDTOList.length > 0) {
                            compItem.qualitySectionMasterDTOList.map(sectionItem => {
                                treeData.push(new ChildViewModel(sectionItem.id, compItem.id, sectionItem.sectionName, sectionItem.qualitySubSectionDTOs.length,false));
                            })
                        }
                    }
                }else{
                    if(i===0){
                        treeData.push(new TreeViewModel(compItem.id, compItem.componentName, compItem.qualitySectionMasterDTOList.length > 0 ? true : false, compItem.qualitySectionMasterDTOList.length,true));
                        if (compItem.qualitySectionMasterDTOList.length > 0) {
                            compItem.qualitySectionMasterDTOList.map(sectionItem => {
                                treeData.push(new ChildViewModel(sectionItem.id, compItem.id, sectionItem.sectionName, sectionItem.qualitySubSectionDTOs.length,true));
                            })
                        }
                    }else{
                        treeData.push(new TreeViewModel(compItem.id, compItem.componentName, compItem.qualitySectionMasterDTOList.length > 0 ? true : false, compItem.qualitySectionMasterDTOList.length,false));
                        if (compItem.qualitySectionMasterDTOList.length > 0) {
                            compItem.qualitySectionMasterDTOList.map(sectionItem => {
                                treeData.push(new ChildViewModel(sectionItem.id, compItem.id, sectionItem.sectionName, sectionItem.qualitySubSectionDTOs.length,false));
                            })
                        }
                    }
                }
                
            });
            let seqNo = 1;
            if (response.data.qualityComponentMasterDTOS != undefined && response.data.qualityComponentMasterDTOS != null && response.data.qualityComponentMasterDTOS.length > 0) {
                if (response.data.qualityComponentMasterDTOS[0].qualitySectionMasterDTOList != null)
                    seqNo = response.data.qualityComponentMasterDTOS[0].qualitySectionMasterDTOList.length + 1;
            }

            this.treeViewGridObj.fields.dataSource = treeData;
            //setInspectionTypeInfo
            this.state.selectedInspectionInfo.inspTypeInfo.map(item => {
                if (item.name == "MATERIAL") { this.state.materialValue = item.value; this.state.materialValueId = item.id; }
                else if (item.name == "UOM") { this.state.uomValue = item.value; this.state.uomValueId = item.id; }
                else if (item.name == "Checking") { this.state.checkingValue = item.value; this.state.checkingValueId = item.id; }
                else if (item.name == "FPOINT") { this.state.fpointValue = item.value; this.state.fpointValueId = item.id; }
                else if (item.name == "Process") { this.state.selectedProdProcess = item.value; this.state.processValueId = item.id; }
            })
            this.inspectionId = id;
            this.setState({
                sectionSequenceNo: seqNo,
                treeData: treeData, insId: this.state.selectedInspectionInfo.id, insName: this.state.selectedInspectionInfo.name, insCode: this.state.selectedInspectionInfo.code, insDate: this.state.selectedInspectionInfo.date, insRuleId: this.state.selectedInspectionInfo.rule
                , insDesc: this.state.selectedInspectionInfo.description, insRevision: this.state.selectedInspectionInfo.revision, inspCategoryIds: categoriesIdsList, isAutomatic: this.state.selectedInspectionInfo.isAutomatic
                , subSectionModel: [], selectedComp: null
            })
            if (response.data.qualityComponentMasterDTOS.length > 0) {
                this.onCompClickGetSections(response.data.qualityComponentMasterDTOS[0].id);
                this.setState({ isNewInsp: false })
            }
            else
                this.setState({ isNewInsp: true })

        }
        else
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }



    async saveInspection(newIns) {
        global.showLoader();
        let response = (await this.inspectionService.saveInspectionType(newIns))
        if ((response != undefined) && (response.data.status == "success")) {
            this.toastObj.show(this.createToaster("Inspection saved successfully!!", "Success"));
            this.getSpecificInsp(response.data.data);
        }
        else if ((response != undefined) && (response.data.status == "unsuccess")) {
            this.toastObj.show(this.createToaster(response.data.message, "Information"));
        }
        else
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
        global.hideLoader();
    }

    async saveHeader(headerInfo) {
        global.showLoader();
        let response = (await this.inspectionService.saveComponent(headerInfo))
        if ((response != undefined) && (response.data.status == "success")) {
            this.headerdialogInstance.hide();
            this.setState({ showDialog: false });
            global.hideLoader();
            this.toastObj.show(this.createToaster(`Header ${this.state.isEditHeader ? 'update' : 'saved'} successfully!!`, "Success"));
            this.cleanHeaderGrid();
            this.getSpecificInsp(response.data.data);
        }
        else if ((response != undefined) && ((response.data != undefined)) && (response.data.status == "unsuccess")) {
            this.toastObj.show(this.createToaster(response.data.message, "Information"));
        } else {
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
        }
        global.hideLoader();
    }

    async saveExistingHeader(headerInfo) {
        let response = (await this.inspectionService.addExistingComponent(headerInfo))
        if ((response != undefined) && (response.data.status == "success")) {
            this.toastObj.show(this.createToaster("Header saved successfully!!", "Success"));
            this.cleanHeaderGrid();
            this.getSpecificInsp(response.data.data);
        }
        else if ((response != undefined) && (response.data.status == "unsuccess")) {
            // this.setState({ isDisabledSaveButton: false })
            this.toastObj.show(this.createToaster("The Selected Header is Already Added!!", "Information"));
        }
        else
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));

    }

    async saveExistinSection(sectionInfo) {
        let response = (await this.inspectionService.addExistingSection(sectionInfo))
        if ((response != undefined) && (response.data.status == "success")) {
            this.toastObj.show(this.createToaster("Section saved successfully!!", "Success"));
            this.cleanSectionGrid();
            // Returns model data not Id ******** ISSUE ********* //
            this.getSpecificInsp(this.inspectionId);
        }
        else if ((response != undefined) && (response.data.status == "unsuccess")) {
            // this.setState({ isDisabledSaveButton: false })
            this.toastObj.show(this.createToaster("The Selected Section is Already Added!!", "Information"));
        }
        else
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }


    cleanHeaderGrid = () => {
        this.headerdialogInstance.hide();
        this.setState({ compId: null, compName: null, compDesc: null, headerCategoryIds: [], aqlOrDclId: null })
    }

    cleanSectionGrid = () => {
        this.dialogInstance.hide();
        this.setState({ sectionView: true, sectionId: null, sectionName: null, sectionDesc: null, sectionCategoryIds: [], subSectionModel: [], showDialog: false, subSection: [] })
    }

    async saveSection(sectionInfo) {
        global.showLoader();
        let response = (await this.inspectionService.saveSection(sectionInfo))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ showDialog: false });
            this.toastObj.show(this.createToaster("Section saved successfully!!", "Success"));
            this.dialogInstance.hide();
            global.hideLoader();
            this.getSpecificInsp(this.inspectionId);
        }
        else
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
        global.hideLoader()
    }

    async getAllQuestions() {
        let response = (await this.inspectionService.getQuesList())
        if ((response != undefined) && (response.data.status == "success")) {
            let questions = []; let fields = [];

            response.data.data.map(item => {
                if (item.quesType == 'Field') {
                    fields.push(item);
                }
                else {
                    questions.push(item);
                }
            })
            this.setState({ ques: questions, field: fields }, () => {
            })
        }
    }

    async deleteComp(inspId, compId) {
        let response = (await this.inspectionService.deleteComponent(inspId, compId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.toastObj.show(this.createToaster("Header deleted successfully!!", "Success"));
            this.setState({compId:null,selectedCompId:null})
            this.getSpecificInsp(this.inspectionId);
        }
    }

    async deleteSection(compId, sectionId) {
        let response = (await this.inspectionService.deleteSection(compId, sectionId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.toastObj.show(this.createToaster("Section deleted successfully!!", "Success"));
            this.getSpecificInsp(this.inspectionId);
        }
    }

    async deleteSubSection(subSectionId) {
        let response = (await this.inspectionService.deleteSubSection(subSectionId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.toastObj.show(this.createToaster("Sub section deleted successfully!!", "Success"));
            this.getSpecificInsp(this.inspectionId);
        }
    }


    async getQuesByCategory() {
        // let response = (await this.inspectionService.getQuestionsByCategory(this.state.selectedQuesCategory))
        // if ((response != undefined) && (response.data.status == "success"))
        //     this.setState({ ques: response.data.data }, () => {
        //     })
    }



    createToaster(message, type) {
        switch (type) {
            case "Success":
                return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
            case "Error":
                return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
            case "Warning":
                return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
            case "Information":
                return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
    }
    filterBoxOpen = () => {
        this.setState({
            filterBox: !this.state.filterBox
        });
    }
    filterBoxOpen2 = () => {
        this.setState({
            filterBox2: !this.state.filterBox2
        });
    }
    filterBoxOpen3 = () => {
        this.setState({
            filterBox3: !this.state.filterBox3
        });
    }
    filterBoxOpen4 = () => {
        this.setState({
            filterBox4: !this.state.filterBox4
        });
    }

    viewTabs = (selecttab) => {
        this.setState({ tabContentSection: selecttab, sectionType: selecttab });
    }

    lineLoadView = (view) => {
        if (!view) {
            this.setState({ compId: null })
        }
        this.setState({ lineLoadView: view, compHeadTypeName: null, compHeadTypeId: null })
    }

    sectionView = (view) => {
        if (!view) {
            this.setState({ sectionId: null })
        }
        this.setState({ sectionView: view })
    }


    linkClicked = (args) => {
        this.getHeadTypes();
        if (args == 'new') {
            this.setState({
                orderDataSource: []
            });
            this.setState({ lineLoadView: true, isEditHeader: false, showDialog: true, compId: null, compHeadTypeId: null, compHeadTypeName: null }, () => {
                this.categoryMultiObj.value = [];
                this.headerInfoDropObj.value = null;
            });
        }
        else if (args == 'Edit') {
            let component = this.state.selectedComp;
            let compCategory = [];
            component[0].qualityCategoryMasterDTOList.map(item => {
                compCategory.push(item.id);
            })
            this.setState({
                lineLoadView: true,
                isEditHeader: true, showDialog: true, compId: component[0].id, compName: component[0].componentName,
                compDesc: component[0].componentDesc, headerCategoryIds: compCategory,
                compHeadTypeId: component[0].qualityHeaderTypeMasterDTO != null ? component[0].qualityHeaderTypeMasterDTO.id : null, aqlOrDclId: component[0].qualityInspAqlFpointDclDTO != null ? component[0].qualityInspAqlFpointDclDTO.dclAqlId : null
            });
        }
        else if (args == 'Delete') {
            this.deleteComp(this.inspectionId, this.state.compId);
        }
    }

    showMoreToggle() {
        if (this.state.showMore == true) { this.setState({ showMore: false }) }
        else { this.setState({ showMore: true }) }
    }

    dialogClose() {
        this.setState({
            showDialog: false,
            editHeaderDialog: false,
            editSetionDialog: false,
            sectionDialogBox: false,
            lineLoadView: true,
        });
    }

    onChange(event) {
        let key = event.itemData.Name
        switch (key) {
            case names.store:
                this.setState({
                    forProduction: true,
                    forStore: false
                })
                break;
            case names.production:
                this.setState({
                    forProduction: false,
                    forStore: true
                })
                break;

            default:
                this.setState({
                    forProduction: false,
                    forStore: false
                })
                break;
        }


    }


    onDragRow = (args) => {
        let that = this;
        let sequencedArray = this.moveArrayItemToNewIndex(this.state.subSection, args.fromIndex, args.dropIndex);
        sequencedArray.forEach((section, index) => {
            section.sequence = index + 1;
        })
        that.state.subSection = sequencedArray;
    }

    moveArrayItemToNewIndex(arr, old_index, new_index) {
        if (new_index >= arr.length) {
            var k = new_index - arr.length + 1;
            while (k--) {
                arr.push(undefined);
            }
        }
        arr.splice(new_index, 0, arr.splice(old_index, 1)[0]);
        return arr;
    };

    actionBeginFieldForm = (args) => {
        let that = this;
        const { subSectionModel, isPrefilled, tabContentSection } = that.state;
        if ((args.requestType === 'save') && (args.data != undefined && args.data.text != undefined && args.data.text != null && args.data.text.trim() != "")
            && ((args.data.isPrefilled == false && args.data.mappingFieldName == undefined) || (args.data.isPrefilled == true && args.data.mappingFieldName != undefined))) {
            //Add record to bottom of grid
            if (this.fieldFormGrid.editSettings.newRowPosition === 'Bottom') {
                args.index = (this.fieldFormGrid.pageSettings.currentPage * this.fieldFormGrid.pageSettings.pageSize) - 1;
            }
            let quesObj = that.state.field.filter(function (item) {
                return item.quesText == args.data.mappingFieldName;
            })
            // Add Record
            if (((args.data.id == null) || (args.data.id == undefined)) && ((args.data.uniqueId == null) || (args.data.uniqueId == undefined))) {
                args.cancel = true;
                let uniqueIdForUi = parseInt(Math.random() * (1000000 - 1) + 1);
                args.data.uniqueId = uniqueIdForUi;
                let tempArray = that.state.subSection == null ? [] : that.state.subSection;
                tempArray.push(new SubSectionModel(null, args.data.isPrefilled, quesObj.length > 0 ? quesObj[0].id : null, subSectionModel.length + 1, args.data.text, tabContentSection, null, quesObj.length > 0 ? quesObj[0].mappingField : null, args.data.mappingFieldName, null,args.data.isMandate,args.data.isEditable,args.data.role, uniqueIdForUi));
                this.setState({ subSection: tempArray }, () => {
                    // that.fieldFormGrid.closeEdit();
                    that.fieldFormGrid.refresh()
                })
                // that.state.subSection = tempArray;
            }
            // Edit Record Created On UI
            else if (((args.data.id == null) || (args.data.id == undefined)) && ((args.data.uniqueId != null) || (args.data.uniqueId != undefined))) {
                that.state.subSection.forEach(subSection => {
                    if (subSection.uniqueId == args.data.uniqueId) {
                        subSection.text = args.data.text;
                        subSection.isPrefilled = args.data.isPrefilled;
                        subSection.referenceId = quesObj.length > 0 ? quesObj[0].id : null;
                        subSection.mappingField = quesObj.length > 0 ? quesObj[0].mappingField : null;
                        subSection.mappingFieldName = args.data.mappingFieldName;
                        subSection.isEditable=args.data.isEditable;
                        subSection.isMandate=args.data.isMandate;
                        subSection.role=args.data.role;

                    }
                })
            }
            // Edit Record Created On Backend
            else if ((args.data.id != null) || (args.data.id != undefined)) {
                that.state.subSection.forEach(subSection => {
                    if (subSection.id == args.data.id) {
                        subSection.text = args.data.text;
                        subSection.isPrefilled = args.data.isPrefilled;
                        subSection.referenceId = quesObj.length > 0 ? quesObj[0].id : null;
                        subSection.mappingField = quesObj.length > 0 ? quesObj[0].mappingField : null;
                        subSection.mappingFieldName = args.data.mappingFieldName;
                        subSection.isEditable=args.data.isEditable;
                        subSection.isMandate=args.data.isMandate;
                        subSection.role=args.data.role;
                    }
                })
            }
        } else if (((args.requestType === 'save') && (args.data != undefined)) && (((args.data.text == undefined) || (args.data.text == null)) || ((args.data.text != undefined) && (args.data.text.trim() == "")))) {
            args.cancel = true;
            this.toastObj.show(this.createToaster("Please enter valid fields !!", "Information"));
        } else if (((args.requestType === 'save') && (args.data != undefined)) &&
            (((args.data.isPrefilled == true) && (args.data.mappingFieldName == null || args.data.mappingFieldName == undefined))
                || ((args.data.isPrefilled == false) && (args.data.mappingFieldName != undefined) || (args.data.mappingFieldName != null)))) {
            args.cancel = true;
            this.toastObj.show(this.createToaster("Please select Prefilled or Mapping field !!", "Information"));
        }
        else if (args.requestType === 'delete') {
            let tempArray = [];
            if (args.data[0].id == undefined || args.data[0].id == null) {
                tempArray = that.state.subSection.filter(element => element.uniqueId != args.data[0].uniqueId)
            } else {
                tempArray = that.state.subSection.filter(element => element.id != args.data[0].id)
            }
            that.state.subSection = tempArray;
            that.fieldFormGrid.dataSource = tempArray;
        }
    }

    actionBeginQuestionForm = (args) => {
        let that = this;
        const { subSectionModel, tabContentSection } = that.state;

        if ((args.requestType === 'save') && ((args.data.text != undefined) || (args.data.text != null))) {
            //Add record to bottom of grid
            if (this.questionFormGrid.editSettings.newRowPosition === 'Bottom') {
                args.index = (this.questionFormGrid.pageSettings.currentPage * this.questionFormGrid.pageSettings.pageSize) - 1;
            }
            let quesObj = that.state.ques.filter(function (item) {
                return item.quesText == args.data.text;
            })
            let subSectionAnsGrpOption = [];
            quesObj[0].qualityQuesAnsGroupDTO.map(item => {
                if (item.ansGrpName == args.data.subSectionQuesAnsGroupDTO.answerGroupName)
                    subSectionAnsGrpOption = (item.quesAnsGrpOption);
            })
            let subSectionAnsGrp = new SubSectionAnsGrpModel(null, args.data.subSectionQuesAnsGroupDTO.answerGroupName, "question", subSectionAnsGrpOption);
            // Add Record
            if (((args.data.id == null) || (args.data.id == undefined)) && ((args.data.uniqueId == null) || (args.data.uniqueId == undefined))) {
                args.cancel = true;
                let uniqueIdForUi = parseInt(Math.random() * (1000000 - 1) + 1);
                args.data.uniqueId = uniqueIdForUi;
                let tempArray = that.state.subSection == null ? [] : that.state.subSection;
                tempArray.push(new SubSectionModel(null, null, quesObj.length > 0 ? quesObj[0].id : null, subSectionModel.length + 1, args.data.text, tabContentSection, null, null, null, subSectionAnsGrp,args.data.isMandate,args.data.isEditable,args.data.role, uniqueIdForUi));
                // that.state.subSection = tempArray;
                this.setState({ subSection: tempArray }, () => {
                    that.questionFormGrid.refresh()
                })
            }
            // Edit Record Created On UI
            else if (((args.data.id == null) || (args.data.id == undefined)) && ((args.data.uniqueId != null) || (args.data.uniqueId != undefined))) {
                that.state.subSection.forEach(subSection => {
                    if (subSection.uniqueId == args.data.uniqueId) {
                        subSection.text = args.data.text;
                        subSection.referenceId = quesObj.length > 0 ? quesObj[0].id : null;
                        subSection.subSectionQuesAnsGroupDTO = subSectionAnsGrp;
                        subSection.isEditable=args.data.isEditable;
                        subSection.isMandate=args.data.isMandate;
                        subSection.role=args.data.role;
                    }
                })
            }
            // Edit Record Created On Backend
            else if ((args.data.id != null) || (args.data.id != undefined)) {
                that.state.subSection.forEach(subSection => {
                    if (subSection.id == args.data.id) {
                        subSection.text = args.data.text;
                        subSection.referenceId = quesObj.length > 0 ? quesObj[0].id : null;
                        subSection.subSectionQuesAnsGroupDTO = subSectionAnsGrp;
                        subSection.isEditable=args.data.isEditable;
                        subSection.isMandate=args.data.isMandate;
                        subSection.role=args.data.role;
                    }
                })
            }
        } else if ((args.requestType === 'save') && ((args.data.text == undefined) || (args.data.text == null))) {
            args.cancel = true;
            this.toastObj.show(this.createToaster("Please enter valid fields !!", "Information"));
        }
        else if (args.requestType === 'delete') {
            let tempArray = [];
            if (args.data[0].id == undefined || args.data[0].id == null) {
                tempArray = that.state.subSection.filter(element => element.uniqueId != args.data[0].uniqueId)
            } else {
                tempArray = that.state.subSection.filter(element => element.id != args.data[0].id)
            }
            that.state.subSection = tempArray;
            that.questionFormGrid.dataSource = tempArray;
        }
    }


    actionBeginRoleForm = (args) => {
        let that = this;
        const { subSectionModel, tabContentSection, roleName } = that.state;
        if ((args.requestType === 'save') && ((args.data.text != undefined) || (args.data.text != null))) {
            //Add record to bottom of grid
            if (this.resultFormGrid.editSettings.newRowPosition === 'Bottom') {
                args.index = (this.resultFormGrid.pageSettings.currentPage * this.resultFormGrid.pageSettings.pageSize) - 1;
            }
            //Add record to bottom of grid
            if (((args.data.id == null) || (args.data.id == undefined)) && ((args.data.uniqueId == null) || (args.data.uniqueId == undefined))) {
                args.cancel = true;
                let uniqueIdForUi = parseInt(Math.random() * (1000000 - 1) + 1);
                args.data.uniqueId = uniqueIdForUi;
                let tempArray = that.state.subSection == null ? [] : that.state.subSection;
                tempArray.push(new SubSectionModel(null, null, null, subSectionModel.length + 1, args.data.text, tabContentSection, null, null, null, null,args.data.isMandate,args.data.isEditable,args.data.role, uniqueIdForUi));
                // that.state.subSection = tempArray;
                this.setState({ subSection: tempArray }, () => {
                    that.resultFormGrid.refresh()
                })
            }
            // Edit Record Created On UI
            else if (((args.data.id == null) || (args.data.id == undefined)) && ((args.data.uniqueId != null) || (args.data.uniqueId != undefined))) {
                that.state.subSection.forEach(subSection => {
                    if (subSection.uniqueId == args.data.uniqueId) {
                        subSection.text = args.data.text;
                        subSection.isEditable=args.data.isEditable;
                        subSection.isMandate=args.data.isMandate;
                        subSection.role=args.data.role;
                    }
                })
            }
            // Edit Record Created On Backend
            else if ((args.data.id != null) || (args.data.id != undefined)) {
                that.state.subSection.forEach(subSection => {
                    if (subSection.id == args.data.id) {
                        subSection.text = args.data.text;
                        subSection.isEditable=args.data.isEditable;
                        subSection.isMandate=args.data.isMandate;
                        subSection.role=args.data.role;
                    }
                })
            }
        }
        else if ((args.requestType === 'save') && ((args.data.text == undefined) || (args.data.text == null))) {
            args.cancel = true;
            this.toastObj.show(this.createToaster("Please enter valid fields !!", "Information"));
        }
        else if (args.requestType === 'delete') {
            let tempArray = [];
            if (args.data[0].id == undefined || args.data[0].id == null) {
                tempArray = that.state.subSection.filter(element => element.uniqueId != args.data[0].uniqueId)
            } else {
                tempArray = that.state.subSection.filter(element => element.id != args.data[0].id)
            }
            that.state.subSection = tempArray;
            that.resultFormGrid.dataSource = tempArray;
        }
    }

    createInsdrop(args) {
        this.elem = document.createElement('input');
        return this.elem;
    }
    readInsdrop(args) {

        return this.dropInstance.value;
    }
    destroyInsdrop(args) {
        this.dropInstance.destroy();
    }
    writeInsdrop(args) {
        debugger;
        this.dropInstance = new DropDownList({
            dataSource: this.state.ques,
            fields: { text: 'quesText', value: 'quesText' },
            value: (args.rowData).text,
            change: this.change.bind(this),
            allowFiltering: true
        });
        this.dropInstance.appendTo(this.elem);
    }

    change(args) {
        let that = this;
        let quesObj = that.state.ques.filter(function (item) {
            return item.quesText == args.value;
        })
        let ansType = quesObj[0].qualityQuesAnsGroupDTO.filter(function (item) {
            return item.id == quesObj[0].quesGroupId;
        })
        args.element.form[2].value = ansType[0].ansGrpName;
    }

    quesParams = {
        create: this.createInsdrop.bind(this),
        read: this.readInsdrop.bind(this),
        destroy: this.destroyInsdrop.bind(this),
        write: this.writeInsdrop.bind(this)
    };

    rowSelectedSectionGrid = (args) => {
        let that = this;
        let selectedData = args.data;
        let selectedSectionObj = that.state.sectionData.filter(function (item) {
            return item.id == args.data.id;
        })
        //   that.sectiongridInstance.childGrid.dataSource = selectedSectionObj[0].qualitySubSectionDTOs; 
        //   that.sectiongridInstance.refresh();
    }

    cmdForGeneralInfo = (args) => {
        let that = this;
        if (args.commandColumn.type === "Edit") {
            let sectionCategory = [];
            args.rowData.qualityCategoryMasterDTOList.map(item => {
                sectionCategory.push(item.id);
            })
            let sectionType = null;
            args.rowData.qualitySubSectionDTOs.map(item => {
                sectionType = item.type;
            })
            if (args.rowData != undefined)
                this.state.sectionSequenceNo = args.rowData.sequence;
            this.setState({ sectionView: true, subSection: args.rowData.qualitySubSectionDTOs, subSectionForSave: args.rowData.qualitySubSectionDTOs, sectionId: args.rowData.id, sectionName: args.rowData.sectionName, sectionDesc: args.rowData.sectionDesc, sectionCategoryIds: sectionCategory, sectionType: sectionType })
            this.viewTabs(this.state.sectionType);
            this.addNewSectionDialog(args.commandColumn.type);
        }
        else {
            if (args.commandColumn.type === "Delete") {
                this.deleteSection(this.state.compId, args.rowData.id);
            }
        }
    }


    sideBar = (event) => {
        document.body.classList.toggle('overflow-hidden-body');
        // if (event.target.id == "cancel")
        // this.toastObj.show(this.state.toasterMessage[0]);
    }

    ///////////////////////////////////////////////////////// Templates ///////////////////////////////////////
    prefilledTemplate = (args) => {
        return (<div> <CheckBoxComponent id="isPrefilled" name='prefilled' onClick={this.onClickPrefilledButton} checked={this.state.isPrefilled}></CheckBoxComponent></div>);
    }
    fieldCategoryTemplate = () => {
        let that = this;
        return (<div>  <DropDownListComponent id='category' placeholder="Select a Category" change={this.onChangeQuesCategory} dataSource={that.state.quesCategory} fields={{ text: 'quesCategory', value: 'id' }} popupHeight="220px" /></div>);
    }

    quesCategoryTemplate = () => {
        let that = this;
        return (<div>  <DropDownListComponent id='category' placeholder="Select a Category" change={this.onChangeQuesCategory} dataSource={that.state.quesCategory} fields={{ text: 'quesCategory', value: 'id' }} popupHeight="220px" /></div>);
    }
    selectRoleTemplate = (args) => {
        let that = this;
        return (<div>  <DropDownListComponent id='text' allowFiltering={true} value={args.name} placeholder="Select a role" dataSource={that.state.roles} change={this.onChangeRole} fields={{ text: 'name', value: 'name' }} popupHeight="220px" /></div>);
    }

    selectRoleTemplateForQues = (args) => {
        let that = this;
        return (<div>  <DropDownListComponent id='role' allowFiltering={true} value={args.name} placeholder="Select a role" dataSource={that.state.roles} change={this.onChangeRole} fields={{ text: 'name', value: 'name' }} popupHeight="220px" /></div>);
    }

    selectRoleTemplateForField = (args) => {
        let that = this;
        return (<div>  <DropDownListComponent id='role' allowFiltering={true} value={args.name} placeholder="Select a role" dataSource={that.state.roles} change={this.onChangeRole} fields={{ text: 'name', value: 'name' }} popupHeight="220px" /></div>);
    }

    ///////////////////////////////////Side Panel Questions/////////////////////////////////////////

    viewActions = () => {
        return (<div><ButtonComponent className='action float-left mr-2 btn-sm' size="sm" cssClass='e-flat' color='primary' onClick={this.sideBarPanel}>View Action</ButtonComponent></div>);
    }
    sideBarPanel() {
        this.setState({ isSidebar: true });
    }
    closeSidePanel() {
        this.setState({ isSidebar: false });
    }

    fieldTemplate = (args) => {
        let that = this;
        return (<div>  <DropDownListComponent id='mappingFieldName' allowFiltering={true} value={args.mappingFieldName} placeholder="Select a Mapping Field" dataSource={that.state.field} fields={{ text: 'quesText', value: 'quesText' }} popupHeight="220px" /></div>);
    }

    quesTemplate = () => {
        let that = this;
        return (<div>  <DropDownListComponent id='mappingField' placeholder="Select a Question" disabled={this.state.isPrefilled} dataSource={that.state.ques} fields={{ text: 'quesText', value: 'id' }} popupHeight="220px" /></div>);
    }

    inputTemplate = () => {
        return (<div>  <TextBoxComponent id='text' placeholder="Field Name" floatLabelType="Auto" /></div>);
    }

    ///////////////////////////////////////////////////////// on change handler ///////////////////////////////////////

    onChangeInsName = (event) => {
        let that = this;
        this.setState({ insName: event.value });
    }

    onChangeCode = (event) => {
        let that = this;
        that.setState({ insCode: event.value });
    }

    onChangeDate = (event) => {
        let that = this;
        that.setState({ insDate: event.value });
    }

    onChangeRevision = (event) => {
        let that = this;
        that.setState({ insRevision: event.value });

    }
    onChangeDesc = (event) => {
        let that = this;
        that.setState({ insDesc: event.value });

    }
    onChangeRule = (event) => {
        let that = this;
        that.setState({ insRuleId: event.value });
    }

    onChangeHeadType = (event) => {
        let that = this;
        if (event.value != null) {
            that.dclDropDownListObject.value = null;
            that.aqlDropDownListObject.value = null;
            that.setState({ compHeadTypeId: event.value, aqlorDclId: null, compHeadTypeName: event.itemData.headerTypeName });
        }
    }

    onChangeCompName = (event) => {
        let that = this;
        that.setState({ compName: event.value });
    }

    onChangeCompDesc = (event) => {
        let that = this;
        that.setState({ compDesc: event.value });
    }

    onChangeSectionName = (event) => {
        let that = this;
        that.setState({ sectionName: event.value });

    }
    onChangeSectionDesc = (event) => {
        let that = this;
        that.setState({ sectionDesc: event.value });

    }

    onChangeInspectionFilterMultiSelect = (event) => {
        this.setState({ inspCategoryIds: event.value, selectedInspCategory: event.itemData.categoryName })
    }

    onChangeProductionProcess = (event) => {
        this.setState({ selectedProdProcess: event.itemData.processName })
    }

    onChangeSectionCategoryFilterMultiSelect = (event) => {
        let that = this;
        if (event.value != undefined && event.value.length > 0)
            that.getSections(event.value);
        else {
            that.sectionNameMultiSelectObj.dataSource = [];
            that.sectionNameMultiSelectObj.value = null;
            that.setState({ sections: [] });
        }
        that.setState({ sectionCategoryIds: event.value })
    }

    onChangeHeaderFilterMultiSelect = (event) => {
        this.setState({ headerCategoryIds: event.value })

    }

    onChangeSelectedSecCategoryFilterMultiSelect = (event) => {
        this.setState({ sectionCategoryIds: event.value })
    }

    onChangeExistHeaderFilterMultiSelect = (event) => {
        this.setState({ existingHeaderCategoryIds: event.value })
        this.getComponents(event.value);

    }


    onChangeExistingHeader = (event) => {
        let that = this;
        if (event.e != null && event.e != undefined) {
            let selectedCompSecObj = that.state.components.filter(function (item) {
                return item.id == event.value;
            });
            // let headerIds=(selectedCompSecObj[0].qualityHeaderTypeMasterDTO.id);

            let headCategoryIds = [];

            selectedCompSecObj[0].qualityCategoryMasterDTOList.map(element => {
                headCategoryIds.push(element.id);
            });

            that.setState({ compId: event.value, compName: selectedCompSecObj[0].componentName, compDesc: selectedCompSecObj[0].componentDesc, headerCategoryIds: headCategoryIds });
        }
    }

    onChangeExistingSection = (event) => {
        let that = this;
        that.setState({ sectionId: event.value });

    }

    onChangeAql = (event) => {
        let that = this;
        // that.dclDropDownListObject.value = null;
        that.setState({ aqlorDclId: event.value });
    }

    onChangeDcl = (event) => {
        let that = this;
        // that.aqlDropDownListObject.value = null;
        that.setState({ aqlorDclId: event.value });

    }

    onChangeQuesCategory = (event) => {
        this.setState({ selectedQuesCategory: event.value })
        this.getQuesByCategory(event.value)
    }


    onChangeRole = (event) => {
        let that = this;
        // that.setState({ roleName: event.value });
        that.state.roleName = event.value;
    }

    onClickMaterialButton = (args) => {
        this.setState({ materialValue: args.value })
    }
    onClickUnitButton = (args) => {
        this.setState({ uomValue: args.value })



    }
    onClickCheckingButton = (args) => {
        this.setState({ checkingValue: args.value })

    }
    onClickFPointButton = (args) => {
        this.setState({ fpointValue: args.value })

    }

    onSaveInspectionTypeInfo = () => {
        let inspInfo = [];
        inspInfo.push(new InspectionTypeInfoDTO(this.state.materialValueId, "MATERIAL", this.state.materialValue));
        inspInfo.push(new InspectionTypeInfoDTO(this.state.uomValueId, "UOM", this.state.uomValue));
        inspInfo.push(new InspectionTypeInfoDTO(this.state.checkingValueId, "Checking", this.state.checkingValue));
        inspInfo.push(new InspectionTypeInfoDTO(this.state.fpointValueId, "FPOINT", this.state.fpointValue));
        this.setState({ inspTypeInfo: inspInfo })
    }

    onSaveInspection = () => {
        let that = this;
        const { insId, insName, insCode, insDate, insRevision, insDesc, insRuleId, inspCategoryIds, isAutomatic, inspTypeInfo } = that.state;
        let categories = null;
        if (that.validateFields()) {
            // inspCategoryIds.map(item => {
            categories = new CategoryModel(inspCategoryIds, null)
            // })
            let compSeq = []; let count = 1;
            that.treeViewGridObj.treeData.map(comp => {
                if (comp.hasChild != undefined) {
                    compSeq.push(new ComponentSequenceModel(comp.id, count))
                    count = count + 1;
                }
            })
            // that.onSaveInspectionTypeInfo();
            let inspInfo = [];
            if (this.state.selectedInspCategory == 'Store') {
                inspInfo.push(new InspectionTypeInfoDTO(this.state.materialValueId, "MATERIAL", this.state.materialValue));
                inspInfo.push(new InspectionTypeInfoDTO(this.state.uomValueId, "UOM", this.state.uomValue));
                inspInfo.push(new InspectionTypeInfoDTO(this.state.checkingValueId, "Checking", this.state.checkingValue));
                inspInfo.push(new InspectionTypeInfoDTO(this.state.fpointValueId, "FPOINT", this.state.fpointValue));
            }
            else if (this.state.selectedInspCategory == 'Production') {
                inspInfo.push(new InspectionTypeInfoDTO(this.state.processValueId, "Process", this.state.selectedProdProcess));
            }
            let newInspection = new InpectionTypeModel(insId, insCode, insDate, insDesc, true, insName, insRevision, insRuleId, compSeq, categories, isAutomatic, inspInfo)
            this.saveInspection(newInspection);
        }
        // else {
        //     this.toastObj.show(this.createToaster("Please enter inspection details", "Information"));
        // }
    }

    dateConverter = (date) => {
        if (date != undefined && date != null) {
            return new Date(date.year, date.month - 1, date.day, 23, 59)
        }
    }

    onSaveHeader = () => {
        let that = this;
        const { insId, insName, insCode, insDate, insRevision, insDesc, insRuleId, compId, compName, compDesc, compHeadTypeId, inspCategoryIds, headerCategoryIds, aqlorDclId, lineLoadView, selectedInspectionInfo, isAutomatic, inspTypeInfo } = that.state;
        let categories = []; let inspCategories = null;
        let sequence = selectedInspectionInfo == null ? 1 : selectedInspectionInfo.qualityComponentMasterDTOS.length + 1;

        if (that.validateFields()) {
            if (lineLoadView) {
                if (compName != null && compName.trim() != "" && compDesc != null && compDesc.trim() != "" && compHeadTypeId != null && headerCategoryIds != null && headerCategoryIds.length != 0) {
                    headerCategoryIds.map(item => {
                        categories.push(new CategoryModel(item, null))
                    })
                    
                    inspCategories = new CategoryModel(inspCategoryIds, null);
        
                    let inspInfo = [];
                    if (this.state.selectedInspCategory == 'Store') {
                        inspInfo.push(new InspectionTypeInfoDTO(this.state.materialValueId, "MATERIAL", this.state.materialValue));
                        inspInfo.push(new InspectionTypeInfoDTO(this.state.uomValueId, "UOM", this.state.uomValue));
                        inspInfo.push(new InspectionTypeInfoDTO(this.state.checkingValueId, "Checking", this.state.checkingValue));
                        inspInfo.push(new InspectionTypeInfoDTO(this.state.fpointValueId, "FPOINT", this.state.fpointValue));
                    }
                    else if (this.state.selectedInspCategory == 'Production') {
                        inspInfo.push(new InspectionTypeInfoDTO(this.state.processValueId, "Process", this.state.selectedProdProcess));
                    }
                    let headTypes = (new HeadTypeModel(compHeadTypeId, null));
                    let aqlOrDclInfo = (new QualityInspAqlFpointDclModel(null, null, null, null, aqlorDclId));
                    let inspectionInfo = new InpectionTypeModel(insId, insCode, insDate, insDesc, true, insName, insRevision, insRuleId, [], inspCategories, isAutomatic, inspInfo)
                    let compInfo = new ComponentModel(compId, categories, compDesc, compName, true, null, sequence, headTypes, inspectionInfo, null, aqlOrDclInfo);
                    this.saveHeader(compInfo);
                }
                else {
                    this.toastObj.show(this.createToaster("Please enter header details", "Information"));
                }
            }
            else if(!lineLoadView){
                
                if(headerCategoryIds.length == 0){
                    this.toastObj.show(this.createToaster("Please enter header details", "Information"));
                   
                }else{
                   
                    if (compName != null && compName != "" && compId != null) {
                       
                        inspCategories = new CategoryModel(inspCategoryIds, null);
                        let inspectionInfo = new InpectionTypeModel(insId, insCode, insDate, insDesc, true, insName, insRevision, insRuleId, [], inspCategories,isAutomatic, inspTypeInfo)
                        let compInfo = new ComponentModel(compId, null, null, null, true, null, sequence, null, inspectionInfo, null, null);
                        this.saveExistingHeader(compInfo);
                    }
                }
            }

            // else {
                

            // }
        }
        else {
            this.toastObj.show(this.createToaster("Please enter inspection details", "Information"));
        }
    }

    onSaveSection = () => {
        let that = this;
        const { sectionId, sectionName, tabContentSection, sectionSequenceNo, sectionDesc, sectionCategoryIds, compId, isPrefilled, subSection, subSectionModel, categoryIds, subSectionForSave, sectionView } = that.state;
        let categories = [];
        if (sectionView) {
            if ((subSection != null && subSection != undefined && subSection.length > 0) && sectionName != null && sectionName.trim() != "" && sectionDesc != null && sectionDesc.trim() != "" && sectionCategoryIds != undefined && sectionCategoryIds.length != 0) {
                if (compId != null && compId != "") {
                    //add old sections 
                    // if (subSectionForSave != null && subSectionForSave.length > 0) {
                    //     subSectionForSave.map(item => {
                    //         if (item.pid != undefined)
                    //             subSection.push(item);
                    //     })
                    // }
                    sectionCategoryIds.map(item => {
                        categories.push(new CategoryModel(item, null))
                    })
                    let sectionInfo = new SectionModel(sectionId, categories, compId, sectionDesc, true, sectionName, sectionSequenceNo, subSection);
                    this.saveSection(sectionInfo);
                }
                else {
                    this.toastObj.show(this.createToaster("Please enter section details", "Information"));
                }
            }
            else {
                this.toastObj.show(this.createToaster("Please enter section details", "Information"));
            }
        }
        else {
            if (compId != null && sectionId != null) {
                let existinSectionInfo = new SectionModel(sectionId, null, compId, null, true, null, sectionSequenceNo, []);
                this.saveExistinSection(existinSectionInfo);
            }
            else {
                this.toastObj.show(this.createToaster("Please enter section details", "Information"));
            }
        }
    }

    validateFields() {
        let that = this;
        const { insId, insName, insCode, insDate, insRevision, insDesc, insRuleId, inspCategoryIds, isAutomatic, materialValue, uomValue, checkingValue, fpointValue, selectedProdProcess } = that.state;
        if ((insName != null && insName != undefined && insName.trim() == "") || insName == null || insName == undefined || insCode == null || insCode == "" || insDate == null || insDate == "" || insRevision == null || insRevision == "" || insDesc == null || insDesc == "" || insRuleId == null || insRuleId == "" || inspCategoryIds == null || isAutomatic == null) {
            this.toastObj.show(this.createToaster("Please enter inspection details !!", "Information"));
            return false;
        } if (inspCategoryIds == 1) {
            if (materialValue == null || uomValue == null || checkingValue == null || fpointValue == null) {
                this.toastObj.show(this.createToaster("Please Select Store Information !! ", "Information"));
                return false;
            }
        } else if (inspCategoryIds == 2) {
            if (selectedProdProcess == null) {
                this.toastObj.show(this.createToaster("Please Select Production Information !! ", "Information"));
                return false;
            }
        }

        return true;

    }

    onCompClickGetSections = (compId) => {
        let that = this;
        that.setState({ compId: that.treeViewGridObj.state != null ? that.treeViewGridObj.state.selectedCompId : compId });
        let selectedCompSecObj = that.state.selectedInspectionInfo.qualityComponentMasterDTOS.filter(function (item) {
            if(that.state.selectedCompId){
                return item.id == that.state.selectedCompId;
            }else{
                return item.id == that.state.compId;
            }
        });
        let subSectionInfos = [];
        this.state.sectionSequenceNo = 1;
        if (selectedCompSecObj.length > 0) {
            let sectionInfos = [];
            selectedCompSecObj[0].qualitySectionMasterDTOList.map(sectionInfo => {
                let subSectionInfo = [];
                sectionInfo.qualitySubSectionDTOs.map(subSection => {
                    subSectionInfo.push(new SubSectionModel(subSection.id, subSection.isPrefilled, subSection.referenceId, subSection.sequence, subSection.text, subSection.type, sectionInfo.id, subSection.mappingField, subSection.mappingFieldName, subSection.subSectionQuesAnsGroupDTO,subSection.isMandate,subSection.isEditable,subSection.role));
                    subSectionInfos.push(new SubSectionModel(subSection.id, subSection.isPrefilled, subSection.referenceId, subSection.sequence, subSection.text, subSection.type, sectionInfo.id, subSection.mappingField, subSection.mappingFieldName, subSection.subSectionQuesAnsGroupDTO,subSection.isMandate,subSection.isEditable,subSection.role));

                })
                let categoryNames = sectionInfo.qualityCategoryMasterDTOList.map(item => item.categoryName).join(', ');

                sectionInfos.push(new SectionModel(sectionInfo.id, sectionInfo.qualityCategoryMasterDTOList, sectionInfo.componentId, sectionInfo.sectionDesc, true, sectionInfo.sectionName, sectionInfo.sequence, subSectionInfo, sectionInfo.id, categoryNames))
            })
            that.sectiongridInstance.childGrid.dataSource = subSectionInfos;
            that.setState({ sectionSequenceNo: selectedCompSecObj[0].qualitySectionMasterDTOList.length + 1, sectionData: sectionInfos, subSectionData: subSectionInfos, selectedComp: selectedCompSecObj, compHeadTypeName: selectedCompSecObj[0].qualityHeaderTypeMasterDTO.headerTypeName })
        }

    }

    onClickPrefilledButton = (args) => {
        this.setState({ isPrefilled: args.target.checked })

    }

    onClickModeButton = (args) => {
        let flag = true;
        if (args.value == 'automatic') flag = true;
        else if (args.value == 'manual') flag = false;
        this.setState({ isAutomatic: flag })

    }

    rendereComplete() {
        this.onChange();
    }

    itemTemplate(data) {
        return (<div className='inspectionListdrop'>
            <div className="name"> {data.Name}<sup>{data.totalinspection}</sup>  <span>{data.Type}</span></div>
            <div className="designation"> {data.Designation} </div>
        </div>);
    }
    //set the value to value template
    valueTemplate(data) {
        return (<div className='inspectionListdrop'>
            <div className="name"> {data.Name}<sup>{data.totalinspection}</sup>  <span>{data.Type}</span></div>
            <div className="designation"> {data.Designation} </div>
        </div>);
    }
    nodeClicked = (data) => {
        let that = this;
        that.setState({ selectedCompId: data.node.dataset.uid })
        that.onCompClickGetSections(data.node.dataset.uid)

    }
    nodeTemplate(data) {
        return (<div>
            <div className="treeviewdiv">
                <div className="textcontent">
                    <div sm='2' className="treeName">{data.name}</div>
                </div>

                <div className="countcontainer" hidden={data.count == 0 ? true : false}>
                    <div className="treeCount e-badge e-badge-primary">{data.count}</div>
                </div>
            </div>
        </div>);
    }

    content1() {
        return (
            <Col>
                <Row className='content-spacing'><CheckBoxComponent label='Request Picture' /></Row>
                <Row className='content-spacing'><CheckBoxComponent label='Request Description' /></Row>
            </Col>);
    }
    content2() {
        return (
            <Col>
                <Row className='content-spacing'><CheckBoxComponent label='Request Picture' /></Row>
                <Row className='content-spacing'><CheckBoxComponent label='Request Description' /></Row>
            </Col>);
    }
    content3() {
        return (
            <Col>
                <Row className='content-spacing'><CheckBoxComponent label='Request Picture' /></Row>
                <Row className='content-spacing'><CheckBoxComponent label='Request Description' /></Row>
            </Col>);
    }
    addNewSectionDialog = (args) => {
        if (args == 'new') {
            this.cleanSectionGrid();
            this.setState({ sectionView: true, tabContentSection: null, isEditSection: false, sectionDialogBox: true, sectionType: null, subSection: null, subSectionForSave: null });
        }
        else if (args == 'Edit') {
            this.setState({ isEditSection: true, sectionDialogBox: true, });
        }
    }
    addQuestionAction = () => {
        this.setState({ sectionDialogBox: true, });
    }

    clickHeader = () => { this.setState({ editHeaderDialog: true }); }

    backToInspectionList = () => {
        this.props.history.push("/inspectionList");
    }

    render() {
        return (
            <div className='control-pane' id='targetElement'>
                <div className="container-fluid-inner inspection-section">
                    <div className="add-line-section" hidden={!this.state.isSidebar}>
                        <SplitterLayout secondaryInitialSize={300} secondaryMinSize={300}>
                            <div className="blank-space" onClick={this.closeSidePanel}></div>
                            <div className="overflow-hidden add-line-section-form">
                                <div className="heading mb-2">View Actions <a className="close" onClick={this.closeSidePanel}>+</a></div>
                                <Col>
                                    <Row>
                                        <AccordionComponent>
                                            <AccordionItemsDirective>
                                                <AccordionItemDirective className='accordion-header' header='Yes' iconCss='e-people e-acrdn-icons' content={this.content1} />
                                                <AccordionItemDirective className='accordion-header' header='No' iconCss='e-people e-acrdn-icons' content={this.content2} />
                                                <AccordionItemDirective className='accordion-header' header='N/A' iconCss='e-people e-acrdn-icons' content={this.content3} />
                                            </AccordionItemsDirective>
                                        </AccordionComponent>
                                    </Row>
                                    <Row>
                                        <AccordionComponent>
                                            <AccordionItemsDirective>
                                                <AccordionItemDirective className='accordion-header' header='Pass' iconCss='e-people e-acrdn-icons' content={this.content1} />
                                                <AccordionItemDirective className='accordion-header' header='Fail' iconCss='e-people e-acrdn-icons' content={this.content2} />
                                            </AccordionItemsDirective>
                                        </AccordionComponent>
                                    </Row>

                                    <div className='mt-3'></div>
                                    <Button color="primary" className="mr-2" size="sm" /* onClick={this.onSaveInspection} */>Save</Button>
                                    <Button size="sm" >Cancel</Button>
                                </Col>
                            </div>
                        </SplitterLayout>
                    </div>
                    <div className="add-line-section-overlay" hidden={!this.state.isSidebar}></div>

                    <h1 className='pagesheading'>Inspection</h1>
                    <div className='clearfix'></div>
                    <Row>
                        <Col sm='6'>
                            <Link to="/inspectionList" className='back-btn'></Link>
                            <div className="title-container">
                                <div className="title">{this.state.insName}</div>
                                <div className="workflow-name">{this.state.insDesc}</div>
                            </div>
                        </Col>
                        <Col sm='6' className='text-right'>
                        </Col>
                    </Row>
                    {/* <hr /> */}
                    {/* <InspectionListComponent/> */}


                    {/* ---------------------------------------------------------Inspection Form Begins----------------------------------------------- */}
                    <div className='form-section'>
                        <Col>
                            <Row className='form_main_heading'>Inspection Details</Row>
                            <Row className='form_section_fields'>
                                <Col sm='3'><TextBoxComponent placeholder="Name" floatLabelType="Auto" onChange={this.onChangeInsName} value={this.state.insName} /></Col>
                                <Col sm='9'><TextBoxComponent placeholder="Description" floatLabelType="Auto" onChange={this.onChangeDesc} value={this.state.insDesc} /></Col>
                            </Row>
                            <div className='toggleContainer'><div> </div><div className='showMoreToggle' onClick={this.showMoreToggle}>{this.state.showMore ? 'Show less' : 'Show more'} {this.state.showMore ? <i class="fa fa-chevron-up"></i> : <i class="fa fa-chevron-down"></i>}</div></div>

                            <div hidden={!this.state.showMore ? true : false}>
                                <Row className='form_heading' >Version</Row>
                                <Row className='form_section_fields'>
                                    <Col><TextBoxComponent placeholder="Code" floatLabelType="Auto" onChange={this.onChangeCode} value={this.state.insCode} /></Col>
                                    <Col><TextBoxComponent placeholder="Revision" floatLabelType="Auto" onChange={this.onChangeRevision} value={this.state.insRevision} /></Col>
                                    <Col><DatePickerComponent placeholder="Date" min={this.state.minDate} allowEdit={false} floatLabelType="Auto" format='dd/MM/yy' onChange={this.onChangeDate} value={this.state.insDate} ></DatePickerComponent></Col>
                                </Row>

                                <Row className='form_heading'>Mode</Row>
                                <Row className='form_section_fields'>
                                    <div className='radioGroup'>
                                        <div><RadioButtonComponent label='Manual' checked={!this.state.isAutomatic} value='manual' onChange={this.onClickModeButton} name='manual' id="manual"></RadioButtonComponent></div>
                                        <div><RadioButtonComponent label='Automatic' checked={this.state.isAutomatic} value='automatic' onChange={this.onClickModeButton} name='automatic' id="automatic"></RadioButtonComponent></div>
                                    </div>
                                    <Col sm='3' className='selectCategory' >
                                        <div id='filtering'>
                                            <DropDownListComponent id="Category" ref={(scope) => { this.mulObj = scope; }} dataSource={this.state.inspCategories} fields={{ text: 'categoryName', value: 'id' }} placeholder="Select Category" floatLabelType="Auto"
                                                change={this.onChangeInspectionFilterMultiSelect} showDropDownIcon={true} filterBarPlaceholder="Search Category" popupHeight="350px" value={this.state.inspCategoryIds} />
                                        </div>
                                    </Col>
                                    <Col sm='3'><DropDownListComponent id="insprule" dataSource={this.state.rules} ref={(combobox) => { this.listObj = combobox; }} filterBarPlaceholder='Search a Rule' placeholder="Select a Rule" floatLabelType="Auto" popupHeight="200px" value={this.state.insRuleId} popupHeight="220px" change={this.onChangeRule} /></Col>
                                </Row>

                                {/* ---------------------------------------------------------Store Info------------------------------------------------ */}
                                <div hidden={this.state.selectedInspCategory == 'Store' ? false : true}>
                                    <Row className='form_category'>Store Information</Row>
                                    <Row className='form_section_fields'>
                                        <Col sm='2' className='store_info_fields'>Select material</Col>
                                        <div className='radioGroup'>
                                            <div><RadioButtonComponent label='Fabric' value='Fabric' onChange={this.onClickMaterialButton} checked={this.state.materialValue == 'Fabric' ? true : false} name='2'></RadioButtonComponent></div>
                                            <div><RadioButtonComponent label='Accessory' value='Accessory' onChange={this.onClickMaterialButton} checked={this.state.materialValue == 'Accessory' ? true : false} name='2'></RadioButtonComponent></div>
                                        </div>
                                    </Row>
                                    <Row className='form_section_fields'>
                                        <Col sm='2' className='store_info_fields'>Select Unit</Col>
                                        <div className='radioGroup'>
                                            <div><RadioButtonComponent label='Pcs' value='Pcs' onChange={this.onClickUnitButton} checked={this.state.uomValue == 'Pcs' ? true : false} name='3'></RadioButtonComponent></div>
                                            <div><RadioButtonComponent label='Others' value='Others' onChange={this.onClickUnitButton} checked={this.state.uomValue == 'Others' ? true : false} name='3'></RadioButtonComponent></div>
                                        </div>
                                    </Row>
                                    <Row className='form_section_fields'>
                                        <Col sm='2' className='store_info_fields'>100% checking?</Col>
                                        <div className='radioGroup'>
                                            <div><RadioButtonComponent label='Yes' value='Yes' onChange={this.onClickCheckingButton} checked={this.state.checkingValue == 'Yes' ? true : false} name='4'></RadioButtonComponent></div>
                                            <div><RadioButtonComponent label='No' value='No' onChange={this.onClickCheckingButton} checked={this.state.checkingValue == 'No' ? true : false} name='4'></RadioButtonComponent></div>
                                        </div>
                                    </Row>
                                    <Row className='form_section_fields'>
                                        <Col sm='2' className='store_info_fields'>4 Point?</Col>
                                        <div className='radioGroup'>
                                            <div><RadioButtonComponent label='Yes' value='Yes' onChange={this.onClickFPointButton} checked={this.state.fpointValue == 'Yes' ? true : false} name='5'></RadioButtonComponent></div>
                                            <div><RadioButtonComponent label='No' value='No' onChange={this.onClickFPointButton} checked={this.state.fpointValue == 'No' ? true : false} name='5'></RadioButtonComponent></div>
                                        </div>
                                    </Row>
                                </div>
                                {/* ---------------------------------------------------------Store Info Ends------------------------------------------------ */}

                                {/* ---------------------------------------------------------Production Info------------------------------------------------ */}
                                <div hidden={this.state.selectedInspCategory == 'Production' ? false : true}>
                                    <Row className='form_category'>Production Information</Row>
                                    <Row className='form_section_fields'>
                                        <Col sm='2' className='store_info_fields'>Select Production Process</Col>
                                        <Col sm='3'>
                                            <DropDownListComponent placeholder="Select a producion process" popupHeight="220px" value={this.state.selectedProdProcess} placeholder="Select a Production Process" dataSource={this.state.productionProcessList} change={this.onChangeProductionProcess} fields={{ text: 'processName', value: 'processName' }} popupHeight="220px" />
                                        </Col>
                                    </Row>
                                </div>
                                {/* ---------------------------------------------------------Production Info Ends------------------------------------------------ */}

                            </div>
                        </Col>
                    </div>

                    {/* ---------------------------------------------------------Inspection Form Ends------------------------------------------------ */}
                    <div className='inspection_grid'>
                        <Row>
                            <Col sm='2'>
                                <div className='inspection-tree' height={this.state.gridHeight} >
                                    <div className='text-center pt-2'>
                                        <Button color="primary" size="sm" onClick={this.linkClicked.bind(this, 'new')}><i class="fa fa-plus"></i> {'Add Header Information'}</Button></div>
                                    <TreeViewComponent ref={grid => this.treeViewGridObj = grid} nodeClicked={this.nodeClicked} fields={this.treefields} nodeTemplate={this.nodeTemplate} allowDragAndDrop={true} allowTextWrap={true}/>
                                </div>

                            </Col>
                            <Col sm='10' className='grid_container'>
                                {/* ----------- temp ----------- */}

                                <div className='header-information' hidden={this.state.isNewInsp}>
                                    <Row>
                                        <Col sm='12' xs='12'>
                                            <span className='header-name'>{this.state.selectedComp != null ? this.state.selectedComp[0].componentName : ""}</span> <a className='ml-1 primaryText' onClick={this.linkClicked.bind(this, 'Edit')}> <i class="fa fa-edit"></i></a> <a className='ml-1 primaryText' onClick={this.linkClicked.bind(this, 'Delete')}> <i class="fa fa-trash"></i></a>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col sm='3' xs='12'>
                                            <label>Header Type</label>
                                            {this.state.selectedComp != null && this.state.selectedComp[0].qualityHeaderTypeMasterDTO != null ? this.state.selectedComp[0].qualityHeaderTypeMasterDTO.headerTypeName : ""}
                                        </Col>
                                        {/* <Col sm='3' xs='12'>
                                        <label>Category</label>
                                        {this.state.selectedComp!=null?this.state.selectedComp[0].inspectionCategoryMasterDTO.name:""}
                                    </Col> */}
                                        <Col sm='6' xs='12'>
                                            <label>Description</label>
                                            {this.state.selectedComp != null ? this.state.selectedComp[0].componentDesc : ""}
                                        </Col>
                                    </Row>
                                </div>
                                <div hidden={this.state.isNewInsp || this.state.compHeadTypeName != 'General Information'}>
                                    <Button color="primary" size="sm" onClick={(e) => this.addNewSectionDialog('new')}><i class="fa fa-plus"></i> Add Sections </Button>
                                    <div className='mt-1'></div>

                                    <GridComponent
                                        enableHover={false}
                                        allowFiltering={false}
                                        allowSorting={true}
                                        allowSelection={true}
                                        allowResizing={true}
                                        rowHeight={25}
                                        gridLines='Both'
                                        allowPaging={true}
                                        allowTextWrap={true}
                                        allowRowDragAndDrop={true}
                                        editSettings={this.headerEditSettings}
                                        ref={(g) => { this.sectiongridInstance = g; }}
                                        toolbar={['Search']}
                                        pageSettings={AppConfig.pageSettings}
                                        dataSource={this.state.sectionData}
                                        childGrid={this.childGrid}
                                        rowSelected={this.rowSelectedSectionGrid}
                                        height={this.state.gridHeight}
                                        selectionSettings={{ type: 'Multiple' }}
                                        commandClick={this.cmdForGeneralInfo.bind(this)}>
                                        <ColumnsDirective>
                                            <ColumnDirective field='sectionName' headerText='Name' width='120' />
                                            <ColumnDirective field='category' headerText='Category' width='120' />
                                            {/* <ColumnDirective field='type' headerText='Type' width='100' /> */}
                                            <ColumnDirective field='sectionDesc' headerText='Description' width='250' />
                                            <ColumnDirective headerText='Action' allowSorting={false} allowEditing={false} textAlign='Left' width='75' commands={this.sectionCommands} />
                                        </ColumnsDirective>
                                        <Inject services={[RowDD, DetailRow, CommandColumn, Edit, Filter, Page, Sort, Selection]} />
                                    </GridComponent>
                                    <div className='mt-1'></div>
                                </div>
                                <div className='button_group'>
                                    <Button color="primary" className="mr-2" size="sm" onClick={this.onSaveInspection} >Save</Button>
                                    <Button size="sm" onClick={this.backToInspectionList}>Cancel</Button>
                                </div>
                            </Col>
                        </Row>
                        {/* -----------------------Header Information Start-------------------------------------*/}
                        <DialogComponent id="defaultDialog" showCloseIcon={true} animationSettings={this.animationSettings} isModal={true} allowDragging={true} visible={this.state.showDialog} width={'70%'} ref={dialog => this.headerdialogInstance = dialog}
                            header={!this.state.isEditHeader ? 'Add Header Information' : 'Edit Header Information'} buttons={this.buttons} close={this.dialogClose.bind(this)}>
                            <Row>
                                <Col sm='12'>

                                    <div className='filter-Selection' >
                                        <div className="mt-3" hidden={this.state.isEditHeader}>
                                            <Row >
                                                <Col sm='4'>
                                                    <RadioButtonComponent onClick={e => this.lineLoadView(true)} label='Add New Component' checked={this.state.lineLoadView} name='1' value="payment" ></RadioButtonComponent>
                                                </Col>
                                                <Col sm='3'>
                                                    <RadioButtonComponent onClick={e => this.lineLoadView(false)} label='Add Existing Component' checked={!this.state.lineLoadView} name='1' value="payment"></RadioButtonComponent>
                                                </Col>
                                            </Row>
                                        </div>
                                        <Row className='existing_comp' hidden={!this.state.lineLoadView}>
                                            <Col sm='4'>
                                                <TextBoxComponent placeholder="Header" onChange={this.onChangeCompName} value={this.state.compName} floatLabelType="Auto" />
                                            </Col>
                                            <Col sm='4'>
                                                <DropDownListComponent id="HeaderType" filterBarPlaceholder='Select a Head Type' placeholder="Select a Head Type" floatLabelType="Auto" popupHeight="220px" dataSource={this.state.headTypes}
                                                    fields={{ text: 'headerTypeName', value: 'id' }} allowFiltering={true} value={this.state.compHeadTypeId} change={this.onChangeHeadType} />
                                            </Col>
                                            <Col sm='4' className={this.state.lineLoadView}>
                                                {/* <DropDownListComponent id="Category" filterBarPlaceholder='Search a category' placeholder="Select a category" popupHeight="220px" dataSource={this.state.categories} fields={{ text: 'categoryName', value: 'id' }} allowFiltering={true}  /> */}
                                                <MultiSelectComponent id="Category" ref={(scope) => { this.mulObj = scope; }} dataSource={this.state.categories} floatLabelType="Auto" fields={{ text: 'categoryName', value: 'id' }} placeholder="Select  Category" mode="CheckBox"
                                                    change={this.onChangeHeaderFilterMultiSelect} value={this.state.headerCategoryIds} showSelectAll={true} showDropDownIcon={true} filterBarPlaceholder="Search Category" popupHeight="350px">
                                                    <Inject services={[CheckBoxSelection]} />
                                                </MultiSelectComponent>
                                            </Col>
                                        </Row>
                                        <div>
                                            <Row hidden={!this.state.lineLoadView}>
                                                <Col sm='12'> <TextBoxComponent placeholder="Description" onChange={this.onChangeCompDesc} value={this.state.compDesc} floatLabelType="Auto" /></Col>

                                            </Row>

                                            <div hidden={this.state.compHeadTypeName == 'DCL' ? false : true} className='mt-3'>
                                                <b>DCL</b>
                                                <Row>
                                                    {/* <Col sm='4'>
                                            <DropDownListComponent id="dclCategory" floatLabelType="Auto"  placeholder="Select DCL Category" popupHeight="220px" dataSource={'DCL'} allowFiltering={false} />
                                            </Col> */}
                                                    <Col sm='4'>
                                                        <DropDownListComponent id="dcl" floatLabelType="Auto" ref={(scope) => { this.dclDropDownListObject = scope; }} placeholder="Select DCL" popupHeight="220px" value={this.state.aqlOrDclId} change={this.onChangeDcl} dataSource={this.state.defectList} fields={{ text: 'name', value: 'id' }} allowFiltering={true} />
                                                    </Col>
                                                </Row>

                                            </div>

                                            <div hidden={this.state.compHeadTypeName == 'AQL' ? false : true} className='mt-3'>
                                                <b>AQL</b>
                                                <Row>
                                                    <Col sm='4'>
                                                        <DropDownListComponent id="aql" floatLabelType="Auto" placeholder="Select AQL" ref={(scope) => { this.aqlDropDownListObject = scope; }} popupHeight="220px" value={this.state.aqlOrDclId} change={this.onChangeAql} dataSource={this.state.aqlList} fields={{ text: 'name', value: 'id' }} allowFiltering={true} />
                                                    </Col>
                                                    {/* <Col sm='12'> <TextBoxComponent placeholder="Description" onChange={this.onChangeCompDesc} value={this.state.compDesc}floatLabelType="Auto" /></Col> */}
                                                </Row>
                                            </div>

                                            <div className='mt-2' hidden={this.state.lineLoadView}>
                                                <Row>
                                                    <Col sm='4'>
                                                        <MultiSelectComponent id="Category" ref={(scope) => { this.categoryMultiObj = scope; }} dataSource={this.state.categories} fields={{ text: 'categoryName', value: 'id' }} placeholder="Select  Category" mode="CheckBox"
                                                            change={this.onChangeExistHeaderFilterMultiSelect} showSelectAll={true} showDropDownIcon={true} value={this.state.existingHeaderCategoryIds} floatLabelType='Always' popupHeight="350px">
                                                            <Inject services={[CheckBoxSelection]} />
                                                        </MultiSelectComponent>
                                                    </Col>
                                                    <Col sm='4'>
                                                        <DropDownListComponent id="HeaderInfo" ref={(scope) => { this.headerInfoDropObj = scope; }} filterBarPlaceholder="Select a Header Info" placeholder="Select a Header Info" floatLabelType='Always' popupHeight="220px" value={this.state.compName} change={this.onChangeExistingHeader}  dataSource={this.state.components} allowFiltering={true} fields={{ text: 'componentName', value: 'id' }} />

                                                    </Col>
                                                </Row>
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </DialogComponent>

                        {/* -----------------------Header Information End-------------------------------------*/}



                        {/* ----------------------- sectionDialogBox ----------------------- */}
                        <DialogComponent id="sectionDialogBox" showCloseIcon={true} animationSettings={this.animationSettings} visible={this.state.sectionDialogBox} width={'70%'} ref={dialog => this.dialogInstance = dialog} isModal={true} allowDragging={true} header={!this.state.isEditSection ? 'Add Section' : 'Edit Section'} buttons={this.sectionButtons} close={this.dialogClose.bind(this)}>
                            <Row>
                                <Col sm='12'>
                                    <div className="mt-3" hidden={this.state.isEditSection}>
                                        <Row>
                                            <Col sm='4'>
                                                <RadioButtonComponent onClick={e => this.sectionView(true)} checked={this.state.sectionView} label='Add New Section' name='sectionRadio' ></RadioButtonComponent>
                                            </Col>
                                            <Col sm='3'>
                                                <RadioButtonComponent onClick={e => this.sectionView(false)} label='Add Existing Section' name='sectionRadio' ></RadioButtonComponent>
                                            </Col>
                                        </Row>
                                    </div>

                                    <div hidden={!this.state.sectionView}>
                                        <Row>
                                            <Col sm='4'>
                                                <TextBoxComponent placeholder="Section Name" onChange={this.onChangeSectionName} value={this.state.sectionName} floatLabelType="Auto" />
                                            </Col>
                                            <Col sm='3'>
                                                <MultiSelectComponent id="Category" ref={(scope) => { this.mulObj = scope; }} dataSource={this.state.categories} floatLabelType="Auto" fields={{ text: 'categoryName', value: 'id' }} placeholder="Select  Category" mode="CheckBox"
                                                    change={this.onChangeSelectedSecCategoryFilterMultiSelect} value={this.state.sectionCategoryIds} showSelectAll={true} showDropDownIcon={true} filterBarPlaceholder="Search Category" popupHeight="350px">
                                                    <Inject services={[CheckBoxSelection]} />
                                                </MultiSelectComponent>
                                            </Col>
                                            <Col sm='4'>
                                                <TextBoxComponent placeholder="Description" onChange={this.onChangeSectionDesc} value={this.state.sectionDesc} floatLabelType="Auto" />
                                            </Col>
                                        </Row>
                                        {/* ---------------Create Section Options Start--- */}
                                        <div className='mt-2'></div>
                                        <Row id='sectionType'>
                                            <Col sm='2'><strong>Section Type:</strong> &nbsp;&nbsp;</Col>
                                            <Col sm='2' >
                                                <RadioButtonComponent label='Field Form' checked={this.state.sectionType == 'Field_Form' ? true : false} disabled={this.state.sectionType == 'Field_Form' ? false : this.state.sectionType == null ? false : true} onClick={e => this.viewTabs('Field_Form')} name='payment' value="fieldform"></RadioButtonComponent>
                                            </Col>
                                            <Col sm='2' >
                                                <RadioButtonComponent label='Question Form' checked={this.state.sectionType == 'Question_Form' ? true : false} disabled={this.state.sectionType == 'Question_Form' ? false : this.state.sectionType == null ? false : true} onClick={e => this.viewTabs('Question_Form')} name='payment' value="credit/debit"></RadioButtonComponent></Col>

                                            {/* <Col sm='2' >
                <RadioButtonComponent label='DCL Form' onClick={() => this.viewTabs('dclForm')} name='payment' value="credit/debit"></RadioButtonComponent>
            </Col> */}
                                            <Col sm='2'>
                                                <RadioButtonComponent label='Result Form' checked={this.state.sectionType == 'Result_Form' ? true : false} disabled={this.state.sectionType == 'Result_Form' ? false : this.state.sectionType == null ? false : true} onClick={() => this.viewTabs('Result_Form')} name='payment' value="credit/debit"></RadioButtonComponent>
                                            </Col>
                                        </Row>
                                        <div className='mt-2'></div>
                                        {/* ---------------Create Section Options Ends--- */}

                                    </div>

                                    <div className='mt-2' hidden={this.state.sectionView}>
                                        <Row>
                                            <Col sm='4'>
                                                <MultiSelectComponent id="Category" ref={(scope) => { this.mulObj = scope; }} floatLabelType='Auto' dataSource={this.state.categories} fields={{ text: 'categoryName', value: 'id' }} placeholder="Select Category" mode="CheckBox"
                                                    change={this.onChangeSectionCategoryFilterMultiSelect} showSelectAll={true} showDropDownIcon={true} filterBarPlaceholder="Search Category" popupHeight="350px" value={this.state.sectionCategoryIds}>
                                                    <Inject services={[CheckBoxSelection]} />
                                                </MultiSelectComponent>                                        </Col>
                                            <Col sm='4'>
                                                <DropDownListComponent id="sectionName" ref={grid => this.sectionNameMultiSelectObj = grid} floatLabelType='Auto' filterBarPlaceholder='Search Section Name' placeholder="Select Section Name" popupHeight="220px" change={this.onChangeExistingSection} dataSource={this.state.sections} allowFiltering={true} fields={{ text: 'sectionName', value: 'id' }} />
                                            </Col>

                                        </Row>
                                    </div>
                                    <div hidden={!this.state.sectionView}>
                                        {
                                            this.state.tabContentSection == 'Field_Form' ?
                                                <div>
                                                    {/* <CriteriaformFieldComponent /> */}
                                                    <GridComponent rowDrop={this.onDragRow} allowTextWrap='true' toolbar={this.toolbarOptions} ref={grid => this.fieldFormGrid = grid} allowPaging={true} dataSource={this.state.subSection} editSettings={this.editSettings} pageSettings={this.pageSettings} allowRowDragAndDrop={true} actionBegin={this.actionBeginFieldForm}>
                                                        <ColumnsDirective>
                                                            <ColumnDirective field='text' headerText='Field Name' editTemplate={this.inputTemplate}/>
                                                            <ColumnDirective field='isPrefilled' headerText='Prefilled' displayAsCheckBox="true" textAlign="Center" editType="booleanedit" type="boolean" />
                                                            <ColumnDirective field='mappingFieldName' headerText='Mapping Field' editType="dropdownedit" editTemplate={this.fieldTemplate} disabled={true}/>
                                                            <ColumnDirective field='isEditable' headerText='Editable' displayAsCheckBox="true" textAlign="Center" editType="booleanedit" type="boolean" />
                                                            <ColumnDirective field='isMandate' headerText='Mandate' displayAsCheckBox="true" textAlign="Center" editType="booleanedit" type="boolean" />
                                                            <ColumnDirective field='role' headerText='Select Role' editType="dropdownedit" editTemplate={this.selectRoleTemplateForField} />
                                                        </ColumnsDirective>
                                                        <Inject services={[Page, Toolbar, Edit, RowDD, Selection, Search]} />
                                                    </GridComponent>

                                                </div> : this.state.tabContentSection == 'Question_Form' ?
                                                    <div>
                                                        {/* <QuestionformComponent /> */}
                                                        <GridComponent rowDrop={this.onDragRow} toolbar={this.toolbarOptions} allowPaging={true} dataSource={this.state.subSection} ref={grid => this.questionFormGrid = grid}
                                                            editSettings={this.editSettings} pageSettings={this.pageSettings} allowRowDragAndDrop={true} actionBegin={this.actionBeginQuestionForm}>
                                                            <ColumnsDirective>
                                                                {/* <ColumnDirective field='category' headerText='Category' editTemplate={this.quesCategoryTemplate} textAlign='Center' width='200' /> */}
                                                                <ColumnDirective field='text' headerText='Question' edit={this.quesParams} textAlign='Center' />
                                                                <ColumnDirective field='subSectionQuesAnsGroupDTO.answerGroupName' headerText='Answer Type' isIdentity={true} width='125' textAlign='Center' />
                                                                <ColumnDirective field='isEditable' headerText='Editable' displayAsCheckBox="true" textAlign="Center" editType="booleanedit" type="boolean" />
                                                                <ColumnDirective field='isMandate' headerText='Mandate' displayAsCheckBox="true" textAlign="Center" editType="booleanedit" type="boolean" />
                                                                <ColumnDirective field='role' headerText='Select Role' editTemplate={this.selectRoleTemplateForQues} />
                                                            </ColumnsDirective>
                                                            <Inject services={[Page, Toolbar, Edit, RowDD, Selection, Search]} />
                                                        </GridComponent>

                                                    </div> : this.state.tabContentSection == 'dclForm' ?
                                                        <div>
                                                            <DclformComponent />

                                                        </div> : this.state.tabContentSection == 'Result_Form' ?
                                                            <div className='text-nowrap'>
                                                                {/* <ResultformComponent /> */}
                                                                <GridComponent rowDrop={this.onDragRow} toolbar={this.toolbarOptions}
                                                                    allowTextWrap={true}
                                                                    allowPaging={true} dataSource={this.state.subSection} ref={grid => this.resultFormGrid = grid} editSettings={this.editSettings} pageSettings={this.pageSettings} actionBegin={this.actionBeginRoleForm}
                                                                    allowRowDragAndDrop={true}>
                                                                    <ColumnsDirective>
                                                                        <ColumnDirective field='text' headerText='Select Role' editTemplate={this.selectRoleTemplate} />
                                                                        <ColumnDirective field='isEditable' headerText='Editable' displayAsCheckBox="true" textAlign="Center" editType="booleanedit" type="boolean" />
                                                                       <ColumnDirective field='isMandate' headerText='Mandate' displayAsCheckBox="true" textAlign="Center" editType="booleanedit" type="boolean" />
                                                                        <ColumnDirective field='role' headerText='Role' editTemplate={this.selectRoleTemplateForQues} />
                                                                    </ColumnsDirective>
                                                                    <Inject services={[Page, Toolbar, Edit, RowDD, Selection, Search]} />
                                                                </GridComponent>
                                                            </div> : <AccordionComponent>  </AccordionComponent>
                                        }

                                    </div>
                                    {/* ---------------Section Type Forms Ends------------ */}


                                    {/* <div className='mt-2' hidden={this.state.lineLoadView}>
                                    <Row>
                                        <Col sm='4'>
                                            <DropDownListComponent id="Category1" filterBarPlaceholder='Search Section Name' placeholder="Select Section Name" popupHeight="220px" dataSource={this.Storecategsearch} allowFiltering={true} fields={this.fields} />
                                        </Col>
                                        <Col sm='4'>
                                            <DropDownListComponent id="categsearchData" placeholder="Select a category" popupHeight="220px" dataSource={this.state.categories} fields={{ text: 'categoryName', value: 'id' }} allowFiltering={true} fields={this.fields} />
                                        </Col>
                                    </Row>
                                </div> */}
                                </Col>
                            </Row>
                        </DialogComponent>
                        {/* ----------------------- sectionDialogBox end ----------------------- */}

                    </div>
                </div>
                <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>


            </div>


        );

    }
}

export default InspectionComponent;
