export class PlanningHistoryModel {
    constructor(apmName, fmName, plantName, buyerName, qaName, unitName, merchandiserName, lineItemDesc, lineItemNo, leftPlannedQty, leftPoQtyTobeScheduled,
        poOrderedQty, scheduledQty, poQty, plantId, saidId, pcdDate, status, unitId, buyerPoNo, alertCode, pcdDateForFilter,plannedQty,exFactoryDate) {
        this.apmName = apmName;
        this.buyerName = buyerName;
        this.fmName = fmName;
        this.leftPlannedQty = leftPlannedQty;
        this.leftPoQtyTobeScheduled = leftPoQtyTobeScheduled;
        this.lineItemDesc = lineItemDesc;
        this.lineItemNo = lineItemNo;
        this.merchandiserName = merchandiserName;
        this.poOrderedQty = poOrderedQty;
        this.pcdDate = pcdDate;
        this.scheduledQty = scheduledQty;
        this.plantId = plantId;
        this.plantName = plantName;
        this.poQty = poQty;
        this.qaName = qaName;
        this.saidId = saidId;
        this.status = status;
        this.unitName = unitName;
        this.unitId = unitId;
        this.buyerPoNo = buyerPoNo;
        this.alertCode = alertCode;
        this.pcdDateForFilter = pcdDateForFilter;  //temporary variable for end date filter
        this.plannedQty=plannedQty;
        this.exFactoryDate=exFactoryDate;
    }
    apmName = null;
    buyerName = null;
    fmName = null;
    pcdDateForFilter = null;
    leftPlannedQty = null;
    leftPoQtyTobeScheduled = null;
    lineItemDesc = null;
    lineItemNo = null;
    merchandiserName = null;
    poOrderedQty = null;
    pcdDate = null;
    scheduledQty = null;
    plantId = null;
    plantName = null;
    poQty = null;
    qaName = null;
    saidId = null;
    status = null;
    unitName = null;
    unitId = null;
    buyerPoNo = null;
    alertCode = null;
    plannedQty=null;
    exFactoryDate=null;
}
export default PlanningHistoryModel;



export class StockModel {
    constructor(id, buyer_po, materialBluePCDdate, issued_qty, required_material_Qty, buyer, resv_store_location, material_category, uom, materialBluePCDQty, said,
        line_item_no, material_code, material_description, reserved_qty) {
        this.buyer_po = buyer_po;
        this.materialBluePCDdate = materialBluePCDdate;
        this.issued_qty = issued_qty;
        this.required_material_Qty = required_material_Qty;
        this.buyer = buyer;
        this.resv_store_location = resv_store_location;
        this.material_category = material_category;
        this.uom = uom;
        this.materialBluePCDQty = materialBluePCDQty;
        this.id = id;
        this.said = said;
        this.line_item_no = line_item_no;
        this.material_code = material_code;
        this.material_description = material_description;
        this.reserved_qty = reserved_qty;
    }
    buyer_po = null;
    materialBluePCDdate = null;
    issued_qty = null;
    required_material_Qty = null;
    buyer = null;
    resv_store_location = null;
    material_category = null;
    uom = null;
    materialBluePCDQty = null;
    id = null;
    said = null;
    line_item_no = null;
    material_code = null;
    material_description = null;
    reserved_qty = null;
}