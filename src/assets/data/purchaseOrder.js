
export const polistData = [
    { id: 1, "poName": "PO1234567" },
    { id: 2, "poName": "PO9876543" },
]

export const potypeData = [
    { id: 1, "potype": "Fabric Po" },
    { id: 2, "potype": "Accessorize P0" },
    { id: 3, "potype": "Accessorize Po" },
    { id: 4, "potype": "Job work Po" },  
]

export const grnDropdownData = [
    { id: 1, "potype": "GRN123456" },
    { id: 2, "potype": "GRN987654" },
]

export const btnDropdownData = [
    { id: 1, "btnText": "BT123456" },
    { id: 2, "btnText": "BT987654" },
]

export const decisionWiseDropdownData = [
    { id: 1, "decision": "Approved" },
    { id: 2, "decision": "Reject" },
]
 
export const plantListData = [
    { id: 1, "plant": "Radiaant Expovision A70" },
    { id: 2, "plant": "3050 – Radiaant Expovision D207" },
    { id: 3, "plant": "Radiaant Expovision A70" }, 
]

export const departmenttypeData = [
    { id: 1, "dept": "UD/Accounts/BVT/HOD1/HOD2" },
    { id: 2, "dept": "UD/Accounts/BVT/HOD1/HOD2" },
    { id: 3, "dept": "UD/Accounts/BVT/HOD1/HOD2" }, 
] 

export const departmentDropdownData = [
    { id: 1, "department": "UD/Accounts/BVT/HOD1/HOD2" },
    { id: 2, "department": "UD/Accounts/BVT/HOD1/HOD2" },
]
 

export const statusDropdownData = [
    { id: 1, "department": "Pending" },
    { id: 2, "department": "Complete" },
]

export const poGroupslistData = [
    { id: 1, "pogroups": "PO1" },
    { id: 2, "pogroups": "PO2" },
    { id: 3, "pogroups": "PO3" },
]
 
export const mappingDepartmentGridData = [
    {
        'poType':'Fabric Po',
        'poGroup': 'PO1',
        'plant': 'Radiaant Expovision A70',
        'processFlow': ' ', 
        'unit':'A',
        'line':'PL1',
        'qcName':'Anuj',
        'qcUserid':'01ARAD', 
    }, 
    {
        'poType':'Accessorize Po',
        'poGroup': 'PO2',
        'plant': 'Radiaant Expovision A70',
        'processFlow': ' ', 
        'unit':'B',
        'line':'PL2',
        'qcName':'Anuj',
        'qcUserid':'01ARAD', 
    },
    {
        'poType':'Job work Po',
        'poGroup': 'PO3',
        'plant': 'Radiaant Expovision A70',
        'processFlow': ' ',
        'unit':'C',
        'line':'PL3',
        'qcName':'Anuj',
        'qcUserid':'01ARAD',  
    },
] 


export const addmappingDepartmentGridData = [  
    {
        'poType': ' ',
        'poGroups': ' ',
        'plant': ' ',
        'department':'',
        'seq':' ',  
    },
]

export const grnGridInfoData = [
    {
        'btId': 1,
        'btDate': '01/05/2020',
        'ud': 'pass',
        'accounts': 'pass',
        'bvt': 'pending',
        'hod1': '',
        'hod2': '',
        'audit': '',
        'management': '',
    }, 
] 


export const qualityAssigneeGridData = [
    {
        'btId': 1,
        'btDate': '01/05/2020',
        'ud': 'pass',
        'accounts': 'pass',
        'bvt': 'pending',
        'hod1': '',
        'hod2': '',
        'audit': '',
        'management': '',
    }, 
] 



export const inspectionDetailsdata = [
    {
        'id': 'INS0710200001',
        'description': 'Fabric - 100% FCR', 
        'type': 'Fabric PO', 
        'assignee':'Ankur',
        'order': 'PO12345678',  
        'buyerVendor': 'ABC fabrics', 
        'plant':'Radiaant Expovision A70',
        'unit': 'A', 
        'tableLine':'Table1',
        'result': ' ',  
        'status': 'Pending',   
    },
    {
        'id': 'INS0710200001',
        'description': 'Fabric - 100% FCR', 
        'type': 'Fabric PO', 
        'assignee':'Ankur',
        'order': 'PO12345678',  
        'buyerVendor': 'ABC fabrics', 
        'plant':'Radiaant Expovision A70',
        'unit': 'A', 
        'tableLine':'Table1',
        'result': ' ',  
        'status': 'Pending',   
    },
    {
        'id': 'INS0710200001',
        'description': 'Fabric - 100% FCR', 
        'type': 'Fabric PO', 
        'assignee':'Ankur',
        'order': 'PO12345678',  
        'buyerVendor': 'ABC fabrics', 
        'plant':'Radiaant Expovision A70',
        'unit': 'A', 
        'tableLine':'Table1',
        'result': ' ',  
        'status': 'Pending',   
    },
    {
        'id': 'INS0710200001',
        'description': 'Fabric - 100% FCR', 
        'type': 'Fabric PO', 
        'assignee':'Ankur',
        'order': 'PO12345678',  
        'buyerVendor': 'ABC fabrics', 
        'plant':'Radiaant Expovision A70',
        'unit': 'A', 
        'tableLine':'Table1',
        'result': ' ',  
        'status': 'Pending',   
    },
 ] 

export const assigneeBoxGridData = [
    {
        'id': 1,
        'description': 'description',
        'roles': 'pass', 
        'userid':'userid1',
    }, 
    {
        'id': 2,
        'description': 'description',
        'roles': 'pass', 
        'userid':'userid2',
    }, 
    {
        'id': 3,
        'description': 'description',
        'roles': 'pass', 
        'userid':'userid3',
    }, 
]

export const assignTableGridData = [
    {
        'id': 1,
        'description': 'description',
        'table': 'table1',  
    }, 
] 

export const lineInfoGridData = [
    {
        'lineitem': '000010',
        'liDescription': 'CT VELVET 2/40X14 GSM 280 SLD LILAC 044D', 
        'poQty': '5000',
        'pcbDate': '7/10/2020',
        'uom': 'U',
        'pcbqty': '2000',
        'grnQty': '500',
        'returenQty': '0',
        'balanceqty': '04500',
      },
      {
        'lineitem': '000020',
        'liDescription': 'VISVORTEX 30x30/80x64 PISTA GREEN 054P', 
        'poQty': '5000',
        'pcbDate': '7/10/2020',
        'uom': 'U',
        'pcbqty': '2000',
        'grnQty': '500',
        'returenQty': '0',
        'balanceqty': '04500',
      }
]

export const grnLineGridDataN = [
    {
       
        'lineitem': '000010',
        'lineDescription': 'CT VELVET 2/40X14 GSM 280 SLD LILAC 044D',
        'uom': 'M',
        'grnQty': 500,
      },
      { 
        'lineitem': '000020',
        'lineDescription': 'VISVORTEX 30x30/80x64 PISTA GREEN 054P',
        'uom': 'M',
        'grnQty': 500,
      }
]



export const actionHistoryGridData = [
    {
        'department': 'UD',
        'module': 'VI',
        'date': '1/10/2020',
        'decision': true,
        'userName': 'Ankur',
        'documents': 'PDF1, PDF 2',
    },
    {
        'department': 'UD',
        'module': 'Lab Inspection',
        'date': '12/08/2020',
        'decision': false,
        'userName': 'Vikas',
        'documents': 'PDF link 1',
    },
    {
        'department': 'UD',
        'module': '4 point/AQL',
        'date': '12/08/2020',
        'decision': false,
        'userName': 'Vikas',
        'documents': 'PDF link 1',
    },
    {
        'department': 'FCR/ACR',
        'module': 'FCR/ACR',
        'date': '12/08/2020',
        'decision': false,
        'userName': 'Vikas',
        'documents': 'PDF link 1',
    },
    {
        'department': 'Accounts',
        'module': 'Accounts',
        'date': '12/08/2020',
        'decision': false,
        'userName': 'Gaurav',
        'documents': 'PDF link 1',
    },
    {
        'department': 'BVT',
        'module': 'Debit note',
        'date': '3/10/2020',
        'decision': false,
        'userName': 'Gaurav',
        'documents': 'PDF link 1',
    }


]

export const grnLineGridBTData = [
    {
        'department': 'UD',
        'module': 'VI',
        'date': '12/08/2020',
        'decision': true,
        'userName': 'Vikas',
        'documents': 'PDF link 1',
    },
    {
        'department': 'UD',
        'module': 'Lab Inspection',
        'date': '12/08/2020',
        'decision': false,
        'userName': 'Vikas',
        'documents': 'PDF link 1',
    },
    {
        'department': 'UD',
        'module': '4 point/AQL',
        'date': '12/08/2020',
        'decision': false,
        'userName': 'Vikas',
        'documents': 'PDF link 1',
    },
    {
        'department': 'FCR/ACR',
        'module': '4 point/AQL',
        'date': '12/08/2020',
        'decision': false,
        'userName': 'Vikas',
        'documents': 'PDF link 1',
    },
    {
        'department': 'Accounts',
        'module': 'Accounts',
        'date': '12/08/2020',
        'decision': false,
        'userName': 'Vikas',
        'documents': 'PDF link 1',
    }


]




export const invoiceDataGridData = [
    {
        'invoicenumber': 'Inv1',
        'invoiceDate': '1/10/2020',
        'vendorName': 'ABC Fabric',
        'grnNumber': 'GRN1',
        'grnDate': '1/10/2020',
        'grnQty': '500',
        'totalValue': '120000',
        'btNumber': 'BT1',
        'btStatus': 'Complete',
        'pendingOnDepartment': ' ',
        'assignee':'',
        'poNumber': 'PO1234',
        'poType': 'Fabric Po',
        'action': '', 
    }, 
    {
        'invoicenumber': 'Inv2',
        'invoiceDate': '1/10/2020',
        'vendorName': 'ABC Fabric',
        'grnNumber': 'GRN2',
        'grnDate': '1/10/2020',
        'grnQty': '500',
        'totalValue': '120000',
        'btNumber': 'BT1',
        'btStatus': 'Complete',
        'pendingOnDepartment': ' ',
        'assignee':'',
        'poNumber': 'PO1234',
        'poType': 'Fabric Po',
        'action': '', 
    }, 
    {
        'invoicenumber': 'Inv',
        'invoiceDate': '1/10/2020',
        'vendorName': 'ABC Fabric',
        'grnNumber': 'GRN3',
        'grnDate': '1/10/2020',
        'grnQty': '500',
        'totalValue': '120000',
        'btNumber': 'BT1',
        'btStatus': 'Complete',
        'pendingOnDepartment': ' ',
        'assignee':'',
        'poNumber': 'PO1234',
        'poType': 'Fabric Po',
        'action': '', 
    }, 
]
