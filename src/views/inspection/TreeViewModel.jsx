export class TreeViewModel {
    constructor(id, name,hasChild, count,isSelected) {
        this.id = id;
        this.name = name;
        this.hasChild=hasChild;
        this.count= count;
        this.isSelected = isSelected;   
    }
    id = null;
    name = null;
    hasChild=null;
    count =null;
    isSelected=null
    
}


export default TreeViewModel; 

export class ChildViewModel{
    constructor(id,pid,name,count,isSelected)
    {
        this.id=id;
        this.pid=pid;
        this.name=name;
        this.count=count;
        this.isSelected = isSelected;
        
    }
    id = null;
    pid=null;
    name = null;
    count =null;
    isSelected=null

}