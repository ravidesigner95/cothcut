import React from 'react';

class Dashboard extends React.PureComponent {
  state = {}
  render() {
    return (
      <div className="container-fluid-inner">
         <h1 className='pagesheading'>Production Dashboard</h1>
        <iframe src="https://app.powerbi.com/view?r=eyJrIjoiZTZiZDU5NTAtNGYyZC00NzdlLWEzOWItODkxMzU2MzZlZTFmIiwidCI6Ijc1ZDg4MmE0LWFkM2EtNDM3Ni1hNGFhLTBhNjVhZmVmOTlkMiJ9"
          height="765" width="100%" allowFullScreen={true} frameBorder="0"></iframe> 
      </div>
    );
  }
}

export default Dashboard;
