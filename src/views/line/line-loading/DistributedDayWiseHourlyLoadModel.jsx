class DistributedDayWiseHourlyLoadModel {
    constructor(id, hourType, labour, noOfHours, noOfmachines, skilledWorker, targetQty, unskilledWorker, distributedDayWiseLoadTableDtoLoadId) {
        this.hourType = hourType;
        this.id = id;
        this.labour = labour;
        this.noOfHours = noOfHours;
        this.noOfmachines = noOfmachines;
        this.skilledWorker = skilledWorker;
        this.targetQty = targetQty;
        this.unskilledWorker = unskilledWorker;
        this.distributedDayWiseLoadTableDtoLoadId = distributedDayWiseLoadTableDtoLoadId;
    }
    hourType = null;
    id = null;
    labour = null;
    noOfHours = null;
    noOfmachines = null;
    skilledWorker = null;
    targetQty = null;
    unskilledWorker = null;
    distributedDayWiseLoadTableDtoLoadId = null;
}
export default DistributedDayWiseHourlyLoadModel;
