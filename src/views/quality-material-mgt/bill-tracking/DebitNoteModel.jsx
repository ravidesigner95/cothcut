
    export class DebitNoteModel {
        constructor(btNumber, debitNoteNumber, invoiceNumber, invoiceType,poLineItem,poLineItemDesc,totalDebitAmount,totalDebitQuantity,totalGRNQty,totalGrnAmount) {
            this.debitNoteNumber = debitNoteNumber;
            this.invoiceNumber = invoiceNumber;
            this.invoiceType = invoiceType;
            this.poLineItem = poLineItem;
            this.poLineItemDesc = poLineItemDesc;
            this.totalDebitAmount = totalDebitAmount;
            this.totalDebitQuantity = totalDebitQuantity;
            this.totalGRNQty = totalGRNQty;
            this.totalGrnAmount = totalGrnAmount;
            this.btNumber =btNumber;
        }
        debitNoteNumber = null;
        invoiceNumber = null;
        invoiceType = null;
        poLineItem = null;
        poLineItemDesc=null;
        totalDebitAmount = null;
        totalDebitQuantity=null;
        totalGRNQty=null;
        totalGrnAmount=null;
        btNumber= null;
    }
    
    export default DebitNoteModel; 
    