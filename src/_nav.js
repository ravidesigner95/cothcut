export default {
  items: [
    {
      name: 'CPM',
      url: '/userProfilling.jsx',
      icon: 'fa fa-list-ul',
      children: [
        {
          name: 'Dashboard',
          url: '/dashboard',
          icon: 'fa fa-newspaper-o',
        },
        {
          name: 'Hourly Production',
          url: '/hourly-production',
          icon: 'fa fa-clock-o',
        },
        {
          name: 'Line Configuration',
          url: '/line/line-configuration',
          icon: 'fa fa-th-list',
        },
        {
          name: 'Planning',
          url: '/line/line-planning',
          icon: 'fa fa-address-card-o',
        },
        {
          name: 'Scheduling',
          url: '/line/line-scheduling',
          icon: 'fa fa-calendar'
        }
      ]
    },
    // {
    //   name: 'Line Loading',
    //   url: '/lineloading',
    //   icon: 'fa fa-list-ul'
    // },
    {
      name: 'Admin',
      url: '/userProfilling.jsx',
      icon: 'fa fa-list-ul',
      children: [
        {
          name: 'User Management',
          url: '/admin/user-management',
          icon: 'icon-user',

        },
        {
          name: 'User Roles Permission',
          url: '/admin/user-permission',
          icon: 'icon-user-following',

        },
      
        {
          name: 'Add Team',
          url: '/admin/group-management',
          icon: 'fa fa-users',

        },
        {
          name: 'Line Process',
          url :  '/admin/line-Process',
          icon:   'fa fa-th-list',
        },
        {
          name: 'Line Configuration',
          url :  '/admin/line-configuration',
          icon:   'fa fa-th-list',
        }
      ]
    },
    {
      name: 'Version: 4.0',
      icon: 'fa fa-info-circle',
      class: 'mt-auto',
      attributes: { disabled: true },
    },

  ],
};
