import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { GridComponent, ColumnsDirective, ColumnDirective, Filter, Inject, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, Reorder } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { PropertyPane } from '../../assets/data/purchaseOrder';
import { SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { rippleMouseHandler } from '@syncfusion/ej2-buttons';
// import { orderDataSource } from '../../assets/data/data';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { sampleData } from '../../assets/data/inspection';
import { SampleBase } from '../../assets/data/sample-base';
import { rightToLeft } from '@syncfusion/ej2-base';
import QualityMasterService from './QualityMasterService';
import { DefectsModel, QualityMasterDefectsDataModel } from './DefectsModel';
import { AppConfig } from '../../appConfig/appConfig';
import '../../App.scss';


class defectsmasterComponent extends React.PureComponent {

  constructor() {
    super(...arguments);
    this.QualityMasterService = new QualityMasterService();
    this.orderDataSource = [
      {
        Code: 'STC020',
        Description: 'Bunched treads, slightly affecting appreance',
        Level: 'Minor',
      },
      {

        Code: 'STC021',
        Description: 'Stitching not under the marks/notches',
        Level: 'Major',
      },
      {

        Code: 'STC022',
        Description: 'One skipped stitch',
        Level: 'Minor',
      },
    ];

    this.state = {
      defectsList: [],
      selectedDefectData: [],
      selectedDefectName: null,
      selectedDefectId: null,
      defectNames: null,
      defectStatus: false,
      defectField:false,
      showDialog: false,
      toasterPosition: { X: 'Right', Y: 'Bottom' },
      orderDataSource: [
        {

          Code: 'STC020',
          Description: 'Bunched treads, slightly affecting appreance',
          Level: 'Minor',
        },
        {

          Code: 'STC021',
          Description: 'Stitching not under the marks/notches',
          Level: 'Major',
        },
        {

          Code: 'STC022',
          Description: 'One skipped stitch',
          Level: 'Minor',
        },
      ]
    };

    this.buttonRef = element => {
      this.buttonEle = element;
    };
    this.buttons = [{
      click: () => {
        this.onSaveClick();
        // this.getDefectList();
        // this.dialogInstance.hide();
      },
      buttonModel: {
        content: 'Save',
        isPrimary: true
      }
    },
    {
      click: () => {
        this.getDefectList();
        this.setState({defectField:false})
        this.dialogInstance.hide();

      },
      buttonModel: {
        content: 'Cancel',
        cssClass: 'e-secondary'
      }
    }];
    this.animationSettings = { effect: 'None' };

    this.toolbarOptions = ['Add', 'Edit', 'Delete', 'Update', 'Cancel'];
    // this.toolbarOptionsedit = ['Add', 'Delete', 'Cancel'];
    // this.editSettingsForAQLLeftPanel = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Bottom' };
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, newRowPosition: 'Top' };
    this.editparams = { params: { popupHeight: '500px' } };
    this.validationRule = { required: true };
    this.orderidRules = { required: true, number: true };
    this.pageSettings = { pageCount: 5 };
    this.format = { type: 'dateTime', format: 'M/d/y hh:mm a' };
    this.droplist = [
      { text: 'Top', value: 'Top' },
      { text: 'Bottom', value: 'Bottom' }
    ];

  }

  componentDidMount() {
    this.setState({
      orderDataSource: this.orderDataSource
    });
    this.getDefectList();
  }

  async getDefectList() {
    let response = (await this.QualityMasterService.getDefectsList())
    global.showLoader();
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let DefectsInfoList = [];
      let idSelectedData;
      let idSelectedDefectName;
      response.data.data.map(DefectsInfo => {
        let defectData = [];
        if(DefectsInfo.id == this.state.selectedDefectId){
           idSelectedData = DefectsInfo.qualityMasterDefectsData
           idSelectedDefectName = DefectsInfo.name
          }
        DefectsInfo.qualityMasterDefectsData.map(item => {
          defectData.push(new QualityMasterDefectsDataModel(item.id,null, item.code, item.level, item.description));
        })
        DefectsInfoList.push(new DefectsModel(DefectsInfo.id, DefectsInfo.name, DefectsInfo.isActive, false, defectData));
      })
      if(this.state.selectedDefectId){
        this.setState({ defectsList: DefectsInfoList, selectedDefectData: idSelectedData, selectedDefectName: idSelectedDefectName, selectedDefectId: this.state.selectedDefectId });
      }else{
        this.setState({ defectsList: DefectsInfoList, selectedDefectData: DefectsInfoList[0].qualityMasterDefectsData, selectedDefectName: DefectsInfoList[0].name, selectedDefectId: DefectsInfoList[0].id });
      }
      
      const newobj = [...this.state.defectsList];
      if(this.state.selectedDefectId){
        newobj.filter((item,findex) => item.id == this.state.selectedDefectId ? item.isTab = true : item.isTab = false)
      }else{
        newobj.filter((item,findex) => findex == 0 ? item.isTab = true : item.isTab = false)
      }
      this.setState({defectsList:newobj})
      
      this.defectNametemplate();
    }
    else {
      global.hideLoader();
      // this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }

  async saveDefectDetail(newDefect) {
    let that=this;
    let response = (await this.QualityMasterService.saveDefects(newDefect))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster(response.data.message, "Success"));
      this.getDefectList();
      that.dialogInstance.hide();
    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }

  async deleteDefectData(id) {
    let that =this;
    let response = (await this.QualityMasterService.deleteDefectData(id))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster("Defect  deleted successfully!!", "Success"));
      this.getDefectList();
      that.dialogInstance.hide();
    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }
  async deleteDefect(id) {
    let response = (await this.QualityMasterService.deleteDefect(id))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster("Defect  deleted successfully!!", "Success"));
      this.setState({selectedDefectId:null})
      this.getDefectList();
    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }


  actionBegin(args) {
    let that = this
    if (args.requestType === 'add') {
      this.setState({defectField:true})
    }
    if (args.requestType === 'cancel') {
      this.setState({defectField:false})
    }
    if (args.requestType === 'save') {
      args.cancel = true;
      this.setState({defectField:false})
      if(args.data.id == undefined && args.data.deid == undefined){
        that.setState({selectedDefectData:[...that.state.selectedDefectData,new QualityMasterDefectsDataModel(null, new Date().valueOf(), args.data.code, args.data.level, args.data.description)]})
      }else{
        let selectdefectCop = [...that.state.selectedDefectData]
        if(args.data.id){
          this.state.selectedDefectData.forEach((el,i)=>{
            if(el.id == args.data.id) {
              selectdefectCop[i].code = args.data.code
              selectdefectCop[i].level = args.data.level
              selectdefectCop[i].description = args.data.description
            }
          })
        }else{
          this.state.selectedDefectData.forEach((el,i)=>{
            if(el.deid == args.data.deid) {
              selectdefectCop[i].code = args.data.code
              selectdefectCop[i].level = args.data.level
              selectdefectCop[i].description = args.data.description
            }
          })
        }

        that.setState({selectedDefectData:selectdefectCop})
      }
      // if(this.state.selectedDefectName=="" || this.state.selectedDefectName==null ){
      //   this.toastObj.show(this.createToaster("Please enter defect name!","Information" ));
      // }
      // else{
      // let defectNewRow = [];
      // let defectData = [];
      // selectedDefectData.push(new QualityMasterDefectsDataModel(args.data.id, args.data.code, args.data.level, args.data.description));
      // defectNewRow = (new DefectsModel(this.state.selectedDefectId, this.state.selectedDefectName, this.state.defectStatus, defectData));
      // this.saveDefectDetail(defectNewRow);
      
      // that.state.selectedDefectData = defectData
      // }
      // if (this.gridInstance.pageSettings.currentPage !== 1 && this.gridInstance.editSettings.newRowPosition === 'Top') {
      //   args.index = (this.gridInstance.pageSettings.currentPage * this.gridInstance.pageSettings.pageSize) - this.gridInstance.pageSettings.pageSize;
      // }
      // else if (this.gridInstance.editSettings.newRowPosition === 'Bottom') {
      //   args.index = (this.gridInstance.pageSettings.currentPage * this.gridInstance.pageSettings.pageSize) - 1;
      // }
      
    }
    if (args.requestType === 'delete') {
      args.cancel = true;
      // this.deleteDefectData(args.data[0].id);
      // that.setState({selectedDefectData:[...that.state.selectedDefectData]})
      let data = that.state.selectedDefectData.filter((el,i)=> {
        if(args.data[0].id){
          return  el.id !== args.data[0].id
        }else{
          return  el.deid !== args.data[0].deid
        }
      })
      
      that.setState({selectedDefectData:data})
    }
  }
  ddChange() {
    this.gridInstance.editSettings.newRowPosition = this.dropDownInstance.value;
  }

  defectClicked = (item_id,i) => {
    let selectedObj = this.state.defectsList.filter(function (item) {
      return item.id == item_id;
    });
    this.setState({ selectedDefectData: selectedObj[0].qualityMasterDefectsData, selectedDefectName: selectedObj[0].name, selectedDefectId: selectedObj[0].id })

    const newobj = [...this.state.defectsList];
    newobj.filter((item,findex) => findex == i ? item.isTab = true : item.isTab = false)
    this.setState({defectsList:newobj})

    this.defectNametemplate()

  }
  onSaveClick() {
    if(this.state.selectedDefectName=="" || this.state.selectedDefectName==null ){
      this.toastObj.show(this.createToaster("Please enter defect name!","Information" ));
    }else if(this.state.defectField){
      this.toastObj.show(this.createToaster("Please fill all fields and update!","Information" ));
    }
    else{
    let defectUpdated = new DefectsModel(this.state.selectedDefectId, this.state.selectedDefectName, this.state.defectStatus, false, this.state.selectedDefectData)
    this.saveDefectDetail(defectUpdated)
    
    }
  }

  defectNametemplate = (args) => {
    const defectNames = []
    for (const [index, value] of this.state.defectsList.entries()) {
      defectNames.push(<li key={index} className={`selected-vertical-tabs ${value.isTab ? 'active':''}`} onClick={this.defectClicked.bind(this, value.id,index)}> <a>{value.name}</a><span className='actions'><i className="fa fa-pencil" onClick={this.linkClicked.bind(this, value.id)}></i><i className="fa fa-trash" onClick={this.deleteDefectById.bind(this, value.id)}></i></span></li>)
    }
    this.setState({ defectNames: defectNames })
  }

  onChangeDefectName = (event) => {
    this.setState({ selectedDefectName: event.value.trim() })
  }

  onChangeStatus = (event) => {
    this.setState({ defectStatus: event.checked })
  }
/*   onChangeStatus = (event) => {
    if(this.state.defectStatus == true)
    this.setState({ defectStatus: false })
    else
    this.setState({ defectStatus: true })
  } */

  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }

  linkClicked = (args) => {

    if (args == 'new') {
      this.setState({ selectedDefectData: [], selectedDefectName: null, selectedDefectId: null, defectStatus: false });
    }
    else {
      let selectedObj = this.state.defectsList.filter(function (item) {
        return item.id == args;
      })
      this.setState({ selectedDefectData: selectedObj[0].qualityMasterDefectsData, selectedDefectName: selectedObj[0].name, selectedDefectId: selectedObj[0].id, defectStatus: selectedObj[0].isActive })
    }
    // this.setState({
    //   showDialog: true
    // });
    this.dialogInstance.show();
  }

  deleteDefectById = (args) => {
    let that = this;
    that.deleteDefect(args);
   }

  dialogClose() {
    this.setState({
      showDialog: false,
      defectField:false
      //  orderDataSource: this.orderDataSource
    });
    // this.buttonEle.style.display = "block";
  }
  dialogOpen() {
    // this.buttonEle.style.display = "none";
  }

  render() {
    return (<div className='control-pane' id='targetElement2'>
      <div className='control-section col-lg-12 defaultDialogComponent dialog-target'>
        <Button color="primary" className='addbtn' size="sm" onClick={this.linkClicked.bind(this, 'new')} >Add</Button>
        <Row >
          <Col sm='2'>
            <ul className='vertical-tabs'>
              {this.state.defectNames}
            </ul>
          </Col>
          <Col sm='10'>
            <GridComponent
              enableHover={false}
              allowFiltering={false}
              allowSorting={true}
              allowSelection={true}
              allowResizing={true}
              rowHeight={25}
              gridLines='Both'
              // toolbar={this.toolbarOptions}
              allowPaging={true}
              allowTextWrap={true}
              allowRowDragAndDrop={false}
              height={AppConfig.gridHeight}
              filterSettings={AppConfig.FilterSettingsModel}
              // editSettings={AppConfig.editSettings}
              pageSettings={AppConfig.pageSettings}
              dataSource={this.state.selectedAqlData}
              // rowSelected={this.rowSelected}
              selectionSettings={{ enableToggle: false }}
              dataSource={this.state.selectedDefectData} >
              <ColumnsDirective>
                <ColumnDirective field='code' headerText='Code' width='100' validationRules={{ required: true }}></ColumnDirective>
                <ColumnDirective field='description' headerText='Description' width='450' validationRules={{ required: true }}></ColumnDirective>
                <ColumnDirective field='level' headerText='Level' width='150' textAlign='center' validationRules={{ required: true }}/>
              </ColumnsDirective>
              <Inject services={[Page, Sort, Toolbar, Selection]} />
            </GridComponent>
          </Col>
        </Row>
        <DialogComponent
          id="defectDetails"
          showCloseIcon={true}
          allowDragging={true}
          animationSettings={this.animationSettings}
          visible={this.state.showDialog}
          width={'96%'}
          ref={dialog => this.dialogInstance = dialog}
          isModal={true}
          //target={'#targetElement2'} 
          header='Defect Details'
          buttons={this.buttons}
          // close={this.dialogClose.bind(this)}
          >
          <Row>
            <Col sm='3'>
              <TextBoxComponent placeholder="" value={this.state.selectedDefectName} onChange={this.onChangeDefectName} floatLabelType="Auto" />

            </Col>
            <Col sm='9' className='status-switch'>
              Status<label htmlFor="unchecked" className='mr-3'></label>
              <SwitchComponent id="unchecked" checked={this.state.defectStatus} change={this.onChangeStatus}></SwitchComponent> 
            </Col>
          </Row>
          <GridComponent rowSelected={this.rowSelected}
            enableHover={false}
            allowFiltering={false}
            allowSorting={true}
            allowSelection={true}
            allowResizing={true}
            rowHeight={25}
            height='60'
            gridLines='Both'
            allowPaging={true}
            allowTextWrap={true}
            allowRowDragAndDrop={false}
            filterSettings={AppConfig.FilterSettingsModel}
            editSettings={AppConfig.editSettings}
            pageSettings={AppConfig.pageSettings}
            toolbar={AppConfig.toolbarOptionsButton}
            dataSource={this.state.selectedDefectData}
            ref={grid => this.gridInstance = grid}
            actionBegin={this.actionBegin.bind(this)} >
            <ColumnsDirective>
              <ColumnDirective field='code' headerText='Code' width='100' textAlign='center' validationRules={{ required: true }}></ColumnDirective>
              <ColumnDirective field='description' headerText='Description' width='450' textAlign='center' validationRules={{ required: true }}></ColumnDirective>
              <ColumnDirective field='level' headerText='Level' width='150' textAlign='center' validationRules={{ required: true }}/>
            </ColumnsDirective>
            <Inject services={[Page, Sort, Selection, Toolbar, Edit]} />
          </GridComponent>

        </DialogComponent>
      </div>
      <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
    </div>);
  }
}

export default defectsmasterComponent;
