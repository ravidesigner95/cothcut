export class QualityTableAssigneeModel {
    constructor(id, ticketNo,inspectionDate,qty,tableOrMachineDesc,inspectionType,tableOrMachineCode,thanOrRollId) {
        this.id = id;
        this.ticketNo = ticketNo;
        this.inspectionDate = inspectionDate;
        this.qty = qty;
        this.tableOrMachineDesc = tableOrMachineDesc;
        this.tableOrMachineCode = tableOrMachineCode;
        this.inspectionType = inspectionType;
        this.thanOrRollId=thanOrRollId;

    }
    id = null;
    ticketNo = null;
    inspectionDate = null;
    qty = null;
    tableOrMachineDesc = null;
    inspectionType = null;
    tableOrMachineCode = null;
    thanOrRollId=[];

}

export default QualityTableAssigneeModel; 