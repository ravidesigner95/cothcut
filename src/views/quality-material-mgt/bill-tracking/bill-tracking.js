import React from 'react';
import { withTranslation } from "react-i18next";
import { DateRangePickerComponent } from '@syncfusion/ej2-react-calendars';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import {
  Button, CardGroup, Col, Card,
  CardBody, CardText, CardFooter, CardHeader, CardTitle, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row
} from 'reactstrap';
import { AccordionComponent } from '@syncfusion/ej2-react-navigations';
import { GridComponent, ColumnsDirective, ColumnDirective, Inject, Selection, DetailRow, Page, Sort, Resize, Freeze, Edit, CommandColumn, Group, Filter, Reorder, ColumnChooser, Toolbar } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent, MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { polistData, potypeData, grnDropdownData, grnLineGridDataN, decisionWiseDropdownData, departmentDropdownData, statusDropdownData, btnDropdownData, lineInfoGridData, grnGridInfoData, actionHistoryGridData, grnLineGridBTData, invoiceDataGridData } from '../../../assets/data/purchaseOrder';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';

import BillTrackingService from './BillTrackingService';
import BillTrackingInfoModel from './BillTrackingInfoModel';
import GrnInvoiceModel from './GrnInvoiceModel';
import PoInfoModel from './PoInfoModel';
import BtHistoryModel from './BtHistoryModel';
import { ToastComponent } from '@syncfusion/ej2-react-notifications';

import SinglePagePDFViewer from "../../../components/pdf/single-page";
import ApproveReviewModel from './ApproveReviewModel';
import DebitNoteModel from './DebitNoteModel';
import PoModel from './PoModel';
import DocumentModel from './DocumentModel';
import { AppConfig } from '../../../appConfig/appConfig';

class BillTracking extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    this.BillTrackingService = new BillTrackingService();
    this.editSettings = { allowEditing: true, allowAdding: true, allowDeleting: true, allowEditOnDblClick: false };
    this.numericParams= { params: { decimals: 2 } };
    this.viewIcon = [
      {
        type: 'View', buttonOption: {
          iconCss: 'e-icons',
          // content: 'Update Store',
          cssClass: 'e-flat e-btn pdf-view'
        },

      },
      //    {
      //    type: 'download', buttonOption: {
      //     iconCss: 'e-icons',
      //     // content: 'Update Store',
      //     cssClass: 'e-flat bill-approved'
      //   },
      // }
    ];
    this.commandsInvocieGrid = [
      // {
      //   type: 'Bill Created', buttonOption:
      //     { iconCss: 'e-icons', cssClass: 'e-flat bill-created' }
      // },
      // { type: 'Bill Approved', buttonOption: { iconCss: 'e-icons', cssClass: 'e-flat bill-approved' } },
      { type: 'View', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } }];
    this.state = {
      previousDebitNoteCommands: [{ type: 'SaveSap', buttonOption: { iconCss: 'e-icons e-save', cssClass: 'e-flat edit-btn' } },
      { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },],
      FilterSettingsModel: { type: 'Menu' },
      IFilter: { type: 'CheckBox' },
      polist: polistData,
      pofields: { text: 'poName', value: 'id' },
      potype: potypeData,
      potypefields: { text: 'potype', value: 'id' },
      grnDropdown: grnDropdownData,
      grnfields: { text: 'potype', value: 'id' },
      btnDropdown: btnDropdownData,
      btnfields: { text: 'btnText', value: 'id' },
      decisionWiseDropdown: decisionWiseDropdownData,
      decisionWisefields: { text: 'decision', value: 'id' },
      departmentDropdown: departmentDropdownData,
      departmentfields: { text: 'department', value: 'id' },
      statusDropdown: statusDropdownData,
      statusfields: { text: 'department', value: 'id' },
      startDate: '01/11/2020',
      endDate: '01/11/2021',
      format: { type: 'dateTime', format: 'dd/MM/yy' },
      toasterPosition: { X: 'Right', Y: 'Bottom' },
      filterBox: true,
      invoiceDataGrid: invoiceDataGridData,
      lineInfoGrid: lineInfoGridData,
      grnGrid: [
        {
          'grnId': 12,
          'grnDate': '12/06/2020',
          'grnTotalQty': 250,
        }


      ],
      grnLineGridData: grnLineGridDataN,
      grnGridInfo: grnGridInfoData,
      actionHistoryGrid: actionHistoryGridData,
      grnLineGridBT: grnLineGridBTData,
      hideSectionbox: true,
      collapedClass: 'heading closeed',
      accordionbox: true,
      showActionHistorySection: true,
      billTrackingDialogBox: false,
      viewallDocumentsDialogBox: false,
      viewBillApprovedDialogBox: false,
      invoicedialogDialogBox: false,
      debitNoteGrid: true,
      grnBox: true,
      divHidden: null,
      formHeading: '',
      grnBarclass: 'grnclose',

      btInfoList: [],
      billTrackingModel: new BillTrackingInfoModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,null),
      selectedRowBtGrid: null,
      pdfInfo: null,
      pdf: false,
      imageInfo: null,
      image: false,
      currentProcessId: null,
      currentProcessName: null,
      pdfViewDialog: false,
      reviewDialogBox: false,
      selectedReviewProcess: null,
      currentProcessEditable: false,
      selectedPoNumber: [],
      selectedPotype: [],
      selectedGrnNumber: [],
      selectedBtNumber: [],
      selectedDepartment: [],
      selectedStatus: [],
      selectedGrnDate: [],
      selectedLotNumber: [],
      selectedAssignee: [],

      uniquePoNumber: [],
      uniquePoType: [],
      uniqueGrnNumber: [],
      uniqueBtNumber: [],
      uniqueDept: [],
      uniqueStatus: [],
      uniqueGrnDate: [],
      uniqueAssignee: [],
      uniqueLotNumber: [],
      isUpload: false,
      docId: null,
      docName: null,
      fileModel: new FormData(),
      windowsHeight: window.innerHeight - 280 + 'px',
      btMiroDetails: [],
      previousDebitNotes: [],
      debitNoteDetails: [],
      debitNoteDetailsDialog: false,
      previousGenerateddebitNoteDetails: [],
      selectedDebitNoteNumber: ""
    }
    this.commands = [{ type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
    { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },
    { type: 'Save', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
    { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } }];

    // this.previousDebitNoteCommands = [{ type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
    // { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },
    // { type: 'Save', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
    // { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } },
    // { type: 'Update Sap', buttonOption: { iconCss: 'e-icons', cssClass: 'e-flat e-primary' } }];


    this.grnLineGrid = {
      dataSource: this.state.grnLineGridData,
      queryString: 'grnID',
      // allowPaging: true,
      //  pageSettings: { pageSize: 6, pageCount: 3 },
      columns: [
        { field: 'lineitem', headerText: 'Line item #', width: 120 },
        { field: 'lineDescription', headerText: 'Line Description', width: 120 },
        { field: 'uom', headerText: 'Uom', width: 120 },
        { field: 'grnQty', headerText: 'GRN Qty', width: 150 }
      ],
    };
    this.grnLineGridn = { showGroupedColumn: false, columns: ['btProcessName'] };
    this.toolbarOptions = ['Search', 'ColumnChooser'];

  }
  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }


  componentDidMount() {
    this.getBtList();
  }

  ///////////////////////////////////////////API CALLS///////////////////////////////

  async getBtList() {
    global.showLoader();
    let response = (await this.BillTrackingService.getBtList())
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let btInfoList = [];
      response.data.data.map(item => {
        btInfoList.push(new BillTrackingInfoModel(item.totalValue, item.pendingOnDepartment, item.btId, item.assigneeName, item.assigneeUserID, item.grnDate, item.invoiceDate, item.vendorName,
          item.grnQty, item.grnNumber, item.invoiceNumber, item.btNumber, item.poType, item.btStatus, item.poNumber, item.poGroup, item.poTypeId, item.poGroupId, item.lotNumber, item.plantId, item.plantName,item.remark))
      })
      let uniquePoNumber = btInfoList.filter((value, index, array) => array.findIndex(t => (t.poNumber === value.poNumber) && (value.poNumber != null) && (value.poNumber != "")) === index);
      let uniquePoType = btInfoList.filter((value, index, array) => array.findIndex(t => (t.poType === value.poType) && (value.poType != null) && (value.poType != "")) === index);
      let uniqueBtNumber = btInfoList.filter((value, index, array) => array.findIndex(t => (t.btNumber === value.btNumber) && (value.btNumber != null) && (value.btNumber != "")) === index);
      let uniqueGrnNumber = btInfoList.filter((value, index, array) => array.findIndex(t => (t.grnNumber === value.grnNumber) && (value.grnNumber != null) && (value.grnNumber != "")) === index);
      let uniqueDept = btInfoList.filter((value, index, array) => array.findIndex(t => (t.pendingOnDepartment === value.pendingOnDepartment) && (value.pendingOnDepartment != null) && (value.pendingOnDepartment != "")) === index);
      let uniqueStatus = btInfoList.filter((value, index, array) => array.findIndex(t => (t.btStatus === value.btStatus) && (value.btStatus != null) && (value.btStatus != "")) === index);
      let uniqueGrnDate = btInfoList.filter((value, index, array) => array.findIndex(t => (t.grnDate === value.grnDate) && (value.grnDate != null) && (value.grnDate != "")) === index);
      let uniqueAssignee = btInfoList.filter((value, index, array) => array.findIndex(t => (t.assigneeName === value.assigneeName) && (value.assigneeName != null) && (value.assigneeName != "")) === index);
      let uniqueLotNumber = btInfoList.filter((value, index, array) => array.findIndex(t => (t.lotNumber === value.lotNumber) && (value.lotNumber != null) && (value.lotNumber != "")) === index);

      this.setState({ btInfoList, uniquePoNumber: uniquePoNumber, uniqueBtNumber: uniqueBtNumber, uniqueGrnNumber: uniqueGrnNumber, uniquePoType: uniquePoType, uniqueDept: uniqueDept, uniqueStatus: uniqueStatus, uniqueGrnDate: uniqueGrnDate, uniqueAssignee: uniqueAssignee, uniqueLotNumber: uniqueLotNumber })

      //  this.downloadFile(442964);
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));

    }
  }

  async getGrnInvoiceData(btNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.getGrnInvoiceList(btNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      console.log(response)
      let btGrnInvoiceInfoList = [];
      response.data.data.map(item => {
        btGrnInvoiceInfoList.push(new GrnInvoiceModel(item.grnQty, item.invoiceCurrency, item.invoiceNumber, item.invoiceValue, item.lineItem, item.lineItemDesc, item.uom))
      })
      this.setState({ btGrnInvoiceInfoList })
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));

    }
  }
  async getPoData(btNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.getPoInfoList(btNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      console.log(response)
      let btPoInfoList = [];
      let poList;

      response.data.data.btPODetailDTOS.map(item => {
        btPoInfoList.push(new PoInfoModel(item.id, item.poQty, item.pOLineDescription, item.poNumber, item.pcdDate
          , item.balanceQty, item.grnTotQty, item.pcdQty, item.returnQty, item.poLineItem, item.Uom))
      })

      poList = new PoModel(response.data.data.poDeliverDate, response.data.data.vendorName, response.data.data.totalPoQty, response.data.data.status, btPoInfoList, response.data.data.score);
      this.setState({ btPoInfoList, poList })
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));

    }
  }

  async getPoBtHistory(btNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.getBtHistory(btNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      console.log(response)
      let btHistoryList = [];
      response.data.data.map(item => {
        btHistoryList.push(new BtHistoryModel(item.date, item.comments, item.btHistoryDocDTOList, item.btProcessName, item.userId, item.userName, item.btProcessId, item.status, item.btNumber))
      })
      this.setState({ btHistoryList })
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));

    }
  }
  async getAllDocuments(btNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.getAllDocuments(btNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();

      let documentsList = [];
      response.data.data.map(item => {
        if (item.documentName != null) {
          documentsList.push(item);
        }
      });

      this.setState({ documentsList })
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));

    }
  }

  async getBtProcessHistory(btNumber, processId, processName) {
    global.showLoader();
    let response = (await this.BillTrackingService.getBtProcessHistory(btNumber, processId))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let btProcessHistory = response.data.data;
      this.setState({ btProcessHistory })

      if (processName.includes('BVT')) {
        this.setState({ divHidden: 'bvtForm', formHeading: processName })
      }
      else {
        this.setState({ divHidden: 'accountsForm', formHeading: processName })

      }
    }

    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));

    }

  }

  async getBtMiroInfo(btNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.getMiroDetail(btNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let btMiro = [];
      btMiro.push(response.data.data);
      this.setState({ btMiroDetails: btMiro })
    }

    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));

    }

  }

  async getBtProcessStatus(btNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.getBtProcessStatus(btNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let btProcessStatus = response.data.data;
      const processFlowName = []
      for (const [index, value] of btProcessStatus.entries()) {
        processFlowName.push(<li className={value.status == "P" ? "current" : value.status == "C" ? "pass" : ""} onClick={(e) => { this.billTrackingDialog(e, value) }}> <i class={value.status == "P" ? "fa fa-hourglass-half" : value.status == "C" ? "fa fa-check" : "fa fa-long-arrow-right"}></i> <p>{value.processName}</p></li>)
      }
      this.setState({ btProcessStatus, processFlowName })
      this.closebillTrackingDialog();
    }
    else {
      global.hideLoader();
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }

  }

  async getBtReviewProcess(btNumber, processId) {
    global.showLoader();
    let response = (await this.BillTrackingService.getBtReviewProcess(btNumber, processId))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let btReviewProcess = response.data.data;
      // response.data.data.map(item => {
      //   btReviewProcess.push(item.processName);
      // })

      this.setState({ btReviewProcess, reviewDialogBox: true })
    }
    else {
      global.hideLoader();
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }
  }

  async downloadReport(docId, type) {
    global.showLoader();
    let response = (await this.BillTrackingService.downloadReport(docId))
    if ((response != undefined) && (response.status == 200)) {
      if (response.data.data.fileName.includes('.pdf')) {
        let pdfEncoded = "data:application/pdf;base64," + response.data.data.imageEncoded;
        this.setState({ pdfInfo: pdfEncoded, pdf: true, imageInfo: null, image: false })
        this.showPDFDialog();
      }
      else if (response.data.data.fileName.includes('.jpg') || response.data.data.fileName.includes('.jpeg') || response.data.data.fileName.includes('.png')) {
        let imageEncoded = "data:image/jpeg;base64," + response.data.data.imageEncoded;
        this.setState({ imageInfo: imageEncoded, image: true, pdfInfo: null, pdf: false })
        this.showPDFDialog();
      }
      else {
        this.toastObj.show(this.createToaster("Not Supported!!", "Information"));
      }
      global.hideLoader();


      // var arrBuffer = this.base64ToArrayBuffer(response.data.data.imageEncoded);
      // var blob = new Blob([arrBuffer], {type: 'image/jpg'});
      //  var blobURL = URL.createObjectURL(blob);
      //  window.open(blobURL);
    }
    else {
      global.hideLoader();
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }

  }

  async downloadFile(docId) {
    global.showLoader();
    let response = (await this.BillTrackingService.downloadFile(docId))
    if ((response != undefined) && (response.status == 200)) {
      // var arrBuffer = this.base64ToArrayBuffer(base64Data);
      var blob = new Blob([response.data], { type: 'application/pdf' });
      var blobURL = URL.createObjectURL(blob);
      window.open(blobURL);
      // const url =  window.URL.createObjectURL(new Blob([response.data], { type: 'application/pdf' }));
      // // const url = window.URL.createObjectURL(new Blob([response.data]));
      // const link = document.createElement('a');
      // link.href = url;
      // link.setAttribute('download', 'abhinav.pdf'); //or any other extension
      // document.body.appendChild(link);
      // link.click();
      global.hideLoader();
    }
    else {
      global.hideLoader();
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }

  }

  async getDebitNoteDetails(btNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.getBtDebitNoteInfo(btNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let debitInfo = response.data.data;
      this.setState({ debitNoteDetails: debitInfo })
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));
    }

  }

  async getPreviouslyGeneratedDebitNote(btNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.getPreviousDebitNote(btNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      let debitInfo = response.data.data;
      this.setState({ previousDebitNotes: debitInfo })
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));
    }
  }

  async generateOrUpdateDebitNote(debitNotes) {
    global.showLoader();
    let response = (await this.BillTrackingService.createDebitNote(debitNotes))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Debit Note Generated Successfully!", "Success"));
      this.getPreviouslyGeneratedDebitNote(this.state.selectedRowBtGrid.btNumber);
      this.getDebitNoteDetails(this.state.selectedRowBtGrid.btNumber);
    }
    else if((response != undefined) && (response.data.status == "fail")){
      global.hideLoader();
      this.toastObj.show(this.createToaster(response.data.data.message, "Error"));
      this.getPreviouslyGeneratedDebitNote(this.state.selectedRowBtGrid.btNumber);
      this.getDebitNoteDetails(this.state.selectedRowBtGrid.btNumber);
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));
    }

  }

  async deleteDebitNote(debitNoteNumber) {
    global.showLoader();
    let response = (await this.BillTrackingService.deleteDebitNote(debitNoteNumber))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Debit Note Deleted Successfully!", "Success"));
      this.getPreviouslyGeneratedDebitNote(this.state.selectedRowBtGrid.btNumber);
      this.getDebitNoteDetails(this.state.selectedRowBtGrid.btNumber);
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));
    }

  }

  async deleteDebitNoteById(debitNoteId) {
    global.showLoader();
    let response = (await this.BillTrackingService.deleteDebitNoteById(debitNoteId))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Debit Note Deleted Successfully!", "Success"));
      this.getPreviouslyGeneratedDebitNote(this.state.selectedRowBtGrid.btNumber);
      this.getDebitNoteDetails(this.state.selectedRowBtGrid.btNumber);
    }
    else {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Something Went Wrong", "Error"));
    }

  }

  base64ToArrayBuffer(data) {
    var binaryString = window.atob(data);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++) {
      var ascii = binaryString.charCodeAt(i);
      bytes[i] = ascii;
    }
    return bytes;
  };

  async saveCommentsAndReviewApi(processDto, status) {
    global.showLoader();
    let response = (await this.BillTrackingService.saveCommentsAndReview(processDto))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      this.toastObj.show(this.createToaster(status == "C" ? "Request Approved Successfully!" : "Request Review Successfully!", "Success"));
      this.getBtProcessStatus(this.state.selectedRowBtGrid.btNumber);
      this.getPoBtHistory(this.state.selectedRowBtGrid.btNumber);
      this.getBtList();
      this.closebillTrackingDialog();
      this.closeReviewDialog();
    }
    else {
      global.hideLoader();
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }

  }

  async saveBtHistory(btHistoryDto, status) {
    global.showLoader();
    let response = (await this.BillTrackingService.saveBtHistory(btHistoryDto))
    if ((response != undefined) && (response.data.status == "success")) {
      global.hideLoader();
      this.toastObj.show(this.createToaster("Request saved SuccessFully", "Success"));
      if (status == "C") {
        this.saveCommentsAndReview(status, null);
      }
      else if (status == "R") {
        this.saveCommentsAndReview(status, this.state.selectedReviewProcess);
      }
      this.getBtProcessStatus(this.state.selectedRowBtGrid.btNumber);
      this.getPoBtHistory(this.state.selectedRowBtGrid.btNumber);
      this.getBtList();
      this.closebillTrackingDialog();
      this.closeReviewDialog();
    }
    else {
      global.hideLoader();
      this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    }

  }

  viewReport = async (args) => {

    this.setState({ pdfInfo: [], pdf: false, imageInfo: [], image: false })
    // this.downloadReport(args.rowData.docId)
    if (args.rowData.documentId != null) {
      this.downloadReport(args.rowData.documentId)
    }
    else {
      this.toastObj.show({ title: 'Information!', content: 'Report File Not Found', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });

    }
  }


  onFileChanges = (event) => {
    let that = this;
    that.state.fileModel = new FormData()
    that.state.fileModel.append('file', event.target.files[0])
    that.setState({ docId: null, docName: null });
    that.uploadFiles();
  }

  onChangeFilterMultiSelect = (event) => {
    // if (event.e != undefined && event.e != null)
    this.setState({ [event.element.id]: event.value })
  }

  async uploadFiles() {
    global.showLoader();
    let resp = (await this.BillTrackingService.uploadFile(this.state.fileModel))
    if (resp.data.status == "success") {
      this.setState({ docId: resp.data.data.documentId, docName: resp.data.data.fileName });
      global.hideLoader();
      this.toastObj.show(this.createToaster("File uploaded Successfully!!", "Success"));
    }
  }

  search = () => {
    let that = this;
    const { selectedBtNumber, selectedDepartment, selectedPoNumber, selectedPotype, selectedGrnNumber, selectedStatus, selectedGrnDate, selectedAssignee, selectedLotNumber } = that.state;
    that.btGrid.clearFiltering();
    if (selectedBtNumber.length == 0 && selectedDepartment.length == 0 && selectedPoNumber.length == 0 && selectedGrnNumber.length == 0
      && selectedPotype.length == 0 && selectedStatus.length == 0 && selectedGrnDate.length == 0 && selectedAssignee.length == 0 && selectedLotNumber.length == 0) {
      that.toastObj.show(this.createToaster("Please select at least one filter", "Information"));
    } else {
      if (selectedBtNumber.length > 0) {
        that.btGrid.filterByColumn("btNumber", "equal", selectedBtNumber);
      }
      if (selectedDepartment.length > 0) {
        that.btGrid.filterByColumn("pendingOnDepartment", "equal", selectedDepartment);
      }
      if (selectedPoNumber.length > 0) {
        that.btGrid.filterByColumn("poNumber", "equal", selectedPoNumber);
      }
      if (selectedPotype.length > 0) {
        let poObj = that.state.btInfoList.filter(function (item) {
          return item.poType == selectedPotype;
        })
        that.btGrid.filterByColumn("poTypeId", "equal", poObj[0].poTypeId);
      }
      if (selectedGrnNumber.length > 0) {
        that.btGrid.filterByColumn("grnNumber", "equal", selectedGrnNumber);
      }
      if (selectedStatus.length > 0) {
        that.btGrid.filterByColumn("btStatus", "equal", selectedStatus);
      }
      if (selectedGrnDate.length > 0) {
        that.btGrid.filterByColumn("grnDate", "equal", selectedGrnDate);
      }
      if (selectedAssignee.length > 0) {
        that.btGrid.filterByColumn("assigneeName", "equal", selectedAssignee);
      }
      if (selectedLotNumber.length > 0) {
        that.btGrid.filterByColumn("lotNumber", "equal", selectedLotNumber);
      }
      that.toastObj.show(this.createToaster("Filter Applied Successfully!", "Information"));
    }
  }

  resetFilter = () => {
    let that = this;
    that.setState({
      selectedBtNumber: [], selectedGrnNumber: [], selectedDepartment: [], selectedPoType: [], selectedPoNumber: [],
      selectedStatus: [], selectedGrnDate: [], selectedAssignee: [], selectedLotNumber: []
    });
    that.btGrid.clearFiltering();
    that.toastObj.show(this.createToaster("Filter Reset Successfully!", "Information"));
  }

  rowSelectedBtGrid = (args) => {
    let that = this;
    that.setState({ selectedRowBtGrid: args.data });
  }

  onClickReview = () => {
    let that = this;
    this.getBtReviewProcess(that.state.selectedRowBtGrid.btNumber, this.state.currentProcessId);
  }

  onReview = () => {
    let that = this;
    if (this.state.selectedReviewProcess != null) {
      //  that.saveCommentsAndReview("R",this.state.selectedReviewProcess);
      that.onSaveForm("R");
    }
    else {
      this.toastObj.show({ title: 'Information!', content: 'Please Select Process!!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    }
  }

  onClickApprove = () => {
    let that = this;
    //  that.saveCommentsAndReview("C",null);
    that.onSaveForm("C");
  }

  onClickSave = () => {
    let that = this;
    that.onSaveForm("P");
  }


  onSaveForm = (status) => {
    let that = this;
    let comments = that.commentsObj.respectiveElement.value;
    let docModel = [];
    if (that.state.docId != null && that.state.docName != null) {
      docModel.push(new DocumentModel(that.state.docId, that.state.docName));
    }
    let saveHistory = new BtHistoryModel(null, comments, docModel, that.state.currentProcessName, null, null, that.state.currentProcessId, status == "C" ? "Passed" : "", that.state.selectedRowBtGrid.btNumber);
    that.resetCommentsAndDoc();
    that.saveBtHistory(saveHistory, status);
    // that.saveCommentsAndReview("P",null);
  }

  resetCommentsAndDoc() {
    let that = this;
    that.fileInput.value = '';
    that.commentsObj.respectiveElement.value = '';
  }

  saveCommentsAndReview = (status, reviewProcessId) => {
    let that = this;
    let saveInfo = new ApproveReviewModel(that.state.selectedRowBtGrid.btNumber, that.state.currentProcessId, reviewProcessId, status);
    this.saveCommentsAndReviewApi(saveInfo, status);
  }

  debitNoteByLineItemCommandClick = (args) => {
    let that = this;
    if (args.requestType == "save") {
      let updatedDebitNoteInfo = [];
      updatedDebitNoteInfo.push(new DebitNoteModel(args.data.btNumber, args.data.debitNoteNumber, args.data.invoiceNumber, args.data.invoiceType,
        args.data.poLineItem, args.data.poLineItemDesc, args.data.totalDebitAmount, args.data.totalDebitQuantity, args.data.totalGRNQty, args.data.totalGrnAmount))
      that.generateOrUpdateDebitNote(updatedDebitNoteInfo);
    }
    else  if (args.requestType == "delete") {
      that.deleteDebitNoteById(args.data[0].debitNoteId);
    }
  }

  previousDebitNoteCommandClick = (args) => {
    if (args.name == "commandClick") {
        if (args.commandColumn.type == "Delete") {
           this.deleteDebitNote(args.rowData.debitNoteNumber);
        }
        if (args.commandColumn.type == "SaveSap") {
         
        }
    }
}

  typetemplate(props) {
    return (
      <div class="description-content">
        {props.poTypeId}
        <div class="overlay-description"> {props.poType}</div>
      </div>
    )
  }

  grptemplate(props) {
    return (
      <div class="description-content">
        {props.poGroupId}
        <div class="overlay-description"> {props.poGroup}</div>
      </div>
    )
  }

  onChangeProcess = (event) => {
    this.setState({ selectedReviewProcess: event.value })

  }
  buttonTemplate = (args) => {
    return (<div><a onClick={this.invoicedialogBox}> View </a></div>);
  }
  showPDFDialog = () => {
    this.setState({ pdfViewDialog: true });
  }

  closeDialog = () => {
    this.setState({ pdfViewDialog: false });
  }

  debitNoteDetailCloseDialog = () => {
    this.setState({ debitNoteDetailsDialog: false });
  }


  filterBoxOpen = () => {
    this.setState({
      filterBox: !this.state.filterBox
    });
  }
  customAccordion = () => {
    this.setState({
      accordionbox: !this.state.accordionbox
    });
    if (this.state.accordionbox) {
      this.setState({
        collapedClass: 'heading open',
        // accordionbox: false,
      })
    }
    else {
      this.setState({
        collapedClass: 'heading closeed',
        // accordionbox: true,
      })
    }
  }

  showActionHistory = () => {
    this.setState({

    });
  }

  refreshGrid = () => {
    global.showLoader();
    let that = this;
    that.getBtList();
    // global.hideLoader();
  }

  viewFile(value) {
    this.setState({ pdfInfo: [], pdf: false, imageInfo: [], image: false })
    this.downloadReport(value.docId);
    // if(value.fileName.includes('.pdf')){
    // this.downloadReport(value.docId);
    // }
    // else if(value.fileName.includes('.jpg')){
    //   this.downloadReport(value.docId);
    // }
    // else{
    //   this.toastObj.show(this.createToaster("Not Supported!!", "Information"));
    // }
  }

  viewDebitNoteInfo(value) {
    this.setState({ previousGenerateddebitNoteDetails: value.btDebitNoteGRNDTOList, debitNoteDetailsDialog: true, selectedDebitNoteNumber: value.debitNoteNumber })

    console.log(value);
  }
  // grn status start template 
  udTemplate(props) {
    console.log(props.ud)
    return (<div className='deptpass'>
      {/* <span className={props.ud==true+'active'}>as</span>  */}
      <span className='false'></span>
    </div>);
  }
  depatTemplate(props) {
    if (props.column.field == "ud")
      return (<div className='deptpass'><span className={props.ud}> </span></div>);
    else if (props.column.field == "accounts")
      return (<div className='deptpass'><span className={props.accounts}> </span></div>);
    else if (props.column.field == "bvt")
      return (<div className='deptpass'><span className={props.bvt}> </span></div>);
    else if (props.column.field == "hod1")
      return (<div className='deptpass'><span className={props.hod1}> </span></div>);
    else if (props.column.field == "hod2")
      return (<div className='deptpass'><span className={props.hod2}> </span></div>);
    else if (props.column.field == "audit")
      return (<div className='deptpass'><span className={props.audit}> </span></div>);
    else if (props.column.field == "management")
      return (<div className='deptpass'><span className={props.management}> </span></div>);
  }

  documentsTemplate = (props) => {
    let documentArray = [];
    for (const [index, value] of props.btHistoryDocDTOList.entries()) {
      if (value.fileName != null)
        documentArray.push(<a key={index} > <i className='fa fa-file-pdf-o' onClick={() => { this.viewFile(value) }}> {value.fileName}</i> </a>)
    }
    return (
      <div className='deptpass' >
        {documentArray}
      </div>)
  }


  debitNoteTemplate = (props) => {
    return (
      <a onClick={() => { this.viewDebitNoteInfo(props) }}><u>{props.debitNoteNumber}</u></a>
    )
  }



  decisionTemplate = (props) => {
    return (<div className='decision'><span className={props.decision ? "true" : "false"}> </span></div>);
  }
  billTrackingDialog = (event, selectedDialog) => {

    // if (selectedDialog.processName == 'UD') {
    //   this.setState({ divHidden: 'udForm', formHeading: 'UD' })
    // }
    // else if (selectedDialog.processName == 'Accounts') {
    //   this.getBtProcessHistory(this.state.selectedRowBtGrid.btNumber,selectedDialog.processId);
    //   this.setState({ divHidden: 'accountsForm', formHeading: 'Accounts' })
    // }
    // else if (selectedDialog.processName  == 'BVT') {
    //   this.setState({ divHidden: 'bvtForm', formHeading: 'BVT' })
    // }
    // else if (selectedDialog.processName  == 'HOD1') {
    //   this.setState({ divHidden: 'hod1Form', formHeading: 'HOD 1' })
    // }
    // else if (selectedDialog.processName  == 'HOD2') {
    //   this.setState({ divHidden: 'hod2Form', formHeading: 'HOD 2' })
    // }
    // else if (selectedDialog.processName  == 'Audit') {
    //   this.setState({ divHidden: 'auditForm', formHeading: 'Audit' })
    // }
    // else if (selectedDialog.processName  == 'Management') {
    //   this.setState({ divHidden: 'managementForm', formHeading: 'Management' })
    // }
    if (selectedDialog.status == "P" && selectedDialog.isformOpen) {
      this.setState({ billTrackingDialogBox: true, currentProcessId: selectedDialog.processId, currentProcessName: selectedDialog.processName, currentProcessEditable: selectedDialog.editableOrNot });

      this.getBtProcessHistory(this.state.selectedRowBtGrid.btNumber, selectedDialog.processId, selectedDialog.processName);
      this.getDebitNoteDetails(this.state.selectedRowBtGrid.btNumber);
      this.getPreviouslyGeneratedDebitNote(this.state.selectedRowBtGrid.btNumber)
      // this.getBtMiroInfo(this.state.selectedRowBtGrid.btNumber);
    }

  }

  viewallDocuments = () => {
    this.getAllDocuments(this.state.selectedRowBtGrid.btNumber);
    this.setState({ viewallDocumentsDialogBox: !this.state.viewallDocumentsDialogBox });
  }

  viewallApprovedDocuments = () => {
    this.setState({ viewBillApprovedDialogBox: !this.state.viewBillApprovedDialogBox });
  }

  generateDebitNote = () => {
    let that = this;
    that.generateOrUpdateDebitNote(that.debitNoteGrid.currentViewData);

    // this.setState({ debitNoteGrid: !this.state.debitNoteGrid });

  }
  invoicedialogBox = () => {
    let that = this;
    that.getGrnInvoiceData(that.state.selectedRowBtGrid.btNumber);
    that.getPoData(that.state.selectedRowBtGrid.btNumber);
    that.getPoBtHistory(that.state.selectedRowBtGrid.btNumber);
    that.getBtProcessStatus(that.state.selectedRowBtGrid.btNumber);
    this.setState({ invoicedialogDialogBox: true });
  }

  closeInvoicedialogBox = () => {
    this.setState({ invoicedialogDialogBox: false, docId: null, docName: null });
  }


  onCommandClick = (args) => {
    console.log(args);
    if (args.commandColumn.buttonOption.type == 'Bill Approved') {
      console.log('d');
    }
    else if (args.commandColumn.buttonOption.type == 'Bill Created') {
      console.log('12');
    }
    else if (args.commandColumn.type == 'View') {
      this.invoicedialogBox();
    }
  }
  closeReviewDialog = () => {
    this.clearDropDowns();
  }

  clearDropDowns = () => {
    this.setState({ selectedReviewProcess: null, reviewDialogBox: false, btReviewProcess: [] });
  }
  opengrnBox = () => {

    this.setState({ grnBox: !this.state.grnBox });
  }
  closebillTrackingDialog = () => {
    this.setState({ billTrackingDialogBox: false, docId: null, docName: null });

  }

  render() {
    const { t, i18n } = this.props;
    return (
      <div className="container-fluid-inner billtracking">
        <h1 className='pagesheading'>Bill Tracking</h1>
        <div className='clearfix' ></div>
        <Button color="primary" size="sm" className='float-right' onClick={this.refreshGrid}>Refresh</Button>
        {/* -------------------------------------- filter  --------------------------------------  */}

        <a className={this.state.filterBox ? 'filter-icon-open' : 'filter-icon-close'} onClick={this.filterBoxOpen}>Filter <i className='fa  fa-filter'></i></a>
        <div className='filter-dropdown' hidden={this.state.filterBox}>
          <div className='filter-Selection'>
            <h3>PO Info</h3>
            <Row>
              {/* <Col sm='3'>
                <DropDownListComponent dataSource={this.state.uniquePoNumber} fields={this.state.pofields} placeholder={t("cpm.lm.line.load.filter.po")}
                  color="primary" id="pio" allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              <Col sm='2'>  <MultiSelectComponent id="selectedPoNumber" ref={(scope) => { this.selectedPoNumberObj = scope; }} dataSource={this.state.uniquePoNumber}
                value={this.state.selectedPoNumber} change={this.onChangeFilterMultiSelect} fields={{ text: 'poNumber', value: 'poNumber' }}
                placeholder={"Select Po "} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search po" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              {/* <Col sm='3'>
                <DropDownListComponent dataSource={this.state.potype} fields={this.state.potypefields} placeholder={t("cpm.lm.line.load.filter.poType")}
                  color="primary" id="potype" allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              <Col sm='2'>  <MultiSelectComponent id="selectedPotype" ref={(scope) => { this.selectedPoTypeObj = scope; }} dataSource={this.state.uniquePoType}
                value={this.state.selectedPoType} change={this.onChangeFilterMultiSelect} fields={{ text: 'poType', value: 'poType' }}
                placeholder={"Select Po Type "} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search po type" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
            </Row>
          </div>
          <div className='filter-Selection'>
            <h3>GRN</h3>
            <Row>
              {/* <Col sm='3'>
                <DropDownListComponent placeholder={t("cpm.lm.line.load.filter.grn")} dataSource={this.state.grnDropdown} fields={this.state.grnfields}
                  color="primary" id="grn" allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              <Col sm='2'>  <MultiSelectComponent id="selectedGrnNumber" ref={(scope) => { this.selectedGrnObj = scope; }} dataSource={this.state.uniqueGrnNumber}
                value={this.state.selectedGrnNumber} change={this.onChangeFilterMultiSelect} fields={{ text: 'grnNumber', value: 'grnNumber' }}
                placeholder={"Select Grn Number "} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Grn Number" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              {/* <Col sm='3'>
                <DropDownListComponent placeholder={t("cpm.lm.line.load.filter.bt")} dataSource={this.state.btnDropdown} fields={this.state.btnfields}
                  color="primary" id="bt" allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              <Col sm='2'>  <MultiSelectComponent id="selectedBtNumber" dataSource={this.state.uniqueBtNumber}
                value={this.state.selectedBtNumber} change={this.onChangeFilterMultiSelect} fields={{ text: 'btNumber', value: 'btNumber' }}
                placeholder={"Select Bt Number "} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Bt Number" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              {/* <Col sm='3'>
                <DateRangePickerComponent id="grnDateRang" format='dd/MM/yy' startDate={this.state.startDate} endDate={this.state.endDate} floatLabelType="Auto" placeholder={t("cpm.lm.line.load.filter.grnDateRang")}  ></DateRangePickerComponent>
              </Col> */}
              <Col sm='2'>  <MultiSelectComponent id="selectedGrnDate" dataSource={this.state.uniqueGrnDate}
                value={this.state.selectedGrnDate} change={this.onChangeFilterMultiSelect} fields={{ text: 'grnDate', value: 'grnDate' }}
                placeholder={"Select Grn Date"} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search Grn Date" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
            </Row>
          </div>
          <div className='filter-Selection'>
            <h3>Decision</h3>
            <Row>
              <Col sm='2'>  <MultiSelectComponent id="selectedDepartment" dataSource={this.state.uniqueDept}
                value={this.state.selectedDepartment} change={this.onChangeFilterMultiSelect} fields={{ text: 'pendingOnDepartment', value: 'pendingOnDepartment' }}
                placeholder={"Select department "} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search department" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              {/* <Col sm='3'>
                <DropDownListComponent dataSource={this.state.decisionWiseDropdown} fields={this.state.decisionWisefields} placeholder={t("cpm.lm.line.load.filter.decisionWise")}
                  color="primary" id="decisionWise" allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              {/* <Col sm='2'>  <MultiSelectComponent id="selectedDecision" dataSource={this.state.decisionWiseDropdown} fields={this.state.decisionWisefields} 
                value={this.state.selectedDecision} change={this.onChangeFilterMultiSelect} 
                placeholder={t("cpm.lm.line.load.filter.decisionWise")} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder={t("cpm.lm.line.load.filter.decisionWise")} popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col> */}
              {/* <Col sm='3'>
                <DropDownListComponent dataSource={this.state.statusDropdown} fields={this.state.statusfields} placeholder='Status'
                  color="primary" id="decisionWise" allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              <Col sm='2'>  <MultiSelectComponent id="selectedStatus" dataSource={this.state.uniqueStatus}
                value={this.state.selectedStatus} change={this.onChangeFilterMultiSelect} fields={{ text: 'btStatus', value: 'btStatus' }}
                placeholder={"Select status "} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search status" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>

              <Col sm='2'>  <MultiSelectComponent id="selectedAssignee" dataSource={this.state.uniqueAssignee}
                value={this.state.selectedAssignee} change={this.onChangeFilterMultiSelect} fields={{ text: 'assigneeName', value: 'assigneeName' }}
                placeholder={"Select Assignee "} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search assignee" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>

              <Col sm='2'>  <MultiSelectComponent id="selectedLotNumber" dataSource={this.state.uniqueLotNumber}
                value={this.state.selectedLotNumber} change={this.onChangeFilterMultiSelect} fields={{ text: 'lotNumber', value: 'lotNumber' }}
                placeholder={"Select  Lot Number "} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder="Search lot number" popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
            </Row>
          </div>
          <Button color="primary" size="sm" className='mt-2 mb-2' onClick={this.search}>Apply filter</Button>
          <Button color="primary" size="sm" id="resetOrders" onClick={this.resetFilter} className="ml-2">{"Reset Filter"}</Button>


        </div>



        {/* -------------------------------------- filter  --------------------------------------  */}
        <div className='mt-2'></div>

        <GridComponent gridLines='Both' allowReordering={true} commandClick={this.onCommandClick} allowTextWrap={true} rowHeight={30} height={this.state.windowsHeight} allowPaging={true} allowResizing={true}
          allowSorting={false} ref={grid => this.btGrid = grid} rowSelected={this.rowSelectedBtGrid} dataSource={this.state.btInfoList} toolbar={this.toolbarOptions} showColumnChooser={true}
          filterSettings={this.state.FilterSettingsModel} allowFiltering={true} commandClick={this.onCommandClick} pageSettings={AppConfig.pageSettings}>
          <ColumnsDirective>
            <ColumnDirective field='invoiceNumber' filter={this.state.IFilter} headerText='Invoice #' width='120' />
            <ColumnDirective field='invoiceDate' headerText='Invoice Date' filter={this.state.IFilter} width='150' />
            <ColumnDirective field='vendorName' filter={this.state.IFilter} headerText='Vendor Name' width='150' />
            <ColumnDirective field='grnNumber' filter={this.state.IFilter} headerText='GRN #' width='110' />
            <ColumnDirective field='grnDate' filter={this.state.IFilter} headerText='GRN Date' width='120' />
            <ColumnDirective field='grnQty' filter={this.state.IFilter} headerText='GRN Qty' width='120' />
            <ColumnDirective field='lotNumber' headerText='Lot Number' filter={this.state.IFilter} width='150' />
            <ColumnDirective field='plantName' headerText='Plant Name' filter={this.state.IFilter} width='150' />
            <ColumnDirective field='totalValue' headerText='Total Value' filter={this.state.IFilter} width='150' />
            <ColumnDirective field='btNumber' filter={this.state.IFilter} headerText='BT #' width='120' />
            <ColumnDirective field='btStatus' filter={this.state.IFilter} headerText='BT Status' width='120' />

            <ColumnDirective field='remark' filter={this.state.IFilter} headerText='Remark' width='120' />

            <ColumnDirective field='pendingOnDepartment' filter={this.state.IFilter} headerText='Department' width='160' />
            <ColumnDirective field='assigneeName' filter={this.state.IFilter} headerText='Assignee' width='120' />
            <ColumnDirective field='poNumber' filter={this.state.IFilter} headerText='PO #' width='110' />
            <ColumnDirective field='poTypeId' filter={this.state.IFilter} template={this.typetemplate} headerText='PO Type' width='120' />
            <ColumnDirective field='poGroupId' filter={this.state.IFilter} headerText='PO Group' template={this.grptemplate} width='120' />
            {/* <ColumnDirective field='decision'  filter={this.state.IFilter} headerText='Decision' template={this.buttonTemplate} width='100' /> */}
            <ColumnDirective field='action' headerText='Action' textAlign="Light" commands={this.commandsInvocieGrid} width='110' />
          </ColumnsDirective>
          <Inject services={[DetailRow, Group, Page, Reorder, Resize, Sort, CommandColumn, Filter, Toolbar, ColumnChooser]} />
        </GridComponent>
        {/* ------------- ---------------------- invoice dialog ------------- ---------------------- */}
        <DialogComponent allowDragging={true} id="modalDialog" isModal={true} width='90%' visible={this.state.invoicedialogDialogBox} showCloseIcon={true} header={this.state.selectedRowBtGrid != null ? 'Invoice Number - ' + this.state.selectedRowBtGrid.invoiceNumber : ""} close={this.closeInvoicedialogBox}>
          <div>
            <Row>
              <Col sm='6'>
                <b>Grn Number:</b>{this.state.selectedRowBtGrid != null ? this.state.selectedRowBtGrid.grnNumber : ""}
                <b className='pl-3'>Grn Date:</b>{this.state.selectedRowBtGrid != null ? this.state.selectedRowBtGrid.grnDate : ""}
                <b className='pl-3'>Total Grn Qty:</b> {this.state.selectedRowBtGrid != null ? this.state.selectedRowBtGrid.grnQty : ""}
                {/* <b className='pl-3'>PO Type</b> {this.state.selectedRowBtGrid != null ? this.state.selectedRowBtGrid.poType : ""} */}
              </Col>
              <Col sm='6' className='text-right'> </Col>
            </Row>
            <div className='mt-1'></div>
            <GridComponent dataSource={this.state.btGrnInvoiceInfoList} gridLines='Both' rowHeight='25' allowPaging={true} allowResizing={true} allowSorting={true}>
              <ColumnsDirective>
                <ColumnDirective field='lineItem' headerText='Line item #' width='100' />
                <ColumnDirective field='lineItemDesc' headerText='Line Description' width='250' />
                <ColumnDirective field='uom' headerText='UOM' />
                <ColumnDirective field='grnQty' headerText='Qty' />
                <ColumnDirective field='invoiceValue' headerText='Invoice Value' />
                <ColumnDirective field='invoiceCurrency' headerText='Invoice Currency' />
              </ColumnsDirective>
              <Inject services={[DetailRow, Page, Resize, Sort]} />
            </GridComponent>
            <div className='mt-3'></div>
            <Card>
              <CardTitle onClick={this.opengrnBox} className={this.state.grnBox == true ? 'grnTitleOpen' : 'grnTitleClose'}>
                PO number- {this.state.selectedRowBtGrid != null ? this.state.selectedRowBtGrid.poNumber : ""}
                {/* Date: 12/06/2020 Qty: 250   */}
              </CardTitle>
              <CardBody hidden={this.state.grnBox}>
                <CardText>
                  <Row>
                    <Col sm='6'>
                      <b>Po Delivery Date:</b> {this.state.poList != null ? this.state.poList.poDeliverDate : ""}
                      <b className='pl-3'>Total Po Qty:</b> {this.state.poList != null ? this.state.poList.totalPoQty : ""}
                      <b className='pl-3'>PO Status</b> {this.state.poList != null ? this.state.poList.status : ""}
                    </Col>
                    <Col sm='6' className='text-right'>
                      <b className='pl-3'>Vendor Name:</b>{this.state.poList != null ? this.state.poList.vendorName : ""}     <b className='pl-3'>Score:</b> {this.state.poList != null ? this.state.poList.score : ""}
                    </Col>
                  </Row>
                  <br />
                  <GridComponent gridLines='Both' allowTextWrap={true} rowHeight='25' allowPaging={true} allowResizing={true} allowSorting={true} dataSource={this.state.btPoInfoList}  >
                    <ColumnsDirective>
                      <ColumnDirective field='poLineItem' headerText='Line item #' width='100' />
                      <ColumnDirective field='pOLineDescription' headerText='Line Description' width='300' />
                      <ColumnDirective field='Uom' headerText='UOM' width='70' />
                      <ColumnDirective field='poQty' headerText='PO Qty' width='90' />
                      <ColumnDirective field='pcdDate' headerText='PCD Date' width='90' />
                      <ColumnDirective field='pcdQty' headerText='PCD Qty' width='90' />
                      <ColumnDirective field='grnTotQty' headerText='GRN Qty' width='90' />
                      <ColumnDirective field='returnQty' headerText='Return Qty' width='90' />
                      <ColumnDirective field='balanceQty' headerText='Balance Qty' width='110' />
                    </ColumnsDirective>
                    <Inject services={[DetailRow, Group, Page, Resize, Sort]} />

                  </GridComponent>
                  <div className="billtracking-track">
                    <ul className="process-model">
                      <li className="first">
                        <i>#</i>
                        <p>{this.state.selectedRowBtGrid != null ? this.state.selectedRowBtGrid.btNumber : ""}</p>
                      </li>
                      {this.state.processFlowName}
                      {/* <li className="pass" onClick={(e) => { this.billTrackingDialog(e, "ud") }}> <i class="fa fa-check"></i> <p>UD</p></li> */}
                      {/* <li className="pass" onClick={(e) => { this.billTrackingDialog(e, "accounts") }}>
                        <i class="fa fa-check"></i>
                        <p>Accounts</p>
                      </li>
                      <li className="pass" onClick={(e) => { this.billTrackingDialog(e, "bvt") }}>
                        <i class="fa fa-check"></i>
                        <p>BVT</p>
                      </li>
                      <li className='current'>
                        <i class="fa fa-hourglass-half"></i>
                        <p>HOD1</p>
                      </li>
                      <li>
                        <i class="fa fa-long-arrow-right"></i>
                        <p>HOD2</p>
                      </li>
                      <li>
                        <i class="fa fa-long-arrow-right"></i>
                        <p>Audit</p>
                      </li>
                      <li>
                        <i class="fa fa-long-arrow-right"></i>
                        <p>Management</p>
                      </li> */}
                    </ul>
                  </div>
                  <div className='clearfix'></div>
                  <h6 className='mt-1' onClick={this.showActionHistory}>Action History</h6>
                  <GridComponent allowFiltering={true} allowSorting={true} hidden={this.state.showActionHistorySection} gridLines='Both' rowHeight='25' allowResizing={true} allowSorting={true} allowFiltering={false} dataSource={this.state.btHistoryList} height={150} pageSettings={{ pageCount: 5 }} allowGrouping={true} groupSettings={this.grnLineGridn} allowSorting={true}
                  >
                    <ColumnsDirective>
                      <ColumnDirective field='btProcessName' headerText='Department' width='100' />
                      <ColumnDirective field='comments' headerText='Comments' width='125' />
                      <ColumnDirective field='date' headerText='Date' width='135' />
                      {/* <ColumnDirective field='decision' headerText='Decision' template={this.decisionTemplate} width='100' /> */}
                      <ColumnDirective field='status' headerText='Decision' width='100' />

                      <ColumnDirective field='userName' headerText='User Name' width='130' />
                      <ColumnDirective field='documents' headerText='Documents' template={this.documentsTemplate} width='130' />
                    </ColumnsDirective>
                    <Inject services={[DetailRow, Page, Group, Resize, Sort]} />
                  </GridComponent>
                  <Button color="primary" size="sm" className='mt-3 mb-2 mr-2' onClick={(e) => { this.viewallDocuments() }}>View all Documents</Button>
                  <Button color="primary" size="sm" className='mt-3 mb-2' disabled onClick={(e) => { this.viewallApprovedDocuments() }} >View Bill Approved Document</Button>
                </CardText>
              </CardBody>
            </Card>
            {/* <Card>
              <CardTitle className='grnTitleOpen'>
                PO1234 Date: 12/06/2020 Qty: 250
                      </CardTitle>
            </Card> */}
          </div>
        </DialogComponent>
        {/* ------------- ---------------------- invoice dialog ------------- ---------------------- */}


        {/* ------------- ---------------------- View all Documents ------------- ---------------------- */}
        <DialogComponent allowDragging={true} id="modalDialog" isModal={true} width='30%' visible={this.state.viewallDocumentsDialogBox} showCloseIcon={true} header='View all Documents' close={this.viewallDocuments}>
          <div>
            {/* <dl>
              <dt><a href=''>Document 1</a><br /></dt>
              <dt><a href=''>Document 2</a><br /></dt>
              <dt><a href=''>Document 3</a><br /></dt>
            </dl> */
              <GridComponent gridLines='Both' allowResizing={true} allowTextWrap={false} allowSelection={true} rowHeight={35}
                dataSource={this.state.documentsList} enableHover={true}
                allowSorting={true} allowPaging={true} commandClick={this.viewReport} >
                <ColumnsDirective>
                  <ColumnDirective field='departmentName' headerText='Department Name' width='130' />

                  <ColumnDirective field='documentName' headerText='Document Name' width='130' />
                  <ColumnDirective headerText='Action' textAlign='Left' commands={this.viewIcon} width='85' />
                </ColumnsDirective>
                <Inject services={[Sort, Selection, Resize, Page, Group, CommandColumn]} />
              </GridComponent>}
          </div>

        </DialogComponent>
        {/* ------------- ---------------------- View all Documents ------------- ---------------------- */}

        {/* ------------- ---------------------- Review Dialog ------------- ---------------------- */}
        <DialogComponent allowDragging={true} id="modalDialog" isModal={true} width='30%' visible={this.state.reviewDialogBox} showCloseIcon={true} header='Review' close={this.closeReviewDialog}>
          <div>
            <Row>
              <Col sm='4'>
                <DropDownListComponent id="selectedRole" placeholder="Select Process" floatLabelType="Auto" floatLabelType="Auto" filterType="Contains" fields={{ text: 'processName', value: 'processId' }}
                  dataSource={this.state.btReviewProcess} popupHeight="300px" allowFiltering={true} change={this.onChangeProcess} /></Col>
            </Row>
            <div className='mt-3'></div>
            <Button color="primary" size="sm" className='mr-2' onClick={this.onReview} >Review</Button>
            <Button color="primary" size="sm" onClick={this.clearDropDowns} >Cancel</Button>


          </div>

        </DialogComponent>
        {/* ------------- ---------------------- View all Documents ------------- ---------------------- */}

        {/* ------------- ---------------------- View Bill Approved Document  ------------- ---------------------- */}
        <DialogComponent allowDragging={true} id="modalDialog" isModal={true} width='30%' visible={this.state.viewBillApprovedDialogBox} showCloseIcon={true} header='View all Approved Documents' close={this.viewallApprovedDocuments}>
          <div>
            {/* <dl>
              <dt><a href=''>Document 1</a><br /></dt>
              <dt><a href=''>Document 2</a><br /></dt>
              <dt><a href=''>Document 3</a><br /></dt>
            </dl> */}
          </div>

        </DialogComponent>
        {/* ------------- ---------------------- View all Documents ------------- ---------------------- */}



        {/*  ------------- ---------------------- GRN Form dialog ---------------------- --------------- */}
        <DialogComponent allowDragging={true} id="modalDialog" isModal={true} width='80%' visible={this.state.billTrackingDialogBox} showCloseIcon={true} header={this.state.formHeading} close={this.closebillTrackingDialog}>
          {/* ----------------------------- UD Form  -----------------------------*/}
          {this.state.divHidden == 'udForm' ?
            <div className='pl-3 pr-3'>
              <Row>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="GRN Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="BT Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
              </Row>
              <div className='mt-2'></div>
              <GridComponent gridLines='Both' rowHeight='25' gridLines='Both' dataSource={this.state.grnGridInfo} allowSorting={true}>
                <ColumnsDirective>
                  <ColumnDirective field='documentid' headerText='Document #' width='100' />
                  <ColumnDirective field='lineitem' headerText='Line item' width='125' />
                  <ColumnDirective field='liDescription' headerText='LI Description' width='150' />
                  <ColumnDirective field='uom' headerText='Uom' width='135' />
                  <ColumnDirective field='totalQty' headerText='Total Qty' width='100' />
                  <ColumnDirective field='price' headerText='Price' width='130' />
                  <ColumnDirective field='taxAmount' headerText='Tax Amount' width='130' />
                  <ColumnDirective field='value' headerText='Value' width='130' />
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort]} />
              </GridComponent>
              <TextBoxComponent placeholder="Add Comments" multiline={true} floatLabelType="Auto" />
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2' >Approve</Button>
              <Button color="primary" size="sm" className='mt-3 mb-2'>Review</Button>
            </div>
            : null}
          {/* ----------------------------- UD Form end  -----------------------------*/}

          {/* ----------------------------- Accounts Form  -----------------------------*/}
          {this.state.divHidden == 'accountsForm' ?
            <div className='pl-3 pr-3'>
              <Row>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN #" disabled={true} floatLabelType="Auto" value={this.state.btProcessHistory.grnNumber} /></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN Date" disabled={true} floatLabelType="Auto" value={this.state.btProcessHistory.grnDate} ></TextBoxComponent></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT #" disabled={true} floatLabelType="Auto" value={this.state.btProcessHistory.btNumber} /></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT Date" floatLabelType="Auto" disabled={true} value={this.state.btProcessHistory.btDate}></TextBoxComponent></Col>
              </Row>
              <div className='mt-2'></div>
              <GridComponent gridLines='Both' rowHeight='25' gridLines='Both' dataSource={this.state.btProcessHistory.btgrndtos} ref={grid => this.processTrailGrid = grid} allowSorting={true}>
                <ColumnsDirective>
                  <ColumnDirective field='documentName' headerText='Document #' width='100' />
                  <ColumnDirective field='lineItem' headerText='Line item' width='125' />
                  <ColumnDirective field='lineItemDescription' headerText='LI Description' width='150' />
                  <ColumnDirective field='uom' headerText='Uom' width='135' />
                  <ColumnDirective field='totalQty' headerText='Total Qty' width='100' />
                  <ColumnDirective field='price' headerText='Price' width='130' />
                  <ColumnDirective field='taxAmount' headerText='Tax Amount' width='130' />
                  <ColumnDirective field='value' headerText='Value' width='130' />
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort]} />
              </GridComponent>
              {/* <div className='mt-4'></div>
              <GridComponent gridLines='Both' rowHeight='25' gridLines='Both' dataSource={this.state.btMiroDetails} ref={grid => this.btMiroGrid = grid} allowSorting={true}>
                <ColumnsDirective>
                  <ColumnDirective field='accountingDocNo' headerText='Document #' width='130' />
                  <ColumnDirective field='companyCode' headerText='Company Code' width='140' />
                  <ColumnDirective field='matDesc' headerText='Material Description' width='150' />
                  <ColumnDirective field='vendorNo' headerText='Vendor No.' width='135' />
                  <ColumnDirective field='compGstNo' headerText='Company Gst No.' width='140' />
                  <ColumnDirective field='billDate' headerText='Bill Date' width='130' />
                  <ColumnDirective field='vendorName' headerText='Vendor Name' width='130' />
                  <ColumnDirective field='matType' headerText='Material Type' width='130' />
                  <ColumnDirective field='businessPlace' headerText='Bussiness Place' width='140' />
                  <ColumnDirective field='totalTax' headerText='Total Tax' width='125' />
                  <ColumnDirective field='totalValueInLocalCurrency' headerText='Total Value' width='120' />
                  <ColumnDirective field='qty' headerText='Total Qty' width='135' />
                  <ColumnDirective field='fiscalPeriod' headerText='Fiscal Period' width='100' />
                  <ColumnDirective field='matUnit' headerText='UOM' width='80' />
                  <ColumnDirective field='fiscalYear' headerText='Fiscal Year' width='130' />
                  <ColumnDirective field='invDate' headerText='Invoice Date' width='130' />
                  <ColumnDirective field='billNo' headerText='Bill No.' width='130' />

                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort]} />
              </GridComponent> */}
              <div className='mt-2'></div>
              <Col sm='12' md='4'>
                <div className='fileUpload'>
                  <label className='pr-3'>Attach Document</label>
                  <input id="fileUpload" type="file" onChange={this.onFileChanges} ref={ref => this.fileInput = ref} disabled={!this.state.currentProcessEditable} placeholder='Attach Document' />
                  {/* <input id="fileUpload" type="file" multiple onChange={this.onFileChanges} ref={ref => this.fileInput = ref}   placeholder='Attach Document' /> */}

                </div>
              </Col>
              <TextBoxComponent placeholder="Add Comments" multiline={true} ref={(scope) => { this.commentsObj = scope; }} disabled={!this.state.currentProcessEditable} value={this.state.btProcessHistory.comments} floatLabelType="Auto" />
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2' onClick={this.onClickSave} disabled={!this.state.currentProcessEditable} >Save</Button>
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2' onClick={this.onClickApprove} disabled={!this.state.currentProcessEditable}>Approve</Button>

              <Button color="primary" size="sm" className='mt-3 mb-2' onClick={this.onClickReview} disabled={!this.state.currentProcessEditable || this.state.formHeading == "UD"}>Review</Button>
            </div>
            : null}
          {/* ----------------------------- Accounts Form end  -----------------------------*/}

          {/* ----------------------------- BVT Form  -----------------------------*/}
          {this.state.divHidden == 'bvtForm' ?
            <div className='pl-3 pr-3'>
              <Row>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN #" disabled={true} floatLabelType="Auto" value={this.state.btProcessHistory.grnNumber} /></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN Date" disabled={true} floatLabelType="Auto" value={this.state.btProcessHistory.grnDate} ></TextBoxComponent></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT #" disabled={true} floatLabelType="Auto" value={this.state.btProcessHistory.btNumber} /></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT Date" floatLabelType="Auto" disabled={true} value={this.state.btProcessHistory.btDate}></TextBoxComponent></Col>
              </Row>
              <div className='mt-2'></div>
              <GridComponent gridLines='Both' rowHeight='25' gridLines='Both' dataSource={this.state.btProcessHistory.btgrndtos} allowSorting={true}>
                {/* <ColumnsDirective>
                  <ColumnDirective field='btDate' headerText='Line item' width='125' />
                  <ColumnDirective field='ud' headerText='LI Description' width='100' />
                  <ColumnDirective field='accounts' headerText='Uom' width='135' />
                  <ColumnDirective field='bvt' headerText='Total Qty' width='100' />
                  <ColumnDirective field='hod1' headerText='Price' width='130' />
                  <ColumnDirective field='hod2' headerText='Tax Amount' width='130' />
                  <ColumnDirective field='audit' headerText='Value' width='130' />
                </ColumnsDirective> */}
                <ColumnsDirective>
                  <ColumnDirective field='documentName' headerText='Document #' width='100' />
                  <ColumnDirective field='lineitem' headerText='Line item' width='125' />
                  <ColumnDirective field='lineItemDescription' headerText='LI Description' width='150' />
                  <ColumnDirective field='uom' headerText='Uom' width='135' />
                  <ColumnDirective field='totalQty' headerText='Total Qty' width='100' />
                  <ColumnDirective field='price' headerText='Price' width='130' />
                  <ColumnDirective field='taxAmount' headerText='Tax Amount' width='130' />
                  <ColumnDirective field='value' headerText='Value' width='130' />
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort]} />
              </GridComponent>
              <div className='mt-4'></div>
              <GridComponent gridLines='Both' rowHeight='25' allowResizing={true} gridLines='Both' dataSource={this.state.previousDebitNotes} 
              commandClick={this.previousDebitNoteCommandClick} allowSorting={true} editSettings={this.editSettings}>
                <ColumnsDirective>
                  <ColumnDirective field='debitNoteNumber' headerText='Debit Note Number' allowEditing={false} template={this.debitNoteTemplate} width='90' />        
                   <ColumnDirective field='lineItems' headerText='Line item' allowEditing={false} width='90' />
                  <ColumnDirective field='lineItemsDescription' headerText='LI Description' allowEditing={false} width='130' />
                  <ColumnDirective field='totalQty' headerText='Total Debit Qty' width='100' />
                  <ColumnDirective field='totalAmount' headerText='Total Debit Amount' width='100' />
                  <ColumnDirective field='status' headerText='Status' width='100' />
                  <ColumnDirective headerText='Action' width='100' commands={this.state.previousDebitNoteCommands}></ColumnDirective>
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort, Edit, CommandColumn]} />
              </GridComponent>
              <div className='mt-4'></div>
              {/* <div hidden={this.state.debitNoteGrid}> */}
              <GridComponent gridLines='Both' rowHeight='25' allowResizing={true} gridLines='Both' dataSource={this.state.debitNoteDetails} ref={grid => this.debitNoteGrid = grid} allowSorting={true} editSettings={this.editSettings}>
                <ColumnsDirective>
                  <ColumnDirective field='invoiceNumber' headerText='Invoice #' allowEditing={false} width='125' />
                  <ColumnDirective field='invoiceType' headerText='Invoice Type' allowEditing={false} width='120' />
                  <ColumnDirective field='poLineItem' headerText='Line item' allowEditing={false} width='90' />
                  <ColumnDirective field='poLineItemDesc' headerText='LI Description' allowEditing={false} width='130' />
                  {/* <ColumnDirective field='accounts' headerText='Uom' width='70' /> */}
                  <ColumnDirective field='totalGrnAmount' headerText='Total Grn Amount' allowEditing={false} width='120' />
                  <ColumnDirective field='totalGRNQty' headerText='Total Grn Qty' allowEditing={false} width='120' />
                  <ColumnDirective field='totalDebitQuantity' headerText='Total Qty' editType='numericedit' edit={{ params: { decimals: 2, min: 1 } }} validationRules={{ required: true }}  width='130' />
                  <ColumnDirective field='totalDebitAmount' headerText='Total Amount' editType='numericedit' edit={{ params: { decimals: 2, min: 1 } }} validationRules={{ required: true }} width='130' />
                  <ColumnDirective headerText='Action' width='100' commands={this.commands}></ColumnDirective>
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort, Edit, CommandColumn]} />
              </GridComponent>
              <Button color="primary" size="sm" className='mt-2 mb-2' onClick={this.generateDebitNote} disabled={!this.state.currentProcessEditable}>Generate Debit Note</Button>
              {/* </div> */}
              <Col sm='12' md='4'>
                <div className='fileUpload'>
                  <label className='pr-3'>Attach Document</label>
                  <input id="fileUpload" type="file" onChange={this.onFileChanges} ref={ref => this.fileInput = ref} disabled={!this.state.currentProcessEditable} placeholder='Attach Document' />
                  {/* <input id="fileUpload" type="file" multiple onChange={this.onFileChanges} ref={ref => this.fileInput = ref}   placeholder='Attach Document' /> */}

                </div>
              </Col>
              <div className='mt-2'></div>
              <TextBoxComponent placeholder="Add Comments" multiline={true} floatLabelType="Auto" disabled={!this.state.currentProcessEditable} ref={(scope) => { this.commentsObj = scope; }} />
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2' onClick={this.onClickSave} disabled={!this.state.currentProcessEditable} >Save</Button>
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2' onClick={this.onClickApprove} disabled={!this.state.currentProcessEditable} >Approve</Button>
              <Button color="primary" size="sm" className='mt-3 mb-2' onClick={this.onClickReview} disabled={!this.state.currentProcessEditable}>Review</Button>
            </div>
            : null}
          {/* ----------------------------- BVT Form end  -----------------------------*/}

          {/* ----------------------------- hod1 Form  -----------------------------*/}
          {this.state.divHidden == 'hod1Form' ?
            <div className='pl-3 pr-3'>
              <Row>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="GRN Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="BT Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
              </Row>
              <div className='mt-2'></div>
              <GridComponent gridLines='Both' rowHeight='25' gridLines='Both' dataSource={this.state.grnGridInfo} allowSorting={true}>
                <ColumnsDirective>
                  <ColumnDirective field='btId' headerText='Document #' width='100' />
                  <ColumnDirective field='btDate' headerText='Line item' width='125' />
                  <ColumnDirective field='ud' headerText='LI Description' width='150' />
                  <ColumnDirective field='accounts' headerText='Uom' width='135' />
                  <ColumnDirective field='bvt' headerText='Total Qty' width='100' />
                  <ColumnDirective field='hod1' headerText='Price' width='130' />
                  <ColumnDirective field='hod2' headerText='Tax Amount' width='130' />
                  <ColumnDirective field='audit' headerText='Value' width='130' />
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort]} />
              </GridComponent>
              <TextBoxComponent placeholder="Add Comments" multiline={true} floatLabelType="Auto" />
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2'>Appove</Button>
              <Button color="primary" size="sm" className='mt-3 mb-2'>Reject</Button>
            </div>
            : null}
          {/* ----------------------------- hod1 Form end  -----------------------------*/}
          {/* ----------------------------- hod2 Form  -----------------------------*/}
          {this.state.divHidden == 'hod2Form' ?
            <div className='pl-3 pr-3'>
              <Row>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="GRN Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="BT Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
              </Row>
              <div className='mt-2'></div>
              <GridComponent gridLines='Both' rowHeight='25' gridLines='Both' dataSource={this.state.grnGridInfo} allowSorting={true}>
                <ColumnsDirective>
                  <ColumnDirective field='btId' headerText='Document #' width='100' />
                  <ColumnDirective field='btDate' headerText='Line item' width='125' />
                  <ColumnDirective field='ud' headerText='LI Description' width='150' />
                  <ColumnDirective field='accounts' headerText='Uom' width='135' />
                  <ColumnDirective field='bvt' headerText='Total Qty' width='100' />
                  <ColumnDirective field='hod1' headerText='Price' width='130' />
                  <ColumnDirective field='hod2' headerText='Tax Amount' width='130' />
                  <ColumnDirective field='audit' headerText='Value' width='130' />
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort]} />
              </GridComponent>
              <TextBoxComponent placeholder="Add Comments" multiline={true} floatLabelType="Auto" />
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2'>Appove</Button>
              <Button color="primary" size="sm" className='mt-3 mb-2'>Reject</Button>
            </div>
            : null}
          {/* ----------------------------- hod2 Form end  -----------------------------*/}
          {/* ----------------------------- Audit Form  -----------------------------*/}
          {this.state.divHidden == 'auditForm' ?
            <div className='pl-3 pr-3'>
              <Row>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="GRN Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="BT Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
              </Row>
              <div className='mt-2'></div>
              <GridComponent gridLines='Both' rowHeight='25' gridLines='Both' dataSource={this.state.grnGridInfo} allowSorting={true}>
                <ColumnsDirective>
                  <ColumnDirective field='btId' headerText='Document #' width='100' />
                  <ColumnDirective field='btDate' headerText='Line item' width='125' />
                  <ColumnDirective field='ud' headerText='LI Description' width='150' />
                  <ColumnDirective field='accounts' headerText='Uom' width='135' />
                  <ColumnDirective field='bvt' headerText='Total Qty' width='100' />
                  <ColumnDirective field='hod1' headerText='Price' width='130' />
                  <ColumnDirective field='hod2' headerText='Tax Amount' width='130' />
                  <ColumnDirective field='audit' headerText='Value' width='130' />
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort]} />
              </GridComponent>
              <TextBoxComponent placeholder="Add Comments" multiline={true} floatLabelType="Auto" />
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2'>Appove</Button>
              <Button color="primary" size="sm" className='mt-3 mb-2'>Reject</Button>
            </div>
            : null}
          {/* ----------------------------- audit Form end  -----------------------------*/}
          {/* ----------------------------- Management Form  -----------------------------*/}
          {this.state.divHidden == 'managementForm' ?
            <div className='pl-3 pr-3'>
              <Row>
                <Col sm='3'>  <TextBoxComponent placeholder="GRN #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="GRN Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
                <Col sm='3'>  <TextBoxComponent placeholder="BT #" floatLabelType="Auto" /></Col>
                <Col sm='3'>  <DatePickerComponent placeholder="BT Date" floatLabelType="Auto" value={this.dateValue}></DatePickerComponent></Col>
              </Row>
              <div className='mt-2'></div>
              <GridComponent gridLines='Both' rowHeight='25' gridLines='Both' dataSource={this.state.grnGridInfo} allowSorting={true}>
                <ColumnsDirective>
                  <ColumnDirective field='btId' headerText='Document #' width='100' />
                  <ColumnDirective field='btDate' headerText='Line item' width='125' />
                  <ColumnDirective field='ud' headerText='LI Description' width='150' />
                  <ColumnDirective field='accounts' headerText='Uom' width='135' />
                  <ColumnDirective field='bvt' headerText='Total Qty' width='100' />
                  <ColumnDirective field='hod1' headerText='Price' width='130' />
                  <ColumnDirective field='hod2' headerText='Tax Amount' width='130' />
                  <ColumnDirective field='a`ud`it' headerText='Value' width='130' />
                </ColumnsDirective>
                <Inject services={[DetailRow, Page, Resize, Sort]} />
              </GridComponent>
              <TextBoxComponent placeholder="Add Comments" multiline={true} floatLabelType="Auto" />
              <Button color="primary" size="sm" className='mt-3 mb-2 mr-2'>Appove</Button>
              <Button color="primary" size="sm" className='mt-3 mb-2'>Reject</Button>
            </div>
            : null}
          {/* ----------------------------- Management Form end  -----------------------------*/}
        </DialogComponent>
        {/*  ------------- ---------------------- GRN Form dialog /* ---------------------- --------------- */}
        {/* ------------- ---------------------- PDF Documents ------------- ---------------------- */}
        <DialogComponent allowDragging={true} id="modalDialogPDF" isModal={true} width='60%' visible={this.state.pdfViewDialog} showCloseIcon={true} header='Attached Document View' close={this.closeDialog}>
          <br />
          <br />
          <div className='pl-3 mt-n2 pb-3 pr-3'>
            {this.state.pdf == true &&
              <center> <div >
                <SinglePagePDFViewer pdf={this.state.pdfInfo} />
                <hr />
              </div>
              </center>
            }
             {this.state.image == true &&
              <center> <div >
                <img src={this.state.imageInfo} />
                <hr />
              </div>
              </center>
            }
          </div>
        </DialogComponent>
        {/* ------------- ---------------------- PDF Documents ------------- ---------------------- */}


        <DialogComponent allowDragging={true} id="modalDialogPDF" isModal={true} width='60%' visible={this.state.debitNoteDetailsDialog} showCloseIcon={true} header=  {this.state.previousDebitNotes!= null ? 'Debit Note Number - '+this.state.selectedDebitNoteNumber: ""}  close={this.debitNoteDetailCloseDialog}>
          <br />
          <br />
          <div className='pl-3 mt-n2 pb-3 pr-3'>
          <GridComponent gridLines='Both' rowHeight='25' allowResizing={true} allowPaging={true} gridLines='Both' dataSource={this.state.previousGenerateddebitNoteDetails} ref={grid => this.previousGeneratedDebitNoteDetailGrid = grid} actionBegin={this.debitNoteByLineItemCommandClick} allowSorting={true} editSettings={this.editSettings}>
                  <ColumnsDirective>
                    <ColumnDirective field='invoiceNumber' headerText='Invoice #' allowEditing={false} width='125' />
                    <ColumnDirective field='invoiceType' headerText='Invoice Type' allowEditing={false} width='120' />
                    <ColumnDirective field='poLineItem' headerText='Line item' allowEditing={false} width='90' />
                    <ColumnDirective field='poLineItemDesc' headerText='LI Description' allowEditing={false} width='130' />
                    {/* <ColumnDirective field='accounts' headerText='Uom' width='70' /> */}
                    <ColumnDirective field='totalGrnAmount' headerText='Total Grn Amount' allowEditing={false} width='120' />
                    <ColumnDirective field='totalGRNQty' headerText='Total Grn Qty'  allowEditing={false}   width='120' />
                    <ColumnDirective field='totalDebitQuantity' headerText='Total Debit Qty'  editType='numericedit' edit={{ params: { decimals: 2, min: 1 } }} validationRules={{ required: true }} width='130' />
                    <ColumnDirective field='totalDebitAmount' headerText='Total Debit Amount'  editType='numericedit' edit={{ params: { decimals: 2, min: 1} }} validationRules={{ required: true }} width='150' />
                    <ColumnDirective headerText='Action' width='100' commands={this.commands}></ColumnDirective>
                  </ColumnsDirective>
                  <Inject services={[DetailRow, Page, Resize, Sort, Edit, CommandColumn]} />
                </GridComponent>
          </div>
        </DialogComponent>

        <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
      </div >
    );
  }
}
export default withTranslation("translations")(BillTracking);
