import { rightToLeft, rippleEffect } from '@syncfusion/ej2-base';
import { ChangeEventArgs, rippleMouseHandler } from '@syncfusion/ej2-buttons';
import { CheckBoxComponent, SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { ButtonComponent, RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { CheckBoxSelection, DropDownList, DropDownListComponent, MultiSelectComponent } from '@syncfusion/ej2-react-dropdowns';
import { ColumnDirective, ColumnsDirective, CommandColumn, DetailRow, Edit, ExcelExport, Filter, ForeignKey, Grid, GridComponent, Group, Inject, Page, Reorder, Resize, Selection, SelectionType, Sort, Toolbar, VirtualScroll } from '@syncfusion/ej2-react-grids';
import { NumericTextBoxComponent, TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { Toast, ToastComponent, ToastModel } from '@syncfusion/ej2-react-notifications';
import { AnimationSettingsModel, ButtonPropsModel, DialogComponent } from '@syncfusion/ej2-react-popups';
import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

import { AppConfig } from '../../appConfig/appConfig';
import { data } from '../../assets/data/data';
import { sampleData } from '../../assets/data/inspection';
import { PropertyPane } from '../../assets/data/purchaseOrder';
import { SampleBase } from '../../assets/data/sample-base';
import BuyerTestPlanMappingModel, { LabTestMasterModel, MICMasterModel, TestPlanGroupModel } from './DefectLabModel';
import QualityMasterService from './QualityMasterService';

class DefectLabMasterComponent extends React.PureComponent {
  constructor() {
    super(...arguments);
    this.QualityMasterService = new QualityMasterService();

    this.state = {
      isManual: false,
      labModel: new LabTestMasterModel(null, null, null, null, null, [], null),
      testPlanModel: new TestPlanGroupModel(null, "", "", []),
      toolbarOptions: ['Add','Update','Delete','Cancel'],
      toasterPosition: { X: 'Right', Y: 'Bottom' },
      buyerId:null,
      testName: false,
      testMethod: false,
      buyerMappings: [],
      buyers: [],
      productTypes: [],
      testPlanGroups: [],
      selectedBuyerName: null,
      selectedBuyerCode: null,
      selectedProductType: null,
      selectedProductTypeGroup: null,
      selectedTestPlanGroup: null,
      selectedTestPlanGroupInDialogue: null,
      newTestPlan: false,
      selectedPlanGroupCode: null,
      selectedPlanGroupName: null,
      labTestMasterList: [],
      labTestList: [],
      newLabTest: false,
      labTestMasterListForDialogue: [],
      micMasterList: [],
      micDataList: [],
      isManual: false,
      isTestMF: false,
      editArgs: null
    };
    this.buttons = [{
      click: () => {
        this.onSaveLabTest();
      },
      buttonModel: {
        content: 'Save',
        isPrimary: true
      }
    },
    {
      click: () => {
        this.dialogInstance.hide();
        this.setState({ labModel: new LabTestMasterModel(null, null, null, null, null, [], null),isTestMF: false }, () => {
          this.dropDownRef.value = null
        });
        this.setState({ testMethod: false });
        this.setState({ testName: false, isTestMF: false });
      },
      buttonModel: {
        content: 'Cancel',
        cssClass: 'e-secondary'
      }
    }];
    this.testPlanButtons = [{
      click: () => {
        this.onSaveTestPlan();
      },
      buttonModel: {
        content: 'Save',
        isPrimary: true
      }
    },
    {
      click: () => {
        this.dialogInstance.hide();
        this.setState({ testPlanModel: new TestPlanGroupModel(null, "", "", []) }, () => {
          this.testPlanDropDownRef.value = null
        });
        this.setState({ testMethod: false });
        this.setState({ testName: false });
      },
      buttonModel: {
        content: 'Cancel',
        cssClass: 'e-secondary'
      }
    }];

    this.commands = [
      { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
      { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },
    ];

    this.childGrid = {
      dataSource: this.state.micDataList,
      queryString: 'pid',
      pageSettings: { pageSize: 6, pageCount: 5 },
      editSettings: { allowEditing: true },
      toolbar: ['Update', 'Cancel'],
      columns: [
        { field: 'micCode', headerText: 'MIC Code', width: 120, allowEditing: false },
        { field: 'description', headerText: 'MIC Description', width: 120, allowEditing: false },
        { field: 'inputMinRange', headerText: 'Input Minimum Range', width: 120 },
        { field: 'inputMaxRange', headerText: 'Input Maximum Range', width: 150 },
        { field: 'passMinRange', headerText: 'Pass Minimum Range', width: 120 },
        { field: 'passMaxRange', headerText: 'Pass Maximum Range', width: 150 }
      ],
      childGrid: this.secondChildGrid,
      actionBegin: (args) => {
        let that = this;
        if (args.requestType === "save") {
          let model = Object.assign({}, that.state.testPlanModel)
          model.labTestDTOs.forEach(e => {
            if (e.id == args.data.pid) {
              e.micDTOs.forEach(element => {
                if (element.id == args.data.id) {
                  element.inputMaxRange = args.data.inputMaxRange;
                  element.inputMinRange = args.data.inputMinRange;
                  element.passMaxRange = args.data.passMaxRange;
                  element.passMinRange = args.data.passMinRange;
                }
              });
            }
          })
          that.setState({ testPlanModel: model })
        }
        // if (args.requestType === "save") {
        //   let model = Object.assign({}, that.state.testPlanModel)
        //   model.labTestDTOs.forEach(e => {
        //     if(args.data.id){
        //       if (e.id == args.data.id) {
        //         e.micDTOs.forEach(element => {
        //           if (element.id == args.data.id) {
        //             element.inputMaxRange = args.data.inputMaxRange;
        //             element.inputMinRange = args.data.inputMinRange;
        //             element.passMaxRange = args.data.passMaxRange;
        //             element.passMinRange = args.data.passMinRange;
        //           }
        //         });
        //       }
        //     }
        //     else{
        //       e.micDTOs.forEach(element => {
        //         if (element.uiGridId == args.data.uiGridId) {
        //           element.inputMaxRange = args.data.inputMaxRange;
        //           element.inputMinRange = args.data.inputMinRange;
        //           element.passMaxRange = args.data.passMaxRange;
        //           element.passMinRange = args.data.passMinRange;
        //         }
        //       });
        //     }
        //   })
        //   that.setState({ testPlanModel: model })
        // }
      },
    };

    this.testNameClick = this.testNameClick.bind(this);
    this.testMethodClick = this.testMethodClick.bind(this);
  }

  dialogClose() {
    this.setState({
      testName: false,
      testMethod: false,
      isTestMF: false
    });
    this.setState({ labModel: new LabTestMasterModel(null, null, null, null, null, [], null) }, () => {
      this.dropDownRef.value = null
    });
  }

  testNameClick() {
    this.setState({ testName: true });
  }
  testMethodClick() {
    // this.setState({ testMethod: true });
    this.dialogInstance.show()
  }
  onChangeStatus = (event) => {
    this.setState({ isManual: event.checked });
  }
  manualTemplate = (args) => {
    console.log('ckebox', args)

    return (<div><SwitchComponent id='toggle' disabled={this.disableValidate(args)} change={() => this.setState({ isManual: this.state.isManual ? false : true })} editType="booleanedit" type="boolean" checked={args.isManual} /></div>);
  }

  disableValidate(args) {
    if (!args.index) {
      if (args.id) {
        if (args.id === this.state.editArgs.id) {
          return false
        } else {
          return true
        }
      } else {
        return false
      }
    } else {
      return true
    }
  }

  componentDidMount() {
    // this.setState({
    //   defectLabDataSource: this.defectLabDataSource
    // });
    this.getBuyerMappings()
    this.getBuyersList()
    this.getProductGroupList()
    this.getTestPlanGroupList()
    this.getLabTestsList()
  }

  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }

  async getBuyerMappings() {
    let response = (await this.QualityMasterService.getBuyerMappingList())
    if ((response != undefined) && (response.status == 200)) {
      let buyerMappingList = [];
      response.data.map(item => {
        buyerMappingList.push(new BuyerTestPlanMappingModel(item.id, item.buyerName, item.buyerId, item.productType, item.productTypeGroup, item.testPlanGroupDTO, item.testPlanGroupDTO != null ? item.testPlanGroupDTO.testPlanGroupCode : ""))
      })
      this.setState({ buyerMappings: buyerMappingList }, () => {
      })
    }
  }

  async getBuyersList() {
    let response = (await this.QualityMasterService.getBuyersList())
    if ((response != undefined) && (response.status == 200))
      this.setState({ buyers: response.data }, () => {
      })
  }

  async getProductGroupList() {

    let response = (await this.QualityMasterService.getProductsList())
    if ((response != undefined) && (response.status == 200))
      this.setState({ productTypes: response.data }, () => {
      })
  }

  async getTestPlanGroupList() {

    let response = (await this.QualityMasterService.getTestPlanGroupList())
    let testPlans = [];
    testPlans.push(new TestPlanGroupModel(0, 'Create New...', '', []));
    if ((response != undefined) && (response.status == 200))
      response.data.map(testPlanObj => {
        testPlans.push(testPlanObj);
      });
    this.setState({ testPlanGroups: response.data, testPlanGroupListForDialogue: testPlans }, () => {
    })
  }

  async getLabTestsList() {

    let response = (await this.QualityMasterService.getLabTestMasterList());

    let labTests = [];
    labTests.push(new LabTestMasterModel(0, 'Create New...', '', '', '', [], null));

    if ((response != undefined) && (response.status == 200))
      response.data.map(labTestObj => {
        labTests.push(labTestObj);
      });
    this.setState({ labTestMasterList: response.data, labTestMasterListForDialogue: labTests }, () => {
    })
  }

  async saveBuyerMapping(newBuyerMapping) {
    let response = (await this.QualityMasterService.createBuyerTestPlanMapping(newBuyerMapping))
    if ((response != undefined) && (response.data.status == "success")) {

      this.toastObj.show(this.createToaster(response.data.message, "Success"));
      this.setState({selectedProductType:null,selectedProductTypeGroup:null,selectedTestPlanGroup:null})
      this.getBuyerMappings();
    }
    else if ((response != undefined) && (response.data.status == "unsuccess")) {
      // this.setState({ isDisabledSaveButton: false })
      this.toastObj.show(this.createToaster(response.data.message, "Information"));
    }
    else
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    // this.buyermappinggridInstance.dataSource = this.state.buyerMappings;
    // this.buyermappinggridInstance.refresh();
  }


  async deleteBuyerMapping(buyerMappingId) {
    let response = (await this.QualityMasterService.deleteBuyerTestPlanMapping(buyerMappingId))
    if ((response != undefined) && (response.status == 200)) {
      this.toastObj.show(this.createToaster("Mapping deleted successfully!!", "Success"));
      this.getBuyerMappings();

    }
    else {
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }

  async saveTestPlan(testPlan) {
    let response = (await this.QualityMasterService.createTestPlanGroup(testPlan))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster(response.data.message, "Success"));
      this.setState({ testPlanModel: new TestPlanGroupModel(null, "", "", []) }, () => {
        this.testPlanDropDownRef.value = null;
        this.testPlanDialogInstance.hide();
        // this.setState({ testMethod: false });
        // this.setState({ testName: false });
        this.getTestPlanGroupList();
      });
    }
    else if ((response == undefined) || (response.data.status == "unsuccess")) {
      // this.setState({ isDisabledSaveButton: false })
      this.toastObj.show(this.createToaster("Please enter details", "Information"));
    }
    else{

      this.toastObj.show(this.createToaster("Something went wrong", "Error"));
    }
  }


  async saveLabTest(labTest) {
    let response = (await this.QualityMasterService.createLabTestMaster(labTest))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster(response.data.message, "Success"));
      this.setState({ labModel: new LabTestMasterModel(null, null, null, null, null, [], null) }, () => {
        this.dropDownRef.value = null;
        this.dialogInstance.hide();
        this.setState({ testMethod: false });
        this.setState({ testName: false });
        this.getLabTestsList();
      });
    }
    else if ((response != undefined) && (response.data.status == "unsuccess")) {
      // this.setState({ isDisabledSaveButton: false })
      // this.toastObj.show(this.createToaster("Please enter details", "Information"));
      this.toastObj.show(this.createToaster(response.data.message, "Information"));
    }
    else
      this.toastObj.show(this.createToaster("Something went wrong", "Error"));

  }

  createBuyerdrop(args) {
    this.elem = document.createElement('input');
    return this.elem;
  }

  readBuyerdrop(args) {
    return this.dropInstance.value;
  }

  destroyBuyerdrop(args) {
    this.dropInstance.destroy();
  }

  writeBuyerdrop(args) {
    this.dropInstance = new DropDownList({
      dataSource: this.state.buyers,
      fields: { text: 'buyerName', value: 'buyerName' },
      placeholder:"Buyer Name",
      value: args.rowData.buyerName,
      change: this.change.bind(this)
    });
    this.dropInstance.appendTo(this.elem);
  }

  change(args) {
    args.element.form[2].value = args.itemData.buyerId;
  }

  buyerParams = {
    create: this.createBuyerdrop.bind(this),
    read: this.readBuyerdrop.bind(this),
    destroy: this.destroyBuyerdrop.bind(this),
    write: this.writeBuyerdrop.bind(this)
  };

  // buyerTemplate = (args) => {
  //   let that = this;
  //   return (<div>  <DropDownListComponent id='buyerName' value={args.buyerName} placeholder="Select a Buyer" fields={{ text: 'buyerName', value: 'buyerName' }}
  //     dataSource={that.state.buyers} change={e=>this.onChangeBuyer(e,args)} popupHeight="220px" /></div>);
  // }

  buyerCodeTemplate = () => {
    let that = this;
    return (<div>  <TextBoxComponent id='buyerId' placeholder="" floatLabelType="Auto" readOnly={true} /></div>);
  }

  onChangeBuyer = (e, args) => {
    // const filterval = this.state.buyers.filter(item => item.buyerName == e.itemData.buyerName)
    // const filtervalId = filterval[0].buyerId;
    this.setState({buyerId:e.itemData.buyerId}) 
    args.buyerId = e.itemData.buyerId;
    

    // this.setState({selectedBuyerCode:filtervalId})

    // const buyerMappingsco = [...this.state.buyerMappings]
    // buyerMappingsco.map((item, i) => {
    //   if (item.id == args.id) {

    //     item.buyerId = filtervalId
    //   }
    // })

    // console.log(buyerMappingsco);

    // this.setState({ buyerMappings: buyerMappingsco })

  }

  // buyerCodeTemplate = () => {
  //   let that = this;
  //   return (<div>  <TextBoxComponent id='buyerId' placeholder="" floatLabelType="Auto" readOnly={true} /></div>);
  // }

  productTypeTemplate = (args) => {
    let that = this;
    return (<div>  <DropDownListComponent id='productType' value={args.productType} placeholder="Select a Product" fields={{ text: 'productName', value: 'productName' }}
      dataSource={that.state.productTypes} change={this.onChangeProductType} popupHeight="220px" /></div>);
  }



  onChangeProductType = (event) => {
    if (event.value != null)
      this.setState({ selectedProductType: event.itemData.productName, selectedProductTypeGroup: event.itemData.productCode })
  }

  testPlanGroupTemplate = (args) => {
    let that = this;
    return (<div>  <DropDownListComponent id='testPlanGroup' value={args.testPlanGroup} placeholder="Select a Test Plan Group" fields={{ text: 'testPlanGroupCode', value: 'testPlanGroupCode' }}
      dataSource={that.state.testPlanGroups} change={this.onChangeTestPlanGroup} popupHeight="220px" /></div>);
  }

  addOrEditTestPlanGroupTemplate = (args) => {
    let that = this;
    return (<div>  <DropDownListComponent id='name' value={args.name} placeholder="Select a Test" fields={{ text: 'name', value: 'name' }}
      dataSource={that.state.labTestMasterList} change={args => this.onChangeLabTest(args)} popupHeight="220px" /></div>);
  }

  onChangeTextField = (args) => {
    let that = this;
    if ((args.event != undefined) && (args.value != null) && (args.value != undefined)) {
      that.setState({ labModel: { ...that.state.labModel, [args.event.target.id]: args.value.trim() } })
    }
  }

  onChangeTestPlanTextField = (args) => {
    let that = this;
    if ((args.event != undefined) && (args.value != null) && (args.value != undefined)) {
      that.setState({ testPlanModel: { ...that.state.testPlanModel, [args.event.target.id]: args.value.trim() } })
    }
  }

  onChangeTestPlanGroup = (event) => {
    if (event.value != null)
      this.setState({ selectedTestPlanGroup: event.itemData })
  }

  onChangeLabTest = (args) => {
    this.setState({testGroupRowData:args.itemData})
    // let that = this;
    // if (event.value != null) {
    //   let micInfos = [];
    //   event.itemData.micMasterDTOs.map(micObj => {
    //     micInfos.push(new MICMasterModel(micObj.id, micObj.micCode, micObj.description, micObj.inputMinRange, micObj.inputMaxRange, micObj.passMinRange, micObj.passMaxRange, micObj.isManual, micObj.unit, event.itemData.id));
    //   })
    //   that.testGridInstance.childGrid.dataSource.push(micInfos);

    //   that.state.testPlanModel.labTestDTOs.push(event.itemData);
    // }
  }

  onChangeTestPlanGroupDialogue = (event) => {
    let that = this;
    if (event.itemData != null && event.itemData != undefined) {
      if (event.itemData.id == 0) {
        that.setState({ newTestPlan: true, testPlanModel: new TestPlanGroupModel(null, "", "", []) });
      }
      else {
        let labInfos = [];
        let micInfos = [];
        event.itemData.labTestDTOs.map(labObj => {
          let micInfo = [];
          labObj.micDTOs.map(micObj => {
            micInfos.push(new MICMasterModel(micObj.id, micObj.micCode, micObj.description, micObj.inputMinRange, micObj.inputMaxRange, micObj.passMinRange, micObj.passMaxRange, micObj.isManual, micObj.unit, labObj.id, null));
            micInfo.push(new MICMasterModel(micObj.id, micObj.micCode, micObj.description, micObj.inputMinRange, micObj.inputMaxRange, micObj.passMinRange, micObj.passMaxRange, micObj.isManual, micObj.unit, labObj.id, null));
          })
          labInfos.push(new LabTestMasterModel(labObj.id, labObj.name, labObj.description, labObj.code, labObj.testMethod, micInfo, labObj.id));

        })
        that.testGridInstance.childGrid.dataSource = micInfos;

        that.setState({ testPlanModel: { ...that.state.testPlanModel, id: event.itemData.id, testPlanGroupCode: event.itemData.testPlanGroupCode, testPlanGroupName: event.itemData.testPlanGroupName, labTestDTOs: labInfos }, micDataList: micInfos, newTestPlan: false });

      }
    }
  }

  onChangeLabTestDialogue = (event) => {
    if (event.itemData != null && event.itemData != undefined) {
      if (event.itemData.id == 0) {
        this.setState({ newLabTest: true, labModel: new LabTestMasterModel(null, null, null, null, null, [], null) });
      }
      else
        this.setState({ labModel: { ...this.state.labModel, id: event.itemData.id, name: event.itemData.name, description: event.itemData.description, testMethod: event.itemData.testMethod, micDTOs: event.itemData.micDTOs }, newLabTest: false });
    }
  }

  actionBeginBuyerMapping = (args) => {
    let that = this;
    const { selectedProductTypeGroup, selectedTestPlanGroup } = that.state;
    if (args.requestType === "save" && args.action === "add") {
      args.cancel = true;
      if (args.data.buyerName != null && args.data.buyerId != null && args.data.productType != null && selectedProductTypeGroup != null && selectedTestPlanGroup != null) {
        let newBuyerMapping = new BuyerTestPlanMappingModel(null, args.data.buyerName, args.data.buyerId, args.data.productType, selectedProductTypeGroup, selectedTestPlanGroup);
        this.saveBuyerMapping(newBuyerMapping);
      }
      else {
        this.toastObj.show(this.createToaster("Please Select All Fields and update!", "Information"));
        args.cancel = true;
      }
    }
    else if (args.requestType === "save" && args.action === "edit") {
      let newBuyerMapping = new BuyerTestPlanMappingModel(args.data.id,args.data.buyerName, args.data.buyerId, args.data.productType, selectedProductTypeGroup, selectedTestPlanGroup);
      this.saveBuyerMapping(newBuyerMapping);
    }
    else if (args.requestType === "beginEdit") {
        this.setState({selectedProductTypeGroup:args.rowData.productTypeGroup,selectedTestPlanGroup:args.rowData.testPlanGroupDTO})
    }
    else if (args.requestType === "cancel") {
      this.setState({selectedProductType:null,selectedProductTypeGroup:null,selectedTestPlanGroup:null})
    }
    else if (args.requestType === "delete") {
      this.deleteBuyerMapping(args.data[0].id);
    }

  }

  actionBeginTestPlan = (args) => {
    let that = this;
    if (args.requestType === "save") {
      this.setState({ isTestMF: false })
      args.cancel = true;
      const selectedLabTest = that.state.labTestMasterList.filter(item => {
        return item.name == args.data.name;
      })
      let model = Object.assign({}, that.state.testPlanModel);
      let micDTOs = [];
      selectedLabTest[0].micDTOs.map((item,i) => {
        micDTOs.push(new MICMasterModel(null, item.micCode, item.description, item.inputMinRange, item.inputMaxRange, item.passMinRange, item.passMaxRange, item.isManual, item.unit, selectedLabTest[0].id, new Date().valueOf()+i ))
      })
      let labTest = new LabTestMasterModel(null, selectedLabTest[0].name, selectedLabTest[0].description, selectedLabTest[0].code, selectedLabTest[0].testMethod, micDTOs, selectedLabTest[0].id);
        const {testGroupRowData} = that.state
        if(!model.labTestDTOs.find(item => item.name == args.data.name)){
            if(args.data.id){
                model.labTestDTOs.filter((item,i)=> {
                  if(item.id == args.data.id){
                    if(testGroupRowData){
                      model.labTestDTOs[i] = new LabTestMasterModel(testGroupRowData.id, testGroupRowData.name, testGroupRowData.description, testGroupRowData.code, testGroupRowData.testMethod, testGroupRowData.micDTOs, selectedLabTest[0].id);
                    }
                  }
                })
            }
            else if(args.data.pid){
              model.labTestDTOs.filter((item,i)=> {
                if(item.pid == args.data.pid){
                  if(testGroupRowData){
                    model.labTestDTOs[i] = new LabTestMasterModel(testGroupRowData.id, testGroupRowData.name, testGroupRowData.description, testGroupRowData.code, testGroupRowData.testMethod, testGroupRowData.micDTOs, selectedLabTest[0].id);
                  }
                }
              })
            }
            else{
              model.labTestDTOs.push(labTest);
            }
        }else{
          this.toastObj.show(this.createToaster("Test Name Already Exist", "Information"));
        }
      // model.labTestDTOs.push(labTest);
      let micInfos = [];
      let labInfos = [];

      model.labTestDTOs.map(labObj => {
        let micInfo = [];
        labObj.micDTOs.map((micObj,i) => {
          micInfos.push(new MICMasterModel(micObj.id, micObj.micCode, micObj.description, micObj.inputMinRange, micObj.inputMaxRange, micObj.passMinRange, micObj.passMaxRange, micObj.isManual, micObj.unit, labObj.pid, micObj.uiGridId));
          micInfo.push(new MICMasterModel(micObj.id, micObj.micCode, micObj.description, micObj.inputMinRange, micObj.inputMaxRange, micObj.passMinRange, micObj.passMaxRange, micObj.isManual, micObj.unit, labObj.pid, micObj.uiGridId));
        })
        labInfos.push(new LabTestMasterModel(labObj.id, labObj.name, labObj.description, labObj.code, labObj.testMethod, micInfo, labObj.pid));
      })
      model.labTestDTOs = labInfos;
      that.testGridInstance.childGrid.dataSource = micInfos;
      that.setState({ testPlanModel: model }, () => {
        that.testGridInstance.closeEdit();
        that.testGridInstance.refresh();
      })
    }
    if(args.requestType === "add"){
      this.setState({ isTestMF: true })
    }
    if(args.requestType === "cancel"){
      this.setState({ isTestMF: false })
    }
    if (args.requestType === "delete") {
      // console.log(args)
      // args.cancel = true;
      const selectedLabTests = that.state.testPlanModel.labTestDTOs.filter(item => {
        if (item.id) {
          return item.id != args.data[0].id;
        } else {
          return item.pid != args.data[0].pid;
        }

      })
      // let testPlanModel = {...that.state.testPlanModel};
      // testPlanModel.labTestDTOs = selectedLabTests;
      // that.setState(testPlanModel)
      this.toastObj.show(this.createToaster("MIC deleted successfuly", "Success"));
      that.state.testPlanModel.labTestDTOs = selectedLabTests
      that.testGridInstance.dataSource = selectedLabTests
      that.testGridInstance.refresh();

      // console.log(arryclon)
      // console.log(selectedLabTests)
      // that.setState({testPlanModel:{...that.state.testPlanModel,labTestDTOs:selectedLabTests}});
      // this.setState({testPlanModel:{...this.state.testPlanModel,labTestDTOs:selectedLabTests}})
      // this.state.testPlanModel.labTestDTOs = selectedLabTests;
    }
  }


  actionBeginMICMaster = (args) => {
    let that = this;
    if (args.requestType === "beginEdit") {
      this.setState({ editArgs: args.rowData, isManual: args.rowData.isManual })
    }
    if (args.requestType === "add") {
      this.setState({ isTestMF: true })
    }
    if (args.requestType === "cancel") {
      this.setState({ isTestMF: false })
    }
    if (args.requestType === "save") {
      args.cancel = true;
      args.data.isManual = that.state.isManual
      //  that.state.isManual=false;
      let labmodalarry = [...that.state.labModel.micDTOs]
      if (args.data.id || args.data.uiGridId) {
        labmodalarry.filter((item, index) => {
          if (args.data.id) {
            if (args.data.id == item.id) {
              labmodalarry[index].micCode = args.data.micCode
              labmodalarry[index].description = args.data.description
              labmodalarry[index].inputMinRange = args.data.inputMinRange
              labmodalarry[index].inputMaxRange = args.data.inputMaxRange
              labmodalarry[index].passMinRange = args.data.passMinRange
              labmodalarry[index].isManual = args.data.isManual
              labmodalarry[index].unit = args.data.unit
            }
          }
          if (args.data.uiGridId) {
            if (args.data.uiGridId == item.uiGridId) {

              labmodalarry[index].micCode = args.data.micCode
              labmodalarry[index].description = args.data.description
              labmodalarry[index].inputMinRange = args.data.inputMinRange
              labmodalarry[index].inputMaxRange = args.data.inputMaxRange
              labmodalarry[index].passMinRange = args.data.passMinRange
              labmodalarry[index].isManual = args.data.isManual
              labmodalarry[index].unit = args.data.unit

            }
          }

        })
      } else {
        labmodalarry.push(new MICMasterModel(null, args.data.micCode, args.data.description, args.data.inputMinRange, args.data.inputMaxRange, args.data.passMinRange, args.data.passMaxRange, args.data.isManual, args.data.unit, null, new Date().valueOf()))
      }

      that.setState({ labModel: { ...that.state.labModel, micDTOs: labmodalarry }, isTestMF: false, isManual: false });

    }
    if (args.requestType === "delete") {
      args.cancel = true;
      this.setState({ isTestMF: false })
      const selectedMICs = this.state.labModel.micDTOs.filter(item => {
        if (item.id) {
          return item.id != args.data[0].id;
        }
        else {
          return item.uiGridId != args.data[0].uiGridId;
        }

      })
      this.toastObj.show(this.createToaster("MIC deleted successfuly", "Success"));
      this.setState({ labModel: { ...this.state.labModel, micDTOs: selectedMICs } })
      // that.state.labModel.micDTOs = selectedMICs;
      // that.gridInstance.dataSource = selectedMICs;
    }
  }

  onSaveTestPlan = () => {
    if (this.state.isTestMF) {
      this.toastObj.show(this.createToaster("Please fill all fiels and save", "Information"));
    } else {
      this.saveTestPlan(this.state.testPlanModel);
    }
    
  }

  onSaveLabTest = () => {
    const {name,testMethod,description,micDTOs} = this.state.labModel
    if(name == null || testMethod == null || description == null || micDTOs.length < 1){
      this.toastObj.show(this.createToaster(`Please fill ${name == null ? 'Test Name': testMethod == null ? 'Test Method' : description == null ? 'Description' : micDTOs.length < 1 ? 'MIC' :""}`, "Information"));
    }else{
      if (this.state.isTestMF) {
        this.toastObj.show(this.createToaster("Please fill all fiels and save", "Information"));
      } else {
        this.saveLabTest(this.state.labModel);
      }
    }
    
  }

numericParams = {
    params: {
        decimals: 0,
        format: "N",
        validateDecimalOnType: true
    }
};

  render() {
    return (<div className='control-pane' id='targetElementQuest'>
      <div className='control-section col-lg-12 defaultDialogComponent dialog-target'>
        <Row >
          <Col sm='12'>
            <GridComponent
              enableHover={true}
              allowFiltering={false}
              allowSorting={true}
              allowSelection={true}
              allowResizing={true}
              rowHeight={25}
              allowPaging={true}
              allowTextWrap={true}
              height={AppConfig.gridHeight}
              filterSettings={AppConfig.FilterSettingsModel}
              editSettings={AppConfig.editSettings}
              pageSettings={AppConfig.pageSettings}
              dataSource={this.state.buyerMappings}
              toolbar={this.state.toolbarOptions}
              actionBegin={this.actionBeginBuyerMapping}
              ref={grid => this.buyermappinggridInstance = grid}>
              <ColumnsDirective>
                <ColumnDirective field='buyerName' headerText='Buyer Name' textAlign='left' width='100' validationRules={{ required: true }} editType='dropdownedit' edit={this.buyerParams}></ColumnDirective>
                <ColumnDirective field='buyerId' headerText='Buyer Code' textAlign='left' width='100' allowEditing={false} ></ColumnDirective>
                <ColumnDirective field='productType' headerText='Product Type' textAlign='left' width='100' validationRules={{ required: true }} editType='dropdownedit' editTemplate={this.productTypeTemplate}></ColumnDirective>
                <ColumnDirective field='testPlanGroup' headerText='Test Plan Group' textAlign='left' width='100' validationRules={{ required: true }} editType='dropdownedit' editTemplate={this.testPlanGroupTemplate}></ColumnDirective>
              </ColumnsDirective>
              <Inject services={[Page, Toolbar, Edit]} />
            </GridComponent>
            <div className='mt-1'></div>
            <Row className='float-right'>
              <div className='mr-4'>
                <Button className='e-primary' onClick={this.testNameClick}>Add/Edit Test plan Group</Button>
                <Button className='e-primary' onClick={this.testMethodClick}>Add/Edit Test Method</Button>
              </div>
            </Row>
          </Col>


          <DialogComponent id="defaultDialog" showCloseIcon={true} animationSettings={this.animationSettings} isModal={true} allowDragging={true} visible={this.state.testName} width={'70%'} ref={dialog => this.testPlanDialogInstance = dialog}
            header='Test Plan Group' close={this.dialogClose.bind(this)} buttons={this.testPlanButtons}>
            <Row>
              <Col><DropDownListComponent id='testPlanGroup' placeholder="Select a Test Plan Group" validationRules={{ required: true }} floatLabelType="Auto" fields={{ text: 'testPlanGroupCode', value: 'testPlanGroupCode' }}
                dataSource={this.state.testPlanGroupListForDialogue} change={this.onChangeTestPlanGroupDialogue} ref={grid => this.testPlanDropDownRef = grid} /></Col>
              <Col><TextBoxComponent className={`${this.state.newTestPlan ? 'remove' : null}`} placeholder="Plan Group" floatLabelType="Auto" validationRules={{ required: true }} id="testPlanGroupName" disabled={!this.state.newTestPlan} value={this.state.testPlanModel.testPlanGroupName} change={this.onChangeTestPlanTextField} /></Col>
              <Col><TextBoxComponent className={`${this.state.newTestPlan ? 'remove' : null}`} placeholder="Group Code" floatLabelType="Auto" validationRules={{ required: true }} id="testPlanGroupCode" disabled={!this.state.newTestPlan} value={this.state.testPlanModel.testPlanGroupCode} change={this.onChangeTestPlanTextField} /></Col>
              <Col sm='12' /* hidden={!this.state.testName ? true : false} */>
                <div className='mt-3'></div>
                <GridComponent
                  enableHover={true}
                  allowFiltering={false}
                  allowSorting={true}
                  allowSelection={true}
                  allowResizing={true}
                  rowHeight={25}
                  allowPaging={true}
                  allowTextWrap={true}
                  /* height={AppConfig.gridHeight} */
                  filterSettings={AppConfig.FilterSettingsModel}
                  editSettings={AppConfig.editSettings}
                  pageSettings={AppConfig.pageSettings}
                  dataSource={this.state.testPlanModel.labTestDTOs}
                  actionBegin={this.actionBeginTestPlan}
                  toolbar={this.state.toolbarOptions}
                  childGrid={this.childGrid}
                  ref={grid => this.testGridInstance = grid}>
                  <ColumnsDirective>
                    <ColumnDirective field='name' headerText='Test Name' textAlign='left' width='100' validationRules={{ required: true }} editTemplate={this.addOrEditTestPlanGroupTemplate}></ColumnDirective>
                    <ColumnDirective headerText='Action' foreignKeyField='Action' width='40' commands={this.commands} />
                  </ColumnsDirective>
                  <Inject services={[Page, Toolbar, DetailRow, Edit]} />
                </GridComponent>
              </Col>
            </Row>
          </DialogComponent>

          <DialogComponent id="defaultDialog" showCloseIcon={true} animationSettings={this.animationSettings} isModal={true} allowDragging={true} visible={this.state.testMethod} width={'70%'} ref={dialog => this.dialogInstance = dialog}
            header='Test Method' close={this.dialogClose.bind(this)} buttons={this.buttons}>
            <Row>
              <Col><DropDownListComponent id='labTest' placeholder="Select a Test Method" floatLabelType="Auto" fields={{ text: 'name', value: 'name' }}
                dataSource={this.state.labTestMasterListForDialogue} change={this.onChangeLabTestDialogue} ref={grid => this.dropDownRef = grid} /></Col>
              <Col><TextBoxComponent className={`${this.state.newLabTest ? 'remove' : null}`} placeholder="Test Name" id="name" floatLabelType="Auto" disabled={!this.state.newLabTest} value={this.state.labModel.name} change={this.onChangeTextField} /></Col>
              <Col><TextBoxComponent className={`${this.state.newLabTest ? 'remove' : null}`} placeholder="Test Method" id="testMethod" floatLabelType="Auto" disabled={!this.state.newLabTest} value={this.state.labModel.testMethod} change={this.onChangeTextField} /></Col>
              <Col><TextBoxComponent className={`${this.state.newLabTest ? 'remove' : null}`} placeholder="Test Description" id="description" floatLabelType="Auto" disabled={!this.state.newLabTest} value={this.state.labModel.description} change={this.onChangeTextField} /></Col>
              <Col sm='12' /* hidden={!this.state.testName ? true : false} */>
                <div className='mt-3'></div>
                <GridComponent
                  enableHover={true}
                  allowFiltering={false}
                  allowSorting={true}
                  allowSelection={true}
                  allowResizing={true}
                  rowHeight={25}
                  allowPaging={true}
                  allowTextWrap={true}
                  /* height={AppConfig.gridHeight} */
                  filterSettings={AppConfig.FilterSettingsModel}
                  editSettings={AppConfig.editSettings}
                  pageSettings={AppConfig.pageSettings}
                  dataSource={this.state.labModel.micDTOs}
                  toolbar={this.state.toolbarOptions}
                  ref={grid => this.gridInstance = grid}
                  actionBegin={this.actionBeginMICMaster}>
                  <ColumnsDirective>
                    <ColumnDirective field='micCode' headerText='MIC' textAlign='left' width='150' validationRules={{ required: true }}></ColumnDirective>
                    <ColumnDirective field='description' headerText='MIC Description' textAlign='left' width='150' validationRules={{ required: true }}></ColumnDirective>
                    <ColumnDirective field='inputMinRange' headerText='Input Minimum Range' textAlign='left' width='150' edit={this.numericParams} editType='numericedit' validationRules={{ required: true }}></ColumnDirective>
                    <ColumnDirective field='inputMaxRange' headerText='Input Maximum Range' textAlign='left' width='150' edit={this.numericParams} editType='numericedit' validationRules={{ required: true }}></ColumnDirective>
                    <ColumnDirective field='passMinRange' headerText='Pass Minimum Range' textAlign='left' width='150' edit={this.numericParams} editType='numericedit' validationRules={{ required: true }}></ColumnDirective>
                    <ColumnDirective field='passMaxRange' headerText='Pass Maximum Range' textAlign='left' width='150' edit={this.numericParams} editType='numericedit' validationRules={{ required: true }}></ColumnDirective>
                    <ColumnDirective field='unit' headerText='Unit' textAlign='left' width='150' validationRules={{ required: true }}></ColumnDirective>
                    <ColumnDirective field='isManual' headerText='Manual' disabled={true} template={this.manualTemplate} editTemplate={this.manualTemplate} textAlign='Center' width='150' />
                  </ColumnsDirective>
                  <Inject services={[Page, Toolbar, DetailRow, Edit]} />
                </GridComponent>
              </Col>
            </Row>
          </DialogComponent>

        </Row>
      </div>
      <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
    </div>);

  }
}

export default DefectLabMasterComponent;
