import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation, Trans } from "react-i18next";
import noaccess from '../../../assets/img/no-access.png'; 
class Noaccess extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    const { t, i18n } = this.props;

    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    return (
      <div className="blank-page">
    
        <div className="messageBox">
          <img src={noaccess} alt="img"/>
          <h3>You are not authorized for Application</h3>
          {/* <h4>No Access for Module</h4> */}
        </div>
      </div>
    );
  }
}
export default withTranslation("translations")(Noaccess); 
