import React from 'react';
import { DropDownListComponent, revertHighlightSearch } from '@syncfusion/ej2-react-dropdowns';
import ReactDOM from 'react-dom';
import { Toolbar, Search, GridComponent, Resize, Group, ColumnsDirective, CommandColumn, ColumnDirective, Filter, Reorder, Inject, Selection, VirtualScroll, Edit, Page, Sort, FilterSettingsModel, IFilter } from '@syncfusion/ej2-react-grids';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { CheckBoxComponent, RadioButtonComponent, SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { UserProfileModel } from './UserProfileModel';
import { UserProfileFiltersModel } from './UserProfileFiltersModel'
import UserProfileService from './UserProfileService';
import { withTranslation, Trans } from "react-i18next";
import errorMessages from '../../errorHandler/errorMessages';
import cloneDeep from 'lodash/cloneDeep';
import SplitterLayout from 'react-splitter-layout';
import UserProductionModel from './UserProductionModel'
import 'react-splitter-layout/lib/index.css';
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';

import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { element } from 'prop-types';
let refresh;

class UserProfiling extends React.PureComponent {

    constructor() {
        super();

        this.userProfileService = new UserProfileService();

        this.initialErrorStatus = {
            firstName: false,
            lastName: false,
            email: false,
            phone: false,
            radiantUserId: false,
            managerId: false,
            username: false,
            password: false,
            confirmPassword: false,
            plantId: false,
            units: false,
            roles: false,
        };

        this.state = {
            userData: [],
            isSidebar: true,
            isEdit: false,
            isShowConfirmationDialog: false,
            filterSettings: { type: 'CheckBox' },
            toolbarOptions: ['Search'],
            toasterMessage: [
                { title: 'Warning!', content: 'Changes Discarded !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Success!', content: 'User added successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
                { title: 'Information!', content: 'Please read the comments carefully.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Success!', content: 'User updated successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Success!', content: 'User deleted successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Success!', content: 'User saved successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'User already exists!!', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },

            ],

            animationSettings: { effect: 'FlipYLeft' },
            confirmButton: [{
                click: (args) => {
                    this.delete(args);
                },
                buttonModel: { content: 'Yes', isPrimary: true }
            },
            {
                click: (args) => {
                    this.cancelConformDialog(args);
                },
                buttonModel: { content: 'No' }
            }],
            model: new UserProfileModel(null, null, null, null, null, null, null, null, null, null, [], [], [], [], [], null, null, null, [], [], [], []),
            errorObject: this.initialErrorStatus,
            validationErrorCss: "custom-error",
            commands: [
                { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
                // { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },
            ],
            selectedPlantName: null,
            unitList: [],
            plantList: [],
            lineProcesses: [],
            resetLineProcesses: [],
            QaList: [],
            apmList: [],
            mrList: [],
            peList: [],
            groups: [],
            ProductionList: [],

        }
        this.groupOptions = { showGroupedColumn: false, };
        this.rolesList = [];
        this.fields = { text: 'role', value: 'Id' };
        this.value = 'role';

        this.groupList = [];
        this.groupFields = { text: 'group', value: 'Id' };
        this.groupValue = 'group';

        this.plantFields = { text: 'plant', value: 'Id' };
        this.plantValue = 'plant';

        this.unitFields = { text: 'unit', value: 'Id' };
        this.unitValue = 'unit';
        this.MechandiserList = [];

        this.QaList = [];
        this.qaFields = { text: 'qa', value: 'Id' };
        this.qaValue = 'qa';

        this.ProductionList = [];
        this.roles = []
        this.groups = []
        this.plants = []
        this.mechandiser = []
        this.units = []
        this.qa = []
        this.apm = []
        this.pe = []
        this.production = []
        this.getUserData()
    }

    componentDidMount() {
        this.userRoles()
        this.userGroups()
        this.getUserData()
        this.userPlants()
        this.userMechandiser()
        this.getGridData()
        this.getMechandiser()
        this.getQa()
        this.getPE();
        this.getAPM()
        this.getProductionData()
        this.userGroups()
        document.addEventListener("keydown", this.escFunction, false);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.escFunction, false);
    }

    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.setState({ isSidebar: true, isEdit: false })
            document.body.classList.remove('overflow-hidden');
        }
    }

    async getUserData() {
        let responseData = (await this.userProfileService.getUsers());
        let users = [];
        for (let i = 0; i < responseData.length; i++) {
            for (let j = 0; j < responseData[i].plants.length; j++) {
                responseData[i].plants[j] = parseInt(responseData[i].plants[j])
            }
            for (let j = 0; j < responseData[i].units.length; j++) {
                responseData[i].units[j] = parseInt(responseData[i].units[j])
            }
        }
        responseData.map(element => {
            
            users.push(new UserProfileModel(element.id, element.firstName, element.lastName, element.email, element.phone, element.radiantUserId, element.managerId, element.password, element.confirmPassword,
                element.plantName, element.units, element.plants, element.orderFilters, element.groups, element.roles, element.description, element.username, element.active?"Active":"InActive", [], [], [], []))
        })
        this.setState({ userData: users })
    }

    async getRoleData() {
        const responseData = (await this.userProfileService.getRoles())
        return responseData
    }

    async getUnitData(plants) {
        const responseData = (await this.userProfileService.getUnits(plants))
        return responseData
    }

    async getMechandiser() {
        const responseData = (await this.userProfileService.getMechandiserList())
        this.setState({ mrList: responseData.data })
    }

    async getQa() {
        const responseData = (await this.userProfileService.getQaList())
        this.setState({ QaList: responseData.data })
    }

    async getAPM() {
        const responseData = (await this.userProfileService.getAPMList())
        this.setState({ apmList: responseData.data })
    }

    async getPE() {
        const responseData = (await this.userProfileService.getPEList())
        this.setState({ peList: responseData.data })
    }

    async getGroupData() {
        const responseData = (await this.userProfileService.getGroups())
        return responseData
    }

    async getProductionData() {
        const responseData = (await this.userProfileService.getProductionMgrList())
        this.setState({ ProductionList: responseData.data })
    }

    async userGroups() {
        const response = await this.userProfileService.getgroupList()
        this.setState({ groups: response.data })
    }

    async userRoles() {
        const response = await this.getRoleData()
        if (response != undefined)
            response.data.forEach(roleObj => {
                this.rolesList.push({ "Id": roleObj.id, "role": roleObj.name })
            });
    }
    async userPlants() {
        const response = await this.userProfileService.getPlantList()
        this.state.plantList = response.data.data;
    }


    async getUnits(plantIds,unitIds) {
        const response = await this.getUnitData(plantIds)
        if ((response != undefined) && (response.data.status == "success")) {
            let units = [];
            let unitIdObject = (response.data.data.filter(element => {
                return (plantIds).includes(element.plant.id);
            })).filter(element2 => {
                if ((unitIds).includes(element2.id))
                    return element2.id;
            })
            unitIdObject.forEach(element => {
                units.push(element.id);
            })
            this.setState({ unitList: response.data.data, model: { ...this.state.model, units } })
        }
    }


    async userMechandiser() {
        const response = await this.getMechandiser()
        this.plantList = response;
    }

    async userQa() {
        const response = await this.getQa()
        this.QaList = response.data.data;
    }

    async userPe() {
        const response = await this.getPE()
        this.peList = response.data.data;
    }
    async userApm() {
        const response = await this.getAPM()
        this.apmList = response.data.data;
    }
    async userProduction() {
        const response = await this.getProductionData()
        this.ProductionList = response;
    }


    async onEditClick(selectedRow) {
        
        let data = cloneDeep(selectedRow);
        await this.getUnits(data.plants,data.units);
        data.orderFilters.map(element => {
            if (element.roleName == 'MR')
                data.mr = element.assignedUserIds;
            if (element.roleName == 'PE')
                data.pe = element.assignedUserIds;
            if (element.roleName == 'APM')
                data.apm = element.assignedUserIds;
            if (element.roleName == 'QAMGR')
                data.qa = element.assignedUserIds;
        })
        
        if(data.active =='Active'){
               data.active =true
        }else{
             data.active=false
        }
        this.setState({ model: data, isSidebar: false, isEdit: true })
    }

    rowselected = (args) => {                     // TO BE IMPROVED !!
        let that = this;
        if ((args.target != null) && (args.target.classList.length > 2) && (that.state.isSidebar == true)) {
            if ((args.target.classList.contains('e-edit')) && (args.data != undefined)) {
                that.onEditClick(args.data)
            }
            if ((args.target.classList.contains('e-delete')) && (args.data != undefined)) {
                // delete
                args.cancel = true;
                const id = args.data.id;
                this.setState({ isShowConfirmationDialog: true, lineIdToDelete: id })
            }
        }
    }

    onRemoveFile(args) {
        args.postRawFile = false;
    }

    clearButtonClick() {
        this.uploadObj.clearAll();
    }

    load() {
        refresh = this.refreshing;
    }

    statusTemplate(event) {
        return (<div className={event.active == 'Active' ? 'Active' : 'Inactive'}>{event.active == 'Active' ? 'Active' : 'Inactive'}</div>);
    }

    UserAccess() {
        return (
            <div>
                <div className="mr-3 float-left"> <CheckBoxComponent checked={true} label='Read'></CheckBoxComponent></div>
                <CheckBoxComponent checked={false} label='Write'></CheckBoxComponent>
            </div>
        )
    }

    async insertUser(data) {
        const { t, i18n } = this.props;
        data.active = true; // by default true
        const responseData = (await this.userProfileService.saveUser(data))
        if ((responseData != undefined) && (responseData.status == "success")) {
            this.getUserData()
            this.toastObj.show(this.state.toasterMessage[6]);
            this.setState({ isSidebar: true, isEdit: false, lineProcesses: this.state.resetLineProcesses, model: new UserProfileModel(null, null, null, null, null, null, null, null, null, null, [], [], [], [], [], null, null, null, [], [], [], []), errorObject: this.initialErrorStatus })
        }

        else if ((responseData != undefined) && (responseData.status == "fail")) {
            this.setState({ isDisabled: false, isEdit: false, toasterPosition: { X: 'Left', Y: 'Bottom' } })
            this.toastObj.show(this.createToaster(t(errorMessages[responseData.data.data.errorCode])));
        }
        else if ((responseData != undefined) && (responseData.status == "unsuccess")) {
            if(responseData.message != null){
                this.toastObj.show(this.createToaster(responseData.message, "Error"));
            }else{
                this.toastObj.show(this.state.toasterMessage[7]);
            }
            this.setState({ isEdit: false, lineProcesses: this.state.resetLineProcesses, errorObject: this.initialErrorStatus })
        }
        else {
            this.toastObj.show(this.state.toasterMessage[2]);
            this.setState({ isEdit: false, lineProcesses: this.state.resetLineProcesses, errorObject: this.initialErrorStatus })
        }
    }

    save = () => {
        const { model } = this.state;
        document.body.classList.remove('overflow-hidden');
        this.state.errorObject.username = false
        if (this.isValidate(model)) {
            let dummyModel = cloneDeep(model);
            if (!this.state.isEdit) {
                this.setState({ isDisabled: true }, () => {
                    this.insertUser(this.modifyModel(dummyModel));
                })
            }
            else {
                this.editLine(this.modifyModel(dummyModel));
            }
        }
    }

    modifyModel = (model) => {
        const { mrList, peList, apmList, QaList } = this.state;
        let orderFilters = [];
        if ((model.mr.length > 0) || (this.getOrderFilterId(model.orderFilters, 'MR') != null))
            orderFilters.push(new UserProfileFiltersModel(this.getOrderFilterId(model.orderFilters, 'MR'), model.mr, this.getUserNames(model.mr, mrList), 'MR'));
        if ((model.apm.length > 0) || (this.getOrderFilterId(model.orderFilters, 'APM') != null))
            orderFilters.push(new UserProfileFiltersModel(this.getOrderFilterId(model.orderFilters, 'APM'), model.apm, this.getUserNames(model.apm, apmList), 'APM'));
        if ((model.pe.length > 0) || (this.getOrderFilterId(model.orderFilters, 'PE') != null))
            orderFilters.push(new UserProfileFiltersModel(this.getOrderFilterId(model.orderFilters, 'PE'), model.pe, this.getUserNames(model.pe, peList), 'PE'));
        if ((model.qa.length > 0) || (this.getOrderFilterId(model.orderFilters, 'QAMGR') != null))
            orderFilters.push(new UserProfileFiltersModel(this.getOrderFilterId(model.orderFilters, 'QAMGR'), model.qa, this.getUserNames(model.qa, QaList), 'QAMGR'));   
        model.orderFilters = orderFilters;
        delete model.mr;
        delete model.apm;
        delete model.qa;
        delete model.pe;
        return model;
    }

    getOrderFilterId = (orderList, type) => {
        const { isEdit } = this.state;
        let id = null;
        if (isEdit)
            orderList.forEach(element => {
                if (element.roleName == type)
                    id = element.id;
            })
        return id;
    }

    getUserNames(idList, nameList) {
        let userNames = [];
        idList.map(itemId => {
            nameList.find(itemName => {
                if (itemName.radiantUserId == itemId) {
                    userNames.push(itemName.firstName)
                }
            })
        })
        return userNames;
    }

    isValidate(model) {
        let isValidate = true;
        let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        let phoneno = /^\d{10}$/;
        let passw = /^\w{7,14}$/;
        let password = model.password
        let pass2 = model.confirmPassword
        let errorObj = Object.assign({}, this.state.errorObject);
        if (model.firstName == null) {
            errorObj.firstName = true;
            isValidate = false;
        }
        if (model.lastName == null) {
            errorObj.lastName = true;
            isValidate = false;
        }
        if (!mailformat.test(model.email) || model.email == null) {
            errorObj.email = true;
            isValidate = false;
        }

        if (!phoneno.test(model.phone)) {
            errorObj.phone = true;
            isValidate = false;
        }
        if (model.radiantUserId == null) {
            errorObj.radiantUserId = true;
            isValidate = false;
        }
        if (model.managerId == null) {
            errorObj.managerId = true;
            isValidate = false;

        }
        if (model.username == null) {
            errorObj.username = true;
            isValidate = false;

        }
        if (!this.state.isEdit) {
            if (model.password == null) {
                errorObj.password = true;
                isValidate = false;
            }
            if (model.password != null && (model.password.length < 7 || model.password.length > 14)) {
                errorObj.password = true;
                isValidate = false;
            }
            if (model.password !== null && password !== pass2) {
                errorObj.confirmPassword = true;
                isValidate = false;
            }
        }
        // if (model.units.length != 0 && model.plants.length == 0) {
        //     errorObj.plantId = true;
        //     isValidate = false;
        // }
        // if (model.plants.length != 0 && model.units.length == 0) {
        //     errorObj.units = true;

        //     isValidate = false;
        // }
        if (model.roles.length == 0 && model.groups.length == 0) {
            errorObj.roles = true;
            isValidate = false;
        }

        this.setState({ errorObject: errorObj })
        return isValidate;
    }

    onChangeIsActive = (event) => {
        this.setState({ model: { ...this.state.model, active: event.checked  } })
    }

    sideBar = (event) => {
        document.body.classList.toggle('overflow-hidden');
        this.setState({ isSidebar: !this.state.isSidebar, isEdit: false, errorObject: this.initialErrorStatus, model: new UserProfileModel(null, null, null, null, null, null, null, null, null, null, [], [], [], [], [], null, null, null, [], [], [], []) })
    }
    handleChange = (event) => {
        let that = this;
        that.setState({ model: { ...that.state.model, [event.target.id]: event.target.value }, errorObject: { ...this.state.errorObject, [event.target.id]: false } })
    }
    handleRole = (event) => {
        let that = this;
        that.setState({ model: { ...that.state.model, 'roles': event.value }, errorObject: { ...this.state.errorObject, [event.element.id]: false } })
    }

    handleGroup = (event) => {
        let that = this;
        that.setState({ model: { ...that.state.model, 'groups': event.value }, errorObject: { ...this.state.errorObject, [event.element.id]: false } })
    }

    handleUnit = (event) => {
        let that = this;
        // if (event.e != undefined)
            that.setState({ model: { ...that.state.model, 'units': event.value }, errorObject: { ...this.state.errorObject, [event.element.id]: false } })
    }

    onChangeSupervisor = (event) => {
        const { model } = this.state;
        if (event.e != undefined)
            this.setState({ model: { ...model, [event.element.id]: event.value }, errorObject: { ...this.state.errorObject, [event.element.id]: false } })
    }

    onChangePlant = (event) => {
        let that = this;
        // if (event.e != undefined) {
            that.setState({ model: { ...that.state.model, plants: event.value }, errorObject: { ...this.state.errorObject, [event.element.id]: false } })
            if (event.value.length > 0) {
                that.setState({ unitList: [] }, () => {
                    that.getUnits(event.value,this.state.model.units)
                })
            }
            if (event.value.length == 0)
                that.setState({ unitList: [], model: { ...that.state.model, units: [] } })
        // }
    }

    filterOrder = (event) => {
        let that = this;
        // if (event.e != undefined)
            that.setState({ model: { ...that.state.model, [event.element.id]: event.value } })
    }

    async getGridData() {
        let gridData = [];
        let response = (await this.userProfileService.getGridData())
        if ((response != undefined) && (response.data.status == "success")) {
            response.data.data.map(lineObj => {
                let lineProcesses = [];
                gridData.push(new UserProductionModel(Number(lineObj.id), lineObj.unit.plant.name, lineObj.lineName, lineObj.unit.name, lineObj.status, lineObj.isInternal, lineObj.numberOfMachines, lineObj.categories[0], lineProcesses))
            })
            this.setState({ tableData: gridData })
        }
    }

    async editLine(editedLine) {
        let response = (await this.userProfileService.editLine(editedLine))
        if ((response != undefined) && (response.data.status == "success")) {
            this.getUserData();
            this.toastObj.show(this.state.toasterMessage[4]);
        }else if ((response.data != undefined) && (response.data.status == "unsuccess")) {
                this.toastObj.show( this.createToaster(response.data.message, "Error"));
        }else {
            this.toastObj.show(this.state.toasterMessage[2]);
        }
        this.setState({ isSidebar: true, isEdit: false, lineProcesses: this.state.resetLineProcesses, model: new UserProfileModel(null, null, null, null, null, null, null, null, null, null, [], [], [], [], [], null, null, null, [], [], [], []), errorObject: this.initialErrorStatus, })
    }



    createToaster(message, type) {
        switch (type) {
          case "Success":
            return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
          case "Error":
            return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
          case "Warning":
            return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
          case "Information":
            return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
      }





    async deleteUser(deleteUserId) {
        let response = (await this.userProfileService.deleteUser(deleteUserId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.getGridData();
            this.getUserData();
            this.toastObj.show(this.state.toasterMessage[5]);
        } else {
            this.toastObj.show(this.state.toasterMessage[2]);
        }
        this.setState({ lineIdToDelete: null });
    }

    delete = (args) => {
        const { lineIdToDelete } = this.state;
        this.deleteUser(lineIdToDelete);
        this.setState({ isShowConfirmationDialog: false })
        setTimeout(() => {
            this.getUserData()
            this.setState({ isSidebar: true, isEdit: false, lineProcesses: this.state.resetLineProcesses, model: new UserProfileModel(null, null, null, null, null, null, null, null, null, null, [], [], [], [], [], null, null, null, [], [], [], []) })
        }, 2);
    }

    cancelConformDialog = (args) => {
        this.setState({ isShowConfirmationDialog: false, lineIdToDelete: null })
    }

    createToaster(message) {
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
    }

    render() { 
        const { t, i18n } = this.props;
        return (
            <div>
                <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
                <div className="container-fluid-inner">
                    <h1 className='pagesheading'>User Management</h1>
                    <div className='clearfix'></div>
                    <Row>
                        <Col>
                            <Button color="primary" size="sm" className="mb-2" onClick={e => this.sideBar(e)}>{t("cpm.lm.line.adduser")}</Button>

                            <GridComponent toolbar={this.state.toolbarOptions} allowFiltering={true} allowResizing={true} filterSettings={this.state.filterSettings} allowTextWrap={true} gridLines='Both' dataSource={this.state.userData} actionBegin={this.actionBegin} allowPaging={true} ref={grid => this.gridInstance = grid} pageSettings={{ pageCount: 10 }} allowGrouping={true} rowSelected={this.rowselected} groupSettings={this.groupOptions} allowSorting={true} load={this.load}>
                                <ColumnsDirective>
                                    <ColumnDirective field='username' headerText='User Name' width='150'></ColumnDirective>
                                    <ColumnDirective field='firstName' headerText='First Name' width='150'></ColumnDirective>
                                    <ColumnDirective field='lastName' headerText='Last Name' width='150'></ColumnDirective>
                                    <ColumnDirective field='groups' headerText='Groups Name' width='120' />
                                    <ColumnDirective field='roles' headerText='Assign Role ' width='120' />
                                    <ColumnDirective field='active' headerText='Status' width='80' template={this.statusTemplate} />
                                    <ColumnDirective field='email' headerText='Email ID' width='200'></ColumnDirective>
                                    <ColumnDirective field='phone' headerText='Phone' width='90'></ColumnDirective>
                                    <ColumnDirective field='description' allowFiltering={false} headerText='Description'></ColumnDirective>
                                    <ColumnDirective field='action' textAlign='Center' allowSorting={false} allowFiltering={false} headerText={'Action'} width='70' commands={this.state.commands} ></ColumnDirective>
                                </ColumnsDirective>
                                <Inject services={[CommandColumn, Resize, Page, Selection, Group, Sort, Edit, Search,Toolbar, Filter]} />
                            </GridComponent>


                            {/* ----------------------- Add User Section  -----------------------  */}
                            <div className="add-line-section" hidden={this.state.isSidebar}>
                                <SplitterLayout secondaryInitialSize={750} secondaryMinSize={750}>
                                    <div> </div>
                                    <div className="overflow-hidden add-line-section-form">
                                        <div className="heading mb-2">{this.state.isEdit ? t("cpm.lm.line.edituser") : t("cpm.lm.line.adduser")}  <a className="close" onClick={e => this.sideBar(e)}>+</a></div>
                                        <Col xs='12'>
                                            <Row>
                                                <Col xs='12' md='6'>
                                                    <div className={this.state.errorObject.firstName == true ? this.state.validationErrorCss : "required-field"} >
                                                        <TextBoxComponent color="primary" placeholder='First name' id="firstName" value={this.state.model.firstName} floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.firstName} >{t("cpm.lm.line.validation.firstName")}</span>

                                                    </div>
                                                </Col>
                                                <Col xs='12' md='6'>
                                                    <div className={this.state.errorObject.lastName == true ? this.state.validationErrorCss : "required-field"}>
                                                        <TextBoxComponent placeholder='Last name' id="lastName" value={this.state.model.lastName} floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.lastName} >{t("cpm.lm.line.validation.lastName")}</span>

                                                    </div>

                                                </Col>

                                            </Row>
                                            <div className="mt-3"></div>

                                            <Row>
                                                <Col xs='12' md='6'>
                                                    <div className={this.state.errorObject.email == true ? this.state.validationErrorCss : "required-field"}>
                                                        <TextBoxComponent placeholder='Email ID' id="email" value={this.state.model.email} floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.email} >{t("cpm.lm.line.validation.email")}</span>
                                                    </div>
                                                </Col>

                                                <Col xs='12' md='6'>
                                                    <div className={this.state.errorObject.phone == true ? this.state.validationErrorCss : "required-field"}>
                                                        <TextBoxComponent placeholder='Phone' id="phone" value={this.state.model.phone} floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.phone} >{t("cpm.lm.line.validation.phone")}</span>

                                                    </div>
                                                </Col>
                                            </Row>
                                            <div className="mt-3"></div>

                                            <Row>
                                                <Col xs='12' md='6'>
                                                    <div className={this.state.errorObject.radiantUserId == true ? this.state.validationErrorCss : "required-field"}>
                                                        <TextBoxComponent placeholder='Radiant Employee ID' id="radiantUserId" value={this.state.model.radiantUserId} floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.radiantUserId} >{t("cpm.lm.line.validation.userId")}</span>

                                                    </div>
                                                </Col>
                                                <Col xs='12' md='6'>
                                                    <div className={this.state.errorObject.managerId == true ? this.state.validationErrorCss : "required-field"}>

                                                        <TextBoxComponent required={true} placeholder='Manager’s Employee ID' id="managerId" value={this.state.model.managerId} floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.managerId} >{t("cpm.lm.line.validation.managerId")}</span>
                                                    </div>
                                                </Col>
                                            </Row>
                                            <div className="mt-3"></div>
                                            <Row>
                                                <Col xs='12' md='6'>
                                                    <div className={this.state.errorObject.username == true ? this.state.validationErrorCss : "required-field"}>

                                                        <TextBoxComponent placeholder='Username' id="username" value={this.state.model.username} floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.username} >{t("cpm.lm.line.validation.username")}</span>

                                                    </div>
                                                </Col>

                                                <div hidden={!this.state.isEdit}>
                                                    <Col xs='12' md='6'>
                                                        <span className="mr-3"> Active</span>
                    
                                                        <SwitchComponent checked={this.state.model.active} change={this.onChangeIsActive} />
                                                    </Col>
                                                </div>

                                            </Row>
                                            <div className="mt-3"></div>
                                            <Row>
                                                <Col xs='12' md='6'>
                                                    <div hidden={this.state.isEdit} className={this.state.errorObject.password == true ? this.state.validationErrorCss : "required-field"}>

                                                        <TextBoxComponent type="password" value={this.state.model.password} placeholder='Password' id="password" floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.password} >{t("cpm.lm.line.validation.password")}</span>

                                                    </div>
                                                </Col>
                                                <Col xs='12' md='6'>
                                                    <div hidden={this.state.isEdit} className={this.state.errorObject.confirmPassword == true ? this.state.validationErrorCss : "required-field"}>

                                                        <TextBoxComponent type="password" value={this.state.model.confirmPassword} placeholder='Confirm Password' id="confirmPassword" floatLabelType="Auto" onChange={this.handleChange} />
                                                        <span hidden={!this.state.errorObject.confirmPassword} >{t("cpm.lm.line.validation.confPassword")}</span>
                                                    </div>
                                                </Col>
                                            </Row>

                                            <div className="mt-3"></div>

                                            <Row>
                                                <Col xs='6' md='6'>
                                                    {/* <div className={this.state.errorObject.plantId == true ? this.state.validationErrorCss : "required-field"}> */}
                                                        {/* 
                                                        <MultiSelectComponent id="plantId"  value={this.state.model.plants} dataSource={this.state.plantList} fields={{ text: 'name', value: 'id' }} 
                                                        placeholder="Select Plant" floatLabelType="Auto" change={this.onChangePlant} /> */}
                                                        <MultiSelectComponent id="plantId" ref={t => this.mulObjPlant = t} value={this.state.model.plants} dataSource={this.state.plantList}
                                                            fields={{ text: 'name', value: 'id' }} floatLabelType="Auto" placeholder="Select Plant" mode="CheckBox" showSelectAll={true}
                                                            showDropDownIcon={true} filterBarPlaceholder="Select Plant" popupHeight="350px" change={this.onChangePlant} >
                                                            <Inject services={[CheckBoxSelection]} />
                                                        </MultiSelectComponent>
                                                        {/* <span hidden={!this.state.errorObject.plantId} >{t("cpm.lm.line.validation.plants")}</span> */}
                                                    {/* </div> */} 
                                                </Col>

                                                <Col xs='6' md='6'>
                                                    {/* <div className={this.state.errorObject.units == true ? this.state.validationErrorCss : "required-field"}> */}
                                                        {/* <MultiSelectComponent id="units"  value={this.state.model.units} dataSource={this.state.unitList} fields={{ text: 'name', value: 'id' }}
                                                         placeholder="Select Unit" floatLabelType="Auto" change={this.handleUnit} /> */}
                                                        <MultiSelectComponent id="units" ref={t => this.mulObjUnit = t} value={this.state.model.units} dataSource={this.state.unitList}
                                                            fields={{ text: 'name', value: 'id' }} floatLabelType="Auto" placeholder="Select Unit" mode="CheckBox" showSelectAll={true}
                                                            showDropDownIcon={true} filterBarPlaceholder="Select Unit" popupHeight="350px" change={this.handleUnit} >
                                                            <Inject services={[CheckBoxSelection]} />
                                                        </MultiSelectComponent>
                                                        {/* <span hidden={!this.state.errorObject.units} >{t("cpm.lm.line.validation.units")}</span>
                                                    </div> */}
                                                </Col>
                                            </Row>

                                            {/* <div className="mt-3"></div>
                                            <Row>
                                                <Col xs='6' md='6'>
                                                    <MultiSelectComponent id="mr" ref={t => this.mulObjMr = t} value={this.state.model.mr} dataSource={this.state.mrList}
                                                        fields={{ text: 'firstName', value: 'radiantUserId' }} floatLabelType="Auto" placeholder="Select Mechandiser" mode="CheckBox" showSelectAll={true}
                                                        showDropDownIcon={true} filterBarPlaceholder="Search Mechandiser " popupHeight="350px" change={this.filterOrder} >
                                                        <Inject services={[CheckBoxSelection]} />
                                                    </MultiSelectComponent>
                                                </Col>
                                                <Col xs='6' md='6'>
                                                    <MultiSelectComponent id="apm" ref={t => this.mulObjApm = t} value={this.state.model.apm} dataSource={this.state.apmList}
                                                        fields={{ text: 'firstName', value: 'radiantUserId' }} floatLabelType="Auto" placeholder="Select APM" mode="CheckBox" showSelectAll={true}
                                                        showDropDownIcon={true} filterBarPlaceholder="Search APM " popupHeight="350px" change={this.filterOrder} >
                                                        <Inject services={[CheckBoxSelection]} />
                                                    </MultiSelectComponent>
                                                </Col>
                                            </Row>

                                            <div className="mt-3"></div>

                                            <Row>
                                                <Col xs='6' md='6'>
                                                    <MultiSelectComponent id="pe" ref={t => this.mulObjPe = t} value={this.state.model.pe} dataSource={this.state.peList}
                                                        fields={{ text: 'firstName', value: 'radiantUserId' }} floatLabelType="Auto" placeholder="Search Purchase Executive" mode="CheckBox" showSelectAll={true}
                                                        showDropDownIcon={true} filterBarPlaceholder="Search Purchase Executive " popupHeight="350px" change={this.filterOrder} >
                                                        <Inject services={[CheckBoxSelection]} />
                                                    </MultiSelectComponent>
                                                </Col>
                                                <Col xs='6' md='6'>
                                                    <MultiSelectComponent id="qa" ref={t => this.mulObjQa = t} value={this.state.model.qa} dataSource={this.state.QaList}
                                                        fields={{ text: 'firstName', value: 'radiantUserId' }} floatLabelType="Auto" placeholder="Select QA " mode="CheckBox" showSelectAll={true}
                                                        showDropDownIcon={true} filterBarPlaceholder="Select QA  " popupHeight="350px" change={this.filterOrder} >
                                                        <Inject services={[CheckBoxSelection]} />
                                                    </MultiSelectComponent>
                                                </Col>

                                            </Row> */}

                                            <div className="mt-3"></div>

                                            <Row>
                                                <Col xs='12' md='12'>
                                                    <MultiSelectComponent id="group" value={this.state.model.groups} dataSource={this.state.groups} fields={{ text: 'groupName', value: 'groupName' }} placeholder="Select Group" floatLabelType="Auto" change={this.handleGroup} />
                                                </Col>
                                            </Row>
                                            <div className="mt-3"></div>
                                            <Row>
                                                <Col xs='12' md='12'>
                                                    <div className={this.state.errorObject.roles == true ? this.state.validationErrorCss : "required-field"}>

                                                        <MultiSelectComponent id="roles" value={this.state.model.roles} dataSource={this.rolesList} fields={{ text: 'role', value: 'role' }} placeholder="Select Role" floatLabelType="Auto" change={this.handleRole} />

                                                        <span hidden={!this.state.errorObject.roles} >{t("cpm.lm.line.validation.Roles")}</span>
                                                    </div>
                                                </Col>
                                            </Row>

                                            <div className="mt-3"></div>
                                            <Row>
                                                <Col xs='12' md='12'>
                                                    <TextBoxComponent id='description' value={this.state.model.description} multiline={true} floatLabelType="Auto" placeholder="Description" onChange={this.handleChange} />
                                                </Col>
                                            </Row>
                                            <div className="mt-3"></div>
                                            <hr />
                                            <Row className="mt-3">
                                                <Col>
                                                    <Button color="primary" size="sm" id="save" className="float-left mr-3" onClick={e => this.save(e)} >Save</Button>
                                                    <Button color="primary" size="sm" id="cancel" className="float-left" onClick={e => this.sideBar(e)}>Cancel</Button>
                                                </Col>
                                            </Row>
                                        </Col>

                                    </div>
                                </SplitterLayout>
                            </div>
                            <div className="add-line-section-overlay" hidden={this.state.isSidebar}></div>
                            {/* ----------------------- Add User end          -----------------------  */}
                            <DialogComponent id="confirmDialog" animationSettings={this.state.animationSettings} showCloseIcon={true} close={this.cancelConformDialog} buttons={this.state.confirmButton} header='Delete User Profile !!' visible={this.state.isShowConfirmationDialog} width='400px' content='Are you sure you want to permanently delete User ?'></DialogComponent>
                            <div className="overlayer-popup" hidden={!this.state.isShowConfirmationDialog} ></div>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}
export default withTranslation("translations")(UserProfiling);
