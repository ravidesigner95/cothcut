import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { GridComponent, ColumnsDirective, ColumnDirective, Filter, Inject, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, Reorder } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { qualitymasterData, defectsmasterdata } from '../../assets/data/inspection';
import CriteriaformFieldComponent from './aql-master-component.jsx';
import AQLQualityMasterComponent from './aql-master-component.jsx';
import DefectsmasterComponent from '../qualitymaster/defectsmasterdata.jsx';
import QuestionmasterComponent from '../qualitymaster/questionmaster.jsx';
import FourPointMasterComponent from '../qualitymaster/four-point.jsx';
import DefectLabMasterComponent from '../qualitymaster/defect-lab.jsx';
class QualitymasterComponent extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    this.state = { }
  }
  componentDidMount() { }
  
  render() {
    return (
      <div className="container-fluid-inner  quality-master">
        <h1 className='pagesheading'>Quality Master</h1> 
        <TabComponent id='defaultTab' className='mt-n2'>
          <div className="e-tab-header" >
            <div>  AQL </div>
            <div>  Defects </div>
            <div>  Questions </div>
            <div>  4 Points  </div>
            <div>  Defect lab  </div>
          </div>
          
          <div className="e-content mt-2">
            <div>
              <AQLQualityMasterComponent />
            </div>
            <div>
              <DefectsmasterComponent />
            </div>
            <div>
              <QuestionmasterComponent />
            </div>
            <div>
              <FourPointMasterComponent/>
            </div>
            <div>
              <DefectLabMasterComponent/>
            </div>
          </div>
        </TabComponent>
      </div> 
    );
  }
}
export default QualitymasterComponent;
