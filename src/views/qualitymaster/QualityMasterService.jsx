import axios from 'axios';
class QualityMasterService {
    constructor() { }

    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    qualityServiceUrl = process.env.REACT_APP_BASEURL_QUALITY + '/qualityMaster/';

    addAqlUrl = this.qualityServiceUrl + 'createAql';
    deleteAqlUrl = this.qualityServiceUrl + 'deleteAql';
    getAqlUrl = this.qualityServiceUrl + 'getAql';
    deleteAqlDataUrl = this.qualityServiceUrl + 'deleteAqlData';

    addDefectsUrl = this.qualityServiceUrl + 'createDefects';
    deleteDefectsUrl = this.qualityServiceUrl + 'deleteDefects';
    getDefectsUrl = this.qualityServiceUrl + 'getDefects';
    deleteDefectsDataUrl = this.qualityServiceUrl + 'deleteDefectsData';

    getQuesCategoryUrl = this.qualityServiceUrl + 'getQuesCategory';
    getQuesAnsUrl = this.qualityServiceUrl + 'getQuesAnsGrp';
    getQuesUrl = this.qualityServiceUrl + 'getQuestions';
    saveQuesUrl = this.qualityServiceUrl + 'saveOrUpdateQuestions';
    deleteQuesUrl = this.qualityServiceUrl + 'deleteQuestion';

    getBuyersUrl = this.qualityServiceUrl + `getAllBuyers`;
    getProductTypeUrl = this.qualityServiceUrl + `getAllProductTypes`;
    getTestPlanGroupsUrl = this.qualityServiceUrl + `getAllTestPlanGroups`;
    getLabTestMastersUrl = this.qualityServiceUrl + `getAllLabTestMasters`;

    getBuyerTestPlanMappingUrl = this.qualityServiceUrl + `getAllBuyerTestPlanMapping`;
    createBuyerTestPlanMappingUrl = this.qualityServiceUrl + `createBuyerTestPlanMapping`;
    deleteBuyerTestPlanMappingUrl = this.qualityServiceUrl + `deleteBuyerTestPlanMapping`;
    createTestPlanGroupUrl = this.qualityServiceUrl + `createTestPlanGroup`;
    createLabTestMasterUrl = this.qualityServiceUrl + `createLabTestMaster`;
    updateTestPlanGroupUrl = this.qualityServiceUrl + `updateTestPlanGroup`;
    updateLabTestMasterUrl = this.qualityServiceUrl + `updateLabTestMaster`;


    getFourPointUrl = this.qualityServiceUrl + `getFourPoint`;
    saveFourPointUrl = this.qualityServiceUrl + `createFourPoint`;
    deleteFourPointUrl = this.qualityServiceUrl + `deleteFourPoint`;


    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    //---------------------------------------API-CALLS-----------------------------------------//

    async getAqlList() {
        try {
            const response = await axios.get(this.getAqlUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getDefectsList() {
        try {
            const response = await axios.get(this.getDefectsUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getQuesCategoryList() {
        try {
            const response = await axios.get(this.getQuesCategoryUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getQuesAnsList() {
        try {
            const response = await axios.get(this.getQuesAnsUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getQuesList() {
        try {
            const response = await axios.get(this.getQuesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async saveAql(newAql) {
        try {
            const response = await axios.post(this.addAqlUrl, newAql, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }



    async saveDefects(newDefect) {
        try {
            const response = await axios.post(this.addDefectsUrl, newDefect, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async saveQues(newQues) {
        try {
            const response = await axios.post(this.saveQuesUrl, newQues, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }


    async deleteAql(id) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    aqlId: id,

                }
            };
            return await axios.delete(this.deleteAqlUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async deleteAqlData(id) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    aqlDataId: id,

                }
            };
            return await axios.delete(this.deleteAqlDataUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }



    async deleteDefect(id) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    defectsId: id,

                }
            };
            return await axios.delete(this.deleteDefectsUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteDefectData(id) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    defectsDataId: id,

                }
            };
            return await axios.delete(this.deleteDefectsDataUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteQues(id) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    quesId: id,

                }
            };
            return await axios.delete(this.deleteQuesUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getBuyersList() {
        try {
            const response = await axios.get(this.getBuyersUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getProductsList() {
        try {
            const response = await axios.get(this.getProductTypeUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getTestPlanGroupList() {
        try {
            const response = await axios.get(this.getTestPlanGroupsUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getLabTestMasterList() {
        try {
            const response = await axios.get(this.getLabTestMastersUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getBuyerMappingList() {
        try {
            const response = await axios.get(this.getBuyerTestPlanMappingUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async createBuyerTestPlanMapping(buyerTestPlanMapping) {
        try {
            const response = await axios.post(this.createBuyerTestPlanMappingUrl, buyerTestPlanMapping, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async deleteBuyerTestPlanMapping(id) {
        try {
            return await axios.delete(this.deleteBuyerTestPlanMappingUrl + `/${id}`, this.options);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async createTestPlanGroup(testPlanGroup) {
        try {
            const response = await axios.post(this.createTestPlanGroupUrl, testPlanGroup, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async createLabTestMaster(labTestMaster) {
        try {
            const response = await axios.post(this.createLabTestMasterUrl, labTestMaster, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async updateTestPlanGroup(testPlanGroup) {
        try {
            const response = await axios.put(this.updateTestPlanGroupUrl, testPlanGroup, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async updateLabTestMaster(labTestMaster) {
        try {
            const response = await axios.put(this.updateLabTestMasterUrl, labTestMaster, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async saveFourPoint(fourPoint) {
        try {
            const response = await axios.post(this.saveFourPointUrl, fourPoint, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async getAllFourPoint() {
        try {
            const response = await axios.get(this.getFourPointUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteFourPoint(id) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    fourPointId: id,

                }
            };
            return await axios.delete(this.deleteFourPointUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

}
export default QualityMasterService;