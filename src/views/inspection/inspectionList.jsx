import { RadioButtonComponent, SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { ComboBoxComponent, DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { ColumnDirective, ColumnsDirective, CommandColumn, Edit, Filter, Grid, GridComponent, Group, Inject, Page, Reorder, Resize, Search, Selection, SelectionType, Sort, Toolbar, VirtualScroll } from '@syncfusion/ej2-react-grids';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { Toast, ToastComponent, ToastModel } from '@syncfusion/ej2-react-notifications';
import { AnimationSettingsModel, ButtonPropsModel, DialogComponent } from '@syncfusion/ej2-react-popups';
import { push } from 'object-path';
import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import DropdownItem from 'reactstrap/lib/DropdownItem';
import { AppConfig } from '../../appConfig/appConfig';

import { data } from '../../assets/data/data';
import { sampleData } from '../../assets/data/inspection'
import CriteriaformFieldComponent from './criteriaformfield';
import DclformComponent from './dclform.jsx';
import { names } from './nameSharing'
import QuestionformComponent from './questionform.jsx';
import ResultformComponent from './resultform.jsx'; 
import InspectionService from './InspectionService';
import InspectionModel from './InspectionModel';
import { CategoryModel } from './ComponentModel';
 
// import { sampleBase64pdf } from "../../../sampleBase64pdf";
class InspectionListComponent extends React.PureComponent {
    constructor(props) {
        super(...arguments);
        this.inspectionService = new InspectionService();
        this.commands = [
            { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
            { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } }, 
            { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } }
        ];
        this.temp = 'categories';
        this.dateValue = new Date();
        this.searchData = data[this.temp];
        this.fields = { text: 'Name', value: 'Code' };
        // this.value = 'Game3';    
        this.Storecategsearch = data['Storecategsearch'];
        this.headtypesearchData = data['HeaderType'];
        this.insrulesearchData = data['insrulesearchData'];
        this.processtypesearchData = data['HeaderType'];
        this.countries = sampleData;
        this.state = {
            inspections:[],
            filterBox: true,
            tabContentSection: '',
            lineLoadView: true,
            // lineLoadView2: true,
            divHidden: null,
            showDialog: false,
            listfield: { dataSource: this.countries, id: 'id', text: 'name', parentID: 'pid', hasChildren: 'hasChild' },

            //Animate Store rules
            forStore: true,
            forProduction: true,
            forHeaderInfo: true,

        }
        this.toolbarOptions = ['Edit', 'Delete', 'Update', 'Cancel', 'Search'];
        this.editSettings = { allowEditing: false, allowAdding: true, allowDeleting: true, newRowPosition: 'Top' }
        this.setState({
            orderDataSource: this.orderDataSource
        });
        this.buttonRef = element => {
            this.buttonEle = element;
        };
        this.formData = [
            {
                Name: 'Target GS - Packing Softhome Workflow',
                type:'gsp',
                Description: 'Yellow sealing for production process',
                Category: 'Producton in line Inspection',
                totalHeader:'General',
            },
            {
                Name: 'Sewing 5 pc In-line Inspection-Stiching',
                type:'gsp',
                Description: 'Sewing 5 PC In-line Inspection-Stiching', 
                Category: 'Stiching, Finishing, Re-cutting, Packing',
                totalHeader:'General',
            },
            {
                Name: 'General Information',
                type:'gsp',
                Description: 'Includes Product, Order and lot of other prefilled information',
                Category: 'Fabric',
                totalHeader:'',
            },
            {
                Name: 'TARGET',
                type:'gsp',
                Description: 'Inspection which are done in line, during any production process',
                Category: 'Producton in line Inspection',
                totalHeader:'',
            },
            {
                Name: 'Production Yellow Seiling',
                type:'gsp',
                Description: 'Yellow sealing for production process',
                Category: 'Producton in line Inspection',
                totalHeader:'',
            },
            {
                Name: 'Sewing 5 pc In-line Inspection-Stiching',
                type:'gsp',
                Description: 'Sewing 5 PC In-line Inspection-Stiching',
                Category: 'Stiching, Finishing, Re-cutting, Packing',
                totalHeader:'',
            },
            {
                Name: 'General Information',
                type:'gsp',
                Description: 'Includes Product, Order and lot of other prefilled information',
                Category: 'Fabric',
                totalHeader:'',
            },
            {
                Name: 'TARGET',
                type:'gsp',
                Description: 'Inspection which are done in line, during any production process',
                Category: 'Producton in line Inspection',
                totalHeader:'',
            },
        ];

        this.animationSettings = { effect: 'None' };
        this.toolbarOptions = ['Search'];
    }
    componentDidMount() {
        this.getInspectionList();
        }

    rowselected = (args) => {                     // TO BE IMPROVED !!
      
    }

    async getInspectionList() {
        global.showLoader();
        let response = (await this.inspectionService.getInspectionList())
        global.hideLoader();
        let inspData=[];
        if ((response != undefined) && (response.status == 200)){
            response.data.map(item=>{
            if(item.isActive){    
            let categoryNames =item.inspectionCategoryMasterDTO!=null? item.inspectionCategoryMasterDTO.categoryName :""; 
             inspData.push(new InspectionModel(item.id,item.code,item.date,item.description,item.isActive,item.name,item.revision,
                item.rulesDto,item.qualityComponentMasterDTO,item.qualityCategoryMasterDTOList,categoryNames,item.headerList))
             }
            })
        
            this.setState({ inspections:inspData });
        }
        else{
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
        }
           
        
    }
    async deleteInspType(inspId) {
        let response = (await this.inspectionService.deleteInspType(inspId))
        if ((response != undefined) && (response.data.status == "success")) {
          this.toastObj.show(this.createToaster("Inspection Type deleted successfully!!", "Success"));
          this.getInspectionList();
        }
      }

      createToaster(message, type) {
        switch (type) {
            case "Success":
                return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
            case "Error":
                return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
            case "Warning":
                return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
            case "Information":
                return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
    }

    commandClick= async (args)=>{
        if (args.commandColumn.type === "Edit") {
            // this.props.propsData.history.replace({
            //     pathname: '/inspection',
            //     state:{
            //         id:args.rowData.id
            //     }
            // })
            this.props.history.push({
                pathname: '/inspection',
                data: args.rowData.id 
              })

              
        }
        else if(args.commandColumn.type==="Delete"){
           this.deleteInspType(args.rowData.id);
        }

    }
    cellSelection(args){
        // push.to="/inspection"
        this.props.history.push('/');
        // if(data.categories){ 
            
        // } 
    }

    headerNameTemplate(props){
        return(<div>{props.Name}</div>)
    }
    render() {
        return (<div className="container-fluid-inner">
                    <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={{ X: 'Right', Y: 'Bottom' }}   ></ToastComponent>
            <h1 className='pagesheading'>Inspection List</h1> 
            <div className='clearfix'></div>
            <Link to="/inspection" className='float-right mb-2 btn btn-primary btn-sm'>Add New Inspection</Link> 
            <div className='clearfix'></div>
            <GridComponent
                id='grid'
                dataSource={this.state.inspections}
                enableHover={false}
                allowExcelExport={true}
                // allowFiltering={true}
                allowSorting={true}
                allowSelection={true}
                allowResizing={true}
                showColumnChooser={true}
                rowHeight={25}
                toolbar={this.toolbarOptions}
                gridLines='Both' 
                height={AppConfig.gridHeight}
                allowTextWrap={true}
                ref={(g) => { this.gridInstance = g; }}
                filterSettings={this.Filter}
                toolbarClick={this.toolbarClick}
                sortSettings={this.state.sortingOptions}
                allowPaging={true}
                editSettings={this.editSettings}
                pageSettings={AppConfig.pageSettings}
                commandClick={this.commandClick}
                filterSettings={this.state.FilterSettingsModel}>
                <ColumnsDirective field='action' allowSorting={true}>
                    <ColumnDirective field='name' headerText='Name'  width='200' />
                    <ColumnDirective field='code' width='150' headerText='Code' />
                    <ColumnDirective field='description' width='200' headerText='Description' />
                    <ColumnDirective field='category' headerText='Category'  width='150'  />
                    <ColumnDirective field='headerList' headerText='Total Header' width='200'  />
                    <ColumnDirective headerText='Action' foreignKeyField='Action' width='90' commands={this.commands} />
                </ColumnsDirective>
                <Inject services={[Filter, Edit, CommandColumn, VirtualScroll, Sort, Resize, Toolbar, Page]} />
            </GridComponent>
        </div>


        );

    }
}

export default InspectionListComponent;
