import React from 'react';
import ReactDOM from 'react-dom';
import { ButtonComponent, CheckBoxComponent } from '@syncfusion/ej2-react-buttons';
import { DropDownListComponent, revertHighlightSearch } from '@syncfusion/ej2-react-dropdowns';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { DateTimePickerComponent } from '@syncfusion/ej2-react-calendars';
import { ProgressButtonComponent } from '@syncfusion/ej2-react-splitbuttons';
import { Button, Card, CardBody, CardHeader, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { GridComponent, ExcelExport, Toolbar, FilterSettingsModel, Page, Group, ColumnsDirective, CommandColumn, ColumnDirective, Filter, ColumnMenu, Edit, QueryCellInfoEventArgs, RowDataBoundEventArgs, Selection, ForeignKey, Sort } from '@syncfusion/ej2-react-grids';
import { ScheduleComponent, TimelineViews, ResourcesDirective, ResourceDirective, TimelineMonth, ViewsDirective, ViewDirective, Day, Week, WorkWeek, Month, Agenda, Inject, Resize, DragAndDrop } from '@syncfusion/ej2-react-schedule';
import { MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { PivotViewComponent, ConditionalFormatting } from "@syncfusion/ej2-react-pivotview";
import LineSchedulingService from './LineSchedulingService';
import GridModel from './GridModel';
import PlannedLineModel from './PlannedLineModel';
import PlannedEventModel from './PlannedEventModel';
import SavePlannedEventModel from './SavePlannedEventModel';
import ScheduledModel from './ScheduledModel';
import ScheduleModel from './ScheduleModel';
import BuyerPoModel from './BuyerPoModel';
import { withTranslation } from "react-i18next";
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import './cell.css';
import SplitterLayout from 'react-splitter-layout';
import 'react-splitter-layout/lib/index.css';
import errorMessages from '../../errorHandler/errorMessages';
import { PlanningHistoryModel, StockModel } from './PlanningHistoryModel';
import LinePlanningService from '../line-planning/LinePlanningService';


class LineScheduling extends React.PureComponent {
  constructor(props) {
    super()
    this.LineSchedulingService = new LineSchedulingService();
    this.linePlanningService = new LinePlanningService()
    this.mounted = false;
    this.commands = [
      {
        buttonOption: {
          content: 'Check', cssClass: 'e-flat e-primary'
        }
      }
    ];
    this.state = {
      selectedLineItemNo: null,
      planningHistoryPlants: [],
      showDialog: false,
      planningHistoryUnits: [],
      // planningHistoryLines: [],
      planningHistoryBuyerPo: [],
      planningHistorySaid: [],
      planningHistoryLineItem: [],
      planningHistoryStatus: [],
      planningHistoryAlert: [],
      // planningHistoryDaysLeft: [],
      selectedPlanningHistoryStartDate: null,
      selectedPlanningHistoryPlants: [],
      selectedPlanningHistoryUnits: [],
      // selectedPlanningHistoryLines: [],
      selectedPlanningHistoryAlert: [],
      selectedPlanningHistoryBuyerPo: [],
      selectedPlanningHistorySaid: [],
      selectedPlanningHistoryLineItem: [],
      selectedPlanningHistoryStatus: [],
      selectedPlanningHistoryEndDate: null,
      selectedYear: null,
      summarySelectedYear: null,
      // selectedPlanningHistoryDaysLeft: [],
      isShowFilters: false,
      isShowStockDialog: false,
      isShowPlanningHistoryScreen: false,
      orders: [],
      stocks: [],
      orderDetails: {
        isInvokedFromPlanningHistory: false,
        plantId: null, //3010,
        unitId: null,//38,
        prodLineId: null,
        lineName: null,
        unitName: null, //"A",
        saidId: null,//"20TM04T032",
        lineItemNo: null,//"000030",
        buyerPoNo: null//"3744425"
      },
      FilterSettingsModel2: { type: 'Menu' },
      IFilter2: { type: 'CheckBox' },
      animationSettings: { effect: 'Fade' },
      //-------------------------------------------------//
      format: { type: 'dateTime', format: 'dd/MM/yyyy' },
      totalNoOfMachines: null,
      selectedPlantName: null,
      selectedUnitName: null,
      selectedLineName: null,
      selectedReportPlantId: null,
      selectedReportUnitId: null,
      selectedReportStartDate: null,
      selectedReportEndDate: null,
      selectedUnitNameForPlannedTab: null,
      unitsForPlannedTab: [],
      dynamicPivotGrids: [],
      lineSchedulingDetails: [],
      lineNameToRefresh: null,
      minStartDate: null,
      selectedProdLineId: null,
      selectedProdLineMonth: null,
      selectedProdLineYear: null,
      maxAllowedWorkingHoursFromPivot: null,
      maxAllowedMachinesFromPivot: null,
      minNavigationDate: null,
      maxNavigationDate: null,
      scheduledQty: null,
      scheduledQtyBuyerPo: null,
      multiSelectedPlants: [],
      dynamicSummaryViewPlants: [],
      dynamicSummaryViewCategory: [],
      summaryDataPlants: [],
      summaryDataCategory: [],
      selectedSaidToRefreshGrid: null,
      selectedLineItemNoToRefreshGrid: null,
      isSummaryView: false,
      isCategoryView: false,
      // isScheduleView: false,
      isSidebar: false,
      isDisabledScheduledButton: false,
      maxAllowedWorkingHours: null,
      plannedLineData: [],
      scheduledLineData: [],
      lineItemSchedulerData: [],
      buyerPoData: [],
      productionCategory: [],
      buyerPoList: [],
      buyerPoLineItemsList: [],
      selectedBuyerPo: null,
      selectedBuyerPoData: null,
      configKeys: {},
      apm: [],
      dynamicTableWorkInProgress: [],
      dynamicTableProductionLines: [],
      schedulerEventsDataOnLoad: [],
      deletedEventsId: [],
      currentView: "Month",
      currrentDate: new Date(),
      selectedEventColor: null,
      selectedLineItemIdToEdit: null,
      selectedLineItemNameToSchedule: null,
      selectedLineItemColorToSchedule: null,
      selectedScheduledLineItemId: null,
      selectedLineItemUnitIdToGetUnitList: null,
      selectedPlantForUpcomingBuyerPo: null,
      model: new PlannedLineModel(null, null, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null),
      scheduledModel: new ScheduledModel(),
      plants: [],
      units: [],
      unitProductionLines: [],
      FilterSettingsModel: { type: 'Menu' },
      buyerPoExportProperties:{fileName:'Buyer_PO_Report.xlsx'},
      schedulingExportProperties:{fileName:'Scheduling_Report.xlsx'},
      schedulingStatusExportProperties:{fileName:'Scheduling_Status.xlsx'},

      IFilter: { type: 'CheckBox' },
      days: [{ id: 10, name: "10 days" }, { id: 20, name: "20 days" }, { id: 30, name: "30 days" }, { id: 40, name: "40 days" }, { id: 50, name: "50 days" }, { id: 60, name: "60 days" }],
      months: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      selectedTab: "planned",
      isDisabledFields: false,
      // isDisabledFieldsOnscheduledTab: false,
      transferLineItemPLlist: [],
      transferLineItemUnitlist: [],
      daywiseAvailableMachines: new Map(),
      selectedEventName: null,
      setWidth: '750px',
      index: null,
      isRefreshPivotGrid: false,
      workingDays: [1, 2, 3, 4, 5, 6],
      isMonthlyView: "month",
      toasterMessage: [
        { title: 'Information!', content: 'Navigation to freezed months are not allowed !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Warning!', content: 'Selected cell is not editable !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
        { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
        { title: 'Success!', content: 'Updated successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
        { title: 'Information!', content: 'Fill all mandatory fields to create new event !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Warning!', content: 'No Change Found !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
        { title: 'Information!', content: 'Delete operation not allowed !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Information!', content: 'New events not allowed !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Success!', content: 'Successfully scheduled !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
        { title: 'Information!', content: 'Please select Buyer Po of selected Line Item to schedule !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Information!', content: 'Operation allowed to only selected LineItem !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Warning!', content: ' No planned line item for the selected time frame !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
        { title: 'Warning!', content: ' No Buyer Po data for the selected line Item !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
        { title: 'Warning!', content: ' No scheduled line item for the selected time frame !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
        { title: 'Success!', content: 'Successfully UnScheduled !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
        { title: 'Information!', content: 'No operation allowed !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Information!', content: 'Please select Plant !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Information!', content: "Line Id can't be null or Scheduled qty. and No of Machines can't be zero !!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Information!', content: "No Line Item selected !!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Warning!', content: 'Start Date or End Date should be greater than current date !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
        { title: 'Error!', content: 'Something went wrong !! Please contact support team !!', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
        { title: 'Information!', content: 'Filter Reset Successfully !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Information!', content: 'Search completed !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Information!', content: "Please select any Filter !!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
        { title: 'Success!', content: 'Report Downloaded successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      ],
    }

    this.onClickSchedule = this.onClickSchedule.bind(this);
  }

  componentDidMount() {
    this.mounted = true;
    this.getPlantList();
    this.getCategory();
    this.getConfgKey();
    this.getAllOrders();
    document.addEventListener("keydown", this.escFunction, false);
    console.log(React.version);
  }

  componentWillUnmount() {
    this.mounted = false;
    document.removeEventListener("keydown", this.escFunction, false);
  }

  escFunction = (event) => {
    if (event.keyCode === 27) {
      this.setState({
        isSidebar: false, isDisabledFields: false,
        model: new PlannedLineModel(null, null, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null), selectedLineItemIdToEdit: null
      })
      document.body.classList.remove('overflow-hidden-body');
    }
  }

  //################################################################ PLANNING-TAB ##################################################################################//

  //---------------------API-CALLS----------------------//

  async getEstimatedWorkingDays(buyerPo, lineItem, noOfMachines, prodLineId, qty, saidNo, sam, startDate) {
    global.showLoader();
    let response = (await this.LineSchedulingService.getEstimatedWorkingDays(buyerPo, lineItem, noOfMachines, prodLineId, qty, saidNo, sam, startDate))
    if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
      global.hideLoader();
      return response.data.data;
    }
    global.hideLoader();
  }

  async getUnitListForPlannedTab(plantId) {
    let response = (await this.LineSchedulingService.getUnitList(plantId))
    if ((response != undefined) && (response.data.status == "success")) {
      this.setState({ unitsForPlannedTab: response.data.data, selectedUnitNameForPlannedTab: null }, () => {
        if (this.state.orderDetails.isInvokedFromPlanningHistory)
          this.onChangePlannedTabUnit(null);
        else
          global.hideLoader();
      })
    } else {
      this.setState({ orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: false } });
      this.toastObj.show(this.state.toasterMessage[2]);
      global.hideLoader();
    }
  }

  async getConfgKey() {
    let response = (await this.LineSchedulingService.getConfgKey())
    if ((response != undefined) && (response.data.data.length > 0) && (response.data.status == "success") && (this.mounted)) {
      let configObj = {};
      response.data.data.forEach(element => {
        configObj[element.keyName] = /^\d+$/.test(element.keyValue) == true ? Number(element.keyValue) : element.keyValue;
      })
      this.setState({ configKeys: configObj })
    }
  }

  async saveScheduledData() {
    let response = (await this.LineSchedulingService.saveScheduledData(this.state.scheduledModel));
    if ((response != undefined) && (response.data.status == "success")) {
      this.setState({
        isDisabledScheduledButton: false, isRefreshPivotGrid: true, selectedEventColor: null, selectedEventName: null, selectedLineItemColorToSchedule: null, selectedLineItemNameToSchedule: null, isDisabledFields: false,
        model: new PlannedLineModel(null, null, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null), selectedLineItemIdToEdit: null, index: null
      })
      global.hideLoader()
      this.toastObj.show(this.state.toasterMessage[8]);
    } else {
      this.setState({ isDisabledScheduledButton: false })
      global.hideLoader()
      this.toastObj.show(this.state.toasterMessage[2]);
    }
  }

  async getCategory() {
    let response = (await this.LineSchedulingService.getCategory())
    if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
      this.setState({ productionCategory: response.data.data })
    }
  }

  async getDaywiseNoOfAllocatedMachines(prodLineId) {
    let response = (await this.LineSchedulingService.getDaywiseNoOfAllocatedMachines(prodLineId))
    if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
      response.data.data.forEach(element => {
        this.state.daywiseAvailableMachines.set((new Date(element.date.year, element.date.month - 1, element.date.day)).toDateString(), element.availableMachines);
      });
    }
  }

  async getAPM(selectedLineItemObj) {
    let response = (await this.LineSchedulingService.getAPM(selectedLineItemObj.plantId))
    if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
      this.state.apm = response.data.data;
      this.getUnitListSidebar(selectedLineItemObj)
    }
  }

  async getUnitListSidebar(selectedLineItemObj) {
    let response = (await this.LineSchedulingService.getUnitList(selectedLineItemObj.plantId))
    if ((response != undefined) && (response.data.status == "success")) {
      this.state.transferLineItemUnitlist = response.data.data;
      this.getUnitProductionLines(selectedLineItemObj);
    }
  }

  async getUnitProductionLinesForTransferTo(unitId) {
    let response = (await this.LineSchedulingService.getUnitProductionLines(unitId))
    if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
      this.setState({ transferLineItemPLlist: response.data.data })
    } else {
      this.setState({ transferLineItemPLlist: [] })
    }
  }

  async getUnitProductionLines(selectedLineItemObj) {
    let response = (await this.LineSchedulingService.getUnitProductionLines(selectedLineItemObj.unitId))
    if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
      this.state.transferLineItemPLlist = response.data.data;
      this.state.transferLineItemPLlist.find(item => {
        if (item.id == selectedLineItemObj.prodLineId) {
          this.state.maxAllowedWorkingHours = item.workingHour;
        }
      })
      this.getPlannedEvents(selectedLineItemObj.prodLineId, this.getMonthName(selectedLineItemObj.startDateTime), selectedLineItemObj.startDateTime.getFullYear(), selectedLineItemObj);  // ***need to check for now only startTime is given.
    }
  }

  async getPlannedEvents(prodLineId, month, year, selectedLineItemObj) {
    let that = this;
    const { selectedPlant, selectedLine, selectedUnit, selectedTab } = that.state;
    let response = (await that.LineSchedulingService.getPlannedEvents(prodLineId, month, year))
    if ((response != undefined) && (response.data.status == "success")) {
      let tempSchedulerEventsOnLoad = [];
      this.state.totalNoOfMachines = response.data.data[0].prodLine.numberOfMachines;
      response.data.data.map(item => {
        item.dayWiseWorkLoad.map(dayWiseWorkLoadObj => {
          let convertedStartDate = new Date(Date.UTC(dayWiseWorkLoadObj.startDateTime.date.year, dayWiseWorkLoadObj.startDateTime.date.month - 1, dayWiseWorkLoadObj.startDateTime.date.day, dayWiseWorkLoadObj.startDateTime.time.hour, dayWiseWorkLoadObj.startDateTime.time.minute))
          let convertedEndDate = new Date(Date.UTC(dayWiseWorkLoadObj.endDateTime.date.year, dayWiseWorkLoadObj.endDateTime.date.month - 1, dayWiseWorkLoadObj.endDateTime.date.day, dayWiseWorkLoadObj.endDateTime.time.hour, dayWiseWorkLoadObj.endDateTime.time.minute))
          let schedulerDateObj = new PlannedEventModel(dayWiseWorkLoadObj.id, item.id, dayWiseWorkLoadObj.categoryColor, dayWiseWorkLoadObj.description, convertedStartDate, convertedEndDate, convertedStartDate, convertedEndDate, dayWiseWorkLoadObj.saidId, dayWiseWorkLoadObj.lineItem, dayWiseWorkLoadObj.prodLineId, dayWiseWorkLoadObj.plannedInfoId,
            new PlannedLineModel(null, null, null, null, null, null, null, selectedPlant, selectedUnit, selectedLine, null, null, null, "", "", "", "", selectedLineItemObj.productCategoryName, "", null, null, dayWiseWorkLoadObj.lineItem, null, null, convertedStartDate, convertedEndDate, convertedStartDate, convertedEndDate, null, null, null, null, null, null, null, null), true)
          tempSchedulerEventsOnLoad.push(schedulerDateObj);
        })
      })
      that.setState({ schedulerEventsDataOnLoad: tempSchedulerEventsOnLoad, }, () => {
        that.getClickedEventPlannedData(selectedLineItemObj, that.getArrayEventIndex(selectedLineItemObj), null);
      })
    } else {
      that.toastObj.show(that.state.toasterMessage[2]);
      global.hideLoader();
    }
  }

  async getClickedEventPlannedData(eventModel, index, type) {
    const { selectedTab, selectedLine, selectedBuyerPoData, transferLineItemPLlist, schedulerEventsDataOnLoad, selectedBuyerPo, selectedLineItemNo, buyerPoLineItemsList } = this.state;
    let that = this;
    try {
      let reponse = this.getScheduledQty(selectedBuyerPoData.saidNr, selectedLineItemNo, selectedBuyerPo);
      let eventObject = eventModel.plannedData == undefined ? eventModel : eventModel.plannedData;
      let clickedEventObj = Object.assign({}, schedulerEventsDataOnLoad[index])
      let categoryColor = type == null ? clickedEventObj.categoryColor : eventModel.categoryColor;
      let response = (await this.LineSchedulingService.getClickedEventPlannedData(eventObject, eventModel.saidId, eventModel.prodLineId, categoryColor))
      let noOfMachineInLine = null;
      if ((response != undefined) && (response.data.status == "success")) {
        let eventDetailObj = response.data.data;
        noOfMachineInLine = transferLineItemPLlist.find(item => {
          return item.id == clickedEventObj.prodLineId;
        })
        let buyerPoInfo;
        buyerPoLineItemsList.forEach(item => {
          if (item.lineItemNr == selectedLineItemNo) {
            buyerPoInfo = item;
          }
        })


        // let response = (await this.LineSchedulingService.getScheduledQty(buyerPoInfo.saidNr,selectedLineItemNo,buyerPoInfo.getBuyerPoNr))                //get Scheduled Qty 


        let leftQtyScheduledBuyerPo = buyerPoInfo.buyerPoDelQty - this.state.scheduledQtyBuyerPo;
        if (leftQtyScheduledBuyerPo < 0) {
          leftQtyScheduledBuyerPo = 0;
        }
        this.getQtyTobeScheduled(eventDetailObj.totalQtyAllocatedInLine, eventDetailObj.totalQtyUnscheduledInLine, leftQtyScheduledBuyerPo, buyerPoInfo.buyerPoDelQty);
        clickedEventObj.plannedData = new PlannedLineModel(eventDetailObj.id, eventDetailObj.fmId, eventDetailObj.fmName, eventDetailObj.qaId, eventDetailObj.qaName, eventDetailObj.apmId, clickedEventObj.saidId, eventDetailObj.plantId, eventDetailObj.unitId, eventDetailObj.prodLineId, eventDetailObj.mechandiserId, eventDetailObj.productCategoryId, eventDetailObj.buyerId, eventDetailObj.plantName,
          clickedEventObj.plannedData.unitName, clickedEventObj.plannedData.lineName, eventDetailObj.merchandiserName, "", eventDetailObj.buyerName, eventDetailObj.apm, eventDetailObj.sam, eventDetailObj.lineItemNo, eventDetailObj.totalQtyAllocatedInLine, eventDetailObj.totalQtyToProduced, eventObject.startDateTime, eventObject.endDateTime,
          eventObject.startDateTime, eventObject.endDateTime, eventDetailObj.noOfMachineToUse, eventDetailObj.estimatedWorkingDays, eventDetailObj.status, this.state.scheduledQty, selectedBuyerPo, selectedBuyerPoData.exFactoryDate, eventDetailObj.unitId, null, eventDetailObj.totalQtyUnscheduledInLine, buyerPoInfo.buyerPoDelQty, leftQtyScheduledBuyerPo);
        let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: clickedEventObj })
        that.setState({ schedulerEventsDataOnLoad: modifiedArray, minStartDate: eventObject.startDateTime, totalNoOfMachines: noOfMachineInLine.numberOfMachines, selectedLineItemIdToEdit: eventDetailObj.id, index, selectedEventColor: clickedEventObj.categoryColor, selectedEventName: clickedEventObj.description, selectedLineItemNameToSchedule: clickedEventObj.description, selectedLineItemColorToSchedule: clickedEventObj.categoryColor }, () => {
          that.estimateReqWorkingDays()
        })
        global.hideLoader();
      } else {
        global.hideLoader();
        that.toastObj.show(that.state.toasterMessage[2]);
      }
    } catch (error) {
      global.hideLoader();
      that.toastObj.show(that.state.toasterMessage[20]);
    }

  }

  async getScheduledQty(saidId, lineItemNo, buyerPoNo) {
    let response = (await this.LineSchedulingService.getScheduledQty(saidId, lineItemNo, buyerPoNo))
    if ((response != undefined) && (response.data.status == "success")) {
      this.state.scheduledQtyBuyerPo = response.data.data;
    }
    else {
      this.toastObj.show(this.state.toasterMessage[2]);
    }

  }

  async getQtyTobeScheduled(plannedQty, unscheduleQty, leftQtyScheduledBuyerPo, buyerPoQty) {
    // let leftQtyScheduledBuyerPo = buyerPoQty - this.state.scheduledQtyBuyerPo;
    // if(leftQtyScheduledBuyerPo<0){
    //   leftQtyScheduledBuyerPo =0;
    // }
    // let leftQtyToScheduled = (buyerPoQty - (plannedQty - unscheduleQty));
    if (unscheduleQty <= leftQtyScheduledBuyerPo) {
      this.state.scheduledQty = unscheduleQty;
    }
    else {
      this.state.scheduledQty = leftQtyScheduledBuyerPo;
    }
  }

  //-------------------BUYER-PO-----------------------//

  async getBuyerPoList(plantIds, unitName) {
    let response = (await this.LineSchedulingService.getBuyerPoList(plantIds, unitName))
    if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
      let buyerPoList = [];
      response.data.data.forEach(item => {
        buyerPoList.push(new BuyerPoModel(new Date(item.buyerPoDate), new Date(item.buyerPoDelDate), item.buyerPoDelQty, new Date(item.buyerPoDpciPCDDate), item.buyerPoNr, new Date(item.buyerPoOrangeDate1), new Date(item.buyerPoOrangeDate2),
          item.buyerPoOrderedQty, new Date(item.buyerPoPCDDate), item.buyerPoPCDPercentage, new Date(item.exFactoryDate), item.lineItemNr, item.saidNr, null, null
        ))
      })
      this.setState({ buyerPoList })
      if (this.state.orderDetails.isInvokedFromPlanningHistory)
        this.onSelectBuyerPoGrid();
      else
        global.hideLoader();
    } else {
      this.setState({ orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: false } })
      this.toastObj.show(this.state.toasterMessage[2]);
      global.hideLoader();
    }
  }

  async getBuyerPoLineItemList(saidNr, buyerPoNo, plantId) {
    let response = (await this.LineSchedulingService.getSelectedBuyerPoLineItems(saidNr, buyerPoNo, plantId))
    if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
      this.setState({ buyerPoLineItemsList: response.data.data }, () => {
        if (this.state.orderDetails.isInvokedFromPlanningHistory)
          this.onChangeLineItem(null);
      })
    } else {
      this.setState({ orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: false } })
      this.toastObj.show(this.state.toasterMessage[2]);
      global.hideLoader();
    }
  }

  onChangeMultiSelectPlantToGetUnit = (event) => {
    let that = this;
    const { orderDetails } = that.state;
    if (((event != null) && (event.e != undefined && event.e != null)) || (orderDetails.isInvokedFromPlanningHistory && event == null)) {
      global.showLoader();
      let selectedValue = orderDetails.isInvokedFromPlanningHistory ? orderDetails.plantId : event.value;
      that.setState({
        selectedBuyerPo: null, unitsForPlannedTab: [], selectedLineItemNo: null, selectedUnitNameForPlannedTab: null, buyerPoLineItemsList: [], plannedLineData: []
        , selectedBuyerPoData: null, dynamicTableProductionLines: [], buyerPoList: [], selectedPlantForUpcomingBuyerPo: selectedValue
      })
      that.getUnitListForPlannedTab(selectedValue);
    }
  }

  onChangePlannedTabUnit = (event) => {
    let that = this;
    const { selectedPlantForUpcomingBuyerPo, orderDetails } = that.state;
    if (((event != null) && (event.e != undefined && event.e != null)) || (orderDetails.isInvokedFromPlanningHistory && event == null)) {
      let selectedValue = orderDetails.isInvokedFromPlanningHistory ? orderDetails.unitName : event.value;
      global.showLoader();
      let plantList = [];
      plantList.push(selectedPlantForUpcomingBuyerPo);
      that.setState({ selectedUnitNameForPlannedTab: selectedValue, selectedLineItemNo: null, selectedBuyerPo: null, buyerPoLineItemsList: [], selectedBuyerPoData: null, plannedLineData: [], dynamicTableProductionLines: [], buyerPoList: [] })
      that.getBuyerPoList(plantList, selectedValue);
    }
  }

  onChangeLineItem = (event) => {
    let that = this;
    const { orderDetails } = that.state;
    if (((event != null) && (event.e != undefined && event.e != null)) || (orderDetails.isInvokedFromPlanningHistory && event == null)) {
      let saidNo = orderDetails.isInvokedFromPlanningHistory ? orderDetails.saidId : event.itemData.saidNr;
      let lineItemNo = orderDetails.isInvokedFromPlanningHistory ? orderDetails.lineItemNo : event.value;
      this.setState({ selectedSaidToRefreshGrid: saidNo, selectedLineItemNoToRefreshGrid: lineItemNo, selectedLineItemNo: lineItemNo })
      this.getPlannedLineItemBySaidIdAndLineItem(saidNo, lineItemNo);
    }
  }

  async getPlannedLineItemBySaidIdAndLineItem(saidId, lineItemNo) {
    const { selectedMonth, productionCategory, selectedSaidToRefreshGrid, selectedPlantForUpcomingBuyerPo, selectedUnitNameForPlannedTab } = this.state;
    let res = (await this.linePlanningService.getSaidDetails(selectedSaidToRefreshGrid, selectedPlantForUpcomingBuyerPo, selectedUnitNameForPlannedTab));
    let selectedfmname;  
    let selectedfmid;  
    if ((res != undefined) && (res.data.status == "success") && (res.data.data.length > 0)) {
            selectedfmname = res.data.data[0].fmName;
            selectedfmid = res.data.data[0].fMUserId;
      }
    let response = (await this.LineSchedulingService.getLineItemBySaidIdAndLineItemNo(saidId, lineItemNo));
    if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
      let plannedListFromDB = [];
      response.data.data.map(plannedObj => {
        let productCategoryNameObj = productionCategory.find(function (item) {
          return item.id == plannedObj.productCategoryId;
        })
        let productCategoryName = productCategoryNameObj == undefined ? "" : productCategoryNameObj.name;
        let convetedStartDateTime = new Date(Date.UTC(plannedObj.startDateTime.date.year, plannedObj.startDateTime.date.month - 1, plannedObj.startDateTime.date.day, plannedObj.startDateTime.time.hour, plannedObj.startDateTime.time.minute))
        let convertedEndDateTime = new Date(Date.UTC(plannedObj.endDateTime.date.year, plannedObj.endDateTime.date.month - 1, plannedObj.endDateTime.date.day, plannedObj.endDateTime.time.hour, plannedObj.endDateTime.time.minute))
        plannedListFromDB.push(new PlannedLineModel(plannedObj.id, selectedfmid, selectedfmname, plannedObj.qaId, plannedObj.qaName, plannedObj.apmId, plannedObj.saidId, plannedObj.plantId, plannedObj.unitId, plannedObj.prodLineId, plannedObj.mechandiserId, plannedObj.productCategoryId, plannedObj.buyerId, plannedObj.plantName, plannedObj.unitName,
          plannedObj.lineName, plannedObj.merchandiserName, productCategoryName, plannedObj.buyerName, plannedObj.apm, plannedObj.sam, plannedObj.lineItemNo, plannedObj.totalQtyAllocatedInLine, plannedObj.totalQtyToProduced,
          convetedStartDateTime, convertedEndDateTime, convetedStartDateTime, convertedEndDateTime, plannedObj.noOfMachineToUse, plannedObj.estimatedWorkingDays, plannedObj.status, plannedObj.scheduledQty, plannedObj.buyerPo, plannedObj.shipToDate, plannedObj.transferTo, plannedObj.plannedInfoId, plannedObj.unScheduledQty))
      })
      this.setState({ plannedLineData: plannedListFromDB, dynamicTableWorkInProgress: [], scheduledLineData: [], orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: false } }, () => {
        // this.createNewGrid();
        this.dynamicGridObj.dataSource = this.state.plannedLineData;
        this.dynamicGridObj.refresh();
        global.hideLoader();
      })
    }
    else {
      this.setState({ dynamicTableProductionLines: [], plannedLineData: [], dynamicTableWorkInProgress: [], orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: false } });
      this.toastObj.show(this.state.toasterMessage[11]);
      global.hideLoader();
    }
  }

  //----------------------GRID-EVENTS------------------------//

  // createNewGrid = () => {
  //   const { plannedLineData } = this.state;
  //   let dynamicTableProductionLines = [];
  //   dynamicTableProductionLines.push(
  //     <GridComponent key={"dynamicGrid"} allowExcelExport={true} dataSource={plannedLineData} toolbarClick={this.exportDynamicGrid} id='dynamicGrid' toolbar={['ExcelExport']}
  //       ref={grid => this.dynamicGridObj = grid} allowResizing={true} rowSelected={this.rowSelected} allowTextWrap={true} gridLines='Both' allowGrouping={true}>
  //       <ColumnsDirective >
  //         <ColumnDirective field='lineName' headerText={"Line Name"} width='100' />
  //         <ColumnDirective field='saidId' headerText={"Said"} width='100' />
  //         <ColumnDirective field='startDateTime' format={this.state.format} headerText={"StartDate"} width='100' />
  //         <ColumnDirective field='endDateTime' format={this.state.format} headerText={"EndDate"} width='100' />
  //         <ColumnDirective field='lineItemNo' headerText={"Line Item No"} width='100' />
  //         <ColumnDirective field='totalQtyToProduced' headerText={"Total Qty."} width='100' />
  //         <ColumnDirective field='totalQtyAllocatedInLine' headerText={"Planned Qty."} width='100' />
  //         <ColumnDirective field='unScheduledQty' headerText={"UnScheduled Qty."} width='100' />
  //         <ColumnDirective field='buyerName' headerText={"Buyer Po"} width='100' />
  //         <ColumnDirective field='apm' headerText={"Apm"} width='100' />
  //         <ColumnDirective field='merchandiserName' headerText={"Merchandiser Name"} width='100' />
  //         <ColumnDirective field='productCategoryName' headerText={"Category"} width='100' />
  //         {/* <ColumnDirective field='status' headerText={"Status"} width='100' /> */}
  //       </ColumnsDirective>
  //       <Inject services={[Selection, ForeignKey, Group, Resize, Toolbar, ExcelExport]} />
  //     </GridComponent>)
  //   this.setState({ dynamicTableProductionLines, dynamicTableWorkInProgress: [], orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: false } }, () => {
  //     global.hideLoader()
  //   })
  // }

  rowSelected = (args) => {
    const { selectedTab } = this.state;
    let selectedLineItemObj = Object.assign({}, args.data);
    global.showLoader();
    if (selectedTab == "planned") {
      this.getAPM(selectedLineItemObj)
      this.setState({
        isSidebar: true, currrentDate: new Date(selectedLineItemObj.startDateTime.getFullYear(), selectedLineItemObj.startDateTime.getMonth()),
        selectedLineItemIdToEdit: selectedLineItemObj.id, selectedLineItemUnitIdToGetUnitList: selectedLineItemObj.unitId
      })
      document.body.classList.add('overflow-hidden-body');
    }
  }

  getMonthName = (startDate) => {
    const { months } = this.state;
    return months[startDate.getMonth()];
  }

  onSelectBuyerPoGrid = (args) => {
    const { selectedPlantForUpcomingBuyerPo, orderDetails, buyerPoList } = this.state;
    let data = null;
    if (orderDetails.isInvokedFromPlanningHistory) {
      data = buyerPoList.find(element => {
        if (element.buyerPoNr == orderDetails.buyerPoNo && element.saidNr == orderDetails.saidId)
          return element;
      })
    } else {
      data = args.data;
    }
    if (data != null && data != undefined)
      this.setState({ selectedBuyerPo: data.buyerPoNr, selectedBuyerPoData: data, buyerPoLineItemsList: [], dynamicTableProductionLines: [], plannedLineData: [] }, () => {
        this.getBuyerPoLineItemList(data.saidNr, data.buyerPoNr, selectedPlantForUpcomingBuyerPo);
      })
  }

  //----------------------SCHEDULER-SIDEBAR-EVENTS------------------------//

  onChangeNumText = (event) => {
    let that = this;
    const { schedulerEventsDataOnLoad, index } = that.state;
    let objectToBeChanged = Object.assign({}, schedulerEventsDataOnLoad[index])
    objectToBeChanged.plannedData[event.target.id] = event.value;
    let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: objectToBeChanged })
    that.setState({ schedulerEventsDataOnLoad: modifiedArray }, () => {
      that.estimateReqWorkingDays();
    })
  }

  onChangeNumTextScheduleQty = (event) => {
    let that = this;
    const { schedulerEventsDataOnLoad, index } = that.state;
    let objectToBeChanged = Object.assign({}, schedulerEventsDataOnLoad[index])
    objectToBeChanged.plannedData[event.target.id] = event.value;
    let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: objectToBeChanged })
    that.setState({ schedulerEventsDataOnLoad: modifiedArray })
  }

  onChangeSidebarDropdown = (event) => {
    if (event.e != undefined) {
      let that = this;
      const { index, schedulerEventsDataOnLoad } = that.state;
      let objectToBeChanged = Object.assign({}, schedulerEventsDataOnLoad[index])
      objectToBeChanged.plannedData[event.element.id] = event.value;
      let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: objectToBeChanged })
      that.setState({ schedulerEventsDataOnLoad: modifiedArray })
    }
  }

  onChangeSidebarProdLineIdDropdown = (event) => {
    let that = this;
    const { schedulerEventsDataOnLoad, index, transferLineItemPLlist } = that.state;
    if (event.e != undefined) {
      let objectToBeChanged = Object.assign({}, schedulerEventsDataOnLoad[index])
      objectToBeChanged.plannedData.estimatedWorkingDays = 0;
      objectToBeChanged.plannedData.scheduledQty = 0;
      objectToBeChanged.plannedData.prodLineId = event.value;
      objectToBeChanged.plannedData.productCategoryId = event.itemData.categories[0].id;
      objectToBeChanged.plannedData.scheduleStartDateTime = objectToBeChanged.plannedData.startDateTime;
      objectToBeChanged.plannedData.scheduleEndDateTime = objectToBeChanged.plannedData.endDateTime;
      transferLineItemPLlist.find(item => {
        if (item.id == event.value) {
          this.state.maxAllowedWorkingHours = item.workingHour;
        }
      })
      let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: objectToBeChanged })
      that.setState({ schedulerEventsDataOnLoad: modifiedArray, totalNoOfMachines: event.itemData.numberOfMachines })
    }
  }

  onChangeSidebarUnit = (event) => {
    const { index, schedulerEventsDataOnLoad } = this.state;
    if (event.e != undefined) {
      let objectToBeChanged = Object.assign({}, schedulerEventsDataOnLoad[index])
      objectToBeChanged.plannedData.transferTo = event.value;
      objectToBeChanged.plannedData.prodLineId = null;
      let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: objectToBeChanged });
      this.setState({ schedulerEventsDataOnLoad: modifiedArray, transferLineItemPLlist: [] }, () => {
        this.getUnitProductionLinesForTransferTo(event.value);
      })
    }
  }


  async onChangeSideBarDatePicker(args, that) {
    const { selectedTab, model, index, schedulerEventsDataOnLoad, isDirtyOnEdit } = that.state;
    if (((args.event != null) && (args.value != null)) || (('values' in args) && (args.value != null))) {  // Change + Today
      let clickedEventObj = Object.assign({}, schedulerEventsDataOnLoad[index])
      if (selectedTab == "planned") {
        if ((clickedEventObj.plannedData.estimatedWorkingDays != null) && (args.element.id == "scheduleStartDateTime")) {

          let estimatedDays = await that.getEstimatedWorkingDays(clickedEventObj.plannedData.buyerPo, clickedEventObj.plannedData.lineItemNo, clickedEventObj.plannedData.noOfMachineToUse,
            clickedEventObj.plannedData.prodLineId, clickedEventObj.plannedData.scheduledQty, clickedEventObj.plannedData.saidId, clickedEventObj.plannedData.sam, that.getStartDateExcludingWeekend(args.value));

          let ceilDay = Math.ceil(estimatedDays.workingDays + estimatedDays.holidays);
          ceilDay = ceilDay > 0 ? ceilDay - 1 : 0
          // let estimatedEndDate = new Date((args.value).getFullYear(), (args.value).getMonth(), (args.value).getDate() + ceilDay, 23, 59)
          let estimatedEndDate = that.getEndDateExcludingWeekend(new Date((args.value).getFullYear(), (args.value).getMonth(), (args.value).getDate()), ceilDay);
          clickedEventObj.plannedData.scheduleStartDateTime = that.getStartDateExcludingWeekend(args.value);
          clickedEventObj.plannedData.scheduleEndDateTime = estimatedEndDate;
          let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: clickedEventObj })
          that.setState({ schedulerEventsDataOnLoad: modifiedArray, minStartDate: args.value })
        }
        else {
          let endDate = new Date((args.value).getFullYear(), (args.value).getMonth(), (args.value).getDate(), 23, 59)
          clickedEventObj.plannedData[args.element.id] = endDate;
          let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: clickedEventObj })
          that.setState({ schedulerEventsDataOnLoad: modifiedArray })
        }
      }
    } else if ((args.event != null) && (args.value == null)) {  // No Date
      let clickedEventObj = Object.assign({}, schedulerEventsDataOnLoad[index])
      if (selectedTab == "planned") {
        if ((clickedEventObj.plannedData.estimatedWorkingDays != null) && (args.element.id == "scheduleStartDateTime")) {
          clickedEventObj.plannedData.scheduleStartDateTime = null;
          clickedEventObj.plannedData.scheduleEndDateTime = null;
          let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: clickedEventObj })
          that.setState({ schedulerEventsDataOnLoad: modifiedArray, minStartDate: args.value })
        }
        else {
          clickedEventObj.plannedData[args.element.id] = null;
          let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: clickedEventObj })
          that.setState({ schedulerEventsDataOnLoad: modifiedArray })
        }
      }
    }
  }

  getStartDateExcludingWeekend(currentDateFromPivotCell) {
    let startDate = currentDateFromPivotCell;
    // if (startDate.getDay() == 0) {
    //   startDate = new Date(startDate.setDate(startDate.getDate() + 1));
    // }
    startDate.setHours(9);
    return startDate;
  }

  async estimateReqWorkingDays() {
    const { model, schedulerEventsDataOnLoad, maxAllowedWorkingHours, index, currrentDate } = this.state;
    if ((this.state.selectedTab == "planned") && ((schedulerEventsDataOnLoad[index].plannedData.sam != null) && (schedulerEventsDataOnLoad[index].plannedData.sam != 0)) && ((schedulerEventsDataOnLoad[index].plannedData.scheduledQty != null) && (schedulerEventsDataOnLoad[index].plannedData.scheduledQty != 0)) && ((schedulerEventsDataOnLoad[index].plannedData.noOfMachineToUse != null) && (schedulerEventsDataOnLoad[index].plannedData.noOfMachineToUse != 0)) && (maxAllowedWorkingHours != null)) {   // Edit mode

      let clickedEventObj = Object.assign({}, schedulerEventsDataOnLoad[index])
      let estimatedDays = await this.getEstimatedWorkingDays(clickedEventObj.plannedData.buyerPo, clickedEventObj.plannedData.lineItemNo, clickedEventObj.plannedData.noOfMachineToUse, clickedEventObj.plannedData.prodLineId, clickedEventObj.plannedData.scheduledQty, clickedEventObj.plannedData.saidId, clickedEventObj.plannedData.sam, clickedEventObj.plannedData.scheduleStartDateTime);

      // let totalMenDaysToProduceQty = ((schedulerEventsDataOnLoad[index].plannedData.scheduledQty) * (schedulerEventsDataOnLoad[index].plannedData.sam)) / (60 * maxAllowedWorkingHours);
      // let workingDays = totalMenDaysToProduceQty / schedulerEventsDataOnLoad[index].plannedData.noOfMachineToUse;
      // let roundOffWorkingDays = Math.round(workingDays * 100) / 100;]

      let ceilDay = Math.ceil(estimatedDays.workingDays + estimatedDays.holidays);
      ceilDay = ceilDay > 0 ? ceilDay - 1 : 0
      let estimatedEndDate = this.getEndDateExcludingWeekend(new Date((clickedEventObj.plannedData.scheduleStartDateTime).getFullYear(), (clickedEventObj.plannedData.scheduleStartDateTime).getMonth(), (clickedEventObj.plannedData.scheduleStartDateTime).getDate()), ceilDay);
      // let estimatedEndDate = new Date((clickedEventObj.startDateTime).getFullYear(), (clickedEventObj.startDateTime).getMonth(), (clickedEventObj.startDateTime).getDate() + ceilDay, 23, 59)
      clickedEventObj.plannedData.scheduleStartDateTime = new Date((clickedEventObj.plannedData.scheduleStartDateTime).getFullYear(), (clickedEventObj.plannedData.scheduleStartDateTime).getMonth(), (clickedEventObj.plannedData.scheduleStartDateTime).getDate(), 9)
      clickedEventObj.plannedData.scheduleEndDateTime = estimatedEndDate;
      clickedEventObj.plannedData.estimatedWorkingDays = estimatedDays.workingDays;
      let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: clickedEventObj })
      this.setState({ schedulerEventsDataOnLoad: modifiedArray, minStartDate: clickedEventObj.plannedData.scheduleStartDateTime })
    }
  }

  getEndDateExcludingWeekend(startDate, noOfDaysToAdd) {
    let endDateExcludingWeekend = null;
    let count = 0;
    while (count < noOfDaysToAdd) {
      endDateExcludingWeekend = new Date(startDate.setDate(startDate.getDate() + 1));
      endDateExcludingWeekend.setHours(23)
      endDateExcludingWeekend.setMinutes(59)
      // if (endDateExcludingWeekend.getDay() != 0) {
      count++;
      // }
    }
    if (noOfDaysToAdd == 0) {
      endDateExcludingWeekend = startDate;
      endDateExcludingWeekend.setHours(23)
      endDateExcludingWeekend.setMinutes(59)
    }
    return endDateExcludingWeekend;
  }

  //-------------------SCHEDULER-EVENTS-------------------//


  onClickPlannedEventPopup = (args) => {
    console.log(args);
    const { selectedLineItemIdToEdit, selectedEventColor, selectedTab, transferLineItemPLlist, selectedLineItemUnitIdToGetUnitList } = this.state;
    if (args.type == "QuickInfo") {
      args.cancel = true;
      if (((args.type == "QuickInfo") && (args.data.monthId != undefined)) || ((args.type == "Editor") && (args.data.monthId != undefined))) {
        global.showLoader();
        this.setState({ transferLineItemPLlist: [] }, () => {
          this.getUnitProductionLinesForTransferTo(selectedLineItemUnitIdToGetUnitList).then(() => {
            this.getClickedEventPlannedData(args.data, this.getArrayEventIndex(args.data), "onClick");
          })
        })
      }
    }
  }

  getArrayEventIndex = (selectedLineItemObj) => {
    const { schedulerEventsDataOnLoad, selectedTab } = this.state;
    // let that = this;
    let index;
    // && (that.state.schedulerDataOnLoad[i].id == selectedLineItemObj.id)
    let lineItemNo = selectedLineItemObj.lineItemNo == undefined ? selectedLineItemObj.lineItem : selectedLineItemObj.lineItemNo;  // ** To be fixed (Different line item name confliction)
    let categoryColor = selectedLineItemObj.categoryColor;
    let id = selectedTab == "planned" ? selectedLineItemObj.id : selectedLineItemObj.plannedInfoId;
    if (categoryColor == undefined) {
      schedulerEventsDataOnLoad.map(item => {
        if (item.plannedInfoId == id) {
          categoryColor = item.categoryColor;
        }
      })
    }

    for (let i = 0; i < schedulerEventsDataOnLoad.length; i++) {
      if ((schedulerEventsDataOnLoad[i].startDateTime.getTime() == selectedLineItemObj.startDateTime.getTime()) &&
        (schedulerEventsDataOnLoad[i].endDateTime.getTime() == selectedLineItemObj.endDateTime.getTime()) &&
        (schedulerEventsDataOnLoad[i].saidId == selectedLineItemObj.saidId) && (schedulerEventsDataOnLoad[i].lineItem == lineItemNo) && (schedulerEventsDataOnLoad[i].categoryColor == categoryColor)) {   // Unique ID need to be set for comparision.
        index = i;
        break;
      }
    }
    return index;
  }

  setColorToEventOnUI = (args) => {
    let categoryColor = args.data.categoryColor;
    if (!args.element || !categoryColor) {
      return;
    }
    args.element.firstChild.style.borderLeftColor = categoryColor;
    args.element.style.backgroundColor = categoryColor;
  }


  //-----------------CRUD-OPERATION--------------------------//

  onSchedule = (isEditOnUnschedule) => {
    this.modifyData(isEditOnUnschedule);
  }


  modifyData(isEditOnUnschedule) {
    let copiedschedulerEventsDataOnLoad = this.state.schedulerEventsDataOnLoad;
    let modifiedSchedulerDataToSetInModel = [];
    let unScheduledDataId = [];
    let prodLineIdToGetRefreshedEvents = null;
    //Converting To Desired Date Format
    if ((this.state.selectedTab == "planned") || (isEditOnUnschedule)) {
      for (let i = 0; i < copiedschedulerEventsDataOnLoad.length; i++) {
        if ((copiedschedulerEventsDataOnLoad[i].plannedData.id == this.state.selectedLineItemIdToEdit) && (this.state.selectedLineItemIdToEdit != null)) {
          let newEventObject = null;
          if (this.state.selectedTab == "planned") {
            newEventObject = new SavePlannedEventModel(null, copiedschedulerEventsDataOnLoad[i].monthId, copiedschedulerEventsDataOnLoad[i].categoryColor,
              copiedschedulerEventsDataOnLoad[i].description, copiedschedulerEventsDataOnLoad[i].startDateTime, copiedschedulerEventsDataOnLoad[i].endDateTime, null, null,
              copiedschedulerEventsDataOnLoad[i].saidId, copiedschedulerEventsDataOnLoad[i].lineItem, copiedschedulerEventsDataOnLoad[i].prodLineId, copiedschedulerEventsDataOnLoad[i].plannedInfoId,
              copiedschedulerEventsDataOnLoad[i].plannedData, false)
          } else {
            // TO DO FOR EDIT CASE ON UN-SCHEDULE
            let scheduledId = copiedschedulerEventsDataOnLoad[i].plannedInfoId;  //swap
            let plannedInfoId = copiedschedulerEventsDataOnLoad[i].plannedData.id;
            newEventObject = new SavePlannedEventModel(null, copiedschedulerEventsDataOnLoad[i].monthId, copiedschedulerEventsDataOnLoad[i].categoryColor,
              copiedschedulerEventsDataOnLoad[i].description, copiedschedulerEventsDataOnLoad[i].startDateTime, copiedschedulerEventsDataOnLoad[i].endDateTime, null, null,
              copiedschedulerEventsDataOnLoad[i].saidId, copiedschedulerEventsDataOnLoad[i].lineItem, copiedschedulerEventsDataOnLoad[i].prodLineId, plannedInfoId, copiedschedulerEventsDataOnLoad[i].plannedData, false)
            copiedschedulerEventsDataOnLoad[i].plannedData.id = scheduledId;
          }

          // DIFFERENCIATE BW NEW & OLD EVENT
          let monthId = null;
          if (copiedschedulerEventsDataOnLoad[i].IsAllDay == undefined) {
            if (this.state.selectedTab == "planned") {
              newEventObject.id = null;
            } else {
              newEventObject.id = copiedschedulerEventsDataOnLoad[i].id;
            }
            monthId = copiedschedulerEventsDataOnLoad[i].monthId;
          }

          delete newEventObject.monthId;
          delete newEventObject.oldStartDateTime;
          delete newEventObject.oldEndDateTime;
          delete newEventObject.IsReadonly;
          if (this.state.selectedTab == "planned") {
            newEventObject.scheduledData.status = "SCHEDULED"
            let eventDescName = (newEventObject.description).substr(0, (newEventObject.description).indexOf('- Qty'));
            newEventObject.description = eventDescName + "- Qty " + newEventObject.scheduledData.totalQtyAllocatedInLine;
          } else { //Scheduled
            // this.state.deletedEventsId.push(this.state.selectedScheduledLineItemId);
            // newEventObject.scheduledData.status = "UN-SCHEDULED"
          }

          // IF EVENT OBJECT EXIST MODIFY THAT MODEL

          let isAddedToExistedObj = false;
          for (let j = 0; j < modifiedSchedulerDataToSetInModel.length; j++) {
            if (((modifiedSchedulerDataToSetInModel[j].date).getFullYear() == (newEventObject.startDateTime).getFullYear())
              && ((modifiedSchedulerDataToSetInModel[j].date).getMonth() == (newEventObject.startDateTime).getMonth())) {
              modifiedSchedulerDataToSetInModel[j].dayWiseWorkLoad.push(newEventObject);
              isAddedToExistedObj = true;
              break;
            }
          }
          // IF EVENT OBJECT DOESN'T EXIST CREATE NEW MODEL
          if (modifiedSchedulerDataToSetInModel.length == 0) {
            isAddedToExistedObj = true;
            let dayWiseWorkLoadData = [];
            dayWiseWorkLoadData.push(newEventObject)
            let newSchedulerModel = new ScheduleModel(monthId, copiedschedulerEventsDataOnLoad[i].startDateTime, copiedschedulerEventsDataOnLoad[i].prodLineId, dayWiseWorkLoadData)
            modifiedSchedulerDataToSetInModel.push(newSchedulerModel);
          }
          if (!isAddedToExistedObj) {
            let dayWiseWorkLoadData = [];
            dayWiseWorkLoadData.push(newEventObject)
            let newSchedulerModel = new ScheduleModel(monthId, copiedschedulerEventsDataOnLoad[i].startDateTime, copiedschedulerEventsDataOnLoad[i].prodLineId, dayWiseWorkLoadData)
            modifiedSchedulerDataToSetInModel.push(newSchedulerModel);
          }
        }
      }
    } else { //schedule
      for (let i = 0; i < copiedschedulerEventsDataOnLoad.length; i++) {
        if ((this.state.selectedLineItemIdToEdit != null) && (copiedschedulerEventsDataOnLoad[i].plannedData.id == this.state.selectedLineItemIdToEdit)) {
          unScheduledDataId.push(copiedschedulerEventsDataOnLoad[i].id);
          prodLineIdToGetRefreshedEvents = copiedschedulerEventsDataOnLoad[i].prodLineId;
        }
      }
    }

    this.setState({ scheduledModel: { ...this.state.scheduledModel, schedulerData: modifiedSchedulerDataToSetInModel, deletedEventsId: unScheduledDataId }, isDisabledScheduledButton: true }, () => {
      if (this.isValidate(isEditOnUnschedule)) {
        global.showLoader()
        if ((this.state.selectedTab == "planned") || (isEditOnUnschedule))
          this.saveScheduledData();
        else
          this.unScheduledData(prodLineIdToGetRefreshedEvents);
      } else {
        this.setState({ isDisabledScheduledButton: false })
        global.hideLoader()
      }
    })
    console.log(modifiedSchedulerDataToSetInModel);
  }


  //################################################################ SCHEDULED-TAB ##################################################################################//

  //--------------------API-CALLS---------------------//

  async getPlantList() {
    // global.showLoader();
    let response = (await this.LineSchedulingService.getPlantList())
    if ((response != undefined) && (response.data.status == "success") && (this.mounted))
      this.setState({ plants: response.data.data }, () => {
        // global.hideLoader();
      })
  }

  async getUnitList(plantId) {
    let response = (await this.LineSchedulingService.getUnitList(plantId))
    if ((response != undefined) && (response.data.status == "success")) {
      this.setState({ units: response.data.data })
    }
  }

  async getProductionLines(unitId) {
    let response = (await this.LineSchedulingService.getUnitProductionLines(unitId))
    if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
      let defaultSelectionModifiedArray = response.data.data;
      defaultSelectionModifiedArray.push({ lineName: "All", id: 0 });
      this.setState({ unitProductionLines: defaultSelectionModifiedArray, selectedLineName: 0 })
    }
  }

  async unScheduledData(prodLineId) {
    let that = this;
    const { t, i18n } = that.props;
    let response = (await that.LineSchedulingService.saveUnScheduledData(that.state.scheduledModel));
    if ((response != undefined) && (response.data.status == "success")) {
      that.setState({
        isDisabledScheduledButton: false, isRefreshPivotGrid: true, selectedEventColor: null, selectedEventName: null, selectedLineItemColorToSchedule: null, selectedLineItemNameToSchedule: null, isDisabledFields: false,
        model: new PlannedLineModel(null, null, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", "", null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null), selectedLineItemIdToEdit: null, index: null
      })
      await that.getSelectedMonthScheduledEvents(prodLineId, that.state.currrentDate)
      that.scheduleObj.refresh();
      global.hideLoader()
      that.toastObj.show(that.state.toasterMessage[14]);
    } else if ((response != undefined) && (response.data.status == "fail")) {
      that.setState({ isDisabledScheduledButton: false })
      global.hideLoader()
      that.toastObj.show(that.createToaster(t(errorMessages[response.data.data.errorCode]), "Error"));
    } else {
      that.setState({ isDisabledScheduledButton: false })
      global.hideLoader()
      that.toastObj.show(that.state.toasterMessage[2]);
    }
  }

  //--------------------ON-CHANGE-HANDLER---------------------//

  onChangeYear = (event) => {
    const { selectedUnitName, selectedPlantName, selectedLineName } = this.state;
    if (event.event != undefined) {

      if (selectedUnitName != null && event.value != null) {
        global.showLoader();
        if (selectedLineName != null) {
          this.getLineScheduledDetails(selectedUnitName, selectedLineName, event.value.getFullYear());
        } else {
          this.setState({ selectedLineName: 0 });
          this.getLineScheduledDetails(selectedUnitName, 0, event.value.getFullYear());
        }
      } else {
        global.hideLoader();
      }
      this.setState({ selectedYear: event.value, targetYear: event.value != null ? event.value.getFullYear() : null })
    }
  }

  onChangePlant = (event) => {
    if (event.e != undefined) {
      this.setState({ selectedPlantName: event.value, units: [], unitProductionLines: [], dynamicPivotGrids: [] }, () => {
        this.getUnitList(event.value)
      })
    }
  }

  onChangeUnit = (event) => {
    if (event.e != undefined) {
      this.setState({ unitProductionLines: [], selectedUnitName: event.value, dynamicPivotGrids: [], selectedYear: null }, () => {
        // global.showLoader()
        this.getProductionLines(event.value);
        // this.getLineScheduledDetails(event.value, 0,this.state.selectedYear)
      })
    }
  }

  onChangeProductionLine = (event) => {
    const { selectedUnitName } = this.state;
    if (event.e != undefined) {
      if (this.state.selectedYear == null) {
        this.toastObj.show({ title: 'Info!', content: "Please select Year!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
      }
      else {
        global.showLoader();
        this.state.dynamicPivotGrids = []
        this.setState({ selectedLineName: event.value, dynamicPivotGrids: [] })
        this.getLineScheduledDetails(selectedUnitName, event.value, this.state.selectedYear.getFullYear())
      }
      this.setState({ selectedLineName: event.value })
    }
  }

  async getLineScheduledDetails(unitId, prodLineId, year) {
    let response = (await this.LineSchedulingService.getLineScheduledDetails(unitId, prodLineId, year))
    if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
      this.state.lineSchedulingDetails = response.data.data;
      this.createPivotGrid();
    } else {
      // Toaster
      global.hideLoader();
    }
  }

  //-------------------PIVOT-GRID---------------------//

  createPivotGrid() {
    const { t, i18n } = this.props;
    const { lineSchedulingDetails } = this.state;
    let dynamicPivotGridLinesDetails = [];
    for (let i = 0; i < lineSchedulingDetails.length; i++) {
      dynamicPivotGridLinesDetails.push(
        <div key={i.toString()}>
          <div className="mt-2"></div>
          <Row>
            <Col lg='12'>
              <Card>
                <CardHeader>
                  <Row>
                    <Col md="3" sm="4" xs="4" className="pt-1">Line Name : <b>{lineSchedulingDetails[i].lineName}</b> </Col>
                    <Col md="2" sm="4" xs="4" className="pt-1">Catg : <b>{this.state.unitProductionLines[i].categories[0].name}</b> </Col>
                    <Col md="2" sm="4" xs="4" className="pt-1">Machines : <b>{lineSchedulingDetails[i].maxNoOfMachAllowedInLine}</b> </Col>
                    <Col md="2" sm="4" xs="4" className="pt-1"> Work.Hrs : <b>{lineSchedulingDetails[i].numberOfLineWorkingHours}</b> </Col>

                    <Col md="3" sm="3" xs="4" className="text-right">
                      <a className="icon-button reset-planning-data ml-1 mr-1" id={lineSchedulingDetails[i].prodLineId} name={lineSchedulingDetails[i].lineName} onClick={this.onResetLineSchedulingData} title="Reset Planning Data"></a>
                    </Col>
                  </Row>
                </CardHeader>
                <CardBody>
                  <div className="clearfix"></div>
                  <PivotViewComponent className="line-planning-cal"
                    locale={i18n.language}
                    ref={(pivotview) => { this[lineSchedulingDetails[i].lineName] = pivotview; }}
                    id={"lineNameEnds" + lineSchedulingDetails[i].prodLineId + "lineIdEnds" + i}   // FORMAT : lineNameabc-789_1
                    dataSourceSettings={{
                      enableSorting: false,
                      columns: [{ name: "month", caption: "Month" }, { name: "week", caption: "Week" }],
                      values: [{ name: "cellValue", caption: "Data" }],
                      rows: [{ name: "rowLabel", caption: "Parameter" }],
                      expandAll: false,
                      showGrandTotals: false,
                      filters: [],
                      formatSettings: [{ name: 'cellValue', useGrouping: true }],
                      // emptyCellsTextContent: '0',
                      dataSource: this.convertPivotData(lineSchedulingDetails[i].lineSchedulingForMonthDtos),
                      conditionalFormatSettings: this.conditionalFormattingSchedulingView(lineSchedulingDetails[i])
                    }}
                    // editSettings={{ allowAdding: false, allowDeleting: false, allowEditing: true, mode: 'Normal' }}
                    cellClick={(e) => { this.onCellClick(e, lineSchedulingDetails[i].prodLineId, lineSchedulingDetails[i].lineName) }}
                    gridSettings={{
                      allowSelection: true,
                      selectionSettings: { mode: 'Cell', type: 'Single' }
                    }}
                    allowConditionalFormatting={true}
                    showTooltip={false}>
                    <Inject services={[ConditionalFormatting]} />
                  </PivotViewComponent>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
    this.setState({ dynamicPivotGrids: dynamicPivotGridLinesDetails })
    global.hideLoader();
  }

  convertPivotData(lineSchedulingForMonthDtos) {
    let modifiedLineSchedulingDataForGrid = [];
    for (let i = 0; i < lineSchedulingForMonthDtos.length; i++) {
      for (let j = 0; j < lineSchedulingForMonthDtos[i].lineSchedulingForWeekDtos.length; j++) {
        modifiedLineSchedulingDataForGrid = modifiedLineSchedulingDataForGrid.concat(lineSchedulingForMonthDtos[i].lineSchedulingForWeekDtos[j].lineSchedulingWeekInfoDtos)
      }
    }
    return modifiedLineSchedulingDataForGrid;
  }


  onResetLineSchedulingData = (event) => {
    const { selectedLineName, selectedUnitName, selectedYear } = this.state;
    global.showLoader();
    this.getLineSchedulingDetailsOnRefresh(selectedLineName, selectedUnitName, event.target.name, selectedYear.getFullYear());
  }


  async getLineSchedulingDetailsOnRefresh(prodLineId, unitId, lineNameToRefresh, year) {
    let response = (await this.LineSchedulingService.getLineScheduledDetails(unitId, prodLineId, year))
    if ((response != undefined) && (response.data.status == "success") && (response.data.data.length > 0)) {
      this.state.linePlanningDetails = response.data.data;

      this[lineNameToRefresh].dataSourceSettings.dataSource = this.convertPivotData(this.returnChangedLineFromPivot(lineNameToRefresh, response.data.data).lineSchedulingForMonthDtos);
      this[lineNameToRefresh].pivotRefresh();
      global.hideLoader();
    } else {
      if ((response.data != undefined) && (response.data.data != undefined) && (response.data.data.length == 0)) {
        this.toastObj.show(this.state.toasterMessage[8]);
        this.setState({ dynamicPivotGrids: [] }, () => {
          global.hideLoader();
        })
      } else {
        this.toastObj.show(this.state.toasterMessage[2]);
        this.setState({ dynamicPivotGrids: [] }, () => {
          global.hideLoader();
        })
      }
    }
  }


  returnChangedLineFromPivot(lineNameToRefresh, data) {
    let lineItemDataToRefresh = data.filter(function (item) {
      return item.lineName == lineNameToRefresh;
    })
    return lineItemDataToRefresh[0];
  }


  onCellClick = (event, selectedProdLineId, selectedLineName) => {
    let that = this;
    const { t, i18n } = that.props;
    const { lineSchedulingDetails } = this.state;
    let selectedMonth;
    if ((event.data != undefined) && (event.data.columnHeaders != undefined)) {
      selectedMonth = (event.data.columnHeaders.indexOf('.') !== -1) ? event.data.columnHeaders.substr(0, event.data.columnHeaders.indexOf('.')) : event.data.columnHeaders;
    }
    // if ((((event.data != undefined) && (event.data.rowHeaders != undefined))) && ((event.data.style == undefined) || ((event.data.style != undefined) && (event.data.style.text2.indexOf(selectedMonth) == -1)))) {  // DON'T REMOVE 
    if ((event.data != undefined) && (event.data.rowHeaders != undefined)) {
      // let selectedLineId = Number((event.currentCell.offsetParent.id).split('lineNameEnds').pop().split('lineIdEnds')[0]);
      //   let selectedLineNameObject = that.state.unitProductionLines.find(function (item) {
      //     return item.id == selectedLineId;
      //   })
      // let selectdedLineName = (event.currentCell.offsetParent.id).split('lineNameStarts').pop().split('lineNameEnds')[0];
      let selectedYear;
      let selectedMonthNo;
      lineSchedulingDetails.map(linePlanningDetail => {
        if (linePlanningDetail.prodLineId == selectedProdLineId) {
          linePlanningDetail.lineSchedulingForMonthDtos.map(lineSchedulingForMonthDto => {
            if (lineSchedulingForMonthDto.monthName == selectedMonth) {
              selectedYear = lineSchedulingForMonthDto.year;
              selectedMonthNo = lineSchedulingForMonthDto.monthNumber;
            }
          })
        }
      })

      let tempMinNavigationDate;
      let tempMaxNavigationDate;
      // Fetching dates to be freeze.
      for (let i = 0; i < lineSchedulingDetails.length; i++) {
        if (selectedProdLineId == lineSchedulingDetails[i].prodLineId) {
          var noOfMachineInLine = lineSchedulingDetails[i].maxNoOfMachAllowedInLine;
          var workingHoursForLine = lineSchedulingDetails[i].numberOfLineWorkingHours;
          for (let j = 0; j < lineSchedulingDetails[i].lineSchedulingForMonthDtos.length; j++) {
            if (lineSchedulingDetails[i].lineSchedulingForMonthDtos[j].isFreeze == false) {
              tempMinNavigationDate = new Date(lineSchedulingDetails[i].lineSchedulingForMonthDtos[j].year, lineSchedulingDetails[i].lineSchedulingForMonthDtos[j].monthNumber - 1)
              break;
            }
          }
          let lastMonthObject = lineSchedulingDetails[i].lineSchedulingForMonthDtos[lineSchedulingDetails[i].lineSchedulingForMonthDtos.length - 1];
          let lastDateOfMonth = new Date(lastMonthObject.year, lastMonthObject.monthNumber, 0).getDate()
          tempMaxNavigationDate = new Date(lastMonthObject.year, lastMonthObject.monthNumber - 1, lastDateOfMonth)   // find last date of month
        }
      }

      that.setState({ lineNameToRefresh: selectedLineName, isRefreshPivotGrid: false, maxAllowedWorkingHoursFromPivot: workingHoursForLine, maxAllowedMachinesFromPivot: noOfMachineInLine, minNavigationDate: tempMinNavigationDate, maxNavigationDate: tempMaxNavigationDate, deletedEventsId: [], selectedProdLineId: selectedProdLineId, selectedProdLineMonth: selectedMonth, selectedProdLineYear: selectedYear }, () => {
        that.getSelectedMonthScheduledEvents(selectedProdLineId, new Date(selectedYear, selectedMonthNo - 1));
      })
    }
  }



  async getSelectedMonthScheduledEvents(prodLineId, selectedMonthView) {
    let that = this;
    const { selectedUnitName, selectedPlantName, selectedLineName } = this.state;
    await that.getDaywiseNoOfAllocatedMachines(prodLineId);
    let response = (await that.LineSchedulingService.getSelectedMonthScheduledEvents(prodLineId, that.state.selectedYear.getFullYear()))
    if ((response != undefined) && (response.data.status == "success")) {
      let tempSchedulerEventsOnLoad = [];
      response.data.data.map(item => {
        item.dayWiseWorkLoadSchedule.map(dayWiseWorkLoadScheduleObj => {
          let convertedStartDate = new Date(Date.UTC(dayWiseWorkLoadScheduleObj.startDateTime.date.year, dayWiseWorkLoadScheduleObj.startDateTime.date.month - 1, dayWiseWorkLoadScheduleObj.startDateTime.date.day, dayWiseWorkLoadScheduleObj.startDateTime.time.hour, dayWiseWorkLoadScheduleObj.startDateTime.time.minute))
          let convertedEndDate = new Date(Date.UTC(dayWiseWorkLoadScheduleObj.endDateTime.date.year, dayWiseWorkLoadScheduleObj.endDateTime.date.month - 1, dayWiseWorkLoadScheduleObj.endDateTime.date.day, dayWiseWorkLoadScheduleObj.endDateTime.time.hour, dayWiseWorkLoadScheduleObj.endDateTime.time.minute))
          let schedulerDateObj = new PlannedEventModel(dayWiseWorkLoadScheduleObj.id, item.id, dayWiseWorkLoadScheduleObj.categoryColor, dayWiseWorkLoadScheduleObj.description, convertedStartDate, convertedEndDate, convertedStartDate, convertedEndDate, dayWiseWorkLoadScheduleObj.saidId, dayWiseWorkLoadScheduleObj.lineItem, dayWiseWorkLoadScheduleObj.prodLineId, dayWiseWorkLoadScheduleObj.plannedInfoId,
            new PlannedLineModel(null, null, null, null, null, null, null, selectedPlantName, selectedUnitName, selectedLineName, null, null, null, "", "", "", "", "", "", null, null, dayWiseWorkLoadScheduleObj.lineItem, null, null, convertedStartDate, convertedEndDate, convertedStartDate, convertedEndDate, null, null, null, null, null, null, null, null, null), true)
          tempSchedulerEventsOnLoad.push(schedulerDateObj);
        })
      })
      that.setState({ schedulerEventsDataOnLoad: tempSchedulerEventsOnLoad, currrentDate: selectedMonthView, isSidebar: true })
      document.body.classList.add('overflow-hidden-body');
    } else {
      that.toastObj.show(that.state.toasterMessage[2]);
      document.body.classList.add('overflow-hidden-body');
    }
  }


  conditionalFormattingSchedulingView(lineSchedulingDetails) {
    const { t, i18n } = this.props;
    let conditionalFormatting = [];
    let namesOfFreezedColumnHeader = [];
    lineSchedulingDetails.lineSchedulingForMonthDtos.map(item => {
      if (item.isFreeze == true) {
        namesOfFreezedColumnHeader.push(item.monthName)
      }
    })
    lineSchedulingDetails.lineSchedulingForMonthDtos.map(item => {
      if (item.isFreeze == true) {
        conditionalFormatting.push(
          {
            label: item.monthName,
            conditions: 'GreaterThan',
            value1: -100000000000000,
            //value2: 50000,
            style: {
              backgroundColor: '#bbb9b7',
              text: 'freeze',
              text2: namesOfFreezedColumnHeader
            },
          },
        )
      }
    })
    conditionalFormatting.push(
      {
        label: t("cpm.lm.line.plan.line.open.md") || t("cpm.lm.line.plan.line.open.hrs"),
        conditions: 'LessThan',
        value1: -1,
        // value2: 50000,
        style: {
          backgroundColor: '#ff656d',
          text: 'overloaded',
          text2: namesOfFreezedColumnHeader
        }
      },
      {
        label: t("cpm.lm.line.plan.line.open.md") || t("cpm.lm.line.plan.line.open.hrs"),
        conditions: 'GreaterThan',
        value1: -1,
        //value2: -50000,
        style: {
          backgroundColor: '#6dc76e',
          text: 'editable',
          text2: namesOfFreezedColumnHeader
        }
      })
    return conditionalFormatting;

  }


  //-------------------SCHEDULER-EVENTS--------------------//

  onRenderCell = (args) => {
    let that = this;
    if ((args.elementType === 'monthCells') && (that.scheduleObj.currentView === 'Month') && (that.state.daywiseAvailableMachines.size > 0)) {
      let ele = document.createElement('div');
      ele.innerHTML = this.getAllocatedMachines(args.date);
      (args.element).appendChild(ele.firstChild);
    }
  }

  getAllocatedMachines = (value) => {
    const { daywiseAvailableMachines } = this.state;
    let machines = daywiseAvailableMachines.get(value.toDateString());
    if (machines != undefined)
      return '<div class="machine-cap">' + machines + '</div>';
    else
      return '<div></div>';
  }


  onClickScheduledEventPopup = (args) => {
    console.log(args);
    // const { selectedLineItemIdToEdit, selectedEventColor, selectedTab } = this.state;
    if (args.type == "QuickInfo") {
      args.cancel = true;
      if (((args.data.monthId != undefined)) || ((args.type == "Editor") && (args.data.monthId != undefined))) {
        this.getClickedEventScheduledData(args.data, this.getArrayEventIndexForScheduledEvent(args.data));
        this.setState({ isDisabledFields: true })
      }
    }
  }


  onClickSchedule(){
    this.setState({
        showDialog: true
    });
  }
  dialogClose() {
    this.setState({
        showDialog: false,
    });
  }

  getArrayEventIndexForScheduledEvent = (selectedLineItemObj) => {
    const { schedulerEventsDataOnLoad, selectedTab } = this.state;
    let index;
    for (let i = 0; i < schedulerEventsDataOnLoad.length; i++) {
      if (schedulerEventsDataOnLoad[i].id == selectedLineItemObj.id) {
        index = i;
        break;
      }
    }
    return index;
  }


  async getClickedEventScheduledData(eventModel, index) {
    const { selectedTab, selectedLine, selectedBuyerPoData, schedulerEventsDataOnLoad, selectedBuyerPo } = this.state;
    let that = this;
    let response = (await this.LineSchedulingService.getClickedEventScheduledData(eventModel.plannedData, eventModel.saidId, eventModel.prodLineId, eventModel.id))
    if ((response != undefined) && (response.data.status == "success")) {
      let eventDetailObj = response.data.data;
      let clickedEventObj = Object.assign({}, schedulerEventsDataOnLoad[index])

      let convertedShipToDate = new Date(Date.UTC(eventDetailObj.shipToDate.date.year, eventDetailObj.shipToDate.date.month - 1, eventDetailObj.shipToDate.date.day, eventDetailObj.shipToDate.time.hour, eventDetailObj.shipToDate.time.minute))
      let convetedStartDateTime = new Date(Date.UTC(eventDetailObj.linePlanningInfoDto.startDateTime.date.year, eventDetailObj.linePlanningInfoDto.startDateTime.date.month - 1, eventDetailObj.linePlanningInfoDto.startDateTime.date.day, eventDetailObj.linePlanningInfoDto.startDateTime.time.hour, eventDetailObj.linePlanningInfoDto.startDateTime.time.minute))
      let convertedEndDateTime = new Date(Date.UTC(eventDetailObj.linePlanningInfoDto.endDateTime.date.year, eventDetailObj.linePlanningInfoDto.endDateTime.date.month - 1, eventDetailObj.linePlanningInfoDto.endDateTime.date.day, eventDetailObj.linePlanningInfoDto.endDateTime.time.hour, eventDetailObj.linePlanningInfoDto.endDateTime.time.minute))
      let convertedScheduledStartDate = new Date(Date.UTC(eventDetailObj.scheduleStartDateTime.date.year, eventDetailObj.scheduleStartDateTime.date.month - 1, eventDetailObj.scheduleStartDateTime.date.day, eventDetailObj.scheduleStartDateTime.time.hour, eventDetailObj.scheduleStartDateTime.time.minute))
      let convertedScheduledEndDate = new Date(Date.UTC(eventDetailObj.scheduleEndDateTime.date.year, eventDetailObj.scheduleEndDateTime.date.month - 1, eventDetailObj.scheduleEndDateTime.date.day, eventDetailObj.scheduleEndDateTime.time.hour, eventDetailObj.scheduleEndDateTime.time.minute))
      clickedEventObj.plannedData = new PlannedLineModel(eventDetailObj.linePlanningInfoDto.id, eventDetailObj.linePlanningInfoDto.fmId, eventDetailObj.linePlanningInfoDto.fmName, eventDetailObj.linePlanningInfoDto.qaId, eventDetailObj.linePlanningInfoDto.qaName, eventDetailObj.linePlanningInfoDto.apmId, eventDetailObj.linePlanningInfoDto.saidId, clickedEventObj.plannedData.plantId, clickedEventObj.plannedData.unitId, eventDetailObj.linePlanningInfoDto.prodLineId, eventDetailObj.linePlanningInfoDto.mechandiserId, eventDetailObj.linePlanningInfoDto.productCategoryId, eventDetailObj.linePlanningInfoDto.buyerId, clickedEventObj.plannedData.plantName,
        clickedEventObj.plannedData.unitName, clickedEventObj.plannedData.lineName, eventDetailObj.linePlanningInfoDto.merchandiserName, "", eventDetailObj.linePlanningInfoDto.buyerName, eventDetailObj.linePlanningInfoDto.apm, eventDetailObj.linePlanningInfoDto.sam, eventDetailObj.linePlanningInfoDto.lineItemNo, eventDetailObj.linePlanningInfoDto.totalQtyAllocatedInLine, eventDetailObj.linePlanningInfoDto.totalQtyToProduced, convetedStartDateTime, convertedEndDateTime,
        convertedScheduledStartDate, convertedScheduledEndDate, eventDetailObj.noOfMachineToUse, eventDetailObj.linePlanningInfoDto.estimatedWorkingDays, eventDetailObj.linePlanningInfoDto.status, eventDetailObj.scheduleQtyForProduction, eventDetailObj.buyerPoNo, convertedShipToDate, eventDetailObj.linePlanningInfoDto.transferTo, null, null);
      let modifiedArray = Object.assign([], schedulerEventsDataOnLoad, { [index]: clickedEventObj })
      that.setState({ schedulerEventsDataOnLoad: modifiedArray, index, selectedLineItemIdToEdit: eventDetailObj.linePlanningInfoDto.id, isDisabledFields: true, selectedLineItemNameToSchedule: clickedEventObj.description, selectedLineItemColorToSchedule: clickedEventObj.categoryColor, selectedEventColor: clickedEventObj.categoryColor, selectedEventName: clickedEventObj.description })      // scheduled startdate not added here**
      global.hideLoader();
    } else {
      global.hideLoader();
      that.toastObj.show(that.state.toasterMessage[2]);
    }
  }

  //##################################################################### PLANNING-HISTORY-VIEW #############################################################//


  async getAllOrders() {
    global.showLoader();
    let response = (await this.LineSchedulingService.getAllOrders())
    if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
      let orders = response.data.data;
      let ordersList = [];
      orders.forEach(element => {
        ordersList.push(new PlanningHistoryModel(element.apmName, element.fmName, element.plantName, element.buyerName, element.qaName, element.unitName,
          element.merchandiserName, element.lineItemDesc, element.lineItemNo, element.leftPlannedQty, element.leftPoQtyTobeScheduled, element.poOrderedQty,
          element.scheduledQty, element.poQty, Number(element.plantId), element.saidId, element.pcdDate == null ? null : new Date(element.pcdDate),
          element.status, Number(element.unitId), element.buyerPoNo, element.alertCode, element.pcdDate == null ? null : new Date(element.pcdDate), element.plannedQty, element.exFactoryDate == null ? null : new Date(element.exFactoryDate)));
      })
      let uniquePlants = ordersList.filter((value, index, array) => array.findIndex(t => (t.plantName === value.plantName)) === index);
      let uniqueUnits = ordersList.filter((value, index, array) => array.findIndex(t => (t.unitName === value.unitName) && (value.unitName != null) && (value.unitName != "")) === index);
      let uniqueStatus = ordersList.filter((value, index, array) => array.findIndex(t => (t.status === value.status)) === index);
      let uniqueBuyerPos = ordersList.filter((value, index, array) => array.findIndex(t => (t.buyerName === value.buyerName)) === index);
      let uniqueSaids = ordersList.filter((value, index, array) => array.findIndex(t => (t.saidId === value.saidId)) === index);
      let uniqueLineItems = ordersList.filter((value, index, array) => array.findIndex(t => (t.lineItemNo === value.lineItemNo)) === index);
      let uniqueAlertCode = ordersList.filter((value, index, array) => array.findIndex(t => (t.alertCode === value.alertCode)) === index);
      this.setState({
        orders: ordersList, planningHistoryAlert: uniqueAlertCode, planningHistoryStatus: uniqueStatus, planningHistoryLineItem: uniqueLineItems, planningHistoryPlants: uniquePlants,
        planningHistoryUnits: uniqueUnits, planningHistoryBuyerPo: uniqueBuyerPos, planningHistorySaid: uniqueSaids, isShowPlanningHistoryScreen: true,
        orderDetails: { ...this.state.orderDetails, isInvokedFromPlanningHistory: true }
      })
    }
    global.hideLoader();
  }

  async closePlanningHistoryScreen(args, isShowPlanningHistoryScreen) {
    let that = this;
    const { orderDetails } = that.state;
    if ((args.event != null && args.event != undefined) || (args.type != undefined && args.type != null)) {
      if (args.type != undefined && args.type != null) // for status button
        await that.getAllOrders();
      that.setState({
        isShowPlanningHistoryScreen, orderDetails: { ...orderDetails, isInvokedFromPlanningHistory: isShowPlanningHistoryScreen }
      }, () => {
        that.resetOrders(false);
      });
    }
  }

  expandCollapseFilterOption = () => {
    this.setState({
      isShowFilters: !this.state.isShowFilters
    });
  }

  searchOrders = () => {
    let that = this;
    const { selectedPlanningHistoryStartDate, selectedPlanningHistoryAlert, selectedPlanningHistoryEndDate, selectedPlanningHistoryPlants, selectedPlanningHistoryUnits, selectedPlanningHistoryBuyerPo,
      selectedPlanningHistorySaid, selectedPlanningHistoryLineItem, selectedPlanningHistoryStatus } = that.state;
    if (selectedPlanningHistoryStartDate == null && selectedPlanningHistoryEndDate == null && selectedPlanningHistoryAlert.length == 0 && selectedPlanningHistoryPlants.length == 0 && selectedPlanningHistoryUnits.length == 0 && selectedPlanningHistoryBuyerPo.length == 0 && selectedPlanningHistorySaid.length == 0
      && selectedPlanningHistoryLineItem.length == 0 && selectedPlanningHistoryLineItem.length == 0 && selectedPlanningHistoryStatus.length == 0) {
      that.toastObj.show(this.state.toasterMessage[23]);
    } else {
      that.orderGrid.clearFiltering();
      if (selectedPlanningHistoryPlants.length > 0)
        that.orderGrid.filterByColumn("plantName", "equal", selectedPlanningHistoryPlants);
      if (selectedPlanningHistoryUnits.length > 0)
        that.orderGrid.filterByColumn("unitName", "equal", selectedPlanningHistoryUnits);
      if (selectedPlanningHistoryBuyerPo.length > 0)
        that.orderGrid.filterByColumn("buyerName", "equal", selectedPlanningHistoryBuyerPo);
      if (selectedPlanningHistorySaid.length > 0)
        that.orderGrid.filterByColumn("saidId", "equal", selectedPlanningHistorySaid);
      if (selectedPlanningHistoryLineItem.length > 0)
        that.orderGrid.filterByColumn("lineItemNo", "equal", selectedPlanningHistoryLineItem);
      if (selectedPlanningHistoryStatus.length > 0)
        that.orderGrid.filterByColumn("status", "equal", selectedPlanningHistoryStatus);
      if (selectedPlanningHistoryAlert.length > 0)
        that.orderGrid.filterByColumn("alertCode", "equal", selectedPlanningHistoryAlert);
      if (selectedPlanningHistoryStartDate != null)
        that.orderGrid.filterByColumn("pcdDate", "greaterthanorequal", selectedPlanningHistoryStartDate);
      if (selectedPlanningHistoryEndDate != null)
        that.orderGrid.filterByColumn("pcdDateForFilter", "lessthanorequal", selectedPlanningHistoryEndDate);
      that.toastObj.show(this.state.toasterMessage[22]);
    }
  }

  commandClick = (args) => {
    this.getStockDetails(args.rowData.saidId, args.rowData.lineItemNo+"-"+args.rowData.lineItemDesc, args.rowData.buyerPoNo);
  }

  resetOrders = (isInvokedFromOnClick) => {
    let that = this;
    that.setState({
      selectedPlanningHistoryPlants: [], selectedPlanningHistoryUnits: [], selectedPlanningHistoryBuyerPo: [], selectedPlanningHistoryLineItem: [],
      selectedPlanningHistorySaid: [], selectedPlanningHistoryStartDate: null, selectedPlanningHistoryEndDate: null, selectedPlanningHistoryStatus: [],
      selectedPlanningHistoryAlert: []
    });
    that.orderGrid.clearFiltering();
    if (isInvokedFromOnClick)
      that.toastObj.show(this.state.toasterMessage[21]);
  }

  getSelectedRowPlannings = (args) => {
    let that = this;
    if (args.target.localName != "button") {
      that.state.orderDetails.plantId = args.data.plantId;
      that.state.orderDetails.unitId = args.data.unitId;
      that.state.orderDetails.unitName = args.data.unitName;
      that.state.orderDetails.saidId = args.data.saidId;
      that.state.orderDetails.buyerPoNo = args.data.buyerPoNo;
      that.state.orderDetails.lineItemNo = args.data.lineItemNo;
      that.setState({ isShowPlanningHistoryScreen: false }, () => {
        this.onChangeMultiSelectPlantToGetUnit(null);
      })
    }
  }

  onChangeFilterDatePicker = (args) => {
    if (args.event != undefined && args.event != null) {
      this.setState({ [args.element.id]: args.value })
    }
  }

  onChangePlanningHistoryMultiSelect = (event) => {
    // if (event.e != undefined && event.e != null)
    this.setState({ [event.element.id]: event.value })
  }

  customizeCell = (args) => {
    if (args.column.field === "alertCode" && args.data && args.cell) {
      if (args.data.alertCode == "lt 7 days") {
        args.cell.classList.add('below-80');
      } else if (args.data.alertCode == "lt 14 days") {
        args.cell.classList.add('below-30');
      }
    }
  }

  async getStockDetails(saidId, lineItemNo, BuyerPoNo) {
    global.showLoader();
    let response = (await this.LineSchedulingService.getStockDetails(saidId, lineItemNo, BuyerPoNo))
    if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
      let stocks = response.data.data;
      let stockList = [];
      stocks.forEach(element => {
        stockList.push(new StockModel(element.id, element.buyer_po, element.materialBluePCDdate == null ? null : new Date(element.materialBluePCDdate), element.issued_qty, element.required_material_Qty, element.Buyer, element.resv_store_location,
          element.material_category, element.uom, element.materialBluePCDQty, element.said, element.line_item_no, element.material_code, element.material_description, element.reserved_qty));
      })
      this.setState({
        stocks: stockList, isShowStockDialog: true
      })
    } else {
      this.toastObj.show({ title: 'Information!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    }
    global.hideLoader();
  }

  closeStockDialog = () => {
    this.setState({ isShowStockDialog: false, stocks: [] })
  }

  //######################################################################### SUMMARY-VIEW #####################################################################//

  onChangeMultiSelectPlant = (event) => {
    let that = this;
    that.setState({ dynamicSummaryViewPlants: [], dynamicSummaryViewCategory: [], multiSelectedPlants: event.value, isMonthlyView: "month" })
    if ((that.state.isSummaryView) && (that.state.multiSelectedPlants.length > 0) && (that.state.summarySelectedYear != null)) {
      global.showLoader();
      this.getSummeryViewData(this.state.multiSelectedPlants, "month", that.state.summarySelectedYear)
    }
  }

  onChangeSummaryYear = (event) => {
    let that = this;
    const { multiSelectedPlants, isMonthlyView } = this.state;
    if (event.event != undefined) {
      global.showLoader();
      if (multiSelectedPlants != null && event.value != null) {
        that.getSummeryViewData(multiSelectedPlants, isMonthlyView, event.value.getFullYear());
      } else {
        global.hideLoader();
      }
      this.setState({ summarySelectedYear: event.value.getFullYear() })
    }
  }
  switchView = (isCategory) => {
    let that = this;
    that.setState({ isCategoryView: isCategory, dynamicSummaryViewPlants: [], dynamicSummaryViewCategory: [], isMonthlyView: "month" }, () => {
      if ((that.state.isSummaryView) && (that.state.multiSelectedPlants.length > 0) && (that.state.summarySelectedYear != null)) {
        global.showLoader()
        that.getSummeryViewData(that.state.multiSelectedPlants, "month", that.state.summarySelectedYear)
      }
    })
  }

  switchMonthView = (monthType) => {
    let that = this;
    this.setState({ isMonthlyView: monthType, }, () => {
      if ((that.state.isSummaryView) && (that.state.multiSelectedPlants.length > 0) && (that.state.summarySelectedYear != null)) {
        global.showLoader();
        this.getSummeryViewData(this.state.multiSelectedPlants, monthType, that.state.summarySelectedYear)
      }
    })
  }

  async getSummeryViewData(plantList, viewType, year) {
    let response = (await this.LineSchedulingService.getSummeryViewData(plantList, this.state.isCategoryView, viewType, year))
    if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
      if (this.state.isCategoryView) {
        this.setState({ summaryDataCategory: response.data.data }, () => {
          this.createSummaryViewCategory()
        })
      } else {
        this.setState({ summaryDataPlants: response.data.data }, () => {
          this.createSummaryViewPlants()
        })
      }
    }
    else {
      this.toastObj.show(this.state.toasterMessage[2]);
      global.hideLoader()
    }
  }

  createSummaryViewPlants() {
    const { summaryDataPlants, isMonthlyView } = this.state;
    let dynamicSummaryViewPlants = [];
    let modifiedSummaryData = [];
    let gridColumns = isMonthlyView == "month" ? [{ name: 'month' }] : [{ name: 'month' }, { name: 'week' }];
    summaryDataPlants.map(item => {
      modifiedSummaryData = modifiedSummaryData.concat(item);
    })
    dynamicSummaryViewPlants.push(<div key={Math.random()}>
      <PivotViewComponent
        gridSettings={this.gridSettings}
        // ref={d => this.pivotObj = d}
        id={"summeryViewPlants"}
        height={700}
        allowConditionalFormatting={true}
        showTooltip={false}
        dataSourceSettings={{
          enableSorting: false,
          columns: gridColumns,
          values: [
            { name: 'capAMC', caption: 'Cap. Avl. M/C' },
            { name: 'capAVLMD', caption: 'Cap. Avl. M/D' },
            { name: 'bookQty', caption: 'Booked Qty' },
            { name: 'bookCAPMD', caption: 'Booked Cap. M/D' },
            { name: 'bookAVLMD', caption: 'Avl. M/Days' }
          ],
          formatSettings: [{ name: 'capAMC', useGrouping: true }, { name: 'capAVLMD', useGrouping: true }, { name: 'bookQty', useGrouping: true },
          { name: 'bookCAPMD', useGrouping: true }, { name: 'bookAVLMD', useGrouping: true }],
          dataSource: modifiedSummaryData,
          rows: [{ name: 'plant' }, { name: 'units' }, { name: 'line' }],
          expandAll: isMonthlyView == "month" ? false : true,
          showGrandTotals: false,
          filters: [],
          conditionalFormatSettings: this.conditionalFormattingSummaryView()
        }}>
        <Inject services={[ConditionalFormatting]} />
      </PivotViewComponent>
    </div >
    )
    this.setState({ dynamicSummaryViewPlants }, () => {
      global.hideLoader();
    })
  }


  conditionalFormattingSummaryView() {
    let ConditionalFormattingData = [];
    ConditionalFormattingData.push(
      {
        measure: 'bookAVLMD',
        conditions: 'LessThan',
        value1: -1,
        style: {
          backgroundColor: '#ff656d',
          text: 'overloaded',
        }
      },
      {
        measure: 'bookAVLMD',
        conditions: 'GreaterThan',
        value1: -1,
        style: {
          backgroundColor: '#6dc76e',
          text: 'editable',
        }
      }
    )
    return ConditionalFormattingData;
  }



  createSummaryViewCategory() {
    const { summaryDataCategory, isMonthlyView } = this.state;
    let dynamicSummaryViewCategory = [];
    let modifiedSummaryData = [];
    let gridColumns = isMonthlyView == "month" ? [{ name: 'month' }] : [{ name: 'month' }, { name: 'week' }];
    summaryDataCategory.map(item => {
      modifiedSummaryData = modifiedSummaryData.concat(item);
    })
    dynamicSummaryViewCategory.push(<div key={Math.random()}>
      <PivotViewComponent
        gridSettings={this.gridSettings}
        // ref={d => this.pivotObj = d}
        id={"summeryViewCategory"}
        height={700}
        allowConditionalFormatting={true}
        showTooltip={false}
        dataSourceSettings={{
          enableSorting: false,
          columns: gridColumns,
          values: [
            { name: 'capAMC', caption: 'Cap. Avl. M/C' },
            { name: 'capAVLMD', caption: 'Cap. Avl. M/D' },
            { name: 'bookQty', caption: 'Booked Qty' },
            { name: 'bookCAPMD', caption: 'Booked Cap. M/D' },
            { name: 'bookAVLMD', caption: 'Avl. M/Days' }
          ],
          formatSettings: [{ name: 'capAMC', useGrouping: true }, { name: 'capAVLMD', useGrouping: true }, { name: 'bookQty', useGrouping: true },
          { name: 'bookCAPMD', useGrouping: true }, { name: 'bookAVLMD', useGrouping: true }],
          dataSource: modifiedSummaryData,
          rows: [{ name: 'line' }, { name: 'plant' }, { name: 'units' }],
          expandAll: isMonthlyView == "month" ? false : true,
          showGrandTotals: false,
          filters: [],
          conditionalFormatSettings: this.conditionalFormattingSummaryView()
        }}>
        <Inject services={[ConditionalFormatting]} />
      </PivotViewComponent>
    </div >
    )
    this.setState({ dynamicSummaryViewCategory }, () => {
      global.hideLoader();
    })
  }

  //######################################################################### OTHER-EVENTS #####################################################################//


  exportBuyerPoGrid = (args) => {
    let that = this;
    // exportProperties={this.state.excelExportProperties}
    if (that.buyerPoGridObj && args.item.id === 'buyerPoGrid_excelexport') {
        that.buyerPoGridObj.excelExport(that.state.buyerPoExportProperties);
    }
  }

  exportSchedulingStatusGrid = (args) => {
    let that = this;
    // exportProperties={this.state.excelExportProperties}
    if (that.orderGrid && args.item.id === 'orderStatus_excelexport') {
        that.orderGrid.excelExport(that.state.schedulingStatusExportProperties);
    }
  }

  exportDynamicGrid = (args) => {
    let that = this;
    if (that.dynamicGridObj && args.item.id === 'dynamicGrid_excelexport') {
      that.dynamicGridObj.excelExport(that.state.schedulingExportProperties);
    }
  }

  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }

  sideBar = () => {
    let that = this;
    const { selectedSaidToRefreshGrid, selectedLineItemNoToRefreshGrid, isRefreshPivotGrid, selectedYear } = that.state;
    that.setState({
      buyerPoData: [], deletedEventsId: [], selectedLineItemUnitIdToGetUnitList: null, isDisabledScheduledButton: false, isSidebar: !that.state.isSidebar, selectedLineItemColorToSchedule: null, selectedLineItemNameToSchedule: null, isDisabledFields: false,
      model: new PlannedLineModel(null, null, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", "", null, null, null, null, null, new Date(), new Date(), new Date(), new Date(), null, null, null, null, null, null, null, null, null), selectedLineItemIdToEdit: null
    }, () => {
      if ((that.state.selectedTab == "scheduled") && (isRefreshPivotGrid)) {
        global.showLoader()
        that.getLineSchedulingDetailsOnRefresh(that.state.selectedProdLineId, that.state.selectedUnitName, that.state.lineNameToRefresh, new Date().getFullYear())
      }
      if (that.state.selectedTab == "planned") {
        that.setState({ dynamicTableProductionLines: [], plannedLineData: [], plannedLineData: [] })
        that.getPlannedLineItemBySaidIdAndLineItem(selectedSaidToRefreshGrid, selectedLineItemNoToRefreshGrid)
      }
    })
    document.body.classList.toggle('overflow-hidden-body');
  }


  disableDatePickerDate = (args) => {
    if (args.date.getDay() === 0) {
      args.isDisabled = true;
    }
  }

  isValidate(isEditOnUnschedule) {
    const { schedulerEventsDataOnLoad, scheduledModel, selectedTab, selectedLineItemIdToEdit } = this.state;
    let isValidateFields = true;
    if ((scheduledModel.schedulerData.length == 0) && (selectedTab == "planned")) {
      isValidateFields = false;
      this.toastObj.show(this.state.toasterMessage[18]);
    }
    else if (selectedTab == "planned") {
      for (let i = 0; i < schedulerEventsDataOnLoad.length; i++) {
        if (schedulerEventsDataOnLoad[i].plannedData.id == selectedLineItemIdToEdit) {
          if ((schedulerEventsDataOnLoad[i].plannedData.noOfMachineToUse == 0) || (schedulerEventsDataOnLoad[i].plannedData.scheduledQty == 0) || (schedulerEventsDataOnLoad[i].plannedData.prodLineId == null)) {
            isValidateFields = false;
            this.toastObj.show(this.state.toasterMessage[17]);
            break;
          }
          let startDate = new Date((schedulerEventsDataOnLoad[i].plannedData.scheduleStartDateTime).toDateString());
          let endDate = new Date((schedulerEventsDataOnLoad[i].plannedData.scheduleEndDateTime).toDateString());
          let date = new Date();
          let currentDate = new Date(date.getFullYear(), date.getMonth(), date.getDate())
          if ((startDate < currentDate) || (endDate < currentDate)) {
            isValidateFields = false;
            this.toastObj.show(this.state.toasterMessage[19]);
            break;
          }
        }
      }
    }
    else if ((selectedTab != "planned") && (scheduledModel.deletedEventsId.length == 0) && (!isEditOnUnschedule)) {
      isValidateFields = false;
      this.toastObj.show(this.state.toasterMessage[18]);
    }
    return isValidateFields;
  }


  switchSummaryView = () => {
    let that = this;
    that.setState({ isSummaryView: !that.state.isSummaryView, dynamicSummaryViewPlants: [], dynamicSummaryViewCategory: [], isMonthlyView: "month" }, () => {
      if ((that.state.isSummaryView) && (that.state.multiSelectedPlants.length > 0)) {
        global.showLoader()
        that.getSummeryViewData(that.state.multiSelectedPlants, "month")
      }
    })
  }


  onChangeTab = (event) => {
    let that = this;
    that.multiSelectObj.selectAll(false);
    if (event.target.id == "planned") {
      that.setState({
        selectedBuyerPoData: null, selectedLineItemNoToRefreshGrid: null, selectedSaidToRefreshGrid: null, minStartDate: null, isMonthlyView: "month",
        selectedBuyerPo: null, buyerPoLineItemsList: [], multiSelectedPlants: [], dynamicSummaryViewPlants: [], dynamicSummaryViewCategory: [], summaryDataPlants: [], summaryDataCategory: [],
        selectedTab: event.target.id, transferLineItemPLlist: [], selectedPlantName: null, selectedUnitName: null, selectedLineName: null,
        units: [], unitProductionLines: [], transferLineItemUnitlist: [], dynamicTableWorkInProgress: [], dynamicPivotGrids: [], schedulerEventsDataOnLoad: [],
        index: null, selectedEventColor: null, selectedEventName: null, dynamicTableProductionLines: [], selectedPlant: null, selectedUnit: null,
        selectedLine: null, selectedMonth: null, isDisabledFields: false,
        model: new PlannedLineModel(null, null, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", "", null, null, null, null, null, new Date(), new Date(), new Date(), new Date(), null, null, null, null, null, null, null, null), selectedLineItemIdToEdit: null
      })
    }
    if (event.target.id == "scheduled") {
      that.setState({
        selectedBuyerPoData: null, selectedLineItemNoToRefreshGrid: null, selectedSaidToRefreshGrid: null, minStartDate: null, isMonthlyView: "month",
        selectedBuyerPo: null, buyerPoLineItemsList: [], multiSelectedPlants: [], dynamicSummaryViewPlants: [], dynamicSummaryViewCategory: [], summaryDataPlants: [], summaryDataCategory: [],
        selectedTab: event.target.id, transferLineItemPLlist: [], selectedPlantName: null, selectedUnitName: null, selectedLineName: null,
        units: [], unitProductionLines: [], transferLineItemUnitlist: [], dynamicTableWorkInProgress: [], dynamicPivotGrids: [],
        schedulerEventsDataOnLoad: [], index: null, selectedEventColor: null, selectedEventName: null, dynamicTableProductionLines: [],
        selectedPlant: null, selectedUnit: null, selectedLine: null, selectedMonth: null, isDisabledFields: false,
        model: new PlannedLineModel(null, null, null, null, null, null, null, null, null, null, null, null, null, "", "", "", "", "", "", null, null, null, null, null, new Date(), new Date(), new Date(), new Date(), null, null, null, null, null, null, null, null), selectedLineItemIdToEdit: null
      })
    }
  }


  onChangeReportDialoguePlant = (event) => {
    this.setState({
        selectedReportPlantId: event.value,
    }, () => {
        if (event.e != undefined) {
            this.multiSelectObj.selectAll(false);
            this.setState({ units: [] }, () => {
                this.getUnitList(event.value)
            })
        }
    })
}
onChangeReportDialogueUnit = (event) => {
  let that = this;
  that.setState({ selectedReportUnitId: event.value });
}
onChangeReportDialogueStartDate = (event) => {
  let that = this;
  that.setState({ selectedReportStartDate: event.value });
}
onChangeReportDialogueEndDate = (event) => {
  let that = this;
  that.setState({ selectedReportEndDate: event.value });
}
refreshSchedulingReportGrid(){
  this.setState({showDialog:false,selectedReportPlantId:null,selectedReportUnitId:null,selectedReportStartDate:null,selectedReportEndDate:null}) 
}

generateSchedulingStatusReport = async (args) => {
  const {selectedReportPlantId, selectedReportUnitId, selectedReportStartDate, selectedReportEndDate} = this.state;

  if(selectedReportPlantId != null && selectedReportUnitId != null && selectedReportStartDate !=null && selectedReportEndDate !=null){
       this.genSchedulingReport(selectedReportPlantId, selectedReportUnitId, selectedReportStartDate, selectedReportEndDate)
       this.refreshSchedulingReportGrid()   
   }
}

async genSchedulingReport(plantId, unitId, startDate, endDate) {
  global.showLoader();
  let startMonth = startDate.getMonth()+1;
  let startDay = startDate.getDate();
  if(startMonth < 10){
      startMonth = '0' + startMonth;
  }
  if(startDay < 10){
      startDay = '0' + startDay;
  }
  let selectStartDate = startDay+ '-' + startMonth + '-' + startDate.getFullYear() ;

  let endMonth = endDate.getMonth()+1;
  let endDay = endDate.getDate();
  if(endMonth < 10){
      endMonth = '0' + endMonth;
  }
  if(endDay < 10){
      endDay = '0' + endDay;
  }
  let selectEndDate = endDay+ '-' + endMonth + '-' +  endDate.getFullYear() ;

  let response1 = (await this.LineSchedulingService.generatePlanningStatusReport(plantId, unitId, selectStartDate, selectEndDate))
  if ((response1 != undefined) && (response1.data.status == "success")) {
      this.downloadSchedulingStatusReport(response1.data.message)
  //     let response = (await this.lineLoadingService.downloadReport(response1.data.value))
  //     if ((response != undefined) && (response.data.status == "success")) {
  //         this.toastObj.show({ title: 'Success!', content: response1.data.message, cssClass: 'e-toast-success', icon: 'e-error toast-icons' });
  //         this.setState({selectedReportStartDate: null, selectedReportPlantId: null, selectedReportUnitId: null, selectedReportEndDate: null})
  //     }
  //   global.hideLoader();
  }
  else {
    this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
    global.hideLoader();
  }
}

async downloadSchedulingStatusReport(reportId) {
  global.showLoader();
  let response = (await this.LineSchedulingService.downloadReport(reportId))
  if ((response != undefined) && (response.status == 200)) {
      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'Scheduling Status.xlsx'); //or any other extension
      document.body.appendChild(link);
      link.click();
      global.hideLoader();
      this.toastObj.show(this.state.toasterMessage[24]);

  }
  else {
      global.hideLoader();
      this.toastObj.show(this.state.toasterMessage[2]);
  }
}

  //####################################################################### RENDER ######################################################################//

  render() {
    const { t, i18n } = this.props;
    return (
      <div className="container-fluid-inner">
        <h1 className='pagesheading'>Scheduling</h1>
        <div className='clearfix'></div>
        <Row>
          <Col xs='6' md='6'>
            <Button size="sm" id="planned" color={this.state.selectedTab == "planned" ? "primary" : "secondary"} onClick={this.onChangeTab}>Planned</Button>
            <Button size="sm" className="ml-2 mr-2" color={this.state.selectedTab == "scheduled" ? "primary" : "secondary"} id="scheduled" onClick={this.onChangeTab}>Scheduled</Button>
          </Col>
          <Col xs='6' md='6'>
              <div>
                <a color="primary" className="ml-2 float-right primaryText" onClick={this.onClickSchedule} id="Report"><i className='fa fa-download'></i> Schedule Status</a>
              </div>
          </Col>
        </Row>
        <div className="mt-3"></div>
        <section hidden={this.state.selectedTab == "planned" ? false : true}>
          <div>
            <Button color="primary" size="sm" className="mb-2" onClick={(e) => { this.closePlanningHistoryScreen(e, true) }}>Order Status</Button>
          </div>

                    {/* //---------------------------------------------------------------------------------Planning Report Dialog Start---------------------------------------------------------------// */}
                      <DialogComponent visible={this.state.showDialog} id="defaultDialog" showCloseIcon={true} animationSettings={this.animationSettings} isModal={true} allowDragging={true} width={'70%'} ref={dialog => this.dialogInstance = dialog}
                      header= 'Schedule Status' close={this.dialogClose.bind(this)}>
                          
                          <Row>
                              <Col><DropDownListComponent className='datePicker' id='selectPlantName' placeholder="Select Plant Name" value={this.state.selectedReportPlantId} floatLabelType="Auto" fields={{ text: 'name', value: 'id' }} change={this.onChangeReportDialoguePlant} dataSource={this.state.plants} popupHeight="220px"></DropDownListComponent></Col>
                              <Col><DropDownListComponent className='datePicker' id='selectUnitName' placeholder="Select Unit Name" value={this.state.selectedReportUnitId} floatLabelType="Auto" dataSource={this.state.units} fields={{ text: 'name', value: 'id' }}  change={this.onChangeReportDialogueUnit}  popupHeight="220px"></DropDownListComponent></Col>
                              <Col><DatePickerComponent className='datePicker' value={this.state.selectedReportStartDate}  placeholder=" Start Date" floatLabelType="Auto"  change={this.onChangeReportDialogueStartDate} ></DatePickerComponent></Col>
                              <Col><DatePickerComponent className='datePicker' value={this.state.selectedReportEndDate}  placeholder=" End Date" floatLabelType="Auto"  change={this.onChangeReportDialogueEndDate} ></DatePickerComponent></Col>
                          </Row>
                          <div className='mt-2'></div>
                          <Row className='float-right'><Button color="primary" size="sm" id="generateSchedulingStatusReport" onClick={this.generateSchedulingStatusReport}><i className='fa fa-download'></i> Generate Report</Button></Row>

                      </DialogComponent>
                    {/* //---------------------------------------------------------------------------------Planning Report Dialog End---------------------------------------------------------------// */}


          <div className="upcoming-buyerpo" >
            <Row>
              <Col sm='2'>
                <div className="heading-item-list mt-3">Upcoming Buyer/PO</div>
              </Col>
              <Col sm='3'>
                <DropDownListComponent color="primary" id="plants" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedPlantForUpcomingBuyerPo} change={this.onChangeMultiSelectPlantToGetUnit} dataSource={this.state.plants} placeholder={t("cpm.lm.line.plant.name")} floatLabelType="Auto" />
              </Col>
              <Col sm='3'>
                <DropDownListComponent color="primary" id="selectedUnitNameForPlannedTab" allowFiltering={true} dataSource={this.state.unitsForPlannedTab} fields={{ text: 'name', value: 'name' }} value={this.state.selectedUnitNameForPlannedTab} change={this.onChangePlannedTabUnit} placeholder={t("cpm.lm.line.unit.name")} floatLabelType="Auto" />
              </Col>
            </Row>
            <div className='mt-3'></div>

            <GridComponent allowExcelExport={true} toolbarClick={this.exportBuyerPoGrid} id='buyerPoGrid' toolbar={['ExcelExport', 'Search']} allowFiltering={true} allowSorting={true} filterSettings={this.state.FilterSettingsModel}  ref={grid => this.buyerPoGridObj = grid} rowSelected={this.onSelectBuyerPoGrid}
              dataSource={this.state.buyerPoList} allowResizing={true} allowTextWrap={true} gridLines='Both' height='150' >
              <ColumnsDirective>
                <ColumnDirective field='buyerPoNr' allowSorting={true} filter={this.state.IFilter} headerText=' Buyer Po' width='150' />
                <ColumnDirective field='exFactoryDate' allowSorting={true} filter={this.state.IFilter} format={this.state.format} headerText='Ex Fact Dt' width='150' />
                <ColumnDirective field='saidNr' allowSorting={true} filter={this.state.IFilter} headerText='Said' width='120' />
              </ColumnsDirective>
              <Inject services={[Filter, Selection, Sort, Toolbar, ExcelExport]} />
            </GridComponent>
          </div>
          <div className="mt-3"></div>
          <div className="d-flex flex-row">
            <div className="pr-2 mr-3 pt-2 "><b>Buyer Po: </b>{this.state.selectedBuyerPo}</div>
            <div className="required-field">
              <DropDownListComponent color="primary" id="selectLineItem" allowFiltering={true} fields={{ text: 'lineItemNr', value: 'lineItemNr' }} change={this.onChangeLineItem} value={this.state.selectedLineItemNo} dataSource={this.state.buyerPoLineItemsList} placeholder={t("Select Line item")} floatLabelType="Never" />
            </div>
          </div>
        </section>


        {/* ------------------Schedule Tab view ------------------ */}
        <section hidden={this.state.selectedTab == "scheduled" ? false : true}>
          <div hidden={this.state.isSummaryView}>
            <Button color="primary" size="sm" className="mb-2" onClick={this.switchSummaryView}>Switch Summary</Button>
            <Row>
              <Col xs='12' md='3'>
                <div className="required-field">
                  <DropDownListComponent color="primary" id="selectedPlantName" allowFiltering={true} dataSource={this.state.plants} fields={{ text: 'name', value: 'id' }} value={this.state.selectedPlantName} change={this.onChangePlant} placeholder={t("cpm.lm.line.plant.name")} floatLabelType="Auto" />
                </div>
              </Col>
              <Col xs='12' md='2'>
                <div className="required-field">
                  <DropDownListComponent color="primary" id="selectedUnitName" allowFiltering={true} dataSource={this.state.units} fields={{ text: 'name', value: 'id' }} value={this.state.selectedUnitName} change={this.onChangeUnit} placeholder={t("cpm.lm.line.unit.name")} floatLabelType="Auto" />
                </div>
              </Col>
              <Col xs='12' md='4' lg='2'>
                <DropDownListComponent color="primary" id="lineName" allowFiltering={true} dataSource={this.state.unitProductionLines} fields={{ text: 'lineName', value: 'id' }} value={this.state.selectedLineName} change={this.onChangeProductionLine} placeholder={t("cpm.lm.line.prod.line")} floatLabelType="Auto" />
              </Col>
              <Col xs='12' md='4' lg='1'>
                <div className="required-field">
                  <DatePickerComponent size='sm' min={this.minDate} max={this.maxDate} value={this.state.selectedYear} start="Decade" depth="Decade" format="yyyy" floatLabelType="Auto" placeholder="Year" change={this.onChangeYear} />
                </div>
              </Col>
              <Col xs='12' md='4'>
                <ul className="legend-group mt-3">
                  <li><span className="editable"></span>{t("cpm.lm.line.plan.map.legend.Editable")}</li>
                  <li><span className="overloaded"></span>{t("cpm.lm.line.plan.map.legend.Overloaded")}</li>
                  <li><span className="freeze"></span>{t("cpm.lm.line.plan.map.legend.Freeze")}</li>
                </ul>
              </Col>
            </Row>
            <div className='control-section'>
              {this.state.dynamicPivotGrids}
            </div>
          </div>

          {/* ------------------Schedule Tab switch view ------------------ */}
          <div hidden={!this.state.isSummaryView}>
            <Button color="primary" size="sm" onClick={this.switchSummaryView}>Switch Scheduling</Button>
            <Button color="primary" size="sm" className="ml-2 mr-2" id="plantView" color={this.state.isCategoryView == false ? "primary" : "secondary"} onClick={() => { this.switchView(false) }}>Plant View</Button>
            <Button color="primary" size="sm" id="categoryView" color={this.state.isCategoryView == true ? "primary" : "secondary"} onClick={() => { this.switchView(true) }}>Category View</Button>
            <div className="mt-3"></div>
            <Row>
              <Col xs='12' md='3'>
                <div className="required-field">
                  <MultiSelectComponent id="plantIdCheckbox" dataSource={this.state.plants}
                    change={this.onChangeMultiSelectPlant} ref={t => this.multiSelectObj = t} fields={{ text: 'name', value: 'id' }} placeholder={t("cpm.lm.line.plant.name")} mode="CheckBox" showSelectAll={true}
                    showDropDownIcon={true}
                    filterBarPlaceholder="Search plants" popupHeight="350px">
                    <Inject services={[CheckBoxSelection]} />
                  </MultiSelectComponent>
                </div>
              </Col>
              <Col xs='12' md='4' lg="2">
                <div className="required-field">
                  <DatePickerComponent size='sm' min={this.minDate} max={this.maxDate} value={this.state.summarySelectedYear} start="Decade" depth="Decade" format="yyyy" placeholder="Year" change={this.onChangeSummaryYear} />                            </div>
              </Col>
              <Col xs='12' md='3'>
                <Button color="primary" size="sm" className="ml-2" color={this.state.isMonthlyView == "month" ? "primary" : "secondary"} onClick={(e) => this.switchMonthView("month")}>Monthly View</Button>
                <Button color="primary" size="sm" className="ml-2" color={this.state.isMonthlyView == "week" ? "primary" : "secondary"} onClick={(e) => this.switchMonthView("week")}>Weekly View</Button>
              </Col>

            </Row>
            {(this.state.isSummaryView && this.state.dynamicSummaryViewPlants.length > 0) ? this.state.dynamicSummaryViewPlants : null}
            {(this.state.isCategoryView && this.state.dynamicSummaryViewCategory.length > 0) ? this.state.dynamicSummaryViewCategory : null}
          </div>
        </section>




        <div className="clearfix"></div>

        <section hidden={this.state.selectedTab == "planned" ? false : true}>
          <div className="mt-3"></div>
          {/* {this.state.dynamicTableProductionLines}
        {this.state.dynamicTableWorkInProgress} */}
          <GridComponent key={"dynamicGrid"} allowExcelExport={true} dataSource={this.state.plannedLineData} toolbarClick={this.exportDynamicGrid} id='dynamicGrid' toolbar={['ExcelExport', 'Search']}
            ref={grid => this.dynamicGridObj = grid} allowResizing={true} rowSelected={this.rowSelected} allowTextWrap={true} gridLines='Both' allowGrouping={true}>
            <ColumnsDirective >
              <ColumnDirective field='lineName' headerText={"Line Name"} width='100' />
              <ColumnDirective field='saidId' headerText={"Said"} width='100' />
              <ColumnDirective field='startDateTime' format={this.state.format} headerText={"StartDate"} width='100' />
              <ColumnDirective field='endDateTime' format={this.state.format} headerText={"EndDate"} width='100' />
              <ColumnDirective field='lineItemNo' headerText={"Line Item No"} width='100' />
              <ColumnDirective field='totalQtyToProduced' headerText={"Total Qty.(Said)"} width='100' />
              <ColumnDirective field='totalQtyAllocatedInLine' headerText={"Planned Qty.(Said)"} width='100' />
              <ColumnDirective field='unScheduledQty' headerText={"UnScheduled Qty.(Said)"} width='100' />
              <ColumnDirective field='buyerName' headerText={"Buyer Po"} width='100' />
              <ColumnDirective field='apm' headerText={"Apm"} width='100' />
              <ColumnDirective field='merchandiserName' headerText={"Merchandiser Name"} width='100' />
              <ColumnDirective field='productCategoryName' headerText={"Category"} width='100' />
              {/* <ColumnDirective field='status' headerText={"Status"} width='100' /> */}
            </ColumnsDirective>
            <Inject services={[Selection, ForeignKey, Group, Resize, Toolbar, ExcelExport]} />
          </GridComponent>
          <div id="container"></div>
        </section>


        {/* //---------------------------------------------SIDEBAR-----------------------------------------------------// */}

        <div>
          <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={{ X: 'Left', Y: 'Bottom' }}   ></ToastComponent>
          <div className="add-line-section" hidden={!this.state.isSidebar}>
            <SplitterLayout secondaryInitialSize={800} secondaryMinSize={800} >
              <div className="blank-space" onClick={this.sideBar}></div>
              <div className="overflow-hidden add-line-section-form">
                <div className="heading mb-2">{t("cpm.lm.line.schedule.header")}<a className="close" onClick={this.sideBar}>+</a></div>
                <Col xs='12'>
                  <Row>
                    <Col md="12">
                      <div className="line-name-message"><span className="selectedEvent-mark" style={{ backgroundColor: this.state.selectedLineItemColorToSchedule }}></span> {this.state.selected == "planned" ? t("cpm.lm.line.schedule.header.main.lineItem") : t("cpm.lm.line.schedule.header.main.scheduled")} : <b>{this.state.selectedLineItemNameToSchedule}</b></div>
                    </Col>
                  </Row>

                  {/* //------------------------------------------------------------- PLANNING TAB ----------------------------------------------------------------// */}

                  {(this.state.selectedTab == "planned") ? <div>
                    <Row>
                      <Col xs='12' md='4'>
                        <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.saidNo")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.saidId} id="saidNo" floatLabelType="Auto" />
                      </Col>
                      <Col xs='12' md='4'>
                        <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.lineItemNo")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.lineItemNo} id="lineItemNo" floatLabelType="Auto" />
                      </Col>
                      <Col xs='12' md='4'>
                        <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.buyerName")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.buyerName} id="buyerName" floatLabelType="Auto" />
                      </Col>
                    </Row>
                    <Row >
                      <Col xs='12' md='4'>
                        <TextBoxComponent disabled={true} color="primary" id="merchandiserName" value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.merchandiserName} placeholder={t("cpm.lm.line.schedule.sidebar.field.merchantName")} floatLabelType="Auto" />
                      </Col>
                      <Col xs='12' md='4'>
                        <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.totalQty")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.totalQtyToProduced} id="totalQtyToProduced" floatLabelType="Auto" />
                      </Col>

                      <Col xs='12' md='4'>
                        <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.buyerQty")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.buyerPoQty} id="buyerQtyToProduced" floatLabelType="Auto" />
                      </Col>
                      <Col xs='12' md='4'>
                        <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.buyerQtyRemaining")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.leftPoQty} id="buyerQtyToProduced" floatLabelType="Auto" />
                      </Col>
                      <Col xs='12' md='4'>
                        <TextBoxComponent id="totalQtyAllocatedInLine" disabled={true} placeholder={t("cpm.lm.line.schedule.sidebar.field.allocatedQty.")} floatLabelType="Auto" value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.totalQtyAllocatedInLine}  >
                        </TextBoxComponent>
                      </Col>
                      <Col xs='12' md='4'>
                        <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.plannedQtyRemaining")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.unScheduledQty} id="buyerQtyToProduced" floatLabelType="Auto" />
                      </Col>
                    </Row>
                    <Row>
                      <Col xs='12' md='4'>
                        {/* <TextBoxComponent  disabled= {true} color="primary" id="productCategoryId" placeholder={t("cpm.lm.line.schedule.sidebar.field.category")} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.productCategoryName}  floatLabelType="Auto"/> */}
                        <DropDownListComponent color="primary" id="productCategoryId" enabled={false} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.productCategoryId} dataSource={this.state.productionCategory} change={this.onChangeSidebarDropdown} placeholder={t("cpm.lm.line.schedule.sidebar.field.category")} fields={{ text: 'name', value: 'id' }} floatLabelType="Auto" />
                      </Col>
                      <Col xs='12' md='4'>
                        <DropDownListComponent color="primary" id="apm" fields={{ text: 'apmName' }} enabled={false} allowFiltering={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.apm} dataSource={this.state.apm} change={this.onChangeSidebarDropdown} placeholder={t("cpm.lm.line.schedule.sidebar.field.apm")} floatLabelType="Auto" />
                      </Col>
                      <Col xs='12' md='2'>
                        <NumericTextBoxComponent placeholder={t("cpm.lm.line.schedule.field.noOfMachineToUse")} id="noOfMachineToUse" min={0} format='###.###' value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.noOfMachineToUse} onChange={this.onChangeNumText} floatLabelType="Auto" />
                      </Col>

                      <Col xs='12' md='2'>
                        <NumericTextBoxComponent disabled={true} placeholder={t("cpm.lm.line.schedule.field.totalNoOfMachineToUse")} id="totalNoOfMachineToUse" min={0} format='###.###' value={this.state.totalNoOfMachines} floatLabelType="Auto" />
                      </Col>
                    </Row>
                    <Row>
                      <Col xs='12' md='4'>
                        <NumericTextBoxComponent max={this.state.index == null ? null : this.state.scheduledQty} placeholder={t("cpm.lm.line.schedule.sidebar.field.scheduleQty")} id="scheduledQty" min={0} format='###.###' value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.scheduledQty} onChange={this.onChangeNumText} floatLabelType="Auto" />
                      </Col>

                      <Col xs='12' md='4'>
                        <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.buyerPo")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.buyerPo} id="buyerPo" floatLabelType="Auto" />
                      </Col>

                      <Col xs='12' md='4'>
                        <DateTimePickerComponent enabled={false} allowEdit={false} floatLabelType="Auto" value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.shipToDate} format='dd/MM/yy hh:mm a' id="shipToDate" placeholder={t("cpm.lm.line.schedule.sidebar.field.shipToDate")}></DateTimePickerComponent>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs='12' md='4'>
                        <DropDownListComponent color="primary" id="transferTo" allowFiltering={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.transferTo} change={this.onChangeSidebarUnit} fields={{ text: 'name', value: 'id' }} dataSource={this.state.transferLineItemUnitlist} placeholder={t("cpm.lm.line.schedule.sidebar.field.transferTo")} floatLabelType="Auto" />
                      </Col>
                      <Col xs='12' md='4'>
                        <DropDownListComponent color="primary" id="prodLineId" allowFiltering={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.prodLineId} change={this.onChangeSidebarProdLineIdDropdown} fields={{ text: 'lineName', value: 'id' }} dataSource={this.state.transferLineItemPLlist} placeholder={t("cpm.lm.line.prod.line")} floatLabelType="Auto" />
                      </Col>

                      <Col xs='12' md='4'>
                        <span>Estimated working days with </span><b>{this.state.index == null ? 0 : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.noOfMachineToUse}</b><span> Machines is : </span><b>{this.state.index == null ? 0 : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.estimatedWorkingDays}</b><span> Days.</span>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs='12' md='4'>
                        <div className="required-field">
                          <DatePickerComponent weekNumber={true} floatLabelType="Auto" renderDayCell={this.disableDatePickerDate} allowEdit={false} change={(e) => { this.onChangeSideBarDatePicker(e, this) }} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.scheduleStartDateTime} format='dd/MM/yy hh:mm a' id="scheduleStartDateTime" placeholder={t("cpm.lm.line.schedule.field.scheduleStartDateTime")}></DatePickerComponent >
                        </div>
                      </Col>
                      <Col xs='12' md='4'>
                        <div className="required-field">
                          <DatePickerComponent weekNumber={true} floatLabelType="Auto" renderDayCell={this.disableDatePickerDate} min={this.state.minStartDate} allowEdit={false} change={(e) => this.onChangeSideBarDatePicker(e, this)} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.scheduleEndDateTime} format='dd/MM/yy hh:mm a' id="scheduleEndDateTime" placeholder={t("cpm.lm.line.schedule.field.scheduleEndDateTime")}></DatePickerComponent >
                        </div>
                      </Col>
                      <Col xs='12' md='4'>
                        <TextBoxComponent id="sam" placeholder={t("cpm.lm.line.schedule.field.sam")} disabled={true} min={0} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.sam} floatLabelType="Auto" />
                      </Col>

                      <Col xs='12' md='4'>
                        <Button color="primary" size="sm" id="save" className="float-left mr-3" disabled={this.state.isDisabledScheduledButton} onClick={() => { this.onSchedule(false) }}>{t("cpm.lm.line.schedule.sidebar.field.schedule")}</Button>
                      </Col>
                    </Row>

                  </div> : null}

                  {/* //------------------------------------------------------------- SCHEDULED TAB ----------------------------------------------------------------// */}


                  {((this.state.selectedTab == "scheduled")) ?
                    <div>
                      <Row>
                        <Col xs='12' md='4'>
                          <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.saidNo")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.saidId} id="saidNo" floatLabelType="Auto" />
                        </Col>
                        <Col xs='12' md='4'>
                          <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.lineItemNo")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.lineItemNo} id="lineItemNo" floatLabelType="Auto" />
                        </Col>
                        <Col xs='12' md='4'>
                          <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.buyerName")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.buyerName} id="buyerName" floatLabelType="Auto" />
                        </Col>

                      </Row>
                      <Row >
                        <Col xs='12' md='4'>
                          <TextBoxComponent disabled={true} color="primary" id="merchandiserName" value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.merchandiserName} placeholder={t("cpm.lm.line.schedule.sidebar.field.merchantName")} floatLabelType="Auto" />
                        </Col>

                        <Col xs='12' md='4'>
                          <TextBoxComponent placeholder={t("cpm.lm.line.schedule.sidebar.field.totalQty")} disabled={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.totalQtyToProduced} id="totalQtyToProduced" floatLabelType="Auto" />
                        </Col>

                        <Col xs='12' md='4'>
                          <TextBoxComponent id="totalQtyAllocatedInLine" disabled={true} placeholder={t("cpm.lm.line.schedule.sidebar.field.allocatedQty.")} floatLabelType="Auto" value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.totalQtyAllocatedInLine}  >
                          </TextBoxComponent>
                        </Col>

                      </Row>

                      <Row>
                        <Col xs='12' md='4'>
                          <DropDownListComponent color="primary" id="productCategoryId" enabled={false} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.productCategoryId} dataSource={this.state.productionCategory} change={this.onChangeSidebarDropdown} placeholder={t("cpm.lm.line.schedule.sidebar.field.category")} fields={{ text: 'name', value: 'id' }} floatLabelType="Auto" />
                        </Col>
                        <Col xs='12' md='4'>
                          <DropDownListComponent color="primary" id="apm" fields={{ text: 'apmName' }} enabled={false} allowFiltering={true} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.apm} dataSource={this.state.apm} change={this.onChangeSidebarDropdown} placeholder={t("cpm.lm.line.schedule.sidebar.field.apm")} floatLabelType="Auto" />
                        </Col>
                        <Col xs='12' md='4'>
                          <NumericTextBoxComponent placeholder={t("cpm.lm.line.schedule.field.noOfMachineToUse")} disabled={true} id="noOfMachineToUse" min={0} format='###.###' value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.noOfMachineToUse} onChange={this.onChangeNumText} floatLabelType="Auto" />
                        </Col>
                      </Row>


                      <Row>
                        <Col xs='12' md='4' >
                          <TextBoxComponent id="buyerPo" placeholder={t("cpm.lm.line.schedule.sidebar.field.buyerPo")} disabled={true} floatLabelType="Auto" value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.buyerPo}  >
                          </TextBoxComponent>
                        </Col>

                        <Col xs='12' md='4'>
                          {/* <TextBoxComponent id="scheduledQty" placeholder={t("cpm.lm.line.schedule.sidebar.field.scheduleQty")} disabled={true} floatLabelType="Auto" value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.scheduledQty}  >
                          </TextBoxComponent> */}
                          <NumericTextBoxComponent id="scheduledQty" min={0} max={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.totalQtyAllocatedInLine} placeholder={t("cpm.lm.line.schedule.sidebar.field.scheduleQty")} disabled={false} floatLabelType="Auto" min={0} format='###.###' onChange={this.onChangeNumTextScheduleQty} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.scheduledQty}  >
                          </NumericTextBoxComponent>
                        </Col>
                        <Col xs='12' md='4'>
                          <div className="required-field">
                            <DatePickerComponent weekNumber={true} enabled={false} floatLabelType="Auto" allowEdit={false} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.scheduleStartDateTime} format='dd/MM/yy hh:mm a' id="scheduleStartDateTime" placeholder={t("cpm.lm.line.schedule.field.scheduleStartDateTime")}></DatePickerComponent >
                          </div>
                        </Col>
                        <Col xs='12' md='4'>
                          <div className="required-field">
                            <DatePickerComponent weekNumber={true} enabled={false} floatLabelType="Auto" allowEdit={false} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.scheduleEndDateTime} format='dd/MM/yy hh:mm a' id="scheduleEndDateTime" placeholder={t("cpm.lm.line.schedule.field.scheduleEndDateTime")}></DatePickerComponent >
                          </div>
                        </Col>
                        <Col xs='12' md='4'>
                          <TextBoxComponent id="sam" placeholder={t("cpm.lm.line.schedule.field.sam")} disabled={true} min={0} value={this.state.index == null ? null : this.state.schedulerEventsDataOnLoad[this.state.index].plannedData.sam} floatLabelType="Auto" />
                        </Col>
                        <Col xs='12' md='4'>
                          <Button color="primary" size="sm" id="unSchedule" className="float-left mr-3" disabled={this.state.selectedLineItemIdToEdit == null} onClick={() => { this.onSchedule(true) }}>{t("cpm.lm.line.schedule.sidebar.field.edit")}</Button>
                        </Col>
                        <Col xs='12' md='4'>
                          <Button color="primary" size="sm" id="unSchedule" className="float-left mr-3" disabled={this.state.selectedLineItemIdToEdit == null} onClick={() => { this.onSchedule(false) }}>{t("cpm.lm.line.schedule.sidebar.field.unSchedule")}</Button>
                        </Col>
                      </Row>
                    </div>
                    : null}

                  <div>
                    <div className="mt-3"></div>
                    <div className="line-name-message">
                      <span className="selectedEvent-mark" style={{ backgroundColor: this.state.selectedEventColor }}></span> {t("cpm.lm.line.plan.schedulerHeader.eventName")} : <b>{this.state.selectedEventName}</b></div>
                    <Row>
                      <Col xs="12">
                        <div className='control-wrapper'>
                          <ScheduleComponent
                            ref={t => this.scheduleObj = t}
                            workDays={this.state.workingDays}  // to be fixed refresh issue.
                            showWeekend={false}  // to be fixed refresh issue.
                            // actionBegin={this.schedulerCRUD}
                            readonly={true}
                            // editorTemplate={this.schedulerEditorTemplate}
                            selectedDate={this.state.currrentDate}
                            currentView={this.state.currentView}
                            // dragStart={(this.onDragStart)}
                            renderCell={this.state.selectedTab == "planned" ? null : this.onRenderCell}
                            allowDragAndDrop={true}
                            showTimeIndicator={true}
                            popupOpen={this.state.selectedTab == "planned" ? this.onClickPlannedEventPopup : this.onClickScheduledEventPopup}
                            eventRendered={this.setColorToEventOnUI}
                            allowResizing={true}
                            showQuickInfo={true}
                            eventSettings={{
                              dataSource: this.state.schedulerEventsDataOnLoad,
                              fields: {
                                id: 'id',
                                subject: { name: 'description' },
                                location: { name: 'lineItem' },
                                description: { name: 'description', title: 'Description', validation: { required: true } },
                                startTime: { name: 'startDateTime', title: 'Start Duration', validation: { required: true } },
                                endTime: { name: 'endDateTime', title: 'End Duration', validation: { required: true } },
                              }
                            }}
                          >
                            <ViewsDirective>
                              <ViewDirective option='Month' />
                            </ViewsDirective>
                            <Inject services={[Month]} />
                          </ScheduleComponent>
                        </div>
                      </Col>
                    </Row>
                  </div>
                  <hr />
                  <Row className="mt-3">
                    <Col xs="12">
                      <Button color="primary" size="sm" id="cancel" disabled={this.state.isDisabledScheduledButton} onClick={this.sideBar}>{t("cpm.lm.line.close")}</Button>
                    </Col>
                  </Row>
                </Col>

              </div>
            </SplitterLayout>
          </div>
        </div>
        <div className="add-line-section-overlay" hidden={!this.state.isSidebar}></div>
        {/* ----------------------- Add line Section end -----------------------  */}
        <div className="mt-3"></div>

        {/*---------------- Filter Dialog ---------------- */}
        <DialogComponent id="planningHistoryScreen" isModal={true} animationSettings={this.state.animationSettings} showCloseIcon={true} beforeClose={(e) => { this.closePlanningHistoryScreen(e, false) }} header='Order Status' visible={this.state.isShowPlanningHistoryScreen} width='95%' >

          <a className={this.state.isShowFilters ? 'filter-icon-open' : 'filter-icon-close'} onClick={this.expandCollapseFilterOption}>Filter <i className='fa  fa-filter'></i></a>
          <div className='mt-2 filter-dropdown' hidden={!this.state.isShowFilters}>
            <div className='filter-Selection'>
              <h3>Location</h3>

              <Row>
                <Col sm='3'>
                  <MultiSelectComponent id="selectedPlanningHistoryPlants" ref={(scope) => { this.planningHistoryPlantObj = scope; }} dataSource={this.state.planningHistoryPlants}
                    value={this.state.selectedPlanningHistoryPlants} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'plantName', value: 'plantName' }}
                    placeholder={"Select Plants"} mode="CheckBox" showSelectAll={true}
                    showDropDownIcon={true} filterBarPlaceholder="Search plants" popupHeight="300px">
                    <Inject services={[CheckBoxSelection]} />
                  </MultiSelectComponent></Col>
                <Col sm='3'>   <MultiSelectComponent id="selectedPlanningHistoryUnits" ref={(scope) => { this.planningHistoryUnitObj = scope; }} dataSource={this.state.planningHistoryUnits}
                  value={this.state.selectedPlanningHistoryUnits} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'unitName', value: 'unitName' }} placeholder={"Select Units"}
                  mode="CheckBox" showSelectAll={true}
                  showDropDownIcon={true} filterBarPlaceholder="Search Units" popupHeight="300px">
                  <Inject services={[CheckBoxSelection]} />
                </MultiSelectComponent></Col>
              </Row>
            </div>

            <div className='filter-Selection'>
              <h3>Order</h3>
              <Row>
                <Col sm='3'>
                  <MultiSelectComponent id="selectedPlanningHistoryBuyerPo" ref={(scope) => { this.planningHistoryBuyerPoObj = scope; }} dataSource={this.state.planningHistoryBuyerPo}
                    value={this.state.selectedPlanningHistoryBuyerPo} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'buyerName', value: 'buyerName' }}
                    placeholder={"Select Buyer Po"} mode="CheckBox" showSelectAll={true} showDropDownIcon={true} filterBarPlaceholder="Search Buyer Po" popupHeight="300px">
                    <Inject services={[CheckBoxSelection]} />
                  </MultiSelectComponent></Col>
                <Col sm='3'>   <MultiSelectComponent id="selectedPlanningHistorySaid" ref={(scope) => { this.planningHistorySaidObj = scope; }} dataSource={this.state.planningHistorySaid}
                  value={this.state.selectedPlanningHistorySaid} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'saidId', value: 'saidId' }} placeholder={"Select Said"} mode="CheckBox" showSelectAll={true}
                  showDropDownIcon={true} filterBarPlaceholder="Search Said" popupHeight="300px">
                  <Inject services={[CheckBoxSelection]} />
                </MultiSelectComponent></Col>
                <Col sm='3'>   <MultiSelectComponent id="selectedPlanningHistoryLineItem" ref={(scope) => { this.planningHistoryLineItemObj = scope; }} dataSource={this.state.planningHistoryLineItem}
                  value={this.state.selectedPlanningHistoryLineItem} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'lineItemNo', value: 'lineItemNo' }} placeholder={"Select LineItems"} mode="CheckBox" showSelectAll={true}
                  showDropDownIcon={true} filterBarPlaceholder="Search LineItems" popupHeight="300px">
                  <Inject services={[CheckBoxSelection]} />
                </MultiSelectComponent></Col>
              </Row>
            </div>
            <div className='filter-Selection'>
              <h3>Status</h3>
              <Row>
                <Col sm='3'>
                  <DatePickerComponent id="selectedPlanningHistoryStartDate" popupHeight="300px" placeholder='Start Date' format='dd/MM/yy hh:mm a' allowEdit={false} change={this.onChangeFilterDatePicker} value={this.state.selectedPlanningHistoryStartDate} /></Col>
                <Col sm='3'>
                  <DatePickerComponent id="selectedPlanningHistoryEndDate" popupHeight="300px" placeholder='End Date' format='dd/MM/yy hh:mm a' allowEdit={false} change={this.onChangeFilterDatePicker} value={this.state.selectedPlanningHistoryEndDate} /></Col>
                <Col sm='3'>   <MultiSelectComponent id="selectedPlanningHistoryStatus" ref={(scope) => { this.planningHistoryStatus = scope; }} dataSource={this.state.planningHistoryStatus}
                  value={this.state.selectedPlanningHistoryStatus} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'status', value: 'status' }} placeholder={"Status"} mode="CheckBox" showSelectAll={true}
                  showDropDownIcon={true} filterBarPlaceholder="Search Pending" popupHeight="300px">
                  <Inject services={[CheckBoxSelection]} />
                </MultiSelectComponent></Col>
                <Col sm='3'>
                  <MultiSelectComponent id="selectedPlanningHistoryAlert" ref={(scope) => { this.planningHistoryAlert = scope; }} dataSource={this.state.planningHistoryAlert}
                    value={this.state.selectedPlanningHistoryAlert} change={this.onChangePlanningHistoryMultiSelect} fields={{ text: 'alertCode', value: 'alertCode' }} placeholder={"Alert"} mode="CheckBox" showSelectAll={true}
                    showDropDownIcon={true} filterBarPlaceholder="Search Pending" popupHeight="300px">
                    <Inject services={[CheckBoxSelection]} />
                  </MultiSelectComponent></Col>
              </Row>
              <div className='mt-2'></div>
              <Row>
                <Col sm='6'>
                  <Button color="primary" size="sm" id="searchOrders" onClick={this.searchOrders}>{"Search"}</Button>
                  <Button color="primary" size="sm" className='ml-3' id="resetOrders" onClick={() => { this.resetOrders(true) }}  >{"Reset Filter"}</Button>
                </Col>
              </Row>
            </div>
          </div>
          <div>
            <GridComponent gridLines='Both' showColumnMenu={false} queryCellInfo={this.customizeCell} selectionSettings={{ enableToggle: false }} ref={grid => this.orderGrid = grid} allowGrouping={true}
              allowResizing={false} allowTextWrap={true} rowHeight={30} filterSettings={this.state.FilterSettingsModel2} dataSource={this.state.orders} allowFiltering={true}
              allowSorting={true} allowPaging={true} commandClick={this.commandClick} rowSelected={this.getSelectedRowPlannings} enableHover={false} allowExcelExport={true} id="orderStatus"  toolbarClick={this.exportSchedulingStatusGrid}  toolbar={['ExcelExport', 'Search']} >
              <ColumnsDirective>
                <ColumnDirective field='saidId' filter={this.state.IFilter} headerText='Said No' width='85' />
                <ColumnDirective field='buyerPoNo' filter={this.state.IFilter} headerText='Buyer Po No' width='80' />
                <ColumnDirective field='lineItemNo' filter={this.state.IFilter} headerText='Line ItemNo' width='80' />
                <ColumnDirective field='buyerName' filter={this.state.IFilter} headerText='Buyer Name' width='80' />

                <ColumnDirective field='plantName' filter={this.state.IFilter} headerText='Plant Name' width='100' />
                <ColumnDirective field='unitName' filter={this.state.IFilter} headerText='Unit Name' width='90' />

                <ColumnDirective field='pcdDate' filter={this.state.IFilter} format={{ type: 'date', format: "dd/MM/yy" }} headerText='PCD Date' width='70' />
                <ColumnDirective field='poOrderedQty' filter={this.state.IFilter} headerText='Buyer Po Qty' width='90' />
                <ColumnDirective field='scheduledQty' filter={this.state.IFilter} headerText='Scheduled Qty' width='85' />
                <ColumnDirective field='leftPoQtyTobeScheduled' filter={this.state.IFilter} headerText='Balanced Scheduled Qty' width='100' />
                <ColumnDirective field='plannedQty' filter={this.state.IFilter} headerText='Planned Qty' width='85' />
                <ColumnDirective field='exFactoryDate' filter={this.state.IFilter} format={{ type: 'date', format: "dd/MM/yy" }} headerText='Ex-Factory Date' width='100' />

                <ColumnDirective field='status' filter={this.state.IFilter} headerText='Status' width='75' />

                <ColumnDirective field='alertCode' filter={this.state.IFilter} headerText='7 or 14 days left' width='100' />
                <ColumnDirective headerText='Stock' commands={this.commands} width='90' textAlign='Left' />
                <ColumnDirective field='pcdDateForFilter' visible={false} format={{ type: 'date', format: "dd/MM/yy" }} headerText='Filter Date' width='70' />
              </ColumnsDirective>
              <Inject services={[Filter, Sort, Resize, Page, Group, CommandColumn, ColumnMenu, Toolbar, ExcelExport]} />
            </GridComponent>
          </div>
        </DialogComponent>
        {/*---------------- Filter Dialog ---------------- */}
        {/*---------------- Stock Dialog ---------------- */}
        <DialogComponent id="stockDialog" isModal={true} animationSettings={this.state.animationSettings} showCloseIcon={true} beforeClose={this.closeStockDialog} header='Stock' visible={this.state.isShowStockDialog} width='80%'>
          <div>
            <GridComponent gridLines='Both' selectionSettings={{ enableToggle: false }} ref={grid => this.stockGrid = grid} allowGrouping={true} enableHover={true}
              allowResizing={false} allowTextWrap={true} rowHeight={30} filterSettings={this.state.FilterSettingsModel2} dataSource={this.state.stocks} allowFiltering={true}
              allowSorting={true} allowPaging={true}>
              <ColumnsDirective>
                <ColumnDirective field='said' headerText='Said No' width='85' />
                <ColumnDirective field='line_item_no' headerText='LineItem No' width='100' />
                <ColumnDirective field='buyer_po' headerText='Buyer PO' width='90' />
                <ColumnDirective field='material_code' headerText='Material Code' width='80' />
                <ColumnDirective field='material_description' headerText='Material Desc' width='70' />
                <ColumnDirective field='material_category' headerText='Material Category' width='90' />
                <ColumnDirective field='materialBluePCDdate' format={{ type: 'date', format: "dd/MM/yy" }} headerText='Mat Blue PCD Date' width='115' />
                <ColumnDirective field='materialBluePCDQty' headerText='Mat Blue PCD Qty' width='115' />
                <ColumnDirective field='required_material_Qty' headerText='Material Qty' width='80' />
                <ColumnDirective field='issued_qty' headerText='Issued Qty' width='65' />
                <ColumnDirective field='reserved_qty' headerText='Reserved Qty' width='70' />
              </ColumnsDirective>
              <Inject services={[Filter, Sort, Resize, Page, Group]} />
            </GridComponent>
          </div>
        </DialogComponent>
        {/*---------------- Stock Dialog ---------------- */}
      </div>

    )
  }
}
export default withTranslation("translations")(LineScheduling);
