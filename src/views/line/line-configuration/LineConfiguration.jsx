import * as React from 'react';
import { GridComponent, Resize, Toolbar, ColumnsDirective, CommandColumn, ColumnDirective, Filter, Reorder, Inject, Selection, VirtualScroll, Edit, Page, Sort, FilterSettingsModel, IFilter } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { RadioButtonComponent, CheckBoxComponent, SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import LineConfigurationModel from './LineConfigModel';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { withTranslation, Trans } from "react-i18next";
import LineConfigService from './LineConfigService';
import ProductionProcessModel from './ProductionProcessModel';
import ProdSubLineDtosModel from './prodSubLineDtosModel'
import WorkingShiftModel from './WorkingShiftModel';
import { ensureBlock } from '@babel/types';
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import SplitterLayout from 'react-splitter-layout';
import 'react-splitter-layout/lib/index.css';
import errorMessages from '../../errorHandler/errorMessages';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { element } from 'prop-types';

class LineConfiguration extends React.PureComponent {

    constructor(props) {
        super(props);
        this.lineConfigService = new LineConfigService();
        this.initialErrorStatus = {
            plantId: false,
            unitId: false,
            lineName: false,
            lineNameLength: false,
            numberOfMachines: false,
            workingHour: false,
            supervisor: false,
            shift: false
        };
        this.initialErrorStatusInner = {
            category: false,
            endDate: false,
            numberOfMachines: false,
            startDate: false,
            workingHour: false
        }
        this.toolbarOptions = ['Search'];
        this.mounted = false;
        this.state = {
            model: new LineConfigurationModel(null, null, null, "", "", "", true, true, null, [], [], [], null, null, []),
            prodSubLinemodel: new ProdSubLineDtosModel(null, null, null, null, null, null, null),
            date: new Date(),
            format: { type: 'dateTime', format: 'dd/MM/yyyy' },
            configKeys: {},
            minStartDate: null,
            prodSubLineList: [],
            errorObject: this.initialErrorStatus,
            errorObjectInner: this.initialErrorStatusInner,
            isSidebar: true,
            ischangecat: false,
            isShowShiftDetails: false,
            isEdit: false,
            isShowConfirmationDialog: false,
            isDisabled: false,
            validationErrorCss: "custom-error",
            tableData: [],
            plants: [],
            units: [],
            category: [],
            lineProcesses: [],
            resetLineProcesses: [],
            supervisorList: [],
            workingShifts: [],
            selectedWorkingShift: null,
            editSettings: { allowEditing: false, allowAdding: false, allowDeleting: true },
            animationSettings: { effect: 'FlipYLeft' },
            commands: [
                { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
                { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },],
            FilterSettingsModel: { type: 'Menu' },
            IFilter: { type: 'CheckBox' },
            PageSettingsModel: { pageSize: 8, pageSizes: true },
            toasterPosition: { X: 'Right', Y: 'Bottom' },
            toasterMessage: [
                { title: 'Warning!', content: 'Line Discarded !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Success!', content: 'Line added successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
                { title: 'Information!', content: 'Please read the comments carefully.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Success!', content: 'Line saved successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Success!', content: 'Line deleted successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Information!', content: 'Start Date & End Date must not be collide with existing dates !!.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Information!', content: 'Please check time formats !!.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Information!', content: 'Please check start & end time !!.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
            ],
            confirmButton: [{
                click: (args) => {
                    this.delete(args);
                    // this.setState({ isShowConfirmationDialog: false });
                },
                buttonModel: { content: 'Yes', isPrimary: true }
            },
            {
                click: (args) => {
                    this.cancelConformDialog(args);
                    // this.setState({ isShowConfirmationDialog: false });
                },
                buttonModel: { content: 'No' }
            }],
            editSettingsWorkingShiftGrid: { allowEditing: true, allowEditOnDblClick: true },
            workingShiftGridCommand: [{ type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat edit-btn' } },
            { type: 'Save', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
            { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } }],
            innerGridCommand: [{ type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },],
            editSettingsInnerGrid: { allowDeleting: true },
        }
    }

    componentDidMount() {
        this.mounted = true;
        if (this.mounted) {
            this.getWorkingShifts();
            this.getLineProcesses();
            this.getPlantList();
            this.getGridData();
            this.getCategory();
            this.getSupervisorList();
            this.getConfgKey();
            document.addEventListener("keydown", this.escFunction, false);
        }
    }


    componentWillUnmount() {
        this.mounted = false;
        document.removeEventListener("keydown", this.escFunction, false);
    }

    escFunction = (event) => {
        if (event.keyCode === 27) {
            this.setState({ isSidebar: true, isEdit: false })
            document.body.classList.remove('overflow-hidden-body');
        }
    }

    createToaster(message) {
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
    }

    //---------------------------------------------------------API--------------------------------------------------//

    async getConfgKey() {
        let response = (await this.lineConfigService.getConfgKey())
        if ((response != undefined) && (response.data.data.length > 0) && (response.data.status == "success") && (this.mounted)) {
            let configObj = {};
            response.data.data.forEach(element => {
                configObj[element.keyName] = /^\d+$/.test(element.keyValue) == true ? Number(element.keyValue) : element.keyValue;
            })
            this.setState({ configKeys: configObj })
        }
    }

    async getWorkingShifts() {
        let response = (await this.lineConfigService.getWorkingShifts())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            this.setState({ workingShifts: response.data.data })
        }
    }

    async getWorkingShiftDetails(shiftName) {
        let response = (await this.lineConfigService.getWorkingShiftDetails(shiftName))
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            let workingShiftList = [];
            response.data.data.forEach(element => {
                workingShiftList.push(new WorkingShiftModel(element.id, element.hour, element.shiftCategory, element.workCategory, this.convertShiftTime(element.startTime), this.convertShiftTime(element.endTime), null));
            })
            this.setState({ model: { ...this.state.model, prodLineShiftTimingsDtos: workingShiftList } })
        }
    }

    async getSupervisorList() {
        let response = (await this.lineConfigService.getSupervisorList())
        if ((response != undefined) && (response.data.length > 0) && (this.mounted)) {
            this.setState({ supervisorList: response.data })
        }
    }

    async getGridData() {
        let gridData = [];
        global.showLoader();
        let response = (await this.lineConfigService.getGridData())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            response.data.data.map(lineObj => {
                let lineProcesses = [];
                let prodSubLineDtoList = [];
                let workingShiftList = [];
                lineObj.lineProcesses.map(processObj => {
                    lineProcesses.push(new ProductionProcessModel(processObj.id, processObj.name, true))
                })
                lineObj.prodSubLineDtos.forEach(element => {
                    let startDate = new Date(element.startDate.year, element.startDate.month - 1, element.startDate.day, 9);
                    let endDate = new Date(element.endDate.year, element.endDate.month - 1, element.endDate.day, 9)
                    prodSubLineDtoList.push(new ProdSubLineDtosModel(element.id, element.category, endDate, element.numberOfMachines, startDate, element.workingHour, element.prodLineId));
                });
                lineObj.prodLineShiftTimingsDtos.forEach(shiftObj => {
                    workingShiftList.push(new WorkingShiftModel(shiftObj.id, shiftObj.hour, shiftObj.shiftCategory, shiftObj.workCategory, this.convertShiftTime(shiftObj.startTime), this.convertShiftTime(shiftObj.endTime), shiftObj.shiftMasterId));
                })
                gridData.push(new LineConfigurationModel(lineObj.plantId, lineObj.unit.id, lineObj.id, lineObj.unit.plant.name, lineObj.lineName, lineObj.unit.name, lineObj.status, lineObj.isInternal, lineObj.numberOfMachines, prodSubLineDtoList, lineObj.categories[0], lineProcesses, lineObj.workingHour, lineObj.supervisor, workingShiftList))
                console.log(lineObj);
            })
            this.setState({ tableData: gridData, isShowShiftDetails: false }, () => {
                global.hideLoader()
                this.lineGridObj.clearFiltering();
            })

        }
        //this.setState({ tableData: require('../../../assets/data/gridData.js').dummyGridData })
    }

    async getLineProcesses() {
        let lineProcessesList = []
        let response = (await this.lineConfigService.getLineProcesses())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            response.data.data.map(processObj => {
                lineProcessesList.push(new ProductionProcessModel(processObj.id, processObj.name, false))
            })
            this.setState({ lineProcesses: lineProcessesList, resetLineProcesses: lineProcessesList })
        }
        //  this.setState({ lineProcesses: require('../../../assets/data/lineProcesses.js').dummyLineProcessesData, resetLineProcesses: require('../../../assets/data/lineProcesses.js').dummyLineProcessesData })
    }

    async getPlantList() {
        let response = (await this.lineConfigService.getPlantList())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted))
            this.setState({ plants: response.data.data })
        //   this.setState({ plants: require('../../../assets/data/plants.js').dummyPlantsData })
    }

    async getUnitList(plantId) {
        let response = (await this.lineConfigService.getUnitList(plantId))
        if ((response != undefined) && (response.data.status == "success") && (this.mounted))
            this.setState({ units: response.data.data })
        // this.setState({ units: require('../../../assets/data/units.js').dummyUnitData })
    }

    async getCategory() {
        let response = (await this.lineConfigService.getCategory())
        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            this.setState({ category: response.data.data })
        }
        //   this.setState({ category: require('../../../assets/data/category.js').dummyCategoryData })
    }

    async addLine(newLine) {
        const { t, i18n } = this.props;
        let response = (await this.lineConfigService.addLine(newLine))
        if ((response != undefined) && (response.data.status == "success")) {
            this.getGridData();
            this.toastObj.show(this.state.toasterMessage[1]);
            this.setState({
                isSidebar: true, isShowShiftDetails: false, selectedWorkingShift: null, isDisabled: false, lineProcesses: this.state.resetLineProcesses, isEdit: false,
                model: new LineConfigurationModel(null, null, null, "", "", "", true, true, null, [], this.state.category[0], [], null, null, []),
                errorObject: this.initialErrorStatus, errorObjectInner: this.initialErrorStatusInner, toasterPosition: { X: 'Right', Y: 'Bottom' }
            })
            global.hideLoader()

        } else if ((response != undefined) && (response.data.status == "fail")) {
            this.setState({ isDisabled: false, isEdit: false, toasterPosition: { X: 'Left', Y: 'Bottom' } })
            this.toastObj.show(this.createToaster(t(errorMessages[response.data.data.errorCode])));
            global.hideLoader()
        }
        else {
            this.toastObj.show(this.state.toasterMessage[2]);
            global.hideLoader()
            this.setState({ isSidebar: true, isShowShiftDetails: false, isDisabled: false, lineProcesses: this.state.resetLineProcesses, isEdit: false, selectedWorkingShift: null, model: new LineConfigurationModel(null, null, null, "", "", "", true, true, null, [], this.state.category[0], [], null, null, []), errorObject: this.initialErrorStatus, errorObjectInner: this.initialErrorStatusInner, toasterPosition: { X: 'Right', Y: 'Bottom' } })
        }
    }

    async editLine(editedLine) {
        const { t, i18n } = this.props;
        let response = (await this.lineConfigService.editLine(editedLine))
        if ((response != undefined) && (response.data.status == "success")) {
            this.getGridData();
            this.toastObj.show(this.state.toasterMessage[4]);
            global.hideLoader()
        } else if ((response != undefined) && (response.data.status == "fail")) {
            this.toastObj.show(this.createToaster(t(errorMessages[response.data.data.errorCode])));
            global.hideLoader()
        }else {
            this.toastObj.show(this.state.toasterMessage[2]);
            global.hideLoader()
        }
        this.setState({ isSidebar: true, isShowShiftDetails: false, lineProcesses: this.state.resetLineProcesses, isEdit: false, selectedWorkingShift: null, model: new LineConfigurationModel(null, null, null, "", "", "", true, true, null, [], this.state.category[0], [], null, null, []), errorObject: this.initialErrorStatus, errorObjectInner: this.initialErrorStatusInner })
    }

    async deleteLine(deletedLineId) {
        const { t, i18n } = this.props;
        let response = (await this.lineConfigService.deleteLine(deletedLineId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.getGridData();
            this.toastObj.show(this.state.toasterMessage[5]);
            global.hideLoader()
        } else if ((response != undefined) && (response.data.status == "fail")) {
            this.toastObj.show(this.createToaster(t(errorMessages[response.data.data.errorCode])));
            global.hideLoader()
        } else {
            this.toastObj.show(this.state.toasterMessage[2]);
            global.hideLoader()
        }
        this.setState({ lineIdToDelete: null });
    }


    async addOverride(editedLine) {
        let response = (await this.lineConfigService.addOverride(editedLine))
        if ((response != undefined) && (response.data.status == "success")) {
            // this.getGridData();
            this.state.prodSubLineList = [];
            response.data.data.forEach(element => {
                let startDate = new Date(element.startDate.year, element.startDate.month - 1, element.startDate.day, 9);
                let endDate = new Date(element.endDate.year, element.endDate.month - 1, element.endDate.day, 9)
                this.state.prodSubLineList.push(new ProdSubLineDtosModel(element.id, element.category, endDate, element.numberOfMachines, startDate, element.workingHour, element.prodLineId));
            });
            this.subLineObj.dataSource = this.state.prodSubLineList;
            this.subLineObj.refresh();
            this.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-info toast-icons' });
            global.hideLoader()
        } else {
            this.toastObj.show({ title: 'Info!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
            global.hideLoader()
        }
    }

    async removeOverride(subLineId) {
        let response = (await this.lineConfigService.removeOverride(subLineId))
        this.setState({ isDisabled: true });
        if ((response != undefined) && (response.data.status == "success")) {
            this.state.prodSubLineList = [];
            response.data.data.forEach(element => {
                let startDate = new Date(element.startDate.year, element.startDate.month - 1, element.startDate.day, 9);
                let endDate = new Date(element.endDate.year, element.endDate.month - 1, element.endDate.day, 9)
                this.state.prodSubLineList.push(new ProdSubLineDtosModel(element.id, element.category, endDate, element.numberOfMachines, startDate, element.workingHour, element.prodLineId));
            });
            this.subLineObj.dataSource = this.state.prodSubLineList;
            this.subLineObj.refresh();
            this.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-info toast-icons' });
            global.hideLoader()
        } else {
            this.toastObj.show({ title: 'Info!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
            global.hideLoader()
        }
    }

    //---------------------------------------------CRUD-----------------------------------------------//


    async onEditClick(data) {
        let selectedRowLineProcesses = data.lineProcesses;
        let selectedWorkingShift = data.prodLineShiftTimingsDtos.length > 0 ? data.prodLineShiftTimingsDtos[0].shiftCategory : null;
        let allLineProcessesFromState = Object.assign([], this.state.lineProcesses);
        for (let i = 0; i < selectedRowLineProcesses.length; i++) {
            for (let j = 0; j < allLineProcessesFromState.length; j++) {
                if (selectedRowLineProcesses[i].name == allLineProcessesFromState[j].name) {
                    allLineProcessesFromState.splice(j, 1)
                }
            }
        }
        selectedRowLineProcesses = selectedRowLineProcesses.concat(allLineProcessesFromState);
        await this.getUnitList(data.plantId);
        this.setState({ model: data, isSidebar: false, lineProcesses: selectedRowLineProcesses, isEdit: true, prodSubLineList: data.prodSubLineDtos, selectedWorkingShift })
    }



    onSave = (event) => {
        let tempLineProcesses = [];
        document.body.classList.remove('overflow-hidden-body');
        let copyLineProcesses = JSON.parse(JSON.stringify(this.state.lineProcesses));
        for (let i = 0; i < copyLineProcesses.length; i++) {
            if (copyLineProcesses[i].isChecked == true) {
                delete copyLineProcesses[i].isChecked;
                tempLineProcesses.push(copyLineProcesses[i]);
            }
        }
        let model = Object.assign({}, this.state.model, { lineProcesses: tempLineProcesses });
        delete model.plantName;
        delete model.unitName;
        if (this.isValidate(model)) {
            if (!this.state.isEdit) {
                this.setState({ isDisabled: true }, () => {
                    // model.prodSubLineDtos = this.state.prodSubLineList
                    model.prodLineShiftTimingsDtos.forEach(element => {      // Setting shift master id.
                        element.shiftMasterId = element.id;
                    })
                    global.showLoader()
                    this.addLine(model);
                })
            } else {
                // model.prodSubLineDtos = this.state.prodSubLineList
                model.prodLineShiftTimingsDtos.forEach(element => {         // Setting shift master id , if user has changed the shift. 
                    if (element.shiftMasterId == null)
                        element.shiftMasterId = element.id;
                })
                global.showLoader()
                this.editLine(model);
            }
        }
        // this.setState({ tableData: [...this.state.tableData, model], isSidebar: true, lineProcesses: this.state.resetLineProcesses, isEdit: false })
        // this.toastObj.show(this.state.toasterMessage[1]);
    }

    onAddOverride = (event) => {
        let that = this;
        const { prodSubLinemodel, prodSubLineList } = that.state;
        // let newProdSubLineList = prodSubLineList;
        let newProdSubLineList = Object.assign([], prodSubLineList);
        newProdSubLineList.push(prodSubLinemodel);


        if (this.isValidateInner()) {
            that.setState({ prodSubLinemodel: new ProdSubLineDtosModel(null, null, null, null, null, null, null), minStartDate: null })
            let model = Object.assign({}, this.state.model);
            this.setState({ isDisabled: true }, () => {
                model.prodSubLineDtos = newProdSubLineList;
                global.showLoader()
                this.addOverride(model);
            })
        }

        // this.setState({ tableData: [...this.state.tableData, model], isSidebar: true, lineProcesses: this.state.resetLineProcesses, isEdit: false })
        // this.toastObj.show(this.state.toasterMessage[1]);
    }


    isValidate(model) {
        let isValidate = true;
        let errorObj = Object.assign({}, this.state.errorObject);
        if (model.plantId == null) {
            errorObj.plantId = true;
            isValidate = false;
        }
        if (model.unitId == null) {
            errorObj.unitId = true;
            isValidate = false;
        }
        if (model.lineName == "") {
            errorObj.lineName = true;
            isValidate = false;
        }
        if (model.numberOfMachines == null) {
            errorObj.numberOfMachines = true;
            isValidate = false;
        }
        if (model.workingHour == null) {
            errorObj.workingHour = true;
            isValidate = false;
        }
        if (model.lineName.length > 25) {
            errorObj.lineNameLength = true;
            isValidate = false;
        }
        if (model.supervisor == null) {
            errorObj.supervisor = true;
            isValidate = false;
        }
        if (model.prodLineShiftTimingsDtos.length == 0) {
            errorObj.shift = true;
            isValidate = false;
        }
        // if (model.lineProcesses.length == 0) {
        //     isValidate = false;
        // }
        this.setState({ errorObject: errorObj })
        return isValidate;
    }


    //-------------------------------------ON-CHANGE EVENTS------------------------------------------------//

    onChangeWorkingShift = (event) => {
        if ((event.e != undefined) && (event.e != null) && (event.value != undefined) && (event.value != null))
            this.getWorkingShiftDetails(event.value);
        this.setState({ selectedWorkingShift: event.value, errorObject: { ...this.state.errorObject, [event.element.id]: false } })
    }

    onChangeDropdown = (event) => {
        let that = this;
        this.setState({ model: { ...this.state.model, [event.element.id]: event.value }, errorObject: { ...this.state.errorObject, [event.element.id]: false } }, () => {
            if ((event.element.id == "plantId") && (event.e != undefined)) {
                this.setState({ units: [], model: { ...that.state.model, unitId: null } }, () => {
                    this.getUnitList(event.value);
                })
            }
        });
    }

    onChangeStatus = (event) => {
        this.setState({ model: { ...this.state.model, status: event.checked } })
    }

    onChangeTextField = (event) => {
        if ((event.value != null) && (event.value != undefined)) {
            if ((event.value.trim()).length > 25) {
                this.setState({ model: { ...this.state.model, [event.target.id]: event.value.trim() }, errorObject: { ...this.state.errorObject, [event.target.id]: false, lineNameLength: true } })
            } else {
                this.setState({ model: { ...this.state.model, [event.target.id]: event.value.trim() }, errorObject: { ...this.state.errorObject, [event.target.id]: false, lineNameLength: false } })
            }
        }
    }

    onChangeRadioButton = (event) => {
        this.setState({ model: { ...this.state.model, isInternal: event.value } })
    }

    onChangeNumericText = (event) => {
        this.setState({ model: { ...this.state.model, [event.target.id]: Number(event.value) }, errorObject: { ...this.state.errorObject, [event.target.id]: false } })
    }

    onChangeProductionProcessCheckBox = (event) => {
        let that = this;
        const changedObject = Object.assign({}, that.state.lineProcesses[event.target.props.id], { isChecked: event.target.checked });
        const lineProcesses = Object.assign([], that.state.lineProcesses, { [event.target.props.id]: new ProductionProcessModel(changedObject.id, changedObject.name, changedObject.isChecked) });
        // if ((changedObject.name == "Stitching") && (changedObject.isChecked == false)) {
        //     that.setState({ model: { ...this.state.model, numberOfMachines: "" }, lineProcesses })
        // } else {
        that.setState({ lineProcesses })
        // }
    }

    onChangeCategory = (event) => {
        const selectedCategory = this.state.category.filter(item => {
            return item.id == event.value;
        })
        this.setState({ model: { ...this.state.model, categories: selectedCategory } })
    }


    onChangeSupervisor = (event) => {
        const { model } = this.state;
        if (event.e != undefined)
            this.setState({ model: { ...model, [event.element.id]: event.value }, errorObject: { ...this.state.errorObject, [event.element.id]: false } })
    }


    //----------------------------------------change Category ,Date and No.of machine--------------------------------------//


    handleCategory = (event) => {
        let that = this;
        let selectedCategory = event.itemData;
        that.setState({ prodSubLinemodel: { ...that.state.prodSubLinemodel, category: selectedCategory }, errorObjectInner: { ...this.state.errorObjectInner, [event.element.id]: false } })
    }

    changetDate = (event) => {
        let that = this;
        if (((event.event != null) && (event.value != null)) || (('values' in event) && (event.value != null))) {  // Change + Today
            let date = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 9);
            if (event.element.id == "startDate")
                that.setState({
                    prodSubLinemodel: { ...that.state.prodSubLinemodel, [event.element.id]: date, gridStartDate: (date).toDateString(), endDate: null },
                    errorObjectInner: { ...that.state.errorObjectInner, [event.element.id]: false }, minStartDate: event.value
                })
            else
                that.setState({ prodSubLinemodel: { ...that.state.prodSubLinemodel, [event.element.id]: date, gridEndDate: (date).toDateString() }, errorObjectInner: { ...this.state.errorObjectInner, [event.element.id]: false } })
        }
    }

    onChangeNumeric = (event) => {
        let that = this;
        that.setState({ prodSubLinemodel: { ...that.state.prodSubLinemodel, [event.target.id]: Number(event.value) }, errorObjectInner: { ...this.state.errorObjectInner, [event.target.id]: false } })
    }

    isValidateInner = () => {
        const { prodSubLinemodel, prodSubLineList } = this.state;
        let isValidate = true;
        let errorObjInner = Object.assign({}, this.state.errorObjectInner);
        if (prodSubLinemodel.category == null) {
            errorObjInner.category = true;
            isValidate = false;
        }
        if (prodSubLinemodel.endDate == null) {
            errorObjInner.endDate = true;
            isValidate = false;
        }
        if (prodSubLinemodel.numberOfMachines == null) {
            errorObjInner.numberOfMachines = true;
            isValidate = false;
        }
        if (prodSubLinemodel.workingHour == null) {
            errorObjInner.workingHour = true;
            isValidate = false;
        }
        if (prodSubLinemodel.startDate == null) {
            errorObjInner.startDate = true;
            isValidate = false;
        }
        if ((prodSubLinemodel.startDate != null) && (prodSubLinemodel.endDate != null)) {
            let startDate = new Date((prodSubLinemodel.startDate).getFullYear(), (prodSubLinemodel.startDate).getMonth(), (prodSubLinemodel.startDate).getDate());
            let endDate = new Date((prodSubLinemodel.endDate).getFullYear(), (prodSubLinemodel.endDate).getMonth(), (prodSubLinemodel.endDate).getDate());
            for (let i = 0; i < prodSubLineList.length > 0; i++) {
                let prodSubLineObjStartDate = new Date((prodSubLineList[i].startDate).getFullYear(), (prodSubLineList[i].startDate).getMonth(), (prodSubLineList[i].startDate).getDate());
                let prodSubLineObjEndDate = new Date((prodSubLineList[i].endDate).getFullYear(), (prodSubLineList[i].endDate).getMonth(), (prodSubLineList[i].endDate).getDate());
                if ((startDate >= prodSubLineObjStartDate && startDate <= prodSubLineObjEndDate) ||
                    (endDate >= prodSubLineObjStartDate && endDate <= prodSubLineObjEndDate) ||
                    (startDate <= prodSubLineObjStartDate && endDate >= prodSubLineObjEndDate)) {
                    this.toastObj.show(this.state.toasterMessage[6]);
                    isValidate = false;
                    break;
                }
            }
        }
        this.setState({ errorObjectInner: errorObjInner })
        return isValidate;
    }


    sideBar = (event) => {
        document.body.classList.toggle('overflow-hidden-body');
        // if (event.target.id == "cancel")
        // this.toastObj.show(this.state.toasterMessage[0]);
        this.setState({ minStartDate: null, isShowShiftDetails: false, selectedWorkingShift: null, ischangecat: false, isSidebar: !this.state.isSidebar, prodSubLineList: [], units: [], isDisabled: false, model: new LineConfigurationModel(null, null, null, "", "", "", true, true, null, [], this.state.category[0], [], null, null, []), lineProcesses: this.state.resetLineProcesses, isEdit: false, errorObject: this.initialErrorStatus, errorObjectInner: this.initialErrorStatusInner, toasterPosition: { X: 'Right', Y: 'Bottom' } })
        console.log(this.state.model);
    }


    //-------------------------------------------------OTHER EVENTS--------------------------------------//

    workingShiftGridActionBegin = (args) => {
        let that = this;
        if (args.requestType == "save") {
            args.cancel = true;
            if (this.isValidateWorkingShiftHours((args.data.startTime).trim(), (args.data.endTime).trim())) {
                that.state.model.prodLineShiftTimingsDtos.map(item => {
                    if (item.id == args.data.id) {
                        item.startTime = args.data.startTime.trim();
                        item.endTime = args.data.endTime.trim();
                    }
                })
            }
            that.workingShiftObj.closeEdit();
        }
    }


    isValidateWorkingShiftHours(startDate, endDate) {
        let isValidate = true;
        if (/^[0-9]{2}\:[0-9]{2}\:[0-9]{2}?$/.test(startDate) && /^[0-9]{2}\:[0-9]{2}\:[0-9]{2}?$/.test(endDate)) {
            let startDateArray = startDate.split(":");
            let endDateArray = endDate.split(":");
            if (((Number(startDateArray[0]) <= 23) && (Number(endDateArray[0] <= 23)))
                && ((Number(startDateArray[1]) <= 59) && (Number(endDateArray[1] <= 59)))
                && ((Number(startDateArray[2]) <= 59) && (Number(endDateArray[2] <= 59)))) {
                // IF FORMAT OK
                if (((Number(startDateArray[0])) <= (Number(endDateArray[0]))) || ((Number(startDateArray[0])) == (Number(endDateArray[0])))) {    // STARTTIME SHOULDN'T BE GREATER THAN ENDTIME
                    if ((Number(startDateArray[0])) == (Number(endDateArray[0]))) {  //hrs equal
                        if (((Number(startDateArray[1])) <= (Number(endDateArray[1]))) || ((Number(startDateArray[1])) == (Number(endDateArray[1])))) {
                            if ((Number(startDateArray[1])) == (Number(endDateArray[1]))) {
                                if ((Number(startDateArray[2])) <= (Number(endDateArray[2]))) {
                                    if ((Number(startDateArray[2])) == (Number(endDateArray[2]))) {
                                        isValidate = false;
                                        this.toastObj.show(this.state.toasterMessage[8]);
                                    } else {
                                        isValidate = true;
                                    }
                                } else {
                                    isValidate = false;
                                    this.toastObj.show(this.state.toasterMessage[8]);
                                }
                            } else {
                                isValidate = true;
                            }
                        } else {
                            isValidate = false;
                            this.toastObj.show(this.state.toasterMessage[8]);
                        }
                    } else {
                        isValidate = true;
                    }
                }
                else {
                    isValidate = false;
                    this.toastObj.show(this.state.toasterMessage[8]);
                }

            } else {
                isValidate = false;
                this.toastObj.show(this.state.toasterMessage[8]);
            }
        } else {
            isValidate = false;
            this.toastObj.show(this.state.toasterMessage[7]);
        }

        return isValidate;
    }

    disableDatePickerDate = (args) => {
        if (args.date.getDay() === 0) {
            args.isDisabled = true;
        }
    }

    statusTemplate(props) {
        return (<div className={props.status == true ? 'Active' : 'Inactive'}>{props.status == true ? 'Active' : 'Inactive'}</div>);
    }

    linetypeTemplate(props) {
        return (<div className={props.isInternal == true ? "Internal" : "External"}>{props.isInternal == true ? "Internal" : "External"}</div>);
    }

    lineProcessesTemplate(props) {
        let templateArray = [];
        props.lineProcesses.map((item) => {
            templateArray.push(<span className="lineProcessesItem">{item.name}</span>)
        })
        return templateArray;
    }

    gridValueAccess = (field, data, column) => {
        return data[field].map((item) => {
            return item.name;
        })
    }

    rowselected = (args) => {                     // TO BE IMPROVED !!
        // let that = this;
        // if ((args.target.classList.length > 2) && (that.state.isSidebar == true)) {
        //     if ((args.target.classList.contains('e-edit')) && (args.data != undefined)) {
        //         that.onEditClick(args.data)
        //     }
        // }
    }

    actionBegin = (args) => {                   // TO BE IMPROVED !!
        if (args.requestType == "delete") {
            args.cancel = true;
            // const id = args.data[0].id;
            // this.setState({ isShowConfirmationDialog: true, lineIdToDelete: id })
            //   this.deleteLine(id)
        }
    }

    innerGridAction = (args) => {                   // TO BE IMPROVED !!
        if (args.requestType == "add") {
            args.cancel = true;
            // const id = args.data[0].id;
            // this.setState({ isShowConfirmationDialog: true, lineIdToDelete: id })
            //   this.deleteLine(id)
        }
    }

    commandClick = (args) => {
        if (args.name == "commandClick") {
            if (args.commandColumn.type == "Delete") {
                const id = args.rowData.id;
                this.setState({ isShowConfirmationDialog: true, lineIdToDelete: id })
            }
            if (args.commandColumn.type == "Edit") {
                this.onEditClick(args.rowData)
            }
        }
    }

    onInnerCommandClick = (args) => {
        if (args.commandColumn.type == "Delete") {
            args.cancel = true;
            this.removeOverride(args.rowData.id);
        }
    }

    delete = (args) => {
        const { lineIdToDelete } = this.state;
        global.showLoader();
        this.deleteLine(lineIdToDelete);
        this.setState({ isShowConfirmationDialog: false })
    }
    cancelConformDialog = (args) => {
        this.setState({ isShowConfirmationDialog: false, lineIdToDelete: null })
    }

    changeCategory = () => {
        this.setState({ ischangecat: !this.state.ischangecat })
    }

    showshiftdetails = () => {
        this.setState({ isShowShiftDetails: !this.state.isShowShiftDetails })
    }

    convertShiftTime(dateObject) {
        let hour = dateObject.hour < 10 ? "0" + (dateObject.hour).toString() : (dateObject.hour).toString();
        let minute = dateObject.minute < 10 ? "0" + (dateObject.minute).toString() : (dateObject.minute).toString();
        return hour + ":" + minute + ":" + "00";
    }

    //-------------------------------------------------RENDER--------------------------------------------//

    render() {
        const { t, i18n } = this.props;

        const { lineProcesses } = this.state;
        const dynamicLineProcessesArray = [];
        for (let i = 0; i < lineProcesses.length; i++) {
            dynamicLineProcessesArray.push(<div key={i}>
                <Row className='mt-2'>
                    <Col xs='12' md='12'>
                        <div className="mt-1"></div>
                        <CheckBoxComponent label={lineProcesses[i].name} id={i} onChange={this.onChangeProductionProcessCheckBox} checked={lineProcesses[i].isChecked} />
                    </Col>
                </Row>
            </div>)
        }

        return (
            <div>
                <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
                <div className="container-fluid-inner">
                    <h1 className='pagesheading'>Line Configuration</h1>
                    <div className='clearfix'></div>
                    <Row>
                        <Col>
                            <Button color="primary" size="sm" className="mb-2" onClick={e => this.sideBar(e)}>{t("cpm.lm.addline")} </Button>

                            <GridComponent allowTextWrap={true} gridLines='Both' rowHeight='30' commandClick={this.commandClick} ref={grid => this.lineGridObj = grid} allowReordering={true} actionBegin={this.actionBegin} editSettings={this.state.editSettings} 
                                selectionSettings={{ enableToggle: false }} rowSelected={this.rowselected} actionComplete={this.actionComplete} allowPaging={true} pageSettings={{ pageCount: 5, pageSizes: true }}
                                allowResizing={true} dataSource={this.state.tableData} filterSettings={this.state.FilterSettingsModel} allowSorting={true} allowFiltering={true} toolbar = {this.toolbarOptions}>
                                <ColumnsDirective>
                                    <ColumnDirective field='plantName' filter={this.state.IFilter} headerText={t("cpm.lm.grid.plant.name")} width='220' />
                                    <ColumnDirective field='unitName' filter={this.state.IFilter} headerText={t("cpm.lm.grid.unit.name")} width='85' format="C2" />
                                    <ColumnDirective field='lineName' filter={this.state.IFilter} headerText={t("cpm.lm.grid.line.name")} width='100' />
                                    <ColumnDirective field='numberOfMachines' filter={this.state.IFilter} headerText={t("cpm.lm.field.line.noOfMachine")} width='110' />
                                    <ColumnDirective field='workingHour' filter={this.state.IFilter} headerText={t("cpm.lm.field.line.workingHour")} width='105' />
                                    <ColumnDirective field='categories.0.name' filter={this.state.IFilter} headerText={t("cpm.lm.grid.category")} width='120' />
                                    <ColumnDirective field='isInternal' filter={this.state.IFilter} template={this.linetypeTemplate} headerText={t("cpm.lm.grid.line.type")} width='80' />
                                    <ColumnDirective field='status' filter={this.state.IFilter} headerText={t("cpm.lm.grid.status")} template={this.statusTemplate} width='80'></ColumnDirective>
                                    <ColumnDirective field='lineProcesses' allowSorting={false} allowFiltering={false} headerText={t("cpm.lm.grid.line.processes")} template={this.lineProcessesTemplate} valueAccessor={this.gridValueAccess} width='300'></ColumnDirective>
                                    <ColumnDirective field='action' textAlign="Left" allowFiltering={false} headerText={t("cpm.lm.grid.action")} width='60' commands={this.state.commands} ></ColumnDirective>
                                </ColumnsDirective>
                                <Inject services={[CommandColumn, Sort, Filter, Resize, Reorder, Selection, Edit, Page, Toolbar]} />
                            </GridComponent>
                        </Col>
                    </Row>

                    {/* ----------------------- Add line Section  -----------------------  */}
                    <div className="add-line-section" hidden={this.state.isSidebar}>
                        <SplitterLayout secondaryInitialSize={550} secondaryMinSize={550}>
                            <div className="blank-space" onClick={this.sideBar}></div>
                            <div className="overflow-hidden add-line-section-form">
                                <div className="heading mb-2">{this.state.isEdit ? t("cpm.lm.editline") : t("cpm.lm.addline")} <a className="close" onClick={e => this.sideBar(e)}>+</a></div>

                                <Col xs='12'>
                                    <Row>
                                        <Col xs='12' md='6'>
                                            <div className={this.state.errorObject.plantId == true ? this.state.validationErrorCss : "required-field"} >
                                                <DropDownListComponent color="primary" id="plantId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.model.plantId} change={this.onChangeDropdown} dataSource={this.state.plants} placeholder={t("cpm.lm.line.plant.name")} floatLabelType="Auto" />
                                                <span hidden={!this.state.errorObject.plantId} >{t("cpm.lm.line.validation.plantName")}</span>
                                            </div>
                                        </Col>

                                        <Col xs='12' md='6'>
                                            <div className={this.state.errorObject.unitId == true ? this.state.validationErrorCss : "required-field"} >
                                                <DropDownListComponent color="primary" id="unitId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.model.unitId} change={this.onChangeDropdown} dataSource={this.state.units} placeholder={t("cpm.lm.field.unit.name")} floatLabelType="Auto" />
                                                <span hidden={!this.state.errorObject.unitId} >{t("cpm.lm.line.validation.unitName")}</span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row >
                                        <Col xs='12' md='6'>
                                            <DropDownListComponent color="primary" id="categoryId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.model.categories.length > 0 ? this.state.model.categories[0].id : null} change={this.onChangeCategory} dataSource={this.state.category} placeholder={t("cpm.lm.line.category")} floatLabelType="Auto" />
                                        </Col>
                                        <Col xs='12' md='6'>
                                            <div className={this.state.errorObject.lineName == true || this.state.errorObject.lineNameLength == true ? this.state.validationErrorCss : "required-field"} >
                                                <TextBoxComponent placeholder={t("cpm.lm.field.line.name")} id="lineName" value={this.state.model.lineName} onChange={this.onChangeTextField} floatLabelType="Auto" />
                                                <span hidden={!this.state.errorObject.lineName} >{t("cpm.lm.line.validation.lineName")}</span>
                                                <span hidden={!this.state.errorObject.lineNameLength} >{t("cpm.lm.line.validation.lineNameLength")}</span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs='6' md='4'>
                                            <div className={this.state.errorObject.numberOfMachines == true ? this.state.validationErrorCss : "required-field"} >
                                                <NumericTextBoxComponent
                                                    max={this.state.configKeys.totalMachines == undefined ? 250 : this.state.configKeys.totalMachines}
                                                    min={0}
                                                    placeholder={t("cpm.lm.field.line.noOfMachine")}
                                                    id={"numberOfMachines"}
                                                    format='###.###'
                                                    value={this.state.model.numberOfMachines}
                                                    onChange={this.onChangeNumericText}
                                                    floatLabelType="Auto"
                                                >
                                                </NumericTextBoxComponent>
                                                <span hidden={!this.state.errorObject.numberOfMachines} >{t("cpm.lm.line.validation.noOfMachines")}</span>
                                            </div>
                                        </Col>
                                        <Col xs='6' md='2'>
                                            <div className="mt-3"></div>
                                            <div className="sew-machine"></div>
                                        </Col>
                                        <Col xs='6' md='4'>
                                            <div className={this.state.errorObject.workingHour == true ? this.state.validationErrorCss : "required-field"} >
                                                <NumericTextBoxComponent
                                                    min={1}
                                                    max={this.state.configKeys.workingHour == undefined ? 24 : this.state.configKeys.workingHour}
                                                    placeholder={t("cpm.lm.field.line.workingHour")}
                                                    id={"workingHour"}
                                                    format='###.###'
                                                    value={this.state.model.workingHour}
                                                    onChange={this.onChangeNumericText}
                                                    floatLabelType="Auto"
                                                >
                                                </NumericTextBoxComponent>
                                                <span hidden={!this.state.errorObject.workingHour} >{t("cpm.lm.line.validation.workingHours")}</span>
                                            </div>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs='6' md='4'>
                                            <div className={this.state.errorObject.supervisor == true ? this.state.validationErrorCss : "required-field"}>
                                                <DropDownListComponent placeholder={t("cpm.lm.field.line.supervisor")} dataSource={this.state.supervisorList} color="primary" id="supervisor" change={this.onChangeSupervisor} value={this.state.model.supervisor} allowFiltering={true} fields={{ text: 'firstName', value: 'id' }} floatLabelType="Auto" />
                                                <span hidden={!this.state.errorObject.supervisor} >{t("cpm.lm.line.validation.supervisor")}</span>
                                            </div>
                                        </Col>
                                        <Col xs='6' md='4'>
                                            <div className="mt-3"></div>
                                            <span className="mr-3"> {this.state.model.status == false ? t("cpm.lm.field.line.inActive") : t("cpm.lm.field.line.active")} </span>
                                            <SwitchComponent checked={this.state.model.status} change={this.onChangeStatus} />
                                        </Col>

                                    </Row>
                                    <div className="mt-3 hidden-md"></div>
                                    <Row>
                                        <Col xs='4' md='3'><b>{t("cpm.lm.grid.line.type")}</b></Col>
                                        <Col xs='4' md='3'>
                                            <RadioButtonComponent checked={this.state.model.isInternal} onChange={this.onChangeRadioButton} label={t("cpm.lm.line.line.type.int")} value={true} name="position" />
                                        </Col>
                                        <Col xs='4' md='2'>
                                            <RadioButtonComponent checked={!this.state.model.isInternal} onChange={this.onChangeRadioButton} label={t("cpm.lm.line.line.type.ext")} value={false} name="position" />
                                        </Col>

                                    </Row>
                                    <div className="mt-3 hidden-md"></div>
                                    <div className='d-flex'>
                                        <div>
                                            <Button color="primary" size="sm" id="changeCategory" onClick={this.changeCategory}><i className={this.state.ischangecat ? 'fa fa-minus' : 'fa fa-plus'}></i> {t("cpm.lm.line.changeCategory")}</Button>
                                        </div>

                                    </div>
                                    <div className="mt-3 hidden-md"></div>
                                    <div className="change-category-box" hidden={!this.state.ischangecat}>
                                        <Row>
                                            <Col xs='12' md='4'>
                                                <div className={this.state.errorObjectInner.category == true ? this.state.validationErrorCss : "required-field"}>
                                                    <DropDownListComponent color="primary" id='category' allowFiltering={true} value={this.state.prodSubLinemodel.category} fields={{ text: 'name', value: 'id' }} showSelectAll={true}
                                                        change={this.handleCategory} dataSource={this.state.category} placeholder={t("cpm.lm.line.category")} floatLabelType="Auto" />
                                                    <span hidden={!this.state.errorObjectInner.categories} >{'Please select Category'}</span>
                                                </div>
                                            </Col>

                                            <Col xs='12' md='4'>
                                                <div className={this.state.errorObjectInner.startDate == true ? this.state.validationErrorCss : "required-field"}>
                                                    <DatePickerComponent weekNumber={true} floatLabelType="Auto" min={this.state.date} allowEdit={false} renderDayCell={this.disableDatePickerDate} change={this.changetDate} value={this.state.prodSubLinemodel.startDate} format='dd/MM/yy hh:mm a' id="startDate" placeholder={t("cpm.lm.line.plan.field.startDate")}></DatePickerComponent >
                                                    <span hidden={!this.state.errorObjectInner.startDate} >{"Please select StartDate"}</span>
                                                </div>
                                            </Col>
                                            <Col xs='12' md='4'>
                                                <div className={this.state.errorObjectInner.endDate == true ? this.state.validationErrorCss : "required-field"}>
                                                    <DatePickerComponent floatLabelType="Auto" allowEdit={false} min={this.state.minStartDate} change={this.changetDate} renderDayCell={this.disableDatePickerDate} value={this.state.prodSubLinemodel.endDate} format='dd/MM/yy hh:mm a' id="endDate" placeholder={t("cpm.lm.line.plan.field.endDate")}></DatePickerComponent >
                                                    <span hidden={!this.state.errorObjectInner.endDate} >{"Please select EndDate"}</span>
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs='6' md='4'>
                                                <div className={this.state.errorObjectInner.numberOfMachines == true ? this.state.validationErrorCss : "required-field"} >
                                                    <NumericTextBoxComponent
                                                        max={250}
                                                        min={0}
                                                        placeholder={t("cpm.lm.field.line.noOfMachine")}
                                                        id={"numberOfMachines"}
                                                        format='###.###'
                                                        value={this.state.prodSubLinemodel.numberOfMachines}
                                                        onChange={this.onChangeNumeric}
                                                        floatLabelType="Auto"
                                                    >
                                                    </NumericTextBoxComponent>
                                                    <span hidden={!this.state.errorObjectInner.numberOfMachines} >{"Please enter No of Machines !!"}</span>
                                                </div>
                                            </Col>
                                            <Col xs='6' md='4'>
                                                <div className={this.state.errorObjectInner.workingHour == true ? this.state.validationErrorCss : "required-field"} >
                                                    <NumericTextBoxComponent
                                                        max={24}
                                                        min={1}
                                                        placeholder={t("cpm.lm.field.line.workingHour")}
                                                        id={"workingHour"}
                                                        format='###.###'
                                                        value={this.state.prodSubLinemodel.workingHour}
                                                        onChange={this.onChangeNumeric}
                                                        floatLabelType="Auto"
                                                    >
                                                    </NumericTextBoxComponent>
                                                    <span hidden={!this.state.errorObjectInner.workingHour} >{"Please enter working hours !!"}</span>
                                                </div>
                                            </Col>
                                            <Col xs='6' md='2'>
                                                <div className="mt-3"></div>
                                                <div className="sew-machine"></div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs='12' md='6'>
                                                <div className="mt-3"></div>
                                                <Button color="primary" id="add" size="sm" className="mb-2" onClick={e => this.onAddOverride(e)}>{t("cpm.lm.line.add")}</Button>
                                            </Col>
                                        </Row>

                                        {/* ---------------------- grid change catagory--------------------- */}

                                        <GridComponent allowTextWrap={true} gridLines='Both' height='150' className="mt-3" dataSource={this.state.prodSubLineList} editSettings={this.state.editSettingsInnerGrid}
                                            allowReordering={true} pageSettings={{ pageCount: 3, pageSizes: true }} commandClick={this.onInnerCommandClick} ref={grid => this.subLineObj = grid} actionBegin={this.innerGridAction}
                                            filterSettings={this.state.FilterSettingsModel} allowSorting={true} allowFiltering={true} allowResizing={true}>
                                            <ColumnsDirective >
                                                <ColumnDirective field='category.name' headerText='Category' width='150'></ColumnDirective>
                                                <ColumnDirective field='startDate' headerText='Start Date' format={this.state.format} width='150'></ColumnDirective>
                                                <ColumnDirective field='endDate' headerText='End Date' format={this.state.format} width='150' />
                                                <ColumnDirective field='numberOfMachines' headerText='Machines' width='120' />
                                                <ColumnDirective field='workingHour' headerText='Working Hour' width='120' />
                                                <ColumnDirective field='action' allowSorting={false} allowFiltering={false} width='100' commands={this.state.innerGridCommand} ></ColumnDirective>
                                            </ColumnsDirective>
                                            <Inject services={[CommandColumn, Sort, Filter, Resize, Reorder, Selection, Edit, Page]} />
                                        </GridComponent>

                                        {/* ---------------------- grid change catagory--------------------- */}
                                    </div>
                                    <Row>
                                        <Col xs='6' md='4'>
                                            <div className={this.state.errorObject.shift == true ? this.state.validationErrorCss : "required-field"}>
                                                <DropDownListComponent color="primary" id="shift" allowFiltering={true} change={this.onChangeWorkingShift} value={this.state.selectedWorkingShift} dataSource={this.state.workingShifts} placeholder={t("cpm.lm.line.shift")} floatLabelType="Auto" />
                                                <span hidden={!this.state.errorObject.shift} >{t("cpm.lm.line.validation.shift")}</span>
                                            </div>
                                        </Col>
                                        <Col sm='6'>
                                            <div className="mt-3 hidden-md"></div>
                                            <Button color="primary" size="sm" id="showshiftdetails" onClick={this.showshiftdetails}><i className={this.state.isShowShiftDetails ? 'fa fa-minus' : 'fa fa-plus'}></i> {t("cpm.lm.line.showshiftdetails")}</Button>
                                        </Col>
                                    </Row>
                                    {/* ------------- shift grid ----------------  */}

                                    <div className="change-category-box" hidden={!this.state.isShowShiftDetails}>
                                        <h5>Shift Timings</h5>
                                        <GridComponent gridLines='Both' allowTextWrap={true} className="mt-3" height='150' actionBegin={this.workingShiftGridActionBegin} dataSource={this.state.model.prodLineShiftTimingsDtos} ref={grid => this.workingShiftObj = grid} editSettings={this.state.editSettingsWorkingShiftGrid} allowReordering={true} pageSettings={{ pageCount: 3, pageSizes: true }}
                                            filterSettings={this.state.FilterSettingsModel} allowSorting={true} allowFiltering={true} allowResizing={true}>
                                            <ColumnsDirective >
                                                <ColumnDirective field='workCategory' headerText='Work Category' isPrimaryKey={true} width='120'></ColumnDirective>
                                                <ColumnDirective field='hour' headerText='Hour' width='90' isPrimaryKey={true}></ColumnDirective>
                                                <ColumnDirective field='startTime' headerText='Start Time' width='120' validationRules={{ required: true }} ></ColumnDirective>
                                                <ColumnDirective field='endTime' headerText='End Time' width='120' validationRules={{ required: true }}></ColumnDirective>
                                                <ColumnDirective field='action' headerText='Action' allowSorting={false} allowFiltering={false} width='100' commands={this.state.workingShiftGridCommand} ></ColumnDirective>
                                            </ColumnsDirective>
                                            <Inject services={[CommandColumn, Sort, Filter, Resize, Reorder, Selection, Edit, Page]} />
                                        </GridComponent>
                                    </div>
                                    {/* ------------- shift grid end ----------------  */}
                                    <div className="mt-3 clearfix"></div>
                                    <h6 className="text-heading"><span>{t("cpm.lm.line.line.process")}</span></h6>
                                    <div className="line-process-list">
                                        {dynamicLineProcessesArray}
                                    </div>
                                    <hr />
                                    <Row className="mt-3">
                                        <Col>
                                            <Button color="primary" size="sm" id="save" disabled={this.state.isDisabled} className="float-left mr-3" onClick={e => this.onSave(e)}>{t("cpm.lm.line.save")}</Button>
                                            <Button color="primary" size="sm" id="cancel" className="float-left" onClick={e => this.sideBar(e)}>{t("cpm.lm.line.cancel")}</Button>
                                        </Col>
                                    </Row>
                                </Col>
                            </div>
                        </SplitterLayout>
                    </div>
                    <div className="add-line-section-overlay" hidden={this.state.isSidebar}></div>
                    {/* ----------------------- Add line Section end          -----------------------  */}
                </div>
                <DialogComponent id="confirmDialog" animationSettings={this.state.animationSettings} showCloseIcon={true} close={this.cancelConformDialog} buttons={this.state.confirmButton} header='Delete Line Confirmation !!' visible={this.state.isShowConfirmationDialog} width='400px' content='Are you sure you want to permanently delete Line ?'></DialogComponent>
                <div className="overlayer-popup" hidden={!this.state.isShowConfirmationDialog} ></div>
            </div>
        );
    }
}


export default withTranslation("translations")(LineConfiguration);
