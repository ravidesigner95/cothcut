export class PlannedEventModel {
    constructor(id, monthId, categoryColor, description, startDateTime, endDateTime, oldStartDateTime, oldEndDateTime, saidId, lineItem,prodLineId,plannedInfoId, plannedData,IsReadonly) {
        this.id = id;
        this.monthId = monthId;
        this.categoryColor = categoryColor;
        this.description = description;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.oldStartDateTime = oldStartDateTime;
        this.oldEndDateTime = oldEndDateTime;
        this.saidId = saidId;
        this.lineItem = lineItem;
        this.IsReadonly=IsReadonly;
        this.prodLineId=prodLineId;
        this.plannedInfoId=plannedInfoId;
        this.plannedData = plannedData;
    } 
    id = null;
    monthId = null;
    categoryColor = "";
    description = "";
    startDateTime = null;
    endDateTime = null;
    oldStartDateTime = null;
    oldEndDateTime = null;
    saidId = null;
    lineItem = null;
    IsReadonly=false;
    prodLineId=null;
    plannedInfoId=null;
    plannedData = {}
}

export default PlannedEventModel;