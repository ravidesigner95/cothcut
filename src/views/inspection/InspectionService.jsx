import axios from 'axios';
class InspectionService {
    constructor() { }
    
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    qualityServiceUrl = process.env.REACT_APP_BASEURL_QUALITY;
    inspectionUrl = this.qualityServiceUrl + "/quality_insp_master/getAll";
    roleUrl = this.baseUrl + `/roles`;
    getRulesUrl = this.qualityServiceUrl + '/form_designer/getRulesList';
    getCategoriesUrl = this.qualityServiceUrl + '/form_designer/getCategories';
    getInspCategoriesUrl = this.qualityServiceUrl + '/form_designer/getInspectionCategories';

    getHeadTypesUrl = this.qualityServiceUrl + '/form_designer/getHeadTypes';
    getComponentsUrl = this.qualityServiceUrl + '/form_designer/getComponentsByCategory';
    getSpecificInspectionUrl = this.qualityServiceUrl + '/form_designer/getFormData';
    getSectionsUrl = this.qualityServiceUrl + '/form_designer/getSectionsByCategory';



    saveComponentUrl = this.qualityServiceUrl + '/form_designer/saveComponent';
    addExistingComponentUrl = this.qualityServiceUrl + '/form_designer/addExistingComponent'
    saveInspectionTypeUrl = this.qualityServiceUrl + '/form_designer/saveInspectionType';
    saveSectionUrl = this.qualityServiceUrl + '/form_designer/saveSection';
    addExistingSectionUrl = this.qualityServiceUrl + '/form_designer/addExistingSection'


    getQuesCategoryUrl =this.qualityServiceUrl + '/qualityMaster/getQuesCategory';
    getQuesByCategoryUrl = this.qualityServiceUrl  + '/qualityMaster/getQuestionsByCategory';
    getQuesUrl = this.qualityServiceUrl + '/qualityMaster/getQuestions';

    deleteInspUrl=this.qualityServiceUrl  +'/form_designer/deleteInspectionType';
    deleteCompUrl=this.qualityServiceUrl  +'/form_designer/removeComponent';
    deleteSectionUrl=this.qualityServiceUrl+'/form_designer/removeSection';
    deleteSubSectionUrl=this.qualityServiceUrl+'/form_designer/deleteSubSection';


    getAqlUrl = this.qualityServiceUrl + '/qualityMaster/getAql';
    getDefectsUrl = this.qualityServiceUrl + '/qualityMaster/getDefects';
    getProductionProcessesUrl = this.qualityServiceUrl + '/form_designer/getProductionProcesses'




  
    
    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    //---------------------------------------API-CALLS-----------------------------------------//
    async getRolesList() {
        try {
            const response = await axios.get(this.roleUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getRulesList() {
        try {
            const response = await axios.get(this.getRulesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getCategoriesList() {
        try {
            const response = await axios.get(this.getCategoriesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getInspCategoriesList() {
        try {
            const response = await axios.get(this.getInspCategoriesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getHeadTypes() {
        try {
            const response = await axios.get(this.getHeadTypesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getComponents(ids) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    categoryIds:ids + '',
                }
            };
            const response = await axios.get(this.getComponentsUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getSections(ids) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    categoryIds:ids + '',
                }
            };
            const response = await axios.get(this.getSectionsUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async saveComponent(newComponent) {
        try {
            const response = await axios.post(this.saveComponentUrl, newComponent, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async addExistingComponent(existingComponent) {
        try {
            const response = await axios.post(this.addExistingComponentUrl, existingComponent, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async addExistingSection(existingSection) {
        try {
            const response = await axios.post(this.addExistingSectionUrl, existingSection, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }


    async saveInspectionType(newInspection) {
        try {
            const response = await axios.post(this.saveInspectionTypeUrl, newInspection, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async saveSection(newSection) {
        try {
            const response = await axios.post(this.saveSectionUrl, newSection, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async getInspectionList() {
        try {
            const response = await axios.get(this.inspectionUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getSpecificInspDetails(inspectionTypeId) {
        try {
            const response = await axios.get(this.getSpecificInspectionUrl + `/${inspectionTypeId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getQuesCategoryList() {
        try {
            const response = await axios.get(this.getQuesCategoryUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getQuestionsByCategory(id) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    categoryId:id,
                }
            };
            const response = await axios.get(this.getQuesByCategoryUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getQuesList() {
        try {
            const response = await axios.get(this.getQuesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteInspType(inspectionTypeId) {
        try {
            const response = await axios.delete(this.deleteInspUrl+ `/${inspectionTypeId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteSubSection(subSectionId) {
        try {
            const response = await axios.delete(this.deleteSubSectionUrl+ `/${subSectionId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async deleteComponent(inspectionTypeId,componentId) {
        try {
            const response = await axios.delete(this.deleteCompUrl + `/${inspectionTypeId}` +`/${componentId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async deleteSection(componentId,sectionId) {
        try {
            const response = await axios.delete(this.deleteSectionUrl + `/${componentId}`+`/${sectionId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getAqlList() {
        try {
            const response = await axios.get(this.getAqlUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getDefectsList() {
        try {
            const response = await axios.get(this.getDefectsUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getProductionProcesses(){
        try {
            const response = await axios.get(this.getProductionProcessesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
}
export default InspectionService;