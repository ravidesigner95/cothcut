import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { withTranslation, Trans } from "react-i18next";
import { ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import logo from '../../../assets/img/brand/logo.png';
import engicon from '../../../assets/img/eng-icon.png';
import hindiicon from '../../../assets/img/hindi-icon.png'; 
import ResetPasswordModel from './ResetPasswordModel'
import ResetPasswordService from './ResetPasswordService';
class ResetPassword extends Component {
 
  constructor(props,) {
    super(props); 
    this.initialErrorStatus = {
      updatedPassword: false,
      confirmPassword: false,
    
  };
    this.resetPasswordService = new ResetPasswordService();
    
    this.state = { 
      model: new ResetPasswordModel('', '' , ''),
      errorObject: this.initialErrorStatus,

      validationErrorCss: "custom-error",

      isEdit: false,
        param:"requestParam="
     }
  }  
  
  
  handleChange = (event) => {
    let that = this;
    that.setState({ model: { ...that.state.model, [event.target.id]: event.target.value }, errorObject: { ...this.state.errorObject, [event.target.id]: false } })
    

}
async changePassword(model){
 // this.state.model.emailOrUsernameOrDate = window.location.href.substring(window.location.href.lastIndexOf(this.state.param) + this.state.param.length, window.location.href.length)
  if (((this.state.model.updatedPassword != undefined) && ((this.state.model.updatedPassword != ''))) && ((model.emailOrUsernameOrDate != undefined) && ((model.emailOrUsernameOrDate != '')))) {
    const response = await this.resetPasswordService.changePassword(model)

    if ((response == "Password Changed Sucessfully")) {
      alert("Password Successfully Updated")
    } else {
     alert("Something went wrong")
    }

  }
  this.setState({  model: new ResetPasswordModel('', '' , ''), })

}

resetPassword = (event) => {
  let model = Object.assign({}, this.state.model);
  model.emailOrUsernameOrDate = window.location.href.substring(window.location.href.lastIndexOf(this.state.param) + this.state.param.length, window.location.href.length)
  if (this.isValidate(this.state.model)) {
      if (!this.state.isEdit) {
          this.setState({ isDisabled: true }, () => {
              this.changePassword(model)
             
          })
      } else {
          this.editLine(model);
      }
  }

}

isValidate(model) {
  let isValidate = true;
  let password = model.updatedPassword
  let pass2 = model.confirmPassword
  let passw = /^\w{7,14}$/;
  let errorObj = Object.assign({}, this.state.errorObject);

  if (model.updatedPassword.length<7 || model.updatedPassword.length>14) {
    errorObj.updatedPassword = true;
    isValidate = false;
}
if (model.password !== null && password !== pass2) {
    errorObj.confirmPassword = true;
    isValidate = false;
}
this.setState({ errorObject: errorObj })
return isValidate;

}

   render() {
    const { t, i18n } = this.props;

    const changeLanguage = lng => {
      i18n.changeLanguage(lng);
    };
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <div className="mb-1">
                <a className="btn-icon" onClick={() => changeLanguage("de")}><img src={engicon} /></a>
                <a className="btn-icon" onClick={() => changeLanguage("en")} ><img src={hindiicon} /></a>
              </div>
              <CardGroup>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>

                  <CardBody className="text-center">
                    <div>
                      <img src={logo} />
                      <p className="text-muted">
                      </p>
                      <h2>MOEP</h2>
                      <h2>Line Management System</h2>
                    </div>
                  </CardBody>
                </Card>
                <Card className="p-4 login-mobile">
                  <CardBody>
                    <Form>
                       <img src={logo} className="d-lg-none mb-3" />
                      {/* <button onClick={() => changeLanguage("de")}>English</button>
                    <button onClick={() => changeLanguage("en")}>Hindi</button> */}
                      <h2>{t("cpm.resetpassword")}</h2>
                      {/* <h1>Login</h1> */}
                      {/* <p className="text-muted">Sign In to your account</p> */}<p> </p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-key"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        {/* <Input  type="password" placeholder={t("cpm.password")} onKeyPress={this.handleKeyPress} autoComplete="off" id="updatedPassword" value={this.state.model.updatedPassword} onChange={this.handleChange} /> */}
                        <div hidden={this.state.isEdit} className={this.state.errorObject.updatedPassword == true ? this.state.validationErrorCss : "required-field"}>
                        <Input type="password" placeholder={t("cpm.password")} onKeyPress={this.handleKeyPress} autoComplete="off" id="updatedPassword" value={this.state.model.updatedPassword} onChange={this.handleChange} />
                        <span hidden={!this.state.errorObject.updatedPassword} >{t("cpm.lm.line.validation.password")}</span>
                        </div>
                      
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-key"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                       
                        {/* <Input  type="password" placeholder={t("cpm.confirmpassword")} onKeyPress={this.handleKeyPress} autoComplete="off" id="confirmPassword" value={this.state.model.confirmPassword} onChange={this.handleChange} /> */}
                        <div hidden={this.state.isEdit} className={this.state.errorObject.confirmPassword == true ? this.state.validationErrorCss : "required-field"}>
                        <Input type="password" placeholder={t("cpm.confirmpassword")} onKeyPress={this.handleKeyPress} autoComplete="off" id="confirmPassword" value={this.state.model.confirmPassword} onChange={this.handleChange} />
                        <span hidden={!this.state.errorObject.confirmPassword} >{t("cpm.lm.line.validation.confPassword")}</span>
                        </div>
                      </InputGroup> 
                      <Row>
                        <Col xs="6">
                          <Button color="primary" id="savepassword" onClick={e => this.resetPassword(e)}>{t("cpm.savepassword")}</Button>
                        </Col>
                        <Col xs="6" className="text-right"> 
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>

              </CardGroup>
            </Col>
          </Row>
        </Container> 
        </div>

    );
  }
}
export default withTranslation("translations")(ResetPassword); 
