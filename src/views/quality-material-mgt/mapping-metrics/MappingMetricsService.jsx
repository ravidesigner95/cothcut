import axios from 'axios';
class MappingMetricsService {
    constructor() { }
    
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    orderUrl=process.env.REACT_APP_BASEURL_DASHBOARD+`/orderservice`
    qualityServiceUrl = process.env.REACT_APP_BASEURL_QUALITY;
    plantListUrl = this.baseUrl + `/master/plant`;
    // unitListUrl = this.plantListUrl;
    categoryUrl = this.baseUrl + `/master/category`;
    allLinesUrl = this.baseUrl + `/master/lines`;
    userUrl = this.qualityServiceUrl + this.version+ `/user`;
    poGrpUrl = this.qualityServiceUrl  + this.version+`/bt/getPoGroup`;
    poTypesUrl = this.qualityServiceUrl + this.version+ `/bt/getPoTypes`;
    deptTypesUrl = this.qualityServiceUrl + this.version+ `/bt/getProcessFlow`;
    btProcessUrl = this.qualityServiceUrl + this.version+`/bt/getPoBtProcess`;
    saveBtProcessUrl = this.qualityServiceUrl + this.version+ `/bt/saveBtProcess`;
    getQaMetrixUrl = this.qualityServiceUrl + this.version+ `/bt/getQualityAsigneeMapping`;
    saveQaMetrixUrl = this.qualityServiceUrl + this.version+ `/bt/saveQualityAsigneeMapping`;
    deleteQaMetrixUrl = this.qualityServiceUrl + this.version+ `/bt/deleteQualityAssigneeMapping`;

    // buyerUrl='http://localhost:8081/v1/order/buyer'
    // buyerUrl=this.orderUrl+ this.version+`/order/buyer`
    
    rolesUrl=this.baseUrl+`/roles`
    buyerUrl = this.qualityServiceUrl + `/inspection/getBuyer`;
    materialTypeUrl = this.qualityServiceUrl + `/inspection/getInspectionsLot`;
    unitListUrl = this.baseUrl + `/master/unit`;
    inspectionTypeUrl = this.qualityServiceUrl + '/rules/getAll'




    

    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    //---------------------------------------API-CALLS-----------------------------------------//

    async getPlantList() {
        try {
            const response = await axios.get(this.plantListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getBuyerList() {
        try {
            const response = await axios.get(this.buyerUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnitList() {
        try {
            const response = await axios.get(this.unitListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getRolesList() {
        try {
            const response = await axios.get(this.rolesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getLinesList() {
        try {
            const response = await axios.get(this.allLinesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUserList() {
        try {
            const response = await axios.get(this.userUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getMaterialTypeList() {
        try {
            const response = await axios.get(this.materialTypeUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getCategory() {
        try {
            const response = await axios.get(this.categoryUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getPoGrpList() {
        try {
            const response = await axios.get(this.poGrpUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getPoTypesList() {
        try {
            const response = await axios.get(this.poTypesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getDeptTypesList() {
        try {
            const response = await axios.get(this.deptTypesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getBtProcess() {
        try {
            const response = await axios.get(this.btProcessUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async saveBtProcess(newProcess) {
        try {
            const response = await axios.post(this.saveBtProcessUrl, newProcess, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async getQaMetrixProcess() {
        try {
            const response = await axios.get(this.getQaMetrixUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async saveQaMetrixProcess(newProcess) {
        try {
            const response = await axios.post(this.saveQaMetrixUrl, newProcess, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async getInspectionTypeList(){
        try {
            const response = await axios.get(this.inspectionTypeUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteQaMapping(id) {
        try {
        const config = {
                headers: this.options.headers,
                params: {
                    qualityAssigneeMappingId: id,
                   
                }
            };
            return await axios.delete(this.deleteQaMetrixUrl, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
}
export default MappingMetricsService;