import React from 'react';
import ReactDOM from 'react-dom';
import Noaccess from './ResetPassword';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Noaccess />, div);
  ReactDOM.unmountComponentAtNode(div);
});
