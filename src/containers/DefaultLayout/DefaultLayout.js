import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Container } from 'reactstrap';
import axios from 'axios'; 
import {
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppBreadcrumb2 as AppBreadcrumb,
  AppSidebarNav2 as AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
//import navigation from '../../_nav';
// routes config
import routes from '../../routes';
const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));



class DefaultLayout extends Component {
  constructor(props) {
    super(...arguments);
    // this.values=this.props.history.nav;
    this.homeprops =this.props
    this.values = JSON.parse(localStorage.getItem("NAV"))
  }

  componentDidMount(){
   
  }
  

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  signOut = (getargs,interval)=> {
    clearInterval(interval)
    // let acessTokken = localStorage.getItem('ACCESS_TOKEN')
    if(getargs === 'menual'){
      localStorage.removeItem("ACCESS_PATH")
    }
    if(getargs === 'timefun'){
      localStorage.setItem("ACCESS_PATH", this.props.history.location.pathname);
    }
    // if(getargs === 'fne'){
    //   localStorage.removeItem("ACCESS_PATH")
    // }
    localStorage.removeItem("ACCESS_TOKEN")
    localStorage.removeItem("USER_NAME")
    // sessionStorage.removeItem("headerToken");
    // localStorage.setItem("ACCESS_PATH", this.props.history.location.pathname);
    // if(e){
    //   e.preventDefault()
    // }
    this.props.history.push('/login')
  }

  render() {
    return (
      <div className="app">
        <div className="laoderOverlay" id="pageloader"><div className="loader"></div></div>
        <AppHeader fixed>
          <Suspense fallback={this.loading()}>
            <DefaultHeader propsData={this.props} onLogout={this.signOut} />
          </Suspense>
        </AppHeader>
      
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
              <AppSidebarNav navConfig={this.values} {...this.props} router={router} />
              <div className="left-nav-version">Version: 4.0</div>
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          {
            localStorage.getItem('ACCESS_TOKEN') !== null &&
            <main className="main">
            <AppBreadcrumb className="float-right" appRoutes={routes} router={router} />
              <Container fluid>
                <Suspense fallback={this.loading()}>
                  <Switch>
                    {routes.map((route, idx) => {
                      return route.component ? (
                        <Route
                          key={idx}
                          path={route.path}
                          exact={route.exact}
                          name={route.name}
                          render={props => (
                            <route.component {...props} />
                          )} />
                      ) : (null);
                    })}
                    <Redirect from="/" to="/home" />
                  </Switch>
                </Suspense>
              </Container>
            </main>
          }
          
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
