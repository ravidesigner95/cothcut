export class QualityMetrixModel {
    constructor(id, lineID, lineName, unitId, unitName, plantName, plantId, role, materialType, assigneeUserId, assigneeName, buyer, buyerId, 
        productCategory, productCategoryId, inspectionType, inspectionTypeKey) {
        this.id = id;
        this.lineID = lineID;
        this.lineName = lineName;
        this.unitId = unitId;
        this.unitName = unitName;
        this.plantName = plantName;
        this.plantId = plantId;
        this.role = role;
        this.materialType = materialType;
        this.assigneeUserId = assigneeUserId;
        this.assigneeName = assigneeName;
        this.buyer = buyer;
        this.buyerId = buyerId;
        this.productCategory = productCategory;
        this.productCategoryId = productCategoryId;
        this.inspectionType = inspectionType;
        this.inspectionTypeKey = inspectionTypeKey;
    }
    id = null;

    lineID = null;

    lineName = null;

    unitId = null;

    unitName = null;

    plantId = null;

    plantName = null;

    role = null;

    assigneeUserId = null;

    assigneeName = null;

    buyer = null;

    buyerId = null;

    productCategory = null;

    productCategoryId = null;

    materialType = null;

    inspectionType = null;

    inspectionTypeKey = null;
}