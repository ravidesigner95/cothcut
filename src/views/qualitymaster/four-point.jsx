import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { GridComponent, ColumnsDirective, ColumnDirective, Filter, Inject, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, ExcelExport, ForeignKey, Reorder } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent, MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { PropertyPane } from '../../assets/data/purchaseOrder';
import { SwitchComponent, CheckBoxComponent } from '@syncfusion/ej2-react-buttons';
import { rippleMouseHandler, ChangeEventArgs } from '@syncfusion/ej2-buttons';
import { data } from '../../assets/data/data';
import { RadioButtonComponent, ButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { sampleData } from '../../assets/data/inspection';
import { SampleBase } from '../../assets/data/sample-base';
import { rightToLeft, rippleEffect } from '@syncfusion/ej2-base';
import QualityMasterService from './QualityMasterService';
import { AppConfig } from '../../appConfig/appConfig';
import FourPointModel from './FourPointModel';

class FourPointMasterComponent extends React.PureComponent {
  constructor() {
    super(...arguments);
    this.QualityMasterService = new QualityMasterService();

    this.state = {
      toasterPosition: { X: 'Right', Y: 'Bottom' },  
      toolbarOptions: ['Add', 'Delete', 'Update', 'Cancel'],
      fourPointDataSource: []

    };
  }

  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }

  componentDidMount() {
    this.getFourPointList();
  }

  actionBeginFourPoint = (args) => {
    let that = this;
    if (args.requestType === "save") {
      if(args.data.minimumLength > args.data.maximumLength){
        this.toastObj.show(this.createToaster("Minimum Length should be less than Maximum Length", "Information"));
        // this.getFourPointList();
        args.cancel = true;
      }
      if(args.data.minimumLength == null || args.data.maximumLength == null || args.data.penaltyPoint == null){
        this.toastObj.show(this.createToaster("Please fill all the fields", "Information"));
        args.cancel = true;
      }
      else {
      let fourPointData = new FourPointModel(null, args.data.minimumLength, args.data.maximumLength, args.data.penaltyPoint);
      that.saveFourPoint(fourPointData);
      }
    }
    else if (args.requestType === "delete") {
      that.deleteFourPoint(args.data[0].id);
    }
  }



  async getFourPointList() {

    let response = (await this.QualityMasterService.getAllFourPoint())
    if ((response != undefined) && (response.status == 200))
      this.setState({ fourPointDataSource: response.data.data }, () => {
      })
  }

  async saveFourPoint(fourPoint) {
    let response = (await this.QualityMasterService.saveFourPoint(fourPoint))
    if ((response != undefined) && (response.status == 200)) {
      this.toastObj.show(this.createToaster("Saved successfully!!", "Success"));
    }
  }

  async deleteFourPoint(id) {
    let response = (await this.QualityMasterService.deleteFourPoint(id))
    if ((response != undefined) && (response.status == 200)) {
      this.toastObj.show(this.createToaster("Deleted successfully!!", "Success"));
      this.getFourPointList();
    }
  }


  render() {
    return (<div className='control-pane' id='targetElementQuest'>
      <div className='control-section col-lg-12 defaultDialogComponent dialog-target'>
        <Row >
          <Col sm='12'>
            <GridComponent
              enableHover={true}
              allowFiltering={false}
              allowSorting={true}
              allowSelection={true}
              allowResizing={true}
              rowHeight={25}
              allowPaging={true}
              allowTextWrap={true}
              height={AppConfig.gridHeight}
              filterSettings={AppConfig.FilterSettingsModel}
              editSettings={AppConfig.editSettings}
              pageSettings={AppConfig.pageSettings}
              dataSource={this.state.fourPointDataSource}
              toolbar={this.state.toolbarOptions}
              ref={grid => this.gridInstance = grid}
              actionBegin={this.actionBeginFourPoint}>
              <ColumnsDirective>
                <ColumnDirective field='minimumLength' headerText='Minimum Length (inches)' textAlign='left' editType='numericedit' width='100' validationRules={{ required: true , number:true }} edit={this.minLength}></ColumnDirective>
                <ColumnDirective field='maximumLength' headerText='Maximum Length (inches)' textAlign='left' editType='numericedit' width='100'validationRules={{ required: true , number:true }} edit={this.maxLength}></ColumnDirective>
                <ColumnDirective field='penaltyPoint' headerText='Penalty' textAlign='left' allowEditing='false' width='100' validationRules={{ required: true , number:true }} edit={this.penalty}></ColumnDirective>
              </ColumnsDirective>
              <Inject services={[Page, Toolbar, Edit]} />
            </GridComponent>
          </Col>
        </Row>
      </div>
      <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
    </div>);

  }
}

export default FourPointMasterComponent;
