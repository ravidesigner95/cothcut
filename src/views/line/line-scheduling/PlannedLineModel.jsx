class PlannedLineModel {
    constructor(id, fmId, fmName, qaId, qaName, apmId, saidId, plantId, unitId, prodLineId, mechandiserId, productCategoryId, buyerId, plantName, unitName,
        lineName, merchandiserName, productCategoryName, buyerName, apm, sam, lineItemNo, totalQtyAllocatedInLine, totalQtyToProduced,
        startDateTime, endDateTime, scheduleStartDateTime, scheduleEndDateTime, noOfMachineToUse, estimatedWorkingDays, status, scheduledQty, buyerPo, shipToDate, transferTo, plannedInfoId, unScheduledQty,buyerPoQty,leftPoQty) {
        this.saidId = saidId;
        this.fmId = fmId;
        this.fmName = fmName;
        this.qaId = qaId;
        this.qaName = qaName;
        this.apmId = apmId;
        this.lineName = lineName;
        this.id = id;
        this.plantId = plantId;
        this.unitId = unitId;
        this.prodLineId = prodLineId;
        this.mechandiserId = mechandiserId;
        this.productCategoryId = productCategoryId;
        this.buyerId = buyerId;
        this.plantName = plantName;
        this.unitName = unitName;
        this.merchandiserName = merchandiserName;
        this.productCategoryName = productCategoryName;
        this.buyerName = buyerName;
        this.apm = apm;
        this.sam = sam;
        this.lineItemNo = lineItemNo;
        this.totalQtyAllocatedInLine = totalQtyAllocatedInLine;
        this.totalQtyToProduced = totalQtyToProduced;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.scheduleStartDateTime = scheduleStartDateTime;
        this.scheduleEndDateTime = scheduleEndDateTime;
        this.noOfMachineToUse = noOfMachineToUse;
        this.estimatedWorkingDays = estimatedWorkingDays;
        this.status = status;
        this.scheduledQty = scheduledQty;
        this.buyerPo = buyerPo;
        this.shipToDate = shipToDate;
        this.transferTo = transferTo;
        this.plannedInfoId = plannedInfoId;
        this.unScheduledQty = unScheduledQty;
        this.buyerPoQty = buyerPoQty;
        this.leftPoQty=leftPoQty;
    }


    id = null;
    fmId = null;
    fmName = null;
    qaId = null;
    qaName = null;
    apmId = null;
    saidId = null;
    plantId = null;
    unitId = null;
    prodLineId = null;
    mechandiserId = null;
    productCategoryId = null;
    buyerId = null;
    plantName = "";
    unitName = "";
    lineName = "";
    merchandiserName = "";
    productCategoryName = "";
    buyerName = "";
    apm = null;
    sam = null;
    lineItemNo = null;
    totalQtyAllocatedInLine = null;
    totalQtyToProduced = null;
    startDateTime = new Date();
    endDateTime = new Date();
    scheduleStartDateTime = new Date();
    scheduleEndDateTime = new Date();
    noOfMachineToUse = null;
    estimatedWorkingDays = null;
    status = null;
    scheduledQty = null;
    buyerPo = null;
    shipToDate = null;
    transferTo = null;
    plannedInfoId = null;
    unScheduledQty = null;
    leftPoQty= null;
}
export default PlannedLineModel;