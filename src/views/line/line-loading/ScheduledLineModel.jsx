class ScheduledLineModel {
    constructor(id, fmName, qaName, apmId, prodLineId, unitId, unitName, buyerPoNo, saidId, noOfMachineToUse, workingHour, scheduleStartDateTime, scheduleEndDateTime, scheduleQtyForProduction,
        unLoadedQty, totalQtyToProduced, fmId, apm, sam, qaId, lineName, lineItemNo, lineItemDesc, supervisor) {
        this.saidId = saidId;
        this.fmName = fmName;
        this.qaName = qaName;
        this.prodLineId = prodLineId;
        this.lineName = lineName;
        this.unitName = unitName;
        this.id = id;
        this.unitId = unitId;
        this.prodLineId = prodLineId;
        this.buyerPoNo = buyerPoNo;
        this.apm = apm;
        this.apmId = apmId;
        this.sam = sam;
        this.fmId = fmId;
        this.qaId = qaId;
        this.lineItemNo = lineItemNo;
        this.scheduleQtyForProduction = scheduleQtyForProduction;
        this.scheduleStartDateTime = scheduleStartDateTime;
        this.scheduleEndDateTime = scheduleEndDateTime;
        this.noOfMachineToUse = noOfMachineToUse;
        this.workingHour = workingHour;
        this.unLoadedQty = unLoadedQty;
        this.totalQtyToProduced = totalQtyToProduced;
        this.lineItemDesc = lineItemDesc;
        this.supervisor = supervisor;
    }


    id = null;
    fmName = null;
    qaName = null;
    apmId = null;
    saidId = null;
    unitId = null;
    prodLineId = null;
    unitName = null;
    apm = null;
    sam = null;
    fmId = null;
    qaId = null;
    lineItemNo = null;
    totalQtyToProduced = null;
    scheduleStartDateTime = new Date();
    scheduleEndDateTime = new Date();
    noOfMachineToUse = null;
    scheduleQtyForProduction = null;
    buyerPoNo = null;
    unLoadedQty = null;
    workingHour = null;
    lineItemDesc = null;
    supervisor = null;
}
export default ScheduledLineModel;