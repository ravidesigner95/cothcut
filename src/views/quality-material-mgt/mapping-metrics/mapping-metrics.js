import React from 'react';
import { withTranslation } from "react-i18next";
import { DateRangePickerComponent } from '@syncfusion/ej2-react-calendars';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import {
  Button, CardGroup, Col, Card,
  CardBody, CardText, CardFooter, CardHeader, CardTitle, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row
} from 'reactstrap';
import { AccordionComponent } from '@syncfusion/ej2-react-navigations';
import { GridComponent, ColumnsDirective, Toolbar, ColumnDirective, Inject, Selection, DetailRow, Page, Sort, Resize, Freeze, Edit, CommandColumn, Filter, Reorder } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent, DropDownList } from '@syncfusion/ej2-react-dropdowns';
import { polistData, qualityAssigneeGridData, potypeData, poGroupslistData, plantListData, departmenttypeData, mappingDepartmentGridData, addmappingDepartmentGridData } from '../../../assets/data/purchaseOrder';
import MappingMetricsService from './MappingMetricsService';
import BtProcessModel from './BtProcessModel';
import BtProcessFlowDTOSModel from './BtProcessFlowDTOSModel';
import { ToastComponent } from '@syncfusion/ej2-react-notifications';
import { Query, DataManager } from '@syncfusion/ej2-data';
import UnitModel from './UnitModel';
import { QualityMetrixModel } from './QualityMetrixModel';
import { UserModel } from './UserModel';
import { MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import errorMessages from '../../errorHandler/errorMessages';
class MappingMetrics extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    this.MappingMetricsService = new MappingMetricsService();


    this.plantData = [
      { plantName: 'Normal', plantId: '1' },
      { plantName: 'High', plantId: '2' }
    ];
    this.unitData = [
      { unitValue: 2, plantId: '1', unitId: 2 },
      { unitValue: 3, plantId: '1', unitId: 3 },
      { unitValue: 4, plantId: '1', unitId: 4 },
      { unitValue: 11, plantId: '2', unitId: 11 },
      { unitValue: 15, plantId: '2', unitId: 15 },
      { unitValue: 20, plantId: '2', unitId: 20 }
    ];

    this.state = {
      FilterSettingsModel: { type: 'Menu' },
      IFilter: { type: 'CheckBox' },
      toolbarOptions: ['Add', 'Delete', 'Update', 'Cancel'],
      editSettings: { allowEditing: true, allowAdding: true, allowDeleting: true, mode: 'Normal' },
      validationRules: { required: true },
      toasterPosition: { X: 'Right', Y: 'Bottom' },  
      poGroupslist: poGroupslistData,
      poGroupslistfields: { text: 'pogroups', value: 'id' },
      potype: potypeData,
      potypefields: { text: 'potype', value: 'id' },
      plantList: plantListData,
      plantListfields: { text: 'plant', value: 'id' },
      plants: [],
      units: [],
      category: [],
      lines: [],
      users: [],
      userList: [],
      poGrp: [],
      buyers: [],
      roles: [],
      materialTypes: [],
      poTypes: [],
      deptTypes: [],
      qaMetrix: [],
      inspectionTypes:[],
      uniquePoType: [],
      uniquePoGroupName: [],
      uniquePlantName: [],
      selectedPoType: [],
      selectedPoGroupName: [],
      selectedPlantName: [],
      selectedQaPlantName: null,
      model: new BtProcessModel(null, null, null, null, null, null, null, null, null, null, null, []),
      assigneeModel: new BtProcessFlowDTOSModel(null, null, null, null, null),
      // btProcesses:[],
      departmenttype: departmenttypeData,
      departmenttypefields: { text: 'dept', value: 'id' },
      filterBox: true,
      mappingDepartmentGrid: mappingDepartmentGridData,
      addmappingDepartmentGrid: addmappingDepartmentGridData,
      qualityAssigneeGrid: qualityAssigneeGridData,
      showDialogBox: false,
      showDialogBoxHeading: 'Add Department',


      toasterMessage: [
        { title: 'Warning!', content: 'Changes Discarded !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
      ],
      // windowsHeight: window.innerHeight/3 + 'px',
      // windowsHeightsecond: window.innerHeight/3 + 'px',
      windowsHeight:'auto',
      windowsHeightsecond:'auto',
    }
  }
  filterBoxOpen = () => {
    this.setState({
      filterBox: !this.state.filterBox
    });
  }
  addMapping = () => {

  }

  componentDidMount() {
    this.getBtProcess();
    this.getPlantList();
    this.getUserList();
    this.getPoGrp();
    this.getPoTypes();
    this.getDeptTypes();
    this.getUnitList();
    this.getCategoryList();
    this.getLines();
    this.getMaterialTypeTypes();
    this.getQaMetrixList();
    this.getBuyerList();
    this.getRolesList();
    this.getInspectionTypes();


    document.addEventListener("keydown", this.escFunction, false);
    console.log(React.version);
  }


  //////////////////////////////////////////////////////////////API Calls///////////////////////////////////////////////////////////////////

  async getPlantList() {

    let response = (await this.MappingMetricsService.getPlantList())
    if ((response != undefined) && (response.data.status == "success"))
    
      this.setState({ plants: response.data.data }, () => {
      })
  }

  async getBuyerList() {

    let response = (await this.MappingMetricsService.getBuyerList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ buyers: response.data.data }, () => {
      })
  }

  async getInspectionTypes() {

    let response = (await this.MappingMetricsService.getInspectionTypeList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ inspectionTypes: response.data.data }, () => {
      })
  }


  async getCategoryList() {

    let response = (await this.MappingMetricsService.getCategory())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ category: response.data.data }, () => {
      })
  }

  async getLines() {

    let response = (await this.MappingMetricsService.getLinesList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ lines: response.data.data }, () => {
      })
  }

  async getUnitList() {
    let response = (await this.MappingMetricsService.getUnitList())
    if ((response != undefined) && (response.data.status == "success")) {
      this.setState({ units: response.data.data }, () => {
        let units = [];
        response.data.data.map(item => {
          units.push(new UnitModel(item.plant.id, item.id, item.name))
        })
        this.setState({ units });
      })
    }

  }

  async getUserList() {

    let response = (await this.MappingMetricsService.getUserList())
    if ((response != undefined) && (response.status == 200)) {
      let usersInfo = [];
      response.data.map(item => {
        item.roles.map(role => {
          usersInfo.push(new UserModel(role, item.radiantUserId, item.firstName +" "+ item.lastName))
        })
      })
      this.setState({ users: response.data, userList: usersInfo }, () => { })
    }
  }

  async getPoGrp() {

    let response = (await this.MappingMetricsService.getPoGrpList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ poGrp: response.data.data }, () => {
      })
  }
  async getPoTypes() {

    let response = (await this.MappingMetricsService.getPoTypesList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ poTypes: response.data.data }, () => {
      })
  }

  async getMaterialTypeTypes() {

    let response = (await this.MappingMetricsService.getMaterialTypeList())
    if ((response != undefined) && (response.data.status == "success")) {
      let uniqueType = response.data.data.filter((value, index, array) => array.findIndex(t => (t.type === value.type)) === index);
      this.setState({ materialTypes: uniqueType }, () => {
      })
    }
  }

  async getDeptTypes() {

    let response = (await this.MappingMetricsService.getDeptTypesList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ deptTypes: response.data.data }, () => {
      })
  }

  async getQaMetrixList() {

    let response = (await this.MappingMetricsService.getQaMetrixProcess())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ qaMetrix: response.data.data }, () => {
      })
  }

  async getRolesList() {

    let response = (await this.MappingMetricsService.getRolesList())
    if ((response != undefined) && (response.data.status == "success"))
      this.setState({ roles: response.data.data }, () => {
      })
  }


  async getBtProcess() {
    // global.showLoader();
    let response = (await this.MappingMetricsService.getBtProcess())
    if ((response != undefined) && (response.data.status == "success")) {
      // global.hideLoader();
      let btProcesses = [];
      response.data.data.map(item => {
        btProcesses.push(new BtProcessModel(item.id, item.poTypeId, item.poTypeName, item.poGroupName, item.poGroupId, item.plantName, item.plantId, null, null, null, null, null, item.POBTProcessFlowDTOS))
      })
      let uniquePoTypeName = btProcesses.filter((value, index, array) => array.findIndex(t => (t.poTypeName === value.poTypeName)) === index);
      let uniquePoGroupName = btProcesses.filter((value, index, array) => array.findIndex(t => (t.poGroupName === value.poGroupName)) === index);
      let uniquePlantName = btProcesses.filter((value, index, array) => array.findIndex(t => (t.plantName === value.plantName)) === index);


      this.setState({ btProcesses, uniquePoType: uniquePoTypeName, uniquePlantName: uniquePlantName, uniquePoGroupName: uniquePoGroupName })
    }

  }

  async saveBtProcess(newProcess) {
    const { t, i18n } = this.props;
    let response = (await this.MappingMetricsService.saveBtProcess(newProcess))
    if ((response != undefined) && (response.data.status == "success")) {
      if(response.data.message.includes("Duplicate")){
        this.toastObj.show(this.createToaster(response.data.message, "Error"));
      }
      else{
      this.toastObj.show(this.createToaster("Po Bt process saved successfully!!", "Success"));
      }
      this.getBtProcess();
    }else if ((response != undefined) && (response.data.status == "fail")) {
      this.toastObj.show(this.createToaster1(t(errorMessages[response.data.data.errorCode])));
    }else{
      this.toastObj.show(this.createToaster("Something went wrong", "Error")); 
     }
  }


  createToaster1(message) {
    return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
  }
  async saveQaProcess(newProcess) {
    let response = (await this.MappingMetricsService.saveQaMetrixProcess(newProcess))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster("Quality Metrix  saved successfully!!", "Success"));
      this.getQaMetrixList();
    }
  }

  async deleteQaMapping(mappingId) {
    let response = (await this.MappingMetricsService.deleteQaMapping(mappingId))
    if ((response != undefined) && (response.data.status == "success")) {
      this.toastObj.show(this.createToaster("Quality Mapping deleted successfully!!", "Success"));
      this.getQaMetrixList();
    }
  }

  //############ ON-CHANGE-HANDLER  #############//

  onChangePoType = (event) => {
    if (event.value.length > 0) {
      global.showLoader();
      this.setState({ selectedPoType: event.value })
      // remove all load when e is undefined.
    }
  }

  onChangePlant = (event) => {
    if (event.e != undefined) {
      this.setState({ units: [] }, () => {
        let plantObj = this.state.plants.filter(function (item) {
          return item.name == event.value;
        })
        global.showLoader();
        // this.getUnitList(plantObj[0].id);

      })
    }
  }


  plant = {
    create: () => {
      this.plantElem = document.createElement('input');
      return this.plantElem;
    },
    destroy: () => {
      this.plantObj.destroy();
    },
    read: () => {
      return this.plantObj.text;
    },
    write: () => {
      this.plantObj = new DropDownList({
        change: () => {
          this.unitObj.enabled = true;
          const tempQuery = new Query().where('id', 'equal', this.plantObj.value);
          this.unitObj.query = tempQuery;
          this.unitObj.text = '';
          this.unitObj.dataBind();
        },
        dataSource: new DataManager(this.state.plants),
        fields: { value: 'id', text: 'name' },
        floatLabelType: 'Never',
        placeholder: 'Select a Plant'
      });
      this.plantObj.appendTo(this.plantElem);
    }
  };

  unit = {
    create: () => {
      this.unitElem = document.createElement('input');
      return this.unitElem;
    },
    destroy: () => {
      this.unitObj.destroy();
    },
    read: () => {
      return this.unitObj.text;
    },
    write: () => {
      this.unitObj = new DropDownList({
        change: () => {
          this.lineObj.enabled = true;
          const tempQuery = new Query().where('unitId', 'equal', this.unitObj.value);
          this.lineObj.query = tempQuery;
          this.lineObj.text = '';
          this.lineObj.dataBind();
        },
        dataSource: new DataManager(this.state.units),
        enabled: false,
        fields: { value: 'unitId', text: 'unitName' },
        floatLabelType: 'Never',
        placeholder: 'Select a unit'
      });
      this.unitObj.appendTo(this.unitElem);
    }
  };

  line = {
    create: () => {
      this.lineElem = document.createElement('input');
      return this.lineElem;
    },
    destroy: () => {
      this.lineObj.destroy();
    },
    read: () => {
      return this.lineObj.text;
    },
    write: () => {
      this.lineObj = new DropDownList({
        dataSource: new DataManager(this.state.lines),
        enabled: false,
        fields: { value: 'id', text: 'lineName' },
        floatLabelType: 'Never',
        placeholder: 'Select Line'
      });
      this.lineObj.appendTo(this.lineElem);
    }
  };

  role = {
    create: () => {
      this.roleElem = document.createElement('input');
      return this.roleElem;
    },
    destroy: () => {
      this.roleObj.destroy();
    },
    read: () => {
      return this.roleObj.text;
    },
    write: () => {
      this.roleObj = new DropDownList({
        change: () => {
          this.userObj.enabled = true;
          const tempQuery = new Query().where('role', 'equal', this.roleObj.text);
          this.userObj.query = tempQuery;
          this.userObj.text = '';
          this.userObj.dataBind();
        },
        dataSource: new DataManager(this.state.roles),
        fields: { value: 'id', text: 'name' },
        floatLabelType: 'Never',
        placeholder: 'Select Role'
      });
      this.roleObj.appendTo(this.roleElem);
    }
  };

  user = {
    create: () => {
      this.userElem = document.createElement('input');
      return this.userElem;
    },
    destroy: () => {
      this.userObj.destroy();
    },
    read: () => {
      return this.userObj.text;
    },
    write: () => {
      this.userObj = new DropDownList({

        dataSource: new DataManager(this.state.userList),
        fields: { value: 'radiantUserId', text: 'userName' },
        enabled: false,
        floatLabelType: 'Never',
        placeholder: 'Select UserName'
      });
      this.userObj.appendTo(this.userElem);
    }
  };


  createToaster(message, type) {
    switch (type) {
      case "Success":
        return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
      case "Error":
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
      case "Warning":
        return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
      case "Information":
        return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
    }
  }

  processFlowtemplate = (args) => {
    const processFlowName = []
    for (const [index, value] of args.POBTProcessFlowDTOS.entries()) {
      processFlowName.push(<li key={index}><b>{value.processFlowName}</b><span>{value.assigneeName}</span></li>)
    }
    return (
      <div className='processFlow'>
        <ul>
          {processFlowName}
          {/* <li>
            <b> UD</b>
            <span>Kanhaiya</span>
          </li> */}
          {/* <li>
            <b> Accounts</b>
            <span>Anuj</span>
          </li>
          <li>
            <b> BVT</b>
            <span>Naresh Sharma</span>
          </li>
          <li>
            <b> HOD1</b>
            <span>Ankur</span>
          </li>
          <li>
            <b> HOD2</b>
            <span>Rajeev Malik</span>
          </li>
          <li>
            <b> Audit</b>
            <span>Ravi</span>
          </li>
          <li>
            <b> Mgt</b>
            <span>Arvind</span>
          </li>  */}
        </ul>
      </div>
    )
  }


  showdialog = (event, selectedDialog) => {
    this.setState({ showDialogBox: !this.state.showDialogBox });
    // if (selectedDialog == 'assignee') {
    //   this.setState({ divHidden: 'assigneeBox', showDialogBoxHeading: 'Add Assignee' });
    // }
    // else if (selectedDialog == 'assignTable') {
    //   this.setState({ divHidden: 'assignTableBox', showDialogBoxHeading: 'Add Assignee' });
    // }
  }
  closeDialog = () => {
    this.setState({ showDialogBox: !this.state.showDialogBox });
  }

  ////////////////////////////////////////////////////////////  for process metrix //////////////////////////////////////////////////////////////////////
  editPOTypeTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="poType" allowFiltering={true} dataSource={that.state.poTypes} fields={{ text: 'poDescription', value: 'poDescription' }} change={this.poTypeChange} placeholder="PO Type">
      </DropDownListComponent >)
  }
  editPOGroupsTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="poGroups" allowFiltering={true} dataSource={that.state.poGrp} fields={{ text: 'groupName', value: 'groupName' }} placeholder="PO Group">
      </DropDownListComponent >)
  }
  editPlantTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="plants" allowFiltering={true} dataSource={that.state.plants} fields={{ text: 'name', value: 'name' }} placeholder="Select Plant">
      </DropDownListComponent >
    )
  }
  editDeptTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="department" allowFiltering={true} dataSource={that.state.deptTypes} fields={{ text: 'processName', value: 'processName' }} placeholder="Select Department">
      </DropDownListComponent >)
  }
  editseqTemplate = () => {
    return (
      // <DropDownListComponent  id="seq" allowFiltering={true} dataSource={['1', '2', '3', '4', '5']} placeholder="Add Seq"> 
      // </DropDownListComponent >
      <NumericTextBoxComponent max={20} min={1} placeholder="Add Seq" id={"seq"} format='###.###' decimals={0} floatLabelType="Auto" ></NumericTextBoxComponent>
    )
  }

  editAssigneTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="assigne" allowFiltering={true} dataSource={that.state.users} fields={{ text: 'firstName', value: 'firstName' }} placeholder="Select Assigne">
      </DropDownListComponent >)
  }
  ////////////////////////////////////////////////////////////  for quality metrix //////////////////////////////////////////////////////////////////////

  //    editQaPlantTemplate=()=> {
  //     let that = this;
  //    return ( 
  //      <DropDownListComponent id="plants" allowFiltering={true} dataSource={that.state.plants} fields={{ text: 'name', value: 'name' }} change={this.onChangePlant}  placeholder="Select Plant"> 
  //      </DropDownListComponent >
  //      )
  //  }
  //    editQaUnitTemplate=()=> {
  //   let that = this;
  //  return ( 
  //    <DropDownListComponent id="unit" allowFiltering={true} dataSource={that.state.units} fields={{ text: 'name', value: 'name' }}  placeholder="Select Unit"> 
  //    </DropDownListComponent >
  //    )
  //  }

  //  editQaLineTemplate=()=> {
  //   let that = this;
  //  return ( 
  //    <DropDownListComponent id="line" allowFiltering={true} dataSource={that.state.lines} fields={{ text: 'lineName', value: 'lineName' }}  placeholder="Select Plant"> 
  //    </DropDownListComponent >
  //    )
  // }

  //  editQaRolesTemplate=()=> {
  //   let that = this;
  //  return ( 
  //    <DropDownListComponent id="roles" allowFiltering={true} dataSource={that.state.plants} fields={{ text: 'name', value: 'name' }}  placeholder="Select Plant"> 
  //    </DropDownListComponent >
  //    )
  // }

  editQaProductCategoryTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="productCategory" allowFiltering={true} dataSource={that.state.category} fields={{ text: 'name', value: 'name' }} placeholder="Select Category">
      </DropDownListComponent >
    )
  }

  editQaBuyerTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="buyer" allowFiltering={true} dataSource={that.state.buyers} fields={{ text: 'buyerName', value: 'buyerName' }} placeholder="Select Buyer">
      </DropDownListComponent >
    )
  }

  // editQaUserTemplate=()=> {
  //   let that = this;
  //  return ( 
  //    <DropDownListComponent id="assigneeName" allowFiltering={true} dataSource={that.state.users} fields={{ text: 'firstName', value: 'firstName' }}  placeholder="Select Plant"> 
  //    </DropDownListComponent >
  //    )
  // }

  editQaMaterialTypeTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="materialType" allowFiltering={true} dataSource={that.state.materialTypes} fields={{ text: 'type', value: 'type' }} placeholder="Select Type">
      </DropDownListComponent >
    )
  }

  editInspectionTypeTemplate = () => {
    let that = this;
    return (
      <DropDownListComponent id="inspectionType" allowFiltering={true} dataSource={that.state.inspectionTypes} fields={{ text: 'ruleDescription', value: 'ruleDescription' }} placeholder="Select Inspection Type">
      </DropDownListComponent >
    )
  }
  onChangeFilterMultiSelect = (event) => {
    // if (event.e != undefined && event.e != null)
        this.setState({ [event.element.id]: event.value })
  }

  search = () => {
    let that = this;
    const { selectedPoType, selectedPoGroupName, selectedPlantName } = that.state;
    that.btProcessGridObj.clearFiltering();
    if (selectedPoType.length == 0 && selectedPoGroupName.length==0 && selectedPlantName.length==0) {
      that.toastObj.show({ title: 'Information!', content: "Please select at least one filter", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    } else {
      if (selectedPoType.length>0) {
        that.btProcessGridObj.filterByColumn("poTypeName", "equal", selectedPoType);
      }
      if (selectedPoGroupName.length>0) {
        that.btProcessGridObj.filterByColumn("poGroupName", "equal", selectedPoGroupName);
      }
      if (selectedPlantName.length>0) {
        that.btProcessGridObj.filterByColumn("plantName", "equal", selectedPlantName);
      }
      that.toastObj.show({ title: 'Success!', content: "Filter Applied Successfully!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
    }
  }

  reset =() =>{
    this.setState({ selectedPoType: [] , selectedPoGroupName:[] , selectedPlantName :[] })
    this.btProcessGridObj.clearFiltering();
    this.toastObj.show({ title: 'Success!', content: "Filter Reset Successfully!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
  }
  saveBtMetrix = (args) => {
  
    if (args.requestType === "save") {
      let that = this;
      let poTypeObj = that.state.poTypes.filter(function (item) {
        return item.poDescription == args.data.poType;
      })
      let poGroupsObj = that.state.poGrp.filter(function (item) {
        return item.groupName == args.data.poGroups;
      })
      let plantObj = that.state.plants.filter(function (item) {
        return item.name == args.data.plants;
      })
      let deptTypeObj = that.state.deptTypes.filter(function (item) {
        return item.processName == args.data.department;
      })
      let assigneeObj = that.state.users.filter(function (item) {
        return item.firstName == args.data.assigne;
      })
      // let btProcessFlowDTOS =[];
      // btProcessFlowDTOS.push(new BtProcessFlowDTOSModel(deptTypeObj[0].id,deptTypeObj[0].processName,1234,"Arvind",args.data.seq))
      let updateDataBtProcess = [];
      updateDataBtProcess.push(new BtProcessModel(null, poTypeObj[0].id, poTypeObj[0].poDescription, poGroupsObj[0].groupName, poGroupsObj[0].id, plantObj[0].name, plantObj[0].id, deptTypeObj[0].id, deptTypeObj[0].processName, assigneeObj[0].radiantUserId, assigneeObj[0].firstName, args.data.seq, null))

      this.saveBtProcess(updateDataBtProcess);

    }

  
  }

  saveQaMetrix = (args) => {
    let that = this;
    if (args.requestType === "save") {
     
      let categoryObj = that.state.category.filter(function (item) {
        return item.name == args.data.productCategory;
      })
      let lineObj = that.state.lines.filter(function (item) {
        return item.lineName == args.data.lineName;
      })
      let plantObj = that.state.plants.filter(function (item) {
        return item.name == args.data.plantName;
      })
      let unitObj = that.state.units.filter(function (item) {
        return item.unitName == args.data.unitName && item.id == plantObj[0].id;
      })
      let buyerObj = that.state.buyers.filter(function (item) {
        return item.buyerName == args.data.buyer;
      })
      let assigneeObj = that.state.userList.filter(function (item) {
        return item.userName == args.data.assigneeName;
      })
      let inspectionTypeObj = that.state.inspectionTypes.filter(function (item) {
        return item.ruleDescription == args.data.inspectionType;
      })
      let updateDataQaProcess = [];
      updateDataQaProcess.push(new QualityMetrixModel(null, lineObj.length>0?lineObj[0].id:null, args.data.lineName,unitObj.length>0? unitObj[0].unitId:null, args.data.unitName, args.data.plantName, plantObj.length>0?plantObj[0].id:null, args.data.role,
        args.data.materialType, assigneeObj[0].radiantUserId, args.data.assigneeName, args.data.buyer, buyerObj.length > 0 ? buyerObj[0].buyerId : null, categoryObj.length > 0 ? categoryObj[0].name : null, args.data.productCategory,args.data.inspectionType,inspectionTypeObj[0].ruleKey))

      this.saveQaProcess(updateDataQaProcess);

    }
    else if(args.requestType === "delete"){
      
      that.deleteQaMapping(args.data[0].id);
     }
  }

  render() {
    const { t, i18n } = this.props;
    return (
      <div className="container-fluid-inner billtracking">

        <h1 className='pagesheading'>Mapping Metrics</h1>
        <div className='clearfix'></div>


        {/* -------------------------------------- filter  --------------------------------------  */}
        <a className={this.state.filterBox ? 'filter-icon-open' : 'filter-icon-close'} onClick={this.filterBoxOpen}>Filter <i className='fa  fa-filter'></i></a>
        <div className='filter-dropdown mb-2' hidden={this.state.filterBox}>
          <div className='filter-Selection'>
            {/* <h3>PO Info</h3>   */}
            <Row>
              {/* <Col sm='3'>
                <DropDownListComponent dataSource={this.state.uniquePoType} fields={{ text: 'poTypeName', value: 'poTypeId' }} placeholder={t("cpm.lm.line.load.filter.poType")}
                  color="primary" id="potype" change={this.onChangePoType} allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              <Col sm='3'>  <MultiSelectComponent id="selectedPoType" ref={(scope) => { this.selectedPoTypeObj = scope; }} value={this.state.selectedPoType}
               change={this.onChangeFilterMultiSelect}   dataSource={this.state.uniquePoType} fields={{ text: 'poTypeName', value: 'poTypeName' }}
                placeholder={t("cpm.lm.line.load.filter.poType")} filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder={t("cpm.lm.line.load.filter.poType")} popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              {/* <Col sm='3'>
                <DropDownListComponent dataSource={this.state.uniquePoGroupName} fields={{ text: 'poGroupName', value: 'poGroupId' }} placeholder='PO Group'
                  color="primary" id="pio" allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              <Col sm='3'>  <MultiSelectComponent id="selectedPoGroupName" ref={(scope) => { this.selectedPoGroupObj = scope; }} value={this.state.selectedPoGroupName}
               change={this.onChangeFilterMultiSelect}  dataSource={this.state.uniquePoGroupName} fields={{ text: 'poGroupName', value: 'poGroupName' }}
                placeholder='PO Group' filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder='PO Group' popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              {/* <Col sm='3'>
                <DropDownListComponent dataSource={this.state.uniquePlantName} fields={{ text: 'plantName', value: 'plantId' }} placeholder='Plant' color="primary" id="plantList" allowFiltering={true} floatLabelType="Auto" />
              </Col> */}
              <Col sm='3'>  <MultiSelectComponent id="selectedPlantName" ref={(scope) => { this.selectedPlantObj = scope; }} value={this.state.selectedPlantName}
               change={this.onChangeFilterMultiSelect}  dataSource={this.state.uniquePlantName} fields={{ text: 'plantName', value: 'plantName' }}
                placeholder='Plant' filterType="Contains" mode="CheckBox" showSelectAll={true} sortOrder="Ascending"
                showDropDownIcon={true} filterBarPlaceholder='Plant' popupHeight="300px">
                <Inject services={[CheckBoxSelection]} />
              </MultiSelectComponent></Col>
              {/* <Col sm='3'>
                <DropDownListComponent dataSource={this.state.departmenttype} fields={this.state.departmenttypefields} placeholder='Department Type' allowFiltering={true} color="primary" id="departmenttype" floatLabelType="Auto" />
              </Col> */}
              <Col sm='3'>  <Button color="primary" size="sm" className='mr-2' onClick={this.search} id="search">Apply filter</Button>
               <Button color="primary" size="sm"  onClick={this.reset} id="search">Reset filter</Button></Col>
            </Row>
          </div>

        </div>
        <div className='clearfix'></div>
        <Button color="primary" size="sm" className='mb-1' onClick={(e) => { this.showdialog(e) }}>Define PO BT Process</Button>
        <div className='clearfix'></div>
        {/* -------------------------------------- filter  --------------------------------------  */}
        <GridComponent gridLines='Both' allowTextWrap={true} frozenColumns={3} rowHeight='30' allowPaging={true} allowResizing={true} allowSorting={true} ref={grid => this.btProcessGridObj = grid} dataSource={this.state.btProcesses}
        filterSettings={this.state.FilterSettingsModel} allowFiltering={true}   >
          <ColumnsDirective>
            <ColumnDirective field='poTypeName'  filter={this.state.IFilter} headerText='PO Type' width='150' />
            <ColumnDirective field='poGroupName' filter={this.state.IFilter} headerText='PO Group' width='150' />
            <ColumnDirective field='plantName'  filter={this.state.IFilter} headerText='Plant' width='180' />
            <ColumnDirective field='processFlow' width='730' template={this.processFlowtemplate} headerText='Process Flow' />
          </ColumnsDirective>
          <Inject services={[DetailRow, Freeze, Page, Resize, Sort,Filter]} />
        </GridComponent>

        <h5 className='mt-3'> Quality Assignee</h5>

        <GridComponent gridLines='Both' allowReordering={true} allowTextWrap={true} rowHeight='25' allowResizing={true} allowSorting={true} dataSource={this.state.qaMetrix} toolbar={this.state.toolbarOptions} allowPaging={true}  height={this.state.windowsHeightsecond} editSettings={this.state.editSettings} actionBegin={this.saveQaMetrix} >
          <ColumnsDirective>
            <ColumnDirective field='plantName' headerText='Plant' editType='dropdownedit'  edit={this.plant} width='150' />
            <ColumnDirective field='unitName' headerText='Unit' editType='dropdownedit'  edit={this.unit} width='50' />
            <ColumnDirective field='lineName' headerText='Line' editType='dropdownedit'  edit={this.line} width='150' />
            <ColumnDirective field='productCategory' headerText='Product Category' editType='dropdownedit' editTemplate={this.editQaProductCategoryTemplate} width='90' />
            <ColumnDirective field='buyer' headerText='Buyer' editType='dropdownedit' editTemplate={this.editQaBuyerTemplate} width='150' />
            <ColumnDirective field='materialType' headerText='Material Type' editType='dropdownedit' editTemplate={this.editQaMaterialTypeTemplate} width='80' />
            <ColumnDirective field='inspectionType' headerText='Inspection Type' editType='dropdownedit' validationRules={this.state.validationRules} editTemplate={this.editInspectionTypeTemplate} width='140' />
            <ColumnDirective field='role' headerText='Roles' editType='dropdownedit' validationRules={this.state.validationRules} edit={this.role} width='100' />
            <ColumnDirective field='assigneeName' headerText='User Name' editType='dropdownedit' validationRules={this.state.validationRules} edit={this.user} width='150' />
          </ColumnsDirective>
          <Inject services={[DetailRow, Reorder, Toolbar, Edit, Page, Resize, Sort]} />
        </GridComponent>

        <div className='mt-3'></div>

        {/* ------------- ---------------------- View all Documents ------------- ---------------------- */}
        <DialogComponent allowDragging={true} id="modalDialog" isModal={true} width='80%' visible={this.state.showDialogBox} showCloseIcon={true} header={this.state.showDialogBoxHeading} close={this.closeDialog}>
          <div>
            <GridComponent gridLines='Both' allowTextWrap={true} rowHeight='25' allowResizing={true} allowSorting={true} dataSource={this.state.addmappingDepartmentGrid} toolbar={this.state.toolbarOptions} allowPaging={true} editSettings={this.state.editSettings} actionBegin={this.saveBtMetrix} >
              <ColumnsDirective>
                <ColumnDirective field='poType' headerText='PO Type' validationRules={this.state.validationRules} editTemplate={this.editPOTypeTemplate} width='100' />
                <ColumnDirective field='poGroups' headerText='PO Groups' validationRules={this.state.validationRules} editTemplate={this.editPOGroupsTemplate} width='140' />
                <ColumnDirective field='plants' headerText='Plant' validationRules={this.state.validationRules} editTemplate={this.editPlantTemplate} width='250' />
                <ColumnDirective field='department' headerText='Department' validationRules={this.state.validationRules} editTemplate={this.editDeptTemplate} width='130' />
                <ColumnDirective field='assigne' headerText='Assigne' validationRules={this.state.validationRules} editTemplate={this.editAssigneTemplate} width='100' />
                <ColumnDirective field='seq' headerText='Seq' validationRules={this.state.validationRules} editTemplate={this.editseqTemplate} width='80' />
              </ColumnsDirective>
              <Inject services={[DetailRow, Toolbar, Edit, Page, Resize, Sort]} />
            </GridComponent>
          </div>
        </DialogComponent>
        {/* ------------- ---------------------- View all Documents ------------- ---------------------- */}
        <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
      </div >
    );
  }
}

export default withTranslation("translations")(MappingMetrics);
