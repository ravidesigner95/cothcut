import React from 'react';

class HourlyProduction extends React.PureComponent {
  baseUrl = process.env.REACT_APP_MOEP_DASHBOARD;
  moepUrl = process.env.REACT_APP_BASEURL_DASHBOARD
  dashboard = this.moepUrl + "/dashboard/index.html";
 
  render() {
    return ( 
           <div className="container-fluid-inner">
         <h1 className='pagesheading'>Hourly Dashboard</h1>
        {/* <iframe src="https://app.powerbi.com/view?r=eyJrIjoiZTg2YzhlYjItMzQ2My00ZjM4LThjZmItNzVjMjBkZWJmYzZiIiwidCI6Ijc1ZDg4MmE0LWFkM2EtNDM3Ni1hNGFhLTBhNjVhZmVmOTlkMiJ9"
          height="765" width="100%" allowFullScreen="true" frameBorder="0"></iframe>  */}
        <iframe src={this.dashboard} urlname={this.baseUrl}
          height="765" width="100%" allowFullScreen={true} frameBorder="0"></iframe>


        {/* http://192.168.0.36/Dashboard_demo/index.html */}
      </div>
    );
  }
}

export default HourlyProduction;
