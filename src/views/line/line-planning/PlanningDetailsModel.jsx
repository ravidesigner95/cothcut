export class PlanningDetailsModel {
    constructor(id, fmId, fmName, qaId, qaName, apmId, saidId, plantId, unitId, prodLineId, mechandiserId, productCategoryId, buyerId, plantName, unitName,
        lineName, merchandiserName, buyerName, apm, sam, lineItemNo, totalQtyAllocatedInLine, totalQtyToProduced, remainingQtyToPlan,
        startDateTime, endDateTime, noOfMachineToUse, estimatedWorkingDays, lineItemDesc) {
        this.apmId = apmId;
        this.fmId = fmId;
        this.fmName = fmName;
        this.qaId = qaId;
        this.qaName = qaName;
        this.saidId = saidId;
        this.lineName = lineName;
        this.noOfMachineToUse = noOfMachineToUse;
        this.id = id;
        this.plantId = plantId;
        this.unitId = unitId;
        this.prodLineId = prodLineId;
        this.mechandiserId = mechandiserId;
        this.productCategoryId = productCategoryId;
        this.buyerId = buyerId;
        this.plantName = plantName;
        this.unitName = unitName;
        this.merchandiserName = merchandiserName;
        this.buyerName = buyerName;
        this.apm = apm;
        this.sam = sam;
        this.lineItemNo = lineItemNo;
        this.totalQtyAllocatedInLine = totalQtyAllocatedInLine;
        this.totalQtyToProduced = totalQtyToProduced;
        this.remainingQtyToPlan = remainingQtyToPlan;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.estimatedWorkingDays = estimatedWorkingDays;
        this.lineItemDesc = lineItemDesc;
    }
    id = null;
    fmId = null;
    fmName = null;
    qaId = null;
    qaName = null;
    apmId = null;
    saidId = null;
    plantId = null;
    unitId = null;
    prodLineId = null;
    mechandiserId = null;
    productCategoryId = null;
    buyerId = null;
    plantName = "";
    unitName = "";
    lineName = "";
    merchandiserName = "";
    buyerName = "";
    apm = null;
    sam = null;
    lineItemNo = null;
    totalQtyAllocatedInLine = null;
    totalQtyToProduced = null;
    remainingQtyToPlan = null;
    startDateTime = null;
    endDateTime = null;
    noOfMachineToUse = null;
    estimatedWorkingDays = null;
    lineItemDesc = null;
}
export default PlanningDetailsModel;