import axios from 'axios';

class PermissionServices {

    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    saveUrl = this.baseUrl + `/module`;
     options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }

     };
     
     async saveUser(data) {
        console.log(this.saveUrl)
        try {
           
            const response = await axios.post(this.saveUrl, data, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }
    }
    export default PermissionServices;
