import axios from 'axios';

class LineConfigService {
    constructor() { }
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;

    plantListUrl = this.baseUrl + `/master/plant`;
    unitListUrl = this.plantListUrl;
    categoryUrl = this.baseUrl + `/master/category`;
    processesUrl = this.baseUrl + `/master/process`;
    lineCrudUrl = this.baseUrl + `/unit/line`;
    allLinesUrl = this.baseUrl + `/master/lines`;
    addConfgUrl = this.baseUrl + `/appConfg/addOrUpdateKey`;
    getConfgUrl = this.baseUrl + `/appConfg/appCnfgkey`;
    confgkeyUrl = this.baseUrl + `/appConfg/appCnfgkey`;
    supervisorUrl = this.baseUrl + `/user/SUPERVISOR`;
    workingShiftUrl = this.baseUrl + `/unit/getShiftCategories`;
    workingShiftDetailsUrl = this.baseUrl + `/unit/getShiftMaster`;
    addOverridesUrl = this.baseUrl + `/unit/addOverrides`;
    removeOverridesUrl = this.baseUrl + `/unit/removeOverrides`;
    // addLineUrl = this.baseUrl + `/unit/line`;
    // deleteLineUrl=this.baseUrl+`/unit/line`;
    // editLineUrl=this.baseUrl+`/unit/line`;

    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }

    };

    async getConfgKey() {
        try {
            const response = await axios.get(this.confgkeyUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getWorkingShiftDetails(shiftName) {
        try {
            const response = await axios.get(this.workingShiftDetailsUrl + `/${shiftName}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getWorkingShifts() {
        try {
            const response = await axios.get(this.workingShiftUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getSupervisorList() {
        try {
            const response = await axios.get(this.supervisorUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getGridData() {
        try {
            return await axios.get(this.allLinesUrl, this.options);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getLineProcesses() {
        try {
            const response = await axios.get(this.processesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getCategory() {
        try {
            const response = await axios.get(this.categoryUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getPlantList() {
        try {
            const response = await axios.get(this.plantListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnitList(plantId) {
        try {
            const response = await axios.get(this.unitListUrl + `/${plantId}/units/`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async addLine(newline) {
        try {
            const response = await axios.post(this.lineCrudUrl, newline, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }

    async editLine(editedLine) {
        try {
            const response = await axios.put(this.lineCrudUrl, editedLine, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteLine(deletedLineId) {
        try {
            const response = await axios.delete(this.lineCrudUrl + `/${deletedLineId}`, this.options)
            return response;
        }
        catch (error) {
            return error.response;
            console.log(error);
            // alert("error")
        }
    }

    async addOverride(editedLine) {
        try {
            const response = await axios.put(this.addOverridesUrl, editedLine, this.options)
            return response;
        }
        catch (error) {
            return error.response;
            console.log(error);
            // alert("error")
        }
    }

    async removeOverride(subLineId) {
        try {
            const response = await axios.delete(this.removeOverridesUrl + `/${subLineId}`, this.options)
            return response;
        }
        catch (error) {
            return error.response;
            console.log(error);
            // alert("error")
        }
    }

}
export default LineConfigService;