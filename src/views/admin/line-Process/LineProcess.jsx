import * as React from 'react';
import axios from 'axios';
import { GridComponent, Resize, ColumnsDirective, CommandColumn, ColumnDirective, Filter, Reorder, Inject, Selection, VirtualScroll, Edit, Page, Sort, FilterSettingsModel, IFilter } from '@syncfusion/ej2-react-grids';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { withTranslation, Trans } from "react-i18next";
import LineProcessModel from './LineProcessModel';
import SplitterLayout from 'react-splitter-layout';
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import LineProcessService from './LineProcessService'
import 'react-splitter-layout/lib/index.css';
import LineProductionProcessModel from './LineProcessProductionModel'
import errorMessages from '../../errorHandler/errorMessages';
import { AppConfig } from '../../../appConfig/appConfig';

class LineProcesses extends React.PureComponent {
    constructor(props) {
        super(props);
        this.LineProcessService = new LineProcessService();
        this.initialErrorStatus = {
            name: false,
            discription: false,
        };
        this.mounted = false;
        this.state = {
            model: new LineProcessModel(null, '', ''),
            errorObject: this.initialErrorStatus,
            isSidebar: true,
            id: null,
            isEdit: false,
            isShowConfirmationDialog: false,
            tableData: [],
            lineProcesses: [],
            resetLineProcesses: [],
            validationErrorCss: "custom-error",
            editSettings: { allowEditing: false, allowAdding: false, allowDeleting: true },
            animationSettings: { effect: 'FlipYLeft' },
            commands: [
                { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
                { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },],

            IFilter: { type: 'CheckBox' },
            PageSettingsModel: { pageSize: 8, pageSizes: true },
            toasterPosition: { X: 'Right', Y: 'Bottom' },
            toasterMessage: [
                { title: 'Warning!', content: 'Changes Discarded !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Success!', content: 'Line Process added successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
                { title: 'Information!', content: 'Please read the comments carefully.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Success!', content: 'Line Process updated successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Success!', content: 'Line Process deleted successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'Line Process already Exixts!!', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
            ],
            confirmButton: [{
                click: (args) => {
                    this.delete(args);

                },
                buttonModel: { content: 'Yes', isPrimary: true }
            },
            {
                click: (args) => {
                    this.cancelConformDialog(args);

                },
                buttonModel: { content: 'No' }
            }],

        }
    }

    componentDidMount() {
        this.mounted = true;
        if (this.mounted) {
            this.getLineProcesses();

            document.addEventListener("keydown", this.escFunction, false);
        }
    }

    //------------------------------------------API----------------------------------------//

    async getLineProcesses() {
        let lineProcessesList = []
        let response = (await this.LineProcessService.getLineProcesses())

        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            response.data.data.map(processObj => {
                lineProcessesList.push(new LineProductionProcessModel(processObj.id, processObj.name, processObj.description, false))
            })
            this.setState({ tableData: lineProcessesList, resetLineProcesses: lineProcessesList })
        }
    }

    async addLine(newLine) {
        const { t, i18n } = this.props;
        let response = (await this.LineProcessService.addLineProcess(newLine))
        if ((response != undefined) && (response.data.status == "success")) {
            await this.getLineProcesses()
            if (newLine.id != null) {
                this.toastObj.show(this.state.toasterMessage[4]);
            }
            else {
                this.toastObj.show(this.state.toasterMessage[1]);
            }

        } else if ((response != undefined) && (response.data.status == "fail")) {
            this.toastObj.show(this.createToaster1(t(errorMessages[response.data.data.errorCode])));
            //  global.hideLoader()
        } else if ((response != undefined) && (response.data.status == "unsuccess")) {
            this.toastObj.show(this.createToaster(response.data.message, "Error"));
        } else {
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
        }

        this.setState({ isSidebar: true, isDisabled: false, isEdit: false, model: new LineProcessModel(null, '', '',) })
    }
    async deleteLine(deletedLineId) {
        const { t, i18n } = this.props;
        let response = (await this.LineProcessService.deleteLine(deletedLineId))
        if ((response != undefined) && (response.data.status == "success")) {
            await this.getLineProcesses();
            this.toastObj.show(this.state.toasterMessage[5]);
        } else if ((response != undefined) && (response.data.status == "fail")) {
            this.toastObj.show(this.createToaster1(t(errorMessages[response.data.data.errorCode])));
            //  global.hideLoader()
        } else if ((response != undefined) && (response.data.status == "unsuccess")) {
            this.toastObj.show(this.createToaster(response.data.message, "Error"));
        } else {
            this.toastObj.show(this.createToaster("Something went wrong", "Error"));
        }
        this.setState({ lineIdToDelete: null });
    }

    //---------------------------------------------Toaster Message--------------------------
    createToaster1(message) {
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
    }


    createToaster(message, type) {
        switch (type) {
            case "Success":
                return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
            case "Error":
                return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
            case "Warning":
                return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
            case "Information":
                return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
    }

    //---------------------------------CRUD---------------------------------------------//

    onSave = (event) => {
        let model = Object.assign({}, this.state.model);

        if (this.isValidate(this.state.model)) { this.addLine(model); }

    }


    async onEditClick(data) {
        let selectedRowLineProcesses = data.lineProcesses;
        let allLineProcessesFromState = Object.assign([], this.state.lineProcesses);

        selectedRowLineProcesses = allLineProcessesFromState.concat(selectedRowLineProcesses);
        // await this.getUnitList(data.plantId)
        this.setState({ model: data, isSidebar: false, lineProcesses: selectedRowLineProcesses, isEdit: true })
    }

    actionBegin = (args) => {                   // TO BE IMPROVED !!
        if (args.requestType == "delete") {
            args.cancel = true;
            const id = args.data[0].id;
            this.setState({ isShowConfirmationDialog: true, lineIdToDelete: id })
            // this.deleteLine(id)
        }
    }
    delete = (args) => {

        const { lineIdToDelete } = this.state;
        this.deleteLine(lineIdToDelete);
        this.setState({ isShowConfirmationDialog: false })
        // console.log('delete')

    }
    cancelConformDialog = (args) => {
        this.setState({ isShowConfirmationDialog: false, lineIdToDelete: null })
    }

    //-----------------------------ON-CHANGE EVENTS-------------------------------------//


    handleChange = (event) => {
        let that = this;
        that.setState({ model: { ...that.state.model, [event.target.id]: event.target.value } })
    }


    sideBar = (event) => {

        this.setState({ isSidebar: !this.state.isSidebar, units: [], isDisabled: false, model: new LineProcessModel(null, '', ''), lineProcesses: this.state.resetLineProcesses, isEdit: false, errorObject: this.initialErrorStatus })
        console.log(this.state.model);
    }

    //---------------------------------Other event-------------------------------------------//
    rowselected = (args) => {                     // TO BE IMPROVED !!
        let that = this;
        if ((args.target.classList.length > 2) && (that.state.isSidebar == true)) {
            if ((args.target.classList.contains('e-edit')) && (args.data != undefined)) {
                that.onEditClick(args.data)
            }
        }
    }

    isValidate(model) {
        let isValidate = true;
        let errorObj = Object.assign({}, this.state.errorObject);
        if (model.name == null || model.name == "") {
            errorObj.name = true;
            isValidate = false;
        }
        else {
            errorObj.name = false;
        }
        //  if(model.description==null || model.description=="")
        //  {
        //     errorObj.discription = true;
        //     isValidate = false;
        //  }
        //  else{
        //     errorObj.discription = false;
        //  }
        this.setState({ errorObject: errorObj })
        return isValidate;
    }
    //-------------------------------------------------RENDER--------------------------------------------//

    render() {
        const { t, i18n } = this.props;
        console.log('this.state.tableData', this.state.tableData)
        return (
            <div>
                <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
                <div className="container-fluid-inner">
                    <h1 className='pagesheading'>Line Process</h1>
                    <div className='clearfix'></div>
                    <Button color="primary" size="sm" className="mb-2 mt-n3" onClick={e => this.sideBar(e)}>{t("cpm.lm.addnewLine")} </Button>
                           
                    <GridComponent
                                allowTextWrap={true}
                                allowSorting={true}
                                allowFiltering={true}
                                allowReordering={true}
                                allowPaging={true}
                                allowResizing={true}
                                rowHeight='25'
                                gridLines='Both'
                                actionBegin={this.actionBegin}
                                selectionSettings={{ enableToggle: false }}
                                rowSelected={this.rowselected}
                                actionComplete={this.actionComplete}
                                dataSource={this.state.tableData}
                                filterSettings={AppConfig.FilterSettingsModel}
                                height={AppConfig.gridHeight}
                                editSettings={AppConfig.editSettings}
                                pageSettings={AppConfig.pageSettings}

                            >
                                <ColumnsDirective>
                                    <ColumnDirective field='name' filter={this.state.IFilter} headerText={t("cpm.lm.grid.line.process")} width='130' />
                                    <ColumnDirective field='description' filter={this.state.IFilter} headerText={t("cpm.lm.grid.line.description")} allowSorting={false} allowFiltering={false} width='650' />

                                    <ColumnDirective field='action' allowSorting={false} allowFiltering={false} headerText={t("cpm.lm.grid.action")} width='90' commands={this.state.commands} ></ColumnDirective>
                                </ColumnsDirective>
                                <Inject services={[CommandColumn, Sort, Filter, Resize, Reorder, Selection, Edit, Page]} />
                            </GridComponent>
                      
                    {/* ----------------------- Add line Process  -----------------------  */}
                    <div className="add-line-section" hidden={this.state.isSidebar}>
                        <SplitterLayout secondaryInitialSize={550} secondaryMinSize={450}>
                            <div> </div>
                            <div className="overflow-hidden add-line-section-form">
                                <div className="heading mb-2">{t("cpm.lm.addnewLine")} <a className="close" onClick={e => this.sideBar(e)}>+</a></div>
                                <Col xs='12'>
                                    <Row >
                                        <Col xs='12' md='6'>
                                            <div className={this.state.errorObject.name == true ? this.state.validationErrorCss : "required-field"} >
                                                <TextBoxComponent placeholder={t("cpm.lm.field.line.process")} id="name" value={this.state.model.name} onChange={this.handleChange} floatLabelType="Auto" />
                                                <span hidden={!this.state.errorObject.name} >{t("cpm.lm.line.validation.lineprocessName")}</span>
                                            </div>
                                        </Col>
                                        <Col xs='12' md='6'>
                                            {/* <div className={this.state.errorObject.discription == true ? this.state.validationErrorCss : "required-field"} > */}
                                            <TextBoxComponent placeholder={t("cpm.lm.field.line.description")} id="description" value={this.state.model.description} onChange={this.handleChange} floatLabelType="Auto" />
                                            {/* <span hidden={!this.state.errorObject.discription} >{t("cpm.lm.line.validation.lineprocessDis")}</span> */}
                                            {/* </div> */}
                                        </Col>
                                    </Row>
                                    <Row className="mt-3">
                                        <Col>
                                            <Button color="primary" size="sm" id="save" disabled={this.state.isDisabled} className="float-left mr-3" onClick={e => this.onSave(e)}>{t("cpm.lm.line.save")}</Button>
                                            <Button color="primary" size="sm" id="cancel" className="float-left" onClick={e => this.sideBar(e)}>{t("cpm.lm.line.cancel")}</Button>
                                        </Col>
                                    </Row>
                                </Col>
                            </div>
                        </SplitterLayout>
                    </div>
                    <div className="add-line-section-overlay" hidden={this.state.isSidebar}></div>
                </div>
                <DialogComponent id="confirmDialog" animationSettings={this.state.animationSettings} showCloseIcon={true} close={this.cancelConformDialog} buttons={this.state.confirmButton} header='Delete Line Confirmation !!' visible={this.state.isShowConfirmationDialog} width='400px' content='Are you sure you want to permanently delete Line Process?'></DialogComponent>
                <div className="overlayer-popup" hidden={!this.state.isShowConfirmationDialog} ></div>
            </div>
        );
    }
}
export default withTranslation("translations")(LineProcesses);

