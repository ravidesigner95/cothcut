import axios from 'axios';
class ResetPasswordService {
    constructor() { }

    version = `/api/v1`
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    resetPassUrl =this.baseUrl+`/auth/resetPassword`
    async changePassword(data){
        try {
            const options = {
                headers: { 'Content-Type': 'application/json' }
            };
            const response = await axios.post(this.resetPassUrl, data, options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }
    



}
export default ResetPasswordService;