exports.nestedListData = [  
    {
        id: '02', text: 'Marketing', icon: 'folder',
        child: [
            { id: '02-01', text: ' MKT. Mang.', icon: 'file' },
            { id: '02-02', text: 'Sr.MR', icon: 'file' },
            { id: '02-01', text: 'MR.', icon: 'file' },
            { id: '02-02', text: 'AMR', icon: 'file' },
            { id: '02-01', text: 'Co-ordinator', icon: 'file' },
        ]
    },
    {
        id: '01', text: 'Product Devlopment(Sampling)', icon: 'folder',
        child: [
            { id: '01-01', text: '-------', icon: 'file' }
        ]
    },
    {
        id: '03', text: 'HR', icon: 'folder',
        child: [
            { id: '03-01', text: 'SR. Executive', icon: 'file' },
            { id: '03-02', text: 'Executive', icon: 'file' },
            { id: '03-03', text: 'Jr.Excutive', icon: 'file' },
            { id: '03-04', text: 'Assistant', icon: 'file' },  
         ]
    },
    {
        id: '04', text: ' Accounts', icon: 'folder',
        child: [
            {
                id: '04-01', text: 'Camera Roll', icon: 'folder',
                child: [
                    { id: '03-01', text: 'SR. Executive', icon: 'file' },
                    { id: '03-02', text: 'Executive', icon: 'file' },
                    { id: '03-03', text: 'Jr.Excutive', icon: 'file' },
                    { id: '03-04', text: 'Assistant', icon: 'file' },  
                ]
            },
            {
                id: '04-02', text: 'Wind.jpg', icon: 'file'
            },
            {
                id: '04-02', text: 'Stone.jpg', icon: 'file'
            },
            {
                id: '04-02', text: 'Home.jpg', icon: 'file'
            },
            {
                id: '04-02', text: 'Bridge.png', icon: 'file'
            }
        ]
    },
    {
        id: '05', text: 'RMT + PPC', icon: 'folder',
        child: [
            { id: '05-01', text: 'UI-Guide.pdf', icon: 'file' },
            { id: '05-02', text: 'Tutorials.zip', icon: 'file' },
            { id: '05-03', text: 'Game.exe', icon: 'file' },
            { id: '05-04', text: 'TypeScript.7z', icon: 'file' },
        ]
    },
];
