export class GrnInvoiceModel {
    constructor(grnQty, invoiceCurrency, invoiceNumber, invoiceValue,lineItem,lineItemDesc,uom) {
        this.grnQty = grnQty;
        this.invoiceCurrency = invoiceCurrency;
        this.invoiceNumber = invoiceNumber;
        this.invoiceValue = invoiceValue;
        this.lineItem = lineItem;
        this.lineItemDesc = lineItemDesc;
        this.uom = uom;
    }
    grnQty = null;
    invoiceCurrency = null;
    invoiceNumber = null;
    invoiceValue = null;
    lineItem=null;
    lineItemDesc = null;
    uom=null;
}

export default GrnInvoiceModel; 

