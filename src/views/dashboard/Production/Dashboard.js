import React from 'react';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
class ProdDashboard extends React.PureComponent {
  baseUrl = process.env.REACT_APP_MOEP_DASHBOARD;
  moepUrl = process.env.REACT_APP_BASEURL_DASHBOARD
  dashboard = this.moepUrl + "/dashboard/index.html";
  prodURLTabEfficiency = process.env.REACT_APP_MOEP_PRODUCTION_DASHBOARD_Efficiency;
  prodURLTabSummary = process.env.REACT_APP_MOEP_PRODUCTION_DASHBOARD_Summary;
  prodURLTabCapacity = process.env.REACT_APP_MOEP_PRODUCTION_DASHBOARD_Capacity;
  prodURLTabPLView = process.env.REACT_APP_MOEP_PRODUCTION_DASHBOARD_PLView;

  tabContent1 = () => {
    return (
      <div className='pt-1'>

        <iframe src={this.prodURLTabSummary} height="765" width="100%" allowFullScreen={true} frameBorder="0"></iframe>

      </div>
    )
  }
  tabContent2 = () => {
    return (
      <div className='pt-1'>
        <iframe src={this.prodURLTabEfficiency} height="765" width="100%" allowFullScreen={true}  frameBorder="0"></iframe>
      </div>
    )
  }
  tabContent3 = () => {
    return (
      <div className='pt-1'>
        <iframe src={this.prodURLTabCapacity} height="765" width="100%" allowFullScreen={true}  frameBorder="0"></iframe>
      </div>
    )
  }

  tabContent4 = () => {
    return (
      <div className='pt-1'>
        <iframe src={this.prodURLTabPLView} height="765" width="100%" allowFullScreen={true}  frameBorder="0"></iframe>
      </div>
    )
  }

  render() {
    let headertext;
    // Mapping Tab items Header property
    headertext = [
      { text: "Summary View" },
      { text: "Efficiency View" },
      { text: "Capacity View" },
      { text: "P&L View" }
    ];



    return (
      <div className="container-fluid-inner">
        <h1 className='pagesheading'>Production Dashboard</h1>
        {/* <iframe src="https://app.powerbi.com/view?r=eyJrIjoiZTg2YzhlYjItMzQ2My00ZjM4LThjZmItNzVjMjBkZWJmYzZiIiwidCI6Ijc1ZDg4MmE0LWFkM2EtNDM3Ni1hNGFhLTBhNjVhZmVmOTlkMiJ9"
          height="765" width="100%" allowFullScreen="true" frameBorder="0"></iframe>  */}

        {/* <iframe src={this.dashboard} urlname={this.baseUrl}
          height="765" width="100%" allowFullScreen={true} frameBorder="0"></iframe>  */}

        {/* http://192.168.0.36/Dashboard_demo/index.html */}

        <TabComponent heightAdjustMode='Auto' id='defaultTab'>
          <TabItemsDirective>
            <TabItemDirective header={headertext[0]}
              content={this.tabContent1} />
            <TabItemDirective header={headertext[1]}
              content={this.tabContent2} />
            <TabItemDirective header={headertext[2]}
              content={this.tabContent3} />
            <TabItemDirective header={headertext[3]}
              content={this.tabContent4} />
          </TabItemsDirective>
        </TabComponent>
      </div>
    );
  }
}
export default ProdDashboard;
