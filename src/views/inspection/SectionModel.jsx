export class SectionModel {
    constructor(id, qualityCategoryMasterDTOList, componentId, sectionDesc, isEditable, sectionName, sequence,
        qualitySubSectionDTOs, pid, category, type) {
        this.id = id;
        this.qualityCategoryMasterDTOList = qualityCategoryMasterDTOList;
        this.componentId = componentId;
        this.sectionDesc = sectionDesc;
        this.isEditable = isEditable;
        this.sectionName = sectionName;
        this.sequence = sequence;
        this.qualitySubSectionDTOs = qualitySubSectionDTOs;
        this.pid = pid;
        this.category = category;
        this.type = type;
    }
    id = null;
    qualityCategoryMasterDTOList = null;
    componentId = null;
    sectionDesc = null;
    isEditable = null;
    sectionName = null;
    sequence = null;
    qualitySubSectionDTOs = null;
    pid = null;
    category = null;
    type = null;
}


export default SectionModel;
export class SubSectionModel {
    constructor(id, isPrefilled, referenceId, sequence, text, type, pid, mappingField, mappingFieldName, subSectionQuesAnsGroupDTO, isMandate, isEditable
        , role,uniqueId) {
        this.id = id;
        this.isPrefilled = isPrefilled;
        this.referenceId = referenceId;
        this.sequence = sequence;
        this.text = text;
        this.type = type;
        this.pid = pid;
        this.mappingField = mappingField;
        this.mappingFieldName = mappingFieldName;
        this.subSectionQuesAnsGroupDTO = subSectionQuesAnsGroupDTO;
        this.uniqueId = uniqueId;
        this.isMandate = isMandate;
        this.isEditable = isEditable;
        this.role = role;
    }
    id = null;
    isPrefilled = null;
    referenceId = null;
    sequence = null;
    text = null;
    type = null;
    pid = null;
    mappingField = null;
    mappingFieldName = null;
    subSectionQuesAnsGroupDTO = null;
    uniqueId = null;
    isMandate = null;
    isEditable = null;
    role = null;
}