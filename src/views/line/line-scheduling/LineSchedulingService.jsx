import axios from 'axios';

class LineSchedulingService {
    constructor() { }
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    orderUrl = process.env.REACT_APP_BASEURL_DASHBOARD;
    plantListUrl = this.baseUrl + `/master/plant`;
    unitListUrl = this.baseUrl + `/master/plant`;
    unitLinesUrl = this.baseUrl + `/master`;
    categoryUrl = this.baseUrl + `/master/category`;
    plannedLinesUrl = this.baseUrl + `/line/schedule/planned`;
    // buyerPoUrl = `http://192.168.0.25:8081/v1/order/buyer_po`;
    // buyerPoUrl = `http://122.180.11.78:8218/orderservice/v1/order/buyer_po`;
    buyerPoUrlOrder = this.orderUrl + `/orderservice/v1/order/buyer_po`;
    selectedBuyerPoLineItemsUrlOrder = this.orderUrl + `/orderservice/v1/order/buyer_pos`;
    buyerPoUrlLine = this.baseUrl + `/line/schedule/buyerPo`
    plannedEventsUrl = this.baseUrl + `/unit/line/doplan`;
    scheduledEventsUrl = this.baseUrl + `/line/schedule/scheduled`;
    eventPlannedDataUrl = this.baseUrl + `/unit/line/period`;
    eventScheduledDataUrl = this.baseUrl + `/line/schedule/period`;
    saveScheduledDataUrl = this.baseUrl + `/line/schedule/doSchedule`;
    saveUnScheduledDataUrl = this.baseUrl + `/line/schedule/doUnSchedule`;
    scheduledLinesUrl = this.baseUrl + `/line/schedule/scheduled`;
    lineScheduledDetailsURL = this.baseUrl + `/line/schedule/line/schedule`;
    apmUrl = this.baseUrl + `/unit/apm`;;
    selectedScheduledDataEventsUrl = this.baseUrl + `/line/schedule/doSchedule`;
    summaryDataPlantsUrl = this.baseUrl + `/line/schedule/summary_plant`;
    summaryDataCategoryUrl = this.baseUrl + `/line/schedule/summary_category`;
    availableMachinesUrl = this.baseUrl + `/line/schedule/availableMachines`;
    confgkeyUrl = this.baseUrl + `/appConfg/appCnfgkey`;
    estimatedWorkingDaysUrl = this.baseUrl + `/unit/getEstimatedWorkDays`;
    orderListUrl = this.baseUrl + `/line/schedule/scheduling_status`;
    stockDetailUrl = this.orderUrl + `/orderservice/v1/orderDasboard/checkStock`;
    scheduledQtyUrl = this.baseUrl + `/line/schedule/schedule`;
    schedulingStatusUrl=this.baseUrl+`/line/schedule/generateSchedulingStatusReport`;
    downloadReportUrl = this.baseUrl + `/hr/employee/downloadReportOfLine`;
    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    //---------------------------------------API-CALLS-----------------------------------------//

    async getConfgKey() {
        try {
            const response = await axios.get(this.confgkeyUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getAllOrders() {
        try {
            const response = await axios.get(this.orderListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getStockDetails(saidId, lineItemNo, buyerPoNo) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    said_id: saidId,
                    line_item_no: lineItemNo,
                    buyer_po_no: buyerPoNo
                }
            };
            const response = await axios.get(this.stockDetailUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getEstimatedWorkingDays(buyerPo, lineItem, noOfMachines, prodLineId, qty, saidNo, sam, startDate) {
        let obj = {
            buyerPo: buyerPo,
            lineItem: lineItem,
            noOfMachines: noOfMachines,
            prodLineId: prodLineId,
            qty: qty,
            saidNo: saidNo,
            sam: sam,
            startDate: startDate
        }
        try {
            const response = await axios.post(this.estimatedWorkingDaysUrl, obj, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getDaywiseNoOfAllocatedMachines(prodLineId) {
        try {
            const response = await axios.get(this.availableMachinesUrl + `/${prodLineId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getPlantList() {
        try {
            const response = await axios.get(this.plantListUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnitList(plantId) {
        try {
            const response = await axios.get(this.unitListUrl + `/${plantId}/units/`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getUnitProductionLines(unitId) {
        try {
            const response = await axios.get(this.unitLinesUrl + `/${unitId}/lines/`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getCategory() {
        try {
            const response = await axios.get(this.categoryUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getAPM(plantId) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plant_id: plantId
                }
            };
            const response = await axios.get(this.apmUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getSummeryViewData(plantList, isCategoryURL, viewType,year) {
        let url = isCategoryURL ? this.summaryDataCategoryUrl : this.summaryDataPlantsUrl;
        const config = {
            headers: this.options.headers,
            params: {
                type: viewType,
                year:year
            }
        };
        try {
            const response = await axios.post(url, plantList, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getLineScheduledDetails(unitId, lineId,year) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    unit_id: unitId,
                    prod_line_id: lineId,
                    target_month: "null",
                    target_year:year
                }
            };
            return await axios.get(this.lineScheduledDetailsURL, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getSelectedMonthScheduledEvents(lineId , year) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    prod_line_id: lineId,
                    // target_month: month,//"DECEMBER",
                    target_year: year
                    // startTime: new Date(new Date().getFullYear(), 0, 1),
                    // endTime: new Date(new Date().getFullYear(), 11, 31)
                }
            };
            const response = await axios.get(this.selectedScheduledDataEventsUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getPlannedLines(plantId, unitId, prodLineId, noOfDays) {
        return this.getLineItemByPlantIdAndUnitId(plantId, unitId, prodLineId, noOfDays, "planned");
    }

    async getLineItemByPlantIdAndUnitId(plantId, unitId, prodLineId, noOfDays, eventType) {
        let url = null;
        const config = {
            headers: this.options.headers,
            params: {
                plant_id: plantId,
                unit_id: unitId,
                prod_line_id: prodLineId,
                startTime: new Date(),
                endTime: new Date(new Date().setDate(new Date().getDate() + noOfDays))
            }
        };
        if ("planned" == eventType) {
            url = this.plannedLinesUrl;
        } else {
            url = this.scheduledLinesUrl;
        }
        const response = await axios.get(url, config);
        return response;
    }


    async getLineItemBySaidIdAndLineItemNo(saidId, lineItemNo) {
        let url = null;
        const config = {
            headers: this.options.headers,
            params: {
                startTime: new Date(),
                endTime: new Date(new Date().setDate(new Date().getDate() + 120))
            }
        };
        url = this.plannedLinesUrl;
        const response = await axios.get(url + `/${saidId}/${lineItemNo}`, config);
        return response;
    }


    async getScheduledLines(plantId, unitId, prodLineId, noOfDays) {
        return this.getLineItemByPlantIdAndUnitId(plantId, unitId, prodLineId, noOfDays, "scheduled");
    }

    async getBuyerPo(saidId, lineItemNo) {
        try {
            // lineItemNo = "000010";
            const response = await axios.get(this.buyerPoUrlOrder + `/${saidId}/${lineItemNo}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getBuyerPoList(plantIds, unitName) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plantIds: plantIds + '',
                    unit: unitName
                }
            };
            return await axios.get(this.buyerPoUrlLine, config);
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getSelectedBuyerPoLineItems(saidId, buyerPoNo, plantId) {
        try {
            const response = await axios.get(this.selectedBuyerPoLineItemsUrlOrder + `/${saidId}/${buyerPoNo}/${plantId}`, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }



    async saveScheduledData(scheduledModel) {
        try {
            const response = await axios.post(this.saveScheduledDataUrl, scheduledModel, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
        }
    }

    async saveUnScheduledData(scheduledModel) {
        try {
            return await axios.post(this.saveUnScheduledDataUrl, scheduledModel, this.options);
        }
        catch (error) {
            console.log(error);
            return error.response;
        }
    }
    async getPlannedEvents(lineId, month, year) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    prod_line_id: lineId,
                    target_month: month,
                    target_year: year
                }
            };
            const response = await axios.get(this.plannedEventsUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getScheduledEvents(lineId, startDate, nextTwoMonthDate) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    prod_line_id: lineId,
                    startTime: startDate,
                    endTime: nextTwoMonthDate
                }
            };
            const response = await axios.get(this.scheduledEventsUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }


    async getClickedEventPlannedData(eventModel, saidId, lineId, categoryColor) {      // **Need to set Uniqe id.
        try {
            let data = JSON.parse(JSON.stringify(eventModel));
            const config = {
                headers: this.options.headers,
                params: {
                    said_id: saidId,
                    line_item: data.lineItemNo == undefined ? data.lineItem : data.lineItemNo,
                    prod_line_id: lineId,
                    start_date_time: data.oldStartDateTime == undefined ? data.startDateTime : data.oldStartDateTime,
                    end_date_time: data.oldEndDateTime == undefined ? data.endDateTime : data.oldEndDateTime,
                    category_color: categoryColor == null ? eventModel.categoryColor : categoryColor
                }
            };
            const response = await axios.get(this.eventPlannedDataUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async getClickedEventScheduledData(eventModel, saidId, lineId, scheduledEventId) {      // **Need to set Uniqe id.
        try {
            let data = JSON.parse(JSON.stringify(eventModel));
            const config = {
                headers: this.options.headers,
                params: {
                    said_id: saidId,
                    line_item: data.lineItemNo == undefined ? data.lineItem : data.lineItemNo,
                    prod_line_id: lineId,
                    start_date_time: data.scheduleStartDateTime,
                    end_date_time: data.scheduleEndDateTime,
                    scheduled_event_id: scheduledEventId
                }
            };
            const response = await axios.get(this.eventScheduledDataUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getScheduledQty(saidId, lineItemNo, buyerPoNo) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    said_id: saidId,
                    line_item_no: lineItemNo,
                    buyer_po_no: buyerPoNo
                }
            };
            const response = await axios.get(this.scheduledQtyUrl, config);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async generatePlanningStatusReport(plantId, unitId, startDate, endDate) {
        try {
            const config = {
                headers: this.options.headers,
                params: {
                    plant_id: plantId,
                    unit_id: unitId,
                    start_date: startDate,
                    end_date: endDate
                }
            };
            const response = await axios.get(this.schedulingStatusUrl, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }
    async downloadReport(reportId) {
        try {
            const config = {
                responseType: 'blob',
                headers: {
                    'Content-Type': 'application/octet-stream',
                    'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
                },
                headers: this.options.headers,
                params: {
                    reportId: reportId
                }
            };
            const response = await axios.get(this.downloadReportUrl, config)
            return response;
        }
        catch (error) {
            console.log(error);
            return error.response;
            // alert("error")
        }
    }
}



export default LineSchedulingService;