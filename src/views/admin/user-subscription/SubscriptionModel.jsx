
export class PermissionModel {
    constructor(id, userId, alertKey, userRole, status, createdOn, modifiedOn) {
        this.id = id;
        this.userId = userId;
        this.alertKey = alertKey;
        this.userRole = userRole;
        this.status = status;
        this.createdOn = createdOn;
        this.modifiedOn = modifiedOn;
    }
    id = null;
    userId = null;
    alertKey = null;
    userRole = null;
    status = null;
    createdOn = null;
    modifiedOn = null;
}

export default PermissionModel;
