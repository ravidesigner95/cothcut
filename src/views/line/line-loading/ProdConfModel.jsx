class ProdConfModel {
    constructor(id, loadTableId, acceptedQty, terminalMac, loadedDeviceProcessId, status, requestType, isIgnored, isRolledBack, createdDateTime,
         defectedPieces, reworkPieces,createdBy,modifiedBy,remarks) {
        this.acceptedQty = acceptedQty;
        this.loadTableId = loadTableId;
        this.terminalMac = terminalMac;
        this.id = id;
        this.loadedDeviceProcessId = loadedDeviceProcessId;
        this.status = status;
        this.requestType = requestType;
        this.isIgnored = isIgnored;
        this.isRolledBack = isRolledBack;
        this.createdDateTime = createdDateTime;
        this.setDefectPieces(defectedPieces);
        this.setReworkPieces(reworkPieces);
        this.createdBy=createdBy;
        this.modifiedBy=modifiedBy;
        this.remarks=remarks;
    }

    id = null;
    loadTableId = null;
    acceptedQty = null;
    terminalMac = null;
    loadedDeviceProcessId = null;
    status = null;
    requestType = null;
    isIgnored = null;
    isRolledBack = null;
    createdDateTime = null;
    defectedPieces = [];
    reworkPieces = [];

    setDefectPieces(obj) {
        if (obj != null)
            this.defectedPieces.push(obj);
    }

    setReworkPieces(obj) {
        if (obj != null)
            this.reworkPieces.push(obj);
    }
    createdBy=null;
    modifiedBy=null;
}
export default ProdConfModel;