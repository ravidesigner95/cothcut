import axios from 'axios';

class AppConfigService {
    constructor() { }
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    addConfgUrl = this.baseUrl+`/appConfg/addOrUpdateKey`
    getConfgUrl = this.baseUrl+`/appConfg/appCnfgkey`
    deleteConfgUrl = this.baseUrl+`/appConfg/deleteKey/`

    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }

    };

    async addConfgKey(newKey) {
        try {
            const response = await axios.post(this.addConfgUrl, newKey ,this.options)
            return response;
        }
        catch (error) {
            console.log(error); 
            return error.response;
            // alert("error")
        }
    }
    async getConfgKey(){
        try {
            const response = await axios.get(this.getConfgUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }

    async deleteConfgKey(keyId) {
        try {
            const response = await axios.delete(this.deleteConfgUrl + keyId,this.options)
            return response;
        }
        catch (error) {
            console.log(error); 
           // alert("error")
        }
    }  
}
export default AppConfigService;