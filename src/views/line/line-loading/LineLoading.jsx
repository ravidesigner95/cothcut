import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import cloneDeep from 'lodash/cloneDeep';
import { TabContent, TabPane, Nav, NavItem, NavLink, Button, Card, CardHeader, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { DropDownListComponent, revertHighlightSearch } from '@syncfusion/ej2-react-dropdowns';
import { GridComponent, Toolbar, ExcelExport, ColumnsDirective, ColumnDirective, Page, Resize, Inject, Edit, Group, CommandColumn, Filter, Reorder, Sort, DetailRow, ColumnChooser } from '@syncfusion/ej2-react-grids';
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import { RadioButtonComponent, CheckBoxComponent, SwitchComponent } from '@syncfusion/ej2-react-buttons';
import { L10n, setCulture, setCurrencyCode } from '@syncfusion/ej2-base';
import { DatePickerComponent } from '@syncfusion/ej2-react-calendars';
import { ProgressButtonComponent } from '@syncfusion/ej2-react-splitbuttons';
import { MultiSelectComponent, CheckBoxSelection } from '@syncfusion/ej2-react-dropdowns';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { withTranslation } from "react-i18next";
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import LineLoadingService from './LineLoadingService';
import lineCap from '../../../assets/img/line-cap.jpg';
import time from '../../../assets/img/time.svg';
import machines from '../../../assets/img/sew-machine.svg';
import errorMessages from '../../errorHandler/errorMessages';
import DistributedDayWiseLoadModel from './DistributedDayWiseLoadModel';
import DistributedDayWiseHourlyLoadModel from './DistributedDayWiseHourlyLoadModel';
import LineLoadingModel from './LineLoadingModel';
import ScheduledLineModel from './ScheduledLineModel';
import ProdConfModel from './ProdConfModel';
import CountDownClockComponent from './CountDownClockComponent';
import { element } from 'prop-types';

class Lineloading extends React.PureComponent {

    constructor(props) {
        super(...arguments);
        this.lineLoadingService = new LineLoadingService();
        this.updateStoreCommands = [
            {
                type: 'Update Store', buttonOption: {
                    iconCss: 'e-icons',
                    // content: 'Update Store',
                    cssClass: 'e-flat e-btn update-Store'
                }
            }
        ];
        this.updateSapCommands = [
            {
                buttonOption: {
                    content: 'Update Sap', cssClass: 'e-flat e-primary'
                }
            }
        ];
        this.state = {
            showDialog: false,
            plants: [],
            units: [],
            unitProductionLines: [],
            scheduledTabUnits: [],
            scheduledGridSelectedData: {
                saidId: null,
                lineItemNo: null,
                buyerPoNo: null
            },
            selectedScheduledTabUnitId: null,
            load: [],
            dynamicLoads: [],
            scheduledLines: [],
            supervisorList: [],
            hourlyGridData: [],
            maxLoadQty: null,
            maxLoadsList: [],
            productionConfirmationData: [],
            maxAllowedMachines: null,
            configKeys: {},
            transferObj: {
                isTransferTo: false,
                transferLoadQty: null,
                transferToLine: null
            },
            format: { type: 'dateTime', format: 'dd/MM/yyyy' },
            confirmationGridFormat: { type: 'dateTime', format: 'dd/MM/yyyy HH:mm:ss' },
            selectedPlantName: null,
            selectedUnitName: null,
            selectedLineName: [],
            selectedReportPlantId: null,
            selectedReportUnitId: null,
            selectedReportStartDate: null,
            selectedReportEndDate: null,
            lineLoadDialog: false,
            isFreezeHourlyGridRows: false,
            selectedHourlyViewDate: null,
            selectedPlantForLineItem: null,
            rowSelectedScheduledQty: null,
            // lineLoadDialogOverLay: true,
            poDialog: false,
            prodOrderLineDetails: [],
            formUnitProductionLines: [],
            isEdit: false,
            spinRight: { position: 'Right' },
            animationSettings: { effect: 'Zoom' },
            date: new Date(),
            currentDate: new Date(),
            selectProdDate: new Date(),
            rowSelectedScheduleStartDate: new Date(),
            editSettingProdConf: { allowEditing: true, allowEditOnDblClick: true },
            daywiseGridEditSettings: { allowEditing: true, allowEditOnDblClick: false },
            hourwiseGridEditSettings: { allowEditing: true, allowEditOnDblClick: true },
            buyerPoList: [],
            saidNoList: [],
            allSaidList: [],
            lineItemList: [],
            productionOrderList: [],
            loadedLines: [],
            actualLoadedLine: [],
            scheduleList: [],
            selectedBuyerPo: [],
            selectedSaidNo: [],
            selectedLineItem: [],
            isTecho: true,
            showLoadlineFrom: true,
            lineLoadView: true,
            isShowDaywiseView: false,
            isShowHourlyView: false,
            loadedLineIdToDelete: null,
            isShowConfirmationDialog: false,
            clockCount: null,
            isClockCountRunning: false,
            productionConfirmationdiv: false,
            selectedPlantNameForDistributed:null,
            isDatePast:false,
            IFilter: { type: 'CheckBox' },
            FilterSettingsModel: { type: 'Menu' },
            sheduleExportProperties:{fileName:'Line_Schedule_Report.xlsx'},
            loadexcelExportProperties:{fileName:'Line_Load_Report.xlsx'},
            isStartDateDisable: true,
            model: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0,null),
            rollBackModel: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0,null),
            toasterMessage: [{ title: 'Information!', content: 'Please fill all mandatory fields !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
            { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
            { title: 'Warning!', content: 'Production Confirmation is not zero , hence delete not allowed !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
            { title: 'Warning!', content: 'Production Order already exist !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
            { title: 'Success!', content: 'Store Updated successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
            { title: 'Information!', content: 'You cannot add defect & rework qty if it is already zero !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
            { title: 'Information!', content: 'Qty should be greater than zero !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
            { title: 'Success!', content: 'Saved successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
            { title: 'Information!', content: 'Edit not allowed as Date is less than current date !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
            { title: 'Information!', content: 'Loaded qty cannot be zero !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
            { title: 'Information!', content: 'Search completed !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
            { title: 'Information!', content: 'Please select any Filter !! !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
            { title: 'Information!', content: 'Refreshed Successfully !!', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
            { title: 'Success!', content: 'Download Report successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' }



            ],
            daywiseGridcommands: [{ type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat' } },
            { type: 'Save', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
            { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } }],
            editSettingsProductionOrder: { allowEditing: false, allowAdding: true, allowDeleting: true, allowEditOnDblClick: false },
            productionOrderommands: [{ type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat edit-btn' } },
            { type: 'Save', buttonOption: { iconCss: 'e-icons e-update', cssClass: 'e-flat' } },
            { type: 'Cancel', buttonOption: { iconCss: 'e-icons e-cancel-icon', cssClass: 'e-flat' } },
            { type: 'Revert', buttonOption: { iconCss: 'e-icons', cssClass: 'e-flat revert-view' } }],
            commands: [{
                type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat edit-btn' }
            },
            {
                type: 'SaveSap', buttonOption: { iconCss: 'e-icons e-save', cssClass: 'e-flat edit-btn' }
            },
            { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },],
            confirmButton: [{
                click: (args) => {
                    this.delete(args);
                },
                buttonModel: { content: 'Yes', isPrimary: true }
            },
            {
                click: (args) => {
                    this.cancelConformDialog(args);
                },
                buttonModel: { content: 'No' }
            }],
        }
        const { t, i18n } = props;

        this.onClickDistributedReport = this.onClickDistributedReport.bind(this);
    }


    //#################################################################### SCHEDULE TAB ###############################################################//

    //######### API-CALLS ###########//

    componentDidMount() {
        this.getPlantList()
        this.getSupervisorList()
        this.getConfgKey()
        this.handleCountdown()
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.isClockCountRunning !== prevState.isClockCountRunning) {
            switch (this.state.isClockCountRunning) {
                case true:
                    this.handleStartClockCountDown();
            }
        }
    }

    async getUnitListForScheduledTab(plantId) {
        let response = (await this.lineLoadingService.getUnitList(plantId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ scheduledTabUnits: response.data.data, selectedScheduledTabUnitId: null })
        }
    }

    dateConverter = function (data) {
        if (data != undefined && data != null && data.date !== null && data.date !== undefined) {
            let date = data.date;
            let time = data.time;
            //let startDate = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 23, 59)
            return new Date(date.year, date.month - 1, date.day, time.hour, time.minute, time.second)
        }
    }

    async getProdConf(LoadedLineId) {
        let response = (await this.lineLoadingService.getProdConf(LoadedLineId))
        if ((response != undefined) && (response.data.status == "success")) {
            let prodConfList = [];
            response.data.data.forEach(element => {
                let defectQty = { id: null, value: 0 };
                let reworkQty = { id: null, value: 0 };
                if (element.defectedPieces != null)
                    element.defectedPieces.forEach(defectedPiece => {
                        defectQty.id = defectedPiece.id;
                        defectQty.value = parseInt(defectQty.value) + parseInt(defectedPiece.value);

                    })
                if (element.reworkPieces != null)
                    element.reworkPieces.forEach(reworkPiece => {
                        reworkQty.id = reworkPiece.id;
                        reworkQty.value = parseInt(reworkQty.value) + parseInt(reworkPiece.value);
                    })
                prodConfList.push(new ProdConfModel(element.id, element.loadTableId, element.acceptedQty, element.terminalMac,
                    element.loadedDeviceProcessId, element.status, element.requestType, element.isIgnored, element.isRolledBack, this.dateConverter(element.createdDateTime), defectQty, reworkQty,element.createdBy,element.modifiedBy,element.remarks))
            })
            this.setState({ productionConfirmationData: prodConfList });
        }
    }

    async editProdConf(editedProdConfObj) {
        let response = (await this.lineLoadingService.editProdConf(editedProdConfObj))
        if ((response != undefined) && (response.data.status == "success")) {
            this.getProdConf(editedProdConfObj.loadTableId);
            this.toastObj.show(this.state.toasterMessage[7]);
        } else {
            this.toastObj.show(this.state.toasterMessage[1]);
        }
    }

    async revertProdConf(prodConfId, loadTableId) {
        let response = (await this.lineLoadingService.revertConfirmation(prodConfId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.getProdConf(loadTableId);
            this.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-info toast-icons' });
        } else {
            this.toastObj.show({ title: 'Info!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
        }
    }

    async getConfgKey() {
        let response = (await this.lineLoadingService.getConfgKey())
        if ((response != undefined) && (response.data.data.length > 0) && (response.data.status == "success")) {
            let configObj = {};
            response.data.data.forEach(element => {
                configObj[element.keyName] = /^\d+$/.test(element.keyValue) == true ? Number(element.keyValue) : element.keyValue;
            })
            this.setState({ configKeys: configObj })
        }
    }

    async getMaxLoad(saidId, lineItemNo, buyerPoNo, prodOrderId) {
        const { t, i18n } = this.props;
        let response = (await this.lineLoadingService.getMaxLoad(saidId, lineItemNo, buyerPoNo, prodOrderId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ maxLoadQty: response.data.data.maxLoad, maxLoadsList: response.data.data.materialLoadDtos })
        } else if ((response != undefined) && (response.data.status == "fail")) {
            global.hideLoader()
            this.toastObj.show(this.createToaster(t(errorMessages[response.data.data.errorCode]), "Error"));
        }
        else {
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[1]);
        }
    }

    async getBuyerPoList(plantIds, unitName) {
        let response = (await this.lineLoadingService.getBuyerPoList(plantIds, unitName))
        if ((response != undefined) && (response.data.status == "success")) {
            let uniqueSaidList = [];
            uniqueSaidList = response.data.data.filter((value, index, array) => array.findIndex(t => (t.saidNr === value.saidNr)) === index)

            this.setState({
                allSaidList: response.data.data, saidNoList: uniqueSaidList, lineItemList: [], selectedSaidNo: [], productionOrderList: [], loadedLines: [],
                actualLoadedLine: [], scheduledLines: [], selectedBuyerPoSaidId: null, selectedLineItem: [], selectedBuyerPo: []
            })
        }
        global.hideLoader();
    }

    async getSupervisorList() {
        let response = (await this.lineLoadingService.getSupervisorList())
        if ((response != undefined) && (response.data.length > 0)) {
            this.setState({ supervisorList: response.data })
        }
    }

    async getFormUnitProductionLines(unitId, prodLineId) {
        let response = (await this.lineLoadingService.getUnitProductionLines(unitId))
        if ((response != undefined) && (response.data.status == "success")) {
            let maxAllowedMachines = null;
            response.data.data.forEach(item => {
                if (item.id == prodLineId)
                    maxAllowedMachines = item.numberOfMachines;
            })
            this.state.formUnitProductionLines = response.data.data
            this.state.maxAllowedMachines = maxAllowedMachines;
        }
    }

    async getProductionOrder(saidId, lineItem, buyerPo) {
        let response = (await this.lineLoadingService.getProductionOrderList(saidId, lineItem, buyerPo))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ productionOrderList: response.data.data })
        } else {
            this.setState({ productionOrderList: [] })
        }
    }

    async getLoadedLines(plantId, unitId) {
        let that = this;
        const { supervisorList } = that.state;
        let response = (await that.lineLoadingService.getLoadedLines(plantId, unitId))
        if ((response != undefined) && (response.data.status == "success")) {
            let lines = [];
            response.data.data.map(item => {
                let distributedDayWiseLoad = [];
                item.distributedDayWiseLoadTableDtos.map(daywiseItem => {
                    let distributedHourlyLoad = [];
                    let convertedProdDate = new Date(Date.UTC(daywiseItem.prodDate.year, daywiseItem.prodDate.month - 1, daywiseItem.prodDate.day))
                    daywiseItem.distributedDayWiseHourlyLoadTableDtos.map(hourlyWiseItem => {
                        distributedHourlyLoad.push(new DistributedDayWiseHourlyLoadModel(hourlyWiseItem.id, hourlyWiseItem.hourType, hourlyWiseItem.labour, hourlyWiseItem.noOfHours, hourlyWiseItem.noOfmachines,
                            hourlyWiseItem.skilledWorker, hourlyWiseItem.targetQty, hourlyWiseItem.unskilledWorker, daywiseItem.id))
                    })
                    distributedDayWiseLoad.push(new DistributedDayWiseLoadModel(daywiseItem.id, daywiseItem.distributedQty, daywiseItem.leftQty, daywiseItem.confQty, daywiseItem.workingHrs,
                        daywiseItem.reworkQty, daywiseItem.noOfmachines, daywiseItem.dataPoint, daywiseItem.saidNr, daywiseItem.lineId, daywiseItem.lineItemNr, daywiseItem.materialKd,
                        daywiseItem.labour, daywiseItem.unskilledWorker, convertedProdDate, daywiseItem.loadId, daywiseItem.defectQty, daywiseItem.processId, daywiseItem.processName, daywiseItem.totalQtyLoaded,
                        daywiseItem.prodOrderId, daywiseItem.sam, daywiseItem.skilledWorker, distributedHourlyLoad, daywiseItem.targetWip))
                })
                let convertedStartDate = new Date(Date.UTC(item.startdate.year, item.startdate.month - 1, item.startdate.day))
                let convertedEndDate = new Date(Date.UTC(item.endDate.year, item.endDate.month - 1, item.endDate.day))
                let supervisorName = null;
                supervisorList.forEach(supervisorObj => {
                    if (supervisorObj.id == item.supervisor)
                        supervisorName = supervisorObj.firstName + " " + supervisorObj.lastName;
                })
                lines.push(new LineLoadingModel(item.id, item.supervisor, item.fmName, item.qaName, item.apmId, item.unitId, item.lineName, item.unitName, item.scheduleId, item.apm, item.fmId, item.lineId, item.processId, item.prodOrderId, item.qaId, item.bulkUpload, item.buyerPoNr, item.confQty, item.dataPoint, item.defectQty, convertedStartDate, convertedEndDate, item.labour, item.leftQty, item.lineItemNr, item.materialKd,
                    item.noOfmachines, item.processName, item.saidNr, item.sam, item.sapConfQty, item.skilledWorker, item.status, item.totalQtyLoaded, item.unskilledWorker, item.workingHrs,
                    item.yellowCeiling, item.reworkQty, null, null, distributedDayWiseLoad, item.wipTargetTime, supervisorName, item.materialDesc, item.prodOrderRate, item.altBom, item.isTeco, item.sapRejectQty,item.modifiedOn))
            })
            if (that.state.isEdit) {
                that.loadedLinesObj.properties.dataSource = lines;
                that.setState({
                    lineLoadDialog: false, loadedLines: lines, actualLoadedLine: lines, selectedHourlyViewDate: null, isShowDaywiseView: false, isShowHourlyView: false, isEdit: false, maxLoadQty: null, maxLoadsList: [], productionConfirmationdiv: false, maxAllowedMachines: null,
                    model: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0,null),
                    rollBackModel: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0,null)

                });
            } else {
                that.setState({ loadedLines: lines, actualLoadedLine: lines, isTecho: true })
            }
        } else {
            that.setState({ loadedLines: [], isShowHourlyView: false, actualLoadedLine: [], isTecho: true })
            that.toastObj.show(that.state.toasterMessage[1]);
        }
        this.handleClick();
        global.hideLoader();
    }


    async getScheduledLines(plantId, unitId) {
        let response = (await this.lineLoadingService.getScheduledLines(plantId, unitId))
        if ((response != undefined) && (response.data.status == "success")) {
            let scheduledLines = [];
            response.data.data.map(item => {
                let convertedScheduledStartDate = new Date(Date.UTC(item.scheduleStartDateTime.date.year, item.scheduleStartDateTime.date.month - 1, item.scheduleStartDateTime.date.day, item.scheduleStartDateTime.time.hour, item.scheduleStartDateTime.time.minute))
                let convertedScheduledEndDate = new Date(Date.UTC(item.scheduleEndDateTime.date.year, item.scheduleEndDateTime.date.month - 1, item.scheduleEndDateTime.date.day, item.scheduleEndDateTime.time.hour, item.scheduleEndDateTime.time.minute))

                scheduledLines.push(new ScheduledLineModel(item.id, item.linePlanningInfoDto.fmName, item.linePlanningInfoDto.qaName, item.linePlanningInfoDto.apmId, item.prodLineId, item.linePlanningInfoDto.unitId, item.lineMonthlyWorkLoadScheduleDto.prodLine.unit.name, item.buyerPoNo, item.saidId, item.noOfMachineToUse,
                    item.lineMonthlyWorkLoadScheduleDto.prodLine.workingHour, convertedScheduledStartDate, convertedScheduledEndDate, item.scheduleQtyForProduction, item.unLoadedQty, item.linePlanningInfoDto.totalQtyToProduced, item.linePlanningInfoDto.fmId, item.linePlanningInfoDto.apm,
                    item.linePlanningInfoDto.sam, item.linePlanningInfoDto.qaId, item.lineMonthlyWorkLoadScheduleDto.prodLine.lineName, item.lineItemNo, item.linePlanningInfoDto.lineItemDesc, item.lineMonthlyWorkLoadScheduleDto.prodLine.supervisor))
            })
            this.setState({ scheduledLines })
        } else {
            this.setState({ loadscheduledLinesedLines: [] })
            this.toastObj.show(this.state.toasterMessage[1]);
        }
        global.hideLoader()
    }

    async addLineLoad(modelData) {
        const { t, i18n } = this.props;
        const { selectedBuyerPoSaidId, scheduledGridSelectedData, selectedPlantForLineItem, selectedScheduledTabUnitId, model, rowSelectedScheduleStartDate } = this.state;
        let response = (await this.lineLoadingService.addLineLoad(modelData))
        if ((response != undefined) && (response.data.status == "success")) {
            await this.getProductionOrder(scheduledGridSelectedData.saidId, scheduledGridSelectedData.lineItemNo, scheduledGridSelectedData.buyerPoNo);
            this.setState({
                isEdit: false, maxLoadQty: null, maxLoadsList: [], model: new LineLoadingModel(null, model.supervisor, model.fmName, model.qaName, model.apmId,
                    model.unitId, model.lineName, model.unitName, model.scheduleId, model.apm, model.fmId, model.lineId, null, null, model.qaId, 1, scheduledGridSelectedData.buyerPoNo, 0, false,
                    0, rowSelectedScheduleStartDate, null, 0, 0, scheduledGridSelectedData.lineItemNo, null, 0, null, scheduledGridSelectedData.saidId, null, 0, 0, true, null, 0,
                    model.workingHrs, null, 0, null, null, [], model.wipTargetTime, null, null, null, model.altBom, model.isTeco, 0)
            }, () => {
                this.toastObj.show({ title: 'Information!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
                this.getLoadedLines(selectedPlantForLineItem, selectedScheduledTabUnitId);

            })
        } else if ((response != undefined) && (response.data.status == "fail")) {
            global.hideLoader();
            this.toastObj.show(this.createToaster(response.data.data.errorCode == 441 ? "Loaded Qty cannot be less than confirmed qty" : response.data.data.errorCode == 443 ? "Material Id is not present in BOM" : t(errorMessages[response.data.data.errorCode]), "Error"));

        }
        else {
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[1]);
        }
    }

    async editLineLoad(modelData) {
        const { t, i18n } = this.props;
        const { selectedBuyerPoSaidId, scheduledGridSelectedData, selectedPlantForLineItem, selectedScheduledTabUnitId, transferObj } = this.state;
        let response;
        if ((transferObj.isTransferTo == true) && (transferObj.transferLoadQty != null) && (transferObj.transferToLine != null)) {
            response = (await this.lineLoadingService.transferLoad(this.state.model, transferObj.transferLoadQty, transferObj.transferToLine));

        } else {
            response = (await this.lineLoadingService.editLineLoad(modelData));
        }
        if ((response != undefined) && (response.data.status == "success")) {
            await this.getProductionOrder(scheduledGridSelectedData.saidId, scheduledGridSelectedData.lineItemNo, scheduledGridSelectedData.buyerPoNo)
            document.body.classList.remove('overflow-hidden-body');
            this.setState({
                lineLoadDialog: false, isEdit: false, isShowDaywiseView: false, isShowHourlyView: false, productionConfirmationdiv: false, maxLoadQty: null, maxLoadsList: [], transferObj: { isTransferTo: false, transferLoadQty: null, transferToLine: null },
                model: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, null, false, 0,null)
            }, () => {
                this.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
                this.getLoadedLines(selectedPlantForLineItem, selectedScheduledTabUnitId);

            })
        } else if ((response != undefined) && (response.data.status == "fail")) {
            global.hideLoader()
            this.toastObj.show(this.createToaster(response.data.data.errorCode == 441 ? "Loaded Qty cannot be less than confirmed qty" : t(errorMessages[response.data.data.errorCode]), "Error"));
        }else {
            this.toastObj.show(this.state.toasterMessage[1]);
            global.hideLoader();
        }
    }

    async deleteLoadedLine(loadedLineId,modifiedOn) {
        const { t, i18n } = this.props;
        const { selectedBuyerPoSaidId, scheduledGridSelectedData, selectedScheduledTabUnitId, selectedPlantForLineItem, } = this.state;
        let response = (await this.lineLoadingService.deleteLoadedLine(loadedLineId,modifiedOn))
        if ((response != undefined) && (response.data.status == "success")) {
            await this.getProductionOrder(scheduledGridSelectedData.saidId, scheduledGridSelectedData.lineItemNo, scheduledGridSelectedData.buyerPoNo)
            this.getLoadedLines(selectedPlantForLineItem, selectedScheduledTabUnitId);
            this.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
        } else if ((response != undefined) && (response.data.status == "fail")) {
            global.hideLoader()
            this.toastObj.show(this.createToaster(t(errorMessages[response.data.data.errorCode]), "Error"));  
        }
        else {
            this.toastObj.show(this.state.toasterMessage[1]);
            global.hideLoader();
        }
        this.setState({ loadedLineIdToDelete: null })
    }


    // globalDateConverter =(data)=> {
    //     if (data != undefined && data != null && data.date !== null && data.date !== undefined) {
    //     let date = data.date;
    //     //let startDate = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 23, 59)
    //     return new Date(date.year, date.month - 1, date.day,23, 59)
    //      }
    // }

    updateStore = async (args) => {
        let that = this;
        const { t, i18n } = that.props;
        // that.progressBtn.start();
        global.showLoader();
        let response = (await that.lineLoadingService.updateStore(args.rowData.saidId, args.rowData.lineItemNo, args.rowData.buyerPoNo));
        if ((response != undefined) && (response.data.status == "success")) {
            that.toastObj.show(that.state.toasterMessage[4]);
            // that.progressBtn.stop();
            await that.getProductionOrder(args.rowData.saidId, args.rowData.lineItemNo, args.rowData.buyerPoNo)
        } else if ((response != undefined) && (response.data.status == "fail")) {
            if(response.data.data.errorCode == '454'){
                that.toastObj.show(that.createToaster(response.data.data.message, "Error"));
            }else{
                that.toastObj.show(that.createToaster(t(errorMessages[response.data.data.errorCode]), "Error"));
            }
            
        }
        else {
            that.toastObj.show(that.state.toasterMessage[1]);
            // that.progressBtn.stop();
        }
        global.hideLoader()
    }

    async updateSap(args) {
        let that = this;
        const { t, i18n } = that.props;
        // that.progressBtn.start();
        // let confQtyleft = args.rowData.confQty - args.rowData.sapConfQty;
        // let rejectQtyleft = args.rowData.confQty - args.rowData.sapConfQty;

        // if ((args.rowData.confQty == 0 || confQtyleft == 0) && (rejectQtyleft==0)) {
        //     this.toastObj.show({ title: 'Information!', content: "No Qty available to push in Sap!!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
        // }
        // else {
        global.showLoader();
        let response = (await that.lineLoadingService.updateSap(args.rowData.id,args.rowData.modifiedOn));
        if ((response != undefined) && (response.data.status == "success")) {
            this.toastObj.show({ title: 'Success!', content: response.data.message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' });
            // that.progressBtn.stop();
        }
        else if ((response != undefined) && (response.data.status == "unsuccess") && (response.status != 401)) {
            this.toastObj.show({ title: 'Sap Error!', content: response.data.message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
        }
        else if ((response != undefined) && (response.status == 401)) {
            this.toastObj.show({ title: 'Unauthorized!', content: response.data.message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
        }else if((response != undefined) && (response.data.status == "fail") && (response.data.data.errorCode == '447')){
            this.toastObj.show(this.createToaster("Load Table is Updated Please Refresh the Page", "Error"));
        }else {
            that.toastObj.show(that.state.toasterMessage[1]);
            // that.progressBtn.stop();
        }
        global.hideLoader()
        // }
        this.refreshLines(false);
    }

    async generateReport(plantId, unitId) {
     //   const { t, i18n } = this.props;
        if(plantId == null || unitId == null ){
            this.toastObj.show({ title: 'Information!', content: plantId ==null? 'Please Select Plant':'Please Select Unit', cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
        }else{
            global.showLoader();
            let response = (await this.lineLoadingService.generateReport(plantId, unitId))
            if ((response != undefined) && (response.data.status == "success")) {
                this.downloadReport(response.data.message)
            } else {
                this.toastObj.show({ title: 'Information!', content: response.data.message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
                global.hideLoader();
            }
        }
    }

    async downloadReport(reportId) {
        let response = (await this.lineLoadingService.downloadReport(reportId))
        if ((response != undefined) && (response.status == 200)) {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'Attendance_Report.xlsx'); //or any other extension
        //    link.setAttribute('download', response.headers.get('Content-Disposition'));
           document.body.appendChild(link);
            link.click();
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[13]);
        }
        else {
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[1]);
        }
    }
    //############### HANDLE-CHANGE ##############//

    onChangeMultiSelectPlantToGetBuyerPo = (event) => {
        let that = this;
        if (event.e != undefined) {
            that.setState({
                selectedPlantForLineItem: event.value, saidNoList: [], selectedScheduledTabUnitId: null, scheduledTabUnits: [], lineItemList: [], selectedSaidNo: [],
                isTecho: true, scheduledLines: [], selectedBuyerPoSaidId: null, selectedLineItem: [], selectedBuyerPo: [], productionOrderList: [], loadedLines: [], actualLoadedLine: [], buyerPoList: []
            })
            that.getUnitListForScheduledTab(event.value);
        }
    }

    onChangeScheduledTabUnit = async (event) => {
        let that = this;
        const { selectedPlantForLineItem } = that.state;
        if (event.e != undefined) {
            global.showLoader();
            that.setState({
                selectedScheduledTabUnitId: event.value, lineItemList: [], selectedSaidNo: [],
                isTecho: true, scheduledLines: [], selectedBuyerPoSaidId: null, selectedLineItem: [], selectedBuyerPo: [], productionOrderList: [], loadedLines: [], actualLoadedLine: [], buyerPoList: []
            });
            await axios.all([that.getLoadedLines(selectedPlantForLineItem, event.value), that.getScheduledLines(selectedPlantForLineItem, event.value)]);
            this.mergeGridDataForMultiselect()
        }
    }


    async mergeGridDataForMultiselect() {
        let that = this;
        const { loadedLines, scheduledLines } = that.state;
        let mergedArray = scheduledLines.concat(loadedLines);
        let modeifiedArray = [];
        mergedArray.forEach(element => {
            if (element.buyerPoNo != undefined && element.lineItemNo != undefined && element.saidId != undefined)
                modeifiedArray.push({ buyerPoNo: element.buyerPoNo, lineItemNo: element.lineItemNo, saidNo: element.saidId }) // scheduled lines
            else
                modeifiedArray.push({ buyerPoNo: element.buyerPoNr, lineItemNo: element.lineItemNr, saidNo: element.saidNr }) // loaded lines
        })
        let uniqueBuyerPoNo = modeifiedArray.filter((value, index, array) => array.findIndex(t => (t.buyerPoNo === value.buyerPoNo)) === index);
        let uniqueSaidNo = modeifiedArray.filter((value, index, array) => array.findIndex(t => (t.saidNo === value.saidNo)) === index);
        let uniqueLineItemNo = modeifiedArray.filter((value, index, array) => array.findIndex(t => (t.lineItemNo === value.lineItemNo)) === index);
        that.setState({ saidNoList: uniqueSaidNo, buyerPoList: uniqueBuyerPoNo, lineItemList: uniqueLineItemNo }, () => {
            that.scheduledLinesGridObj.clearFiltering();
            that.loadedLinesObj.clearFiltering();
        })
    }

    onChangeFilterMultiSelect = (event) => {
        // if (event.e != undefined && event.e != null)
            this.setState({ [event.element.id]: event.value })
    }

    handleClick = (event) => {
        let that = this;
        let filteredTecoLoads = [];
        if (event != undefined) {
            this.setState({ isTecho: event.checked });
        }
        if (this.state.isTecho) {
            that.state.actualLoadedLine.forEach(load => {
                if (load.isTeco != this.state.isTecho) {
                    filteredTecoLoads.push(load);
                }
            })
        }
        else {
            filteredTecoLoads = that.state.actualLoadedLine;
        }
        this.setState({ loadedLines: filteredTecoLoads });
    }

    search = () => {
        let that = this;
        const { selectedSaidNo, selectedBuyerPo, selectedLineItem } = that.state;
        that.scheduledLinesGridObj.clearFiltering();
        that.loadedLinesObj.clearFiltering();
        if (selectedSaidNo.length == 0 && selectedBuyerPo.length == 0 && selectedLineItem.length == 0) {
            that.toastObj.show(this.state.toasterMessage[11]);
        } else {
            if (selectedSaidNo.length > 0) {
                that.scheduledLinesGridObj.filterByColumn("saidId", "equal", selectedSaidNo);
                that.loadedLinesObj.filterByColumn("saidNr", "equal", selectedSaidNo);
            }
            if (selectedBuyerPo.length > 0) {
                that.scheduledLinesGridObj.filterByColumn("buyerPoNo", "equal", selectedBuyerPo);
                that.loadedLinesObj.filterByColumn("buyerPoNr", "equal", selectedBuyerPo);
            }
            if (selectedLineItem.length > 0) {
                that.scheduledLinesGridObj.filterByColumn("lineItemNo", "equal", selectedLineItem);
                that.loadedLinesObj.filterByColumn("lineItemNr", "equal", selectedLineItem);
            }
            that.toastObj.show(this.state.toasterMessage[10]);
        }
    }

    refreshLines = async (isSap) => {
        let that = this;
        const { selectedScheduledTabUnitId, selectedPlantForLineItem } = that.state;
        if (selectedScheduledTabUnitId != null && selectedPlantForLineItem != null) {
            global.showLoader();
            await axios.all([that.getLoadedLines(selectedPlantForLineItem, selectedScheduledTabUnitId), that.getScheduledLines(selectedPlantForLineItem, selectedScheduledTabUnitId)]);
            that.mergeGridDataForMultiselect();
            if (isSap) { that.toastObj.show(this.state.toasterMessage[12]); }

            global.hideLoader();
        }
    }

    onChangeFormDropdownSuperVisor = (event) => {
        const { model } = this.state;
        if (event.e != undefined)
            this.setState({ model: { ...model, [event.element.id]: event.value } })
    }

    onChangeFormDropdown = (event) => {
        const { model } = this.state;
        if (event.e != undefined)
            this.setState({ model: { ...model, [event.element.id]: event.value, lineName: event.itemData.lineName, supervisor: event.itemData.supervisor, fmId: event.itemData.unit.factoryManagerId, fmName: event.itemData.unit.factoryManagerName, noOfmachines: event.itemData.numberOfMachines }, maxAllowedMachines: event.itemData.numberOfMachines })
    }

    onChangeTransferLoadDropdown = (event) => {
        const { transferObj } = this.state;
        if (event.e != undefined)
            this.setState({ transferObj: { ...transferObj, [event.element.id]: event.value } })
    }

    async onChangeFormProductionOrderDropdown(event, that) {
        const { model, rowSelectedScheduledQty } = that.state;
        if (event.e != undefined) {
            let qty = null;
            await this.getMaxLoad(model.saidNr, event.itemData.lineItemNo, model.buyerPoNr, event.value);
            //code
            qty = Math.min(this.state.maxLoadQty, event.itemData.prodOrdQty, rowSelectedScheduledQty);
            // qty = this.state.maxLoadQty < event.itemData.prodOrdQty ? this.state.maxLoadQty : event.itemData.prodOrdQty;
            this.setState({ model: { ...model, [event.element.id]: event.value, materialDesc: event.itemData.materialDesc, totalQtyLoaded: qty, dataPoint: false, validateLeftQty: event.itemData.leftQtyToLoad, lineItemNr: event.itemData.lineItemNo, totalQty: event.itemData.prodOrdQty, sam: event.itemData.sam, processName: event.itemData.processName, processId: event.itemData.processName, materialKd: event.itemData.materialId, altBom: event.itemData.altBom } })
            console.log(this.state.model)
        }
    }

    onChangeFormTextField = (event) => {
        const { model } = this.state;
        this.setState({ model: { ...model, [event.target.id]: event.value.trim() } })
    }

    onChangeFormSwitchDataPoint = (event) => {
        const { model } = this.state;
        this.setState({ model: { ...model, dataPoint: event.checked } })
    }

    onChangeFormSwitchStatus = (event) => {
        const { model } = this.state;
        this.setState({ model: { ...model, status: event.checked } })
    }

    onChangeFormSwitchTransfer = (event) => {
        const { transferObj, rollBackModel, formUnitProductionLines } = this.state;
        if (event.event != undefined) {
            if (event.checked == true) {
                this.setState({ transferObj: { ...transferObj, isTransferTo: event.checked, transferLoadQty: null, transferToLine: null }, model: rollBackModel, hourlyGridData: [], isShowDaywiseView: false, isShowHourlyView: false, productionConfirmationdiv: false, selectedHourlyViewDate: null })
                this.hourWiseViewObj.dataSource = [];
            }
            if (event.checked == false) {
                let maxAllowedMachines = null;
                formUnitProductionLines.forEach(item => {
                    if (item.id == rollBackModel.lineId)
                        maxAllowedMachines = item.numberOfMachines;
                })
                this.setState({ transferObj: { ...transferObj, isTransferTo: event.checked, transferLoadQty: null, transferToLine: null }, maxAllowedMachines })
            }
        }
    }

    onChangeFormNumericText = (event) => {
        const { model } = this.state;
        if ((event.syntheticEvent != null) && (event.syntheticEvent != undefined)){
            this.setState({ model: { ...model, [event.target.id]: Number(event.value) } })
            
        }
    }

    onChangeTransferLoadQty = (event) => {
        const { transferObj } = this.state;
        this.setState({ transferObj: { ...transferObj, [event.target.id]: Number(event.value) } })
    }

    onChangeFormDatePicker = (event) => {
        const { model } = this.state;
        if ((event.event != null) && (event.value != null)) {
            let startDate = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 9)
            this.setState({ model: { ...model, [event.element.id]: startDate } })
        }
        else if ((event.event != null) && (event.value == null)) {
            this.setState({ model: { ...model, [event.element.id]: null } })
        } else {
            if (('values' in event) && (event.value != null)) {
                let startDate = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 9)
                this.setState({ model: { ...model, [event.element.id]: startDate } })
            }
        }
        // if (('values' in event) && (event.isInteracted == true)) {
        //     if (event.value != null) {
        //         let startDate = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 9)
        //         this.setState({ model: { ...model, [event.element.id]: startDate } })
        //     }
        // }
    }


    //############## GRID #############//


    rowSelectedScheduleGrid = (args) => {
        let selectedData = args.data;
        if (args.target.localName != "button") {
            this.state.scheduledGridSelectedData.saidId = selectedData.saidId;
            this.state.scheduledGridSelectedData.lineItemNo = selectedData.lineItemNo;
            this.state.scheduledGridSelectedData.buyerPoNo = selectedData.buyerPoNo;
            this.rowSelectedAsyncOperation(selectedData);
        }
    }

    async rowSelectedAsyncOperation(selectedData) {
        let that = this;
        let newDate = new Date();
        const { model, selectedBuyerPoSaidId } = that.state;
        await that.getProductionOrder(selectedData.saidId, selectedData.lineItemNo, selectedData.buyerPoNo);
        await that.getFormUnitProductionLines(selectedData.unitId, selectedData.prodLineId);
        let date = (selectedData.scheduleStartDateTime).getTime() > (new Date()).getTime() ? selectedData.scheduleStartDateTime : new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate(), 9);
        document.body.classList.add('overflow-hidden-body');
        that.setState({
            // lineLoadDialogOverLay: false,
            lineLoadDialog: true, rowSelectedScheduleStartDate: date, rowSelectedScheduledQty: selectedData.scheduleQtyForProduction,
            model: {
                ...model, apmId: selectedData.apmId, fmName: selectedData.fmName, qaName: selectedData.qaName, startdate: date, supervisor: selectedData.supervisor,
                unitName: selectedData.unitName, lineId: selectedData.prodLineId, unitId: selectedData.unitId, lineName: selectedData.lineName, saidNr: selectedData.saidId, buyerPoNr: selectedData.buyerPoNo,
                scheduleId: selectedData.id, apm: selectedData.apm, noOfmachines: null, workingHrs: selectedData.workingHour, qaId: selectedData.qaId, fmId: selectedData.fmId
            }
        })
    }

    rowSelectedLoadedLinesGrid = (args) => {    // TO BE IMPROVED !!
        // let that = this;
        // if ((args.target.classList.length > 2)) {
        //     if ((args.target.classList.contains('e-edit')) && (args.data != undefined)) {
        //         global.showLoader();
        //         that.state.scheduledGridSelectedData.saidId = args.data.saidNr;
        //         that.state.scheduledGridSelectedData.lineItemNo = args.data.lineItemNr;
        //         that.state.scheduledGridSelectedData.buyerPoNo = args.data.buyerPoNr;
        //         that.onEditClick(args.data);
        //     }
        // }
    }

    commandClick = (args) => {
        
        if (args.name == "commandClick") {
            if (args.commandColumn.type == "Delete") {
                // const id = args.rowData.id;
                this.setState({ isShowConfirmationDialog: true, loadedLineIdToDelete: args.rowData })
            }
            if (args.commandColumn.type == "Edit") {
                let that= this;
                global.showLoader();
                this.state.scheduledGridSelectedData.saidId = args.rowData.saidNr;
                this.state.scheduledGridSelectedData.lineItemNo = args.rowData.lineItemNr;
                this.state.scheduledGridSelectedData.buyerPoNo = args.rowData.buyerPoNr;
                let currentDate=new Date();
                if(args.rowData.startdate.getDate() == currentDate.getDate()  && 
                         args.rowData.startdate.getMonth() == currentDate.getMonth() &&
                          args.rowData.startdate.getFullYear() == currentDate.getFullYear()){
    
                    this.state.isStartDateDisable=false;
                }else{
                    this.state.isStartDateDisable=true;
                }
                
                this.onEditClick(args.rowData);
            }
            if (args.commandColumn.type == "SaveSap") {
                this.updateSap(args);
            }
        }
    }

    confCommandClick = (args) => {
        let that = this;
        if (args.name == "commandClick") {
            if (args.commandColumn.type == 'Revert') {
                if (args.rowData.requestType = "MOEP" && args.rowData.status == "Confirmed" && !args.rowData.isRolledBack) {
                    that.revertProdConf(args.rowData.id, args.rowData.loadTableId);
                }
                else {
                    args.cancel = true;
                    that.toastObj.show({ title: 'Info!', content: "Can Only Revert Confirmed or not yet rolled back confirmations!", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
                }

            }
        }
    }

    actionBegin = (args) => {                   // TO BE IMPROVED !!
        if (args.requestType == "delete") {
            args.cancel = true;
        }
    }

    onDoubleClickDaywiseGrid = (args) => {
        const { date } = this.state;
        let prodDate = new Date((args.rowData.prodDate).getFullYear(), (args.rowData.prodDate).getMonth(), (args.rowData.prodDate).getDate());
        let todaysDate = new Date((date).getFullYear(), (date).getMonth(), (date).getDate());
        let isFreezeHourlyGridRows = false;
        if (prodDate < todaysDate)
            isFreezeHourlyGridRows = true
        this.setState({ isShowHourlyView: true, hourlyGridData: args.rowData.distributedDayWiseHourlyLoadTableDtos, isFreezeHourlyGridRows, selectedHourlyViewDate: prodDate.toDateString() })
    }

    distributedDayWiseGridActionBegin = (args) => {
        let that = this;
        const { configKeys, maxAllowedMachines, date, model } = this.state;
        if (args.requestType == "save") {
            args.cancel = true;
            let dummyModel = cloneDeep(model);
            let prodDate = new Date((args.data.prodDate).getFullYear(), (args.data.prodDate).getMonth(), (args.data.prodDate).getDate());
            let todaysDate = new Date((date).getFullYear(), (date).getMonth(), (date).getDate())
            if (prodDate >= todaysDate) {
                if ((args.data.workingHrs <= configKeys.workingHour) && (args.data.noOfmachines <= configKeys.totalMachines)) {
                    dummyModel.distributedDayWiseLoadTableDtos.map(item => {
                        if (item.id == args.data.id) {
                            item.workingHrs = args.data.workingHrs;
                            item.noOfmachines = args.data.noOfmachines;
                        }
                    })
                    that.setState({ model: dummyModel })
                } else {
                    let msg = "Machines shouldn't be greater than - " + configKeys.totalMachines + " & Working hour shouldn't be greater than - " + configKeys.workingHour;
                    that.toastObj.show(that.createToaster(msg, "Information"));
                }
            } else {
                that.toastObj.show(that.state.toasterMessage[8]);
            }
            that.daywiseViewObj.closeEdit();
        }
    }

    distributedHourWiseGridActionBegin = (args) => {
        let that = this;
        const { isFreezeHourlyGridRows, configKeys, maxAllowedMachines, model } = that.state;
        if (args.requestType == "save") {
            args.cancel = true;
            let dummyModel = cloneDeep(model);
            if (isFreezeHourlyGridRows == false) {
                if ((args.data.labour <= configKeys.labour) && (args.data.noOfHours <= configKeys.workingHour) && (args.data.noOfmachines <= configKeys.totalMachines) && (args.data.skilledWorker <= configKeys.skilledWorker) && (args.data.unskilledWorker <= configKeys.unskilledWorker)) {
                    dummyModel.distributedDayWiseLoadTableDtos.map(item => {
                        if (item.id == args.data.distributedDayWiseLoadTableDtoLoadId) {
                            item.distributedDayWiseHourlyLoadTableDtos.map(hourlyObj => {
                                if (hourlyObj.id == args.data.id) {
                                    hourlyObj.labour = args.data.labour;
                                    hourlyObj.noOfHours = args.data.noOfHours;
                                    hourlyObj.noOfmachines = args.data.noOfmachines;
                                    hourlyObj.skilledWorker = args.data.skilledWorker;
                                    hourlyObj.unskilledWorker = args.data.unskilledWorker;
                                }
                            })
                            that.hourWiseViewObj.dataSource = item.distributedDayWiseHourlyLoadTableDtos;
                        }
                    })
                    that.setState({ model: dummyModel })
                } else {
                    let msg = "Max value allowed Machines - " + configKeys.totalMachines + " Working Hour -" + configKeys.workingHour + " Labour -" + configKeys.labour + " Skilled Worker -" + configKeys.skilledWorker + " Unskilled Worker -" + configKeys.unskilledWorker;
                    that.toastObj.show(that.createToaster(msg, "Information"));
                }
            } else {
                that.toastObj.show(that.state.toasterMessage[8]);

            }

            that.hourWiseViewObj.closeEdit();
        }
    }

    async onEditClick(selectedLoadedLine) { //to do
        const { model, transferObj, selectedBuyerPoSaidId, scheduledLines, scheduledGridSelectedData } = this.state;
        let totalQty = null;
        let validateLeftQty = null;
        await axios.all([this.getFormUnitProductionLines(selectedLoadedLine.unitId, selectedLoadedLine.lineId), this.getMaxLoad(selectedLoadedLine.saidNr, selectedLoadedLine.lineItemNr, selectedLoadedLine.buyerPoNr, selectedLoadedLine.prodOrderId),
        this.getProdConf(selectedLoadedLine.id), this.getProductionOrder(scheduledGridSelectedData.saidId, scheduledGridSelectedData.lineItemNo, scheduledGridSelectedData.buyerPoNo)
        ])
        
        this.state.productionOrderList.map(item => {
            if (selectedLoadedLine.prodOrderId == item.prodOrdNr) {
                validateLeftQty = item.leftQtyToLoad;
                totalQty = item.prodOrdQty;
            }
        })
        let rowSelectedScheduledQty = null;
        scheduledLines.forEach(item => {
            if (item.id == selectedLoadedLine.scheduleId)
                rowSelectedScheduledQty = item.scheduleQtyForProduction;
        })
        let createModel = new LineLoadingModel(selectedLoadedLine.id, selectedLoadedLine.supervisor, selectedLoadedLine.fmName, selectedLoadedLine.qaName, selectedLoadedLine.apmId, selectedLoadedLine.unitId, selectedLoadedLine.lineName, selectedLoadedLine.unitName, selectedLoadedLine.scheduleId, selectedLoadedLine.apm, selectedLoadedLine.fmId, selectedLoadedLine.lineId, selectedLoadedLine.processId, selectedLoadedLine.prodOrderId, selectedLoadedLine.qaId, selectedLoadedLine.bulkUpload,
            selectedLoadedLine.buyerPoNr, selectedLoadedLine.confQty, selectedLoadedLine.dataPoint, selectedLoadedLine.defectQty, selectedLoadedLine.startdate, selectedLoadedLine.endDate, selectedLoadedLine.labour, selectedLoadedLine.leftQty, selectedLoadedLine.lineItemNr, selectedLoadedLine.materialKd,
            selectedLoadedLine.noOfmachines, selectedLoadedLine.processName, selectedLoadedLine.saidNr, selectedLoadedLine.sam, selectedLoadedLine.sapConfQty, selectedLoadedLine.skilledWorker, selectedLoadedLine.status, selectedLoadedLine.totalQtyLoaded, selectedLoadedLine.unskilledWorker,
            selectedLoadedLine.workingHrs, selectedLoadedLine.yellowCeiling, selectedLoadedLine.reworkQty, totalQty, validateLeftQty, selectedLoadedLine.distributedDayWiseLoadTableDtos, selectedLoadedLine.wipTargetTime, null, selectedLoadedLine.materialDesc, selectedLoadedLine.prodOrderRate, selectedLoadedLine.altBom, selectedLoadedLine.isTeco, selectedLoadedLine.sapRejectQty,selectedLoadedLine.modifiedOn)
        document.body.classList.add('overflow-hidden-body');
        this.setState({ lineLoadDialog: true, rowSelectedScheduledQty, model: createModel, rollBackModel: createModel, isEdit: true, transferObj: { ...transferObj, isTransferTo: false } }, () => {
            global.hideLoader();
            this.newTransferLoads();
            // if(this.state.model.startdate){
            //     console.log(this.state.currentDate > (this.state.model.startdate && this.state.model.endDate)  ? this.setState({isDatePast:true}) : this.setState({isDatePast:false}));
            // }
        });
    }

    delete = (args) => {
        const { loadedLineIdToDelete } = this.state;
        if (loadedLineIdToDelete.confQty == 0) {
            global.showLoader();
            this.state.scheduledGridSelectedData.saidId = loadedLineIdToDelete.saidNr;
            this.state.scheduledGridSelectedData.lineItemNo = loadedLineIdToDelete.lineItemNr;
            this.state.scheduledGridSelectedData.buyerPoNo = loadedLineIdToDelete.buyerPoNr;
            this.deleteLoadedLine(loadedLineIdToDelete.id,loadedLineIdToDelete.modifiedOn);
            this.setState({ isShowConfirmationDialog: false })
        } else {
            this.toastObj.show(this.state.toasterMessage[2]);
        }
    }

    prodConfGridActionBegin = (args) => {
        let that = this;
        if (args.requestType === 'beginEdit') {
            if (args.rowData.isIgnored || args.rowData.requestType == "Sap" || args.rowData.status == "Confirmed") {
                args.cancel = true;
                that.toastObj.show({ title: 'Info!', content: "Cannot Edit Ignored or Sap or Already Confirmed confirmations", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
            }
            else if (args.rowData.loadedDeviceProcessId == null || args.rowData.isRolledBack) {
                args.cancel = true;
                that.toastObj.show({ title: 'Info!', content: "Cannot Edit Rolled back Confirmations", cssClass: 'e-toast-info', icon: 'e-info toast-icons' });
            }
        }
        if (args.requestType == "save") {
            args.cancel = true;
            if (((args.previousData.defectedPieces[0].value == 0) && (args.data.defectedPieces[0].value != 0)) || ((args.previousData.reworkPieces[0].value == 0) && (args.data.reworkPieces[0].value != 0))) {
                that.toastObj.show(that.state.toasterMessage[5]);
            }
            else if ((args.data.defectedPieces[0].value >= 0) && (args.data.reworkPieces[0].value >= 0) && (args.data.acceptedQty >= 0)) {
                let defects = args.data.defectedPieces[0].value == 0 ? null : { id: args.data.defectedPieces[0].id, value: args.data.defectedPieces[0].value }
                let reworks = args.data.reworkPieces[0].value == 0 ? null : { id: args.data.reworkPieces[0].id, value: args.data.reworkPieces[0].value }
                let editedProdConf = new ProdConfModel(args.data.id, args.data.loadTableId, args.data.acceptedQty, args.data.terminalMac, args.data.loadedDeviceProcessId, args.data.status, args.data.requestType,
                    args.data.isIgnored, args.data.isRolledBack, args.data.createdDateTime, defects, reworks,args.data.createdBy,args.data.modifiedBy);
                that.editProdConf(editedProdConf);
                that.prodConfObj.closeEdit();
            }
        }
    }

    //############## OTHER-EVENTS ################//


    disableDatePickerDate = (args) => {
        if (args.date.getDay() === 0) {
            args.isDisabled = true;
        }
    }

    onSave = () => {
        const { model, isEdit } = this.state;
        let modelData = cloneDeep(model);
        delete modelData.totalQty;
        delete modelData.validateLeftQty;
        delete modelData.supervisorName;

        if (isEdit) {
            if (this.isValidate()) {
                global.showLoader()
                this.editLineLoad(modelData);
            }
            else {
                global.hideLoader()
            }
        } else {
            if ((this.isValidateCreateLineLoad()) && (this.isValidate())) {
                global.showLoader()

                this.addLineLoad(modelData);
            } else {
                global.hideLoader()
            }
        }
    }

    isValidate = () => {
        const { model, isEdit } = this.state;
        let isValidateFields = true;
        if ((model.prodOrderId == null) || (model.startdate == null) || (model.workingHrs == null) && (model.totalQtyLoaded == null)) {
            isValidateFields = false;
            this.toastObj.show(this.state.toasterMessage[0]);
        }
        else if (model.noOfmachines == null) {
            isValidateFields = false;
            this.toastObj.show(this.state.toasterMessage[0]);

        } else if (model.totalQtyLoaded == 0) {
            isValidateFields = false;
            this.toastObj.show(this.state.toasterMessage[9]);
        }
        return isValidateFields;
    }

    isValidateCreateLineLoad = () => {
        const { model, isEdit, loadedLines } = this.state;
        let isValidateFields = true;
        for (let i = 0; i < loadedLines.length; i++) {
            let loadDate = new Date(loadedLines[i].startdate.getFullYear(), loadedLines[i].startdate.getMonth(), loadedLines[i].startdate.getDay())
            let modelDate = new Date(loadedLines[i].startdate.getFullYear(), loadedLines[i].startdate.getMonth(), loadedLines[i].startdate.getDay())
            if ((loadedLines[i].prodOrderId == model.prodOrderIdpro) && (loadedLines[i].lineName == model.lineName) && (loadDate.getTime() == modelDate.getTime())) {
                isValidateFields = false;
                this.toastObj.show(this.state.toasterMessage[3]);
                break;
            }
        }
        return isValidateFields;
    }


    //############### DIALOG ##############//

    openLineDialog() {
        this.setState({ lineLoadDialog: true });
        document.body.classList.add('overflow-hidden-body');
    }

    closeLineDialog = () => {
        //  this.setState({ lineLoadDialogOverLay: true });
        const { isEdit, selectedBuyerPoSaidId } = this.state;
        if (isEdit) {
            // global.showLoader();
            // this.getLoadedLines(selectedBuyerPoSaidId, selectedLineItem, selectedBuyerPo);
            this.setState({
                lineLoadDialog: false, rowSelectedScheduledQty: null, selectedHourlyViewDate: null, isShowDaywiseView: false, isShowHourlyView: false, isEdit: false, maxLoadQty: null, maxLoadsList: [], productionConfirmationdiv: false, maxAllowedMachines: null,
                model: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0),
                rollBackModel: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0)

            });
        }
        else {
            this.setState({
                lineLoadDialog: false, poDialog: false, rowSelectedScheduledQty: null, isShowDaywiseView: false, isEdit: false, maxLoadQty: null, maxLoadsList: [], maxAllowedMachines: null, isShowHourlyView: false, selectedHourlyViewDate: null,
                model: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0),
                rollBackModel: new LineLoadingModel(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 1, null, 0, false, 0, null, null, 0, 0, null, null, null, null, null, null, 0, 0, true, null, 0, null, null, 0, null, null, [], 0, null, null, null, false, 0)
            });
        }
        document.body.classList.remove('overflow-hidden-body');
    }

    showDayWiswView = () => {
        this.setState({ isShowDaywiseView: !this.state.isShowDaywiseView, productionConfirmationdiv: false, isShowHourlyView: false })
    }

    productionConfirmation = () => {
        this.setState({ productionConfirmationdiv: !this.state.productionConfirmationdiv, isShowDaywiseView: false, isShowHourlyView: false, selectedHourlyViewDate: null })
    }

    openLineLoadFrom = (event) => {
        this.setState(state => ({
            showLoadlineFrom: !state.showLoadlineFrom
        }));
    }

    cancelConformDialog = (args) => {
        this.setState({ isShowConfirmationDialog: false, loadedLineIdToDelete: null })
    }

    lineLoadView = (view) => {
        this.setState({ lineLoadView: view })
    }

    exportScheduledLinesGrid = (args) => {
        let that = this;
        if (that.scheduledLinesGridObj && args.item.id === 'scheduledLinesGrid_excelexport') {
            that.scheduledLinesGridObj.excelExport(that.state.sheduleExportProperties);
        }
    }

    exportLoadedLinesGrid = (args) => {
        let that = this;
        if (that.loadedLinesObj && args.item.id === 'loadedLinesGrid_excelexport') {
            that.loadedLinesObj.excelExport(that.state.loadexcelExportProperties);
        }
    }

    generateValidationReport = async (args) => {
       const {selectedReportPlantId, selectedReportUnitId, selectedReportStartDate, selectedReportEndDate} = this.state;

       if(selectedReportPlantId != null && selectedReportUnitId != null && selectedReportStartDate !=null && selectedReportEndDate !=null){
            this.genValidationReport(selectedReportPlantId, selectedReportUnitId, selectedReportStartDate, selectedReportEndDate)
            this.refreshValidationReportGrid()   
        }
    }

    //############################################################# LINELOAD-TAB ##########################################################//

    //########## API-CALLS ##########//

    async getPlantList() {
        let response = (await this.lineLoadingService.getPlantList())
        if ((response != undefined) && (response.data.status == "success"))
            this.setState({ plants: response.data.data })
    }

    async getUnitList(plantId) {
        let response = (await this.lineLoadingService.getUnitList(plantId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ units: response.data.data, selectedUnitName: null })
        }
    }

    async getUnitProductionLines(unitId) {
        let response = (await this.lineLoadingService.getUnitProductionLines(unitId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ unitProductionLines: response.data.data, selectedLineNames: [] })
            global.hideLoader();
        }
    }

    async getLoad(prodlineIds, prodDate) {
        let load = [];
        let response = (await this.lineLoadingService.getLoad(prodlineIds, prodDate))
        if ((response != undefined) && (response.length > 0)) {
            response.map(item => {
                if (item.data.status == "success")
                    load.push(item.data.data)
            })
            this.setState({ load }, () => {
                this.createLoad()
            })
        } else {
            global.hideLoader();
        }
    }

    async getLineDetails(args) {
        let convertedProdDate = new Date(Date.UTC(args.prodDate.year, args.prodDate.month - 1, args.prodDate.day))

        let response = (await this.lineLoadingService.getLineDetails(args.prodLineId, convertedProdDate, args.saidNr, args.lineItemNr, args.buyerPoNr))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ prodOrderLineDetails: response.data.data.productionProdOrderDtos, poDialog: true })
        }
    }

    async getLoadOnRefresh(prodlineIds, prodDate, index) {
        let response = (await this.lineLoadingService.getLoad(prodlineIds, prodDate))
        if ((response != undefined) && (response.length > 0)) {
            if (response[0].data.status == "success")
                this.createLoadOnRefresh(response[0].data.data, index)
        } else {
            global.hideLoader();
        }
    }

    async genValidationReport(plantId, unitId, startDate, endDate) {
        global.showLoader();
        let startMonth = startDate.getMonth()+1;
        let startDay = startDate.getDate();
        if(startMonth < 10){
            startMonth = '0' + startMonth;
        }
        if(startDay < 10){
            startDay = '0' + startDay;
        }
        let selectStartDate = startDate.getFullYear() + '-' + startMonth + '-' + startDay;

        let endMonth = endDate.getMonth()+1;
        let endDay = endDate.getDate();
        if(endMonth < 10){
            endMonth = '0' + endMonth;
        }
        if(endDay < 10){
            endDay = '0' + endDay;
        }
        let selectEndDate = endDate.getFullYear() + '-' + endMonth + '-' + endDay;
      
        let response1 = (await this.lineLoadingService.generateValidationReport(plantId, unitId, selectStartDate, selectEndDate))
        if ((response1 != undefined) && (response1.data.status == "success")) {
            this.downloadConfirmationReport(response1.data.message)
        //     let response = (await this.lineLoadingService.downloadReport(response1.data.value))
        //     if ((response != undefined) && (response.data.status == "success")) {
        //         this.toastObj.show({ title: 'Success!', content: response1.data.message, cssClass: 'e-toast-success', icon: 'e-error toast-icons' });
        //         this.setState({selectedReportStartDate: null, selectedReportPlantId: null, selectedReportUnitId: null, selectedReportEndDate: null})
        //     }
        //   global.hideLoader();
        }
        else {
          this.toastObj.show({ title: 'Error!', content: "Something Went Wrong", cssClass: 'e-toast-danger', icon: 'e-error toast-icons' });
          global.hideLoader();
        }
      }

      async downloadConfirmationReport(reportId) {
        global.showLoader();
        let response = (await this.lineLoadingService.downloadReport(reportId))
        if ((response != undefined) && (response.status == 200)) {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'ConfirmationReport.xlsx'); //or any other extension
            document.body.appendChild(link);
            link.click();
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[13]);

        }
        else {
            global.hideLoader();
            this.toastObj.show(this.state.toasterMessage[1]);
        }
    }

 refreshValidationReportGrid(){
    this.setState({showDialog:false,selectedReportPlantId:null,selectedReportUnitId:null,selectedReportStartDate:null,selectedReportEndDate:null}) 
 }




    //############ ON-CHANGE-HANDLER  #############//

    onChangePlant = (event) => {
        this.setState({
            selectedPlantName: event.value,
        }, () => {
            if (event.e != undefined) {
                this.multiSelectObj.selectAll(false);
                this.setState({ units: [], unitProductionLines: [], dynamicLoads: [], isTecho: true, selectProdDate: new Date() }, () => {
                    this.getUnitList(event.value)
                })
                this.setState({ isTecho: false });
            }
        })
    }

    onChangeUnits = (event) => {
        if (event.e != undefined) {
            this.multiSelectObj.selectAll(false);
            this.setState({ unitProductionLines: [], dynamicLoads: [], selectProdDate: new Date() })
            global.showLoader();
            this.getUnitProductionLines(event.value);
            this.setState({ selectedUnitName: event.value, })
        }
    }

    onChangeProductionLine = (event) => {
        if (event.value.length > 0 && this.state.selectProdDate != null) {
            global.showLoader();
            this.getLoad(event.value, this.state.selectProdDate);
            this.setState({ selectedLineName: event.value })
            // remove all load when e is undefined.
        }
    }

    onChangeFormDatePickerLineLoad = (event) => {
        if (event.value != null && this.state.selectedLineName.length > 0 && event.event != null) {
            let selectProdDate = new Date((event.value).getFullYear(), (event.value).getMonth(), (event.value).getDate(), 9)
            global.showLoader();
            this.getLoad(this.state.selectedLineName, selectProdDate);
            this.setState({ selectProdDate })
        }
    }

    onChangeReportDialoguePlant = (event) => {
        this.setState({
            selectedReportPlantId: event.value,
        }, () => {
            if (event.e != undefined) {
                this.multiSelectObj.selectAll(false);
                this.setState({ units: [] }, () => {
                    this.getUnitList(event.value)
                })
            }
        })
    }

    onChangeReportDialogueUnit = (event) => {
        let that = this;
        that.setState({ selectedReportUnitId: event.value });
    }
    onChangeReportDialogueStartDate = (event) => {
        let that = this;
        that.setState({ selectedReportStartDate: event.value });
    }
    onChangeReportDialogueEndDate = (event) => {
        let that = this;
        that.setState({ selectedReportEndDate: event.value });
    }

    //############### LOAD-TEMPLATE ################//


    rowSelectedLoadedQtyGrid = (args) => {
        let that = this;
        that.getLineDetails(args.data);
        that.setState({ rowSelectedLoadedQtyGrid: true });
    }


    createLoad = () => {
        const { load, isClockCountRunning, clockCount } = this.state;
        let dynamicLoads = [];
        for (let i = 0; i < load.length; i++) {
            dynamicLoads.push(
                <div key={i} className="box">
                    <Row>
                        <Col xs='12' md='6'>
                            <b>Line Name:</b>
                            <br /> {load[i].lineName}
                        </Col>
                        <Col xs='12' md='4'>
                            <b>Supervisor:</b>
                            <br /> {load[i].articleLoadOnLineList.length > 0 ? load[i].articleLoadOnLineList[0].supervisorName : null}
                        </Col>
                        <Col xs='12' md='2'>
                            <a className="refresh-btn" id={load[i].lineId} onClick={(e) => { this.onRefreshLineLoad(load[i].lineId, i) }}><i className="fa fa-refresh"></i></a>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs='`3`' md='3' className="text-center">
                            <div className="mt-3"></div>
                            <div className='line-Capicity-count'> {load[i].lineCapacity}%</div>
                            {/* <img src={lineCap} /><br /> */}
                            <span>Line Capacity</span>
                        </Col>
                        <Col xs='3' md='3' className="text-center">
                            <div className="mt-3"></div>
                            <div className='line-Efficiency-count'>{load[i].lineEfficiency}%</div>
                            {/* <img src={lineefc} /><br /> */}
                            <span>Line Efficiency</span>
                        </Col>
                        <Col xs='3' md='3' className="text-center">
                            <img src={machines} width='55' /><br />
                            Machines  <b className="primaryText">({load[i].noOfmachines}) </b>
                        </Col>
                        <Col xs='3' md='3' className="text-center">
                            <img src={time} width='55' /><br />
                            W Hrs  <b className="primaryText">({load[i].workingHrs}) </b>
                        </Col>
                    </Row>
                    <div className="mt-2"></div>
                    <Row>

                        <Col xs='4' md='4' className="text-center">
                            Skilled <b className="primaryText">({load[i].skilledWorker}) </b> <br />
                        </Col>
                        <Col xs='4' md='4' className="text-center">   Semi Skilled <b className="primaryText">({load[i].unskilledWorker}) </b> <br />
                        </Col>
                        <Col xs='4' md='4' className="text-center">
                            Unskilled <b className="primaryText">({load[i].labour}) </b> <br />
                        </Col>
                    </Row>
                    <div className='mt-3'></div>
                    <GridComponent allowTextWrap={true}
                        dataSource={load[i].articleLoadOnLineList}
                        gridLines='Both' allowTextWrap={true} allowReordering={true}
                        ref={(grid) => { this[load[i].lineName] = grid; }}
                        pageSettings={{ pageCount: 3, pageSizes: true }} allowResizing={true} rowSelected={this.rowSelectedLoadedQtyGrid}>
                        <ColumnsDirective>
                            <ColumnDirective field='saidNr' headerText='Said No' width='150'></ColumnDirective>
                            <ColumnDirective field='materialDesc' headerText='Material  Desc' width='150'></ColumnDirective>
                            <ColumnDirective field='buyerPoNr' headerText=' Buyer  Po ' width='100'></ColumnDirective>
                            <ColumnDirective field='lineItemNr' headerText='Line item' width='100'></ColumnDirective>
                            <ColumnDirective field='totalQtyLoaded' headerText='Loaded  Qty.' width='90' ></ColumnDirective>
                            <ColumnDirective field='confQty' headerText='Confirm Qty.' width='90'></ColumnDirective>
                            <ColumnDirective field='totalDefects' headerText='Defects' width='70'></ColumnDirective>
                            <ColumnDirective field='totalRework' headerText='Rework' width='80'></ColumnDirective>
                        </ColumnsDirective>
                        <Inject services={[Resize, Page]} />
                    </GridComponent>
                </div>

            )
        }
        if (isClockCountRunning == false) {
            this.setState({ dynamicLoads, isClockCountRunning: true, clockCount: 600 }, () => {
                global.hideLoader();
            })
        } else {
            this.setState({ dynamicLoads }, () => {
                global.hideLoader();
            })
        }
    }

    createLoadOnRefresh = (load, index) => {
        let template = (<div key={index} className="box">
            <Row>
                <Col xs='12' md='6'>
                    <b>Line Name:</b>
                    <br /> {load.lineName}
                </Col>
                {/* <Col xs='12' md='3'>
                        <b>FI:</b> ---
                   </Col> */}
                <Col xs='12' md='4'>
                    <b>Supervisor:</b>
                    <br /> {load.articleLoadOnLineList.length > 0 ? load.articleLoadOnLineList[0].supervisorName : null}
                </Col>
                <Col xs='12' md='2'>
                    <a className="refresh-btn" id={load.lineId} onClick={(e) => this.onRefreshLineLoad(load.lineId, index)}><i className="fa fa-refresh"></i></a>
                </Col>
            </Row>
            <Row>
                <Col xs='3' md='3' className="text-center">
                    <div className="mt-3"></div>
                    <div className='line-Capicity-count'> {load.lineCapacity}%</div>
                    {/* <img src={lineCap} /><br /> */}
                    <b title='Line Capicity'>Line Capicity</b>
                </Col>
                <Col xs='3' md='3' className="text-center">
                    <div className="mt-3"></div>
                    <div className='line-Efficiency-count'>{load.lineEfficiency}%</div>
                    {/* <img src={lineefc} /><br /> */}
                    <b title='Line Efficiency'>Line Efficiency</b>
                </Col>
                <Col xs='3' md='3' className="text-center">
                    <img src={machines} width='50' /><br />
                    Machines  <b className="primaryText">({load.noOfmachines}) </b>
                </Col>
                <Col xs='3' md='3' className="text-center">
                    <img src={time} width='35' /><br />
                    W Hrs  <b className="primaryText">(8:00Hrs)  </b>
                </Col>
            </Row>
            <div className="mt-2"></div>
            <Row>
                <Col xs='3' md='3' className="text-center">
                    Labour <b className="primaryText">({load.labour}) </b> <br />
                </Col>
                <Col xs='3' md='3' className="text-center">
                    Skilled <b className="primaryText">({load.skilledWorker}) </b> <br />
                </Col>
                <Col xs='3' md='3' className="text-center">  UnSkilled <b className="primaryText">({load.unskilledWorker}) </b> <br />
                </Col>
            </Row>
            <div className='mt-3'></div>
            <GridComponent
                dataSource={load.articleLoadOnLineList}
                gridLines='Both' allowTextWrap={true} allowReordering={true}
                ref={(grid) => { this[load.lineName] = grid; }}
                pageSettings={{ pageCount: 3, pageSizes: true }} allowResizing={true} rowSelected={this.rowSelectedLoadedQtyGrid}>
                <ColumnsDirective>
                    <ColumnDirective field='materialKd' headerText='Material  KD' width='150'></ColumnDirective>
                    <ColumnDirective field='materialDesc' headerText='Material  Desc' width='150'></ColumnDirective>
                    <ColumnDirective field='buyerPoNr' headerText=' Buyer  Po ' width='100'></ColumnDirective>
                    <ColumnDirective field='lineItemNr' headerText='Line item' width='100'></ColumnDirective>
                    <ColumnDirective field='totalQtyLoaded' headerText='Loaded  Qty.' width='90' ></ColumnDirective>
                    <ColumnDirective field='confQty' headerText='Confirm Qty.' width='90'></ColumnDirective>
                    <ColumnDirective field='totalDefects' headerText='Defects' width='70'></ColumnDirective>
                    <ColumnDirective field='totalRework' headerText='Rework' width='80'></ColumnDirective>
                </ColumnsDirective>


                <Inject services={[Resize, Page]} />
            </GridComponent>
        </div>)
        const newArray = Array.from(this.state.dynamicLoads);
        newArray[index] = template;
        this.setState({ dynamicLoads: newArray }, () => {
            global.hideLoader();
        })
    }

    onRefreshLineLoad = (lineId, index) => {
        let lineIdArray = [];
        lineIdArray.push(lineId);
        global.showLoader()
        this.getLoadOnRefresh(lineIdArray, this.state.selectProdDate, index)
    }


    onClickGenerateReport = (plantId, unitId) => {
        this.generateReport(plantId, unitId)
    }

    //############# Distributed Day Report ###########// 

    onClickDistributedReport(){
        this.setState({showDialog: true, selectedPlantNameForDistributed:null})
    }
    dialogClose() {
        this.setState({
            showDialog: false,
        });
    }
    //############## COUNT-DOWN-TIMER ###############//

    handleCountdown() {
        this.setState({
            clockCount: 600,
            isClockCountRunning: true
        })
    }

    handleStartClockCountDown = () => {
        this.timer = setInterval(() => {
            const newCount = this.state.clockCount - 1;
            if (newCount == -1) {
                this.handleStop()
            } else {
                this.setState(
                    { clockCount: newCount >= 0 ? newCount : 0 }
                );
            }
        }, 1000);
    }

    handleStop = () => {
        const { selectedLineName, selectProdDate, lineLoadView } = this.state;
        if (this.timer) {
            clearInterval(this.timer);
            this.setState(
                { isClockCountRunning: false }, () => {
                    if ((selectedLineName.length > 0) && (lineLoadView)) {
                        global.showLoader()
                        this.getLoad(selectedLineName, selectProdDate)
                    } else {
                        this.setState({ clockCount: 600, isClockCountRunning: true })
                    }
                }
            )
        }
    }

    //############################################################### OTHER ###############################################################//

    createToaster(message, type) {
        switch (type) {
            case "Success":
                return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
            case "Error":
                return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
            case "Warning":
                return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
            case "Information":
                return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
    }


    //################################################################################ Validate Report###################
    


    onChangeDistributed = (event) => {
       


        if (event.value.length > 0 && this.state.selectProdDate != null) {
            
        }
    }


    async getUnitListForScheduledTab(plantId) {
        let response = (await this.lineLoadingService.getUnitList(plantId))
        if ((response != undefined) && (response.data.status == "success")) {
            this.setState({ scheduledTabUnits: response.data.data, selectedScheduledTabUnitId: null })
        }
    }

    newTransferLoads = ()=>{
        let newtran = this.state.formUnitProductionLines.filter((item,i)=>(
            item.id !== this.state.model.lineId
        ))

        this.setState({newTransferLine: newtran}) 
    }

    editLineLoadFun = ()=> {
        if(this.state.isEdit)
        {
            // if(this.state.isDatePast){
                
            //     return false
            // }else{
            //     if(this.state.transferObj.isTransferTo == false){
            //         return true;
            //     }else{
            //         return false;
            //     }
            // }
            if(this.state.transferObj.isTransferTo == false){
                    return true;
                }else{
                    return false;
                }
            
        }else{
            return false
        }
    }



    //################################################################ RENDER ##############################################################//


    render() {
        const { t, i18n } = this.props;
        return (
            <div>
                <div className="container-fluid-inner">
                    <h1 className='pagesheading'>Line Loading</h1>
                    <div className='clearfix'></div>
                    <Row>
                        <Col xs='6' md='6'>
                            <div className="custom-button-tabs">
                                <Button color="primary" size="sm" color={this.state.lineLoadView == true ? "primary" : "secondary"} onClick={e => this.lineLoadView(true)} id="dialogBtn">Line Load</Button>
                                <Button color="primary" size="sm" className="mr-2 ml-2" color={this.state.lineLoadView == false ? "primary" : "secondary"} onClick={e => this.lineLoadView(false)} id="dialogBtn">Schedule</Button>
                            </div>
                        </Col>
                        <Col xs='6' md='6'>
                            <div hidden={!this.state.lineLoadView}>

                            </div>
                            <span className='float-right'>
                                <Row>
                                    <Col xs='6' md='6' className='text-right'>
                                        <p id="demo"></p>
                                    </Col>
                                    <Col xs='6' md='6'>
                                        <CountDownClockComponent time={this.state.clockCount} />

                                    </Col>
                                </Row>
                            </span>
                            {this.state.lineLoadView ?
                            <div>
                                <a color="primary" className="ml-4 float-right primaryText" disabled={((this.state.selectedPlantName == null) || (this.state.selectedUnitName == null)) ? true : false} onClick={(e) => { this.onClickGenerateReport(this.state.selectedPlantName, this.state.selectedUnitName) }} id="generateBtn"><i className='fa fa-download'></i> Attendance Report</a>
                                <a color="primary" className="ml-2 float-right primaryText" onClick={this.onClickDistributedReport} id="Report"><i className='fa fa-download'></i> Confirmation Report</a>
                            </div> : null}
                            
                        </Col>

{/* //---------------------------------------------------------------------------------Validation Report Dialog Start---------------------------------------------------------------// */}
                    <DialogComponent visible={this.state.showDialog} id="defaultDialog" showCloseIcon={true} animationSettings={this.animationSettings} isModal={true} allowDragging={true} visible={this.state.showDialog} width={'70%'} ref={dialog => this.dialogInstance = dialog}
                    header= 'Daywise Confirmation And Defects Report' close={this.dialogClose.bind(this)}>
                        
                        <Row>
                            <Col><DropDownListComponent className='datePicker' id='selectPlantName' placeholder="Select Plant Name" value={this.state.selectedReportPlantId} floatLabelType="Auto" fields={{ text: 'name', value: 'id' }} change={this.onChangeReportDialoguePlant} dataSource={this.state.plants} popupHeight="220px"></DropDownListComponent></Col>
                            <Col><DropDownListComponent className='datePicker' id='selectUnitName' placeholder="Select Unit Name" value={this.state.selectedReportUnitId} floatLabelType="Auto" dataSource={this.state.units} fields={{ text: 'name', value: 'id' }}  change={this.onChangeReportDialogueUnit}  popupHeight="220px"></DropDownListComponent></Col>
                            <Col><DatePickerComponent className='datePicker' value={this.state.selectedReportStartDate}  placeholder=" Start Date" floatLabelType="Auto"  change={this.onChangeReportDialogueStartDate} ></DatePickerComponent></Col>
                            <Col><DatePickerComponent className='datePicker' value={this.state.selectedReportEndDate}  placeholder=" End Date" floatLabelType="Auto"  change={this.onChangeReportDialogueEndDate} ></DatePickerComponent></Col>
                        </Row>
                        <div className='mt-2'></div>
                        <Row className='float-right'><Button color="primary" size="sm" id="generateValidationReport" onClick={this.generateValidationReport}><i className='fa fa-download'></i> Generate Report</Button></Row>

                    </DialogComponent>
{/* //---------------------------------------------------------------------------------Validation Report Dialog End---------------------------------------------------------------// */}
                    </Row>


                    {/* //---------------------------------------------------------------------------------SCHEDULE-VIEW---------------------------------------------------------------// */}


                    <div className="scheduleView" hidden={this.state.lineLoadView}>
                        <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={{ X: 'Left', Y: 'Bottom' }}   ></ToastComponent>

                        <Row>
                            <Col xs='12' lg='3' md='4'>
                                <div className="required-field">
                                    <DropDownListComponent placeholder={t("cpm.lm.line.load.filter.plant")} change={this.onChangeMultiSelectPlantToGetBuyerPo}
                                        color="primary" id="plantId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedPlantForLineItem} dataSource={this.state.plants} floatLabelType="Auto" />
                                </div>
                            </Col>
                            <Col xs='12' md='4' lg='3'>
                                <div className="required-field">
                                    <DropDownListComponent color="primary" id="unitId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedScheduledTabUnitId} change={this.onChangeScheduledTabUnit} dataSource={this.state.scheduledTabUnits} placeholder={t("cpm.lm.line.unit.name")} floatLabelType="Auto" />
                                </div>
                            </Col>

                        </Row>
                        <Row>
                            <Col xs='12' lg='3' md='3'>
                                <MultiSelectComponent id="selectedSaidNo" ref={(scope) => { this.scheduledTabSaidGridObj = scope; }} dataSource={this.state.saidNoList}
                                    value={this.state.selectedSaidNo} change={this.onChangeFilterMultiSelect} fields={{ text: 'saidNo', value: 'saidNo' }}
                                    placeholder={t("cpm.lm.line.load.filter.said")} mode="CheckBox" showSelectAll={true}
                                    showDropDownIcon={true} filterBarPlaceholder="Search Saids" popupHeight="300px">
                                    <Inject services={[CheckBoxSelection]} />
                                </MultiSelectComponent>
                            </Col>

                            <Col xs='12' lg='3' md='4'>

                                <MultiSelectComponent id="selectedBuyerPo" ref={(scope) => { this.scheduledTabBuyerPoGridObj = scope; }} dataSource={this.state.buyerPoList}
                                    value={this.state.selectedBuyerPo} change={this.onChangeFilterMultiSelect} fields={{ text: 'buyerPoNo', value: 'buyerPoNo' }}
                                    placeholder={t("cpm.lm.line.load.filter.buyerpo")} mode="CheckBox" showSelectAll={true}
                                    showDropDownIcon={true} filterBarPlaceholder="Search Buyer Po" popupHeight="300px">
                                    <Inject services={[CheckBoxSelection]} />
                                </MultiSelectComponent>
                            </Col>
                            <Col xs='12' md='4' lg='3' >

                                <MultiSelectComponent id="selectedLineItem" ref={(scope) => { this.scheduledTabLineItemNoGridObj = scope; }} dataSource={this.state.lineItemList}
                                    value={this.state.selectedLineItem} change={this.onChangeFilterMultiSelect} fields={{ text: 'lineItemNo', value: 'lineItemNo' }}
                                    placeholder={t("cpm.lm.line.load.filter.lineitem")} mode="CheckBox" showSelectAll={true}
                                    showDropDownIcon={true} filterBarPlaceholder="Search LineItem No" popupHeight="300px">
                                    <Inject services={[CheckBoxSelection]} />
                                </MultiSelectComponent>
                            </Col>
                            <div><Button color="primary" size="sm" className="mr-2" onClick={this.search} id="search"> Search</Button></div>
                            <div><Button color="primary" size="sm" className="mr-2" onClick={this.refreshLines} id="search"> Refresh</Button></div>

                            {/* <Col xs='12' md='4' lg='3' ><div className='mt-3'></div>
                                <ProgressButtonComponent id="spinRight" color='primary' disabled={((this.state.scheduledGridSelectedData.saidId == null) || (this.state.scheduledGridSelectedData.lineItemNo == null) || (this.state.scheduledGridSelectedData.buyerPoNo == null)) ? true : false}
                                    content="Update Store" ref={(scope) => { this.progressBtn = scope; }} isPrimary spinSettings={this.state.spinRight} onClick={() => { this.updateStore(this) }} ></ProgressButtonComponent>
                            </Col> */}
                        </Row>
                        <div className='mt-3'></div>
                        <GridComponent gridLines='Both' id="scheduledLinesGrid" allowExcelExport={true} toolbarClick={this.exportScheduledLinesGrid} ref={grid => this.scheduledLinesGridObj = grid} toolbar={['ExcelExport', 'Search']} allowTextWrap={true} allowSorting={true} gridLines='Both' allowFiltering={true} filterSettings={this.state.FilterSettingsModel}
                            dataSource={this.state.scheduledLines} allowPaging={true} commandClick={this.updateStore} rowSelected={this.rowSelectedScheduleGrid} allowResizing={true} pageSettings={this.pageSettings}>
                            <ColumnsDirective>
                                <ColumnDirective filter={this.state.IFilter} field='lineName' headerText='Line Name' width='150' ></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} field='buyerPoNo' headerText='Buyer po' width='120'></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} field='saidId' headerText='Said Id' width='100' />
                                <ColumnDirective filter={this.state.IFilter} field='lineItemNo' headerText='Line Item No' width='120' />
                                <ColumnDirective allowFiltering={false} field='lineItemDesc' headerText='Material Description' width='200' />
                                <ColumnDirective allowFiltering={false} field='scheduleStartDateTime' format={this.state.format} headerText='Start Date' width='80'></ColumnDirective>
                                <ColumnDirective allowFiltering={false} field='scheduleEndDateTime' format={this.state.format} headerText='End Date' width='100' ></ColumnDirective>
                                <ColumnDirective allowFiltering={false} field='totalQtyToProduced' headerText='Total Qty.' width='80' format='###,##'></ColumnDirective>
                                <ColumnDirective allowFiltering={false} field='scheduleQtyForProduction' headerText='Scheduled Qty.' width='100' format='###,##'></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} field='apm' headerText='APM' width='100'></ColumnDirective>
                                <ColumnDirective headerText='Action' textAlign='Left' commands={this.updateStoreCommands} width='90' />
                                {/* <ColumnDirective field='fm' headerText='FM' width='100' ></ColumnDirective> */}
                                {/* <ColumnDirective field='category' headerText='Category' width='80' />
                                <ColumnDirective field='status' headerText='Status' width='100'></ColumnDirective> */}
                            </ColumnsDirective>
                            <Inject services={[Page, Resize, Sort, Filter, Resize, Reorder, CommandColumn, Toolbar, ExcelExport]} />
                        </GridComponent>
                        <div className='mt-3'></div>


                        <div className='graphView'>
                            <label className='pr-3'> Hide Teco Production Orders</label>
                            <SwitchComponent id="checked" change={this.handleClick} checked={this.state.isTecho}></SwitchComponent>
                        </div>
                        <GridComponent gridLines='Both' id="loadedLinesGrid" allowExcelExport={true} toolbarClick={this.exportLoadedLinesGrid} toolbar={['ExcelExport', 'Search', 'ColumnChooser']} showColumnChooser={true} allowTextWrap={true} dataSource={this.state.loadedLines} ref={grid => this.loadedLinesObj = grid} commandClick={this.commandClick}
                            filterSettings={this.state.FilterSettingsModel} allowSorting={true} allowFiltering={true} actionBegin={this.actionBegin} rowSelected={this.rowSelectedLoadedLinesGrid}
                            editSettings={this.state.editSettingsProductionOrder} allowPaging={true} allowResizing={true} pageSettings={this.pageSettings} allowGrouping={true} >
                            <ColumnsDirective>
                                <ColumnDirective filter={this.state.IFilter} field='prodOrderId' headerText='Prod Order' width='110' ></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} field='lineName' headerText='Line Name' width='100' ></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} field='saidNr' headerText='Said Id' width='100' />
                                <ColumnDirective filter={this.state.IFilter} field='lineItemNr' headerText='Line Item No' width='120' />
                                <ColumnDirective filter={this.state.IFilter} field='materialKd' headerText='Material Id' width='200' />
                                <ColumnDirective filter={this.state.IFilter} field='buyerPoNr' headerText='Buyer Po No' width='120' />

                                <ColumnDirective filter={this.state.IFilter} field='processName' headerText='Process Name' width='100'></ColumnDirective>
                                <ColumnDirective allowFiltering={false} field='totalQtyLoaded' headerText='Loaded Qty' width='80' format='###,##'></ColumnDirective>
                                {/* <ColumnDirective allowFiltering={false} field='leftQty' headerText='WIP' width='70' format='###,##'></ColumnDirective> */}
                                <ColumnDirective allowFiltering={false} field='confQty' headerText='Prod Conf' width='60' />
                                <ColumnDirective allowFiltering={false} field='sapConfQty' headerText='Sap Conf' width='60' />
                                <ColumnDirective allowFiltering={false} field='defectQty' headerText='Reject Qty' width='80' />
                                <ColumnDirective allowFiltering={false} field='sapRejectQty' headerText='Sap Reject Qty' width='90' />

                                <ColumnDirective allowFiltering={false} field='startdate' format={this.state.format} headerText='Start Date' width='100'  />
                                <ColumnDirective allowFiltering={false} field='endDate' format={this.state.format} headerText='End Date' width='100' />
                                <ColumnDirective allowFiltering={false} field='noOfmachines' headerText='OP' width='50' format='###,##' />
                                <ColumnDirective allowFiltering={false} field='workingHrs' headerText='W hrs' width='50'></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} field='dataPoint' headerText='D point' width='90'></ColumnDirective>
                                <ColumnDirective allowFiltering={false} field='bulkUpload' headerText='B Qty' width='80' format='###,##'></ColumnDirective>
                                {/* <ColumnDirective allowFiltering={false} field='yellowCeiling' headerText='Y sealing' width='100'></ColumnDirective> */}
                                <ColumnDirective filter={this.state.IFilter} field='supervisorName' headerText='Supr' width='90'></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} field='status' headerText='Status' width='95'></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} visible={false} field='buyerPoNr' headerText='Status' width='95'></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} visible={false} field='lineItemNr' headerText='Status' width='95'></ColumnDirective>
                                <ColumnDirective filter={this.state.IFilter} visible={false} field='saidNr' headerText='Status' width='95'></ColumnDirective>
                                <ColumnDirective allowSorting={false} allowFiltering={false} headerText={"Action"} width='140' commands={this.state.commands} ></ColumnDirective>
                                {/* <ColumnDirective headerText='Update' commands={this.updateSapCommands} width='100' textAlign='Left' /> */}

                            </ColumnsDirective>
                            <Inject services={[Page, Resize, Group, Edit, CommandColumn, Sort, Filter, Reorder, Toolbar, ExcelExport, ColumnChooser]} />
                        </GridComponent>
                        <DialogComponent id="confirmDialog" animationSettings={{ effect: 'FlipYLeft' }} showCloseIcon={true} close={this.cancelConformDialog} buttons={this.state.confirmButton} header='Delete Line Confirmation !!' visible={this.state.isShowConfirmationDialog} width='400px' content='Are you sure you want to permanently delete Line ?'></DialogComponent>

                    </div>



                    {/* //---------------------------------------------------------------------------------LOAD-LINE-VIEW---------------------------------------------------------------// */}

                    <div className="lineLoadView" hidden={!this.state.lineLoadView}>
                        <div className='mt-2'></div>
                        <div>
                            <div className="container-fluid-inner1">
                                <div className="clearfix"></div>

                                <Row>
                                    <Col xs='12' lg='3' md='3'>
                                        <div className="required-field">
                                            <DropDownListComponent color="primary" id="plantId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedPlantName} change={this.onChangePlant}  dataSource={this.state.plants} placeholder={t("cpm.lm.line.load.filter.plant")} floatLabelType="Auto" />
                                        </div>
                                    </Col>


                                    <Col xs='12' lg='3' md='3'>
                                        <div className="required-field">
                                            <DropDownListComponent color="primary" id="unitId" allowFiltering={true} fields={{ text: 'name', value: 'id' }} value={this.state.selectedUnitName} change={this.onChangeUnits} dataSource={this.state.units} placeholder={t("cpm.lm.line.load.filter.unit")} floatLabelType="Auto" />

                                        </div>
                                    </Col>

                                    <Col xs='12' lg='3' md='3'>
                                        <MultiSelectComponent id="lineId" dataSource={this.state.unitProductionLines}
                                            change={this.onChangeProductionLine} ref={t => this.multiSelectObj = t} fields={{ text: 'lineName', value: 'id' }} placeholder={t("cpm.lm.line.load.filter.prodline")} mode="CheckBox" showSelectAll={true}
                                            showDropDownIcon={true}
                                            filterBarPlaceholder="Search plants" popupHeight="350px" floatLabelType="Auto" >
                                            <Inject services={[CheckBoxSelection]} />
                                        </MultiSelectComponent>
                                    </Col>

                                    <Col xs='12' lg='3' md='3'>
                                        <div className="required-field">
                                            <DatePickerComponent weekNumber={true} floatLabelType="Auto" allowEdit={false} renderDayCell={this.disableDatePickerDate} value={this.state.selectProdDate} change={this.onChangeFormDatePickerLineLoad} format='dd/MM/yy hh:mm a' id="selectdate" placeholder={t("cpm.lm.line.load.filter.date")}></DatePickerComponent >
                                        </div>
                                    </Col>
                                </Row>


                                {/* ----------------------------------------- Line laod list Start -----------------------------------------  */}


                                <div className='mt-3'></div>
                                <div className="line-list">
                                    {this.state.dynamicLoads}
                                </div>

                                {/* ----------------------------------------- Line laod list End -----------------------------------------  */}
                            </div>



                            {/* ---------------------------------- linelaod popup ------------------------ */}
                            <DialogComponent id="defaultdialog" height="1000" className="model-lg" showCloseIcon={true} animationSettings={this.state.animationSettings} visible={this.state.lineLoadDialog} ref={dialog => this.dialogInstance = dialog}
                                header={this.state.isEdit ? t("cpm.lm.line.load.popup.editlineload") : t("cpm.lm.line.load.popup.addlineload")} close={this.closeLineDialog.bind(this)}>
                                <div>

                                    <div className="addLineLoad1">
                                        <Row>
                                            <Col xs='12' lg='2' md='2'>
                                                <div className="required-field">
                                                    <DropDownListComponent placeholder={t("cpm.lm.line.load.popup.prodorder")} enabled={this.state.isEdit ? false : true} color="primary" id="prodOrderId" allowFiltering={true} fields={{ text: 'prodOrdNr', value: 'prodOrdNr' }} value={this.state.model.prodOrderId} change={(e) => { this.onChangeFormProductionOrderDropdown(e, this) }} dataSource={this.state.productionOrderList} floatLabelType="Auto" />
                                                </div>
                                            </Col>

                                            {/* <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.wip")} disabled={true} id="leftQty" value={this.state.model.leftQty} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col> */}
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.prodconf")} disabled={true} id="confQty" value={this.state.model.confQty} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.process")} disabled={true} id="processName" value={this.state.model.processName} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.materialId")} disabled={true} id="materialKd" value={this.state.model.materialKd} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>

                                            <Col xs='12' lg='2' md='2'>
                                                <div className="required-field">
                                                    <DatePickerComponent weekNumber={true} floatLabelType="Auto" min={this.state.isEdit ? this.state.currentDate : this.state.rowSelectedScheduleStartDate}
                                                        enabled={this.state.isEdit ? this.state.model.confQty == 0 && this.state.transferObj.isTransferTo == false && this.state.isStartDateDisable ? true : false : true}
                                                        allowEdit={false} renderDayCell={this.disableDatePickerDate} value={this.state.model.startdate} change={this.onChangeFormDatePicker} format='dd/MM/yy hh:mm a' id="startdate" placeholder={t("cpm.lm.line.load.popup.startdate")} ></DatePickerComponent >
                                                </div>
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <DatePickerComponent floatLabelType="Auto" allowEdit={false} enabled={false} renderDayCell={this.disableDatePickerDate} value={this.state.model.endDate} change={this.onChangeFormDatePicker} format='dd/MM/yy hh:mm a' id="endDate" placeholder={t("cpm.lm.line.load.popup.enddate")}></DatePickerComponent >
                                            </Col>
                                        </Row>
                                        <Row>

                                            <Col xs='6' md='2'>
                                                <div className="d-flex">
                                                    <div className="required-field">
                                                        <NumericTextBoxComponent
                                                            max={this.state.configKeys.totalMachines == undefined ? 250 : this.state.configKeys.totalMachines}
                                                            min={0}
                                                            placeholder={t("cpm.lm.line.load.popup.operators")}
                                                            id={"noOfmachines"}
                                                            format='###.###'
                                                            decimals={0}
                                                            enabled={this.state.isEdit ? this.state.model.confQty == 0 && this.state.transferObj.isTransferTo == false ? true : false : true}
                                                            value={this.state.model.noOfmachines}
                                                            onChange={this.onChangeFormNumericText}
                                                            floatLabelType="Auto"
                                                        >
                                                        </NumericTextBoxComponent>
                                                    </div>
                                                    <div className="sew-machine mt-3 ml-3"></div>
                                                </div>
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <div className="d-flex">
                                                    <NumericTextBoxComponent
                                                        placeholder={t("cpm.lm.line.load.popup.totalQty")}
                                                        id={"totalQty"}
                                                        decimals={0}
                                                        value={this.state.model.totalQty}
                                                        format='###,##'
                                                        enabled={false}
                                                        floatLabelType="Auto"
                                                    >
                                                    </NumericTextBoxComponent>
                                                </div>
                                            </Col>

                                            <Col xs='12' lg='2' md='2'>
                                                <div className="d-flex">
                                                    <div >
                                                        <NumericTextBoxComponent
                                                            placeholder={t("cpm.lm.line.load.popup.loadqty")}
                                                            id={"totalQtyLoaded"}
                                                            value={this.state.model.totalQtyLoaded}
                                                            onChange={this.onChangeFormNumericText}
                                                            format='###,##'
                                                            decimals={0}
                                                            enabled={
                                                                this.editLineLoadFun()
                                                            }
                                                            // max={this.state.maxLoadQty < this.state.model.totalQty ? this.state.maxLoadQty : this.state.model.totalQty}
                                                            max={this.state.isEdit ? this.state.model.totalQty : Math.min(this.state.maxLoadQty, this.state.model.totalQty, this.state.rowSelectedScheduledQty)}
                                                            min={0}
                                                            floatLabelType="Auto"
                                                        >
                                                        </NumericTextBoxComponent>
                                                        {/* {this.state.maxLoadQty} {this.state.model.totalQty} {this.state.rowSelectedScheduledQty} */}
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <div className="d-flex">
                                                    <div >
                                                        <NumericTextBoxComponent
                                                            placeholder={t("cpm.lm.line.load.popup.MaxLoadQTY")}
                                                            id={"maxLoadQty"}
                                                            value={this.state.maxLoadQty}
                                                            decimals={0}
                                                            format='###.###'
                                                            enabled={false}
                                                            floatLabelType="Auto"
                                                        >
                                                        </NumericTextBoxComponent>
                                                    </div>
                                                </div>
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.wipTarget")} id={"wipTargetTime"} value={this.state.model.wipTargetTime}
                                                    enabled={this.state.isEdit ? false : true} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <div className="required-field">
                                                    <NumericTextBoxComponent
                                                        min={0}
                                                        max={this.state.configKeys.workingHour == undefined ? 24 : this.state.configKeys.workingHour}
                                                        placeholder={t("cpm.lm.line.load.popup.workinghrs")}
                                                        id={"workingHrs"}
                                                        format='###.###'
                                                        enabled={this.state.isEdit ? this.state.model.confQty == 0 && this.state.transferObj.isTransferTo == false ? true : false : true}
                                                        value={this.state.model.workingHrs}
                                                        onChange={this.onChangeFormNumericText}
                                                        floatLabelType="Auto"
                                                    >
                                                    </NumericTextBoxComponent>
                                                </div>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col xs='12' lg='2' md='2'>
                                                <NumericTextBoxComponent
                                                    min={0}
                                                    max={this.state.configKeys.skilledWorker == undefined ? 200 : this.state.configKeys.skilledWorker}
                                                    placeholder={t("cpm.lm.line.load.popup.skilledworker")}
                                                    id={"skilledWorker"}
                                                    format='###.###'
                                                    value={this.state.model.skilledWorker}
                                                    enabled={this.state.isEdit ? this.state.transferObj.isTransferTo == false ? true : false : true}
                                                    onChange={this.onChangeFormNumericText}
                                                    floatLabelType="Auto"
                                                >
                                                </NumericTextBoxComponent>
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <NumericTextBoxComponent
                                                    min={0}
                                                    max={this.state.configKeys.unskilledWorker == undefined ? 200 : this.state.configKeys.unskilledWorker}
                                                    placeholder={t("cpm.lm.line.load.popup.unskilledworker")}
                                                    id={"unskilledWorker"}
                                                    format='###.###'
                                                    decimals={0}
                                                    value={this.state.model.unskilledWorker}
                                                    enabled={this.state.isEdit ? this.state.transferObj.isTransferTo == false ? true : false : true}
                                                    onChange={this.onChangeFormNumericText}
                                                    floatLabelType="Auto"
                                                >
                                                </NumericTextBoxComponent>
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <NumericTextBoxComponent
                                                    min={0}
                                                    max={this.state.configKeys.labour == undefined ? 200 : this.state.configKeys.labour}
                                                    placeholder={t("cpm.lm.line.load.popup.labour")}
                                                    id={"labour"}
                                                    decimals={0}
                                                    format='###.###'
                                                    enabled={this.state.isEdit ? this.state.transferObj.isTransferTo == false ? true : false : true}
                                                    value={this.state.model.labour}
                                                    onChange={this.onChangeFormNumericText}
                                                    floatLabelType="Auto"
                                                >
                                                </NumericTextBoxComponent>
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <div className='mt-3'></div>
                                                <span className="mr-3"> {t('cpm.lm.line.load.popup.datapoint')} </span>

                                                <SwitchComponent checked={this.state.model.dataPoint} id={"dataPoint"}
                                                    disabled={this.state.isEdit}
                                                    change={this.onChangeFormSwitchDataPoint} />

                                                {/* <TextBoxComponent placeholder={'Data Point'} id="prodConf" value='' onChange={this.onChangeTextField} floatLabelType="Auto" /> */}
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <NumericTextBoxComponent
                                                    max={this.state.configKeys.bulkUpload == undefined ? 50 : this.state.configKeys.bulkUpload}
                                                    min={1}
                                                    placeholder={t("cpm.lm.line.load.popup.bulkuploadqty")}
                                                    id={"bulkUpload"}
                                                    format='###.###'
                                                    decimals={0}
                                                    enabled={!this.state.isEdit}
                                                    value={this.state.model.bulkUpload}
                                                    onChange={this.onChangeFormNumericText}
                                                    floatLabelType="Auto"
                                                >
                                                </NumericTextBoxComponent>
                                                {/* <TextBoxComponent placeholder={'Buk upload(Qty)'} id="prodConf" value='' onChange={this.onChangeTextField} floatLabelType="Auto" /> */}
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent disabled={this.state.isEdit && this.state.transferObj.isTransferTo == false ? false : true} placeholder={t("cpm.lm.line.load.popup.yellowceiling")} id="yellowCeiling" value={this.state.model.yellowCeiling} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>

                                        </Row>
                                        <Row>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.fm")} disabled={true} id="fmId" value={this.state.model.fmName} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.apm")} disabled={true} id="apm" value={this.state.model.apm} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.qa")} disabled={true} id="qaId" value={this.state.model.qaName} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                {/* <TextBoxComponent placeholder={'Supervisor'} id="prodConf" value='' onChange={this.onChangeTextField} floatLabelType="Auto" /> */}
                                                <DropDownListComponent placeholder={t("cpm.lm.line.load.popup.supervisor")} dataSource={this.state.supervisorList} color="primary" id="supervisor" value={this.state.model.supervisor} enabled={false} allowFiltering={true} fields={{ text: 'firstName', value: 'id' }} floatLabelType="Auto" />

                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <div className='mt-3'></div>
                                                <span className="mr-3"> {t("cpm.lm.line.load.popup.status")} </span>

                                                <SwitchComponent checked={this.state.model.status} id={"status"}
                                                    disabled={this.state.isEdit ? this.state.transferObj.isTransferTo == false ? false : true : false}
                                                    change={this.onChangeFormSwitchStatus} />

                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.sam")} disabled={true} id="sam" value={this.state.model.sam} onChange={this.onChangeFormTextField} floatLabelType="Auto" />
                                            </Col>

                                            <Col xs='12' lg='2' md='2'>
                                                {/* <TextBoxComponent placeholder={'Supervisor'} id="prodConf" value='' onChange={this.onChangeTextField} floatLabelType="Auto" /> */}
                                                <DropDownListComponent placeholder={t("cpm.lm.line.load.popup.line")} enabled={this.state.isEdit ? this.state.model.confQty == 0 && this.state.transferObj.isTransferTo == false ? true : false : true}
                                                    color="primary" id="lineId" allowFiltering={true} dataSource={this.state.formUnitProductionLines} fields={{ text: 'lineName', value: 'id' }} value={this.state.model.lineId}
                                                    change={this.onChangeFormDropdown} floatLabelType="Auto" />

                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.materialDesc")} disabled={true} id="materialDesc" value={this.state.model.materialDesc} floatLabelType="Auto" />
                                            </Col>
                                            <Col xs='12' lg='2' md='2'>
                                                <TextBoxComponent placeholder={t("cpm.lm.line.load.popup.bomSeq")} disabled={true} id="altBom" value={this.state.model.altBom} floatLabelType="Auto" />
                                            </Col>
                                        </Row>
                                        {this.state.isEdit ? <div>
                                            <Row>
                                                <Col xs='12' lg='2' md='2'>
                                                    <div className='mt-3'></div>
                                                    <span className="mr-3"> {t("cpm.lm.line.load.popup.transferLoad")} </span>
                                                    <SwitchComponent checked={this.state.transferObj.isTransferTo} id={"isTransferTo"} change={this.onChangeFormSwitchTransfer} />
                                                </Col>
                                            </Row>
                                        </div> : null}

                                        {this.state.isEdit && this.state.transferObj.isTransferTo ? <div>
                                            <Row>
                                                <Col xs='12' lg='2' md='2'>
                                                    <NumericTextBoxComponent
                                                        min={0}
                                                        max={this.state.model.totalQtyLoaded - this.state.model.confQty}
                                                        placeholder={t("cpm.lm.line.load.popup.transferLoadQty")}
                                                        id="transferLoadQty"
                                                        decimals={0}
                                                        format='###.###'
                                                        value={this.state.transferObj.transferLoadQty}
                                                        onChange={this.onChangeTransferLoadQty}
                                                        floatLabelType="Auto"
                                                    >
                                                    </NumericTextBoxComponent>
                                                </Col>

                                                <Col xs='12' lg='2' md='2'>
                                                    <DropDownListComponent
                                                        placeholder={t("cpm.lm.line.load.popup.transferLine")}
                                                        color="primary"
                                                        id="transferToLine"
                                                        allowFiltering={true}
                                                        dataSource={
                                                            this.state.newTransferLine
                                                            
                                                        }
                                                        fields={{ text: 'lineName', value: 'id' }}
                                                        value={this.state.transferObj.transferToLine}
                                                        change={this.onChangeTransferLoadDropdown}
                                                        floatLabelType="Auto"
                                                    />
                                                </Col>
                                            </Row>
                                        </div> : null}


                                        <div className='mt-3'></div>
                                        <GridComponent gridLines='Both' allowTextWrap={true} allowSorting={true} dataSource={this.state.maxLoadsList} allowPaging={true} pageSettings={this.pageSettings} height='150'>
                                            <ColumnsDirective>
                                                <ColumnDirective allowSorting={true} field='materialKd' headerText='Material Code' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                <ColumnDirective allowSorting={true} field='materialDesc' headerText='Material Description' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                <ColumnDirective allowSorting={true} field='uom' headerText='Base Unit' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                <ColumnDirective allowSorting={true} field='sapStoreQty' headerText='Sap Store Qty' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                <ColumnDirective allowSorting={true} field='virtualStoreQty' headerText='Virtual Store Qty' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                <ColumnDirective allowSorting={true} field='sapConfQty' headerText='Sap Conf Qty' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                <ColumnDirective allowSorting={true} field='moepConfQty' headerText='Moep Conf Qty' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                <ColumnDirective allowSorting={true} field='sapRejectQty' headerText='Sap Reject Qty' isPrimaryKey={true} width='150' ></ColumnDirective>



                                                <ColumnDirective allowSorting={true} field='qtyLoaded' headerText='Qty Available' isPrimaryKey={true} width='150' ></ColumnDirective>
                                            </ColumnsDirective>
                                        </GridComponent>
                                        <div className='mt-3'></div>
                                        <div className="d-flex mb-3">
                                            <div><Button color="primary" size="sm" className="mr-2" onClick={this.onSave} id="LineLoadFrom"> Save</Button></div>
                                            <div hidden={!this.state.isEdit}>
                                                <Button color={this.state.isShowDaywiseView ? "primary" : "secondary"} size="sm" className="" disabled={this.state.transferObj.isTransferTo} onClick={this.showDayWiswView} id="daywiseView"> Day View</Button>
                                            </div>
                                            {this.state.isEdit ? <div><Button color={this.state.productionConfirmationdiv ? "primary" : "secondary"} size="sm" className="ml-2" disabled={this.state.transferObj.isTransferTo} onClick={this.productionConfirmation} id="productionConfirmation"> Production Confirmation</Button></div> : null}
                                            {this.state.isEdit && this.state.isShowHourlyView ? <div> Hourly View : <b>{this.state.selectedHourlyViewDate}</b> </div> : null}
                                        </div>


                                        {/* {(this.state.isShowDaywiseView && this.state.isEdit) ? */}
                                        <div hidden={!this.state.isShowDaywiseView}>
                                            {/* <div> */}
                                            <GridComponent
                                                gridLines='Both' allowTextWrap={true} allowSorting={true} dataSource={this.state.model.distributedDayWiseLoadTableDtos}
                                                actionBegin={this.distributedDayWiseGridActionBegin} recordDoubleClick={this.onDoubleClickDaywiseGrid} editSettings={this.state.daywiseGridEditSettings} ref={grid => this.daywiseViewObj = grid}
                                                allowPaging={true} pageSettings={this.pageSettings} >
                                                <ColumnsDirective>
                                                    <ColumnDirective allowSorting={true} field='prodOrderId' headerText='Prod Order' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='distributedQty' headerText='Loaded Qty' isPrimaryKey={true} width='100' format='###,##'></ColumnDirective>
                                                    {/* <ColumnDirective allowSorting={true} field='leftQty' headerText='WIP' isPrimaryKey={true} width='70' format='###,##'></ColumnDirective> */}
                                                    <ColumnDirective allowSorting={true} field='confQty' headerText='Prod Conf' isPrimaryKey={true} width='100' format='###,##' />
                                                    <ColumnDirective allowSorting={true} field='prodDate' format={this.state.format} headerText='Prod. Date' isPrimaryKey={true} width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='noOfmachines' headerText='Operators' editType='numericedit' edit={{ params: { decimals: 0, min: 0 } }} validationRules={{ required: true }} width='100' />
                                                    <ColumnDirective allowSorting={true} field='workingHrs' headerText='Working hrs' editType='numericedit' edit={{ params: { decimals: 0, min: 0 } }} validationRules={{ required: true }} width='80'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='status' headerText='status' isPrimaryKey={true} width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={false} headerText='Action' commands={this.state.daywiseGridcommands} width='100'></ColumnDirective>
                                                </ColumnsDirective>
                                                <Inject services={[Page, CommandColumn, Edit]} />
                                            </GridComponent>
                                            {/* </div> : null} */}
                                        </div>

                                        <div hidden={!this.state.isShowHourlyView}>
                                            {/* <div> */}
                                            <GridComponent
                                                gridLines='Both' allowTextWrap={true} allowSorting={true} dataSource={this.state.hourlyGridData}
                                                actionBegin={this.distributedHourWiseGridActionBegin} editSettings={this.state.hourwiseGridEditSettings} ref={grid => this.hourWiseViewObj = grid}
                                                allowPaging={true} pageSettings={this.pageSettings} >
                                                <ColumnsDirective>
                                                    <ColumnDirective allowSorting={true} field='hourType' headerText='Hour Type' isPrimaryKey={true} width='150' ></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='targetQty' headerText='Target Qty' isPrimaryKey={true} width='80'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='noOfHours' headerText='No of Hours' editType='numericedit' edit={{ params: { decimals: 0, min: 0 } }} validationRules={{ required: true }} width='100' format='###,##' />
                                                    <ColumnDirective allowSorting={true} field='noOfmachines' headerText='Operator' editType='numericedit' edit={{ params: { decimals: 0, min: 0 } }} validationRules={{ required: true }} width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='skilledWorker' headerText='Skilled Worker' editType='numericedit' edit={{ params: { decimals: 0, min: 0 } }} validationRules={{ required: true }} width='100' />
                                                    <ColumnDirective allowSorting={true} field='unskilledWorker' headerText='Unskilled Worker' edit={{ params: { decimals: 0, min: 0 } }} validationRules={{ required: true }} editType='numericedit' width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='labour' headerText='Labour' editType='numericedit' edit={{ params: { decimals: 0, min: 0 } }} validationRules={{ required: true }} width='100' format='###,##'></ColumnDirective>
                                                    <ColumnDirective allowSorting={false} headerText='Action' commands={this.state.daywiseGridcommands} width='100'></ColumnDirective>
                                                </ColumnsDirective>
                                                <Inject services={[Page, CommandColumn, Edit]} />
                                            </GridComponent>
                                            {/* </div> : null} */}
                                        </div>

                                        {/* {(this.state.productionConfirmationdiv && this.state.isEdit) ? <div> */}
                                        <div hidden={!this.state.productionConfirmationdiv}>
                                            <GridComponent gridLines='Both' allowTextWrap={true} allowSorting={true} dataSource={this.state.productionConfirmationData}
                                                actionBegin={this.prodConfGridActionBegin} editSettings={this.state.editSettingProdConf} ref={grid => this.prodConfObj = grid} commandClick={this.confCommandClick}
                                                allowPaging={true} pageSettings={this.pageSettings} >
                                                <ColumnsDirective>
                                                    <ColumnDirective allowSorting={true} allowEditing={false} field='requestType' headerText='Request Type' width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} allowEditing={false} field='createdDateTime' format={this.state.confirmationGridFormat} headerText='Created On' width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='acceptedQty' headerText='Accepted Qty' edit={{ params: { decimals: 0, min: 0, format: 'N' } }} editType='numericedit' validationRules={{ required: true }} width='150' ></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='defectedPieces.0.value' allowEditing={false} headerText='Defected Qty' edit={{ params: { decimals: 0, min: 0, format: 'N' } }} editType='numericedit' validationRules={{ required: true }} width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='reworkPieces.0.value' allowEditing={false} headerText='Rework Qty' edit={{ params: { decimals: 0, min: 0, format: 'N' } }} editType='numericedit' validationRules={{ required: true }} width='100' />
                                                    <ColumnDirective allowSorting={true} field='terminalMac' headerText='Terminal Mac' isPrimaryKey={true} width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='createdBy' headerText='Created By' allowEditing={false}  width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} field='modifiedBy' headerText='Modified By' allowEditing={false} width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} allowEditing={false} field='status' headerText='Status' width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={false} headerText='Action' validationRules={!this.state.productionConfirmationData} commands={this.state.productionOrderommands} width='100'></ColumnDirective>
                                                    <ColumnDirective allowSorting={true} allowEditing={false} field='remarks' headerText='Remarks' width='150' ></ColumnDirective>
                                                </ColumnsDirective>
                                                <Inject services={[Page, CommandColumn, Edit, Sort]} />
                                            </GridComponent>
                                        </div>

                                        {/* </div> : null} */}
                                        <div className='mt-3'></div>
                                    </div>

                                    {/* <div hidden={!this.state.showLoadlineFrom}>
                            dd
                        </div> */}
                                </div>
                            </DialogComponent>
                            <DialogComponent height="1000" className="model-lg" showCloseIcon={true} animationSettings={this.state.animationSettings} visible={this.state.poDialog} close={this.closeLineDialog.bind(this)}>
                                <div>
                                    <GridComponent allowTextWrap={true}
                                        gridLines='Both' dataSource={this.state.prodOrderLineDetails} allowPaging={true}>
                                        <ColumnsDirective>
                                            <ColumnDirective field='prodOrderId' headerText='Prod. Order' width='150' ></ColumnDirective>
                                            <ColumnDirective field='materialKd' headerText='Material KD' width='150' ></ColumnDirective>
                                            <ColumnDirective field='totalQtyLoaded' headerText='Loaded Qty' width='100' format='###,##'></ColumnDirective>
                                            {/* <ColumnDirective field='leftQty' headerText='Left Qty' width='100' format='###,##'></ColumnDirective> */}
                                            <ColumnDirective field='confQty' headerText='Conf Qty' width='70' format='###,##'></ColumnDirective>
                                            <ColumnDirective field='totalDefects' headerText='Defect Qty' width='100' format='###,##' />
                                            <ColumnDirective field='totalRework' headerText='Rework Qty' width='100'></ColumnDirective>
                                            <ColumnDirective field='status' headerText='Status' width='100' />
                                            <ColumnDirective field='remarks' headerText='Remark' width='80'></ColumnDirective>
                                        </ColumnsDirective>
                                        <Inject services={[Page]} />
                                    </GridComponent>
                                </div>
                            </DialogComponent>
                            {/* ---------------------------------- linelaod popup end ------------------------ */}
                        </div>
                    </div>
                </div>
                {/* <div className="overlayer-popup" hidden={this.state.lineLoadDialogOverLay} ></div> */}
                <div className="overlayer-popup" hidden={!this.state.lineLoadDialog} ></div></div>

        );
    }
}
export default withTranslation("translations")(Lineloading);
