
import * as React from 'react';
import { GridComponent, Resize, ColumnsDirective, CommandColumn, ColumnDirective, Filter, Reorder, Inject, Selection, VirtualScroll, Edit, Page, Sort, FilterSettingsModel, IFilter } from '@syncfusion/ej2-react-grids';
import { TextBoxComponent, NumericTextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import LineKeyConfigurationModel from './LineKeyConfigModel';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { withTranslation, Trans } from "react-i18next";
import { DialogComponent } from '@syncfusion/ej2-react-popups';
import SplitterLayout from 'react-splitter-layout';
import 'react-splitter-layout/lib/index.css';
import AppConfigService from './appconfigservice';
import errorMessages from '../../errorHandler/errorMessages';

class LineConfiguration extends React.PureComponent {
    constructor(props) {
        super(props);
        this.initialErrorStatus = {
            keyName: false,
            keyValue: false,
            keyDescription: false
        };
        this.mounted = false;
        this.AppConfigService = new AppConfigService();
        this.state = {
            model: new LineKeyConfigurationModel(null, "", "", ""),

            errorObject: this.initialErrorStatus,
            isSidebar: true,
            isEdit: false,
            isShowConfirmationDialog: false,
            isDisabled: false,
            validationErrorCss: "custom-error",
            keyName: '',
            description: '',
            keyValue: '',
            tableData: [],
            resetconfgKeys: [],
            confgKeys: [],
            editSettings: { allowEditing: false, allowAdding: false, allowDeleting: true },
            animationSettings: { effect: 'FlipYLeft' },
            commands: [
                { type: 'Edit', buttonOption: { iconCss: ' e-icons e-edit', cssClass: 'e-flat edit-btn' } },
                { type: 'Delete', buttonOption: { iconCss: 'e-icons e-delete', cssClass: 'e-flat' } },],
            FilterSettingsModel: { type: 'Menu' },
            IFilter: { type: 'CheckBox' },
            PageSettingsModel: { pageSize: 8, pageSizes: true },
            toasterPosition: { X: 'Right', Y: 'Bottom' },
            toasterMessage: [
                { title: 'Warning!', content: 'Changes Discarded !!', cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' },
                { title: 'Success!', content: 'Configuration key added successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'Something went wrong', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
                { title: 'Information!', content: 'Please read the comments carefully.', cssClass: 'e-toast-info', icon: 'e-info toast-icons' },
                { title: 'Success!', content: 'Configuration key updated successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Success!', content: 'Configuration key deleted successfully !!', cssClass: 'e-toast-success', icon: 'e-success toast-icons' },
                { title: 'Error!', content: 'Configuration key already exists!!', cssClass: 'e-toast-danger', icon: 'e-error toast-icons' },
            ],
            confirmButton: [{
                click: (args) => {
                    this.delete(args);

                },
                buttonModel: { content: 'Yes', isPrimary: true }
            },
            {
                click: (args) => {
                    this.cancelConformDialog(args);

                },
                buttonModel: { content: 'No' }
            }],
        }
    }
    componentDidMount() {
        this.mounted = true;
        if (this.mounted) {
            this.getAppConf();

            document.addEventListener("keydown", this.escFunction, false);
        }
    }
    //---------------------------------------------CRUD-----------------------------------------------//
    async getAppConf() {
        let confgKeys = []
        let response = (await this.AppConfigService.getConfgKey())

        if ((response != undefined) && (response.data.status == "success") && (this.mounted)) {
            response.data.data.map(keyObj => {
                confgKeys.push(new LineKeyConfigurationModel(keyObj.id, keyObj.keyName, keyObj.keyDescription, keyObj.keyValue))
            })
            this.setState({ tableData: confgKeys, resetconfgKeys: confgKeys })
        }
    }
    async deleteAppConfg(deletedLineId) {
        const { t, i18n } = this.props;
        let response = (await this.AppConfigService.deleteConfgKey(deletedLineId))
        if ((response != undefined) && (response.data.status == "success")) {
            await this.getAppConf();
            this.toastObj.show(this.state.toasterMessage[5]);
        }else if(response.data.status != undefined && response.data.status == "fail"){
            if(response.data.message != undefined){
                this.toastObj.show(this.createToaster(response.data.message,'Error'));
            }else{
                this.toastObj.show(this.createToaster(t(errorMessages[response.data.data.errorCode]),'Error')); 
            } 
            global.hideLoader();
        } else {
            this.toastObj.show(this.state.toasterMessage[2]);
        }
        this.setState({ lineIdToDelete: null });
    }
    async addKey(newKey) {
        const { t, i18n } = this.props;
        let response = (await this.AppConfigService.addConfgKey(newKey))
        if ((response != undefined) && (response.data.status == "success")) {
            await this.getAppConf()
            if (newKey.id != null) {
                this.toastObj.show(this.state.toasterMessage[4]);
            }
            else {
                this.toastObj.show(this.state.toasterMessage[1]);
            }
           
         }else if ((response != undefined) && (response.data.status == "fail")) {
            this.toastObj.show(this.createToaster(t(errorMessages[response.data.errorCode]),'Error'));
          //  global.hideLoader()
        }else if ((response != undefined) && (response.data.status == "unsuccess")) {
            this.toastObj.show(this.createToaster(response.data.message, "Error")); 
        }else{
        }

        this.setState({ isSidebar: true, isDisabled: false, isEdit: false, model: new LineKeyConfigurationModel(null, '', '', '') })
    }

    createToaster1(message) {
        return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
    }


    createToaster(message, type) {
        switch (type) {
          case "Success":
            return { title: 'Success!', content: message, cssClass: 'e-toast-success', icon: 'e-success toast-icons' }
          case "Error":
            return { title: 'Error!', content: message, cssClass: 'e-toast-danger', icon: 'e-error toast-icons' }
          case "Warning":
            return { title: 'Warning!', content: message, cssClass: 'e-toast-warning', icon: 'e-warning toast-icons' }
          case "Information":
            return { title: 'Information!', content: message, cssClass: 'e-toast-info', icon: 'e-info toast-icons' }
        }
      }




    isValidate(model){
        let isValidate = true;
        let errorObj = Object.assign({}, this.state.errorObject);
        if (model.keyName == null || model.keyName == "") {
            errorObj.keyName = true;
            isValidate = false;
        }
        else {
            errorObj.keyName = false;
        }
        if (model.keyValue == null || model.keyValue == "") {
            errorObj.keyValue = true;
            isValidate = false;
        }
        else {
            errorObj.keyValue = false;
        }
        //  if(model.keyDescription==null || model.keyDescription=="")
        //  {
        //     errorObj.keyDescription = true;
        //     isValidate = false;
        //  }
        //  else{
        //     errorObj.keyDescription = false;
        //  }
        this.setState({ errorObject: errorObj })
        return isValidate;
    }

    onSave = (event) => {

        let joined = Object.assign({}, this.state.model);

        if (this.isValidate(this.state.model)) {
            this.addKey(joined)
        }
    }
    
    async onEditClick(data) {
        let selectedRowLineProcesses = data.confgKeys;
        let allLineProcessesFromState = Object.assign([], this.state.confgKeys);

        selectedRowLineProcesses = allLineProcessesFromState.concat(selectedRowLineProcesses);
        // await this.getUnitList(data.plantId)
        this.setState({ model: data, isSidebar: false, confgKeys: selectedRowLineProcesses, isEdit: true })
    }


    //-------------------------------------ON-CHANGE EVENTS------------------------------------------------//
    sideBar = (event) => {
        // if (event.target.id == "cancel")
        this.setState({ isSidebar: !this.state.isSidebar, units: [], isDisabled: false, model: new LineKeyConfigurationModel(null, "", "", ""), confgKeys: this.state.resetconfgKeys, isEdit: false, errorObject: this.initialErrorStatus })
        console.log(this.state.model);
    }
    onChangeTextField = (event) => {
        let that = this;
        that.setState({ model: { ...that.state.model, [event.target.id]: event.target.value } })
    }
    //-------------------------------------------------OTHER EVENTS--------------------------------------//
    actionBegin = (args) => {                   // TO BE IMPROVED !!
        if (args.requestType == "delete") {
            args.cancel = true;
            const id = args.data[0].id;
            this.setState({ isShowConfirmationDialog: true, lineIdToDelete: id })
        }
    }
    rowselected = (args) => {                     // TO BE IMPROVED !!
        let that = this;
        if ((args.target.classList.length > 2) && (that.state.isSidebar == true)) {
            if ((args.target.classList.contains('e-edit')) && (args.data != undefined)) {
                that.onEditClick(args.data)
            }
        }
    }
    delete = (args) => {
        const { lineIdToDelete } = this.state;
        this.deleteAppConfg(lineIdToDelete);
        this.setState({ isShowConfirmationDialog: false })
    }
    cancelConformDialog = (args) => {
        this.setState({ isShowConfirmationDialog: false, lineIdToDelete: null })
    }


    //-------------------------------------------------RENDER--------------------------------------------//
    render() {
        const { t, i18n } = this.props;
        return (
            <div>
                <ToastComponent ref={(toast) => { this.toastObj = toast; }} showCloseButton={true} id='toast_type' position={this.state.toasterPosition}   ></ToastComponent>
                <div className="container-fluid-inner">
                    <h1 className='pagesheading'>App Configuration</h1>
                    <div className='clearfix'></div>
                    <Row>
                        <Col>
                            <Button color="primary" size="sm" className="mb-2" onClick={e => this.sideBar(e)}>{t("cpm.lm.addkey")} </Button>

                            <GridComponent allowTextWrap={true} gridLines='Both' allowReordering={true} actionBegin={this.actionBegin} editSettings={this.state.editSettings} selectionSettings={{ enableToggle: false }} rowSelected={this.rowselected} actionComplete={this.actionComplete} allowPaging={true} pageSettings={{ pageCount: 5, pageSizes: true }} allowResizing={true} dataSource={this.state.tableData} filterSettings={this.state.FilterSettingsModel} allowSorting={true} allowFiltering={true}>
                                <ColumnsDirective>
                                    <ColumnDirective field='keyName' filter={this.state.IFilter} headerText={t("cpm.lm.grid.key.name")} width='100' />
                                    <ColumnDirective field='keyDescription' filter={this.state.IFilter} headerText={t("cpm.lm.grid.line.description")} width='200' />

                                    <ColumnDirective field='keyValue' filter={this.state.IFilter} headerText={t("cpm.lm.grid.key.value")} width='100' />

                                    <ColumnDirective field='action' allowSorting={false} allowFiltering={false} headerText={t("cpm.lm.grid.action")} width='120' commands={this.state.commands} ></ColumnDirective>
                                </ColumnsDirective>
                                <Inject services={[CommandColumn, Sort, Filter, Resize, Reorder, Selection, Edit, Page]} />
                            </GridComponent>
                        </Col></Row>
                    {/* ----------------------- Add line Section  -----------------------  */}
                    <div className="add-line-section" hidden={this.state.isSidebar}>
                        <SplitterLayout secondaryInitialSize={550} secondaryMinSize={450}>
                            <div> </div>
                            <div className="add-line-section-form">
                                <div className="heading mb-2">{t("cpm.lm.addkey")} <a className="close" onClick={e => this.sideBar(e)}>+</a></div>

                                <Col xs='12'>
                                    <Row>
                                        <Col xs='12' md='6'>
                                            <div className={this.state.errorObject.keyName == true ? this.state.validationErrorCss : "required-field"} >
                                                <TextBoxComponent placeholder={t("cpm.lm.field.key.name")} id="keyName" value={this.state.model.keyName} disabled={this.state.isEdit} onChange={this.onChangeTextField} floatLabelType="Auto" />
                                                <span hidden={!this.state.errorObject.keyName} >{t("cpm.lm.line.validation.keyName")}</span>
                                            </div>
                                        </Col>

                                        <Col xs='12' md='6'>
                                            {/* <div className={this.state.errorObject.keyDescription == true ? this.state.validationErrorCss : "required-field"} > */}
                                            <TextBoxComponent placeholder={t("cpm.lm.field.key.description")} id="keyDescription" value={this.state.model.keyDescription} onChange={this.onChangeTextField} floatLabelType="Auto" />
                                            {/* <span hidden={!this.state.errorObject.keyDescription} >{t("cpm.lm.line.validation.lineName")}</span> */}
                                            {/* </div> */}
                                        </Col>

                                    </Row>
                                    <Row>
                                        <Col xs='12' md='6'>
                                            <div className={this.state.errorObject.keyValue == true ? this.state.validationErrorCss : "required-field"} >
                                                <TextBoxComponent placeholder={t("cpm.lm.field.key.value")} id="keyValue" value={this.state.model.keyValue} onChange={this.onChangeTextField} floatLabelType="Auto" />
                                                <span hidden={!this.state.errorObject.keyValue} >{t("cpm.lm.line.validation.keyvalue")}</span>
                                            </div>
                                        </Col>
                                    </Row>

                                    <Row className="mt-3">
                                        <Col>
                                            <Button color="primary" size="sm" id="save" disabled={this.state.isDisabled} className="float-left mr-3" onClick={e => this.onSave(e)}>{t("cpm.lm.line.save")}</Button>
                                            <Button color="primary" size="sm" id="cancel" className="float-left" onClick={e => this.sideBar(e)}>{t("cpm.lm.line.cancel")}</Button>
                                        </Col>
                                    </Row>
                                </Col>
                            </div>
                        </SplitterLayout>
                    </div>
                    <div className="add-line-section-overlay" hidden={this.state.isSidebar}></div>
                    {/* ----------------------- Add line Section end          -----------------------  */}
                </div>
                <DialogComponent id="confirmDialog" animationSettings={this.state.animationSettings} showCloseIcon={true} close={this.cancelConformDialog} buttons={this.state.confirmButton} header='Delete Configuration key !!' visible={this.state.isShowConfirmationDialog} width='400px' content='Are you sure you want to permanently delete key ?'></DialogComponent>
                <div className="overlayer-popup" hidden={!this.state.isShowConfirmationDialog} ></div>
            </div>
        );
    }
}

export default withTranslation("translations")(LineConfiguration);

