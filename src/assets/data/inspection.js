export const sampleData =
    [
        { id: 1, name: 'General Information', hasChild: true },
        { id: 2, pid: 1, name: 'General Info' },
        { id: 3, name: 'Criteria List', hasChild: true },
        { id: 4, pid: 3, name: 'Technical Verification' },
        { id: 5, pid: 3, name: 'Material Verfication' },
        { id: 6, pid: 3, name: 'Labeling' },
        { id: 7, pid: 3, name: 'Measurement' },
        { id: 8, pid: 3, name: 'Check Points' },
        { id: 9, pid: 3, name: 'Workmanship Verfiy' },
    ]

export const qualitymasterData =
    [
        { id: 1, name: 'Targeting Softhome Packing AQL', hasChild: false },
        { id: 2, name: 'Criteria List', hasChild: false },
        // { id: 3, pid: 0, name: 'Criteria List', hasChild: true },
        { id: 3, name: 'Radiaant Softhome Workmanship AQL', hasChild: false },
        { id: 4, name: 'Target Softhome Measurement AQL', hasChild: false },
        { id: 5, name: 'Target Softhome Workmanship AQL', },
    ]
export const defectsmasterdata =
    [
        { id: 1, name: 'Cutting', hasChild: false },
        { id: 2, name: 'Packaging', hasChild: false },
        // { id: 3, pid: 0, name: 'Criteria List', hasChild: true },
        { id: 3, name: 'Stiching', hasChild: false },
        { id: 4, name: 'Sock/Lining', hasChild: false },
    ]

export const sectionGridData = [
    {
        name: 'Technical Verfication',
        description: 'Technical verification prevents the importation of unsafe, substandard or counterfeit goods; it supports government authorities to enforce the implementation of obligatory legislations established for the protection of consumers  health and safety and the environment.',
        category: 'AQL',
        type: 'Field From',
    },
    {
        name: 'Measurement',
        description: 'Technical verification prevents the importation of unsafe, substandard or counterfeit goods; it supports government authorities to enforce the implementation of obligatory legislations established for the protection of consumers  health and safety and the environment.',
        category: 'AQL',
        type: 'Field From',
    },
    {
        name: 'section1',
        description: 'section1',
        type: 'section1',
    },
    {
        name: 'section1',
        description: 'section1',
        type: 'section1',
    }
]

export const orderDataSourceData = [
    {
        name:'Technical Verfication',
        sectionHead: 'Technical Verfication',
        description: 'Stiching',
        category: 'Stiching',
        action: '',
    },
    {
        name:'Technical Verfication',
        sectionHead: 'Technical Verfication',
        description: 'Stiching',
        category: 'Stiching',
        action: '',
    },

];