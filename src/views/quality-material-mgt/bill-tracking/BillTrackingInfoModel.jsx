export class BillTrackingInfoModel {
    constructor(totalValue, pendingOnDepartment, btId, assigneeName,assigneeUserID,grnDate,
        invoiceDate, vendorName, grnQty,grnNumber,invoiceNumber, btNumber, poType, btStatus,poNumber,poGroup,poTypeId,poGroupId,lotNumber,plantId,plantName,remark) {
        this.totalValue = totalValue;
        this.pendingOnDepartment = pendingOnDepartment;
        this.btId = btId;
        this.assigneeName = assigneeName;
        this.assigneeUserID = assigneeUserID;
        this.grnDate = grnDate;
        this.invoiceDate = invoiceDate;
        this.vendorName = vendorName;
        this.grnQty = grnQty;
        this.grnNumber = grnNumber;
        this.invoiceNumber = invoiceNumber;
        this.btNumber = btNumber;
        this.poType = poType;
        this.btStatus = btStatus;
        this.poNumber = poNumber;
        this.poGroup = poGroup;
        this.poTypeId = poTypeId;
        this.poGroupId = poGroupId;
        this.lotNumber = lotNumber;
        this.plantId = plantId;
        this.plantName = plantName;
        this.remark=remark;
    }
    totalValue = null;
    pendingOnDepartment = null;
    btId = null;
    assigneeName = null;
    assigneeUserID=null;
    grnDate = null;
    invoiceDate = null;
    vendorName = null;
    grnQty = null;
    grnNumber=null;
    invoiceNumber = null;
    btNumber = null;
    poType = null;
    btStatus = null;
    poNumber=null;
    poGroup=null;
    poTypeId=null;
    poGroupId=null;
    lotNumber = null;
    plantId = null;
    plantName = null;
}

export default BillTrackingInfoModel; 

 