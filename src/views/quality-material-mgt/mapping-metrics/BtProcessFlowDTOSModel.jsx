export class BtProcessFlowDTOSModel {
    constructor(processFlowId, processFlowName, assigneeId, assigneeName,sequence) {
        this.processFlowId = processFlowId;
        this.processFlowName = processFlowName;
        this.assigneeId = assigneeId;
        this.assigneeName = assigneeName;
        this.sequence = sequence;
    }
    processFlowId = null;
    processFlowName = null;
    assigneeId = null;
    assigneeName = null;
    sequence=null;
}

export default BtProcessFlowDTOSModel; 
