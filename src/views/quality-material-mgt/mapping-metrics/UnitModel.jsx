export class UnitModel {
    constructor(id, unitId, unitName) {
        this.id = id;
        this.unitId = unitId;
        this.unitName = unitName;
        
    }
    id = null;
    unitId = null;
    unitName = null;

}

export default UnitModel; 