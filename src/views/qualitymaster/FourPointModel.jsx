export class FourPointModel {
    constructor(id, minimumLength, maximumLength, penaltyPoint) {
        this.id = id;
        this.minimumLength = minimumLength;
        this.maximumLength = maximumLength;
        this.penaltyPoint = penaltyPoint;

    }
    id = null;
    minimumLength = null;
    maximumLength = null;
    penaltyPoint = null;
}
export default FourPointModel; 