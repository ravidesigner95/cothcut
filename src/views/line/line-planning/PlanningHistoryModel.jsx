export class PlanningHistoryModel {
    constructor(apmName, fmName, plantName, buyerName, qaName, unitName, merchandiserName, lineItemDesc, lineItemNo, leftPlannedQty, leftPoQtyTobePlanned,
         orderedQty, plannedQty, poQty, plantId, saidId, pcdDate, status,unitId, alertCode,pcdDateForFilter) {
        this.apmName = apmName;
        this.buyerName = buyerName;
        this.fmName = fmName;
        this.leftPlannedQty = leftPlannedQty;
        this.leftPoQtyTobePlanned = leftPoQtyTobePlanned;
        this.lineItemDesc = lineItemDesc;
        this.lineItemNo = lineItemNo;
        this.merchandiserName = merchandiserName;
        this.orderedQty = orderedQty;
        this.pcdDate = pcdDate;
        this.plannedQty = plannedQty;
        this.plantId = plantId;
        this.plantName = plantName;
        this.poQty = poQty;
        this.qaName = qaName;
        this.saidId = saidId;
        this.status = status;
        this.unitName = unitName;
        this.unitId=unitId;
        this.alertCode = alertCode;
        this.pcdDateForFilter=pcdDateForFilter;  //temporary variable for end date filter
    }
    pcdDateForFilter=null;
    apmName = null;
    buyerName = null;
    fmName = null;
    leftPlannedQty = null;
    leftPoQtyTobePlanned = null;
    lineItemDesc=null;
    lineItemNo = null;
    merchandiserName = null;
    orderedQty = null;
    pcdDate = null;
    plannedQty = null;
    plantId = null;
    plantName = null;
    poQty = null;
    qaName = null;
    saidId = null;
    status = null;
    unitName = null;
    unitId=null;
    alertCode = null;
}
export default PlanningHistoryModel;