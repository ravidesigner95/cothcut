import React from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { DialogComponent, ButtonPropsModel, AnimationSettingsModel } from '@syncfusion/ej2-react-popups';
import { GridComponent, ColumnsDirective, ColumnDirective, Filter, Inject, Grid, VirtualScroll, Sort, SelectionType, Selection, Resize, Page, Group, Edit, Toolbar, CommandColumn, Reorder } from '@syncfusion/ej2-react-grids';
import { DropDownListComponent } from '@syncfusion/ej2-react-dropdowns';
import { RadioButtonComponent } from '@syncfusion/ej2-react-buttons';
import { TextBoxComponent } from '@syncfusion/ej2-react-inputs';
import { ToastComponent, Toast, ToastModel } from '@syncfusion/ej2-react-notifications';
import { TreeViewComponent } from '@syncfusion/ej2-react-navigations';
import { TabComponent, TabItemDirective, TabItemsDirective } from '@syncfusion/ej2-react-navigations';
import { AccordionComponent, AccordionItemDirective, AccordionItemsDirective } from '@syncfusion/ej2-react-navigations';
import { sampleData } from '../../assets/data/inspection'
// import { sampleBase64pdf } from "../../../sampleBase64pdf";
class DclformComponent extends React.PureComponent {
  constructor(props) {
    super(...arguments);
    this.countries = sampleData;
    this.state = {
      filterBox: true,
      divHidden: null,
      listfield: { dataSource: this.countries, id: 'id', text: 'name', parentID: 'pid', hasChildren: 'hasChild' },

    }
  }
  componentDidMount() {
  }
  filterBoxOpen = () => {
    this.setState({
      filterBox: !this.state.filterBox
    });

  }
  content1 = () => {
    return (<div>
      <Row>
        <Col>
          <span className='margin-top16'>
            <Button color="info" size="sm" onClick="">Yes / No / NA</Button>&nbsp;
               <Button className="brdblue" size="sm" onClick="">Pass / Fail</Button>
            <Button className="brdblue" size="sm" onClick="">Free Text</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }
  content2 = () => {
    return (<div>
        <Row>
            <Col sm='2'>    <DropDownListComponent id="Category" filterBarPlaceholder='Defect Category' placeholder="Select a category" popupHeight="220px" /></Col>
        <Col sm='3'>
        <RadioButtonComponent checked={true} label='100% Checking' name='' value=""></RadioButtonComponent>
        </Col>
        <Col sm='2'>
        <RadioButtonComponent checked={false} label='AQL' name='' value=""></RadioButtonComponent>
        </Col>
        <Col sm='2'>
        <RadioButtonComponent checked={false} label='4 Point' name='' value=""></RadioButtonComponent>
        </Col>
        <Col sm='3'>
        <DropDownListComponent id="Category" filterBarPlaceholder='Select Material Type' placeholder="Select Material Type" popupHeight="220px" />
        </Col>
       
      </Row>
     
      <Row>
        <Col>
          <span className='float-right margin-top16'>
            <Button color="primary" size="sm" onClick="">Add Questions</Button>&nbsp;
               <Button className="btncancel" size="sm" onClick="">Cancel</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }
  content3 = () => {
    return (<div>
      <Row>
        <Col>
          <span className='margin-top16'>
            <Button color="info" size="sm" onClick="">Yes / No / NA</Button>&nbsp;
               <Button className="brdblue" size="sm" onClick="">Pass / Fail</Button>
            <Button className="brdblue" size="sm" onClick="">Free Text</Button>
          </span>
        </Col>
      </Row>
    </div>)
  }

  render() {
    return (
      <AccordionComponent>
      <AccordionItemsDirective>
        <AccordionItemDirective expanded='true' header='DCL Form' content={this.content2} />
        {/* <AccordionItemDirective  header="Does the stitched product design, Color & construction matches as per buyer's Approved Sample or Yellow seal sample and

technical specification" content={this.content3} />
        <AccordionItemDirective  header='Does the product size meet as per approved sample/technical specification.' content={this.content3} />
        <AccordionItemDirective  header='Are the approved Fabric, Accessories, trims used.' content={this.content3} /> */}

      </AccordionItemsDirective>
    </AccordionComponent>

    );
  }
}

export default DclformComponent;
