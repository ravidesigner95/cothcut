import React from 'react';
import { withTranslation } from "react-i18next"; 
 
class ProdQualityDashboard extends React.PureComponent {
  constructor(props) {
    super(...arguments);
      this.state = { 
    } 
  } 
  QMMDashboard= process.env.REACT_APP_MOEP_QMM_PROD_DASHBOARD; 
  render() {
    const { t, i18n } = this.props;
    return (
      <div className="container-fluid-inner billtracking">
        <h1 className='pagesheading'>Production Dashboard</h1>
        {/* <iframe src='https://app.powerbi.com/view?r=eyJrIjoiNmQyZTdjZjgtY2I3OS00Y2NlLWIwZmMtMzYxNzZhYzMyYzZlIiwidCI6Ijc1ZDg4MmE0LWFkM2EtNDM3Ni1hNGFhLTBhNjVhZmVmOTlkMiJ9&pageName=ReportSection' height="765" width="100%" allowFullScreen="true" frameBorder="0"></iframe>  */}
        <iframe src= {this.QMMDashboard} height="765" width="100%" allowFullScreen={true}  frameBorder="0"></iframe> 

        </div >
    );
  }
}

export default withTranslation("translations")(ProdQualityDashboard);
