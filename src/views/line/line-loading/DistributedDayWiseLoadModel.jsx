class DistributedDayWiseLoadModel {
    constructor(id, distributedQty, leftQty, confQty, workingHrs, reworkQty, noOfmachines, dataPoint, saidNr, lineId, lineItemNr, materialKd, labour,
        unskilledWorker, prodDate, loadId, defectQty, processId, processName, totalQtyLoaded, prodOrderId, sam, skilledWorker, distributedDayWiseHourlyLoadTableDtos,targetWip) {
        this.distributedQty = distributedQty;
        this.leftQty = leftQty;
        this.confQty = confQty;
        this.workingHrs = workingHrs;
        this.reworkQty = reworkQty;
        this.noOfmachines = noOfmachines;
        this.dataPoint = dataPoint;
        this.saidNr = saidNr;
        this.lineId = lineId;
        this.lineItemNr = lineItemNr;
        this.materialKd = materialKd;
        this.labour = labour;
        this.unskilledWorker = unskilledWorker;
        this.prodDate = prodDate;
        this.loadId = loadId;
        this.defectQty = defectQty;
        this.processId = processId;
        this.processName = processName;
        this.totalQtyLoaded = totalQtyLoaded;
        this.id = id;
        this.prodOrderId = prodOrderId;
        this.sam = sam;
        this.skilledWorker = skilledWorker;
        this.distributedDayWiseHourlyLoadTableDtos = distributedDayWiseHourlyLoadTableDtos;
        this.targetWip=targetWip;
    }
    targetWip=null;
    distributedDayWiseHourlyLoadTableDtos = null;
    distributedQty = null;
    leftQty = null;
    confQty = null;
    workingHrs = null;
    reworkQty = null;
    noOfmachines = null;
    dataPoint = null;
    saidNr = null;
    lineId = null;
    lineItemNr = null;
    materialKd = null;
    labour = null;
    unskilledWorker = null;
    prodDate = null;
    loadId = null;
    defectQty = null;
    processId = null;
    processName = null;
    totalQtyLoaded = null;
    id = null;
    prodOrderId = null;
    sam = null;
    skilledWorker = null;
}
export default DistributedDayWiseLoadModel;