export class InspectionModel {
    constructor(id, code,date, description,isActive,name,revision,rulesDto,qualityComponentMasterDTOS,
        inspectionCategoryMasterDTO,category,headerList,isAutomatic,inspTypeInfo) {
        this.id = id;
        this.code = code;
        this.date=date;
        this.description= description;
        this.isActive=isActive;
        this.name=name;
        this.revision=revision;
        this.rulesDto=rulesDto;
        this.qualityComponentMasterDTOS=qualityComponentMasterDTOS;
        this.inspectionCategoryMasterDTO= inspectionCategoryMasterDTO;
        this.category=category;
        this.headerList=headerList;
        this.isAutomatic=isAutomatic;
        this.inspTypeInfo=inspTypeInfo;
        

    }
    id=null;
    code = null;
    date = null;
    description=null;
    isActive =null;
    name=null;
    revision=null;
    rulesDto=null;
    qualityComponentMasterDTOS=[];
    inspectionCategoryMasterDTO=null;
    category=null;
    headerList=[];
    isAutomatic=null;
    inspTypeInfo=null;
}
export class InspectionTypeInfoDTO {
    constructor(id,name,value){
        this.id=id;
        this.name=name;
        this.value=value;
    }
    id=null;
    name=null;
    value=null;
    
    
}


export default InspectionModel; 
