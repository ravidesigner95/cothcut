import axios from 'axios';

class LineProcessService {
    constructor() { }
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    deletUrl = this.baseUrl + `/lineProcess/`;
    getLineProcessesUrl = this.baseUrl + `/master/process`;
    addLineProcessesUrl = this.baseUrl + `/lineProcess`;
    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };

    async getLineProcesses() {
        try {
            const response = await axios.get(this.getLineProcessesUrl, this.options);
           return response;
         
        }
        catch (error) {
            console.log(error); 
            // alert("error")
        }
    }
    async addLineProcess(newline) {
        try {
            const response = await axios.post(this.addLineProcessesUrl, newline ,this.options)
            return response;
        }
        catch (error) {
            console.log(error); 
            return error.response;
            // alert("error")
        }
    }

    async deleteLine(deletedLineId) {
        try {
            const response = await axios.delete(this.deletUrl + deletedLineId,this.options)
            return response;
        }
        catch (error) {
            console.log(error); 
           // alert("error")
        }
    }

}
export default LineProcessService;