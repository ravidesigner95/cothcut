import axios from 'axios';
class UserProfileService {
    version = "/v1"
    baseUrl = process.env.REACT_APP_BASEURL + this.version;
    rolesUrl = this.baseUrl + `/roles`;
    updateRolesUrl=this.baseUrl + `/updateroles`;
    getrolesUrl = this.baseUrl + `/roles`;
    moduleUrl = this.baseUrl + `/module`;
    permissionURL = this.baseUrl + `/role/permission`;
    deletePermissionURL =  this.baseUrl +`/roles`;

    options = {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem("ACCESS_TOKEN")
        }
    };
    async saveUser(data) {
        console.log(this.moduleUrl)
        try {
            const response = await axios.post(this.moduleUrl, data, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }
    async getroleList() {
        try {
            const response = await axios.get(this.rolesUrl, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }
    async getmoduleList() {
        try {
            const response = await axios.get(this.moduleUrl, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }

    async getGridData() {
        try {
            const response = await axios.get(this.getrolesUrl, this.options);
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
    async getPermissions(roleName) {
        try {
            let _URL = this.permissionURL
            if (roleName != null && roleName != undefined && roleName.trim() != "")
                _URL = _URL + "?roleName=" + roleName
            const response = await axios.get(_URL, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }
    async savePermission(permissionDTO) {
        console.log(this.permissionURL)
        try {
            const response = await axios.post(this.permissionURL, permissionDTO, this.options)
            return response.data;
        }
        catch (error) {
            alert(error)
        }
    }
    async deleteStatusList(deletedLineId) {
        try {
            const response = await axios.delete(this.permissionURL + deletedLineId, this.options)
            return response.data;
        }
        catch (error) {
            alert("error")
        }
    }
    async saveRole(roleName) {
        console.log(this.rolesUrl)
        try {
            const response = await axios.post(this.rolesUrl + "?role=" + roleName, null, this.options)
            return response.data;
        }
        catch (error) {
            alert(error)
        }
    }

    async updateRole(roleDto) {
        console.log(this.rolesUrl)
        try {
            const response = await axios.post(this.updateRolesUrl , roleDto, this.options)
            return response.data;
        }
        catch (error) {
            alert(error)
        }
    }





    async deletePermission(roleName) {
        try {
            const response = await axios.delete(this.deletePermissionURL + "?role=" + roleName, this.options)
            return response;
        }
        catch (error) {
            console.log(error);
            // alert("error")
        }
    }
}
export default UserProfileService;
