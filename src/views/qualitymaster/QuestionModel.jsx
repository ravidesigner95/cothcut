export class QuestionModel {
    constructor(id, quesType,quesText, prefilledIndicator,mappingField,status,isBold,
        categoryId,quesGroupId,quesGroupOption,quesGroupOptionAction,
        qualityQuesAnsGroupDTO,questionCategoryDTOS  ,categoryName,ansType,actionCount) {
        this.id = id;
        this.quesType = quesType;
        this.quesText=quesText;
        this.prefilledIndicator= prefilledIndicator;
        this.mappingField=mappingField;
        this.status=status;
        this.isBold=isBold;
        this.categoryId=categoryId;
        this.quesGroupId=quesGroupId;
        this.quesGroupOption=quesGroupOption;
        this.quesGroupOptionAction=quesGroupOptionAction;  
        this.qualityQuesAnsGroupDTO=qualityQuesAnsGroupDTO;
        this.questionCategoryDTOS=questionCategoryDTOS;
        // this.questionCategoryDTOS=questionCategoryDTOS;
        // this.qualityQuesAnsGroupDTO=qualityQuesAnsGroupDTO;
        // this.quesAnsGroupOptionDTOS=quesAnsGroupOptionDTOS;
        // this.qualityQuesAnsGroupOptionActionDTOS=qualityQuesAnsGroupOptionActionDTOS;    
        
        //only for UI purpose 
        this.categoryName=categoryName;
        this.ansType=ansType;
        this.actionCount=actionCount;

    }
    id = null;
    quesType = null;
    quesText=null;
    prefilledIndicator =null;
    mappingField=null;
    status=null;
    isBold=null;
    categoryId=[];
    quesGroupId=null;
    quesGroupOption=[];
    quesGroupOptionAction=[];
    qualityQuesAnsGroupDTO=null;
    questionCategoryDTOS=null
    // questionCategoryDTOS=null;
    // qualityQuesAnsGroupDTO=null;
    // quesAnsGroupOptionDTOS=null;
    // qualityQuesAnsGroupOptionActionDTOS=null;

    //only for UI purpose
    categoryName=null;
    ansType=null;
    actionCount=null;



}

export default QuestionModel; 
