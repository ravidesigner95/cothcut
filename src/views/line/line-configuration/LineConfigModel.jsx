
export class LineConfigurationModel {
    constructor(plantId, unitId, id, plantName, lineName, unitName, status, isInternal, numberOfMachines, prodSubLineDtos, categories, lineProcesses, workingHour, supervisor, prodLineShiftTimingsDtos) {
        this.plantId = plantId;
        this.unitId = unitId;
        this.id = id;
        this.plantName = plantName;
        this.lineName = lineName;
        this.unitName = unitName;
        this.status = status;
        this.isInternal = isInternal;
        this.numberOfMachines = numberOfMachines;
        this.prodSubLineDtos = prodSubLineDtos;
        this.supervisor = supervisor;
        //  this.categories.push(categories);
        this.setCategory(categories)
        this.lineProcesses = lineProcesses;
        this.workingHour = workingHour;
        this.prodLineShiftTimingsDtos = prodLineShiftTimingsDtos;
    }
    plantId = null;
    unitId = null;
    id = null;
    supervisor = null;
    numberOfMachines = null;
    prodSubLineDtos = []
    isInternal = null;
    plantName = "";
    lineName = "";
    unitName = "";
    status = null;
    categories = [];
    lineProcesses = [];
    workingHour = null;
    prodLineShiftTimingsDtos = [];

    setCategory(categories) {
        if (categories != undefined) {
            this.categories.push(categories)
        }
    }

}

export default LineConfigurationModel;


