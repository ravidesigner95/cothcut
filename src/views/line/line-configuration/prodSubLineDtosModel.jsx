export class ProdSubLineDtosModel {
  workingHour = null;
  constructor(id, category, endDate, numberOfMachines, startDate, workingHour, prodLineId) {
    this.id = id;
    this.category = category;
    this.endDate = endDate;
    this.numberOfMachines = numberOfMachines;
    this.startDate = startDate;
    this.workingHour = workingHour;
    this.prodLineId = prodLineId;
  }

  id = null;
  category = null;
  endDate = null;
  numberOfMachines = null;
  startDate = null;
  workingHour = null;
  prodLineId = null;
}

export default ProdSubLineDtosModel;