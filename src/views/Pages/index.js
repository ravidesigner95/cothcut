import Login from './Login';
import Page404 from './Page404';
import Page500 from './Page500';  
import Resetpassword from './Reset-password/Resetpassword'
import Noaccess from '/No-access/Noaccess'


export {
  Login, Page404, Page500,Resetpassword,Noaccess
};